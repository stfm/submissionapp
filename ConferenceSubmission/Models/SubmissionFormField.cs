﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConferenceSubmission.Models
{
    public class SubmissionFormField
    {
        public SubmissionFormField()
        {
        }

        public int SubmissionFormFieldId { get; set; }

        public int SubmissionId { get; set; }

        public int FormFieldId { get; set; }

        public FormField FormField {get; set; }

        [Required]
        public String SubmissionFormFieldValue { get; set; }
    }
}
