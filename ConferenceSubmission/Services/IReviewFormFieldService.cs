﻿using System;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.Services
{
    public interface IReviewFormFieldService
    {

        /// <summary>
        /// Adds the review form field.
        /// </summary>
        /// <param name="reviewFormField">Review form field.</param>
        void AddReviewFormField(ReviewFormField reviewFormField);

        /// <summary>
        /// Gets the review form fields for review - these are
        /// the user's answers to the review questions for
        /// a specific review.
        /// </summary>
        /// <returns>The form fields for review.</returns>
        /// <param name="reviewId">Review identifier.</param>
        IEnumerable<ReviewFormField> GetFormFieldsForReview(int reviewId);

        /// <summary>
        /// Removes all review form fields for review.
        /// </summary>
        /// <param name="reviewId">Review identifier.</param>
        void RemoveAllReviewFormFieldsForReview(int reviewId);
    }
}
