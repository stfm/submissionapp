﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ConferenceSubmission.Mappers
{
    public interface IUserMapper
    {
        User Map(IEnumerable<Claim> claims);
    }

    public class UserMapper : IUserMapper
    {
        public User Map(IEnumerable<Claim> claims)
        {
            var user = new User();

            user.UserName = claims.FirstOrDefault(c => c.Type == "urn:salesforce:username") == null ? 
                            "" : claims.FirstOrDefault(c => c.Type == "urn:salesforce:username").Value;

            user.FirstName  = claims.FirstOrDefault(c => c.Type == "urn:salesforce:first_name") == null ? 
                            "": claims.FirstOrDefault(c => c.Type == "urn:salesforce:first_name").Value;

            user.LastName = claims.FirstOrDefault(c => c.Type == "urn:salesforce:last_name") == null ? 
                            "" : claims.FirstOrDefault(c => c.Type == "urn:salesforce:last_name").Value;

            user.Email = claims.FirstOrDefault(c => c.Type == "urn:salesforce:email") == null ? 
                            "" : claims.FirstOrDefault(c => c.Type == "urn:salesforce:email").Value;

            user.STFMUserId = claims.FirstOrDefault(c => c.Type == "urn:salesforce:user_id") == null ?
                            "" : claims.FirstOrDefault(c => c.Type == "urn:salesforce:user_id").Value;

            return user;
        }
    }
}
