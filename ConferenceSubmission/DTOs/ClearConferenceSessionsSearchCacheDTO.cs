﻿namespace ConferenceSubmission.DTOs
{
    public class ClearConferenceSessionsSearchCacheDTO
    {
        public int ConferenceId { get; set; }
    }
}
