﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Data
{
    public interface ISessionObjectiveRepository
    {
        /// <summary>
        /// Get all session objectives for all accepted submissions.
        /// </summary>
        /// <returns></returns>
        IEnumerable<SessionObjectives> getSessionObjectives();

        /// <summary>
        /// Get all session objects for accepted submissions
        /// in the provided conference.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns></returns>
        IEnumerable<SessionObjectives> getSessionObjectives(int conferenceId);

        /// <summary>
        /// Get a map of submissionId to SessionObjectives for all accepted
        /// submissions in a the provided conference.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns></returns>
        Dictionary<int, SessionObjectives> getSessionObjectivesDictionary(int conferenceId);

        /// <summary>
        /// Get a map of submissionId to SessionObjectives for each submissionId
        /// provided.
        /// </summary>
        /// <param name="submissionIds"></param>
        /// <returns></returns>
        Dictionary<int, SessionObjectives> getSessionObjectivesDictionary(IEnumerable<int> submissionIds);
    }
}
