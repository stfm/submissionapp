﻿using ConferenceSubmission.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Data
{
    public class VirtualConferenceDataRepository : IVirtualConferenceDataRepository
    {

        private readonly AppDbContext _appDbContext;

        public VirtualConferenceDataRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public VirtualConferenceData GetVirtualConferenceData(int conferenceId)
        {
            return _appDbContext.VirtualConferenceData.Include(vc => vc.Conference).FirstOrDefault(vc => vc.ConferenceId == conferenceId);
        }
    }
}
