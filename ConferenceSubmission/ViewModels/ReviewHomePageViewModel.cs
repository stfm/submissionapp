﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ReviewHomePageViewModel
    {
        public List<AssignedSubmissionViewModel> AssignedSubmissions        { get; set; }
        public List<Conference>                  ConferencesWithOpenReviews { get; set; }

    }
}
