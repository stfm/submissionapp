﻿using System;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.Services
{
    public interface IDisclosureService
    {

        /// <summary>
        /// Gets the disclosure.
        /// </summary>
        /// <returns>The disclosure.</returns>
        /// <param name="disclosureId">Disclosure identifier.</param>
        Disclosure GetDisclosure(int disclosureId);

        /// <summary>
        /// Gets the disclosure using the provides STFM User ID.
        /// </summary>
        /// <returns>The disclosure.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        Disclosure GetDisclosure(string stfmUserId);

        /// <summary>
        /// Determine if the provided person has a disclosure and if the 
        /// disclosure is within 1 year of the conference start date.
        /// </summary>
        /// <returns><c>true</c>, if disclosure's date is within 1 year of the conference start date <c>false</c> otherwise.</returns>
        /// <param name="conferenceStartDate">Conference start date.</param>
        /// <param name="stfmUserId">STFM user Id (AKA Salesforce ID)</param>
        bool isDisclosureCurrent(DateTime conferenceStartDate, string stfmUserId);


        /// <summary>
        /// Adds the disclosure.
        /// </summary>
        /// <returns>The disclosure id value</returns>
        /// <param name="disclosure">Disclosure.</param>
        int AddDisclosure(Disclosure disclosure);

        /// <summary>
        /// Update an existing disclosure.
        /// </summary>
        /// <param name="disclosure">Disclosure.</param>
        void UpdateDisclosure(Disclosure disclosure);

        /// <summary>
        /// Get all disclosures for the provided submission.
        /// </summary>
        /// <param name="submission">Submission</param>
        /// <returns>Collection of Disclosure Objects - one for each participant</returns>
        List<Disclosure> GetDisclosures(Submission submission);

        /// <summary>
        /// Using the provided disclosure, rename the CV file to include dislosure's
        /// full name.
        /// </summary>
        /// <param name="disclosure">Disclosure</param>
        /// <returns>true if file was renamed successfully</returns>
        bool renameCvFile(Disclosure disclosure);
    }
}
