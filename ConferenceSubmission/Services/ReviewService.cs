﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
	public class ReviewService : IReviewService
    {

        public readonly IReviewRepository _reviewRepository;

        public readonly IReviewFormFieldRepository _reviewFormFieldRepository;

        private readonly IFormFieldRepository _formFieldRepository;

        public ReviewService(IReviewRepository reviewRepository, IReviewFormFieldRepository reviewFormFieldRepository,
                             IFormFieldRepository formFieldRepository)
        {

            _reviewRepository = reviewRepository;

            _reviewFormFieldRepository = reviewFormFieldRepository;

            _formFieldRepository = formFieldRepository;

        }

        public int SaveReview(Review review)
        {
            return _reviewRepository.AddReview(review);
        }

        public bool IsSubmissionCategoryReviewCompleted(int reviewId)
        {
            //Get collection of answers user has provided for Submission Category review Form Fields

            List<ReviewFormField> reviewFormFields = _reviewFormFieldRepository.GetFormFieldsForReview(reviewId).ToList<ReviewFormField>();

            //Get Review

            Review review = _reviewRepository.GetReview(reviewId);

           
            //Get collection of Review FormField objects where AnswerRequired is true

            List<FormField> formFieldsAnswerRequired = _formFieldRepository.GetFormFieldsAnswerRequired(review.Submission.SubmissionCategoryId, "review").ToList<FormField>();


            foreach (FormField formField in formFieldsAnswerRequired)
            {

                if (!reviewFormFields.Any(sff => sff.FormFieldId == formField.FormFieldId))
                {

                    //this FormField where an answer is required is not in the collection
                    //of ReviewFormField objects so user did not answer all the 
                    //submission category review form questions that are required. Return false.
                    return false;
                }

            }
            //If we're here, we know that for each required FormField, we found a corresponding 
            //ReviewFormField object, so the user answered all the submission category 
            //review form questions that are required. Return true.
            return true;

        }

        public void UpdateReview(Review review)
        {
            _reviewRepository.UpdateReview(review);
        
        }

        public Review GetReview(int reviewId)
        {

            return _reviewRepository.GetReview(reviewId);

        }

        public bool CheckIfReviewExists(int reviewerId, int submissionId)
        {
            bool reviewExists = false;

            Review review = _reviewRepository.GetReview(submissionId, reviewerId);

            if (review != null)
            {

                reviewExists = true;

            }

            return reviewExists;

        }
    }
}
