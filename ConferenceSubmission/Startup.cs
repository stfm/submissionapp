﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ConferenceSubmission.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using AspNet.Security.OAuth.Salesforce;
using Microsoft.AspNetCore.Authentication;
using ConferenceSubmission.Mappers;
using AutoMapper;
using ConferenceSubmission.Data;
using ConferenceSubmission.Services;
using DinkToPdf.Contracts;
using DinkToPdf;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ConferenceSubmission
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration       = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            string DefaultConnection = Configuration["DefaultConnection"];

            services.AddDbContext<AppDbContext>(options =>
                                                        options.UseSqlServer(DefaultConnection));

            services.AddAutoMapper(typeof(Startup));

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })

            .AddCookie(options =>
            {
                options.LoginPath = "/signin";
                options.LogoutPath = "/signout";
                options.AccessDeniedPath = "/Notauthorized";
            })
            .AddSalesforce(options => {
                options.ClientId = Configuration["Salesforce:ClientId"];
                options.ClientSecret = Configuration["Salesforce:ClientSecret"];
                options.Environment = Configuration["Salesforce:Environment"] == "Test" ? 
                                        SalesforceAuthenticationEnvironment.Test : 
                                        SalesforceAuthenticationEnvironment.Production;
                options.CallbackPath = SalesforceAuthenticationDefaults.CallbackPath;
                options.ClaimActions.MapJsonKey("urn:salesforce:username", "username");
                options.ClaimActions.MapJsonKey("urn:salesforce:user_id", "user_id");
                options.ClaimActions.MapJsonKey("urn:salesforce:first_name", "first_name");
                options.ClaimActions.MapJsonKey("urn:salesforce:last_name", "last_name");
                options.ClaimActions.MapJsonKey("urn:salesforce:email", "email");
                //options.ClaimActions.MapCustomJson("urn:salesforce:UserProfile", user => user["custom_attributes"]?.Value<string>("UserProfile"));
                options.ClaimActions.MapCustomJson("urn:salesforce:UserProfile", user => user.GetProperty("custom_attributes").GetString("UserProfile"));
                //options.ClaimActions.MapCustomJson("urn:salesforce:UserRole", user => user["custom_attributes"]?.Value<string>("UserRole"));
                options.ClaimActions.MapCustomJson("urn:salesforce:UserRole", user => user.GetProperty("custom_attributes").GetString("UserRole"));
                //options.ClaimActions.MapCustomJson("urn:salesforce:AccessPermissions", user => user["custom_attributes"]?.Value<string>("AccessPermissions"));
                options.ClaimActions.MapCustomJson("urn:salesforce:AccessPermissions", user => user.GetProperty("custom_attributes").GetString("AccessPermissions"));


                options.AuthorizationEndpoint = $"{Configuration["CommunityUrl"]}/services/oauth2/authorize";
            });

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {

                        builder.WithOrigins("https://stfmwebsite-dev.azurewebsites.net",
                                            "https://stfm.org");
                    });

                

            });

            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ItAdmin",
                                    policy => policy.RequireClaim("urn:salesforce:AccessPermissions", "It-Admin"));
                options.AddPolicy("ConferenceAdmin",
                                    policy => policy.RequireClaim("urn:salesforce:AccessPermissions", "Conference-Admin", "It-Admin"));
                options.AddPolicy("CommitteeMember",
                                    policy => policy.RequireClaim("urn:salesforce:AccessPermissions", "Committee-Member", "Conference-Admin", "It-Admin"));
            });

            services.AddHttpClient();

            services.AddControllersWithViews(options =>
                options.ModelBinderProviders.RemoveType<DateTimeModelBinderProvider>());

            //Inject dependencies
            services.AddScoped<IUserMapper, UserMapper>();

            services.AddScoped<ISubmissionCategoryRepository, SubmissionCategoryRepository>();

            services.AddScoped<IConferenceRepository, ConferenceRepository>();

            services.AddScoped<ISubmissionRepository, SubmissionRepository>();

			services.AddScoped<IParticipantRepository, ParticipantRepository>();

            services.AddScoped<ISubmissionService, SubmissionService>();

            services.AddScoped<IConferenceService, ConferenceService>();

            services.AddScoped<ISubmissionCategoryService, SubmissionCategoryService>();

			services.AddScoped<IParticipantService, ParticipantService>();

            services.AddScoped<ISubmissionStatusRepository, SubmissionStatusRepository>();

            services.AddScoped<ISubmissionStatusService, SubmissionStatusService>();

			services.AddScoped<IFormFieldRepository, FormFieldRepository>();

			services.AddScoped<IFormFieldService, FormFieldService>();

            services.AddScoped<IEncryptionService, EncryptionService>();

            services.AddScoped<ISalesforceAPIService, SalesForceAPIService>();

            services.AddScoped<ISubmissionParticipantRepository, SubmissionParticipantRepository>();

            services.AddScoped<ISubmissionParticipantService, SubmissionParticipantService>();

            services.AddScoped<ISubmissionPaymentService, SubmissionPaymentService>();

            services.AddScoped<IDisclosureRepository, DisclosureRepository>();

            services.AddScoped<IDisclosureService, DisclosureService>();

            services.AddScoped<ISubmissionPrintService, SubmissionPrintService>();

            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));

            services.AddScoped<IEmailNotificationService, EmailNotificationService>();

            services.AddScoped<IReviewerRepository, ReviewerRepository>();

            services.AddScoped<IReviewerService, ReviewerService>();

            services.AddScoped<IReviewStatusRepository, ReviewStatusRepository>();

            services.AddScoped<IReviewRepository, ReviewRepository>();

            services.AddScoped<IReviewStatusService, ReviewStatusService>();

            services.AddScoped<IReviewService, ReviewService>();

            services.AddScoped<IReviewFormFieldRepository, ReviewFormFieldRepository>();

            services.AddScoped<IReviewFormFieldService, ReviewFormFieldService>();

            services.AddScoped<IReviewAdminService, ReviewAdminService>();

            services.AddScoped<IFAQRepository, FAQRepository>();

            services.AddScoped<IFAQService, FAQService>();

            services.AddScoped<IConferenceSessionService, ConferenceSessionService>();

            services.AddScoped<IConferenceSessionRepository, ConferenceSessionRepository>();

            services.AddScoped<ISessionObjectiveRepository, SessionObjectiveRepository>();

            services.AddScoped<IAnnouncementRepository, AnnouncementRepository>();

            services.AddScoped<IAnnouncementService, AnnouncementService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<ICreateAuditLogsService, CreateAuditLogsService>();

            services.AddScoped<IAuditLogRepository, AuditLogRepository>();

            services.AddScoped<IGetAuditLogsService, GetAuditLogsService>();

            services.AddScoped<ISalesforceGetUsersService, SalesforceGetUsersService>();

            services.AddScoped<ISubmissionUpdateRepository, SubmissionUpdateRepository>();

            services.AddScoped<ISubmissionReviewerRepository, SubmissionReviewerRepository>();

            services.AddScoped<IPosterHallService, PosterHallService>();

            services.AddScoped<IVirtualMaterialRepository, VirtualMaterialRepository>();

            services.AddScoped<IVirtualMaterialService, VirtualMaterialService>();

            services.AddScoped<IFormPreviewService, FormPreviewService>();

            services.AddScoped<IVirtualConferenceDataRepository, VirtualConferenceDataRepository>();

            services.AddScoped<IVirtualConferenenceDataService, VirtualConferenceDataService>();

            services.AddScoped<ISubmissionPresentationMethodRepository, SubmissionPresentationMethodRepository>();

            services.AddScoped<IGetSubmissionActionsForSubmissionService, GetSubmissionActionsForSubmissionService>();

            services.AddScoped<IVirtualConferenceSessionRepository, VirtualConferenceSessionRepository>();

            services.AddScoped<IVirtualConferenceSessionService, VirtualConferenceSessionService>();

            services.AddScoped<IConferenceSessionsCacheService, ConferenceSessionsCacheService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {

                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");

            }

            app.UseStaticFiles();

            app.UseRouting();
            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
