﻿using System;
using System.ComponentModel.DataAnnotations;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Stores the user's input for setting session data
    /// for a submission.
    /// </summary>
    public class SessionDataBindingModel
    {
        public SessionDataBindingModel()
        {
        }

        public SubmissionSessionViewModel SubmissionData { get; set; }


        [Required]
        public int SubmissionId { get; set; }

        [Required(ErrorMessage = "The session code is required")]
        [Display(Name = "Session Code")]
        public string SessionCode { get; set; }

        [Required(ErrorMessage = "The session start DateTime is required")]
        [Display(Name = "Session Start DateTime")]
        public DateTime SessionStartDateTime { get; set; }

        [Required(ErrorMessage = "The session end DateTime is required")]
        [Display(Name = "Session End DateTime")]
        public DateTime SessionEndDateTime { get; set; }

        [Required(ErrorMessage = "The session location is required")]
        [Display(Name = "Session Location")]
        public string SessionLocation { get; set; }

        [StringLength(250)]
        public string SessionTrack { get; set; }

        public SubmissionPresentationMethod SubmissionPresentationMethod { get; set; }

        public override string ToString()
        {

            return "Session code: " + SessionCode + " |Session Start DateTime: " +
                SessionStartDateTime + " |Session End DateTime: " +
                SessionEndDateTime + " |Session Location: " +
                SessionLocation + " |Session Track: " + SessionTrack;
        }
    }
}
