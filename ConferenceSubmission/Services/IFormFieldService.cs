﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public interface IFormFieldService
    {

		/// <summary>
        /// Get all the FormField objects associated with provided
        /// submission category ID and form field role (e.g. proposal or review)
        /// </summary>
        /// <returns>The form fields.</returns>
        /// <param name="submissionCategoryId">Submission category identifier.</param>
        /// <param name="formFieldRole">Form Field Role (e.g. proposal or review)</param>
        IEnumerable<FormField> GetFormFields(int submissionCategoryId, string formFieldRole);

        /// <summary>
        /// Get all the SubmissionFormField objects associated with a specific SubmissionId
        /// </summary>
        /// <param name="submissionId">Id of the submission</param>
        /// <returns>A list of SubmissionFormField objects</returns>
        IEnumerable<SubmissionFormField> GetFormFieldsForSubmission(int submissionId);

        /// <summary>
        /// Deletes all SubmissionFormFieds for a submission.
        /// </summary>
        /// <param name="submissionId">submission's unique identifier</param>
        /// <returns></returns>
        int RemoveAllFormFieldsForSubmission(int submissionId);

		/// <summary>
        /// Adds the submission form field - this is the user's answer to
        /// a submission proposal question.
        /// </summary>
        /// <param name="submissionFormField">Submission form field.</param>
        void AddSubmissionFormField(SubmissionFormField submissionFormField);

        /// <summary>
        /// Updates the SubmissionFormFieldValue for a SubmissionFormField
        /// </summary>
        /// <param name="submissionFormFieldId">The unique identifier of the SubmissionFormField</param>
        /// <param name="submissionFormFieldValue">The value that will be used to update the SUbmissionFormFieldValue</param>
        void UpdateSubmissionFormFieldValue(int submissionFormFieldId, string submissionFormFieldValue);

        /// <summary>
        /// Get the FormField object with the provided formFieldId
        /// </summary>
        /// <param name="formFieldId">FormFieldId</param>
        /// <returns>FormField</returns>
        FormField GetFormField(int formFieldId);
    }


}
