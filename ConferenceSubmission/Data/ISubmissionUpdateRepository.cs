﻿using ConferenceSubmission.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Data
{
    public interface ISubmissionUpdateRepository
    {
        /// <summary>
        /// Get all SubmissionUpdate objects with the provided
        /// accepted category ID.
        /// </summary>
        /// <param name="acceptedCategoryId"></param>
        /// <returns>Collection of SubmissionUpdate objects</returns>
        IEnumerable<SubmissionUpdate> getSubmissionUpdates(int acceptedCategoryId);
    }
}
