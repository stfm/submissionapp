﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class FAQServiceTest
    {
        [Fact]
        public void GetFAQsTest()
        {
            //arrange
            Mock<IFAQRepository> faqRepository = new Mock<IFAQRepository>();
            faqRepository.Setup(f => f.GetFAQs()).Returns(new List<FAQ> {
                new FAQ {
                    FAQId = 1,
                    Question = "How many stars are in the universe?",
                    Answer = "Very, very, very many!",
                    SortOrder = 0,
                    Display = true,
                    ConferenceType = "1,2,3"
                },
                new FAQ
                {
                    FAQId = 2,
                    Question = "How rich is Bill Gates?",
                    Answer = "A lot richer than me!",
                    SortOrder = 1,
                    Display = true,
                    ConferenceType = "1,2,3"
                },
                new FAQ
                {
                    FAQId = 3,
                    Question = "Why is the sky blue?",
                    Answer = "A clear cloudless day-time sky is blue because molecules in the air scatter blue light from the sun more than they scatter red light.",
                    SortOrder = 2,
                    Display = true,
                    ConferenceType = "1,2,3"
                }
            });
            var faqService = new FAQService(faqRepository.Object);
            var result = new List<FAQ>();

            //act
            result = faqService.GetFAQs();

            //assert
            Assert.NotEmpty(result);
            Assert.Equal(3, result.Count);


        }
    }
}
