﻿using ConferenceSubmission.DTOs;
using ConferenceSubmission.DTOs.SalesforceEvent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class RegistrationReportsConferenceSelectorViewModel
    {
        public RegistrationReportsConferenceSelectorViewModel()
        {
            Conferences      = new List<EntityByNameAndId>();
            SalesforceEvents = new List<SFEvent>();
        }
        public List<EntityByNameAndId>  Conferences         { get; set; }
        public List<SFEvent>            SalesforceEvents    { get; set; }
    }
}
