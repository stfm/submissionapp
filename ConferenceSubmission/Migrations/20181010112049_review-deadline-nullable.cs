﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class reviewdeadlinenullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "ConferenceSubmissionReviewEndDate",
                table: "Conferences",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.CreateIndex(
                name: "IX_ReviewFormFields_ReviewId",
                table: "ReviewFormFields",
                column: "ReviewId");

            migrationBuilder.AddForeignKey(
                name: "FK_ReviewFormFields_Reviews_ReviewId",
                table: "ReviewFormFields",
                column: "ReviewId",
                principalTable: "Reviews",
                principalColumn: "ReviewId",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReviewFormFields_Reviews_ReviewId",
                table: "ReviewFormFields");

            migrationBuilder.DropIndex(
                name: "IX_ReviewFormFields_ReviewId",
                table: "ReviewFormFields");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ConferenceSubmissionReviewEndDate",
                table: "Conferences",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
