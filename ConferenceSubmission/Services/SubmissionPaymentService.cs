﻿using ConferenceSubmission.BindingModels;
using Serilog;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using AuthorizeNet.Api.Controllers;
using ConferenceSubmission.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Authentication;
using System.Web;
using System.Net;
using System.IO;

namespace ConferenceSubmission.Services
{
    public class SubmissionPaymentService : ISubmissionPaymentService
    {
        public SubmissionPaymentService()
        {
        }

        public SubmissionPaymentViewModel ProcessPayment(string ApiLoginID, string ApiTransactionKey,
                                                         Boolean Production,
                                                         CreditCardPaymentBindingModel creditCardPaymentBindingModel)
        {

            SubmissionPaymentViewModel submissionPaymentViewModel = new SubmissionPaymentViewModel();


            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = (Production) ? AuthorizeNet.Environment.PRODUCTION : AuthorizeNet.Environment.SANDBOX;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            var creditCard = new creditCardType
            {
                cardNumber = creditCardPaymentBindingModel.CreditCardNumber,
                expirationDate = creditCardPaymentBindingModel.CreditCardExpirationDate,
                cardCode = creditCardPaymentBindingModel.CreditCardCsvCode
            };

            var billingAddress = new customerAddressType
            {
                firstName = creditCardPaymentBindingModel.BillingFirstName,
                lastName = creditCardPaymentBindingModel.BillingLastName,
                address = creditCardPaymentBindingModel.BillingStreet,
                city = creditCardPaymentBindingModel.BillingCity,
                zip = creditCardPaymentBindingModel.BillingZip

            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };

            // Add line Items
            var lineItems = new lineItemType[2];
            lineItems[0] = new lineItemType
            {
                itemId = "1",
                name = "Payment for Submission " + creditCardPaymentBindingModel.SubmissionId,
                quantity = 1,
                unitPrice = creditCardPaymentBindingModel.PaymentAmount
            };

            var orderType = new orderType
            {
                invoiceNumber = "SubmissionPayment-" + creditCardPaymentBindingModel.SubmissionId,
                description = "Submission Payment"
            };



            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // charge the card

                amount = creditCardPaymentBindingModel.PaymentAmount,
                payment = paymentType,
                billTo = billingAddress,
                lineItems = lineItems

            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the controller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            // validate response
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.transactionResponse.messages != null)
                    {
                        Log.Information("Successfully created transaction with Transaction ID: " + response.transactionResponse.transId);
                        Log.Information("Response Code: " + response.transactionResponse.responseCode);
                        Log.Information("Message Code: " + response.transactionResponse.messages[0].code);
                        Log.Information("Description: " + response.transactionResponse.messages[0].description);
                        Log.Information("Success, Auth Code : " + response.transactionResponse.authCode);

                        submissionPaymentViewModel.PaymentResult = "Payment Successful";
                        submissionPaymentViewModel.SuccessfulPayment = true;
                        submissionPaymentViewModel.PaymentTransactionId = response.transactionResponse.transId;
                        submissionPaymentViewModel.SubmissionId = creditCardPaymentBindingModel.SubmissionId;
                    }
                    else
                    {
                        Log.Information("Failed Transaction.");
                        if (response.transactionResponse.errors != null)
                        {
                            Log.Information("Error Code: " + response.transactionResponse.errors[0].errorCode);
                            Log.Information("Error message: " + response.transactionResponse.errors[0].errorText);
                            submissionPaymentViewModel.PaymentResult = "Payment Failed - " +
                                response.transactionResponse.errors[0].errorText;
                            submissionPaymentViewModel.SuccessfulPayment = false;
                            submissionPaymentViewModel.SubmissionId = creditCardPaymentBindingModel.SubmissionId;
                        }
                    }
                }
                else
                {
                    Log.Information("Failed Transaction.");
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        Log.Information("Error Code: " + response.transactionResponse.errors[0].errorCode);
                        Log.Information("Error message: " + response.transactionResponse.errors[0].errorText);
                        submissionPaymentViewModel.PaymentResult = "Payment Failed - " +
                                response.transactionResponse.errors[0].errorText;
                        submissionPaymentViewModel.SuccessfulPayment = false;
                        submissionPaymentViewModel.SubmissionId = creditCardPaymentBindingModel.SubmissionId;
                    }
                    else
                    {
                        Log.Information("Error Code: " + response.messages.message[0].code);
                        Log.Information("Error message: " + response.messages.message[0].text);
                        submissionPaymentViewModel.PaymentResult = "Payment Failed - " +
                            response.messages.message[0].text;
                        submissionPaymentViewModel.SuccessfulPayment = false;
                        submissionPaymentViewModel.SubmissionId = creditCardPaymentBindingModel.SubmissionId;
                    }
                }
            }
            else
            {
                Log.Information("Null Response.");
                submissionPaymentViewModel.PaymentResult = "Payment Failed";
                submissionPaymentViewModel.SuccessfulPayment = false;
                submissionPaymentViewModel.SubmissionId = creditCardPaymentBindingModel.SubmissionId;
            }

            return submissionPaymentViewModel;


        }

        public SubmissionPaymentViewModel ProcessPaymentNew(string ApiLoginID, string ApiTransactionKey,
                                                         Boolean Production,
                                                         CreditCardPaymentBindingModel creditCardPaymentBindingModel)
        {
            SubmissionPaymentViewModel submissionPaymentViewModel = new SubmissionPaymentViewModel();

            // set the post URL - either test or prod
            string postUrl = Production ?
                "https://secure2.authorize.net/gateway/transact.dll" :
                "https://test.authorize.net/gateway/transact.dll";


            Dictionary<string, string> postValues = new Dictionary<string, string>();

            // set API credentials
            postValues.Add("x_login", ApiLoginID);
            postValues.Add("x_tran_key", ApiTransactionKey);

            // set config values
            postValues.Add("x_delim_data", "TRUE");
            postValues.Add("x_delim_char", "|");
            postValues.Add("x_relay_response", "FALSE");
            postValues.Add("x_type", "AUTH_CAPTURE");
            postValues.Add("x_method", "CC");

            // set credit card number
            postValues.Add("x_card_num", creditCardPaymentBindingModel.CreditCardNumber);

            // set card expiration date
            postValues.Add("x_exp_date", creditCardPaymentBindingModel.CreditCardExpirationDate);

            // set the card code verification (CCV) number
            postValues.Add("x_card_code", creditCardPaymentBindingModel.CreditCardCsvCode);

            postValues.Add("x_amount", creditCardPaymentBindingModel.PaymentAmount.ToString(CultureInfo.InvariantCulture));
            postValues.Add("x_description", "Conference Submission Payment");


            // set purchaser info
            postValues.Add("x_first_name", creditCardPaymentBindingModel.BillingFirstName);
            postValues.Add("x_last_name", creditCardPaymentBindingModel.BillingLastName);
            postValues.Add("x_address", creditCardPaymentBindingModel.BillingStreet);
            postValues.Add("x_city", creditCardPaymentBindingModel.BillingCity);
            postValues.Add("x_zip", creditCardPaymentBindingModel.BillingZip);

            // additional fields can be added here as outlined in the AIM integration guide at: http://developer.authorize.net

            // convert the input fields to the proper format for an http post
            // for example: "x_login=username&x_tran_key=a1B2c3D4"
            string post_string = "";
            foreach (KeyValuePair<string, string> post_value in postValues)
            {
                post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";
            }
            post_string = post_string.TrimEnd('&');

            //code to force using TLS 1.2
            const SslProtocols _Tls12 = (SslProtocols)0x00000C00;
            const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;
            ServicePointManager.SecurityProtocol = Tls12;

            // create an HttpWebRequest object to communicate with Authorize.net
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(postUrl);
            objRequest.Method = "POST";
            objRequest.ContentLength = post_string.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // send post data as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(post_string);
            myWriter.Close();

            // get returned values as a stream; read them into a string
            String postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();
                responseStream.Close();
            }

            // convert the post response to an AuthorizeNetResponse object
            AuthorizeNetResponse response = new AuthorizeNetResponse(postResponse);

            // validate response
            if (response != null)
            {
                if (response["ResponseCode"] == "1")
                {
                        Log.Information("Successfully created transaction with Transaction ID: " + response["TransactionId"]);
                        Log.Information("Response Code: " + response["ResponseCode"]);
                        Log.Information("Message Code: " + response["ResponseReasonCode"]);
                        Log.Information("Description: " + response["ResponseReasonText"]);
                        Log.Information("Success, Auth Code : " + response["AuthorizationCode"]);

                        submissionPaymentViewModel.PaymentResult = "Payment Successful";
                        submissionPaymentViewModel.SuccessfulPayment = true;
                        submissionPaymentViewModel.PaymentTransactionId = response["TransactionId"];
                        submissionPaymentViewModel.SubmissionId = creditCardPaymentBindingModel.SubmissionId;
 
                }
                else
                {
                    Log.Information("Failed Transaction.");
                   

                        Log.Information("Error message: " + response["ResponseReasonText"]);
                    submissionPaymentViewModel.PaymentResult = "Payment Failed - " +
                             response["ResponseReasonText"];
                        submissionPaymentViewModel.SuccessfulPayment = false;
                        submissionPaymentViewModel.SubmissionId = creditCardPaymentBindingModel.SubmissionId;
                  
                }
            }
            else
            {
                Log.Information("Null Response.");
                submissionPaymentViewModel.PaymentResult = "Payment Failed";
                submissionPaymentViewModel.SuccessfulPayment = false;
                submissionPaymentViewModel.SubmissionId = creditCardPaymentBindingModel.SubmissionId;
            }



            return submissionPaymentViewModel;


        }
    }

    class AuthorizeNetResponse : Dictionary<string, string>
    {
        public AuthorizeNetResponse(string postResponse)
        {
            string[] arrResponse = postResponse.Split('|');

            Add("ResponseCode", 0 < arrResponse.Length ? arrResponse[0].Trim() : "");
            Add("ResponseSubCode", 1 < arrResponse.Length ? arrResponse[1].Trim() : "");
            Add("ResponseReasonCode", 2 < arrResponse.Length ? arrResponse[2].Trim() : "");
            Add("ResponseReasonText", 3 < arrResponse.Length ? arrResponse[3].Trim() : "");
            Add("AuthorizationCode", 4 < arrResponse.Length ? arrResponse[4].Trim() : "");
            Add("AVSResponse", 5 < arrResponse.Length ? arrResponse[5].Trim() : "");
            Add("TransactionId", 6 < arrResponse.Length ? arrResponse[6].Trim() : "");
            Add("InvoiceNumber", 7 < arrResponse.Length ? arrResponse[7].Trim() : "");
            Add("Description", 8 < arrResponse.Length ? arrResponse[8].Trim() : "");
            Add("Amount", 9 < arrResponse.Length ? arrResponse[9].Trim() : "");
            Add("Method", 10 < arrResponse.Length ? arrResponse[10].Trim() : "");
            Add("TransactionType", 11 < arrResponse.Length ? arrResponse[11].Trim() : "");
            Add("CustomerId", 12 < arrResponse.Length ? arrResponse[12].Trim() : "");
            Add("FirstName", 13 < arrResponse.Length ? arrResponse[13].Trim() : "");
            Add("LastName", 14 < arrResponse.Length ? arrResponse[14].Trim() : "");
            Add("Company", 15 < arrResponse.Length ? arrResponse[15].Trim() : "");
            Add("Address", 16 < arrResponse.Length ? arrResponse[16].Trim() : "");
            Add("City", 17 < arrResponse.Length ? arrResponse[17].Trim() : "");
            Add("State", 18 < arrResponse.Length ? arrResponse[18].Trim() : "");
            Add("ZipCode", 19 < arrResponse.Length ? arrResponse[19].Trim() : "");
            Add("Country", 20 < arrResponse.Length ? arrResponse[20].Trim() : "");
            Add("Phone", 21 < arrResponse.Length ? arrResponse[21].Trim() : "");
            Add("Fax", 22 < arrResponse.Length ? arrResponse[22].Trim() : "");
            Add("EmailAddress", 23 < arrResponse.Length ? arrResponse[23].Trim() : "");
            Add("ShipToFirstName", 24 < arrResponse.Length ? arrResponse[24].Trim() : "");
            Add("ShipToLastName", 25 < arrResponse.Length ? arrResponse[25].Trim() : "");
            Add("ShipToCompany", 26 < arrResponse.Length ? arrResponse[26].Trim() : "");
            Add("ShipToAddress", 27 < arrResponse.Length ? arrResponse[27].Trim() : "");
            Add("ShipToCity", 28 < arrResponse.Length ? arrResponse[28].Trim() : "");
            Add("ShipToState", 29 < arrResponse.Length ? arrResponse[29].Trim() : "");
            Add("ShipToZipCode", 30 < arrResponse.Length ? arrResponse[30].Trim() : "");
            Add("ShipToCountry", 31 < arrResponse.Length ? arrResponse[31].Trim() : "");
            Add("Tax", 32 < arrResponse.Length ? arrResponse[32].Trim() : "");
            Add("Duty", 33 < arrResponse.Length ? arrResponse[33].Trim() : "");
            Add("Freight", 34 < arrResponse.Length ? arrResponse[34].Trim() : "");
            Add("TaxExempt", 35 < arrResponse.Length ? arrResponse[35].Trim() : "");
            Add("PurchaseOrderNumber", 36 < arrResponse.Length ? arrResponse[36].Trim() : "");
            Add("MD5Hash", 37 < arrResponse.Length ? arrResponse[37].Trim() : "");
            Add("CardResponseCode", 38 < arrResponse.Length ? arrResponse[38].Trim() : "");
            Add("CardholderVerificationResponse", 39 < arrResponse.Length ? arrResponse[39].Trim() : "");
            //Add("Field41",                        40 < arrResponse.Length ? arrResponse[40].Trim() : "");
            //Add("Field42",                        41 < arrResponse.Length ? arrResponse[41].Trim() : "");
            //Add("Field43",                        42 < arrResponse.Length ? arrResponse[42].Trim() : "");
            //Add("Field44",                        43 < arrResponse.Length ? arrResponse[43].Trim() : "");
            //Add("Field45",                        44 < arrResponse.Length ? arrResponse[44].Trim() : "");
            //Add("Field46",                        45 < arrResponse.Length ? arrResponse[45].Trim() : "");
            //Add("Field47",                        46 < arrResponse.Length ? arrResponse[46].Trim() : "");
            //Add("Field48",                        47 < arrResponse.Length ? arrResponse[47].Trim() : "");
            //Add("Field49",                        48 < arrResponse.Length ? arrResponse[48].Trim() : "");
            //Add("Field50",                        49 < arrResponse.Length ? arrResponse[49].Trim() : "");
            Add("AccountNumber", 50 < arrResponse.Length ? arrResponse[50].Trim() : "");
            Add("CardType", 51 < arrResponse.Length ? arrResponse[51].Trim() : "");
            Add("SplitTenderId", 52 < arrResponse.Length ? arrResponse[52].Trim() : "");
            Add("RequestedAmount", 53 < arrResponse.Length ? arrResponse[53].Trim() : "");
            Add("BalanceOnCard", 54 < arrResponse.Length ? arrResponse[54].Trim() : "");
            //Add("Field56",                        55 < arrResponse.Length ? arrResponse[55].Trim() : "");
            //Add("Field57",                        56 < arrResponse.Length ? arrResponse[56].Trim() : "");
            //Add("Field58",                        57 < arrResponse.Length ? arrResponse[57].Trim() : "");
            //Add("Field59",                        58 < arrResponse.Length ? arrResponse[58].Trim() : "");
            //Add("Field60",                        59 < arrResponse.Length ? arrResponse[59].Trim() : "");
            //Add("Field61",                        60 < arrResponse.Length ? arrResponse[60].Trim() : "");
            //Add("Field62",                        61 < arrResponse.Length ? arrResponse[61].Trim() : "");
            //Add("Field63",                        62 < arrResponse.Length ? arrResponse[62].Trim() : "");
            //Add("Field64",                        63 < arrResponse.Length ? arrResponse[63].Trim() : "");
            //Add("Field65",                        64 < arrResponse.Length ? arrResponse[64].Trim() : "");
            //Add("Field66",                        65 < arrResponse.Length ? arrResponse[65].Trim() : "");
            //Add("Field67",                        66 < arrResponse.Length ? arrResponse[66].Trim() : "");
            //Add("Field68",                        67 < arrResponse.Length ? arrResponse[67].Trim() : "");
            //Add("Field69",                        68 < arrResponse.Length ? arrResponse[68].Trim() : "");

        }
    }
}
