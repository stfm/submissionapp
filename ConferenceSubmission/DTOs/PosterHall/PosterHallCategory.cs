﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs.PosterHall
{
    public class PosterHallCategory
    {
        public PosterHallCategory()
        {
            Submissions = new List<PosterWithPresenters>();
        }

        public List<PosterWithPresenters> Submissions { get; }
    }


}
