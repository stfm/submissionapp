﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class PresentationMethod
    {
        public PresentationMethod()
        {
        }

        public int PresentationMethodId { get; set; }

        [StringLength(250)]
        [Required]
        public PresentationMethodType PresentationMethodName { get; set; }

    }


}
