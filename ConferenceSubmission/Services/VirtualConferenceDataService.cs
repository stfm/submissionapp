﻿using ConferenceSubmission.Models;
using ConferenceSubmission.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public class VirtualConferenceDataService : IVirtualConferenenceDataService
    {

        private readonly IVirtualConferenceDataRepository _virtualConferenceDataRepository;


        public VirtualConferenceDataService(IVirtualConferenceDataRepository virtualConferenceDataRepository)
        {
            _virtualConferenceDataRepository = virtualConferenceDataRepository;
        }

        public VirtualConferenceData GetVirtualConferenceData(int conferenceId)
        {
            return _virtualConferenceDataRepository.GetVirtualConferenceData(conferenceId);
        }
    }
}
