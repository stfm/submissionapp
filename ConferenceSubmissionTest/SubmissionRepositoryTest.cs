﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class SubmissionRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public SubmissionRepositoryTest(Xunit.Abstractions.ITestOutputHelper output) {

            Output = output;

        }


        [Fact]
        public void AddSubmissionTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            int submissionId = 0;

            using (var context = new AppDbContext(options))
            {

                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                Conference aConference = conferenceRepository.GetConference(100);

                Submission submission = new Submission
                {

                    AcceptedCategoryId = 1002,
                    Conference = aConference,
                    SubmissionAbstract = "Test Poster Abstract",
                    SubmissionCategoryId = 1002,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 13, 2018"),
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 13, 2018"),
                    SubmissionStatusId = 1,
                    SubmissionTitle = "Test Poster Title",

                };

                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                submissionId = submissionRepository.AddSubmission(submission);

                Output.WriteLine("Submission ID is " + submissionId);

                Assert.True(submissionId > 0);

            }

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Submission submission = submissionRepository.GetSubmissionAllDetailsBySubmissionId(submissionId);

                Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);

                Output.WriteLine("Submission category is " + submission.SubmissionCategory.SubmissionCategoryName);

                Assert.Equal("Test Poster Title", submission.SubmissionTitle);

            }

        }

        [Fact]
        public void GetSubmissionBySubmissionIdTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Submission submission = submissionRepository.GetSubmissionBySubmissionId(10001);

                Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);

                Output.WriteLine("Submission category is " + submission.SubmissionCategory.SubmissionCategoryName);

                Assert.Contains(submission.ParticipantLink, p => p.Participant.StfmUserId == "3001");

                Assert.Contains(submission.ParticipantLink, p => p.Participant.StfmUserId == "3002");

                Assert.Contains(submission.ParticipantLink.FirstOrDefault(p => p.Participant.StfmUserId == "3001")
                    .SubmissionParticipantToParticipantRolesLink, (p => p.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER));

                Assert.Contains(submission.ParticipantLink.FirstOrDefault(p => p.Participant.StfmUserId == "3001")
                    .SubmissionParticipantToParticipantRolesLink, (p => p.ParticipantRole.ParticipantRoleName == ParticipantRoleType.SUBMITTER));

                Assert.Contains(submission.ParticipantLink.FirstOrDefault(p => p.Participant.StfmUserId == "3002")
                    .SubmissionParticipantToParticipantRolesLink, (p => p.ParticipantRole.ParticipantRoleName == ParticipantRoleType.PRESENTER));

                Assert.Equal("Test Title", submission.SubmissionTitle);

                Assert.Equal("Lecture MSE19", submission.SubmissionCategory.SubmissionCategoryName);


            }
        }

        [Fact]
        public void GetSubmissionCompleted()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                IEnumerable<Submission> submissionsComplete = submissionRepository.GetSubmissions(true, 100);

                foreach (Submission submission in submissionsComplete)
                {

                    Output.WriteLine("Submission Id is " + submission.SubmissionId);
                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);
                    Output.WriteLine("Submission complete is " + submission.SubmissionCompleted);
                    Output.WriteLine("Submission conference is " + submission.Conference.ConferenceLongName);


                }

                Assert.True(1 == submissionsComplete.Count());


            }
        }

        [Fact]
        public void GetSubmissionsByConference()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                IEnumerable<Submission> submissions = submissionRepository.GetSubmissionsForConference(100);

                Output.WriteLine("Number of submissions found for conference id 100 is " + submissions.Count());

                foreach (Submission submission in submissions)
                {

                    Output.WriteLine("Submission Id is " + submission.SubmissionId);
                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);
                    Output.WriteLine("Submission complete is " + submission.SubmissionCompleted);
                    Output.WriteLine("Submission conference is " + submission.Conference.ConferenceLongName);
                    Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);
                    Output.WriteLine("Submission accepted category is " + submission.AcceptedCategory.SubmissionCategoryName);



                }

                Assert.True(4 == submissions.Count());


            }
        }


        [Fact]
        public void GetAcceptedSubmissionsByConference()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                IEnumerable<Submission> submissions = submissionRepository.GetAcceptedSubmissionsForConference(100);

                Output.WriteLine("Number of submissions found for conference id 100 is " + submissions.Count());

                foreach (Submission submission in submissions)
                {

                    Output.WriteLine("Submission Id is " + submission.SubmissionId);
                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);
                    Output.WriteLine("Submission complete is " + submission.SubmissionCompleted);
                    Output.WriteLine("Submission conference is " + submission.Conference.ConferenceLongName);
                    Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);
                    Output.WriteLine("Submission accepted category is " + submission.AcceptedCategory.SubmissionCategoryName);



                }

                Assert.True(2 == submissions.Count());


            }
        }

        [Fact]
        public void GetAcceptedSubmissionsByConference_VerifySortOrder()
        {
            //arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptionsForTestingSortOrder();
            List<Submission> results;
            var conferenceId = 101;

            //act
            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);
                results = submissionRepository.GetAcceptedSubmissionsForConference(conferenceId).ToList();
            }

            //assert
            Assert.Equal("P01", results[0].ConferenceSession.SessionCode);
            Assert.Equal("P02", results[1].ConferenceSession.SessionCode);
            Assert.Equal("P03", results[2].ConferenceSession.SessionCode);
            Assert.Equal("L01", results[3].ConferenceSession.SessionCode);
            Assert.Equal("L02", results[4].ConferenceSession.SessionCode);
            Assert.Equal("L03", results[5].ConferenceSession.SessionCode);
        }

        [Fact]
        public void GetAcceptedSubmissionsByAcceptedCategory()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                IEnumerable<Submission> submissions = submissionRepository.GetAcceptedSubmissionsForCategory(1001);

                Output.WriteLine("Number of submissions found for accepted category id 1001 is " + submissions.Count());

                foreach (Submission submission in submissions)
                {

                    Output.WriteLine("Submission Id is " + submission.SubmissionId);
                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);
                    Output.WriteLine("Submission accepted category is " + submission.AcceptedCategory.SubmissionCategoryName);



                }

                Assert.True(2 == submissions.Count());


            }
        }

        [Fact]
        public void GetSubmissionsBySubmissionIds()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                IEnumerable<int> submissionIds = new [] { 10003, 10004 };

                IEnumerable<Submission> submissions = submissionRepository.GetSubmissions(submissionIds);

                Output.WriteLine("Number of submissions found  " + submissions.Count() + " for submission ids: " );

                foreach (var submissionId in submissionIds)
                {
                    Output.WriteLine("submission ID: " + submissionId);

                }
                foreach (Submission submission in submissions)
                {

                    Output.WriteLine("Submission Id is " + submission.SubmissionId);
                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);
                    Output.WriteLine("Submission complete is " + submission.SubmissionCompleted);
                    Output.WriteLine("Submission conference is " + submission.Conference.ConferenceLongName);
                    Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);
                    Output.WriteLine("Submission accepted category is " + submission.AcceptedCategory.SubmissionCategoryName);



                }

                Assert.True(2 == submissions.Count());


            }
        }


        [Fact]
        public void GetSubmissionAllDetailsBySubmissionIdTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Submission submission = submissionRepository.GetSubmissionAllDetailsBySubmissionId(10001);

                Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);

                Output.WriteLine("Submission completed is " + submission.SubmissionCompleted);

                Output.WriteLine("Submission category is " + submission.SubmissionCategory.SubmissionCategoryName);

                Output.WriteLine("Submision Accepted Category is " + submission.AcceptedCategory.SubmissionCategoryName);

                Output.WriteLine("Conference for submission is " + submission.Conference.ConferenceLongName);

                Output.WriteLine("Submission conference session code is " + submission.ConferenceSession.SessionCode);

                Output.WriteLine("Submission payment amount is " + submission.SubmissionPayment.PaymentAmount);

                foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                {


                    Output.WriteLine("SubmissionParticipantId is " + submissionParticipant.SubmissionParticipantId);

                    Output.WriteLine("Participant ID is " + submissionParticipant.ParticipantId);

                    Output.WriteLine("Submission participant's StfmUserId is " + submissionParticipant.Participant.StfmUserId);

                    foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipant.SubmissionParticipantToParticipantRolesLink)
                    {

                        Output.WriteLine("Submission participant role name is " + submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName);

                    }


                    Assert.Equal("Test Title", submission.SubmissionTitle);

                    Assert.Equal("Lecture MSE19", submission.SubmissionCategory.SubmissionCategoryName);

                    Assert.Equal("S01", submission.ConferenceSession.SessionCode);

                    Assert.Equal(25, submission.SubmissionPayment.PaymentAmount);



                }
            }
        }


        [Fact]
        public void UpdateSubmissionTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Submission submission = submissionRepository.GetSubmissionBySubmissionId(10001);

                Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                submission.SubmissionTitle = "Test Title Updated";

                Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);

                submission.SubmissionStatusId = 2;

                submissionRepository.UpdateSubmission(submission);

            }

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Submission submission = submissionRepository.GetSubmissionBySubmissionId(10001);

                Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                Output.WriteLine("Submission Last Updated at " + submission.SubmissionLastUpdatedDateTime);

                Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);

                Assert.Equal("Test Title Updated", submission.SubmissionTitle);

            }
        }


        [Fact]
        public void UpdateSubmissionCompletedTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Submission submission = submissionRepository.GetSubmissionBySubmissionId(10001);

                Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                Output.WriteLine("Submission completed: " + submission.SubmissionCompleted);

                submissionRepository.UpdateSubmissionCompleted(10001, true);
            }

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Submission submission = submissionRepository.GetSubmissionBySubmissionId(10001);

                Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                Output.WriteLine("Submission Last Updated at " + submission.SubmissionLastUpdatedDateTime);

                Output.WriteLine("Submission completed: " + submission.SubmissionCompleted);

                Assert.True(submission.SubmissionCompleted);

            }
        }


        [Fact]
        public void AddSubmissionPaymentTest() {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                SubmissionPayment submissionPayment = new SubmissionPayment { PaymentAmount = 10, PaymentDateTime = DateTime.Parse(" Feb 12, 2018"), SubmissionId = 10002, PaymentTransactionId = "P10999" };

                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                int submissionPaymentId = submissionRepository.AddSubmissionPaymentToSubmission(submissionPayment);

                Output.WriteLine("Submission payment ID is " + submissionPaymentId);

                Assert.True(submissionPaymentId > 0);

            }

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Submission submission = submissionRepository.GetSubmissionAllDetailsBySubmissionId(10002);

                Output.WriteLine("Submission payment amount is " + submission.SubmissionPayment.PaymentAmount);

                Assert.Equal(10, submission.SubmissionPayment.PaymentAmount);

            }



        }

        [Fact]
        public void GetSubmissionsTest() {


            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                IEnumerable<Submission> submissions = submissionRepository.GetSubmissions("3002");

                foreach(Submission submission in submissions) {

                    Output.WriteLine("Submission Id is " + submission.SubmissionId);
                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);
                    Output.WriteLine("Submission status is " + submission.SubmissionStatus.SubmissionStatusName);
                    Output.WriteLine("Submission conference is " + submission.Conference.ConferenceLongName);


                }

                Assert.Equal(2, submissions.Count());

            }


        }

        [Fact]
        public void GetSubmissionsByCategory()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                Dictionary<string, int> submissionsByCategoryMap = submissionRepository.GetSubmissionsByCategory(100, true);

                foreach( var entry in submissionsByCategoryMap)
                {

                    Output.WriteLine("Category: " + entry.Key + " - " + entry.Value + " submissions");

                }


            }
        }

        [Fact]
        public void GetSubmissionsForPosterHallCategory()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptionsForTestingSortOrder();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                //Act
                var results = submissionRepository.GetSubmissionsForPosterHallCategory(1005);

                //Assert
                Assert.Equal(12, results.Count);
                Assert.Equal(12, results.Where(x => x.SubmissionId == 10004).Count());

                Assert.Equal(6, results.Where(x => x.STFMUserId == "3001").Count());
                Assert.Equal(3, results.Where(x => x.STFMUserId == "3004").Count());
                Assert.Equal(3, results.Where(x => x.STFMUserId == "3006").Count());

                Assert.Equal(4, results.Where(x => x.MaterialType == "poster").Count());
                Assert.Equal(4, results.Where(x => x.MaterialType == "link").Count());
                Assert.Equal(4, results.Where(x => x.MaterialType == "handout").Count());

                //Confirming Sort Order or Participants
                Assert.Equal("3001,3006,3004", String.Join(',', results.Where(x => x.SubmissionId == 10004).Select(x => x.STFMUserId).Distinct().ToList()));               
            }
        }

        [Fact]
        public void GetSubmissionsForConferenceRegistrationReport()
        {
            //Arrange
            var conferenceId = 101;

            DbContextOptions<AppDbContext> options = GetDbContextOptionsForTestingSortOrder();

            using (var context = new AppDbContext(options))
            {
                SubmissionRepository submissionRepository = new SubmissionRepository(context);

                //Act
                var results = submissionRepository.GetSubmissionsForConferenceRegistrationReport(conferenceId);

                //Assert
                Assert.NotEmpty(results);
                Assert.Equal(6, results.Count);

                Assert.DoesNotContain(results, x => x.SubmissionId == 10007);
                Assert.DoesNotContain(results, x => x.SubmissionId == 10008);

                Assert.Single(results.FirstOrDefault(x => x.SubmissionId == 10001).PresentersForRegistrationReport);
                Assert.Single(results.FirstOrDefault(x => x.SubmissionId == 10002).PresentersForRegistrationReport);
                Assert.Single(results.FirstOrDefault(x => x.SubmissionId == 10003).PresentersForRegistrationReport);
                Assert.Single(results.FirstOrDefault(x => x.SubmissionId == 10005).PresentersForRegistrationReport);
                Assert.Equal(3, results.FirstOrDefault(x => x.SubmissionId == 10004).PresentersForRegistrationReport.Count);
                Assert.Equal(3, results.FirstOrDefault(x => x.SubmissionId == 10006).PresentersForRegistrationReport.Count);

                Assert.Single(results.FirstOrDefault(x => x.SubmissionId == 10004).PresentersForRegistrationReport.FirstOrDefault(x => x.STFMUserId == "3004").Roles);
                Assert.Equal(ParticipantRoleType.LEAD_PRESENTER, results.FirstOrDefault(x => x.SubmissionId == 10004).PresentersForRegistrationReport.FirstOrDefault(x => x.STFMUserId == "3004").Roles.First());

                Assert.Single(results.FirstOrDefault(x => x.SubmissionId == 10004).PresentersForRegistrationReport.FirstOrDefault(x => x.STFMUserId == "3006").Roles);
                Assert.Equal(ParticipantRoleType.LEAD_PRESENTER, results.FirstOrDefault(x => x.SubmissionId == 10004).PresentersForRegistrationReport.FirstOrDefault(x => x.STFMUserId == "3006").Roles.First());

                Assert.Equal(2, results.FirstOrDefault(x => x.SubmissionId == 10004).PresentersForRegistrationReport.FirstOrDefault(x => x.STFMUserId == "3001").Roles.Count);
                Assert.Contains(results.FirstOrDefault(x => x.SubmissionId == 10004).PresentersForRegistrationReport.FirstOrDefault(x => x.STFMUserId == "3001").Roles, x => x == ParticipantRoleType.LEAD_PRESENTER);
                Assert.Contains(results.FirstOrDefault(x => x.SubmissionId == 10004).PresentersForRegistrationReport.FirstOrDefault(x => x.STFMUserId == "3001").Roles, x => x == ParticipantRoleType.SUBMITTER);
            }

        }


        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 1, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW },
                    new SubmissionStatus { SubmissionStatusId = 3, SubmissionStatusName = SubmissionStatusType.CANCELED }
               );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {
                        
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 3,
                        SubmissionTitle = "Test Title",
                        SubmissionCompleted = false

                    },

                new Submission {

                        AcceptedCategoryId = 1000,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract 2",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 12, 2018"),
                        SubmissionId = 10002,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 12, 2018"),
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Test Title 2",
                    SubmissionCompleted = false

                },

                new Submission
                {

                    AcceptedCategoryId = 1000,
                    Conference = aConference,
                    SubmissionAbstract = "Test Abstract 3",
                    SubmissionCategoryId = 1000,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10003,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 1,
                    SubmissionTitle = "Test Title 3",
                    SubmissionCompleted = true

                },

                new Submission
                {

                    AcceptedCategoryId = 1001,
                    Conference = aConference,
                    SubmissionAbstract = "Test Abstract 4",
                    SubmissionCategoryId = 1001,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10004,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Title 4",
                    SubmissionCompleted = false

                }

                );

                context.SaveChanges();

                ConferenceSession aConferenceSession = new ConferenceSession { SessionCode = "S01", SessionLocation = "Green Room", SessionStartDateTime = DateTime.Parse(" Apr 10, 2018") };

                SubmissionPayment submissionPayment = new SubmissionPayment { PaymentAmount = 25, PaymentDateTime = DateTime.Parse(" Apr 10, 2018"), SubmissionId = 10001, PaymentTransactionId = "P999" };


                Submission aSubmission = context.Submissions.Find(10001);

                aSubmission.ConferenceSession = aConferenceSession;

                aSubmission.SubmissionPayment = submissionPayment;

                context.Update(aSubmission);

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }


                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(

                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 3, ParticipantId = 4002, SubmissionId = 10002 },
                    new SubmissionParticipant { SubmissionParticipantId = 4, ParticipantId = 4003, SubmissionId = 10003 }
                );


                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 2 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 3 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 4 }


                );

                context.SaveChanges();

            }

            return options;
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptionsForTestingSortOrder()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "MSE" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    }, new Conference
                    {
                        ConferenceId = 201,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("February 1, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Conference on Medical Student Education",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("February 4, 2019"),
                        ConferenceTypeId = 2
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" },
                    new SubmissionCategory { ConferenceId = 201, SubmissionCategoryId = 1006, SubmissionCategoryName = "Poster MSE19" }

                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 1, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW },
                    new SubmissionStatus { SubmissionStatusId = 3, SubmissionStatusName = SubmissionStatusType.ACCEPTED }
               );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(101);
                Conference mseConference = context.Conferences.Find(201);

                context.Submissions.AddRange(

                    new Submission
                    {

                        AcceptedCategoryId = 1003,
                        Conference = aConference,
                        SubmissionAbstract = "Test Lecture 01",
                        SubmissionCategoryId = 1003,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 3,
                        SubmissionTitle = "Test Lecture Title 01",
                        SubmissionCompleted = true

                    },

                new Submission
                {

                    AcceptedCategoryId = 1003,
                    Conference = aConference,
                    SubmissionAbstract = "Test Lecture 02",
                    SubmissionCategoryId = 1003,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 12, 2018"),
                    SubmissionId = 10002,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 12, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Lecture Title 02",
                    SubmissionCompleted = true

                },

                new Submission
                {

                    AcceptedCategoryId = 1003,
                    Conference = aConference,
                    SubmissionAbstract = "Test Lecture 03",
                    SubmissionCategoryId = 1003,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10003,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Lecture Title 03",
                    SubmissionCompleted = true

                },

                new Submission
                {

                    AcceptedCategoryId = 1005,
                    Conference = aConference,
                    SubmissionAbstract = "Test Poster 01",
                    SubmissionCategoryId = 1005,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10004,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Poster Title 01",
                    SubmissionCompleted = false

                },
                new Submission
                {

                    AcceptedCategoryId = 1005,
                    Conference = aConference,
                    SubmissionAbstract = "Test Poster 02",
                    SubmissionCategoryId = 1005,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10005,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Poster Title 02",
                    SubmissionCompleted = false

                },
                new Submission
                {

                    AcceptedCategoryId = 1005,
                    Conference = aConference,
                    SubmissionAbstract = "Test Poster 03",
                    SubmissionCategoryId = 1005,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10006,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Poster Title 03",
                    SubmissionCompleted = false

                },
                new Submission
                {

                    AcceptedCategoryId = 1005,
                    Conference = aConference,
                    SubmissionAbstract = "Test Poster 04",
                    SubmissionCategoryId = 1005,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10007,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 1,
                    SubmissionTitle = "Test Poster Title 04",
                    SubmissionCompleted = true
                },
                new Submission
                {

                    AcceptedCategoryId = 1006,
                    Conference = mseConference,
                    SubmissionAbstract = "Test Poster MSE",
                    SubmissionCategoryId = 1006,
                    SubmissionCreatedDateTime = DateTime.Parse("July 14, 2018"),
                    SubmissionId = 10008,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("July 31, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Poster MSE",
                    SubmissionCompleted = true
                },
                new Submission
                {

                    AcceptedCategoryId = 1005,
                    Conference = mseConference,
                    SubmissionAbstract = "Test Poster 07",
                    SubmissionCategoryId = 1005,
                    SubmissionCreatedDateTime = DateTime.Parse("July 14, 2018"),
                    SubmissionId = 10009,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("July 31, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Poster 07",
                    SubmissionCompleted = true
                },
                new Submission
                {

                    AcceptedCategoryId = 1005,
                    Conference = mseConference,
                    SubmissionAbstract = "Test Poster 09",
                    SubmissionCategoryId = 1005,
                    SubmissionCreatedDateTime = DateTime.Parse("July 14, 2018"),
                    SubmissionId = 10010,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("July 31, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Poster 09",
                    SubmissionCompleted = true
                },
                new Submission
                {

                    AcceptedCategoryId = 1005,
                    Conference = mseConference,
                    SubmissionAbstract = "Test Poster 09",
                    SubmissionCategoryId = 1005,
                    SubmissionCreatedDateTime = DateTime.Parse("July 14, 2018"),
                    SubmissionId = 10011,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("July 31, 2018"),
                    SubmissionStatusId = 3,
                    SubmissionTitle = "Test Poster 09",
                    SubmissionCompleted = true
                }
                );

                context.SaveChanges();

                context.VirtualMaterials.AddRange(new List<VirtualMaterial> {
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 1,
                        SubmissionId = 10004,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "poster",
                        MaterialValue = "posterFor10004.jpg",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 2,
                        SubmissionId = 10004,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 13,
                        SubmissionId = 10004,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "handout",
                        MaterialValue = "handout.pdf",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 3,
                        SubmissionId = 10005,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "poster",
                        MaterialValue = "posterFor10005.jpg",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 4,
                        SubmissionId = 10005,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 5,
                        SubmissionId = 10006,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "poster",
                        MaterialValue = "posterFor10006.jpg",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 6,
                        SubmissionId = 10006,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 7,
                        SubmissionId = 10009,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "poster",
                        MaterialValue = "posterFor10006.jpg",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 8,
                        SubmissionId = 10009,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 9,
                        SubmissionId = 10010,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "poster",
                        MaterialValue = "posterFor10006.jpg",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 10,
                        SubmissionId = 10010,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 11,
                        SubmissionId = 10011,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "poster",
                        MaterialValue = "posterFor10006.jpg",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    },
                    new VirtualMaterial
                    {
                        VirtualMaterialId = 12,
                        SubmissionId = 10011,
                        UploadedDateTime = DateTime.Now,
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc",
                        PersonUploaded = "0051N000007Y2R9QAK"
                    }
                });

                context.SaveChanges();

                //adding session for Submission 10001
                ConferenceSession conferenceSessionForSubmission10001 = new ConferenceSession { SessionCode = "L03", SessionLocation = "Green Room", SessionStartDateTime = DateTime.Parse("5-12-2019 12:30"), SessionEndDateTime = DateTime.Parse("5-12-2019 13:30") };

                Submission aSubmission10001 = context.Submissions.Find(10001);

                aSubmission10001.ConferenceSession = conferenceSessionForSubmission10001;

                context.Update(aSubmission10001);

                //adding session for Submission 10002
                ConferenceSession conferenceSessionForSubmission10002 = new ConferenceSession { SessionCode = "L01", SessionLocation = "Purple Room", SessionStartDateTime = DateTime.Parse("5-12-2019 12:30"), SessionEndDateTime = DateTime.Parse("5-12-2019 13:30") };

                Submission aSubmission10002 = context.Submissions.Find(10002);

                aSubmission10002.ConferenceSession = conferenceSessionForSubmission10002;

                context.Update(aSubmission10002);

                //adding session for Submission 10003
                ConferenceSession conferenceSessionForSubmission10003 = new ConferenceSession { SessionCode = "L02", SessionLocation = "Pink Room", SessionStartDateTime = DateTime.Parse("5-12-2019 12:30"), SessionEndDateTime = DateTime.Parse("5-12-2019 13:30") };

                Submission aSubmission10003 = context.Submissions.Find(10003);

                aSubmission10003.ConferenceSession = conferenceSessionForSubmission10003;

                context.Update(aSubmission10003);

                //adding session for Submission 10004
                ConferenceSession conferenceSessionForSubmission10004 = new ConferenceSession { SessionCode = "P02", SessionLocation = "Red Room", SessionStartDateTime = DateTime.Parse("5-11-2019 13:00"), SessionEndDateTime = DateTime.Parse("5-11-2019 13:45") };

                Submission aSubmission10004 = context.Submissions.Find(10004);

                aSubmission10004.ConferenceSession = conferenceSessionForSubmission10004;

                context.Update(aSubmission10004);

                //adding session for Submission 10005
                ConferenceSession conferenceSessionForSubmission10005 = new ConferenceSession { SessionCode = "P03", SessionLocation = "White Room", SessionStartDateTime = DateTime.Parse("5-11-2019 13:00"), SessionEndDateTime = DateTime.Parse("5-11-2019 13:45") };

                Submission aSubmission10005 = context.Submissions.Find(10005);

                aSubmission10005.ConferenceSession = conferenceSessionForSubmission10005;

                context.Update(aSubmission10005);

                //adding session for Submission 10006
                ConferenceSession conferenceSessionForSubmission10006 = new ConferenceSession { SessionCode = "P01", SessionLocation = "White Room", SessionStartDateTime = DateTime.Parse("5-11-2019 13:00"), SessionEndDateTime = DateTime.Parse("5-11-2019 13:45") };

                Submission aSubmission10006 = context.Submissions.Find(10006);

                aSubmission10006.ConferenceSession = conferenceSessionForSubmission10006;

                context.Update(aSubmission10006);

                context.SaveChanges();

                //adding session for Submission 10008
                ConferenceSession conferenceSessionForSubmission10008 = new ConferenceSession { SessionCode = "P31", SessionLocation = "White Room", SessionStartDateTime = DateTime.Parse("2-2-2019 13:00"), SessionEndDateTime = DateTime.Parse("2-2-2019 13:45") };

                Submission mseSubmission10008 = context.Submissions.Find(10008);

                mseSubmission10008.ConferenceSession = conferenceSessionForSubmission10008;

                context.Update(mseSubmission10008);

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }


                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" },
                    new Participant { ParticipantId = 4004, StfmUserId = "3004" },
                    new Participant { ParticipantId = 4005, StfmUserId = "3005" },
                    new Participant { ParticipantId = 4006, StfmUserId = "3006" }
                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(
                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10002 },
                    new SubmissionParticipant { SubmissionParticipantId = 3, ParticipantId = 4003, SubmissionId = 10003 },
                    new SubmissionParticipant { SubmissionParticipantId = 4, ParticipantId = 4001, SubmissionId = 10004, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 5, ParticipantId = 4002, SubmissionId = 10005 },
                    new SubmissionParticipant { SubmissionParticipantId = 6, ParticipantId = 4003, SubmissionId = 10006, SortOrder = 2 },
                    new SubmissionParticipant { SubmissionParticipantId = 7, ParticipantId = 4003, SubmissionId = 10007 },
                    new SubmissionParticipant { SubmissionParticipantId = 8, ParticipantId = 4004, SubmissionId = 10004, SortOrder = 2 },
                    new SubmissionParticipant { SubmissionParticipantId = 9, ParticipantId = 4006, SubmissionId = 10004, SortOrder = 1 },
                    new SubmissionParticipant { SubmissionParticipantId = 10, ParticipantId = 4002, SubmissionId = 10006, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 11, ParticipantId = 4005, SubmissionId = 10006, SortOrder = 1 },
                    new SubmissionParticipant { SubmissionParticipantId = 12, ParticipantId = 4005, SubmissionId = 10008, SortOrder = 1 }
                );


                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 2 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 3 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 4 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 4 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 5 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 6 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 7 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 8 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 9 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 10 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 11 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 12 }
                );

                context.SaveChanges();

                context.PresentationMethod.AddRange(
                    new PresentationMethod { PresentationMethodId = 1, PresentationMethodName = PresentationMethodType.LIVE_IN_PERSON },
                    new PresentationMethod { PresentationMethodId = 2, PresentationMethodName = PresentationMethodType.LIVE_ONLINE },
                    new PresentationMethod { PresentationMethodId = 3, PresentationMethodName = PresentationMethodType.RECORDED_ONLINE }
                );

                context.SaveChanges();

                context.PresentationStatus.AddRange(
                    new PresentationStatus { PresentationStatusId = 1, PresentationStatusName = PresentationStatusType.NO_ACTION },
                    new PresentationStatus { PresentationStatusId = 2, PresentationStatusName = PresentationStatusType.PENDING },
                    new PresentationStatus { PresentationStatusId = 3, PresentationStatusName = PresentationStatusType.NEED_PRESENTATION_MATERIAL },
                    new PresentationStatus { PresentationStatusId = 4, PresentationStatusName = PresentationStatusType.DECLINED }
                );

                context.SaveChanges();

                context.SubmissionPresentationMethod.AddRange(
                    new SubmissionPresentationMethod
                    {
                        SubmissionId = 10004,
                        PresentationMethodId = 3,
                        PresentationStatusId = 1,
                        SubmissionPresentationMethodId = 1
                    },
                    new SubmissionPresentationMethod
                    {
                        SubmissionId = 10005,
                        PresentationMethodId = 3,
                        PresentationStatusId = 2,
                        SubmissionPresentationMethodId = 2
                    },
                    new SubmissionPresentationMethod
                    {
                        SubmissionId = 10006,
                        PresentationMethodId = 3,
                        PresentationStatusId = 3,
                        SubmissionPresentationMethodId = 3
                    },
                    new SubmissionPresentationMethod
                    {
                        SubmissionId = 10009,
                        PresentationMethodId = 3,
                        PresentationStatusId = 4,
                        SubmissionPresentationMethodId = 4
                    },
                    new SubmissionPresentationMethod
                    {
                        SubmissionId = 10010,
                        PresentationMethodId = 2,
                        PresentationStatusId = 1,
                        SubmissionPresentationMethodId = 5
                    },
                    new SubmissionPresentationMethod
                    {
                        SubmissionId = 10011,
                        PresentationMethodId = 2,
                        PresentationStatusId = 1,
                        SubmissionPresentationMethodId = 6
                    }
                );

                context.SaveChanges();

                context.VirtualConferenceSession.AddRange(
                    new VirtualConferenceSession
                    {
                        VirtualConferenceSessionId = 1,
                        SubmissionId = 10004,
                        SubmissionPresentationMethodId = 1,
                        SessionCode = "P01",
                        SessionStartDateTime = new DateTime(2018,5, 1, 8, 30, 0),
                        SessionEndDateTime = new DateTime(2018, 5, 1, 9, 00, 0),
                    },
                    new VirtualConferenceSession
                    {
                        VirtualConferenceSessionId = 2,
                        SubmissionId = 10005,
                        SubmissionPresentationMethodId = 2,
                        SessionStartDateTime = new DateTime(2018, 5, 1, 8, 30, 0),
                        SessionEndDateTime = new DateTime(2018, 5, 1, 9, 00, 0),
                        SessionCode = "P02"
                    },
                    new VirtualConferenceSession
                    {
                        VirtualConferenceSessionId = 3,
                        SubmissionId = 10006,
                        SubmissionPresentationMethodId = 3,
                        SessionStartDateTime = new DateTime(2018, 5, 1, 9, 30, 0),
                        SessionEndDateTime = new DateTime(2018, 5, 1, 10, 00, 0),
                        SessionCode = "P03"
                    },
                    new VirtualConferenceSession
                    {
                        VirtualConferenceSessionId = 4,
                        SubmissionId = 10009,
                        SubmissionPresentationMethodId = 4,
                        SessionStartDateTime = new DateTime(2018, 5, 1, 10, 30, 0),
                        SessionEndDateTime = new DateTime(2018, 5, 1,11, 00, 0),
                        SessionCode = "P04"
                    },
                    new VirtualConferenceSession
                    {
                        VirtualConferenceSessionId = 5,
                        SubmissionId = 10010,
                        SubmissionPresentationMethodId = 5,
                        SessionStartDateTime = new DateTime(2018, 5, 1, 8, 00, 0),
                        SessionEndDateTime = new DateTime(2018, 5, 1, 9, 00, 0),
                        SessionCode = "P05"
                    },
                    new VirtualConferenceSession
                    {
                        VirtualConferenceSessionId = 6,
                        SubmissionId = 10011,
                        SubmissionPresentationMethodId = 6,
                        SessionStartDateTime = new DateTime(2018, 5, 1, 7, 30, 0),
                        SessionEndDateTime = new DateTime(2018, 5, 1, 9, 00, 0),
                        SessionCode = "P06"
                    }
                );

                context.SaveChanges();

            }

            return options;
        }

    }
}
