﻿using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Stores data about use of a room at a conference
    /// </summary>
    public class RoomUse
    {

        public string RoomName { get; set; }
        public DateTime RoomStartDateTime { get; set; } 
        public DateTime RoomEndDateTime { get; set; }

        public List<ConferenceSession> SessionsInRoom { get; set; }


    }
}
