﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    public class ReviewStatus
    {
        public ReviewStatus()
        {
        }

        public int ReviewStatusId { get; set; }

        [StringLength(250)]
        [Required]
        public ReviewStatusType ReviewStatusType { get; set; }


        public List<Review> Reviews { get; set; }
    }
}
