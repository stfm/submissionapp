﻿using System;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.Models;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Data;
using System.Collections.Generic;
using Serilog;
using System.Linq;
using ConferenceSubmission.Extensions;

namespace ConferenceSubmission.Services
{
    public class ConferenceSessionService : IConferenceSessionService
    {

        private readonly IConferenceSessionRepository _conferenceSessionRepository;

        private readonly ISubmissionService _submissionService;

        private readonly IConferenceService _conferenceService;

        private readonly ISessionObjectiveRepository _sessionObjectiveRepository;

        private readonly IConferenceSessionsCacheService _conferenceSessionsCacheService;

        private readonly IFormFieldRepository _formFieldRepository;

        public ConferenceSessionService(IConferenceSessionRepository conferenceSessionRepository, ISubmissionService submissionService,
            IConferenceService conferenceService, ISessionObjectiveRepository sessionObjectiveRepository, IConferenceSessionsCacheService conferenceSessionsCacheService,
            IFormFieldRepository formFieldRepository)
        {

            _conferenceSessionRepository = conferenceSessionRepository;

            _submissionService = submissionService;

            _conferenceService = conferenceService;

            _sessionObjectiveRepository = sessionObjectiveRepository;

            _conferenceSessionsCacheService = conferenceSessionsCacheService;

            _formFieldRepository = formFieldRepository;
        }

        public int AddConferenceSession(SessionDataBindingModel sessionDataBindingModel)
        {
            ConferenceSession conferenceSession = new ConferenceSession
            {
                SessionCode = sessionDataBindingModel.SessionCode,
                SessionEndDateTime = sessionDataBindingModel.SessionEndDateTime,
                SessionStartDateTime = sessionDataBindingModel.SessionStartDateTime,
                SubmissionId = sessionDataBindingModel.SubmissionId,
                SessionLocation = sessionDataBindingModel.SessionLocation,
                SessionTrack = sessionDataBindingModel.SessionTrack,
                SubmissionPresentationMethod = sessionDataBindingModel.SubmissionPresentationMethod


            };

            var conferenceSessionId = _conferenceSessionRepository.AddConferenceSession(conferenceSession);

            return conferenceSessionId;
        }

        public ConferenceSession GetConferenceSession(int submissionId)
        {
            ConferenceSession conferenceSession = _conferenceSessionRepository.GetConferenceSession(submissionId);

            if (conferenceSession is null)
            {

                conferenceSession = new ConferenceSession();


            }

            return conferenceSession;

        }

        public int UpdateConferenceSession(SessionDataBindingModel sessionDataBindingModel)
        {

            ConferenceSession conferenceSession = GetConferenceSession(sessionDataBindingModel.SubmissionId);


            conferenceSession.SessionCode = sessionDataBindingModel.SessionCode;
            conferenceSession.SessionEndDateTime = sessionDataBindingModel.SessionEndDateTime;
            conferenceSession.SessionStartDateTime = sessionDataBindingModel.SessionStartDateTime;
            conferenceSession.SubmissionId = sessionDataBindingModel.SubmissionId;
            conferenceSession.SessionLocation = sessionDataBindingModel.SessionLocation;
            conferenceSession.SessionTrack = sessionDataBindingModel.SessionTrack;
            conferenceSession.SubmissionPresentationMethod = sessionDataBindingModel.SubmissionPresentationMethod;

            return _conferenceSessionRepository.UpdateConferenceSession(conferenceSession);


        }

        public DTOs.ConferenceDTO GetConferenceSessions(int conferenceId)
        {
            //Log.Information("Getting conference sessions for conferenceId of " + conferenceId);

            // Try to get the ConferenceDTO object from cache first. If not, build the Conference DTO object and set it in the cache.
            var conferenceDTOFromCache = _conferenceSessionsCacheService.GetConferenceDTO(conferenceId);

            if (conferenceDTOFromCache != null)
                return conferenceDTOFromCache;
            
            Dictionary<int, SessionObjectives> sessionObjectivesDict = _sessionObjectiveRepository.getSessionObjectivesDictionary(conferenceId);

            //Log.Information("Got dictionary of session objectives - number of entries is " + sessionObjectivesDict.Count);

            List<DTOs.Session> sessions = new List<DTOs.Session>();

            List<Submission> acceptedSubmissions = _submissionService.GetAcceptedSubmissionsForConference(conferenceId);

            //Log.Information("Got accepted submissions for conferenceId of " + conferenceId);

            foreach (Submission acceptedSubmission in acceptedSubmissions)
            {
                //Log.Information("Processing submission id " + acceptedSubmission.SubmissionId);

                List<string> participantNames = new List<string>();

                List<string> participantEmails = new List<string>();

                foreach (SubmissionParticipant participant in acceptedSubmission.ParticipantLink.OrderBy(p => p.SortOrder))
                {
                    try
                    {
                        string name = participant.Participant.ParticipantUser.GetFullNamePretty();

                        participantNames.Add(name.Trim());

                        string email = participant.Participant.ParticipantUser.Email;

                        participantEmails.Add(email.Trim());

                    }
                    catch (Exception ex)
                    {
                        Log.Warning("Unable to add participant " + participant.Participant.StfmUserId + " for submission id " + acceptedSubmission.SubmissionId + " - " + ex.Message);
                    }

                }


                try
                {
                    //Only add the sesssion if the submission was accepted and assigned a session
                    if (acceptedSubmission.ConferenceSession != null)
                    {
                        DTOs.Session session = new DTOs.Session
                        {
                            Abstract = acceptedSubmission.SubmissionAbstract.ReplaceLineBreaksWithBreakTags(),
                            Category = acceptedSubmission.AcceptedCategory.SubmissionCategoryName,
                            SessionId = acceptedSubmission.SubmissionId,
                            Title = acceptedSubmission.SubmissionTitle,
                            Participants = participantNames,
                            ParticipantsEmails = participantEmails

                        };

                        //NOT every submission has objectives
                        if (sessionObjectivesDict.ContainsKey(acceptedSubmission.SubmissionId))
                        {
                            session.Abstract = session.Abstract + "<br /> <br /> Upon completion of this session, participants should be able to: <br />" + sessionObjectivesDict[acceptedSubmission.SubmissionId].objectives;
                        }

                        //NOT every submission has a track
                        if (!String.IsNullOrEmpty(acceptedSubmission.ConferenceSession.SessionTrack))
                        {
                            session.Title = session.Title + " (" + acceptedSubmission.ConferenceSession.SessionTrack + ")";
                            session.Track = acceptedSubmission.ConferenceSession.SessionTrack;
                        }

                        session.EndTime = acceptedSubmission.ConferenceSession.SessionEndDateTime;
                        session.Room = acceptedSubmission.ConferenceSession.SessionLocation;
                        session.SessionCode = acceptedSubmission.ConferenceSession.SessionCode;
                        session.StartTime = acceptedSubmission.ConferenceSession.SessionStartDateTime;

                        sessions.Add(session);
                    }
                }
                catch (Exception ex)
                {
                    Log.Warning("Unable to return submission " + acceptedSubmission.SubmissionId + " because " + ex.Message);
                    //do nothing it is OK to have accepted submissions that do not have a Conference session

                }
            }

            ConferenceSubmission.Models.Conference conference = _conferenceService.GetConference(conferenceId);

            DTOs.ConferenceDTO conferenceWithSessions = new DTOs.ConferenceDTO
            {
                ConferenceCode = conference.ConferenceShortName,
                EndDate = conference.ConferenceEndDate,
                Name = conference.ConferenceLongName.Replace("&#38;", "&"),
                ConferenceId = conference.ConferenceId,
                Sessions = sessions,
                StartDate = conference.ConferenceStartDate
            };


            // Now that the ConferenceDTO is built, set it in the cache.
            _conferenceSessionsCacheService.SetConferenceDTO(conferenceWithSessions);

            return conferenceWithSessions;
        }

        public DTOs.ConferenceDTO GetVirtualConferenceSessions(int conferenceId)
        {
            //Log.Information("Getting conference sessions for conferenceId of " + conferenceId);

            Dictionary<int, SessionObjectives> sessionObjectivesDict = _sessionObjectiveRepository.getSessionObjectivesDictionary(conferenceId);

            //Log.Information("Got dictionary of session objectives - number of entries is " + sessionObjectivesDict.Count);

            List<DTOs.Session> sessions = new List<DTOs.Session>();

            List<Submission> acceptedSubmissions = _submissionService.GetAcceptedSubmissionsForConference(conferenceId);

            //Log.Information("Got accepted submissions for conferenceId of " + conferenceId);

            foreach (Submission acceptedSubmission in acceptedSubmissions)
            {
                

                //Only want to process accepted submissions that are being presented at virtual conference supplement

                if (acceptedSubmission.VirtualConferenceSessions.Count > 0)
                {
                    //Log.Information("Processing submission id " + acceptedSubmission.SubmissionId);

                    List<string> participantNames = new List<string>();

                    List<string> participantEmails = new List<string>();

                    foreach (SubmissionParticipant participant in acceptedSubmission.ParticipantLink.OrderBy(p => p.SortOrder))
                    {
                        try
                        {
                            string name = participant.Participant.ParticipantUser.GetFullNamePretty();

                            participantNames.Add(name.Trim());

                            string email = participant.Participant.ParticipantUser.Email;

                            participantEmails.Add(email.Trim());

                        }
                        catch (Exception ex)
                        {
                            Log.Warning("Unable to add participant " + participant.Participant.StfmUserId + " for submission id " + acceptedSubmission.SubmissionId + " - " + ex.Message);
                        }

                    }


                    try
                    {
                        DTOs.Session session = new DTOs.Session
                        {
                            Abstract = acceptedSubmission.SubmissionAbstract.ReplaceLineBreaksWithBreakTags(),
                            Category = acceptedSubmission.AcceptedCategory.SubmissionCategoryName,
                            SessionId = acceptedSubmission.SubmissionId,
                            Title = acceptedSubmission.SubmissionTitle,
                            Participants = participantNames,
                            ParticipantsEmails = participantEmails

                        };


                        //NOT every submission has objectives
                        if (sessionObjectivesDict.ContainsKey(acceptedSubmission.SubmissionId))
                        {
                            session.Abstract = session.Abstract + "<br /> <br /> Upon completion of this session, participants should be able to: <br />" + sessionObjectivesDict[acceptedSubmission.SubmissionId].objectives;
                        }

                        //NOT every submission has a track
                        if (!String.IsNullOrEmpty(acceptedSubmission.ConferenceSession.SessionTrack))
                        {
                            session.Title = session.Title + " (" + acceptedSubmission.ConferenceSession.SessionTrack + ")";
                            session.Track = acceptedSubmission.ConferenceSession.SessionTrack;
                        }


                        foreach (VirtualConferenceSession virtualConferenceSession in acceptedSubmission.VirtualConferenceSessions) 
                        {
                            if (virtualConferenceSession.SubmissionPresentationMethod.PresentationMethod.PresentationMethodName == PresentationMethodType.RECORDED_ONLINE)
                            {
                                session.EndTime = virtualConferenceSession.SessionEndDateTime;
                                session.Room = "Online";
                                session.SessionCode = virtualConferenceSession.SessionCode;
                                session.StartTime = virtualConferenceSession.SessionStartDateTime;
                            }

                        }

                        //Only add the sesssion if the submission was accepted and assigned a session
                        sessions.Add(session);



                    }
                    catch (Exception ex)
                    {
                        Log.Warning("Unable to return submission " + acceptedSubmission.SubmissionId + " because " + ex.Message);
                        //do nothing it is OK to have accepted submissions that do not have a Conference session

                    }

                }

            }

            ConferenceSubmission.Models.Conference conference = _conferenceService.GetConference(conferenceId);

            DTOs.ConferenceDTO conferenceWithSessions = new DTOs.ConferenceDTO
            {
                ConferenceCode = conference.ConferenceShortName,
                EndDate = conference.ConferenceEndDate,
                Name = conference.ConferenceLongName.Replace("&#38;", "&"),
                ConferenceId = conference.ConferenceId,
                Sessions = sessions,
                StartDate = conference.ConferenceStartDate

            };

            return conferenceWithSessions;

        }

            public List<SessionWithConferenceDetails> GetConferenceSessions(IEnumerable<int> submissionIds)
        {

            //Log.Information("In ConferenceSessionService - Get ConferenceSessions...");

            Dictionary<int, SessionObjectives> sessionObjectivesDict = _sessionObjectiveRepository.getSessionObjectivesDictionary(submissionIds);

            List<SessionWithConferenceDetails> sessions = new List<SessionWithConferenceDetails>();

            List<Submission> acceptedSubmissions = _submissionService.GetSubmissions(submissionIds);

            //Log.Information("Got " + acceptedSubmissions.Count);

            foreach (Submission acceptedSubmission in acceptedSubmissions)
            {

                List<string> participantNames = new List<string>();

                foreach (SubmissionParticipant participant in acceptedSubmission.ParticipantLink.OrderBy(p => p.SortOrder))
                {
                    string name = participant.Participant.ParticipantUser.GetFullNamePretty();

                    //Add space before participant name so the names are formatted consistently 
                    //with the sessions returned from the conference archives application
                    participantNames.Add($" {name.Trim()}");

                }

                SessionWithConferenceDetails sessionWithConferenceDetails = new SessionWithConferenceDetails
                {

                    Abstract = acceptedSubmission.SubmissionAbstract.ReplaceLineBreaksWithBreakTags(),
                    Category = acceptedSubmission.AcceptedCategory.SubmissionCategoryName,
                    EndTime = acceptedSubmission.ConferenceSession.SessionEndDateTime,
                    Room = acceptedSubmission.ConferenceSession.SessionLocation,
                    SessionCode = acceptedSubmission.ConferenceSession.SessionCode,
                    SessionId = acceptedSubmission.SubmissionId,
                    StartTime = acceptedSubmission.ConferenceSession.SessionStartDateTime,
                    Title = acceptedSubmission.SubmissionTitle,
                    Participants = participantNames,
                    ConferenceId = acceptedSubmission.Conference.ConferenceId,
                    ConferenceName = acceptedSubmission.Conference.ConferenceLongName.Replace("&#38;", "&"),
                    ConferenceYear = acceptedSubmission.Conference.ConferenceStartDate.Year.ToString()

                };

                //NOT every submission has objectives
                if (sessionObjectivesDict.ContainsKey(acceptedSubmission.SubmissionId))
                {
                    sessionWithConferenceDetails.Abstract = sessionWithConferenceDetails.Abstract + "<br /> <br /> Upon completion of this session, participants should be able to: <br />" + sessionObjectivesDict[acceptedSubmission.SubmissionId].objectives;
                }

                //NOT every submission has a track
                if (!String.IsNullOrEmpty(acceptedSubmission.ConferenceSession.SessionTrack))
                {
                    sessionWithConferenceDetails.Title = sessionWithConferenceDetails.Title + " (" + acceptedSubmission.ConferenceSession.SessionTrack + ")";
                }

                sessions.Add(sessionWithConferenceDetails);

                //Log.Information("Added sessionWithConferenceDetails: " + sessionWithConferenceDetails.SessionCode);

            }

            return sessions;


        }

        public void UpdateConferenceSessionWithRoom(int submissionId, string room, string track)
        {
            ConferenceSession conferenceSession = _conferenceSessionRepository.GetConferenceSession(submissionId);

            Log.Information("Found conference session with submission id of " + submissionId + " session code " + conferenceSession.SessionCode);

            conferenceSession.SessionLocation = room;

            if (track != null && ! track.Equals("None", StringComparison.InvariantCulture) )
            {

                conferenceSession.SessionTrack = track;
            }

            _conferenceSessionRepository.UpdateConferenceSession(conferenceSession);
        }

        public DTOs.ConferenceDTO GetConferenceSessionsMobileApp(int conferenceId)
        {
            Log.Information("Getting conference sessions for conferenceId of " + conferenceId);

            //Dictionary<int, SessionObjectives> sessionObjectivesDict = _sessionObjectiveRepository.getSessionObjectivesDictionary(conferenceId);

            //Log.Information("Got dictionary of session objectives - number of entries is " + sessionObjectivesDict.Count);

            List<DTOs.Session> sessions = new List<DTOs.Session>();

            List<Submission> acceptedSubmissions = _submissionService.GetAcceptedSubmissionsForConference(conferenceId);

            Log.Information("Got accepted submissions for conferenceId of " + conferenceId);

            foreach (Submission acceptedSubmission in acceptedSubmissions)
            {
                //Log.Information("Processing submission id " + acceptedSubmission.SubmissionId);

                List<string> participantNames = new List<string>();

                List<string> participantEmails = new List<string>();

                foreach (SubmissionParticipant participant in acceptedSubmission.ParticipantLink.OrderBy(p => p.SortOrder))
                {
                    try
                    {
                        string name = participant.Participant.ParticipantUser.GetFullNamePretty();

                        participantNames.Add(name.Trim());

                        string email = participant.Participant.ParticipantUser.Email;

                        participantEmails.Add(email.Trim());

                    }
                    catch (Exception ex)
                    {
                        Log.Warning("Unable to add participant " + participant.Participant.StfmUserId + " for submission id " + acceptedSubmission.SubmissionId + " - " + ex.Message);
                    }

                }


                try
                {
                    DTOs.Session session = new DTOs.Session
                    {
                        Abstract = acceptedSubmission.SubmissionAbstract.ReplaceLineBreaksWithBreakTags(),
                        Category = acceptedSubmission.AcceptedCategory.SubmissionCategoryName,
                        SessionId = acceptedSubmission.ConferenceSession.ConferenceSessionId,
                        Title = acceptedSubmission.SubmissionTitle,
                        Participants = participantNames,
                        ParticipantsEmails = participantEmails,
                        SubmissionId = acceptedSubmission.SubmissionId.ToString()
                        

                    };




                    session.EndTime = acceptedSubmission.ConferenceSession.SessionEndDateTime;
                    session.Room = acceptedSubmission.ConferenceSession.SessionLocation;
                    session.SessionCode = acceptedSubmission.ConferenceSession.SessionCode;
                    session.StartTime = acceptedSubmission.ConferenceSession.SessionStartDateTime;

                    //TODO: For now just get the first String in the List which will be the Keyword One value
                    //assign to Track field so we don't need to modify the Session class.
                    session.Track = GetAdditionalSubmissionDetails(acceptedSubmission.SubmissionId)[0];
                    //Only add the sesssion if the submission was accepted and assigned a session
                    sessions.Add(session);

                }
                catch (Exception ex)
                {
                    Log.Warning("Unable to return submission " + acceptedSubmission.SubmissionId + " because " + ex.Message);
                    //do nothing it is OK to have accepted submissions that do not have a Conference session

                }



            }

            ConferenceSubmission.Models.Conference conference = _conferenceService.GetConference(conferenceId);

            DTOs.ConferenceDTO conferenceWithSessions = new DTOs.ConferenceDTO
            {
                ConferenceCode = conference.ConferenceShortName,
                EndDate = conference.ConferenceEndDate,
                Name = conference.ConferenceLongName.Replace("&#38;", "&"),
                ConferenceId = conference.ConferenceId,
                Sessions = sessions,
                StartDate = conference.ConferenceStartDate

            };

            return conferenceWithSessions;


        }

        public List<ConferenceSession> GetConferenceSessionsForInPerson(int conferenceId)
        {
            List<ConferenceSession> conferenceSessions = _conferenceSessionRepository.GetSessionsForConference(conferenceId).ToList();

            //Business rule is to not include Poster and Breakfast sessions

            conferenceSessions.RemoveAll(cs => cs.Submission.AcceptedCategory.SubmissionCategoryName.Contains("Poster", StringComparison.InvariantCultureIgnoreCase) ||
            cs.Submission.AcceptedCategory.SubmissionCategoryName.Contains("Scholarly", StringComparison.InvariantCultureIgnoreCase));

            return conferenceSessions;


        }

        public SortedDictionary<DateOnly, List<RoomUse>> GetRoomUse(List<ConferenceSession> conferenceSessions)
        {
            SortedDictionary<DateOnly, List<RoomUse>> roomUseDict = new SortedDictionary<DateOnly, List<RoomUse>>();

            //Get collection of unique start dates - these will be the Dictionary keys

            var startDates = conferenceSessions.GroupBy(x => x.SessionStartDateTime.Date).Distinct().ToList();

            var rooms = conferenceSessions.GroupBy(x => x.SessionLocation).Distinct().ToList();

            //for each Date in startDates get earliest time and latest time each room is used then create RoomUse object

            foreach (IGrouping<DateTime, ConferenceSession> conferenceSession in startDates)
            {
                DateOnly startDate = DateOnly.FromDateTime(conferenceSession.Key);

                List<RoomUse> roomsUse = new List<RoomUse>();

                foreach (IGrouping<String, ConferenceSession> room in rooms)
                {

                    String roomName = room.Key;

                    List<ConferenceSession> roomStartDateTimeSessions = conferenceSessions.Where(x => x.SessionLocation.Equals(roomName) && DateOnly.FromDateTime(x.SessionStartDateTime).CompareTo(startDate) == 0).ToList();


                    List<ConferenceSession> roomEndDateTimeSessions = conferenceSessions.Where(x => x.SessionLocation.Equals(roomName) && DateOnly.FromDateTime(x.SessionEndDateTime).CompareTo(startDate) == 0).ToList();

                    List<ConferenceSession> sessionsInRoom = new List<ConferenceSession>();

                    if (roomStartDateTimeSessions.Count > 0 && roomEndDateTimeSessions.Count > 0)
                    {

                        DateTime? roomStartDateTime = roomStartDateTimeSessions.Min(x => x.SessionStartDateTime);

                        DateTime? roomEndDateTime = roomEndDateTimeSessions.Max(x => x.SessionEndDateTime);

                        if (roomStartDateTime != null && roomEndDateTime != null)
                        {
                            RoomUse roomUse = new RoomUse()
                            {
                                RoomName = roomName,
                                RoomStartDateTime = (DateTime)roomStartDateTime,
                                RoomEndDateTime = (DateTime)roomEndDateTime,
                                SessionsInRoom = roomStartDateTimeSessions.OrderBy(s => s.SessionStartDateTime).ToList(),
                                
                            };


                            roomsUse.Add(roomUse);
                        }
                    }

                }

                roomUseDict.Add(startDate, roomsUse);

            }

            return roomUseDict;

        }


        private List<string> GetAdditionalSubmissionDetails(int submissionId)
        {

            List<SubmissionFormField> submissionFormFields = _formFieldRepository.GetFormFieldsForSubmission(submissionId).ToList();

            string keywordOne = "";

            foreach (SubmissionFormField submissionFormField in submissionFormFields)
            {
                foreach (FormFieldProperty formFieldProperty in submissionFormField.FormField.FormFieldProperties)
                {
                    if (formFieldProperty.PropertyValue.Equals("Keyword One", StringComparison.InvariantCultureIgnoreCase))
                    {
                        keywordOne = submissionFormField.SubmissionFormFieldValue;
                    }

                    

                }
            }



            List<string> answers = new List<string>
            {
                keywordOne,
                
            };

            return answers;

        }
    }
}
