﻿using ConferenceSubmission.Extensions;
using ConferenceSubmission.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace ConferenceSubmission.Controllers
{
    public class AuthenticationController : Controller
    {
        public IConfiguration Configuration { get; }
        public AuthenticationController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        [HttpGet("~/signin")]
        public async Task<IActionResult> SignIn() => View(
            "SignIn", 
            new AuthenticationViewModel
            {
                AuthenticationSchemes =  await HttpContext.GetExternalProvidersAsync(),
                ReturnUrl = string.IsNullOrWhiteSpace(HttpContext.Request.Query["ReturnUrl"].ToString()) ? "/" : HttpContext.Request.Query["ReturnUrl"].ToString()
            }
        );

        [HttpPost("~/signin")]
        public async Task<IActionResult> SignIn([FromForm] string provider, [FromForm] string ReturnUrl)
        {
            // Note: the "provider" parameter corresponds to the external
            // authentication provider choosen by the user agent.
            if (string.IsNullOrWhiteSpace(provider))
            {
                return BadRequest();
            }

            if (!await HttpContext.IsProviderSupportedAsync(provider))
            {
                return BadRequest();
            }

            // Instruct the middleware corresponding to the requested external identity
            // provider to redirect the user agent to its own authorization endpoint.
            // Note: the authenticationScheme parameter must match the value configured in Startup.cs
            return Challenge(new AuthenticationProperties { RedirectUri = ReturnUrl }, provider);
        }

        [HttpGet("~/signout"), HttpPost("~/signout")]
        public IActionResult SignOut()
        {
            // Instruct the cookies middleware to delete the local cookie created
            // when the user agent is redirected from the external identity provider
            // after a successful authentication flow (e.g Google or Facebook).
            return SignOut(new AuthenticationProperties { RedirectUri = "/Authentication/SalesforceSignout" },
                CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public IActionResult SalesforceSignout()
        {
            return Redirect($"{Configuration["CommunityUrl"]}/secur/logout.jsp");
        }
    }
}
