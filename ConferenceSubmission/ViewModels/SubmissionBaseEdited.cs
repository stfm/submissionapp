﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// View model class needed for encapsulating information
    /// needed when sending an email that the basic submission
    /// details have been edited.
    /// </summary>
    public class SubmissionBaseEdited : Submission
    {

        public string SubmitterEmailAddress { get; set; }

        public string MainPresenterEmailAddress { get; set; }


    }
}
