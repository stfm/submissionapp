﻿$(document).ready(function () {




$('select').on('change', function() {
var conferenceId = $(this).children("option:selected").val();
//alert("You have selected the conference id - " + conferenceId);
GetCategories(conferenceId);
});



function GetCategories(conferenceId) {
    //alert("Conference Id: " + conferenceId);
    $.ajax({
        url: '/SubmissionAdmin/GetCategories?conferenceId='+conferenceId,
        type: 'POST',
        cache: false,
        async: true

    })
        .done(function (result) {
            //alert("Back from controller");
            $('#categories').html(result);
        }).fail(function (xhr) {
            alert('error : ' + xhr.status + ' - '
            + xhr.statusText + ' - ' + xhr.responseText);
        });
}
});