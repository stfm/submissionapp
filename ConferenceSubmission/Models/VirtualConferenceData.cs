﻿using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Stores information related to a 
    /// conference that is presented 
    /// virtually.
    /// </summary>
    public class VirtualConferenceData
    {

        public int VirtualConferenceDataId { get; set; }

        public int ConferenceId { get; set; }

        public Conference Conference { get; set; }

        [StringLength(100)]
        public string BlobContainerName { get; set; }
       
        [StringLength(100)]
        public string DropboxFolderName { get; set; }

        [StringLength(250)]
        public string DropboxUploadUrl { get; set; }

    }
}
