﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Model classes used in the view that allows
    /// user to edit an existing review.
    /// </summary>
    public class EditReviewBindingModel
    {

        public int ReviewId { get; set; }

        public List<FormField> FormFields { get; set; }

        public List<ReviewFormField> ReviewFormFields { get; set; }

        public Submission Submission { get; set; }

        public List<Presenter> Presenters { get; set; }

        public List<SubmissionFormField> SubmissionFormFields { get; set; }

        public string GetReviewFormFieldValue(int formFieldId)
        {
            var reviewFormField = ReviewFormFields.FirstOrDefault(s => s.FormFieldId == formFieldId);
            return reviewFormField == null ? string.Empty : reviewFormField.ReviewFormFieldValue;
        }



    }
}
