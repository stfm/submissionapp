﻿using System;
using Microsoft.AspNetCore.Mvc;
using ConferenceSubmission.BindingModels;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using ConferenceSubmission.Mappers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using AuthorizeNet.Api.Controllers;
using ConferenceSubmission.ViewModels;
using Microsoft.Extensions.Configuration;
using ConferenceSubmission.Services;
using ConferenceSubmission.Models;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Blobs;
using System.Threading.Tasks;
using Castle.Core.Internal;
using System.Linq;
using System.IO;

namespace ConferenceSubmission.Controllers
{
    [Authorize]
    public class DisclosureController : Controller
    {

        private IDisclosureService _disclosureService;

        private readonly IUserMapper _userMapper;

        private readonly IEmailNotificationService _emailNotificationService;

        private ISubmissionService _submissionService;

        private readonly IConfiguration _configuration;

        public DisclosureController(IDisclosureService disclosureService,
                                    IUserMapper userMapper, IEmailNotificationService emailNotificationService, 
                                    ISubmissionService submissionService, IConfiguration configuration)
        {

            _disclosureService = disclosureService;

            _userMapper = userMapper;

            _emailNotificationService = emailNotificationService;

            _submissionService = submissionService;
            _configuration = configuration; 
        }

        /// <summary>
        /// Responds to user request to create a new disclosure.
        /// NOTE if submissionId = 0 then we are creating a disclosure
        /// for an presenter who is adding himself to the submission.
        /// if submissionId = -1 then we are creating a disclosure
        /// for a person who has clicked on the create disclosure 
        /// link on the home page.
        /// </summary>
        /// <returns>The view page to create new disclosure</returns>
        public IActionResult Index(int submissionId)
        {

            var user = _userMapper.Map(User.Claims);

            Submission submission = _submissionService.GetSubmission(submissionId);

            // If the submission is for CPQI and requires CV file for disclosure set this value to true
            bool isCPQISubmission = false;

            DisclosureBindingModel disclosureBindingModel = new DisclosureBindingModel
            {

                StfmUserId = user.STFMUserId,

                SubmissionId = submissionId,

                isSubmissionForCPQI = isCPQISubmission
                                 

            };

            return View(disclosureBindingModel);

        }


        /// <summary>
        /// Process user submitting disclosure.
        /// </summary>
        /// <returns>The disclosure.</returns>
        /// <param name="disclosureBindingModel">Disclosure binding model.</param>
        public async Task<IActionResult> CreateDisclosureAsync(DisclosureBindingModel disclosureBindingModel) {

            Log.Information("STFM User " + disclosureBindingModel.StfmUserId +
                            " submitted a disclosure with this information: " + disclosureBindingModel.ToString());

            if (disclosureBindingModel.isSubmissionForCPQI)
            {

                if (disclosureBindingModel.CvFile == null)
                {

                    ViewData["Error"] = "You must upload a CV File of type pdf. Please ensure you have uploaded a CV File.";

                    DisclosureBindingModel disclosureBindingModelError = new DisclosureBindingModel
                    {

                        StfmUserId = disclosureBindingModel.StfmUserId,

                        SubmissionId = disclosureBindingModel.SubmissionId,

                        isSubmissionForCPQI = true
                    };

                    return View("Index", disclosureBindingModelError);
                }
            }


            Disclosure disclosure = GetDisclosureFromDisclosureBindingModel(disclosureBindingModel);

            if (disclosure.CvFilename != null)
            {

                string[] cvFilePermittedExtensions = { ".pdf" };

                var extension = Path.GetExtension(disclosure.CvFilename).ToLowerInvariant();

                if (string.IsNullOrEmpty(extension) || !cvFilePermittedExtensions.Contains(extension))
                {

                    ViewData["Error"] = "Only CV Files of Type pdf Are Allowed. Please ensure your CV File is of type pdf.";

                    DisclosureBindingModel disclosureBindingModelError = new DisclosureBindingModel
                    {

                        StfmUserId = disclosureBindingModel.StfmUserId,

                        SubmissionId = disclosureBindingModel.SubmissionId,

                        isSubmissionForCPQI = true


                    };

                    return View("Index", disclosureBindingModelError);
                }
            }


            _disclosureService.AddDisclosure(disclosure);

            if (disclosure.CvFilename != null)
            {
                string blobstorageconnection = _configuration["BlobStorage:key"];

                string blobContainer = "cvfiles";

                BlobContainerClient handoutBlobContainerClient = new BlobContainerClient(blobstorageconnection, blobContainer);

                var handoutBlob = handoutBlobContainerClient.GetBlobClient(disclosure.CvFilename);

                Azure.Response<BlobContentInfo> handoutUploadResponse = await handoutBlob.UploadAsync(disclosureBindingModel.CvFile.OpenReadStream());

                Log.Information("STFM User " + disclosureBindingModel.StfmUserId +
                            " uploaded a CV File as part of disclosure process: " + disclosure.CvFilename);

            }

            var user = _userMapper.Map(User.Claims);

            _emailNotificationService.SendDisclosureEmail(user.Email, disclosure);

            return RedirectToAction(actionName: "DisclosureSuccess", controllerName: "Disclosure", routeValues: new { submissionId = disclosureBindingModel.SubmissionId });


        }

        /// <summary>
        /// Respond to user request to edit an existing disclosure.
        /// </summary>
        /// <returns>The disclosure edit view.</returns>
        /// <param name="submissionId">Submission Id</param>
        public IActionResult EditDisclosure(int submissionId)
        {
            var user = _userMapper.Map(User.Claims);

            Disclosure disclosure = _disclosureService.GetDisclosure(user.STFMUserId);

            Submission submission = _submissionService.GetSubmission(submissionId);


            bool isCPQISubmission = false;

            DisclosureBindingModel disclosureBindingModel = GetDisclosureBindingModelFromDisclosure(disclosure);

            disclosureBindingModel.SubmissionId = submissionId;

            disclosureBindingModel.isSubmissionForCPQI = isCPQISubmission;

            return View(disclosureBindingModel);


        }



        /// <summary>
        /// Process user request to update disclosure.
        /// </summary>
        /// <returns>The disclosure process.</returns>
        /// <param name="disclosureBindingModel">Disclosure binding model.</param>
        public IActionResult EditDisclosureProcess(DisclosureBindingModel disclosureBindingModel)
        {
            Log.Information("STFM User " + disclosureBindingModel.StfmUserId +
                            " updated an existing disclosure with this information: " + disclosureBindingModel.ToString());


            Disclosure disclosure = GetDisclosureFromDisclosureBindingModel(disclosureBindingModel);

            if (disclosureBindingModel.isSubmissionForCPQI)
            {

                if (disclosureBindingModel.CvFile == null)
                {

                    ViewData["Error"] = "You must upload a CV File of type pdf. Please ensure you have uploaded a CV File.";

                    DisclosureBindingModel disclosureBindingModelError = new DisclosureBindingModel
                    {

                        StfmUserId = disclosureBindingModel.StfmUserId,

                        SubmissionId = disclosureBindingModel.SubmissionId,

                        isSubmissionForCPQI = true
                    };

                    return View("EditDisclosure", disclosureBindingModelError);
                }
            }


            if (disclosure.CvFilename != null)
            {

                string[] cvFilePermittedExtensions = { ".pdf" };

                var extension = Path.GetExtension(disclosure.CvFilename).ToLowerInvariant();

                if (string.IsNullOrEmpty(extension) || !cvFilePermittedExtensions.Contains(extension))
                {

                    ViewData["Error"] = "Only CV Files of Type pdf Are Allowed. Please ensure your CV File is of type pdf.";

                    DisclosureBindingModel disclosureBindingModelError = new DisclosureBindingModel
                    {

                        StfmUserId = disclosureBindingModel.StfmUserId,

                        SubmissionId = disclosureBindingModel.SubmissionId,

                        isSubmissionForCPQI = true


                    };

                    return View("Index", disclosureBindingModelError);
                }
            }

            _disclosureService.UpdateDisclosure(disclosure);

            if (disclosure.CvFilename != null)
            {
                string blobstorageconnection = _configuration["BlobStorage:key"];

                string blobContainer = "cvfiles";

                BlobContainerClient handoutBlobContainerClient = new BlobContainerClient(blobstorageconnection, blobContainer);

                var handoutBlob = handoutBlobContainerClient.GetBlobClient(disclosure.CvFilename);

                Azure.Response<BlobContentInfo> handoutUploadResponse = handoutBlob.UploadAsync(disclosureBindingModel.CvFile.OpenReadStream()).Result;

                Log.Information("STFM User " + disclosureBindingModel.StfmUserId +
                            " uploaded a CV File as part of disclosure process: " + disclosure.CvFilename);

            }

            var user = _userMapper.Map(User.Claims);

            _emailNotificationService.SendDisclosureEmail(user.Email, disclosure);

            return RedirectToAction(actionName: "DisclosureSuccess", controllerName: "Disclosure", routeValues: new { submissionId = disclosureBindingModel.SubmissionId });

        }


        public IActionResult DisclosureSuccess(int submissionId) {
            
            ViewData["submissionId"] = submissionId;

            return View();


        }

       
        /// <summary>
        /// Starts the disclosure process for a user who did not
        /// do the disclosure as part of the regular submission
        /// process but needs to create or update a disclosure when
        /// editing the submission or from the home page.
        /// </summary>
        /// <param name="submissionId">Submission ID of the submission user was working on.</param>
        /// <returns>Redirects user to either create a new disclosure or edit existing disclosure.</returns>
        public RedirectToActionResult StartDisclosureProcess(int submissionId) {

            var user = _userMapper.Map(User.Claims);

            Log.Information("STFM user " + user.STFMUserId + " has started the complete disclosure process.");

            Disclosure disclosure = _disclosureService.GetDisclosure(user.STFMUserId);

            if ( disclosure == null) {

                return RedirectToAction("Index", "Disclosure", new { submissionId = submissionId });

            } else {


                return RedirectToAction("EditDisclosure", "Disclosure", new { submissionId = submissionId });
            }



        }


        private DisclosureBindingModel GetDisclosureBindingModelFromDisclosure(Disclosure disclosure)
        {

            DisclosureBindingModel disclosureBindingModel = new DisclosureBindingModel
            {
                
                DislosureId = disclosure.DisclosureId,
                StfmUserId = disclosure.StfmUserId,
                Part1Answer = disclosure.Part1Answer,
                Part2Answer = disclosure.Part2Answer,
                Part3Answer = disclosure.Part3Answer,
                Part4Answer = disclosure.Part4Answer,
                Part5Answer = disclosure.Part5Answer,
                Part6Answer = disclosure.Part6Answer

            };

            return disclosureBindingModel;

        }


        private Disclosure GetDisclosureFromDisclosureBindingModel(DisclosureBindingModel disclosureBindingModel)
        {
            string cvFileName = null;

            if (disclosureBindingModel.CvFile != null)
            {
                Submission submission = _submissionService.GetSubmission(disclosureBindingModel.SubmissionId);
                cvFileName = disclosureBindingModel.StfmUserId + "_" + submission.Conference.ConferenceShortName + "_"  + disclosureBindingModel.CvFile.FileName;
            }

            Disclosure disclosure = new Disclosure
            {

                StfmUserId = disclosureBindingModel.StfmUserId,
                DisclosureDate = DateTime.Now,
                FullName = disclosureBindingModel.FullName,
                Part1Answer = disclosureBindingModel.Part1Answer,
                Part2Answer = disclosureBindingModel.Part2Answer,
                Part3Answer = disclosureBindingModel.Part3Answer,
                Part4Answer = disclosureBindingModel.Part4Answer,
                Part5Answer = disclosureBindingModel.Part5Answer,
                Part6Answer = disclosureBindingModel.Part6Answer,
                CvFilename = cvFileName

            };

            if (disclosureBindingModel.DislosureId > 0)
            {

                disclosure.DisclosureId = disclosureBindingModel.DislosureId;

            }

            return disclosure;

        }
    }
}
