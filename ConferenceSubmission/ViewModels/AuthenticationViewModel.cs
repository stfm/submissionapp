﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class AuthenticationViewModel
    {
        public AuthenticationScheme[] AuthenticationSchemes { get; set; }

        public string ReturnUrl { get; set; }
    }
}
