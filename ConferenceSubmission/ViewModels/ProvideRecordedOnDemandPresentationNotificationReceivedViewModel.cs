﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ProvideRecordedOnDemandPresentationNotificationReceivedViewModel
    {
        public int SubmissionId { get; set; }
        public string SubmissionTitle { get; set; }
        public string ConferenceName { get; set; }
        public bool WillProvideRecordedOnDemandPresentation { get; set; }
        public string SubmissionCategory { get; set; }
    }
}
