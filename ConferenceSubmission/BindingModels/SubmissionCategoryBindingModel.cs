﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Stores conference and submission categories
    /// so user can select specific submission
    /// category for her submission.
    /// </summary>
    public class SubmissionCategoryBindingModel
    {

        [Required]
        public int ConferenceId { get; set; }

        public string ConferenceName { get; set; }


        public IEnumerable<SelectListItem> SubmissionCategoryItems { get; set; }

        [Required]
        [Display(Name = "Select the Submission Category ")]
        public int SelectedSubmissionCategory { get; set; }


        public string SubmissionPaymentRequirementText { get; set; }

        public bool DisplaySubmissionRequirementText { get; set; }

        public string CategoryDetailUrl { get; set; }


        public override string ToString()
        {

            return "ConferenceId: " + ConferenceId + " | SubmissionCategory: " +
                SelectedSubmissionCategory;


        }

    }
}
