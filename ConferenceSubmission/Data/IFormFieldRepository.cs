﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface IFormFieldRepository
    {
        /// <summary>
        /// Create new FormField in repository
        /// </summary>
        /// <param name="formField">FormField to create</param>
        /// <returns>FormFieldId for new record</returns>
        int CreateFormField(FormField formField);

        /// <summary>
        /// Get all the FormField objects associated with provided
        /// submission category ID and form field role (e.g. proposal or review)
        /// </summary>
        /// <returns>The form fields.</returns>
        /// <param name="submissionCategoryId">Submission category identifier.</param>
        /// <param name="formFieldRole">Form Field Role (e.g. proposal or review)</param>
        IEnumerable<FormField> GetFormFields(int submissionCategoryId, string formFieldRole);


        /// <summary>
        /// Gets the form fields and their values for submission.  This includes
        /// the submitters answers to the proposal questions for the submission
        /// category.
        /// </summary>
        /// <returns>The form fields for submission.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        IEnumerable<SubmissionFormField> GetFormFieldsForSubmission(int submissionId);

        /// <summary>
        /// Deletes all SubmissionFormFieds for a submission.
        /// </summary>
        /// <param name="submissionId">submission's unique identifier</param>
        /// <returns></returns>
        int RemoveAllFormFieldsForSubmission(int submissionId);

        /// <summary>
        /// Gets the form field answer for submission.
        /// </summary>
        /// <returns>The form field answer for submission.</returns>
        /// <param name="formFieldId">Form field identifier.</param>
        /// <param name="submissionId">Submission identifier.</param>
        string GetFormFieldAnswerForSubmission(int formFieldId, int submissionId);

        /// <summary>
        /// Adds the submission form field - this is the user's answer to
		/// a submission proposal question.
        /// </summary>
        /// <param name="submissionFormField">Submission form field.</param>
		void AddSubmissionFormField(SubmissionFormField submissionFormField);


        /// <summary>
        /// Updates the SubmissionFormFieldValue value for the SubmissionFormField whose
        /// SubmissionFormFieldId matches the paramater submissionFormFieldId.
        /// </summary>
        /// <param name="submissionFormFieldId">Unique identfier of the SubmissionFormField</param>
        /// <param name="value">The value that will be used to update the SubmissionFormFieldValue</param>
        void UpdateSubmissionFormField(int submissionFormFieldId, string value);


        /// <summary>
        /// Gets the form fields where answer required is true.
        /// </summary>
        /// <returns>The form fields where answer required is true.</returns>
        /// <param name="submissionCategoryId">Submission category identifier.</param>
        /// <param name="formFieldRole">Form field role.</param>
        IEnumerable<FormField> GetFormFieldsAnswerRequired(int submissionCategoryId, string formFieldRole);

        /// <summary>
        /// Get the FormField using the provided form field id
        /// </summary>
        /// <param name="formFieldId">FormFieldId</param>
        /// <returns>FormField</returns>
        FormField GetFormField(int formFieldId);

    }
}
