﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public class SubmissionStatusService : ISubmissionStatusService
    {
        private readonly ISubmissionStatusRepository _submissionStatusRepository;

        public SubmissionStatusService(ISubmissionStatusRepository submissionStatusRepository)
        {
            _submissionStatusRepository = submissionStatusRepository;
        }

        public SubmissionStatus GetBySubmissionStatusType(SubmissionStatusType submissionStatusType)
        {
            return _submissionStatusRepository.GetBySubmissionStatusType(submissionStatusType);
        }
    }
}
