﻿$(document).ready(function () {
    $("#participantsAddedOrRemovedTable").DataTable({
        buttons: [{ extend: 'csv', text: 'Save To CSV File', exportOptions: { modifier: { search: 'none' } } }],
        dom: 'lBfrtip',
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'pageLength': 25,
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: ''
            }
        },
        stateSave: true,
        "columns": [
            { "data": "Session", "targets": 0, "defaultContent": "<i>Value not available</i>" },
            { "data": "Title", "targets": 1, "defaultContent": "<i>Value not available</i>" },
            { "data": "Participants", "targets": 2, "defaultContent": "<i>Value not available</i>" },
            { "data": "SubmissionId", "targets": 3, "defaultContent": "<i>Value not available</i>" }
        ]
    });
});

var loadReportForConference = function () {
    let conferenceId = $("#conferenceSelect").val();
    let asOfDate = $("#asOfDateInput").val();
    if (!asOfDate || asOfDate === '' || !conferenceId || conferenceId === "") {
        alert("Please select a Conference and an As of Date");
        return;
    }

    let data = {
        conferenceId: conferenceId,
        asOfDate: asOfDate
    };

    $.ajax({
        data: data,
        method: "GET",
        url: "/SubmissionAdmin/ParticipantsAddedOrRemovedReportData",
        dataType: "json"
    }).done(function (data) {
        loadDataIntoDatatable(data.submissionsWithParticipantsAddedOrRemoved);
        $("#participantsAddedOrRemovedTableContainer").show();
        toggleInputDisplay();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("We're sorry, the report data could not be loaded at this time.");
    });
};

var loadDataIntoDatatable = function (submissions) {
    let tableRows = [];
    let table = $('#participantsAddedOrRemovedTable').DataTable();
    table.clear();

    submissions.forEach(function (s) {
        tableRows.push({
            "Session": "<p>" + s.sessionCode + "</p>",
            "Title": "<p>" + s.submissionTitle + "</p>",
            "Participants": getParticipantsAsList(s.participantsAddedOrRemoved),
            "SubmissionId": "<p>" + s.submissionId + "</p>"
        });
    });

    table.rows.add(tableRows).draw();
};

var getParticipantsAsList = function (participants) {
    var result = "<ul class='pad-0'>";
    participants.forEach(function (p) {
        result += `<li class='list-no-bullets'>${p.participantName}: <strong class='text-primary'>${p.addedOrRemoved === 0 ? "ADDED" : "REMOVED"}</strong> on ${p.dateAddedOrRemovedAsShortDateString}</li>`;
    });
    result += "</u>";
    return result;
};

var toggleInputDisplay = function () {
    if ($('#inputsContainer').is(':visible')) {
        $("#inputsContainer").hide(75);
        $("#conferencSelectionContainer").show();
    }
    else {
        $("#inputsContainer").show(75);
        $("#conferencSelectionContainer").hide();
    }
}