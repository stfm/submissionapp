﻿using System;
using ConferenceSubmission.Models;
using System.Linq;

namespace ConferenceSubmission.Data
{
    public class ReviewStatusRepository : IReviewStatusRepository
    {

        private readonly AppDbContext _appDbContext;

        public ReviewStatusRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public ReviewStatus GetReviewStatusByReviewStatusType(ReviewStatusType reviewStatusType)
        {
            return _appDbContext.ReviewStatuses.FirstOrDefault(rs => rs.ReviewStatusType == reviewStatusType);
        }
    }
}
