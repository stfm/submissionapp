﻿using ConferenceSubmission.Mappers;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using DinkToPdf;
using DinkToPdf.Contracts;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Blobs;
using ConferenceSubmission.Migrations;

namespace ConferenceSubmission.Controllers
{
    [Authorize("ConferenceAdmin")]
    public class ReviewAdminController : Controller
    {

        private readonly ISubmissionService _submissionService;
        private readonly IReviewerService _reviewerService;
        private readonly IReviewService _reviewService;
        private readonly IReviewAdminService _reviewAdminService;
        private readonly ISubmissionCategoryService _submissionCategoryService;
        private readonly IConverter _converter;
        private readonly ISubmissionPrintService _submissionPrintService;
        private readonly IConferenceService _conferenceService;
        private IConfiguration _configuration;

        public ReviewAdminController(IReviewerService reviewerService, 
                                     ISubmissionService submissionService,
                                     IReviewService reviewService,
                                     IReviewAdminService reviewAdminService,
                                     ISubmissionCategoryService submissionCategoryService,
                                     IConverter converter,
                                     ISubmissionPrintService submissionPrintService,
                                     IConferenceService conferenceService,
                                     IConfiguration configuraton
                                     )
        {
            _submissionService = submissionService;

            _reviewService = reviewService;

            _reviewerService = reviewerService;

            _reviewAdminService = reviewAdminService;

            _submissionCategoryService = submissionCategoryService;

            _converter = converter;

            _submissionPrintService = submissionPrintService;

            _conferenceService = conferenceService;

            _configuration = configuraton;

        }


        public ActionResult Index()
        {
            List<Conference> conferences =_conferenceService.GetConferencesWithUnstartedStatus();

            var model = new ReviewAdminHomePageViewModel()
            {
                conferences = conferences

            };

            return View(model);

        }

        public ActionResult SubmissionCategoryReviewIndex(int conferenceId)
        {

            List<SubmissionCategoryViewModel> categories = _submissionCategoryService.GetActiveSubmissionCategories(conferenceId);

            Conference conference = _conferenceService.GetConference(conferenceId);

            var model = new SubmissionCategoryReviewIndexViewModel()
            {
                submissionCategoryViewModels = categories,
                conference = conference

            };

            return View(model);


        }

        /// <summary>
        /// Responds to user's request to view the average review
        /// score for each submission in the provided category.
        /// </summary>
        /// <returns>View with submissions and their average review score</returns>
        /// <param name="categoryId">Category identifier.</param>
        public ActionResult SubmissionCategoryAverageReviewScores(int categoryId)
        {

            List<SubmissionsAverageReviewScore> submissionsAverageReviews = _reviewAdminService.GetCompletedReviewsForCategoryWithAverageReviewScore(categoryId);

            SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(categoryId);

            var model = new SubmissionsAverageReviewScoreViewModel()
            {

                SubmissionsAverageReviews = submissionsAverageReviews,

                ConferenceLongName = submissionCategory.Conference.ConferenceLongName,

                SubmissionCategoryName = submissionCategory.SubmissionCategoryName,

                CategoryId = categoryId


            };

            return View(model);
        }


        public ActionResult UsersWithMoreThenTwoSubmissions(int conferenceId)
        {


            List<UserMultipleSubmissionsViewModel> userMultipleSubmissionsViewModels = _reviewAdminService.GetUsersWithMoreThenTwoSubmissions(conferenceId, 1);


            Conference conference = _conferenceService.GetConference(conferenceId);

            var model = new ConferenceUserMultipleSubmissionsViewModel
            {

                
                Conference = conference,

                UserMultipleSubmissionsViewModels = userMultipleSubmissionsViewModels


            };

            return View(model);

        }


        /// <summary>
        /// Responds to user request to get the detailed reviews of all
        /// submissions in the provided category.
        /// </summary>
        /// <returns>View page showing detailed reviews</returns>
        /// <param name="categoryId">Category identifier.</param>
        public ActionResult SubmissionCategoryDetailedReviews(int categoryId)
        {

            List<SubmissionDetailedReview> submissionDetailedReviews = _reviewAdminService.GetSubmissionDetailedReviews(categoryId);

            SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(categoryId);


            var model = new SubmissionsDetailedReviewsViewModel()
            {

                ConferenceLongName = submissionCategory.Conference.ConferenceLongName,

                SubmissionCategoryName = submissionCategory.SubmissionCategoryName,

                SubmissionDetailedReviews = submissionDetailedReviews,

                SubmissionCategoryId = submissionCategory.SubmissionCategoryId



            };

            return View(model);

        }

        public ActionResult SubmissionDetailedReviews(int submissionId, int categoryId)
        {

            List<SubmissionDetailedReview> submissionDetailedReviews = _reviewAdminService.GetSubmissionDetailedReviewsForSubmission(submissionId);

            SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(categoryId);


            var model = new SubmissionsDetailedReviewsViewModel()
            {

                ConferenceLongName = submissionCategory.Conference.ConferenceLongName,

                SubmissionCategoryName = submissionCategory.SubmissionCategoryName,

                SubmissionDetailedReviews = submissionDetailedReviews,

                SubmissionCategoryId = submissionCategory.SubmissionCategoryId



            };

            return View(model);

        }

        public ActionResult SubmissionDetailedReviewsNoCategory(int submissionId)
        {

            List<SubmissionDetailedReview> submissionDetailedReviews = _reviewAdminService.GetSubmissionDetailedReviewsForSubmission(submissionId);

            SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(submissionDetailedReviews[0].SubmissionCompletedReviews[0].Submission.SubmissionCategoryId);


            var model = new SubmissionsDetailedReviewsViewModel()
            {

                ConferenceLongName = submissionCategory.Conference.ConferenceLongName,

                SubmissionCategoryName = submissionCategory.SubmissionCategoryName,

                SubmissionDetailedReviews = submissionDetailedReviews,

                SubmissionCategoryId = submissionCategory.SubmissionCategoryId



            };

            return View("SubmissionDetailedReviews", model);

        }

        public ActionResult ReviewersNotDone(int conferenceId)
        {

            List<ReviewIncomplete> reviewersNotDone = null;
            try
            {
               reviewersNotDone = _reviewAdminService.GetReviewersNotDone(conferenceId);

            } catch (Exception ex)
            {

                Log.Error("Unable to get reviewers not done: " + ex.Message);
            }

            var model = new ReviewIncompleteViewModel()
            {

                ConferenceLongName = _conferenceService.GetConference(conferenceId).ConferenceLongName,
                reviewIncomplete = reviewersNotDone

            };

            return View(model);

        }

        public ActionResult ReviewersStatus(int conferenceId)
        {

            List<ReviewerStatus> reviewers = null;

            try
            {

                reviewers = _reviewAdminService.GetReviewersStatus(conferenceId);

            } catch (Exception ex)
            {

                Log.Error("Unable to get reviewers status: " + ex.Message);
            }


            var model = new ReviewersStatusViewModel()
            {
                ConferenceLongName = _conferenceService.GetConference(conferenceId).ConferenceLongName,
                ConferenceId = conferenceId,
                reviewerStatuses = reviewers

            };

        return View(model);


        }

        public ActionResult SubmissionsReviewStatus(int conferenceId)
        {

            List<SubmissionReviewStatus> subissionsReviewStatus = _reviewAdminService.GetSubmissionsReviewStatus(conferenceId);

            var model = new SubmissionsReviewStatusViewModel()
            {
                ConferenceLongName = _conferenceService.GetConference(conferenceId).ConferenceLongName,
                ConferenceId = conferenceId,
                SubmissionsReviewStatus = subissionsReviewStatus
            };

            return View(model);


        }

        public ActionResult SubmissionReviewersNotDone(int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);

            Conference conference = submission.Conference;

            List<SubmissionReviewer> submissionReviewersNotDone = new List<SubmissionReviewer>(); 

            List<SubmissionReviewer> submissionReviewers = _reviewAdminService.GetAssignedReviewers(submissionId);

            foreach(SubmissionReviewer submissionReviewer in submissionReviewers)
            {
                bool isReviewComplete = _reviewAdminService.GetReviewCompleted(submissionReviewer.ReviewerId, submissionId);

                if ( ! isReviewComplete)
                {
                    submissionReviewersNotDone.Add(submissionReviewer);
                }

            }

            var model = new SubmissionReviewersNotDone
            {
                ConferenceLongName = conference.ConferenceLongName,
                Submission = submission,
                submissionReviewersNotDone = submissionReviewersNotDone,
            };

            return View(model);

        }

        /// <summary>
        /// View all reviewers.
        /// </summary>
        /// <returns>view page showing all reviewers</returns>
        public ActionResult ViewAllReviewers()
        {

            List<Reviewer> reviewers = _reviewerService.GetReviewers();

            return View(reviewers);
        }

        /// <summary>
        /// View a summary of reviewer data for each conference
        /// reviewer assigned to review submissions.
        /// </summary
        /// <param name="stfmUserId">STFM User Id</param>
        /// <returns>view of conferences reviewer is assigned to</returns>
        public ActionResult ViewReviewerSummary(string stfmUserId, string successMessage)
        {
           List<ReviewerSummaryForConference> reviewerSummaryForConferences = _reviewerService.GetReviewerSummaryForConferences(stfmUserId);

            Reviewer reviewer = _reviewerService.GetReviewer(stfmUserId);

            ReviewerSummaryAllConferences reviewerSummaryAllConferences = new ReviewerSummaryAllConferences()
            {
                Reviewer = reviewer,
                ReviewerSummaryForConferences = reviewerSummaryForConferences,
                SuccessMessage = successMessage

            };

           return View(reviewerSummaryAllConferences);

        }

        /// <summary>
        /// View all submissions reviewer is assigned to for a specific 
        /// conference.
        /// </summary>
        /// <param name="stfmUserId">STFM User Id of reviewer</param>
        /// <param name="conferenceId">Conference ID of conference</param>
        /// <returns>view of all submissions in the conference reviewer is assigned to review</returns>
        public ActionResult ViewReviewerAssignments(string stfmUserId, int conferenceId)
        {

            Reviewer reviewer = _reviewerService.GetReviewer(stfmUserId);

            Conference conference = _conferenceService.GetConference(conferenceId);

            List<AssignedSubmissionViewModel> submissionsAssigned = 
                _reviewerService.GetAssignedSubmissionWithReviewStatus(stfmUserId, conferenceId);


            ReviewerAssignmentsViewModel reviewerAssignmentsViewModel = new ReviewerAssignmentsViewModel()
            {

                Reviewer = reviewer,
                Conference = conference,
                SubmissionsAssigned = submissionsAssigned
            };

            return View(reviewerAssignmentsViewModel);
        }

        /// <summary>
        /// Find people by last name in order to add one 
        /// as a new reviewer.
        /// </summary>
        /// <returns>view page showing all people from SalesForce with
        /// matching last name.</returns>
        public ActionResult FindPeopleByLastName(string lastName)
        {
               
            List<PotentialReviewer> people = _reviewAdminService.AddNewReviewerFindPeople(lastName);

            return View(people);
        }

        /// <summary>
        /// Get the accepted submissions not assigned to a reviewer or
        /// not reviewed by any assigned reviewers.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns></returns>
        public IActionResult SubmissionsNotReviewed(int conferenceId)
        {

            List<Submission> submissionsNotAssigned = _reviewAdminService.GetSubmissionsNotAssignedToReviewer(conferenceId);

            List<Submission> submissionsNoReviewCompleted = _reviewAdminService.GetSubmissionsNoReviewsCompleted(conferenceId);

            Conference conference = _conferenceService.GetConference(conferenceId);

            SubmissionsNotReviewedViewModel submissionsNotReviewedViewModel = new SubmissionsNotReviewedViewModel()
            {

                SubmissionsNotAssignedToReviewer = submissionsNotAssigned,
                SubmissionsNoReviewedCompleted = submissionsNoReviewCompleted,
                Conference = conference

            };

            return View(submissionsNotReviewedViewModel);
        }

        /// <summary>
        /// Get the collection of reviewers assigned to a submission
        /// and return view showing that information
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns></returns>
        public IActionResult ReviewersAssignedToSubmission(int submissionId)
        {

            List<SubmissionReviewer> _reviewers = _reviewAdminService.GetAssignedReviewers(submissionId);

            Submission submission = _submissionService.GetSubmission(submissionId);

            Conference conference = submission.Conference;

            ReviewersAssignedToSubmissionViewModel reviewersAssignedToSubmissionViewModel = new ReviewersAssignedToSubmissionViewModel()
            {
                reviewers = _reviewers,
                Conference = conference,
                Submission = submission
            };

            return View(reviewersAssignedToSubmissionViewModel);
        }

        /// <summary>
        /// Assign the reviewer to each submission in the 
        /// space delimited string conferenceIds
        /// </summary>
        /// <param name="reviewerId"></param>
        /// <param name="stfmUserId"></param>
        /// <param name="submissionIds"></param>
        /// <returns></returns>
        public RedirectToActionResult AssignReviewerToSubmissions(string reviewerId, string stfmUserId, string submissionIds)
        {

            List<string> submissionIdList = submissionIds.Split(' ').ToList();

            _reviewerService.AssignReviewerToSubmissions(reviewerId, submissionIdList);

            string successMessage = "Successfully assigned reviewer to submissions.";

            return RedirectToAction("ViewReviewerSummary", "ReviewAdmin", routeValues: new { stfmUserId = stfmUserId, successMessage = successMessage });

        }

        /// <summary>
        /// Assign the reviewer to each submission in the 
        /// space delimited string conferenceIds
        /// </summary>
        /// <param name="reviewerId"></param>
        /// <param name="stfmUserId"></param>
        /// <param name="submissionIds"></param>
        /// <returns></returns>
        public RedirectToActionResult UnassignReviewerFromSubmissions(string reviewerId, string stfmUserId, string submissionIds)
        {

            List<string> submissionIdList = submissionIds.Split(' ').ToList();

            _reviewerService.UnassignReviewerFromSubmissions(reviewerId, submissionIdList);

            string successMessage = "Successfully unassigned reviewer from submissions.";

            return RedirectToAction("ViewReviewerSummary", "ReviewAdmin", routeValues: new { stfmUserId = stfmUserId, successMessage = successMessage });

        }

        /// <summary>
        /// Retire the conference identified by the conference ID provided.
        /// </summary>
        /// <param name="conferenceId">conference ID</param>
        /// <returns>Redirects to SubmissionAdmin home page</returns>
        public RedirectToActionResult CreateReviewer(string stfmUserId, string lastName)
        {

            _reviewerService.CreateReviewer(new Reviewer { StfmUserId = stfmUserId });

            return RedirectToAction("FindPeopleByLastName", "ReviewAdmin", routeValues: new { lastName = lastName });
        }

        /// <summary>
        /// Downloads a pdf version of the submission category detailed reviews to the user's browser. 
        /// Does not redirect the user.
        /// </summary>
        /// <param name="categoryId">Unique identifier of submission category</param>
        /// <returns>Pdf File</returns>
        public IActionResult PrintPdf(int categoryId)
        {
            try
            {

                List<SubmissionDetailedReview> submissionDetailedReviews = _reviewAdminService.GetSubmissionDetailedReviews(categoryId);

                SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(categoryId);


                var model = new SubmissionsDetailedReviewsPrintViewModel()
                {

                    ConferenceLongName = submissionCategory.Conference.ConferenceLongName,

                    SubmissionCategoryName = submissionCategory.SubmissionCategoryName,

                    SubmissionDetailedReviews = submissionDetailedReviews



                };

                return File(
                    GetPdfAsByteArray(model),
                    "application/pdf",
                    model.GetFileName());
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Server errored while attempting to print detailed review report for {categoryId}.");
            }

            return RedirectToAction("Error", "Home");

        }

        public IActionResult PrintPdfSingleSubmission(int submissionId, int categoryId)
        {
            try
            {
                Log.Information("Getting detailed reviews for submission " + submissionId + " for category id " + categoryId);

                List<SubmissionDetailedReview> submissionDetailedReviews = _reviewAdminService.GetSubmissionDetailedReviewsForSubmission(submissionId);

                SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(categoryId);


                var model = new SubmissionsDetailedReviewsPrintViewModel()
                {

                    ConferenceLongName = submissionCategory.Conference.ConferenceLongName,

                    SubmissionCategoryName = submissionCategory.SubmissionCategoryName,

                    SubmissionDetailedReviews = submissionDetailedReviews



                };

                return File(
                    GetPdfAsByteArray(model),
                    "application/pdf",
                    model.GetSingleSubmissionFileName());
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Server errored while attempting to print detailed review report for single submission {submissionId}.");
            }

            return RedirectToAction("Error", "Home");

        }

        public IActionResult UploadReviewerAssignments()
        {

                return View("AssignReviewers");

        }

        [HttpPost]
        public async Task<IActionResult> UploadReviewerAssignments(IFormFile assignmentsFile)

        {
           

            Log.Information($"User is uploading reviewer assignments file " +
                $"assignments filename {assignmentsFile.FileName} ");

            if (assignmentsFile is null)
            {

                ViewData["Error"] = "Assignment File Not Provided";

                return View("AssignReviewers");

            }



            string[] assignmentsFilePermittedExtensions = { ".csv" };

            string ext = Path.GetExtension(assignmentsFile.FileName).ToLowerInvariant();

            if (string.IsNullOrEmpty(ext) || !assignmentsFilePermittedExtensions.Contains(ext))
            {

                ViewData["Error"] = "Only Reviewer Assignments File of Type .csv Are Allowed";

                return View("AssignReviewers");
            }


            string blobstorageconnection = _configuration["ReviewerAssignmentsStorage:key"];

            string blobContainer = "submissionreviewers";

            string assignmentsFileName = assignmentsFile.FileName;

            BlobContainerClient blobContainerClient = new BlobContainerClient(blobstorageconnection, blobContainer);

            var blob = blobContainerClient.GetBlobClient(assignmentsFileName);

            Azure.Response<BlobContentInfo> response = await blob.UploadAsync(assignmentsFile.OpenReadStream());

            ViewData["Success"] = "Successfully uploaded reviewer assignments file.  It will be processed within the next 30 minutes after that you can check reviewer assignments.";

            return View("AssignReviewers");

        }

        public IActionResult UploadReviewers()
        {

            return View("UploadReviewers");

        }

        [HttpPost]
        public async Task<IActionResult> UploadReviewers(IFormFile newReviewers)

        {
            if (newReviewers is null)
            {

                ViewData["Error"] = "New Reviewers File Not Provided";

                return View("UploadReviewers");

            }

            Log.Information($"User is uploading new reviewers file " +
                $"filename {newReviewers.FileName} ");


            string[] reviewersFilePermittedExtensions = { ".csv" };

            string ext = Path.GetExtension(newReviewers.FileName).ToLowerInvariant();

            if (string.IsNullOrEmpty(ext) || !reviewersFilePermittedExtensions.Contains(ext))
            {

                ViewData["Error"] = "Only New Reviewers File of Type .csv Are Allowed";

                return View("UploadReviewers");
            }


            string blobstorageconnection = _configuration["ReviewerAssignmentsStorage:key"];

            string blobContainer = "addreviewers";

            string newReviewersFileName = newReviewers.FileName;

            BlobContainerClient blobContainerClient = new BlobContainerClient(blobstorageconnection, blobContainer);

            var blob = blobContainerClient.GetBlobClient(newReviewersFileName);

            Azure.Response<BlobContentInfo> response = await blob.UploadAsync(newReviewers.OpenReadStream());

            ViewData["Success"] = "Successfully uploaded new reviewers file.  It will be processed within the next 30 minutes after that you can check that new reviewers were added.";

            return View("UploadReviewers");

        }


        private byte[] GetPdfAsByteArray(SubmissionsDetailedReviewsPrintViewModel submissionDetailedReviewsPrintViewModel)
        {
            return _converter.Convert(new HtmlToPdfDocument
            {
                Objects =
                {
                    new ObjectSettings()
                    {
                        HtmlContent = _submissionPrintService.RenderViewToString(submissionDetailedReviewsPrintViewModel, "/Views/ReviewAdmin/SubmissionCategoryDetailedReviewsPrint.cshtml")
                    }
                }
            });
        }

    }


}
