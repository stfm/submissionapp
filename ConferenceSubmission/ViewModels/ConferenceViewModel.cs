﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// Represents a conference where presentations will be presented.
    /// </summary>
    public class ConferenceViewModel
    {

        public int ConferenceId { get; set; }

        public string ConferenceShortName { get; set; }

        public String ConferenceLongName { get; set; }

        public DateTime ConferenceCFPEndDate { get; set; }

        public DateTime ConferenceSubmissionReviewEndDate { get; set; }

        public bool ConferenceInactive { get; set; }

        public bool ConferenceVirtual { get; set;  }

        public bool ConferenceHybrid { get; set; }

    }
}
