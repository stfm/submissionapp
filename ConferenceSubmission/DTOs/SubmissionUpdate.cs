﻿namespace ConferenceSubmission.DTOs
{
    /// <summary>
    /// Models data for updating a submission after Staff Admin user
    /// edits it.
    /// </summary>
    public class SubmissionUpdate
    {
        public int SubmissionId { get; set; }
        public string SubmissionTitle { get; set; }

        public string SubmissionAbstract { get; set; }

        public int SubmissionCategoryId { get; set; }

        public int AcceptedCategoryId { get; set; }

        public int ConferenceId { get; set; } 

        public int? LearningObjOneFormFieldId { get; set; }

        public string LearningObjOneValue { get; set; }

        public int? LearningObjTwoFormFieldId { get; set; }

        public string LearningObjTwoValue { get; set; }

        public int? LearningObjThreeFormFieldId { get; set; }

        public string LearningObjThreeValue { get; set; }

        public bool SubmissionAdminEdited { get; set; }

    }
}