﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs
{
    /// <summary>
    /// This class contains the members necessary to form a URL request
    /// to the Salesforce REST API
    /// </summary>
    public class SalesforceAPIVariables
    {
        public string OAuthToken { get; set; }
        public string ServiceUrl { get; set; }
    }
}
