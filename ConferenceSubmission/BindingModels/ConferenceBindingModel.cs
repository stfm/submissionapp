﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Stores information about a conference so user
    /// can select specific conference.
    /// </summary>
    public class ConferenceBindingModel
    {
        
        public IEnumerable<SelectListItem> ConferenceItems { get; set; }

        [Required]
        [Display(Name = "Select the conference ")]
        public int conferenceId { get; set; }


    }
}
