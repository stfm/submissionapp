﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;
using AutoMapper;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmissionTest
{
    public class SubmissionCategoryServiceTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public SubmissionCategoryServiceTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }

        [Fact]
        public void GetSubmissionCategoriesTest()
        {
            // Arrange
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ConferenceSubmissionMappingProfile());
            });
        
            var mapper = config.CreateMapper();
            var repo = new SubmissionCategoryRepositoryForTesting();
            var sut = new SubmissionCategoryService(mapper, repo);

            // Act
            var result = sut.GetSubmissionCategories(ConferenceId: 1);

            Output.WriteLine("Number of submission categories found is " + result.Count());

            foreach (SubmissionCategoryViewModel submissionCategoryViewModel in result)
            {

                Output.WriteLine("Submission category name is " + submissionCategoryViewModel.SubmissionCategoryName);

            }

            Assert.Equal(2, result.Count());


        }


        [Fact]
        public void GetActiveSubmissionCategoriesTest()
        {
            // Arrange
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ConferenceSubmissionMappingProfile());
            });

            var mapper = config.CreateMapper();
            var repo = new SubmissionCategoryRepositoryForTesting();
            var sut = new SubmissionCategoryService(mapper, repo);

            // Act
            var result = sut.GetActiveSubmissionCategories(ConferenceId: 1);

            Output.WriteLine("Number of submission categories found is " + result.Count());

            foreach (SubmissionCategoryViewModel submissionCategoryViewModel in result)
            {

                Output.WriteLine("Submission category name is " + submissionCategoryViewModel.SubmissionCategoryName);

            }

            Assert.True(result.Count() == 1);


        }

        [Fact]
        public void GetSubmissionCategoryTest() {

            // Arrange
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ConferenceSubmissionMappingProfile());
            });

            var mapper = config.CreateMapper();
            var repo = new SubmissionCategoryRepositoryForTesting();
            var sut = new SubmissionCategoryService(mapper, repo);

            // Act
            SubmissionCategory submissionCategory = sut.GetSubmissionCategory(2);

            Output.WriteLine("Submission Category is " + submissionCategory.SubmissionCategoryName);

            Output.WriteLine("Submission category payment requirement is " + submissionCategory.SubmissionPaymentRequirement);

            Assert.Equal("Lecture", submissionCategory.SubmissionCategoryName);


        }


    }
}
