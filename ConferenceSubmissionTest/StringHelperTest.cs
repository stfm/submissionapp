﻿using Xunit;
using ConferenceSubmission.Extensions;

namespace ConferenceSubmissionTest
{
    public class StringHelperTest
    {
        [Fact]
        public void ReplaceLineBreaksWithBreakTags_ReplacesTheRightNumberOfLineBreaks()
        {
            //arrange
            var textWithSixLineBreaks = @"Lorem ipsum dolor sit amet, 
                                          consectetur adipiscing elit. 
                                          Duis ex diam, molestie ut dictum sed, 
                                          fringilla quis velit. 
                                          Pellentesque in placerat ex. 
                                          Integer suscipit nec libero non volutpat. 
                                          Nullam in justo est.";

            //act
            var textWithLineBreaksReplaced = textWithSixLineBreaks.ReplaceLineBreaksWithBreakTags();

            //assert
            var numberOfBrTagsFound = textWithLineBreaksReplaced.Split("<br>").Length-1;
            Assert.True(numberOfBrTagsFound == 6);

        }

        [Fact]
        public void ReplaceLineBreaksWithBreakTags_ReplacesMultipleLineBreaks()
        {
            //arrange
            var textWithTwelveLineBreaks = @"Lorem ipsum dolor sit amet, 

                                          consectetur adipiscing elit. 

                                          Duis ex diam, molestie ut dictum sed, 

                                          fringilla quis velit. 

                                          Pellentesque in placerat ex. 

                                          Integer suscipit nec libero non volutpat. 

                                          Nullam in justo est.";

            //act
            var textWithLineBreaksReplaced = textWithTwelveLineBreaks.ReplaceLineBreaksWithBreakTags();

            //assert
            var numberOfBrTagsFound = textWithLineBreaksReplaced.Split("<br>").Length - 1;
            Assert.True(numberOfBrTagsFound == 12);

        }

        [Fact]
        public void ReplaceLineBreaksWithTags_ReplacesMultipleCarriageReturns()
        {
            //arrange
            var textWith24CarriageReturns = @"There are many variations of passages of Lorem Ipsum available,                                                     but the majority have suffered alteration in some form, by injected humour,                                                     or randomised words which don't look even slightly believable.                                                    If you are going to use a passage of Lorem Ipsum, you need to be sure                                                     there isn't anything embarrassing hidden in the middle of text. All the Lorem                                                     Ipsum generators on the Internet tend to repeat predefined chunks as necessary,                                                     making this the first true generator on the Internet.";
            //act
            var textWithCarriageReturnsReplaceWithBrTags = textWith24CarriageReturns.ReplaceLineBreaksWithBreakTags();
            var numberOfBrTags = textWithCarriageReturnsReplaceWithBrTags.Split("<br>").Length - 1;

            //assert
            Assert.True(numberOfBrTags == 24);
        }

        [Fact]
        public void CapitalizeFirstLetter_WorksOnLowerCaseWord()
        {
            //arrange
            var lowerCaseWord = "lowercaseword";

            //act
            var result = lowerCaseWord.CapitalizeFirstLetter();

            //asert
            Assert.True(result == "Lowercaseword");
        }

        [Fact]
        public void CapitalizeFirstLetter_WorksOnUpperCaseWord()
        {
            //arrange
            var upperCaseWord = "UPPERCASEWORD";

            //act
            var result = upperCaseWord.CapitalizeFirstLetter();

            //asert
            Assert.True(result == "Uppercaseword");
        }

        [Fact]
        public void CapitalizeFirstLetter_WorksOnMixedCaseWord()
        {
            //arrange
            var mixedCaseWord = "MixEdCasEWorD";

            //act
            var result = mixedCaseWord.CapitalizeFirstLetter();

            //asert
            Assert.True(result == "Mixedcaseword");
        }

        [Fact]
        public void CapitalizeFirstLetter_WorksWithNumbers()
        {
            //arrange
            var wordWithNumbers = "wordWITHNumbers989";

            //act
            var result = wordWithNumbers.CapitalizeFirstLetter();

            //asert
            Assert.True(result == "Wordwithnumbers989");
        }


        [Fact]
        public void CapitalizeFirstLetter_WorksWithSpecialChars()
        {
            //arrange
            var wordWithSpecialChars = "wordwithspecialchars#$%*";

            //act
            var result = wordWithSpecialChars.CapitalizeFirstLetter();

            //asert
            Assert.True(result == "Wordwithspecialchars#$%*");
        }
    }

}
