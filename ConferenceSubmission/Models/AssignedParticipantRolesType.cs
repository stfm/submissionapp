﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public enum AssignedParticipantRolesType
    {
        SUBMITTER_ONLY,
        SUBMITTER_AND_LEADPRESENTER,
        SUBMITTER_AND_PRESENTER,
        LEADPRESENTER_ONLY,
        PRESENTER_ONLY
    }
}
