﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public class AnnouncementService : IAnnouncementService
    {
        private readonly IAnnouncementRepository _announcementRepository;
        public AnnouncementService(IAnnouncementRepository announcementRepository)
        {
            _announcementRepository = announcementRepository;
        }

        public int Create(Announcement announcement)
        {
            return _announcementRepository.Create(announcement);
        }

        public void Delete(int announcementId)
        {
            _announcementRepository.Delete(announcementId);
        }

        public Announcement Get(int id)
        {
            return _announcementRepository.Get(id);
        }

        public List<Announcement> GetAll()
        {
            return _announcementRepository.GetAll();
        }

        public List<Announcement> GetCeraAnnouncements()
        {
            return _announcementRepository.GetAllCera();
        }

        public void Update(Announcement announcement)
        {
            _announcementRepository.Update(announcement);
        }
    }

    public interface IAnnouncementService
    {
        Announcement Get(int id);
        List<Announcement> GetAll();
        int Create(Announcement announcement);
        void Update(Announcement announcement);
        void Delete(int announcementId);
        
        /// <summary>
        /// Get collection of Announcements
        /// that are just for CERA
        /// </summary>
        /// <returns></returns>
        List<Announcement> GetCeraAnnouncements();
    }
}
