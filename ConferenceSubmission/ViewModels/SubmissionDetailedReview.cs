﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// Models the detailed review data for
    /// a specific submission.
    /// </summary>
    public class SubmissionDetailedReview
    {

        public int SubmissionId { get; set; }

        public string SubmissionTitle { get; set; }

        public User MainPresenter { get; set; }

        public string SubmissionAbstract { get; set; }

        public Double AverageReviewScore { get; set; }

        public List<Review> SubmissionCompletedReviews { get; set; }

        public SubmissionDetailedReview()
        {

            SubmissionCompletedReviews = new List<Review>();
        }



    }
}
