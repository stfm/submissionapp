﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class parentIdToAuditLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentId",
                table: "AuditLogs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "AuditLogs");
        }
    }
}
