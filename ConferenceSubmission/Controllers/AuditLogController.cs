﻿using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Controllers
{
    public class AuditLogController : Controller
    {
        private readonly IGetAuditLogsService _getAuditLogsService;

        public AuditLogController(IGetAuditLogsService getAuditLogsService)
        {
            _getAuditLogsService = getAuditLogsService;
        }

        [HttpGet]
        public IActionResult GetAuditLogsByEntity(int entityId, EntityType entityType)
        {
            var model = _getAuditLogsService.GetByEntityId(entityId, entityType);

            return PartialView("AuditLogsTable", model);

        }
    }
}
