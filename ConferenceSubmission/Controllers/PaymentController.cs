﻿using System;
using Microsoft.AspNetCore.Mvc;
using ConferenceSubmission.BindingModels;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using ConferenceSubmission.Mappers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using AuthorizeNet.Api.Controllers;
using ConferenceSubmission.ViewModels;
using Microsoft.Extensions.Configuration;
using ConferenceSubmission.Services;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Controllers
{
    [Authorize]
    public class PaymentController : Controller
    {

        private readonly IUserMapper _userMapper;

        private IConfiguration _configuration;

        private ISubmissionPaymentService _submissionPaymentService;

        private ISubmissionService _submissionService;

        private IDisclosureService _disclosureService;

        private readonly IEmailNotificationService _emailNotificationService;

        public PaymentController(IUserMapper userMapper, IConfiguration configuration, 
                                 ISubmissionPaymentService submissionPaymentService,
                                 ISubmissionService submissionService,
                                 IDisclosureService disclosureService,
                                 IEmailNotificationService emailNotificationService)
        {
            _userMapper = userMapper;

            _configuration = configuration;

            _submissionPaymentService = submissionPaymentService;

            _submissionService = submissionService;

            _disclosureService = disclosureService;

            _emailNotificationService = emailNotificationService;

        }

        /// <summary>
        /// Responds to user's initial http request to pay for submitting.
        /// </summary>
        /// <returns>The index.</returns>
        /// <param name="submissionId">Conference identifier.</param>
        public IActionResult Index(int submissionId) {

            /*
             * NOTE: Since fee to submit is the same for all submission categories
             * that require payment the fee value is stored in the application's
             * configuration
             */
            CreditCardPaymentBindingModel creditCardPaymentBindingModel = new CreditCardPaymentBindingModel
            {

                SubmissionId = submissionId,
                PaymentAmount = Decimal.Parse(_configuration["SubmissionPayment:SubmissionFee"])


            };

            return View(creditCardPaymentBindingModel);


        }

        /// <summary>
        /// Responds to user's http post request to pay for submission.
        /// </summary>
        /// <returns>View for successful payment</returns>
        ///<param name="creditCardPaymentBindingModel">Credit card payment binding model</param>
        public IActionResult SubmissionPayment(CreditCardPaymentBindingModel creditCardPaymentBindingModel)
        {

            if (!ModelState.IsValid)
            {

                return View("Index", creditCardPaymentBindingModel);

            }

            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} wants to pay for submission id # {creditCardPaymentBindingModel.SubmissionId}");

            /*
             * If the Production configuration value is false we are using the Authorize.net Test 
             * Sandbox and NOT production Authorize.net - so the credit card will NOT be charged.
             */

            Boolean production = Convert.ToBoolean(_configuration["Authorize.net:Production"]);

            Log.Information($"Application is using Authorize.net setting of production: {production}");

            SubmissionPaymentViewModel submissionPaymentViewModel = 
                _submissionPaymentService.ProcessPaymentNew(_configuration["Authorize.net:APILoginKey"],
                                                         _configuration["Authorize.net:TransactionKey"], 
                                                         production, creditCardPaymentBindingModel);

            if (submissionPaymentViewModel.SuccessfulPayment) {

                /*
                 * We only save successful payments to the database
                 */

                SubmissionPayment submissionPayment = new SubmissionPayment
                {

                    PaymentAmount = (int)creditCardPaymentBindingModel.PaymentAmount,

                    PaymentDateTime = DateTime.Now,

                    PaymentTransactionId = submissionPaymentViewModel.PaymentTransactionId,

                    SubmissionId = creditCardPaymentBindingModel.SubmissionId

                };

                _submissionService.AddSubmissionPayment(submissionPayment);

                submissionPayment.Submission = _submissionService.GetSubmission(submissionPayment.SubmissionId);

                _emailNotificationService.SendSubmissionPaymentEmail(user.Email, submissionPayment);

                Log.Information($"Submission payment email sent to {user.Email}");

            }


            DateTime conferenceStartDate = _submissionService.GetSubmission(creditCardPaymentBindingModel.SubmissionId).Conference.ConferenceStartDate;

            Log.Information("About to check if disclosure is required - conference start date is " + conferenceStartDate + " STFM User id is " + user.STFMUserId);

            if (_disclosureService.GetDisclosure(user.STFMUserId) == null)
            {

                Log.Information("STFM User id " + user.STFMUserId + " does not have a disclosure.  So must complete a new disclosure.");

                submissionPaymentViewModel.ShowDisclosureLink = true;

                submissionPaymentViewModel.DisclosureLinkMethod = "Index";


            } else if (! _disclosureService.isDisclosureCurrent(conferenceStartDate, user.STFMUserId))
            {

                Log.Information("STFM User Id " + user.STFMUserId + " needs to update the disclosure.");

                submissionPaymentViewModel.ShowDisclosureLink = true;

                submissionPaymentViewModel.DisclosureLinkMethod = "EditDisclosure";

            }

            //Redirecting to a new method in order to prevent accidental form resubmission.
            //Passing an anonymous object because the transactionId should not be
            //included in the route values
            return RedirectToAction("SubmissionPaymentProcessed", new
            {
                submissionPaymentViewModel.SuccessfulPayment,
                submissionPaymentViewModel.PaymentResult,
                submissionPaymentViewModel.SubmissionId,
                submissionPaymentViewModel.ShowDisclosureLink,
                submissionPaymentViewModel.DisclosureLinkMethod
            });
        }

        /// <summary>
        /// This method is called internally by the application after it has attempted to process a user's
        /// payment.
        /// </summary>
        /// <param name="submissionPaymentViewModel">A model with results from the payment that was attempted on the user's behalf</param>
        /// <returns>The view that shows the payment results to the user</returns>
        public IActionResult SubmissionPaymentProcessed(SubmissionPaymentViewModel submissionPaymentViewModel)
        {
            var model = new SubmissionPaymentViewModel
            {
                SuccessfulPayment = submissionPaymentViewModel.SuccessfulPayment,
                PaymentResult = submissionPaymentViewModel.PaymentResult,
                SubmissionId = submissionPaymentViewModel.SubmissionId,
                ShowDisclosureLink = submissionPaymentViewModel.ShowDisclosureLink,
                DisclosureLinkMethod = submissionPaymentViewModel.DisclosureLinkMethod
            };
            return View("SubmissionPayment", model);
        }

        /// <summary>
        /// Respond to user's request to view a submission payment.
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns></returns>
        public IActionResult ViewPayment(int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);


            return View("ViewPayment", submission);
        }
    }
}
