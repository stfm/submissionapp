﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using System.Linq;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmissionTest
{
    public class ConferenceRepositoryForTesting : IConferenceRepository
    {
        public ConferenceRepositoryForTesting()
        {
        }

        public IEnumerable<ConferenceType> GetConferenceTypes => throw new NotImplementedException();

        public IEnumerable<Conference> GetConferences => throw new NotImplementedException();

        public IEnumerable<Conference> GetActiveConferences()
        {

            List<Conference> activeConferences = new List<Conference>();

            Conference conference1 = new Conference();

            conference1.ConferenceId = 1;
            conference1.ConferenceDeleted = false;
            conference1.ConferenceEndDate = DateTime.Parse("Nov 15, 2019");
            conference1.ConferenceCFPEndDate = DateTime.Parse("Apr 1, 2019");
            conference1.ConferenceInactive = false;
            conference1.ConferenceLongName = "2019 Conference on Practice Improvement";
            conference1.ConferenceShortName = "CPI19";
            conference1.ConferenceTypeId = 1;
            conference1.ConferenceStartDate = DateTime.Parse("Nov 11, 2019");

            Conference conference2 = new Conference();
            conference2.ConferenceId = 2;
            conference2.ConferenceDeleted = false;
            conference2.ConferenceEndDate = DateTime.Parse("Jan 15, 2020");
            conference2.ConferenceCFPEndDate = DateTime.Parse("Jun 1, 2019");
            conference2.ConferenceInactive = false;
            conference2.ConferenceLongName = "2020 Conference on Medical Student Education";
            conference2.ConferenceShortName = "MSE20";
            conference2.ConferenceTypeId = 2;
            conference2.ConferenceStartDate = DateTime.Parse("Jan 11, 2020");

            activeConferences.Add(conference1);
            activeConferences.Add(conference2);

            return activeConferences;

        }

        public IEnumerable<Conference> GetConferencesWithOpenReviewStatus()
        {
            throw new NotImplementedException();
        }

        public Conference GetConference(int conferenceId)
        {
            return GetActiveConferences().First();
        }

        public Conference GetConference(string conferenceShortName)
        {
            throw new NotImplementedException();
        }

        public ConferenceType GetConferenceType(int conferenceTypeId)
        {
            throw new NotImplementedException();
        }

        public ConferenceType GetConferenceType(string conferenceTypeName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Conference> GetConferencesWithUnstartedStatus()
        {
            throw new NotImplementedException();
        }

        public void RetireConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public bool ToggleConferenceInactive(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public DateTime EditCFPEndDate(int conferenceId, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public DateTime EditSubmissionReviewEndDate(int conferenceId, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Conference> GetActiveCeraConferences()
        {
            throw new NotImplementedException();
        }

        public string GetConferenceName(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EntityByNameAndId> GetConferencesByDateRange(DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public int CreateConference(Conference conference)
        {
            throw new NotImplementedException();
        }
    }
}
