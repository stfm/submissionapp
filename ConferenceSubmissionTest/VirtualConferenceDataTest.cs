﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class VirtualConferenceDataTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public VirtualConferenceDataTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }

        [Fact]
        public void GetVirtualConferenceDataTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                VirtualConferenceDataRepository virtualConferenceDataRepository = new VirtualConferenceDataRepository(context);

                VirtualConferenceData virtualConferenceData = virtualConferenceDataRepository.GetVirtualConferenceData(100);

                Output.WriteLine("Conference name is " + virtualConferenceData.Conference.ConferenceShortName);

                Output.WriteLine("Blob container name is  " + virtualConferenceData.BlobContainerName);

                Assert.Equal("MSE21Presentations", virtualConferenceData.DropboxFolderName);

            }
        }


        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" },
                    new ConferenceType { ConferenceTypeId = 3, ConferenceTypeName = "Practice and Quality Improvement" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2021"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2021 Medical Student Education Conference",
                        ConferenceShortName = "MSE21",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2021"),
                        ConferenceTypeId = 2,
                        ConferenceVirtual = true
                    }

                );

                context.SaveChanges();

                context.VirtualConferenceData.AddRange(

                    new VirtualConferenceData { ConferenceId = 100, VirtualConferenceDataId = 1001, BlobContainerName = "mse21", DropboxFolderName = "MSE21Presentations", DropboxUploadUrl = "https://dropbox.com/xxxxxx" }

                );

                context.SaveChanges();

                
            }

            return options;
        }
    }
}
