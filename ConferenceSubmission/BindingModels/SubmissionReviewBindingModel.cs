﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Stores data needed to create form fields for submission 
    /// review and to store the user's answers.
    /// </summary>
    public class SubmissionReviewBindingModel
    {

        public Submission Submission { get; set; }

        public int SubmissionId { get; set; }

        public IEnumerable<FormField> FormFields { get; set; }

        /// <summary>
        /// Indicates whether or not the user wants to be taken immediately to their next assigned and unstarted
        /// review upon submitting the review they are currently working on
        /// </summary>
        public bool IsSaveAndNext { get; set; }

        public List<Presenter> Presenters { get; set; }

        public List<SubmissionFormField> SubmissionFormFields { get; set; }

    }
}
