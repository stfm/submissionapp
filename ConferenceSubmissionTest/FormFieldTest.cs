﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceSubmissionTest
{
	public class FormFieldTest
	{

		protected Xunit.Abstractions.ITestOutputHelper Output { get; }

		public FormFieldTest(Xunit.Abstractions.ITestOutputHelper output)
		{

			Output = output;
		}


		[Fact]
		public void GetFormFieldsTest()
		{
			var connection = new SqliteConnection("DataSource=:memory:");
			connection.Open();
			var options = new DbContextOptionsBuilder<AppDbContext>()
				.UseSqlite(connection)
				.Options;

			// Insert seed data into the database using one instance of the context
			using (var context = new AppDbContext(options))
			{
				context.Database.EnsureCreated();
				context.ConferenceTypes.AddRange(
					new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
					new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
				context.SaveChanges();

				context.Conferences.AddRange(
					new Conference
					{
						ConferenceId = 100,
						ConferenceDeleted = false,
						ConferenceEndDate = DateTime.Parse("Jan 15, 2018"),
						ConferenceInactive = false,
						ConferenceLongName = "Test Conference 1",
						ConferenceShortName = "TC1",
						ConferenceStartDate = DateTime.Parse("Jan 11, 2018"),
						ConferenceTypeId = 1
					}

				);

				context.SaveChanges();

				Conference aConference = context.Conferences.Find(100);

				context.SubmissionCategories.AddRange(

					new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture TC1" },
					new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar TC1" }


				);

				context.SaveChanges();

				context.SubmissionStatuses.AddRange(

					new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
					new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }

				);

				context.SaveChanges();

				context.Submissions.AddRange(

					new Submission
					{
						AcceptedCategoryId = 1001,
						Conference = aConference,
						SubmissionAbstract = "Test Abstract",
						SubmissionCategoryId = 1000,
						SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
						SubmissionId = 10001,
						SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
						SubmissionStatusId = 2000,
						SubmissionTitle = "Test Title",

					}

				);

				context.SaveChanges();

				context.FormFields.AddRange(

					new FormField
					{

						FormFieldId = 1,
						SubmissionCategoryId = 1000,
						FormFieldDeleted = false,
						FormFieldType = "screentext",
						FormFieldSortOrder = 1,
						FormFieldRole = "proposal"


					},


					new FormField
					{

						FormFieldId = 2,
						SubmissionCategoryId = 1000,
						FormFieldDeleted = false,
						FormFieldType = "textarea",
						FormFieldSortOrder = 2,
						FormFieldRole = "proposal"

					},

					new FormField
					{

						FormFieldId = 3,
						SubmissionCategoryId = 1000,
						FormFieldDeleted = false,
						FormFieldType = "dropdown",
						FormFieldSortOrder = 3,
						FormFieldRole = "proposal"

					}



				);

				context.SaveChanges();

				context.FormFieldProperties.AddRange(

					new FormFieldProperty
					{


						FormFieldPropertyId = 1,
						FormFieldId = 1,
						PropertyName = "textvalue",
						PropertyValue = "Outline your session below."
					},


					new FormFieldProperty
					{


						FormFieldPropertyId = 2,
						FormFieldId = 2,
						PropertyName = "rows",
						PropertyValue = "5"
					},


					new FormFieldProperty
					{


						FormFieldPropertyId = 3,
						FormFieldId = 2,
						PropertyName = "cols",
						PropertyValue = "50"
					},

					new FormFieldProperty
					{


						FormFieldPropertyId = 4,
						FormFieldId = 3,
						PropertyName = "options",
						PropertyValue = "Medical Students,Residents,Physicians"
					},


					new FormFieldProperty
					{


						FormFieldPropertyId = 5,
						FormFieldId = 3,
						PropertyName = "label",
						PropertyValue = "Select the intended audience"
					}



				);

				context.SaveChanges();

				context.SubmissionFormFields.AddRange(


					new SubmissionFormField
					{

						SubmissionFormFieldId = 1,
						FormFieldId = 2,
						SubmissionFormFieldValue = "This is an outline of my submission.",
						SubmissionId = 10001


					},

					new SubmissionFormField
					{

						SubmissionFormFieldId = 2,
						FormFieldId = 3,
						SubmissionFormFieldValue = "Residents",
						SubmissionId = 10001


					}

				);

				context.SaveChanges();

			}

			// Use a separate instance of the context to verify correct data was saved to database
			using (var context = new AppDbContext(options))
			{

				IEnumerable<FormField> formFields = context.FormFields.Where(f => f.FormFieldRole.Equals("proposal")).Include(s => s.FormFieldProperties);

				foreach (var formField in formFields)
				{


					Output.WriteLine("Form field type is " + formField.FormFieldType);


					foreach (var formFieldProperty in formField.FormFieldProperties)
					{

						Output.WriteLine("Form field property name is " + formFieldProperty.PropertyName);
						Output.WriteLine("Form field property value is " + formFieldProperty.PropertyValue);


					}

				}

				Assert.NotNull(formFields);

				IEnumerable<SubmissionFormField> submissionFormFields = context.SubmissionFormFields.Where(s => s.SubmissionId.Equals(10001));

				foreach (var submissionFormField in submissionFormFields)
				{

					Output.WriteLine("Submission form field id is " + submissionFormField.FormFieldId);

					Output.WriteLine("Submission form field value is " + submissionFormField.SubmissionFormFieldValue);


				}


				Assert.NotNull(submissionFormFields);

			}






		}
	}
}
