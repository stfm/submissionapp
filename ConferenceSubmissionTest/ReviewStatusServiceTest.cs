﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class ReviewStatusServiceTest
    {
        [Fact]
        public void GetByReviewStatusType_NOT_STARTED()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            ReviewStatus reviewStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var reviewStatusService = new ReviewStatusService(new ReviewStatusRepository(context), new ReviewRepository(context));
                reviewStatus = reviewStatusService.GetReviewStatusByReviewStatusType(ReviewStatusType.NOT_STARTED);
            }

            //Assert
            Assert.Equal(ReviewStatusType.NOT_STARTED, reviewStatus.ReviewStatusType);
            Assert.Equal(1, reviewStatus.ReviewStatusId);
        }

        [Fact]
        public void GetByReviewStatusTypeUsingSubmissionIdAndReviewerId_INCOMPLETE()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            ReviewStatus reviewStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var reviewStatusService = new ReviewStatusService(new ReviewStatusRepository(context), new ReviewRepository(context));
                reviewStatus = reviewStatusService.GetReviewStatusForReview(10001, 4001);
            }

            //Assert
            Assert.Equal(ReviewStatusType.INCOMPLETE, reviewStatus.ReviewStatusType);
            Assert.Equal(2, reviewStatus.ReviewStatusId);
        }

        [Fact]
        public void GetByReviewStatusTypeUsingSubmissionIdAndReviewerId_COMPLETE()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            ReviewStatus reviewStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var reviewStatusService = new ReviewStatusService(new ReviewStatusRepository(context), new ReviewRepository(context));
                reviewStatus = reviewStatusService.GetReviewStatusForReview(10001, 4002);
            }

            //Assert
            Assert.Equal(ReviewStatusType.COMPLETE, reviewStatus.ReviewStatusType);
            Assert.Equal(3, reviewStatus.ReviewStatusId);
        }

        [Fact]
        public void GetByReviewStatusTypeUsingSubmissionIdAndReviewerId_NOT_STARTED()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            ReviewStatus reviewStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var reviewStatusService = new ReviewStatusService(new ReviewStatusRepository(context), new ReviewRepository(context));
                reviewStatus = reviewStatusService.GetReviewStatusForReview(10005, 4004);
            }

            //Assert
            Assert.Equal(ReviewStatusType.NOT_STARTED, reviewStatus.ReviewStatusType);
            Assert.Equal(1, reviewStatus.ReviewStatusId);
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 1, ReviewStatusType = ReviewStatusType.NOT_STARTED });
                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 2, ReviewStatusType = ReviewStatusType.INCOMPLETE });
                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 3, ReviewStatusType = ReviewStatusType.COMPLETE });

                context.SaveChanges();

                context.ConferenceTypes.AddRange(
                   new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                   new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceCFPEndDate = DateTime.Parse("Nov 11, 2018"),
                        ConferenceSubmissionReviewEndDate = DateTime.Parse("Dec 11, 2018"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceCFPEndDate = DateTime.Parse("Dec 11, 2018"),
                        ConferenceSubmissionReviewEndDate = DateTime.Parse("Jan 10, 2019"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW },
                    new SubmissionStatus { SubmissionStatusId = 2002, SubmissionStatusName = SubmissionStatusType.CANCELED }
               );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {

                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title",
                        SubmissionCompleted = false

                    },

                new Submission
                {

                    AcceptedCategoryId = 1000,
                    Conference = aConference,
                    SubmissionAbstract = "Test Abstract 2",
                    SubmissionCategoryId = 1000,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 12, 2018"),
                    SubmissionId = 10002,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 12, 2018"),
                    SubmissionStatusId = 2000,
                    SubmissionTitle = "Test Title 2",
                    SubmissionCompleted = false

                },

                new Submission
                {

                    AcceptedCategoryId = 1000,
                    Conference = aConference,
                    SubmissionAbstract = "Test Abstract 3",
                    SubmissionCategoryId = 1000,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10003,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 2000,
                    SubmissionTitle = "Test Title 3",
                    SubmissionCompleted = false

                }

                );

                context.SaveChanges();

                ConferenceSession aConferenceSession = new ConferenceSession { SessionCode = "S01", SessionLocation = "Green Room", SessionStartDateTime = DateTime.Parse(" Apr 10, 2018") };

                SubmissionPayment submissionPayment = new SubmissionPayment { PaymentAmount = 25, PaymentDateTime = DateTime.Parse(" Apr 10, 2018"), SubmissionId = 10001, PaymentTransactionId = "P999" };


                Submission aSubmission = context.Submissions.Find(10001);

                aSubmission.ConferenceSession = aConferenceSession;

                aSubmission.SubmissionPayment = submissionPayment;

                context.Update(aSubmission);

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }


                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(

                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 3, ParticipantId = 4002, SubmissionId = 10002 },
                    new SubmissionParticipant { SubmissionParticipantId = 4, ParticipantId = 4003, SubmissionId = 10003 }
                );


                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 2 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 3 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 4 }


                );

                context.SaveChanges();

                context.Reviewers.AddRange(

                    new Reviewer { ReviewerId = 4001, StfmUserId = "3001" },

                    new Reviewer { ReviewerId = 4002, StfmUserId = "3002" },

                    new Reviewer { ReviewerId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();

                //Create some SubmissionReviewer records to assign reviewers to submissions

                context.SubmissionReviewer.AddRange(

                    new SubmissionReviewer { SubmissionReviewerId = 1, SubmissionId = 10001, ReviewerId = 4001 },

                    new SubmissionReviewer { SubmissionReviewerId = 2, SubmissionId = 10002, ReviewerId = 4001 },

                    new SubmissionReviewer { SubmissionReviewerId = 3, SubmissionId = 10001, ReviewerId = 4002 },

                    new SubmissionReviewer { SubmissionReviewerId = 4, SubmissionId = 10002, ReviewerId = 4002 },

                    new SubmissionReviewer { SubmissionReviewerId = 5, SubmissionId = 10003, ReviewerId = 4003 },

                    new SubmissionReviewer { SubmissionReviewerId = 6, SubmissionId = 10003, ReviewerId = 4002 }


                );

                context.SaveChanges();

                context.Reviews.AddRange(


                    new Review { ReviewId = 100, ReviewDateTime = DateTime.Now, ReviewerId = 4001, SubmissionId = 10001, ReviewStatusId = 2 },

                   new Review { ReviewId = 101, ReviewDateTime = DateTime.Now, ReviewerId = 4002, SubmissionId = 10001, ReviewStatusId = 3 }

               );

                context.SaveChanges();

                context.FormFields.AddRange(

                    new FormField
                    {

                        FormFieldId = 2,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "screentext",
                        FormFieldSortOrder = 1,
                        FormFieldRole = "review",
                        AnswerRequired = false


                    },


                    new FormField
                    {

                        FormFieldId = 3,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "textarea",
                        FormFieldSortOrder = 2,
                        FormFieldRole = "review",
                        AnswerRequired = true

                    },

                    new FormField
                    {

                        FormFieldId = 1,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "select",
                        FormFieldSortOrder = 3,
                        FormFieldRole = "review",
                        AnswerRequired = true


                    },

                    new FormField
                    {

                        FormFieldId = 4,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "text",
                        FormFieldSortOrder = 4,
                        FormFieldRole = "review",
                        AnswerRequired = true

                    }



                );

                context.SaveChanges();

                context.FormFieldProperties.AddRange(

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 1,
                        FormFieldId = 2,
                        PropertyName = "textvalue",
                        PropertyValue = "Reviewer Comments"
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 2,
                        FormFieldId = 3,
                        PropertyName = "rows",
                        PropertyValue = "5"
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 3,
                        FormFieldId = 3,
                        PropertyName = "cols",
                        PropertyValue = "50"
                    },

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 4,
                        FormFieldId = 1,
                        PropertyName = "options",
                        PropertyValue = "YES,NO"
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 5,
                        FormFieldId = 1,
                        PropertyName = "label",
                        PropertyValue = "Should this submission be accepted?"
                    },

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 6,
                        FormFieldId = 4,
                        PropertyName = "label",
                        PropertyValue = "Should this submission be accepted in a different category?"
                    }



                );

                context.SaveChanges();

                context.ReviewFormFields.AddRange(


                    new ReviewFormField
                    {

                        ReviewFormFieldId = 1,
                        FormFieldId = 3,
                        ReviewFormFieldValue = "My reviewer comment for reviewId 100",
                        ReviewId = 100


                    },

                    new ReviewFormField
                    {

                        ReviewFormFieldId = 2,
                        FormFieldId = 1,
                        ReviewFormFieldValue = "YES",
                        ReviewId = 100


                    },

                    new ReviewFormField
                    {

                        ReviewFormFieldId = 3,
                        FormFieldId = 4,
                        ReviewFormFieldValue = "Do not move",
                        ReviewId = 100


                    }



                );

                context.SaveChanges();
            }

            return options;
        }
    }
}
