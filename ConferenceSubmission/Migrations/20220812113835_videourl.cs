﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class videourl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.AddColumn<string>(
                name: "VideoUrl",
                table: "VirtualMaterials",
                maxLength: 500,
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VirtualConferenceSession_SubmissionPresentationMethod_SubmissionPresentationMethodId",
                table: "VirtualConferenceSession");

            migrationBuilder.DropColumn(
                name: "VideoUrl",
                table: "VirtualMaterials");

            migrationBuilder.AlterColumn<int>(
                name: "SubmissionPresentationMethodId",
                table: "VirtualConferenceSession",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_VirtualConferenceSession_SubmissionPresentationMethod_SubmissionPresentationMethodId",
                table: "VirtualConferenceSession",
                column: "SubmissionPresentationMethodId",
                principalTable: "SubmissionPresentationMethod",
                principalColumn: "SubmissionPresentationMethodId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
