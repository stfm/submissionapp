﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Stores the user's input for paying for a submission
    /// by Credit Card
    /// </summary>
    public class CreditCardPaymentBindingModel
    {

        [Required]
        public int SubmissionId { get; set; }

        [Required]
        public decimal PaymentAmount { get; set; }

        [Required(ErrorMessage = "The credit card number is required")]
        [CreditCard(ErrorMessage = "The credit card number is not valid")]
        [Display(Name = "Credit Card Number")]
        public string CreditCardNumber { get; set; }

        [Required(ErrorMessage = "The credit card expiration date is required")]
        [StringLength(4, MinimumLength = 4)]
        [RegularExpression(@"\d{4}", ErrorMessage = "Format must be MMYY")]
        [Display(Name = "Credit Card Expiration Date (MMYY)")]
        public string CreditCardExpirationDate { get; set; }

        [Required(ErrorMessage = "The credit card CSV code is required")]
        [Display(Name = "Credit Card Number CSV Code")]
        public string CreditCardCsvCode { get; set; }

        [Required(ErrorMessage = "The first name of the card holder is required")]
        [Display(Name = "Billing First Name")]
        public string BillingFirstName { get; set; }

        [Required(ErrorMessage = "The last name of the card holder is required")]
        [Display(Name = "Billing Last Name")]
        public string BillingLastName { get; set; }

        [Required(ErrorMessage = "The street address of the billing address is required")]
        [Display(Name = "Billing Street Address")]
        public string BillingStreet { get; set; }

        [Required(ErrorMessage = "The city of the billing address is required")]
        [Display(Name = "Billing City")]
        public string BillingCity { get; set; }

        [Required(ErrorMessage = "The zip code of the billing address is required")]
        [Display(Name = "Billing Zip Code")]
        public string BillingZip { get; set; }


    }
}
