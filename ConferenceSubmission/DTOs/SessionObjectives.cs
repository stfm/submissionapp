﻿using System;
namespace ConferenceSubmission.DTOs
{
    public class SessionObjectives
    {
        public int conferenceId { get; set; }

        public int submissionId { get; set; }

        public string objectives { get; set; }
    }
}
