﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;
using AutoMapper;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;


namespace ConferenceSubmissionTest
{
    public class DisclosureServiceTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public DisclosureServiceTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }

        [Fact]
        public void GetDisclosureTest() {


            var repo = new DisclosureRepositoryForTesting();
            var sut = new DisclosureService( repo );

            // Act
            var disclosure = sut.GetDisclosure(1);

            Output.WriteLine("Disclosure date is " + disclosure.DisclosureDate);

            Assert.NotNull(disclosure);

            Assert.Equal("user1", disclosure.StfmUserId);


        }


        [Fact]
        public void IsDisclosureCurrentNullTest()
        {

            var repo = new DisclosureRepositoryForTesting();
            var sut = new DisclosureService(repo);

            // Act
            bool isDisclosureCurrent = sut.isDisclosureCurrent(DateTime.Parse("Nov 15, 2018"), "user3");

            Assert.False(isDisclosureCurrent);

        }

        [Fact]
        public void IsDisclosureCurrentFalse()
        {

            var repo = new DisclosureRepositoryForTesting();
            var sut = new DisclosureService(repo);

            // Act
            bool isDisclosureCurrent = sut.isDisclosureCurrent(DateTime.Parse("Nov 15, 2018"), "user2");

            Assert.False(isDisclosureCurrent);

        }

        [Fact]
        public void IsDisclosureCurrentTrue()
        {

            var repo = new DisclosureRepositoryForTesting();
            var sut = new DisclosureService(repo);

            // Act
            bool isDisclosureCurrent = sut.isDisclosureCurrent(DateTime.Parse("Nov 15, 2018"), "user1");

            Assert.True(isDisclosureCurrent);

        }

        [Fact]
        public void GetDisclosuresTest()
        {

            var repo = new DisclosureRepositoryForTesting();
            var sut = new DisclosureService(repo);

            Participant participant1 = new Participant
            {
                ParticipantId = 1,
                StfmUserId = "user1"
            };

            Participant participant2 = new Participant
            {
                ParticipantId = 2,
                StfmUserId = "user2"
            };

            Participant participant3 = new Participant
            {
                ParticipantId = 3,
                StfmUserId = "user3"
            };

            SubmissionParticipant submissionParticipant1 = new SubmissionParticipant
            {

                Participant = participant1,
                SubmissionId = 1,
                SubmissionParticipantId = 1,
            };

            SubmissionParticipant submissionParticipant2 = new SubmissionParticipant
            {

                Participant = participant2,
                SubmissionId = 1,
                SubmissionParticipantId = 2,
            };

            SubmissionParticipant submissionParticipant3 = new SubmissionParticipant
            {

                Participant = participant3,
                SubmissionId = 1,
                SubmissionParticipantId = 3,
            };


            List<SubmissionParticipant> participants = new List<SubmissionParticipant>();
            participants.Add(submissionParticipant1);
            participants.Add(submissionParticipant2);
            participants.Add(submissionParticipant3);

            Submission submission = new Submission
            {
                SubmissionId = 1,
                ParticipantLink = participants

            };


            List<Disclosure> disclosures = sut.GetDisclosures(submission);

            Assert.True(disclosures.Count == 2);



        }

    }
}
