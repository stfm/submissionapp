﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Data
{
    public interface IFAQRepository
    {
        List<FAQ> GetFAQs();
    }
}
