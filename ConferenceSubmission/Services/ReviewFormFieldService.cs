﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
	public class ReviewFormFieldService : IReviewFormFieldService
    {

        private readonly IReviewFormFieldRepository _reviewFormFieldRepository;

        public ReviewFormFieldService(IReviewFormFieldRepository reviewFormFieldRepository)
        {

            _reviewFormFieldRepository = reviewFormFieldRepository;

        }

        public void AddReviewFormField(ReviewFormField reviewFormField)
        {

            _reviewFormFieldRepository.AddReviewFormField(reviewFormField);

        }

        public IEnumerable<ReviewFormField> GetFormFieldsForReview(int reviewId)
        {

            return _reviewFormFieldRepository.GetFormFieldsForReview(reviewId);

        }

        public void RemoveAllReviewFormFieldsForReview(int reviewId)
        {
            _reviewFormFieldRepository.RemoveAllFormFieldsForReview(reviewId);
        }
    }
}
