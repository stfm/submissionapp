﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface IReviewStatusRepository
    {
        /// <summary>
        /// Gets ReviewStatus by ReviewStatusType
        /// </summary>
        /// <returns>ReviewStatus</returns>
        /// <param name="reviewStatusType">Review status type.</param>
        ReviewStatus GetReviewStatusByReviewStatusType(ReviewStatusType reviewStatusType);

    }
}
