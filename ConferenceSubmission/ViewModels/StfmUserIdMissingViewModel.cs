﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class StfmUserIdMissingViewModel
    {

        public List<StfmUserIdMissingFromSalesforce> StfmUserIds { get; set; }

        public StfmUserIdMissingViewModel() {

            StfmUserIds = new List<StfmUserIdMissingFromSalesforce>();

        }

    }
}
