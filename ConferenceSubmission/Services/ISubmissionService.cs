﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Services
{
    public interface ISubmissionService
    {
        /// <summary>
        /// Get the logged in users submissions
        /// </summary>
        /// <returns>The user's submissions.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        List<SubmissionSummaryViewModel> GetSubmissions(string stfmUserId);

        /// <summary>
        /// Get the logged in users current submissions.
        /// Current submissions are those submissions
        /// where the Conference end date has not passed.
        /// </summary>
        /// <returns>The user's submissions.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        List<SubmissionSummaryViewModel> GetCurrentSubmissions(string stfmUserId);

        /// <summary>
        /// Gets all submissions not withdraw or canceled.
        /// </summary>
        /// <returns>The all submissions not withdraw or canceled.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        List<SubmissionSummaryViewModel> GetAllSubmissionsNotWithdrawnOrCanceled(string stfmUserId);

        /// <summary>
        /// Gets all submissions not canceled.
        /// </summary>
        /// <returns>The all submissions not canceled.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        List<SubmissionSummaryViewModel> GetAllSubmissionsNotCanceled(string stfmUserId);

        /// <summary>
        /// Gets the submission.
        /// </summary>
        /// <returns>The submission.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        Submission GetSubmission(int submissionId);
        
        /// <summary>
        /// Get current CERA submissions
        /// </summary>
        /// <param name="sTFMUserId"></param>
        /// <returns>Collection of submission summary view models</returns>
        List<SubmissionSummaryViewModel> GetCurrentCeraSubmissions(string sTFMUserId);

        /// <summary>
        /// Updates the submission status.
        /// </summary>
        /// <returns><c>true</c>, if submission status was updated, <c>false</c> otherwise.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="submissionStatusId">Submission status identifier.</param>
        bool UpdateSubmissionStatus(int submissionId, int submissionStatusId);

		/// <summary>
        /// Adds the submission.
        /// </summary>
        /// <returns>The submission ID.</returns>
        /// <param name="submission">Submission.</param>
        int AddSubmission(Submission submission);

        /// <summary>
        /// Updates a submission's title and abstract
        /// </summary>
        /// <param name="submissionId">Submission Identifier</param>
        /// <param name="title">Submission Title</param>
        /// <param name="_abstract">Submission Abstract</param>
        /// <param name="adminEditedSubmission">true if admin edit submission for online/brochure</param>
        /// <returns><c>true</c>, if submission's base details were updated, <c>false</c> otherwise.</returns>
        bool UpdateSubmissionBaseDetails(int submissionId, string title, string _abstract, bool adminEditedSubmission);

        /// <summary>
        /// Gets the submission with category proposal questions and answers.
        /// </summary>
        /// <returns>The submission with proposal.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="StfmUserId">STFM User Id</param>
        SubmissionWithProposalAnswersViewModel GetSubmissionWithProposal(int submissionId, string StfmUserId);

        /// <summary>
        /// Gets the submission with category proposal questions and answers.
        /// </summary>
        /// <returns>The submission with proposal.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        SubmissionWithProposalAnswersViewModel GetSubmissionWithProposal(int submissionId);

        /// <summary>
        /// For the provided submissionId did the user answer all the questions that
        /// require an answer.
        /// </summary>
        /// <returns><c>true</c>, if the submission has an answer for all 
        /// submission category proposal questions that require an answer 
        /// <c>false</c> otherwise.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        bool IsSubmissionCategoryProposalCompleted(int submissionId);

        /// <summary>
        /// Updates the submission completed field for the submission with
        /// the provided submissionId.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="submissionCompleted">If set to <c>true</c> submission completed.</param>
        void UpdateSubmissionCompleted(int submissionId, bool submissionCompleted);

        /// <summary>
        /// Adds the submission payment to the database.
        /// </summary>
        /// <param name="submissionPayment">Submission payment.</param>
        void AddSubmissionPayment(SubmissionPayment submissionPayment);

        /// <summary>
        /// Determines if a payment is required to submit by checking the submission payment requirement 
        /// and the membership status of the submitter and main presenter.
        /// </summary>
        /// <returns><c>true</c>, if submission payment is required, <c>false</c> otherwise.</returns>
        /// <param name="submissionPaymentRequirement">Submission payment requirement.</param>
        /// <param name="submissionId">Submission Id</param>
        bool IsSubmissionPaymentRequired(SubmissionPaymentRequirement submissionPaymentRequirement, int submissionId);


        /// <summary>
        /// Should the view page display payment requirement text.
        /// </summary>
        /// <returns>True if the view page should display the payment requirement text.</returns>
        /// <param name="submissionCategoryItems">Submission category items.</param>
        /// <param name="STFMUserId">STFMU ser identifier.</param>
        bool DisplayPaymentRequirementText(IEnumerable<SelectListItem> submissionCategoryItems, string STFMUserId);

        /// <summary>
        /// Gets the information about a submission that is needed for creating a printable version
        /// </summary>
        /// <param name="submissionId">Unique identifier of the submission</param>
        /// <returns>SubmissionPrintViewModel</returns>
        SubmissionPrintViewModel GetSubmissionPrintViewModel(int submissionId);


        /// <summary>
        /// Creates a submission.
        /// </summary>
        /// <returns>The submission.</returns>
        /// <param name="baseSubmissionBindingModel">Base submission binding model.</param>
        /// <param name="submissionParticipants">Submission participants.</param>
        /// <param name="conference">Conference.</param>
        Submission CreateSubmission(BaseSubmissionBindingModel baseSubmissionBindingModel,
                                                   List<SubmissionParticipant> submissionParticipants,
                                    Models.Conference conference);

        /// <summary>
        /// Creates a list of Presenter objects given a list of SubmissionParticipant objects.
        /// </summary>
        /// <param name="submissionParticipants">List of SubmissionParticipant objects.</param>
        /// <returns>List of Presenter objects.</returns>
        List<Presenter> BuildPresentersListFromSubmissionParticipants(List<SubmissionParticipant> submissionParticipants);

        /// <summary>
        /// Get a list of SubmissionFormField objects associated with a particular submission
        /// </summary>
        /// <param name="submissionId">Unique identifier of a particular submission</param>
        /// <returns>List of SubmissionFormField objects</returns>
        List<SubmissionFormField> GetSubmissionFormFields(int submissionId);

        /// <summary>
        /// Gets the information about incomplete submissions needed for the view page.
        /// </summary>
        /// <returns>The incomplete submissions view model.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        IncompleteSubmissionsViewModel GetIncompleteSubmissionsViewModel(int conferenceId);

        /// <summary>
        /// Gets the submissions for conference.
        /// </summary>
        /// <returns>The submissions for conference.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        List<Submission> GetSubmissionsForConference(int conferenceId);

        /// <summary>
        /// Gets the accepted submissions for conference.
        /// </summary>
        /// <returns>The submissions for conference.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        List<Submission> GetAcceptedSubmissionsForConference(int conferenceId);

        /// <summary>
        /// Gets the accepted submissions eligible to be part of the
        /// virtual conference supplement.
        /// </summary>
        /// <returns>The submissions eligible to be part of the
        /// virtual conference supplement.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        List<Submission> GetAcceptedOnlineSubmissionsForConference(int conferenceId);

        /// <summary>
        /// Gets the submissions that will be presented at the 
        /// virtual conference supplement.
        /// </summary>
        /// <returns>The submissions that will be presented at the virtual conference</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        List<Submission> GetVirtualConferenceSubmissions(int conferenceId);

        /// <summary>
        /// Gets the submissions using the provided collection of submission Ids.
        /// </summary>
        /// <returns>The submissions for conference.</returns>
        /// <param name="submissionIds">Collection of session ids</param>
        List<Submission> GetSubmissions(IEnumerable<int> submissionIds);

        /// <summary>
        /// Gets the submissions for the provided stfmUserId
        /// </summary>
        /// <returns>The submissions.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        IEnumerable<Submission> GetCompleteSubmissions(string stfmUserId);

        /// <summary>
        /// Gets the submissions for the provided conference.
        /// </summary>
        /// <returns>The submissions for conference with all participants.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        List<Submission> GetSubmissionsForConferenceWithAllParticipants(int conferenceId);

        /// <summary>
        /// Updates the submission accepted category.
        /// </summary>
        /// <returns><c>true</c>, if submission accepted category was updated, <c>false</c> otherwise.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="submissionCategoryId">Submission category identifier.</param>
        bool UpdateSubmissionAcceptedCategory(int submissionId, int submissionCategoryId);

        /// <summary>
        /// Gets the accepted submissions for category.
        /// </summary>
        /// <returns>The accepted submissions for accepted category ID provided.</returns>
        /// <param name="acceptedCategoryId">Accepted Category Id</param>
        IEnumerable<SubmissionUpdate> GetAcceptedSubmissionsForCategory(int acceptedCategoryId);

        /// <summary>
        /// For a given submission, sets the ParticipantOrderModified flag to true
        /// </summary>
        /// <param name="submissionId">Unique identifier of submission</param>
        void SetParticipantOrderModifiedToTrue(int submissionId);


        /// <summary>
        /// For a given conference, returns all submissions (by Id) whose participant order
        /// has been modified.
        /// </summary>
        /// <param name="conferenceId">Unique identifier of submission</param>
        /// <returns>List of ints where each int is a submission Id</returns>
        List<int> GetSubmissionsWhoseParticipantOrderHasBeenModified(int conferenceId);

        /// <summary>
        /// Attempts to find the submission in the database, returns true or false if found/not found
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns>boolean</returns>
        bool SubmissionExists(int submissionId);

        /// <summary>
        /// Gets all submissions (that have a session) and the presenters in a format needed by the conference registration reports
        /// </summary>
        /// <param name="conferenceId">Unique identifier of conference</param>
        /// <returns>List of SubmissionForRegistrationReport objects</returns>
        List<SubmissionForRegistrationReport> GetSubmissionsForConferenceRegistrationReport(int conferenceId);

        /// <summary>
        /// Gets the SessionsWithNoRegistrationsReportViewModel for a particular Conference and Salesforce Event
        /// </summary>
        /// <param name="conferenceId">Unique Id of Conference in Conference Admin</param>
        /// <param name="eventId">Unique Id of Event in Salesforce</param>
        /// /// <param name="eventName">Name of Event in Salesforce</param>
        /// <returns>SessionsWithNoRegistrationsReportViewModel</returns>
        SessionsWithNoRegistrationsReportViewModel GetSessionsWithNoRegistrationsReportViewModel(int conferenceId, string eventId, string eventName);

        /// <summary>
        /// Gets the AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel for a particular Coference and Salesforce Event
        /// </summary>
        /// <param name="conferenceId">Unique Id of Conference in Conference Admin</param>
        /// <param name="eventId">Unique Id of Event in Salesforce</param>
        /// <param name="eventName">Name of Event in Salesforce</param>
        /// <returns>AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel</returns>
        AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel GetAllSessionsWithPresentersThatHaveNotRegisteredReportViewModel(int conferenceId, string eventId, string eventName);

        /// <summary>
        /// Gets the number of presenters registered and unregistered for conference attendence 
        /// for each session for a particular Coference and Salesforce Event
        /// </summary>
        /// <param name="conferenceId">Unique Id of Conference in Conference Admin</param>
        /// <param name="eventId">Unique Id of Event in Salesforce</param>
        /// <param name="eventName">Name of Event in Salesforce</param>
        /// <returns>AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel</returns>
        SessionRegisteredUnregisteredViewModel GetAllSessionsWithPresentersRegisteredUnregistered(int conferenceId, string eventId, string eventName);

        /// <summary>
        /// For each accepted submission in the conference add a SubmissionPresentationMethod record for
        /// each allowed presentation method for the accepted category.
        /// 
        /// </summary>
        /// <param name="conferenceId">Conference ID</param>
        void AddSubmissionPresentationMethods(int conferenceId);

        /// <summary>
        /// Add SubmissionPresentationMethod records associated with the 
        /// provided submission ID based on the presentation methods
        /// for the submission's accepted submission category.
        /// </summary>
        /// <param name="submissionId">Submission ID</param>
        void AddSubmissionPresentationsToSubmission(int submissionId);

        /// <summary>
        /// Remove all SubmissionPresentationMethods associated 
        /// with the provided submissionId.
        /// </summary>
        /// <param name="submissionId">Submission ID</param>
        void RemoveSubmissionPresentationMethods(int submissionId);


        /// <summary>
        /// For the given Submission, finds the SubmissionPresentationMethod whose PresentationMethodType matches the given PresentationMethodType and updates the PresentationMethodType to the given PresentationMethodType
        /// </summary>
        /// <param name="submissionId">Id of the submission</param>
        /// <param name="presentationMethodType">This tells the system which Submission Presentation Method needs to be updated.</param>
        /// <param name="presentationStatusType">Update the SubmissionPresntationMethod's status to this PresentationStatusType</param>
        void UpdateSubmissionPresentationMethodStatus(int submissionId, PresentationMethodType presentationMethodType, PresentationStatusType presentationStatusType);
    }
}
