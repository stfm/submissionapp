﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class virtualconferencesession : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionPresentationMethod_PresentationMethod_PresentationMethodId",
                table: "SubmissionPresentationMethod");

            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionPresentationMethod_PresentationStatus_PresentationStatusId",
                table: "SubmissionPresentationMethod");

            migrationBuilder.AlterColumn<int>(
                name: "PresentationStatusId",
                table: "SubmissionPresentationMethod",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PresentationMethodId",
                table: "SubmissionPresentationMethod",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "VirtualConferenceSession",
                columns: table => new
                {
                    VirtualConferenceSessionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubmissionId = table.Column<int>(nullable: false),
                    SessionCode = table.Column<string>(maxLength: 10, nullable: false),
                    SessionStartDateTime = table.Column<DateTime>(nullable: false),
                    SessionEndDateTime = table.Column<DateTime>(nullable: false),
                    SessionTrack = table.Column<string>(maxLength: 250, nullable: true),
                    SubmissionPresentationMethodId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VirtualConferenceSession", x => x.VirtualConferenceSessionId);
                    table.ForeignKey(
                        name: "FK_VirtualConferenceSession_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VirtualConferenceSession_SubmissionPresentationMethod_SubmissionPresentationMethodId",
                        column: x => x.SubmissionPresentationMethodId,
                        principalTable: "SubmissionPresentationMethod",
                        principalColumn: "SubmissionPresentationMethodId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VirtualConferenceSession_SubmissionId",
                table: "VirtualConferenceSession",
                column: "SubmissionId");

            migrationBuilder.CreateIndex(
                name: "IX_VirtualConferenceSession_SubmissionPresentationMethodId",
                table: "VirtualConferenceSession",
                column: "SubmissionPresentationMethodId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionPresentationMethod_PresentationMethod_PresentationMethodId",
                table: "SubmissionPresentationMethod",
                column: "PresentationMethodId",
                principalTable: "PresentationMethod",
                principalColumn: "PresentationMethodId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionPresentationMethod_PresentationStatus_PresentationStatusId",
                table: "SubmissionPresentationMethod",
                column: "PresentationStatusId",
                principalTable: "PresentationStatus",
                principalColumn: "PresentationStatusId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionPresentationMethod_PresentationMethod_PresentationMethodId",
                table: "SubmissionPresentationMethod");

            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionPresentationMethod_PresentationStatus_PresentationStatusId",
                table: "SubmissionPresentationMethod");

            migrationBuilder.DropTable(
                name: "VirtualConferenceSession");

            migrationBuilder.AlterColumn<int>(
                name: "PresentationStatusId",
                table: "SubmissionPresentationMethod",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PresentationMethodId",
                table: "SubmissionPresentationMethod",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionPresentationMethod_PresentationMethod_PresentationMethodId",
                table: "SubmissionPresentationMethod",
                column: "PresentationMethodId",
                principalTable: "PresentationMethod",
                principalColumn: "PresentationMethodId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionPresentationMethod_PresentationStatus_PresentationStatusId",
                table: "SubmissionPresentationMethod",
                column: "PresentationStatusId",
                principalTable: "PresentationStatus",
                principalColumn: "PresentationStatusId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
