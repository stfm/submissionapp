﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;
using AutoMapper;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmissionTest
{
    public class ConferenceServiceTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public ConferenceServiceTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }

        [Fact]
        public void GetActiveConferencesTest()
        {
            // Arrange
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ConferenceSubmissionMappingProfile());
            });

            var mapper = config.CreateMapper();
            var repo = new ConferenceRepositoryForTesting();
            var subCatRepo = new SubmissionCategoryRepositoryForTesting();
            var subRepo = new SubmissionRepositoryForTesting();
            var sut = new ConferenceService(mapper, repo, subCatRepo, subRepo, null);

            // Act
            var result = sut.GetActiveConferences();

            Output.WriteLine("Number of active conferences found is " + result.Count());

            foreach (ConferenceViewModel conferenceViewModel in result)
            {

                Output.WriteLine("Conference name is " + conferenceViewModel.ConferenceLongName);

                Output.WriteLine("Conference CFP end date is " + conferenceViewModel.ConferenceCFPEndDate);

            }

            Assert.Equal(2, result.Count());


        }

        [Fact]
        public void GetSubmissionsByCategoryForConferenceTest()
        {
            // Arrange
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ConferenceSubmissionMappingProfile());
            });

            var mapper = config.CreateMapper();
            var repo = new ConferenceRepositoryForTesting();
            var subCatRepo = new SubmissionCategoryRepositoryForTesting();
            var subRepo = new SubmissionRepositoryForTesting();
            var sut = new ConferenceService(mapper, repo, subCatRepo, subRepo, null);

            // Act
            SubmissionsByCategoryForConferenceViewModel submissionsByCategoryForConferenceViewModel = sut.GetSubmissionsByCategoryForConference(100);

            foreach( SubmissionsForCategory submissionsForCategory in submissionsByCategoryForConferenceViewModel.SubmissionsForCategories)
            {

                Output.WriteLine(submissionsForCategory.category + "  - " + submissionsForCategory.submissionsNotFinished + " " +
                submissionsForCategory.submissionsCompleted + " " + submissionsForCategory.totalSubmissions);


            }




        }

    }
}
