﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class submissionpresentationmethod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubmissionPresentationMethodId",
                table: "ConferenceSessions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PresentationStatus",
                columns: table => new
                {
                    PresentationStatusId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PresentationStatusName = table.Column<int>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PresentationStatus", x => x.PresentationStatusId);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionPresentationMethod",
                columns: table => new
                {
                    SubmissionPresentationMethodId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubmissionId = table.Column<int>(nullable: false),
                    PresentationMethodId = table.Column<int>(nullable: true),
                    PresentationStatusId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionPresentationMethod", x => x.SubmissionPresentationMethodId);
                    table.ForeignKey(
                        name: "FK_SubmissionPresentationMethod_PresentationMethod_PresentationMethodId",
                        column: x => x.PresentationMethodId,
                        principalTable: "PresentationMethod",
                        principalColumn: "PresentationMethodId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubmissionPresentationMethod_PresentationStatus_PresentationStatusId",
                        column: x => x.PresentationStatusId,
                        principalTable: "PresentationStatus",
                        principalColumn: "PresentationStatusId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubmissionPresentationMethod_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConferenceSessions_SubmissionPresentationMethodId",
                table: "ConferenceSessions",
                column: "SubmissionPresentationMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionPresentationMethod_PresentationMethodId",
                table: "SubmissionPresentationMethod",
                column: "PresentationMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionPresentationMethod_PresentationStatusId",
                table: "SubmissionPresentationMethod",
                column: "PresentationStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionPresentationMethod_SubmissionId",
                table: "SubmissionPresentationMethod",
                column: "SubmissionId");

            migrationBuilder.AddForeignKey(
                name: "FK_ConferenceSessions_SubmissionPresentationMethod_SubmissionPresentationMethodId",
                table: "ConferenceSessions",
                column: "SubmissionPresentationMethodId",
                principalTable: "SubmissionPresentationMethod",
                principalColumn: "SubmissionPresentationMethodId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ConferenceSessions_SubmissionPresentationMethod_SubmissionPresentationMethodId",
                table: "ConferenceSessions");

            migrationBuilder.DropTable(
                name: "SubmissionPresentationMethod");

            migrationBuilder.DropTable(
                name: "PresentationStatus");

            migrationBuilder.DropIndex(
                name: "IX_ConferenceSessions_SubmissionPresentationMethodId",
                table: "ConferenceSessions");

            migrationBuilder.DropColumn(
                name: "SubmissionPresentationMethodId",
                table: "ConferenceSessions");
        }
    }
}
