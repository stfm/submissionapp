﻿using System;
namespace ConferenceSubmission.Models
{  
    /// <summary>
    /// Represents the many-to-many relationship between a
    /// reviewer and submission assigned to that reviewer to 
    /// review.
    /// </summary>
    public class SubmissionReviewer
    {
        public SubmissionReviewer()
        {
        }

        public int SubmissionReviewerId { get; set; }

        public int SubmissionId { get; set; }

        public int ReviewerId { get; set; }

        public Submission Submission { get; set; }

        public Reviewer Reviewer { get; set; }


    }
}
