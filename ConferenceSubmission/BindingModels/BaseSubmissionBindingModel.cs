﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Stores base submission data from the user's
    /// create submission step 1.
    /// </summary>
    public class BaseSubmissionBindingModel
    {
        public BaseSubmissionBindingModel()
        {
            ParticipantRoleDefinitions = new List<string>();
        }

        [Required]
        public int ConferenceId { get; set; }

        public string ConferenceName { get; set; }

        [Required(ErrorMessage = "The title is required"), MaxLength(125, ErrorMessage = "A title cannot exceed 125 characters including spaces")]
        [Display(Name = "Title (125 characters including spaces - DO NOT use all capitals)")]
        public string SubmissionTitle { get; set; }

        [Required(ErrorMessage = "The abstract is required")]
        [Display(Name = "Abstract")]
        public string SubmissionAbstract { get; set; }



        public IEnumerable<SelectListItem> UserRoleItems { get; set; } =
            new List<SelectListItem> {

            new SelectListItem{Value= "submitter-only", Text="Submitter Only"},
            new SelectListItem{Value= "submitter-mainpresenter", Text="Submitter and Lead Presenter"},
            new SelectListItem{Value= "submitter-copresenter", Text="Submitter and Co-Presenter"},
            new SelectListItem{Value= "submitter-author", Text="Submitter, Author, Not Presenting"}
        };

        public List<string> ParticipantRoleDefinitions { get; set; }

        [Required]
        [Display(Name = "Select Your Role on Submission ")]
        public string SelectedUserRole { get; set; }

        public IEnumerable<SelectListItem> SubmissionCategoryItems { get; set; }

        [Required]
        [Display(Name = "Select the Submission Category ")]
        public int SelectedSubmissionCategory { get; set; }

        public string SelectedSubmissionCategoryName { get; set; }


        public string SubmissionPaymentRequirementText { get; set; }

        public bool DisplaySubmissionRequirementText { get; set; }

        public string CategoryAbstractInstructions { get; set; }

        public int CategoryAbstractMaxLength { get; set; }

        public string CategoryInstructions { get; set; }

        public override string ToString()
        {

            return "ConferenceId: " + ConferenceId + " | SubmissionCategory: " +
                SelectedSubmissionCategory + " | SubmissionTitle: " + SubmissionTitle +
                " | SubmissionAbstract: " + SubmissionAbstract +
                " | User Role: " + SelectedUserRole;


        }

    }
}
