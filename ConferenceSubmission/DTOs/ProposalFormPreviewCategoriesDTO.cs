﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs
{
    public class ProposalFormPreviewCategoriesDTO
    {
        public ProposalFormPreviewCategoriesDTO()
        {
            Categories = new List<ProposalFormPreviewCategory>();
        }

        public int ConferenceId { get; set; }
        public string ConferenceName { get; set; }

        public List<ProposalFormPreviewCategory> Categories { get; set; }
    }

    public class ProposalFormPreviewCategory
    {
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
    }
}
