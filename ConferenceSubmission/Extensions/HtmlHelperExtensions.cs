﻿using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Extensions
{
    public static class HtmlHelperExtensions
    {
        ///<summary>
        /// Adds a partial view script to the Http context to be rendered in the parent view
        /// </summary>

        public static IHtmlHelper Script(this IHtmlHelper htmlHelper, string template)
        {
            htmlHelper.ViewContext.HttpContext.Items["_script_" + Guid.NewGuid()] = template;
            return null;
        }

        ///<summary>
        /// Adds partial view links (CSS) to the Http context to be rendered in the parent view
        /// </summary>

        public static IHtmlHelper Link(this IHtmlHelper htmlHelper, string template)
        {
            htmlHelper.ViewContext.HttpContext.Items["_link_" + Guid.NewGuid()] = template;
            return null;
        }

        ///<summary>
        /// Renders any scripts used within the partial views
        /// </summary>

        /// 
        public static IHtmlHelper RenderPartialViewScripts(this IHtmlHelper htmlHelper)
        {
            foreach (object key in htmlHelper.ViewContext.HttpContext.Items.Keys)
            {
                if (key.ToString().StartsWith("_script_") || key.ToString().StartsWith("_link_"))
                {
                    var template = htmlHelper.ViewContext.HttpContext.Items[key] as string;
                    if (template != null)
                    {
                        htmlHelper.ViewContext.Writer.Write(template);
                    }
                }
            }
            return null;
        }
    }
}
