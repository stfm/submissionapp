﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs.PosterHall
{
    public class PosterDTO
    {
        public int                  SubmissionId                { get; set; }
        public string               SubmissionTitle             { get; set; }
        public string               SessionCode                 { get; set; }
        public string               SessionTrack                { get; set; }
        public string               SubmissionAbstract          { get; set; }
        public string               STFMUserId                  { get; set; }
        public string               MaterialType                { get; set; }
        public string               MaterialValue               { get; set; }
        public ParticipantRoleType  ParticipantRoleType         { get; set; }
    }
}
