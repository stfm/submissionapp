﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels

{
    /// <summary>
    /// Stores data for each incomplete submission
    /// that we need to show in the view.
    /// </summary>
    public class ReviewIncomplete
    {

        public User Reviewer { get; set; }

        public int NumberOfReviewsNotDone { get; set; }

        public ReviewIncomplete()
        {
        }

    }
}