﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class add_auditlogparenteneity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AuditLogParentEntities",
                columns: table => new
                {
                    AuditLogParentEntityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AuditLogId = table.Column<int>(nullable: false),
                    ParentEntityId = table.Column<int>(nullable: false),
                    ParentEntityType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditLogParentEntities", x => x.AuditLogParentEntityId);
                    table.ForeignKey(
                        name: "FK_AuditLogParentEntities_AuditLogs_AuditLogId",
                        column: x => x.AuditLogId,
                        principalTable: "AuditLogs",
                        principalColumn: "AuditLogId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AuditLogParentEntities_AuditLogId",
                table: "AuditLogParentEntities",
                column: "AuditLogId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuditLogParentEntities");
        }
    }
}
