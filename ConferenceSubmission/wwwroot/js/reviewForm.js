﻿$(document).ready(function () {
    $(".body-content").removeClass("container");
    loadReviewerPreferences();
});


var setIsSaveAndNextToTrue = function () {
    $("#isSaveAndNextInput").val("true");
}

var updateReviewAndSubmissionContainerSizes = function (increaseSubmissionContainer) {

    //The parameter "increaseSubmissionContainer" is a boolean. If it's true, we're increasing the size of the Submission Container
    //and decreasing the size of the Review Container. If it's false, then the opposite is true.

    var containerToDecrease = increaseSubmissionContainer ? "#reviewContainer" : "#submissionContainer";
    var containerToIncrease = increaseSubmissionContainer ? "#submissionContainer" : "#reviewContainer";

    var containerToDecreaseClassList = $(containerToDecrease).attr('class').split(/\s+/);

    $.each(containerToDecreaseClassList, function (index, item) {
        if (item.includes('col-')) {
            var colSize = parseInt(item.slice(item.lastIndexOf('-') + 1));
            if (colSize === 3) {
                return;
            }
            else {
                var newColSize = colSize - 1;
                var classToAdd = "col-md-" + newColSize;

                $(containerToDecrease).removeClass(item);
                $(containerToDecrease).addClass(classToAdd);

                $(containerToIncrease).removeClass('col-md-' + (12 - colSize));
                $(containerToIncrease).addClass('col-md-' + (12 - newColSize));

                var reviewContainerColumnSize = increaseSubmissionContainer ? newColSize : 12 - newColSize;
                var submissionContainerColumnSize = !increaseSubmissionContainer ? newColSize : 12 - newColSize;

                updateReviewAndSubmissionContainerColumnSizesInLocalStorage(reviewContainerColumnSize, submissionContainerColumnSize);
            }
        }
    });
}

var toggleHideSubmission = function () {
    removeColumnClassesFromSubmissionAndReviewContainers();

    if ($("#submissionContainer").css("display") === "none") {

        var submissionContainerColumnClass = getSubmissionContainerColumnSizeFromLocalStorage() + getReviewContainerColumnSizeFromLocalStorage() !== 12 ? 'col-md-6' : 'col-md-' + getSubmissionContainerColumnSizeFromLocalStorage();
        var reviewContainerColumnClass = getSubmissionContainerColumnSizeFromLocalStorage() + getReviewContainerColumnSizeFromLocalStorage() !== 12 ? 'col-md-6' : 'col-md-' + getReviewContainerColumnSizeFromLocalStorage();

        $("#submissionContainer").addClass(submissionContainerColumnClass).show();
        $("#reviewContainer").addClass(reviewContainerColumnClass);
        $("#reviewDisplayBox").addClass('splitDisplayBox');

        $("#toggleSubmissionDisplayIcon").removeClass("fa-eye");
        $("#toggleSubmissionDisplayIcon").addClass("fa-eye-slash");

        $("#toggleSubmissionDisplayText").text("Hide Submission");

        $("#decreaseReviewContainerButton").show();

        updateHideSubmissionStatusInLocalStorage(false);
    }
    else {
        hideSubmission();
        updateHideSubmissionStatusInLocalStorage(true);
    }
}

var updateReviewAndSubmissionContainerColumnSizesInLocalStorage = function (reviewContainerColumnSize, submissionContainerColumnSize) {
    var conferenceAdmin = JSON.parse(localStorage.getItem("conferenceAdmin"));

    if (!conferenceAdmin) {
        conferenceAdmin = {
            "reviewerSettings": {
                "reviewContainerColumnSize": reviewContainerColumnSize,
                "submissionContainerColumnSize": submissionContainerColumnSize
            }
        }
    }
    else {
        if (!conferenceAdmin.reviewerSettings) {
            conferenceAdmin.reviewerSettings = {
                "reviewContainerColumnSize": reviewContainerColumnSize,
                "submissionContainerColumnSize": submissionContainerColumnSize
            };
        }
        else {
            conferenceAdmin.reviewerSettings.reviewContainerColumnSize = reviewContainerColumnSize;
            conferenceAdmin.reviewerSettings.submissionContainerColumnSize = submissionContainerColumnSize;
        }
    }
    localStorage.setItem("conferenceAdmin", JSON.stringify(conferenceAdmin));
}

var updateHideSubmissionStatusInLocalStorage = function (isSubmissionHidden) {
    var conferenceAdmin = JSON.parse(localStorage.getItem("conferenceAdmin"));

    if (!conferenceAdmin) {
        conferenceAdmin = {
            "reviewerSettings": {
                "isSubmissionHidden": isSubmissionHidden
            }
        }
    }
    else {
        if (!conferenceAdmin.reviewerSettings) {
            conferenceAdmin.reviewerSettings = {
                "isSubmissionHidden": isSubmissionHidden
            };
        }
        else {
            conferenceAdmin.reviewerSettings.isSubmissionHidden = isSubmissionHidden;
        }
    }
    localStorage.setItem("conferenceAdmin", JSON.stringify(conferenceAdmin));
}

var loadReviewerPreferences = function () {
    if (getIsSubmissionHiddenFromLocalStorage()) {
        if (!window.matchMedia('(max-width: 767px)').matches) {
            hideSubmission();
        }
        return;
    }
    if (getSubmissionContainerColumnSizeFromLocalStorage() !== -1 && getReviewContainerColumnSizeFromLocalStorage() !== -1 ) {
        removeColumnClassesFromSubmissionAndReviewContainers();

        $("#submissionContainer").addClass("col-md-" + getSubmissionContainerColumnSizeFromLocalStorage());
        $("#reviewContainer").addClass("col-md-" + getReviewContainerColumnSizeFromLocalStorage());
    }
}

// Helper Methods

var removeColumnClassesFromSubmissionAndReviewContainers = function () {
    var submissionContainerClassList = $("#submissionContainer").attr('class').split(/\s+/);
    var reviewContainerClassList = $("#reviewContainer").attr('class').split(/\s+/);

    $.each(submissionContainerClassList, function (index, item) {
        if (item.includes('col-')) {
            $("#submissionContainer").removeClass(item);
        }
    });
    $.each(reviewContainerClassList, function (index, item) {
        if (item.includes('col-')) {
            $("#reviewContainer").removeClass(item);
        }
    });
}

var hideSubmission = function () {
    $("#submissionContainer").hide();
    $("#reviewContainer").addClass('col-md-12');
    $("#reviewDisplayBox").removeClass('splitDisplayBox');

    $("#toggleSubmissionDisplayIcon").addClass("fa-eye");
    $("#toggleSubmissionDisplayIcon").removeClass("fa-eye-slash");

    $("#toggleSubmissionDisplayText").text("Show Submission");

    $("#decreaseReviewContainerButton").hide();
}

var getSubmissionContainerColumnSizeFromLocalStorage = function() {
    var conferenceAdmin = JSON.parse(localStorage.getItem("conferenceAdmin"));

    if (!conferenceAdmin || !conferenceAdmin.reviewerSettings || !conferenceAdmin.reviewerSettings.submissionContainerColumnSize) {
        return -1;
    }

    return conferenceAdmin.reviewerSettings.submissionContainerColumnSize;
}

var getReviewContainerColumnSizeFromLocalStorage = function () {
    var conferenceAdmin = JSON.parse(localStorage.getItem("conferenceAdmin"));

    if (!conferenceAdmin || !conferenceAdmin.reviewerSettings || !conferenceAdmin.reviewerSettings.reviewContainerColumnSize) {
        return -1;
    }

    return conferenceAdmin.reviewerSettings.reviewContainerColumnSize;
}

var getIsSubmissionHiddenFromLocalStorage = function () {
    var conferenceAdmin = JSON.parse(localStorage.getItem("conferenceAdmin"));
    
    if (!conferenceAdmin || !conferenceAdmin.reviewerSettings || !conferenceAdmin.reviewerSettings.isSubmissionHidden) {
        return false;
    }

    return conferenceAdmin.reviewerSettings.isSubmissionHidden;
}