﻿using ConferenceSubmission.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Controllers
{
    [Route("api/posterhall")]
    public class PosterHallAPIController : Controller
    {
        private readonly IPosterHallService _posterHallService;

        public PosterHallAPIController(IPosterHallService posterHallService)
        {
            _posterHallService = posterHallService;
        }

        [EnableCors]
        [HttpGet("category/{submissionCategoryId:int}")]
        public IActionResult GetPostersByCategory(int submissionCategoryId)
        {
            var posters = _posterHallService.GetSubmissionsForPosterHallCategory(submissionCategoryId);

            return Ok(posters);
        }
     }
}
