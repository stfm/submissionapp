﻿using System;
namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsForCategory
    {
        public string category { get; set; }
        public int categoryId { get; set; }
        public int submissionsCompleted { get; set; }
        public int submissionsNotFinished { get; set;  }
        public int totalSubmissions { get; set; }

        public int submissionsAccepted { get; set; }

        public int submissionsRejected { get; set; }

        public int submissionsCancelled { get; set; }

        public int submissionsWithdrawn { get; set; }

        public int submissionsPaidFor { get; set; }
    }
}
