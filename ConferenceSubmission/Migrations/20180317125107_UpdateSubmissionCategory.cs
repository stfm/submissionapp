﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class UpdateSubmissionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Submissions_AcceptedCategoryId",
                table: "Submissions",
                column: "AcceptedCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_SubmissionCategories_AcceptedCategoryId",
                table: "Submissions",
                column: "AcceptedCategoryId",
                principalTable: "SubmissionCategories",
                principalColumn: "SubmissionCategoryId",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_SubmissionCategories_AcceptedCategoryId",
                table: "Submissions");

            migrationBuilder.DropIndex(
                name: "IX_Submissions_AcceptedCategoryId",
                table: "Submissions");
        }
    }
}
