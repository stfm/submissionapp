﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class DataTableScriptsOptions
    {
        public bool IncludeDownloadAsCSVScripts { get; set; }
    }
}
