﻿using ConferenceSubmission.BindingModels;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Controllers
{
    [Authorize("ConferenceAdmin")]
    public class ITAdminController : Controller
    {
        private readonly IAnnouncementService _announcementService;
        public ITAdminController(IAnnouncementService announcementService)
        {
            _announcementService = announcementService;
        }

        public IActionResult ManageAnnouncements()
        {
            var model = new ManageAnnouncementsViewModel();
            model.Announcements = _announcementService.GetAll();

            return View(model);
        }

        [HttpPost]
        public IActionResult CreateAnnouncement(Announcement announcement)
        {
            if (ModelState.IsValid)
            {
                _announcementService.Create(announcement);
                return PartialView("AllAnnouncements", _announcementService.GetAll());
            }
            return BadRequest();
        }

        [HttpPost]
        public IActionResult DeleteAnnouncement(int announcementId)
        {
            if(announcementId > 0)
            {
                _announcementService.Delete(announcementId);
                return PartialView("AllAnnouncements", _announcementService.GetAll());
            }
            return BadRequest();
        }

        public IActionResult UpdateAnnouncement(Announcement announcement)
        {
            if (ModelState.IsValid)
            {
                _announcementService.Update(announcement);
                return PartialView("AllAnnouncements", _announcementService.GetAll());
            }
            return BadRequest();
        }
    }
}
