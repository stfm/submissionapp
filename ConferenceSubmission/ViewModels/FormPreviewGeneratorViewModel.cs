﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class FormPreviewGeneratorViewModel
    {
        public FormPreviewGeneratorViewModel()
        {
            FormFields = new List<FormField>();
        }
        public string ConferenceName { get; set; }
        public int ConferenceId { get; set; }
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryAbstractInstructions { get; set; }
        public int CategoryAbstractMaxLength { get; set; }
        public string CategoryInstructions { get; set; }

        public List<FormField> FormFields{ get; set; }
        public FormPreviewMode FormPreviewMode { get; set; }
    }

    public enum FormPreviewMode
    {
        PROPOSAL,
        REVIEW
    }
}
