﻿using ConferenceSubmission.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Data
{
    public class VirtualMaterialRepository : IVirtualMaterialRepository
    {

        private readonly AppDbContext _appDbContext;

        public VirtualMaterialRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public int AddVirtualMaterial(VirtualMaterial virtualMaterial)
        {
            _appDbContext.Add(virtualMaterial);

            _appDbContext.SaveChanges();

            return virtualMaterial.VirtualMaterialId;
        }

        public void DeleteVirtualMaterial(int submissionId)
        {
            VirtualMaterial virtualMaterial = _appDbContext.VirtualMaterials.FirstOrDefault(vm => vm.SubmissionId == submissionId);

            _appDbContext.Remove<VirtualMaterial>(virtualMaterial);

            _appDbContext.SaveChanges();

        }

        public VirtualMaterial GetVirtualMaterial(int submissionId)
        {
            return _appDbContext.VirtualMaterials.FirstOrDefault(vm => vm.SubmissionId == submissionId);
        }

        public int UpdateVirtualMaterial(VirtualMaterial virtualMaterial)
        {
            _appDbContext.Update<VirtualMaterial>(virtualMaterial);

            _appDbContext.SaveChanges();

            return virtualMaterial.VirtualMaterialId;
        }

        public IEnumerable<VirtualMaterial> GetVirtualMaterials(int submissionId)
        {

            return _appDbContext.VirtualMaterials.Where(vm => vm.SubmissionId == submissionId);


        }

        public IEnumerable<VirtualMaterial> GetVideoFileRecords(int conferenceId)
        {
            return _appDbContext.VirtualMaterials.Include(s => s.Submission)
            .ThenInclude(s => s.Conference).Where(vm => vm.MaterialType == "video" && vm.Submission.Conference.ConferenceId == conferenceId);

        }

        public void DeleteVirtualMaterial(VirtualMaterial virtualMaterial)
        {
            _appDbContext.Remove(virtualMaterial);

            _appDbContext.SaveChanges();
        }
    }
}
