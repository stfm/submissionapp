﻿using System;
namespace ConferenceSubmission.Models
{
    /// <summary>
    /// What is the Submission Participant 
    /// allowed to do with a submission.
    /// </summary>
    public enum ParticipantSubmissionRight
    {

        VIEW_ONLY,

        ALL,

        VIEW_UPDATE_PRESENTERS,

        PENDING_VIRTUAL_DECISION,

        WILL_PRESENT_VIRTUALLY,

        UPDATE_SUBMISSION_PRESENTATION_METHODS

    }
}
