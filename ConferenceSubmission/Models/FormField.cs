﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ConferenceSubmission.Models
{
    public class FormField
    {
        public FormField()
        {

            FormFieldProperties = new List<FormFieldProperty>();
        }

        public int FormFieldId { get; set; }

        public int SubmissionCategoryId { get; set; }

        public SubmissionCategory SubmissionCategory { get; set; }

        /// <summary>
        /// Either proposal or review - determines if this
        /// form field should be on the proposal or the review form.
        /// </summary>
        /// <value>The form field role.</value>
        [StringLength(50)]
        [Required]
        public string FormFieldRole { get; set; }

        /// <summary>
        /// Type of HTML form element - for example Text or TextArea
        /// or DropDown
        /// </summary>
        /// <value>The type of the form field.</value>
        [StringLength(50)]
        [Required]
        public string FormFieldType { get; set; }

        [Required]
        public int FormFieldSortOrder { get; set; }

        [Required]
        public bool FormFieldDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:ConferenceSubmission.Models.FormField"/> requires an answer.
        /// We can use this value to check that whether or not the user has answered category proposal questions 
        /// that require an answer.
        /// </summary>
        /// <value><c>true</c> if answer required; otherwise, <c>false</c>.</value>
        public bool AnswerRequired { get; set; }

        public List<FormFieldProperty> FormFieldProperties { get; set; }

        #region Helper methods to simplify code in the view
        /// <summary>
        /// Gets the property value for the FormFieldProperty 
        /// that has a property name matching the provided propertyName.
        /// </summary>
        /// <returns>The property value or an empty string if a 
        /// FormFieldProperty was not found for the provided property name.</returns>
        /// <param name="propertyName">FormFieldProperty Property name.</param>
        public string GetPropertyValue(string propertyName)
        {
            var property = FormFieldProperties.FirstOrDefault(f => f.PropertyName.ToLower() == propertyName.ToLower());
            return property == null ? string.Empty : property.PropertyValue;
        }
        #endregion

    }
}
