﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace ConferenceSubmission.Services
{
	public class ParticipantService : IParticipantService
    {

        private readonly ISubmissionService _submissionService;
		private readonly IParticipantRepository _participantRepository;
        private readonly IConfiguration _configuration;

		public ParticipantService(IParticipantRepository participantRepository, ISubmissionService submissionService, IConfiguration configuration)
        {

            _submissionService = submissionService;
			_participantRepository = participantRepository;
            _configuration = configuration;
        }

		public int CreateParticipant(Participant participant)
		{
			return _participantRepository.CreateParticipant(participant);
		}


        public string GetAddParticipantUrl(string submissionId, string host)
        {
           
            string addParticipantUrl = "https://" + host + "/Presenter/AddParticipant?SubmissionId=" + submissionId;


            return addParticipantUrl;

        }

        public Participant GetOrCreateParticipant(string stfmUserId)
        {
            var participant = GetParticipantByStfmUserId(stfmUserId);

            if(participant == null)
            {
                participant = new Participant { StfmUserId = stfmUserId };
                participant.ParticipantId = CreateParticipant(participant);
            }

            return participant;
        }

        public Participant GetParticipantByParticipantId(int participantId)
        {
            return _participantRepository.GetParticipantByParticipantId(participantId);
        }

        public Participant GetParticipantByStfmUserId(string StfmUserId)
		{
			return _participantRepository.GetParticipantByStfmUserId(StfmUserId);

		}

		public ParticipantRole GetParticipantRole(ParticipantRoleType participantRoleName)
		{

			return _participantRepository.GetParticipantRole(participantRoleName);

		}
        
		public List<ParticipantRole> GetParticipantRoles(string submissionRole)
        {


            List<ParticipantRole> participantRoles = new List<ParticipantRole>();
            ParticipantRole participantRoleSubmitter = GetParticipantRole(ParticipantRoleType.SUBMITTER);
            ParticipantRole participantRoleMainPresenter = GetParticipantRole(ParticipantRoleType.LEAD_PRESENTER);
            ParticipantRole participantRolePresenter = GetParticipantRole(ParticipantRoleType.PRESENTER);
            ParticipantRole participantRoleAuthor = GetParticipantRole(ParticipantRoleType.AUTHOR);

            switch (submissionRole)
            {

                case "submitter-only":
                    participantRoles.Add(participantRoleSubmitter);
                    break;

                case "submitter-mainpresenter":
                    participantRoles.Add(participantRoleSubmitter);
                    participantRoles.Add(participantRoleMainPresenter);
                    break;

                case "submitter-copresenter":
                    participantRoles.Add(participantRoleSubmitter);
                    participantRoles.Add(participantRolePresenter);
                    break;
                case "submitter-author":
                    participantRoles.Add(participantRoleSubmitter);
                    participantRoles.Add(participantRoleAuthor);
                    break;

            }



            return participantRoles;

        }

        public AssignedParticipantRolesType GetAssignedParticipantRolesType(int submissionId, int submissionParticipantId)
        {

            var roles = _submissionService.GetSubmission(submissionId)
                ?.ParticipantLink
                ?.FirstOrDefault(p => p.SubmissionParticipantId == submissionParticipantId)
                ?.SubmissionParticipantToParticipantRolesLink
                ?.Select(l => l.ParticipantRole.ParticipantRoleName)
                ?.ToList() ?? new List<ParticipantRoleType>();

            //Submitter Only
            if (roles.Any(r => r == ParticipantRoleType.SUBMITTER) && roles.Where(r => r != ParticipantRoleType.AUTHOR).Count() == 1)
            {
                return AssignedParticipantRolesType.SUBMITTER_ONLY;
            }
            //Submitter and Lead Presenter
            if (roles.Any(r => r == ParticipantRoleType.SUBMITTER) && roles.Any(r => r == ParticipantRoleType.LEAD_PRESENTER))
            {
                return AssignedParticipantRolesType.SUBMITTER_AND_LEADPRESENTER;
            }
            //Submitter and Presenter
            if (roles.Any(r => r == ParticipantRoleType.SUBMITTER) && roles.Any(r => r == ParticipantRoleType.PRESENTER))
            {
                return AssignedParticipantRolesType.SUBMITTER_AND_PRESENTER;
            }
            //Lead Presenter Only
            if (roles.Any(r => r == ParticipantRoleType.LEAD_PRESENTER) && roles.Where(r => r != ParticipantRoleType.AUTHOR).Count() == 1)
            {
                return AssignedParticipantRolesType.LEADPRESENTER_ONLY;
            }
            //Presenter Only
            return AssignedParticipantRolesType.PRESENTER_ONLY;

        }

        public List<ParticipantRoleType> GetParticipantsRolesBySubmission(int submissionId, string stfmUserId)
        {

            return _submissionService
                .GetSubmission(submissionId)
                ?.ParticipantLink
                ?.FirstOrDefault(p => p.Participant.StfmUserId == stfmUserId)
                ?.SubmissionParticipantToParticipantRolesLink.Select(l => l.ParticipantRole.ParticipantRoleName)
                ?.ToList()
                ?? new List<ParticipantRoleType>();

        }

        public List<string> GetParticipantRoleDefinitions()
        {
            var participantRoleDefinitionsSection = _configuration.GetSection("ParticipantRoleDefinitions");

            var participantRoleDefinitions = participantRoleDefinitionsSection
                ?.GetChildren()
                ?.Select(c => $"<strong>{(c.Key.Length <= 1 ? c.Key : c.Key.Substring(1, c.Key.Length-1))}:</strong> {c.Value}")
                ?.ToList() ?? new List<string>();

            return participantRoleDefinitions;
        }

        public List<string> GetAddParticipantRoleDefinitions()
        {
            var participantRoleDefinitionsSection = _configuration.GetSection("AddParticipantRoleDefinitions");

            var participantRoleDefinitions = participantRoleDefinitionsSection
                ?.GetChildren()
                ?.Select(c => $"<strong>{(c.Key.Length <= 1 ? c.Key : c.Key.Substring(1, c.Key.Length - 1))}:</strong> {c.Value}")
                ?.ToList() ?? new List<string>();

            return participantRoleDefinitions;
        }

        public List<Participant> GetParticipants()
        {
            return _participantRepository.GetParticipants.ToList();
        }
    }
}
