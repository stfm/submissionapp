﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class DisclosureRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public DisclosureRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;

        }

        [Fact]
        public void GetDisclosureByIdTest()
        {


            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                DisclosureRepository disclosureRepository = new DisclosureRepository(context);

                Disclosure disclosure = disclosureRepository.GetDisclosure(1);

                Output.WriteLine("Disclosure date is " + disclosure.DisclosureDate);

                Assert.NotNull(disclosure);

                Assert.Equal("user1", disclosure.StfmUserId);


            }

        }


        [Fact]
        public void GetDisclosureByStfmUserIdTest()
        {


            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                DisclosureRepository disclosureRepository = new DisclosureRepository(context);

                Disclosure disclosure = disclosureRepository.GetDisclosure("user1");

                Output.WriteLine("Disclosure date is " + disclosure.DisclosureDate);

                Assert.NotNull(disclosure);

                Assert.Equal("user1", disclosure.StfmUserId);


            }

        }

        [Fact]
        public void GetDisclosureNotFound()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                DisclosureRepository disclosureRepository = new DisclosureRepository(context);

                Disclosure disclosure = disclosureRepository.GetDisclosure("user5");

                Assert.Null(disclosure);

            }
        }


        [Fact]
        public void AddDisclosureTest() {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            int disclosureId = 0;


            using (var context = new AppDbContext(options))
            {

                DisclosureRepository disclosureRepository = new DisclosureRepository(context);

                Disclosure disclosure = new Disclosure
                {

                    DisclosureDate = DateTime.Parse("Jan 15, 2018"),
                    StfmUserId = "user1",
                    Part1Answer = "A",
                    Part3Answer = "A",
                    Part5Answer = "A",
                    FullName = "Bruce Phillips"


                };


                disclosureId = disclosureRepository.AddDisclosure(disclosure);

                Assert.True(disclosureId > 0);

            }

            using (var context = new AppDbContext(options))
            {

                DisclosureRepository disclosureRepository = new DisclosureRepository(context);

                Disclosure disclosure = disclosureRepository.GetDisclosure(disclosureId);

                Output.WriteLine("Full name is " + disclosure.FullName);

                Assert.Equal("Bruce Phillips", disclosure.FullName);

            }


        }

        [Fact]
        public void UpdateDisclosureTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                DisclosureRepository disclosureRepository = new DisclosureRepository(context);

                Disclosure disclosure = disclosureRepository.GetDisclosure("user1");

                Output.WriteLine("Disclosure full name is " + disclosure.FullName);

                disclosure.FullName = "Bruce Phillips Updated";

                disclosureRepository.UpdateDisclosure(disclosure);


            }

            using (var context = new AppDbContext(options))
            {
                DisclosureRepository disclosureRepository = new DisclosureRepository(context);

                Disclosure disclosure = disclosureRepository.GetDisclosure("user1");

                Output.WriteLine("Disclosure full name is " + disclosure.FullName);

                Assert.Equal("Bruce Phillips Updated", disclosure.FullName);

            }
        }


        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                context.AddRange(
                    new Disclosure
                    {

                        DisclosureId = 1,
                        DisclosureDate = DateTime.Parse("Jan 15, 2018"),
                        StfmUserId = "user1",
                    Part1Answer = "A",
                    Part3Answer = "A",
                    Part5Answer = "A",
                    FullName="Bruce Phillips"
                         

                    }


                );

                context.SaveChanges();
            }


            return options;
        }
    }
}
