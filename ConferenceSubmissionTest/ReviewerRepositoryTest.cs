﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using ConferenceSubmission.Data;
using System.Linq;
using System.Collections.Generic;
using TimeZoneConverter;

namespace ConferenceSubmissionTest
{
    public class ReviewerRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public ReviewerRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {
            Output = output;

        }

        [Fact]
        public void GetReviewersTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            var reviewers = new List<Reviewer>();

            using (var context = new AppDbContext(options))
            {

                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                reviewers = reviewerRepository.GetReviewers.ToList();
            }

            Assert.True(reviewers.Count() > 0);

            reviewers.ForEach(x => Output.WriteLine("Reviewers: " + x.ReviewerId));
        }

        [Fact]
        public void CreateParticipantTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ReviewerRepository reviewerRepository = new ReviewerRepository(context);


                Reviewer reviewer = new Reviewer { StfmUserId = "3007" };

                int reviewerId = reviewerRepository.CreateReviewer(reviewer);

                Output.WriteLine("Reviewer id is " + reviewerId);

                Reviewer reviewerFromDb = reviewerRepository.GetReviewerByStfmUserId("3007");

                Assert.Equal(reviewerId, reviewerFromDb.ReviewerId);


            }

        }

        [Fact]
        public void GetReviewerByStfmUserIdTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                Reviewer reviewer = reviewerRepository.GetReviewerByStfmUserId("3001");

                Output.WriteLine("Reviewer id is " + reviewer.ReviewerId);

                Assert.Equal(4001, reviewer.ReviewerId);


            }

        }



        /// <summary>
        /// This test is date-dependent because it should retrieve only those SubmissionReviewer
        /// assignments where the current datetime is less that or equal to the 
        /// conference's ConferenceSubmissionReviewEndDate.
        /// </summary>
        [Fact]
        public void GetAssignedSubmissions()
        {
            //arrange
            //This user has 5 submission review assignments. Three of them are for a conference whose 
            //ConferenceSubmissionReviewEndDate has not yet past. Two of them are for a conference whose
            //ConferenceSubmissionReviewEndDate has past. So we expect to get back three assigned submissions.
            var stfmUserId = "3004";
            var assignedSubmissions = new List<SubmissionReviewer>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                //Act
                assignedSubmissions = reviewerRepository.GetAssignedSubmissions(stfmUserId);

            }
            //Assert
            Assert.Equal(3, assignedSubmissions.Count);
        }


        [Fact]
        public void GetReviewsForConference()
        {

            var conferenceId = 1;

            var assignedSubmissions = new List<SubmissionReviewer>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                //Act
                assignedSubmissions = reviewerRepository.GetAssignedReviews(conferenceId);

            }
            //Assert
            Assert.Equal(3, assignedSubmissions.Count);
        }

        /// <summary>
        /// This test is date-dependent because it should retrieve only those SubmissionReviewer
        /// assignments where the current datetime is less than or equal to the 
        /// conference's ConferenceSubmissionReviewEndDate.
        /// This method tests SubmssionReview assignments for a conference whose ConferenceSubmissionReviewEndDate
        /// is equal to the current date. These SubmissionReview Assignments should be included because
        /// the ConferenceSubmissionReviewEndDate is less than or equal to the current date.
        /// </summary>
        [Fact]
        public void GetAssignedSubmissions_CurrentDateEqualsConferenceSubmissionReviewEndDate()
        {
            //arrange
            /// This reviewer has 2 submission reviewer assignments. These are for a conference
            /// whose ConferenceSubmissionReviewEndDate is equal to the current date. Both submission reviewer 
            /// assignments should be returned by the service.
            var stfmUserId = "3005";
            var assignedSubmissions = new List<SubmissionReviewer>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                //Act
                assignedSubmissions = reviewerRepository.GetAssignedSubmissions(stfmUserId);
            }

            //Assert
            Assert.Equal(2, assignedSubmissions.Count);
        }

        /// <summary>
        /// This test is date-dependent because it should retrieve only those SubmissionReviewer
        /// assignments where the current datetime is less than or equal to the 
        /// conference's ConferenceSubmissionReviewEndDate.
        /// This method tests SubmssionReview assignments for a conference where the current date is 
        /// one day past the ConferenceSubmissionReviewEndDate. These SubmissionReview Assignments should NOT be included because
        /// the current date is past the ConferenceSubmissionReviewEndDate.
        /// </summary>
        [Fact]
        public void GetAssignedSubmissions_CurrentDateOneDayGreaterThanConferenceSubmissionReviewEndDate()
        {
            //arrange
            /// This reviewer has 2 submission reviewer assignments. These assignments are for a conference
            /// where the current date is one day past the ConferenceSubmissionReviewEndDate. 
            var stfmUserId = "3006";

            var assignedSubmissions = new List<SubmissionReviewer>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                //Act
                assignedSubmissions = reviewerRepository.GetAssignedSubmissions(stfmUserId);
            }

            //Assert
            Assert.True(assignedSubmissions.Count == 0);
        }

        [Fact]
        public void ReviewerExists_ReturnsTrueForExistentUser()
        {
            //arrange
            var stmfUserId = "3001";

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                //Act
                var result = reviewerRepository.ReviewerExists(stmfUserId);

                //assert
                Assert.True(result);
            }
        }

        [Fact]
        public void ReviewerExists_ReturnsFalseForNonExistentUser()
        {
            //arrange
            var stmfUserId = "9999";

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                //Act
                var result = reviewerRepository.ReviewerExists(stmfUserId);

                //assert
                Assert.False(result);
            }
        }

        [Fact]
        public void GetNextReview_AssignSomeSubmissions_VerifyResults()
        {
            // Arrange
            var stfmUserId = "3004";

            DbContextOptions<AppDbContext> options = GetDbContextOptionsForTestingGetNextReview();

            using (var context = new AppDbContext(options))
            {
                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                // Act
                var results = reviewerRepository.GetNextReview(stfmUserId);

                // Assert
                Assert.NotNull(results);

                //The reviewer has three unstarted reviews for submissions 7, 8, and 9.
                //Sometimes complex linq queries re-order results in unexpected ways, we need to
                //ensure that one of the unstarted submissions is returned.
                Assert.True(results.submissionId == "7" || results.submissionId == "8" || results.submissionId == "9");
                Assert.Equal(3, results.categoryId);
            }
        }

        [Fact]
        public void GetNextReview_AssignThreeSubmissionsToReviewerWhereReviewEndDateIsPassedAndOneWhereItIsNot_VerifyResults()
        {
            // Arrange
            var stfmUserId = "3001";

            DbContextOptions<AppDbContext> options = GetDbContextOptionsForTestingGetNextReview();

            using (var context = new AppDbContext(options))
            {
                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                // Act
                var results = reviewerRepository.GetNextReview(stfmUserId);

                // Assert
                Assert.NotNull(results);
                Assert.Equal("4", results.submissionId);
                Assert.Equal(2, results.categoryId);
            }
        }

        [Fact]
        public void GetNextReview_AssignTwoSubmissionsToReviewerAndCompletedReviewsForEach_VerifyNextUnstartedReviewIsReturned()
        {
            // Arrange
            var stfmUserId = "3002";

            DbContextOptions<AppDbContext> options = GetDbContextOptionsForTestingGetNextReview();

            using (var context = new AppDbContext(options))
            {
                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                // Act
                var results = reviewerRepository.GetNextReview(stfmUserId);

                // Assert
                Assert.Null(results);
            }
        }

        [Fact]
        public void GetNextReview_AssignNoSubmissionsToReviewer_VerifyNextUnstartedReviewIsReturned()
        {
            // Arrange
            var stfmUserId = "3007";

            DbContextOptions<AppDbContext> options = GetDbContextOptionsForTestingGetNextReview();

            using (var context = new AppDbContext(options))
            {
                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                // Act
                var results = reviewerRepository.GetNextReview(stfmUserId);

                // Assert
                Assert.Null(results);
            }
        }

        [Fact]
        public void GetNextReview_AssignTwoSubmissionsToReviewerOneHasReviewAndOneIsPastReviewEndDate_VerifyNextUnstartedReviewIsReturned()
        {
            // Arrange
            var stfmUserId = "3008";

            DbContextOptions<AppDbContext> options = GetDbContextOptionsForTestingGetNextReview();

            using (var context = new AppDbContext(options))
            {
                ReviewerRepository reviewerRepository = new ReviewerRepository(context);

                // Act
                var results = reviewerRepository.GetNextReview(stfmUserId);

                // Assert
                Assert.Null(results);
            }
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                //Create some reviewers 

                context.Reviewers.AddRange(

                    new Reviewer { ReviewerId = 4001, StfmUserId = "3001" },

                    new Reviewer { ReviewerId = 4002, StfmUserId = "3002" },

                    new Reviewer { ReviewerId = 4003, StfmUserId = "3003" },

                    new Reviewer { ReviewerId = 4004, StfmUserId = "3004" },

                    new Reviewer { ReviewerId = 4005, StfmUserId = "3005" },

                    new Reviewer { ReviewerId = 4006, StfmUserId = "3006" }

                );

                context.SaveChanges();

                //Create some submission status types
                context.SubmissionStatuses.AddRange(
                    new SubmissionStatus { SubmissionStatusId = 1, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }
                    );

                //Create some conference types
                context.ConferenceTypes.AddRange(
                        new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Conference On Medical Student Education" },
                        new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Conference On Practice Improvement" },
                        new ConferenceType { ConferenceTypeId = 3, ConferenceTypeName = "STFM Mega Conference" },
                        new ConferenceType { ConferenceTypeId = 4, ConferenceTypeName = "STFM Regional Conference" },
                        new ConferenceType { ConferenceTypeId = 5, ConferenceTypeName = "STFM World Conference" }
                    );

                //create some conferences
                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceLongName = "2019 Conference On Medical Student Education",
                        ConferenceShortName = "2019 MSE",
                        ConferenceCFPEndDate = new DateTime(2018, 9, 21),
                        ConferenceSubmissionReviewEndDate = new DateTime(2018, 11, 21),
                        ConferenceId = 1,
                        ConferenceTypeId = 1
                    },
                    new Conference
                    {
                        ConferenceLongName = "2019 Conference On Practice Improvement",
                        ConferenceShortName = "2019 CPI",
                        ConferenceCFPEndDate = new DateTime(2019, 3, 21),
                        ConferenceSubmissionReviewEndDate = new DateTime(2019, 6, 21),
                        ConferenceId = 2,
                        ConferenceTypeId = 2
                    },
                    new Conference
                    {
                        ConferenceLongName = $"{DateTime.Now.Year} STFM Mega Conference",
                        ConferenceShortName = $"{DateTime.Now.Year} SMC",
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(3),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddMonths(6),
                        ConferenceId = 3,
                        ConferenceTypeId = 3
                    }
                    ,
                    new Conference
                    {
                        ConferenceLongName = $"{DateTime.Now.Year - 1} STFM Mega Conference",
                        ConferenceShortName = $"{DateTime.Now.Year - 1} SMC",
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(-9),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddMonths(-6),
                        ConferenceId = 4,
                        ConferenceTypeId = 3
                    },

                    //Note: The ConferenceSubmissionReviewEndDate is set to the current date at 11:59pm
                    new Conference
                    {
                        ConferenceLongName = $"{DateTime.Now.Year} STFM Regional Conference",
                        ConferenceShortName = $"{DateTime.Now.Year} SRC",
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(-3),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddHours(23).AddSeconds(59),
                        ConferenceId = 5,
                        ConferenceTypeId = 4
                    },

                    //Note: The ConferenceSubmissionReviewEndDate is set to one day behind the current date
                    new Conference
                    {
                        ConferenceLongName = $"{DateTime.Now.Year} STFM World Conference",
                        ConferenceShortName = $"{DateTime.Now.Year} SWC",
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(-3),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-1),
                        ConferenceId = 6,
                        ConferenceTypeId = 4
                    }
                    );
                context.SaveChanges();

                //create some conference categories
                context.SubmissionCategories.AddRange(
                    new SubmissionCategory
                    {
                        ConferenceId = 1,
                        SubmissionCategoryId = 1,
                        SubmissionCategoryName = "2019 MSE Lecture"
                    },
                    new SubmissionCategory
                    {
                        ConferenceId = 2,
                        SubmissionCategoryId = 2,
                        SubmissionCategoryName = "2019 CPI Workshop"
                    },
                    new SubmissionCategory
                    {
                        ConferenceId = 3,
                        SubmissionCategoryId = 3,
                        SubmissionCategoryName = $"{DateTime.Now.Year} CPI Workshop"
                    },
                    new SubmissionCategory
                    {
                        ConferenceId = 4,
                        SubmissionCategoryId = 4,
                        SubmissionCategoryName = $"{DateTime.Now.Year - 1} 2019 CPI Workshop"
                    },
                    new SubmissionCategory
                    {
                        ConferenceId = 5,
                        SubmissionCategoryId = 5,
                        SubmissionCategoryName = $"{DateTime.Now.Year} SRC Workshop"
                    },
                    new SubmissionCategory
                    {
                        ConferenceId = 6,
                        SubmissionCategoryId = 6,
                        SubmissionCategoryName = $"{DateTime.Now.Year} SRC Workshop"
                    });
                context.SaveChanges();

                //create some submission

                context.Submissions.AddRange(
                    new Submission
                    {
                        SubmissionId = 1,
                        SubmissionCategoryId = 1,
                        AcceptedCategoryId = 1,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Lorem ipsum dolor sit amet",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 1)
                    },
                    new Submission
                    {
                        SubmissionId = 2,
                        SubmissionCategoryId = 1,
                        AcceptedCategoryId = 1,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Suspendisse quis felis lectus",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 1)
                    },
                    new Submission
                    {
                        SubmissionId = 3,
                        SubmissionCategoryId = 1,
                        AcceptedCategoryId = 1,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Cras a viverra lectus",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 1)
                    },
                    new Submission
                    {
                        SubmissionId = 4,
                        SubmissionCategoryId = 2,
                        AcceptedCategoryId = 2,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Nunc consectetur neque nec elit varius",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 2)
                    },
                    new Submission
                    {
                        SubmissionId = 5,
                        SubmissionCategoryId = 2,
                        AcceptedCategoryId = 2,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Curabitur nec arcu consectetur purus vestibulum accumsan ut eget magna",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 2)
                    },
                    new Submission
                    {
                        SubmissionId = 6,
                        SubmissionCategoryId = 2,
                        AcceptedCategoryId = 2,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Aliquam tristique sapien vitae lacinia efficitur",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 2)
                    },
                    new Submission
                    {
                        SubmissionId = 7,
                        SubmissionCategoryId = 3,
                        AcceptedCategoryId = 3,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Vestibulum pulvinar ipsum fringilla ultrices varius",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 3)
                    },
                    new Submission
                    {
                        SubmissionId = 8,
                        SubmissionCategoryId = 3,
                        AcceptedCategoryId = 3,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Ut rhoncus mollis erat, vel mollis lorem finibus at",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 3)
                    },
                    new Submission
                    {
                        SubmissionId = 9,
                        SubmissionCategoryId = 3,
                        AcceptedCategoryId = 3,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Fusce ultrices, nulla vel imperdiet sollicitudin, libero elit ultrices odio",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 3)
                    },
                    new Submission
                    {
                        SubmissionId = 10,
                        SubmissionCategoryId = 4,
                        AcceptedCategoryId = 4,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Eget tincidunt ex diam dapibus purus",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 4)
                    },
                    new Submission
                    {
                        SubmissionId = 11,
                        SubmissionCategoryId = 4,
                        AcceptedCategoryId = 4,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Morbi ut iaculis ante",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 4)
                    },
                    new Submission
                    {
                        SubmissionId = 12,
                        SubmissionCategoryId = 5,
                        AcceptedCategoryId = 5,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Quisque dignissim faucibus vehicula",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 5)
                    },
                    new Submission
                    {
                        SubmissionId = 13,
                        SubmissionCategoryId = 5,
                        AcceptedCategoryId = 5,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Diam erat molestie felis",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 5)
                    },
                    new Submission
                    {
                        SubmissionId = 14,
                        SubmissionCategoryId = 6,
                        AcceptedCategoryId = 6,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Phasellus quis velit a dolor aliquam cursus",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 6)
                    },
                    new Submission
                    {
                        SubmissionId = 15,
                        SubmissionCategoryId = 6,
                        AcceptedCategoryId = 6,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Ut luctus, purus at facilisis pharetra",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 6)
                    }
                    );

                context.SaveChanges();

                //assign users to submissions to review
                context.SubmissionReviewer.AddRange(
                        new SubmissionReviewer { ReviewerId = 4001, SubmissionId = 1 },
                        new SubmissionReviewer { ReviewerId = 4001, SubmissionId = 2 },
                        new SubmissionReviewer { ReviewerId = 4001, SubmissionId = 3 },
                        new SubmissionReviewer { ReviewerId = 4001, SubmissionId = 4 },
                        new SubmissionReviewer { ReviewerId = 4002, SubmissionId = 5 },
                        new SubmissionReviewer { ReviewerId = 4002, SubmissionId = 6 },
                        new SubmissionReviewer { ReviewerId = 4003, SubmissionId = 6 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 7 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 8 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 9 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 10 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 11 },
                        new SubmissionReviewer { ReviewerId = 4005, SubmissionId = 12 },
                        new SubmissionReviewer { ReviewerId = 4005, SubmissionId = 13 },
                        new SubmissionReviewer { ReviewerId = 4006, SubmissionId = 14 },
                        new SubmissionReviewer { ReviewerId = 4006, SubmissionId = 15 }
                    );



                context.SaveChanges();

            }

            return options;
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptionsForTestingGetNextReview()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                //Create some reviewers 

                context.Reviewers.AddRange(

                    new Reviewer { ReviewerId = 4001, StfmUserId = "3001" },

                    new Reviewer { ReviewerId = 4002, StfmUserId = "3002" },

                    new Reviewer { ReviewerId = 4003, StfmUserId = "3003" },

                    new Reviewer { ReviewerId = 4004, StfmUserId = "3004" },

                    new Reviewer { ReviewerId = 4005, StfmUserId = "3005" },

                    new Reviewer { ReviewerId = 4006, StfmUserId = "3006" },

                    new Reviewer { ReviewerId = 4007, StfmUserId = "3007" },

                    new Reviewer { ReviewerId = 4008, StfmUserId = "3008" }

                );

                context.SaveChanges();

                //Create some submission status types
                context.SubmissionStatuses.AddRange(
                    new SubmissionStatus { SubmissionStatusId = 1, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }
                    );

                //Create some conference types
                context.ConferenceTypes.AddRange(
                        new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Conference On Medical Student Education" },
                        new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Conference On Practice Improvement" },
                        new ConferenceType { ConferenceTypeId = 3, ConferenceTypeName = "STFM Mega Conference" },
                        new ConferenceType { ConferenceTypeId = 4, ConferenceTypeName = "STFM Regional Conference" },
                        new ConferenceType { ConferenceTypeId = 5, ConferenceTypeName = "STFM World Conference" }
                    );

                //create some conferences

                TimeZoneInfo targetZone = TZConvert.GetTimeZoneInfo("Central Standard Time");
                TimeSpan offset = targetZone.GetUtcOffset(DateTime.Now);
                DateTime dateTimeNowMinusOneMinuteCST = DateTime.Now.AddHours(offset.TotalHours).AddMinutes(-1);

                var conferenceWithConferenceSubmissionReviewEndDateThatIsOneMinuteLessThanTheCurrentDateTime =
                    new Conference
                    {
                        ConferenceLongName = $"{DateTime.Now.Year} Conference On Medical Student Education",
                        ConferenceShortName = $"{DateTime.Now.Year} MSE",
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(3),
                        ConferenceSubmissionReviewEndDate = dateTimeNowMinusOneMinuteCST,
                        ConferenceId = 1,
                        ConferenceTypeId = 1
                    };
                context.Conferences.Add(conferenceWithConferenceSubmissionReviewEndDateThatIsOneMinuteLessThanTheCurrentDateTime);

                var conferenceWithConferenceSubmissionReviewEndDateThatIsThreeMonthsInTheFuture =
                    new Conference
                    {
                        ConferenceLongName = $"{DateTime.Now.Year} Conference On Practice Improvement",
                        ConferenceShortName = $"{DateTime.Now.Year} CPI",
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(6),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddMonths(3),
                        ConferenceId = 2,
                        ConferenceTypeId = 2
                    };
                context.Conferences.Add(conferenceWithConferenceSubmissionReviewEndDateThatIsThreeMonthsInTheFuture);

                var conferenceWithConferenceSubmissionReviewEndDateThatIsSixMonthsInTheFuture =
                    new Conference
                    {
                        ConferenceLongName = $"{DateTime.Now.Year} STFM Mega Conference",
                        ConferenceShortName = $"{DateTime.Now.Year} SMC",
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(9),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddMonths(6),
                        ConferenceId = 3,
                        ConferenceTypeId = 3
                    };
                context.Conferences.Add(conferenceWithConferenceSubmissionReviewEndDateThatIsSixMonthsInTheFuture);

                context.SaveChanges();

                //create some conference categories
                context.SubmissionCategories.AddRange(
                    new SubmissionCategory
                    {
                        ConferenceId = 1,
                        SubmissionCategoryId = 1,
                        SubmissionCategoryName = "MSE Lecture"
                    },
                    new SubmissionCategory
                    {
                        ConferenceId = 2,
                        SubmissionCategoryId = 2,
                        SubmissionCategoryName = "CPI Workshop"
                    },
                    new SubmissionCategory
                    {
                        ConferenceId = 3,
                        SubmissionCategoryId = 3,
                        SubmissionCategoryName = "STFM Mega Conference Poster"
                    });
                context.SaveChanges();

                //create some submission

                context.Submissions.AddRange(
                    new Submission
                    {
                        SubmissionId = 1,
                        SubmissionCategoryId = 1,
                        AcceptedCategoryId = 1,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Lorem ipsum dolor sit amet",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 1)
                    },
                    new Submission
                    {
                        SubmissionId = 2,
                        SubmissionCategoryId = 1,
                        AcceptedCategoryId = 1,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Suspendisse quis felis lectus",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 1)
                    },
                    new Submission
                    {
                        SubmissionId = 3,
                        SubmissionCategoryId = 1,
                        AcceptedCategoryId = 1,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Cras a viverra lectus",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 1)
                    },
                    new Submission
                    {
                        SubmissionId = 4,
                        SubmissionCategoryId = 2,
                        AcceptedCategoryId = 2,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Nunc consectetur neque nec elit varius",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 2)
                    },
                    new Submission
                    {
                        SubmissionId = 5,
                        SubmissionCategoryId = 2,
                        AcceptedCategoryId = 2,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Curabitur nec arcu consectetur purus vestibulum accumsan ut eget magna",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 2)
                    },
                    new Submission
                    {
                        SubmissionId = 6,
                        SubmissionCategoryId = 2,
                        AcceptedCategoryId = 2,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Aliquam tristique sapien vitae lacinia efficitur",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 2)
                    },
                    new Submission
                    {
                        SubmissionId = 7,
                        SubmissionCategoryId = 3,
                        AcceptedCategoryId = 3,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Vestibulum pulvinar ipsum fringilla ultrices varius",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 3)
                    },
                    new Submission
                    {
                        SubmissionId = 8,
                        SubmissionCategoryId = 3,
                        AcceptedCategoryId = 3,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Ut rhoncus mollis erat, vel mollis lorem finibus at",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 3)
                    },
                    new Submission
                    {
                        SubmissionId = 9,
                        SubmissionCategoryId = 3,
                        AcceptedCategoryId = 3,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Fusce ultrices, nulla vel imperdiet sollicitudin, libero elit ultrices odio",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 3)
                    },
                    new Submission
                    {
                        SubmissionId = 10,
                        SubmissionCategoryId = 1,
                        AcceptedCategoryId = 1,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Eget tincidunt ex diam dapibus purus",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 1)
                    },
                    new Submission
                    {
                        SubmissionId = 11,
                        SubmissionCategoryId = 1,
                        AcceptedCategoryId = 1,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Morbi ut iaculis ante",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 1)
                    },
                    new Submission
                    {
                        SubmissionId = 12,
                        SubmissionCategoryId = 2,
                        AcceptedCategoryId = 2,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Quisque dignissim faucibus vehicula",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 2)
                    },
                    new Submission
                    {
                        SubmissionId = 13,
                        SubmissionCategoryId = 2,
                        AcceptedCategoryId = 2,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Diam erat molestie felis",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 2)
                    },
                    new Submission
                    {
                        SubmissionId = 14,
                        SubmissionCategoryId = 3,
                        AcceptedCategoryId = 3,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Phasellus quis velit a dolor aliquam cursus",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 3)
                    },
                    new Submission
                    {
                        SubmissionId = 15,
                        SubmissionCategoryId = 3,
                        AcceptedCategoryId = 3,
                        SubmissionStatusId = 1,
                        SubmissionTitle = "Ut luctus, purus at facilisis pharetra",
                        SubmissionAbstract = "Abstract goes here...",
                        Conference = context.Conferences.FirstOrDefault(c => c.ConferenceId == 3)
                    }
                    );

                context.SaveChanges();

                //assign users to submissions to review
                context.SubmissionReviewer.AddRange(
                        new SubmissionReviewer { ReviewerId = 4001, SubmissionId = 1 },//Review Exists
                        new SubmissionReviewer { ReviewerId = 4001, SubmissionId = 2 },
                        new SubmissionReviewer { ReviewerId = 4001, SubmissionId = 3 },
                        new SubmissionReviewer { ReviewerId = 4001, SubmissionId = 4 },
                        new SubmissionReviewer { ReviewerId = 4002, SubmissionId = 5 },//Review exists
                        new SubmissionReviewer { ReviewerId = 4002, SubmissionId = 6 },//Review exists
                        new SubmissionReviewer { ReviewerId = 4003, SubmissionId = 6 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 7 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 8 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 9 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 10 },
                        new SubmissionReviewer { ReviewerId = 4004, SubmissionId = 11 },
                        new SubmissionReviewer { ReviewerId = 4005, SubmissionId = 12 },
                        new SubmissionReviewer { ReviewerId = 4005, SubmissionId = 13 },
                        new SubmissionReviewer { ReviewerId = 4006, SubmissionId = 14 },
                        new SubmissionReviewer { ReviewerId = 4006, SubmissionId = 15 }, //Review exists
                        new SubmissionReviewer { ReviewerId = 4008, SubmissionId = 1 },
                        new SubmissionReviewer { ReviewerId = 4008, SubmissionId = 5 }   //Review exists
                    );

                context.SaveChanges();

                //Create review statuses

                context.ReviewStatuses.AddRange(new List<ReviewStatus>()
                {
                    new ReviewStatus
                    {
                        ReviewStatusId = 1,
                        ReviewStatusType = ReviewStatusType.NOT_STARTED
                    },
                    new ReviewStatus
                    {
                        ReviewStatusId = 2,
                        ReviewStatusType = ReviewStatusType.INCOMPLETE
                    },
                    new ReviewStatus
                    {
                        ReviewStatusId = 3,
                        ReviewStatusType = ReviewStatusType.COMPLETE
                    }
                });

                context.SaveChanges();

                //ReviewerIds = 4001-4006
                //SubmissionIds = 1-15
                var reviews = new List<Review>()
                {
                    new Review
                    {
                        ReviewId = 1,
                        ReviewerId = 4001,
                        SubmissionId = 1,
                        ReviewStatusId = 2,
                        ReviewDateTime = DateTime.Now
                    },
                    new Review
                    {
                        ReviewId = 2,
                        ReviewerId = 4002,                        
                        SubmissionId = 5,
                        ReviewStatusId = 2,
                        ReviewDateTime = DateTime.Now
                    },
                    new Review
                    {
                        ReviewId = 3,
                        ReviewerId = 4002,                        
                        SubmissionId = 6,
                        ReviewStatusId = 3,
                        ReviewDateTime = DateTime.Now
                    },
                    new Review
                    {
                        ReviewId = 4,
                        ReviewerId = 4006,                        
                        SubmissionId = 15,
                        ReviewStatusId = 3,
                        ReviewDateTime = DateTime.Now
                    },
                    new Review
                    {
                        ReviewId = 5,
                        ReviewerId = 4008,
                        SubmissionId = 5,
                        ReviewStatusId = 3,
                        ReviewDateTime = DateTime.Now
                    }
                };

                context.Reviews.AddRange(reviews);

                context.SaveChanges();

            }

            return options;
        }

    }
}
