﻿using AutoMapper.Configuration;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Serilog;

namespace ConferenceSubmission.Services
{
    public interface ISalesforceGetUsersService
    {
        Task<List<User>> GetPartitionedUsers(List<string> stfmUserIds, SalesforceAPIVariables salesforceAPIVariables);

        /// <summary>
        /// Get a collection of SalesForce users matching provided last name.
        /// </summary>
        /// <param name="lastName">last name</param>
        /// <param name="salesforceAPIVariables">SalesForce connection values</param>
        /// <returns></returns>
        Task<List<User>> GetUsersByLastName(string lastName, SalesforceAPIVariables salesforceAPIVariables);
    }

    public class SalesforceGetUsersService : ISalesforceGetUsersService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public SalesforceGetUsersService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<List<User>> GetPartitionedUsers(List<string> stfmUserIds, SalesforceAPIVariables salesforceAPIVariables)
        {
            //Build a SOQL query to fetch the users by their Ids
            var soqlQuery = $"SELECT+Contact.FirstName,Contact.LastName,Contact.Credentials__c,Contact.Other_Credentials__c,Contact.Email,Id,UserName+FROM+User+WHERE+Id+IN+({string.Join(',', stfmUserIds.Select(s => $"'{s}'"))})";
            
            //Log.Information("Salesforce Query is " + soqlQuery);
            
            string restUri = salesforceAPIVariables.ServiceUrl + $"/services/data/v56.0/query/?q={soqlQuery}";

            HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, restUri);

            //Add OAuth token to header
            httpRequest.Headers.Add("Authorization", "Bearer " + salesforceAPIVariables.OAuthToken);

            //Tell Salesforce to return JSON
            httpRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpClient httpClient = _httpClientFactory.CreateClient();

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequest);

            string result = await httpResponseMessage.Content.ReadAsStringAsync();
            
            //Log.Information("Result of calling SalesForce to get people is " + result);

            var records = JObject.Parse(result)["records"];

            var parsedUsers = records.Children().Select(c => new User
            {
                FirstName = c.SelectToken("Contact")?.SelectToken("FirstName")?.Value<string>() ?? string.Empty,
                LastName = c.SelectToken("Contact")?.SelectToken("LastName")?.Value<string>() ?? string.Empty,
                STFMUserId = c.Value<string>("Id"),
                Email = c.SelectToken("Contact")?.SelectToken("Email")?.Value<string>() ?? string.Empty,
                UserName = c.Value<string>("Username"),
                Credentials = $"{(c.SelectToken("Contact")?.SelectToken("Credentials__c")?.Value<string>() ?? string.Empty).Replace(";", ", ")}, {c.SelectToken("Contact")?.SelectToken("Other_Credentials__c")?.Value<string>() ?? string.Empty}".TrimEnd(new char[] { ',', ' ' })
            }).ToList();

            return parsedUsers;
        }

        public async Task<List<User>> GetUsersByLastName(string lastName, SalesforceAPIVariables salesforceAPIVariables)
        {
            //Build a SOQL query to fetch the users matching provided last name
            lastName = lastName.Replace("'", @"\'", System.StringComparison.InvariantCultureIgnoreCase);

            var soqlQuery = $"SELECT+Contact.FirstName,Contact.LastName,Contact.Credentials__c,Contact.Other_Credentials__c,Contact.Email,Id,UserName+FROM+User+WHERE+Contact.LastName+=+'{lastName}'";
            string restUri = salesforceAPIVariables.ServiceUrl + $"/services/data/v56.0/query/?q={soqlQuery}";

            HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, restUri);

            //Add OAuth token to header
            httpRequest.Headers.Add("Authorization", "Bearer " + salesforceAPIVariables.OAuthToken);

            //Tell Salesforce to return JSON
            httpRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpClient httpClient = _httpClientFactory.CreateClient();

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequest);

            string result = await httpResponseMessage.Content.ReadAsStringAsync();

            //Log.Information("Result of calling SalesForce to get people by last name of " + lastName + " is " + result);

            var records = JObject.Parse(result)["records"];

            var parsedUsers = records.Children().Select(c => new User
            {
                FirstName = c.SelectToken("Contact")?.SelectToken("FirstName")?.Value<string>() ?? string.Empty,
                LastName = c.SelectToken("Contact")?.SelectToken("LastName")?.Value<string>() ?? string.Empty,
                STFMUserId = c.Value<string>("Id"),
                Email = c.SelectToken("Contact")?.SelectToken("Email")?.Value<string>() ?? string.Empty,
                UserName = c.Value<string>("Username"),
                Credentials = $"{(c.SelectToken("Contact")?.SelectToken("Credentials__c")?.Value<string>() ?? string.Empty).Replace(";", ", ")}, {c.SelectToken("Contact")?.SelectToken("Other_Credentials__c")?.Value<string>() ?? string.Empty}".TrimEnd(new char[] { ',', ' ' })
            }).ToList();

            return parsedUsers;
        }
    }
}
