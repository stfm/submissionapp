﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class DisplaySubmissionCategoryViewModel
    {
        public string SubmittedCategory { get; set; }
        public string AcceptedCategory { get; set; }
    }
}
