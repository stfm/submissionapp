﻿using System;
using System.Collections.Generic;

namespace ConferenceSubmission.DTOs
{
    /// <summary>
    /// Stores data about an accepted
    /// submission that has been scheduled
    /// for presentation.
    /// </summary>
    public class Session
    {
        public Session()
        {
            Participants = new List<string>();
            ParticipantsEmails = new List<string>();
        }
        public int SessionId { get; set; }
        public string Title { get; set; }
        public string Abstract { get; set; }
        public string SessionCode { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Room { get; set; }
        public string Category { get; set; }
        public List<string> Participants { get; set; }

        public List<string> ParticipantsEmails { get; set; }

        public string Track { get; set; }

        public string SubmissionId { get; set; }
    }
}
