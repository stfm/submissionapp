﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ConferenceSubmission.Data
{
    public class FormFieldRepository : IFormFieldRepository
    {
        private readonly AppDbContext _appDbContext;

        public FormFieldRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public int CreateFormField(FormField formField)
        {
            _appDbContext.FormFields.Add(formField);

            _appDbContext.SaveChanges();

            return formField.FormFieldId;
        }

        public void AddSubmissionFormField(SubmissionFormField submissionFormField)
		{

			_appDbContext.Add(submissionFormField);

			_appDbContext.SaveChanges();

		}

        public void UpdateSubmissionFormField(int submissionFormFieldId, string value)
        {
            _appDbContext.SubmissionFormFields
                .FirstOrDefault(s => s.SubmissionFormFieldId == submissionFormFieldId)
                .SubmissionFormFieldValue = value;
            _appDbContext.SaveChanges();
        }

		public string GetFormFieldAnswerForSubmission(int formFieldId, int submissionId)
        {


            SubmissionFormField submissionFormField = _appDbContext.SubmissionFormFields.Where(s => s.SubmissionId.Equals(submissionId) && s.FormFieldId.Equals(formFieldId)).FirstOrDefault();

            return submissionFormField.SubmissionFormFieldValue;

        }

        public IEnumerable<FormField> GetFormFields(int submissionCategoryId, string formFieldRole)
        {


            return _appDbContext.FormFields.Where(f => f.FormFieldRole.Equals(formFieldRole) && f.SubmissionCategoryId == submissionCategoryId).Include(s => s.FormFieldProperties).OrderBy( f => f.FormFieldSortOrder);


        }

        public IEnumerable<FormField> GetFormFieldsAnswerRequired(int submissionCategoryId, string formFieldRole)
        {


            IEnumerable<FormField> formFields = GetFormFields(submissionCategoryId, formFieldRole);

            return formFields.Where(f => f.AnswerRequired == true);

        }


        public IEnumerable<SubmissionFormField> GetFormFieldsForSubmission(int submissionId)
        {


            return _appDbContext.SubmissionFormFields.Where(s => s.SubmissionId.Equals(submissionId))
                                .Include(s => s.FormField)
                                .ThenInclude(ff => ff.FormFieldProperties).OrderBy(s => s.FormField.FormFieldSortOrder);
                             


        }

        public int RemoveAllFormFieldsForSubmission(int submissionId)
        {
            var submissionsToRemove = _appDbContext.SubmissionFormFields.Where(s => s.SubmissionId == submissionId);
            _appDbContext.RemoveRange(submissionsToRemove);
            return _appDbContext.SaveChanges();
        }

        public FormField GetFormField(int formFieldId)
        {
            return _appDbContext.FormFields.Where(f => f.FormFieldId == formFieldId).First();

        }


    }
}
