﻿using System;
namespace ConferenceSubmission.Models
{
    public class SubmissionParticipantToParticipantRole
    {
        public SubmissionParticipantToParticipantRole()
        {
        }

        public int SubmissionParticipantToParticipantRoleId { get; set; }

        public int SubmissionParticipantId { get; set; }

        public SubmissionParticipant SubmissionParticipant{get; set;}

        public int ParticipantRoleId { get; set; }

        public ParticipantRole ParticipantRole { get; set; }
    }
}
