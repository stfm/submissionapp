﻿using System;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionAdminSubmissionsByConferenceViewModel
    {
        public List<ConferenceViewModel> ConferenceViewModels { get; set; }

        public SubmissionAdminSubmissionsByConferenceViewModel()
        {

            ConferenceViewModels = new List<ConferenceViewModel>();

        }
    }
}
