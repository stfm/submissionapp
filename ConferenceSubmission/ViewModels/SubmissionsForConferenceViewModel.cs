﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsForConferenceViewModel
    {
        public List<Submission> ConferenceSubmissions        { get; set; }
        public Conference  Conference { get; set; }

    }
}
