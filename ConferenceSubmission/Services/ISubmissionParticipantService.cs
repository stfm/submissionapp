﻿using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ConferenceSubmission.Services
{
	/// <summary>
    /// Defines what services a SubmissionParticipantService
    /// must provide.
    /// </summary>
    public interface ISubmissionParticipantService
    {
        /// <summary>
        /// Deletes the submission participant.
        /// </summary>
        /// <param name="submissionParticipantId">Submission participant identifier.</param>
        void DeleteSubmissionParticipant(int submissionParticipantId);

        /// <summary>
        /// Updates the order of presenters for a submission. This order is captured in the SubmissionParticipant's SortOrder property.
        /// Note: logic was added to account for the unlikely scenario when a user starts to reorder the presenters
        /// and before the reordering is committed to the database, a new presenter is added to the submission.
        /// </summary>
        /// <param name="model"></param>
        void UpdatePresenterOrder(UpdatePresenterOrderViewModel model);

        /// <summary>
        /// Given a list of SubmissionParticipants, this method returns a new list that excludes any participants
        /// whose role is Submitter ONLY.
        /// </summary>
        /// <param name="submissionParticipants"></param>
        /// <returns></returns>
        List<SubmissionParticipant> FilterSubmissionParticipantsExcludeSubmitterOnly(List<SubmissionParticipant> submissionParticipants);

        /// <summary>
        /// Given a SubmissionParticipant, returns -1 if the SubmissionParticipant is a Submitter only, otherwise it returns 0.
        /// </summary>
        /// <param name="submssionParticipant"></param>
        /// <returns>0 or -1</returns>
        int GetInitialSortOrder(SubmissionParticipant submssionParticipant);

        /// <summary>
        /// Given a submission Id, returns a list of SubmissionParticipant objects.
        /// This will include their submission participant role.
        /// </summary>
        /// <param name="submissionId">Unique identifier of submission</param>
        /// <returns>List of SubmissionParticipant objects</returns>
        List<SubmissionParticipant> GetSubmissionParticipants(int submissionId);

        /// <summary>
        /// Get all SubmissionParticipants for the provided
        /// submission ID except those who are submitter only.
        /// </summary>
        /// <returns>Collection of SubmissionParticipants</returns>
        /// <param name="submissionId">Submission identifier.</param>
        IEnumerable<SubmissionParticipant> GetSubmissionParticipantsExcludeSubmitterOnly(int submissionId);

        /// <summary>
        /// Creates a new SubmissionParticipant object
        /// </summary>
        /// <param name="participantId">Unique identifier of the Participant</param>
        /// <param name="participantRoleType">Identifies which role the user is requesting</param>
        /// <param name="submissionId">Unique identifier of the submission</param>
        /// <returns>Database Id of the newly created SubmissionParticipant</returns>
        int AddSubissionParticipant(int participantId, ParticipantRoleType participantRoleType, int submissionId);

        /// <summary>
        /// Checks the database for a SubmissionParticipant record that matches the parameters 
        /// </summary>
        /// <param name="submissionId">Unique identifier of a submission</param>
        /// <param name="participantId">Unique identifier of a participant</param>
        /// <returns>True if a record was found, false otherwise</returns>
        bool CheckIfSubmissionParticipantExists(int submissionId, int participantId);

        /// <summary>
        /// Gets the STFM User ID for the person on the provided submission
        /// who has the provided role. NOTE participant role type must be either
        /// submitter or lead_presenter.
        /// </summary>
        /// <returns>The person's STFM User Id or null if the participant role type
        /// was not found.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="participantRoleType">Participant role type.</param>
        string GetPerson(int submissionId, ParticipantRoleType participantRoleType);

        /// <summary>
        /// Gets the participants on more then two submissions that have the provided 
        /// submissionStatusId and are in the provided conferenceId.
        /// </summary>
        /// <returns>The participants more then two submissions.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        /// <param name="submissionStatusId">Submission status identifier.</param>
        List<int> GetParticipantsMoreThenTwoSubmissions(int conferenceId, int submissionStatusId);

        /// <summary>
        /// Assigns the lead presenter role to the submission participant.
        /// If there is currently a participant assigned the lead presenter role, this participant
        /// will be assigned the presenter role.
        /// </summary>
        /// <param name="submissionId">Unique identifier of submission</param>
        /// <param name="submissionParticipantId">Unique identifier of submission participant</param>
        void AssignLeadPresenterRole(int submissionId, int submissionParticipantId);

        /// <summary>
        /// Removes a role from a submission participant
        /// </summary>
        /// <param name="submissionParticipantToParticipantRoleId">Unique identifier of SubmissionParticipantToParticipantRole</param>
        void RemoveParticipantRole(int submissionParticipantToParticipantRoleId);

        /// <summary>
        /// Adds a SubmissionParticipantToParticipantRole object
        /// </summary>
        /// <param name="submissionParticipantId">Id of a SubmissionParticipant</param>
        /// <param name="participantRoleId">Id of a ParticipantRole</param>
        /// <returns>Database Id of the newly created SubmissionParticipantToParticipantRole</returns>
        int AddSubmissionParticipantToParticipantRole(int submissionParticipantId, int participantRoleId);

        /// <summary>
        /// Creates an instance of the UpdatePresenterOrderViewModel populated with data from the given submission
        /// </summary>
        /// <param name="submissionId">Id of submission</param>
        /// <returns>UpdatePresenterOrderViewModel</returns>
        UpdatePresenterOrderViewModel GetUpdatePresenterOrderViewModel(Submission submission);

        /// <summary>
        /// Find people in SalesForce who match the provided last name.
        /// </summary>
        /// <param name="lastName">last name</param>
        /// <returns>Collection of User objects</returns>
        List<User> FindPeople(string lastName);

        /// <summary>
        /// Gets a list of conference submissions where each submission has at least one submission participant that has been added or removed
        /// as of a specified date.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <param name="asOfDate">Only include participants added or removed after as of this date or later</param>
        /// <returns>List of submissions and the participants that have been added or removed</returns>
        List<SubmissionWithParticipantsAddedOrRemoved> GetSubmissionsWithParticipantsAddedOrRemoved(int conferenceId, DateTime asOfDate);
    }
}
