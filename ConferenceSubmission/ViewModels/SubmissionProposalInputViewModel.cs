﻿using System;
namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// Submission proposal input view model.
    /// </summary>
    public class SubmissionProposalInputViewModel
    {

        public int SubmissionId { get; set; }

        public int SubmissionCategoryId { get; set; }

        public string SubmissionTitle { get; set; }

        public string ConferenceName { get; set; }

        public string SubmissionCategoryName { get; set; }

        public string SubmissionAbstract { get; set; }

    }
}
