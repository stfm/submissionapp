﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.BindingModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using ConferenceSubmission.Mappers;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Globalization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Dropbox.Api;
using Dropbox.Api.Files;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;

namespace ConferenceSubmission.Controllers
{
    /// <summary>
    /// Responds to user requests related to creating or editing a submission.
    /// </summary>
   [Authorize]
    public class SubmissionController : Controller
    {

        private readonly ISubmissionCategoryService _SubmissionCategoryService;

        private readonly ISubmissionService _submissionService;

        private readonly IParticipantService _participantService;

        private readonly IConferenceService _conferenceService;

        private readonly ISubmissionStatusService _submissionStatusService;

        private readonly IFormFieldService _formFieldService;

        private readonly IEncryptionService _encryptionService;

        private readonly IUserMapper _userMapper;

        private readonly ISubmissionParticipantService _submissionParticipantService;

        private IConfiguration _configuration;

        private readonly IDisclosureService _disclosureService;

        private readonly IHostingEnvironment _hostingEnvironment;

        private readonly ISubmissionPrintService _submissionPrintService;

        private readonly IEmailNotificationService _emailNotificationService;

        private readonly ISalesforceAPIService _salesforceAPIService;

        private readonly IVirtualMaterialService _virtualMaterialService;

        private readonly IVirtualConferenenceDataService _virtualConferenenceDataService;

        public SubmissionController(ISubmissionCategoryService submmissionCategoryService,
                                    ISubmissionService submissionService,
                                    IParticipantService participantService,
                                    IConferenceService conferenceService,
                                    ISubmissionStatusService submissionStatusService,
                                    IFormFieldService formFieldService,
                                    IEncryptionService encryptionService,
                                    IUserMapper userMapper,
                                    ISubmissionParticipantService submissionParticipantService,
                                    IConfiguration configuration,
                                    IDisclosureService disclosureService,
                                    IHostingEnvironment hostingEnvironment,
                                    ISubmissionPrintService submissionPrintService,
                                    IEmailNotificationService emailNotificationService,
                                    ISalesforceAPIService salesforceAPIService,
                                    IVirtualMaterialService virtualMaterialService,
                                    IVirtualConferenenceDataService virtualConferenenceDataService
                                   )
        {

            _SubmissionCategoryService = submmissionCategoryService;

            _submissionService = submissionService;

            _participantService = participantService;

            _conferenceService = conferenceService;

            _submissionStatusService = submissionStatusService;

            _formFieldService = formFieldService;

            _encryptionService = encryptionService;

            _userMapper = userMapper;

            _submissionParticipantService = submissionParticipantService;

            _configuration = configuration;

            _disclosureService = disclosureService;

            _hostingEnvironment = hostingEnvironment;

            _submissionPrintService = submissionPrintService;

            _emailNotificationService = emailNotificationService;

            _salesforceAPIService = salesforceAPIService;

            _virtualMaterialService = virtualMaterialService;

            _virtualConferenenceDataService = virtualConferenenceDataService;
        }

        /// <summary>
        /// Responds to user's initial http request to select the submission
        /// category.  User is directed here after selecting the conference
        /// on the submission home page.
        /// </summary>
        /// <returns>The Submission/Index view page.</returns>
        /// <param name="conferenceId">Conference identifier for the conference user is submitting to.</param>
        public IActionResult Index(int conferenceId)
        {

            Conference conference = _conferenceService.GetConference(conferenceId);

            var user = _userMapper.Map(User.Claims);

            bool isAdminUser = _salesforceAPIService.isAdminUser(User.Claims);

            //Business rule do not let normal users submit if CFP deadline has past
            //Check if conference CFP deadline has passed and if user is not an admin then return user to home page

            Log.Information($"User wants to create a new submission proposal for conferenceId {conferenceId} at {DateTime.UtcNow.AddHours(-6)} and CFP Deadline is {conference.ConferenceCFPEndDate}");

            if (conference.ConferenceCFPEndDate.CompareTo(DateTime.UtcNow.AddHours(-6)) < 0 && ! isAdminUser)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }

            SubmissionCategoryBindingModel submissionCategoryBindingModel = new SubmissionCategoryBindingModel
            {
                ConferenceId = conferenceId,

                ConferenceName = conference.ConferenceLongName,

                SubmissionCategoryItems = GetSubmissionCategoryItems(conferenceId),

                SubmissionPaymentRequirementText = conference.SubmissionPaymentRequirementText,

                CategoryDetailUrl = conference.CategoryDetailUrl

            };

            

            /*
             * If the there are submission categories that require payment and the
             * user membership status means this user must pay to submit to those
             * submission categories we need to notify user on the view page
             */
            submissionCategoryBindingModel.DisplaySubmissionRequirementText =
                _submissionService.DisplayPaymentRequirementText(submissionCategoryBindingModel.SubmissionCategoryItems, user.STFMUserId);

            return View(submissionCategoryBindingModel);

        }

        /// <summary>
        /// Responds to user's initial http request to select the submission
        /// category for a CERA submission.  User is directed here after selecting the conference
        /// on the CERA submission home page.
        /// </summary>
        /// <returns>The Submission/StartCera view page.</returns>
        /// <param name="conferenceId">Conference identifier for the conference user is submitting to.</param>
        public IActionResult StartCera(int conferenceId)
        {
            Conference conference = _conferenceService.GetConference(conferenceId);

            SubmissionCategoryBindingModel submissionCategoryBindingModel = new SubmissionCategoryBindingModel
            {
                ConferenceId = conferenceId,

                ConferenceName = conference.ConferenceLongName,

                SubmissionCategoryItems = GetSubmissionCategoryItems(conferenceId),

                SubmissionPaymentRequirementText = conference.SubmissionPaymentRequirementText,

                CategoryDetailUrl = conference.CategoryDetailUrl

            };

            var user = _userMapper.Map(User.Claims);

            /*
             * If the there are submission categories that require payment and the
             * user membership status means this user must pay to submit to those
             * submission categories we need to notify user on the view page
             */
            submissionCategoryBindingModel.DisplaySubmissionRequirementText =
                _submissionService.DisplayPaymentRequirementText(submissionCategoryBindingModel.SubmissionCategoryItems, user.STFMUserId);

            return View(submissionCategoryBindingModel);

        }

        /// <summary>
        /// Responds to user's initial http request to create a new submission after
        /// user has previously selected the conference and then the submission category.
        /// </summary>
        /// <returns>The Submission/StartSubmission view page.</returns>
        /// <param name="conferenceId">Conference identifier for the conference user is submitting to.</param>
        public IActionResult StartSubmission(int conferenceId, int selectedSubmissionCategory)
        {
            Log.Information($"User wants to create a new submission proposal for conferenceId {conferenceId} and categoryId {selectedSubmissionCategory}");

            Conference conference = _conferenceService.GetConference(conferenceId);

            SubmissionCategory submissionCategory = _SubmissionCategoryService.GetSubmissionCategory(selectedSubmissionCategory);

            BaseSubmissionBindingModel baseSubmissionBindingModel = new BaseSubmissionBindingModel
            {
                ConferenceId = conferenceId,

                ConferenceName = conference.ConferenceLongName,

                SelectedSubmissionCategory = selectedSubmissionCategory,

                SelectedSubmissionCategoryName = submissionCategory.SubmissionCategoryName,

                CategoryAbstractInstructions = submissionCategory.CategoryAbstractInstructions,

                CategoryAbstractMaxLength = submissionCategory.CategoryAbstractMaxLength,

                ParticipantRoleDefinitions = _participantService.GetParticipantRoleDefinitions(),

                CategoryInstructions = submissionCategory.CategoryInstructions

            };

            var user = _userMapper.Map(User.Claims);

            return View(baseSubmissionBindingModel);

        }


        [HttpPost]

        public async Task<IActionResult> UploadMaterialOralPresentation(int submissionId, string videoFilename, IFormFile handoutFile)

        {

            var user = _userMapper.Map(User.Claims);

            Log.Information($"User {user.GetFullNamePretty()} wants to provide virtual oral presentation material for submissionID: {submissionId} - " +
                $"video filename {videoFilename} and handout file {(handoutFile != null ? handoutFile.FileName:"None")}");

            Submission submission = _submissionService.GetSubmission(submissionId);

            VirtualConferenceData virtualConferenceData = _virtualConferenenceDataService.GetVirtualConferenceData(submission.Conference.ConferenceId);

            VirtualConferenceDataViewModel virtualConferenceDataViewModel = new VirtualConferenceDataViewModel()
            {
                Submission = submission,
                VirtualConferenceData = virtualConferenceData
            };

            if (string.IsNullOrEmpty(videoFilename) || !videoFilename.Contains(".", StringComparison.InvariantCultureIgnoreCase) )
            {

                ViewData["Error"] = "Video Filename Not Provided or missing extension";

                return View("UploadMaterialOralPresentation", virtualConferenceDataViewModel);

            }

            if (!(handoutFile is null))
            {

                string handoutFileName = handoutFile.FileName;

                string[] handoutPermittedExtensions = { ".pdf" };

                var extension = Path.GetExtension(handoutFile.FileName).ToLowerInvariant();

                //handout file name cannot have any spaces
                if (handoutFileName.Any(Char.IsWhiteSpace))
                {
                    ViewData["Error"] = "Handout file name must not contain any spaces.";
                    return View("UploadMaterialOralPresentation", virtualConferenceDataViewModel);

                }

                if (string.IsNullOrEmpty(extension) || !handoutPermittedExtensions.Contains(extension))
                {

                    ViewData["Error"] = "Only Handout Files of Type .pdf Are Allowed";

                    return View("UploadMaterialOralPresentation", virtualConferenceDataViewModel);
                }

                //Check handout size 10MB is limit
                if (handoutFile.Length / 1000000 > 10)
                {

                    ViewData["Error"] = "Only Handout Files less then 10 MB are allowed.  Your handout file is " + handoutFile.Length / 1000000 + " MB";

                    return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);

                }

            }

            string[] videoPermittedExtensions = { "mov", "mp4", "avi", "wmv" };

            string[] videoFilenameArray = videoFilename.Split('.');

            var ext = videoFilenameArray[1].ToLowerInvariant();

            if (string.IsNullOrEmpty(ext) || !videoPermittedExtensions.Contains(ext) || videoFilenameArray.Length != 2)
            {

                ViewData["Error"] = "The video file name provided is not correct: " + 
                    videoFilename + " - Only video files ending in .mp4 or .mov (Mac) or .avi or .wmv (Windows) Are Allowed. " +
                    "The filename must be " + submissionId + "_video.mp4 (or .mov or .avi or .wmv).";

                return View("UploadMaterialOralPresentation", virtualConferenceDataViewModel);
            }

            if (! videoFilename.Equals(submissionId+"_video." + ext, StringComparison.InvariantCultureIgnoreCase))
            {
                ViewData["Error"] = "The video file name provided is not correct - filename must be " + submissionId + "_video." + ext;

                return View("UploadMaterialOralPresentation", virtualConferenceDataViewModel);

            }

            //Check that video filename can be found in the Dropbox folder

            string folderName = virtualConferenceData.DropboxFolderName;


            if (!videoFileInDropboxAsync(videoFilenameArray[0], folderName).Result)
            {

                ViewData["Error"] = "Video with filename of " + videoFilename + " not found in our Dropbox folder.  Make sure you only uploaded the single video file that is for this presentation.  Do not upload a folder or more then one file.";

                return View("UploadMaterialOralPresentation", virtualConferenceDataViewModel);

            }


            string blobstorageconnection = _configuration["BlobStorage:key"];

            string blobContainer = virtualConferenceData.BlobContainerName;

           VirtualMaterial virtualMaterial = new VirtualMaterial
            {
                MaterialType = "video",
                MaterialValue = videoFilename,
                PersonUploaded = user.STFMUserId,
                SubmissionId = submissionId,
                UploadedDateTime = DateTime.Now
            };

            _virtualMaterialService.AddVirtualMaterial(virtualMaterial);


            if (!(handoutFile is null))
            {

                string handoutFileName = handoutFile.FileName;

                BlobContainerClient blobContainerClient = new BlobContainerClient(blobstorageconnection, blobContainer);

                var blob = blobContainerClient.GetBlobClient(handoutFileName);

                Azure.Response<BlobContentInfo> response = await blob.UploadAsync(handoutFile.OpenReadStream());

                Log.Information($"Uploaded handout file {response.GetRawResponse()} ");

                virtualMaterial = new VirtualMaterial
                {
                    MaterialType = "handout",
                    MaterialValue = handoutFileName,
                    PersonUploaded = user.STFMUserId,
                    SubmissionId = submissionId,
                    UploadedDateTime = DateTime.Now
                };

                _virtualMaterialService.AddVirtualMaterial(virtualMaterial);

            }

            SendUploadedMaterialEmail(submissionId, submission);

            _submissionService.UpdateSubmissionPresentationMethodStatus(submissionId, PresentationMethodType.RECORDED_ONLINE, PresentationStatusType.NO_ACTION );

            return View("UploadMaterialSuccess");

        }

        /// <summary>
        /// Search provided folderName for file name
        /// that contains the provided videoFilename.
        /// </summary>
        /// <param name="videoFilename">video filename provided by user</param>
        /// <param name="folderName">Dropbox folder name where this file should be found</param>
        /// <returns>true if file found</returns>
        private async Task<bool> videoFileInDropboxAsync(string videoFilename, string folderName)
        {

            try
            {

                using (var dbx = new DropboxClient("U5mw0KN7ybAAAAAAAAAF9nwXzKM6roPi8j96aNa7Zs3-VvE-K7HTOi4AcW6irg1s"))
                {

                    var list = await dbx.Files.ListFolderAsync("/File Requests/" + folderName + "/");

                    bool fileFound = false;

                    foreach (var item in list.Entries.Where(i => i.IsFile))
                    {
                        if (item.Name.Contains(videoFilename, StringComparison.InvariantCultureIgnoreCase))
                        {
                            fileFound = true;
                            break;
                        }
                    }

                    foreach (var item in list.Entries.Where(i => i.IsFolder))
                    {
                        var folderFiles = await dbx.Files.ListFolderAsync("/File Requests/" + folderName + "/" + item.Name + "/");

                        foreach (var itemFile in folderFiles.Entries.Where(i => i.IsFile))
                        {
                            if (itemFile.Name.Contains(videoFilename, StringComparison.InvariantCultureIgnoreCase))
                            {
                                fileFound = true;
                                break;
                            }
                        }

                    }

                    return fileFound;
                }

            }
            catch (ApiException<GetMetadataError> ex)
            {
                Log.Information($"Video with filename of  {videoFilename} was NOT found in our Dropbox folder - {ex.Message}");
                return false;
            }
        }

        public async Task<IActionResult> UploadMaterialPosterPresentation(int submissionId, IFormFile posterFile, string youtubeLink, IFormFile handoutFile)

        {
            var user = _userMapper.Map(User.Claims);

            Submission submission = _submissionService.GetSubmission(submissionId);

            VirtualConferenceData virtualConferenceData = _virtualConferenenceDataService.GetVirtualConferenceData(submission.Conference.ConferenceId);

            VirtualConferenceDataViewModel virtualConferenceDataViewModel = new VirtualConferenceDataViewModel()
            {
                Submission = submission,
                VirtualConferenceData = virtualConferenceData
            };

            Log.Information($"Using updated Azure Storage methods - User {user.GetFullNamePretty()} wants to provide virtual poster material for submissionID: {submissionId} - " +
                $"poster filename {posterFile.FileName} and youtube link {(youtubeLink != null ? youtubeLink : "None")} " +
                $"and handout file {(handoutFile != null ? handoutFile.FileName : "None")} " );

            if (posterFile is null)
            {

                ViewData["Error"] = "Poster File Not Provided";

                return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);

            }



            string[] posterPermittedExtensions = { ".jpg", ".png" };

            var ext = Path.GetExtension(posterFile.FileName).ToLowerInvariant();

            if (string.IsNullOrEmpty(ext) || !posterPermittedExtensions.Contains(ext))
            {
               
                ViewData["Error"] = "Only Poster Files of Type .jpg or .png Are Allowed";

                return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);
            }

            if (!posterFile.FileName.Equals(submissionId + "_poster" + ext, StringComparison.InvariantCultureIgnoreCase))
            {
                ViewData["Error"] = "The poster file name provided is not correct - filename must be " + submissionId + "_poster" + ext;

                return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);

            }

            //Check if YouTube link is for embed video

            if (!(youtubeLink is null))
            {
                if (! youtubeLink.StartsWith("https://www.youtube.com/embed", StringComparison.InvariantCultureIgnoreCase))
                {
                    ViewData["Error"] = "YouTube link must be for the embed version of the video - see instructions above.";

                    return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);

                }
            }

            //Check if handoutFile extension is not pdf
            if (!(handoutFile is null))
            {

                string handoutFileName = handoutFile.FileName;

                string[] handoutPermittedExtensions = { ".pdf" };

                var extension = Path.GetExtension(handoutFile.FileName).ToLowerInvariant();

                if (string.IsNullOrEmpty(extension) || !handoutPermittedExtensions.Contains(extension))
                {

                    ViewData["Error"] = "Only Handout Files of Type .pdf Are Allowed";

                    return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);
                }

                if (!handoutFileName.Equals(submissionId + "_handout" + extension, StringComparison.InvariantCultureIgnoreCase))
                {
                    ViewData["Error"] = "The handout file name provided is not correct - filename must be " + submissionId + "_handout" + extension;

                    return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);

                }

                //Check handout size 10MB is limit
                if (handoutFile.Length/1000000 > 10)
                {

                    ViewData["Error"] = "Only Handout Files less then 10 MB are allowed.  Your handout file is " + handoutFile.Length/1000000 + " MB";

                    return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);

                }

            }


            string blobstorageconnection = _configuration["BlobStorage:key"];

            string blobContainer = virtualConferenceData.BlobContainerName;

            string posterFileName = posterFile.FileName;

            BlobContainerClient posterblobContainerClient = new BlobContainerClient(blobstorageconnection, blobContainer);

            var blob = posterblobContainerClient.GetBlobClient(posterFileName);

            Azure.Response<BlobContentInfo> response = await blob.UploadAsync(posterFile.OpenReadStream());

            Log.Information($"Uploaded poster file {response.GetRawResponse()} ");

            VirtualMaterial virtualMaterial = new VirtualMaterial
            {
                MaterialType = "poster",
                MaterialValue = posterFileName,
                PersonUploaded = user.STFMUserId,
                SubmissionId = submissionId,
                UploadedDateTime = DateTime.Now
            };

            _virtualMaterialService.AddVirtualMaterial(virtualMaterial);

            if (!(youtubeLink is null))
            {

                virtualMaterial = new VirtualMaterial
                {
                    MaterialType = "link",
                    MaterialValue = youtubeLink,
                    PersonUploaded = user.STFMUserId,
                    SubmissionId = submissionId,
                    UploadedDateTime = DateTime.Now
                };

                _virtualMaterialService.AddVirtualMaterial(virtualMaterial);

            }

            if (!(handoutFile is null))
            {

                string handoutFileName = handoutFile.FileName;

                BlobContainerClient handoutBlobContainerClient = new BlobContainerClient(blobstorageconnection, blobContainer);

                var handoutBlob = handoutBlobContainerClient.GetBlobClient(handoutFileName);

                Azure.Response<BlobContentInfo> handoutUploadResponse = await handoutBlob.UploadAsync(handoutFile.OpenReadStream());

                Log.Information($"Uploaded handout file {handoutUploadResponse.GetRawResponse()} ");

                virtualMaterial = new VirtualMaterial
                {
                    MaterialType = "handout",
                    MaterialValue = handoutFileName,
                    PersonUploaded = user.STFMUserId,
                    SubmissionId = submissionId,
                    UploadedDateTime = DateTime.Now
                };

                _virtualMaterialService.AddVirtualMaterial(virtualMaterial);

            }

            SendUploadedMaterialEmail(submissionId, submission);

            _submissionService.UpdateSubmissionPresentationMethodStatus(submissionId, PresentationMethodType.RECORDED_ONLINE, PresentationStatusType.NO_ACTION);

            return View("UploadMaterialSuccess");

        }


        private void SendUploadedMaterialEmail(int submissionId, Submission submission)
        {
            string submitterStfmUserId = _submissionParticipantService.GetPerson(submissionId, ParticipantRoleType.SUBMITTER);

            User submitter = _salesforceAPIService.GetUserInfo(submitterStfmUserId).Result;

            string submitterEmail = submitter.Email;

            string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(submissionId, ParticipantRoleType.LEAD_PRESENTER);

            string mainPresenterEmail = "";

            if (mainPresenterStfmUserId != null)
            {

                User mainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

                mainPresenterEmail = mainPresenter.Email;

            }

            _emailNotificationService.SendUploadedMaterialEmail(submission, submitterEmail, mainPresenterEmail);
        }

        /// <summary>
        /// Responds to user's initial http request to create a new CERA submission after
        /// user has previously selected the conference and then the submission category.
        /// </summary>
        /// <returns>Redirects To Action Result to get proposal answers</returns>
        /// <param name="conferenceId">Conference identifier for the conference user is submitting to.</param>
        public RedirectToActionResult StartCeraSubmission(int conferenceId, int selectedSubmissionCategory)
        {
            Log.Information($"User wants to create a new CERA submission for conferenceId {conferenceId} and categoryId {selectedSubmissionCategory}");

            Conference conference = _conferenceService.GetConference(conferenceId);

            SubmissionCategory submissionCategory = _SubmissionCategoryService.GetSubmissionCategory(selectedSubmissionCategory);

            //Follow these steps to create the new CERA Submission

            //Get Participant or create one if no Participant exists with this STFMUserId
            //A Participant is unique and is associated with the STFM User ID
            //A Participant can be associated with multiple submissions

            var user = _userMapper.Map(User.Claims);

            Participant participant = _participantService.GetOrCreateParticipant(user.STFMUserId);

            //Create SubmissionParticipant record
            //A SubmissionParticipant record associates a Participant with a specific submission

            SubmissionParticipant submissionParticipant = new SubmissionParticipant
            {

                Participant = participant

            };


            //Create the participant roles for this submission participant
            //A Submission participant may have multiple roles (e.g. Submitter and Main Presenter) on a submission

            List<ParticipantRole> participantRoles = _participantService.GetParticipantRoles("submitter-only");


            //For each ParticipantRole create a SubmissionParticipantToParticipantRole record

            List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles =
                participantRoles.Select(pr => new SubmissionParticipantToParticipantRole
                {

                    ParticipantRole = pr

                }).ToList();


            //Associate the multiple submission participant roles with the submission participant

            submissionParticipant.SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRoles;

            //Set the initial SortOrder for the participant. It will be 0 (Submitter and Presenter/Main Presenter)
            //or -1 (Submitter Only)
            submissionParticipant.SortOrder = _submissionParticipantService.GetInitialSortOrder(submissionParticipant);

            //A Submission may have multiple participants
            //For the initial creation of a submission only one SubmissionParticipant is in the collection

            List<SubmissionParticipant> submissionParticipants = new List<SubmissionParticipant>
            {
                submissionParticipant
            };

            BaseSubmissionBindingModel baseSubmissionBindingModel = new BaseSubmissionBindingModel
            {
                ConferenceId = conference.ConferenceId,
                SelectedSubmissionCategory = selectedSubmissionCategory,
                SubmissionTitle = "CERA " + submissionCategory.SubmissionCategoryName + " - " + user.FirstName + " " + user.LastName,
                SubmissionAbstract = "None"

            };
         
            Submission submission = _submissionService.CreateSubmission(baseSubmissionBindingModel, submissionParticipants, conference);


            //Save Submission record

            int submissionId = _submissionService.AddSubmission(submission);

            submission.SubmissionId = submissionId;

            submission.SubmissionCategory = _SubmissionCategoryService.GetSubmissionCategory(submission.SubmissionCategoryId);

            Log.Information($"Submission saved to database with id of {submissionId}");


            return RedirectToAction("SubmissionProposalInput", new { submissionId = submission.SubmissionId, categoryId = submission.SubmissionCategoryId });

        }



        /// <summary>
        /// Responds to user's http post request to create basic submission.
        /// </summary>
        /// <returns>Redirects to CreateSubmissionSuccess Action</returns>
        /// <param name="baseSubmissionBindingModel">Base submission binding model - user's
        /// input for the basic submission data.</param>
        public IActionResult CreateSubmission(BaseSubmissionBindingModel baseSubmissionBindingModel)
        {

            if (!ModelState.IsValid)
            {

                baseSubmissionBindingModel.SelectedSubmissionCategoryName = _SubmissionCategoryService.GetSubmissionCategory(baseSubmissionBindingModel.SelectedSubmissionCategory).SubmissionCategoryName;

                return View("Index", baseSubmissionBindingModel);
            }

            var user = _userMapper.Map(User.Claims);

            //Follow these steps to create the new Submission

            //Get Participant or create one if no Participant exists with this STFMUserId
            //A Participant is unique and is associated with the STFM User ID
            //A Participant can be associated with multiple submissions

            Participant participant = _participantService.GetOrCreateParticipant(user.STFMUserId);

            //Create SubmissionParticipant record
            //A SubmissionParticipant record associates a Participant with a specific submission

            SubmissionParticipant submissionParticipant = new SubmissionParticipant
            {

                Participant = participant

            };


            //Create the participant roles for this submission participant
            //A Submission participant may have multiple roles (e.g. Submitter and Main Presenter) on a submission

            List<ParticipantRole> participantRoles = _participantService.GetParticipantRoles(baseSubmissionBindingModel.SelectedUserRole);


            //For each ParticipantRole create a SubmissionParticipantToParticipantRole record

            List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles =
                participantRoles.Select(pr => new SubmissionParticipantToParticipantRole
                {

                    ParticipantRole = pr

                }).ToList();


            //Associate the multiple submission participant roles with the submission participant

            submissionParticipant.SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRoles;

            //Set the initial SortOrder for the participant. It will be 0 (Submitter and Presenter/Main Presenter)
            //or -1 (Submitter Only)
            submissionParticipant.SortOrder = _submissionParticipantService.GetInitialSortOrder(submissionParticipant);

            //A Submission may have multiple participants
            //For the initial creation of a submission only one SubmissionParticipant is in the collection

            List<SubmissionParticipant> submissionParticipants = new List<SubmissionParticipant>
            {
                submissionParticipant
            };


            //Get the conference person is submitting to
            Conference conference = _conferenceService.GetConference(baseSubmissionBindingModel.ConferenceId);

            Submission submission = _submissionService.CreateSubmission(baseSubmissionBindingModel, submissionParticipants, conference);


            //Save Submission record

            int submissionId = _submissionService.AddSubmission(submission);

            submission.SubmissionId = submissionId;

            submission.SubmissionCategory = _SubmissionCategoryService.GetSubmissionCategory(submission.SubmissionCategoryId);

            Log.Information($"Submission saved to database with id of {submissionId}");

            //Send Submission Created Email

            Log.Information("About to call service to send email for submission created.");

            _emailNotificationService.SendSubmissionCreatedEmail(user.Email, submission);


            /*If we're here, the user has successfully created the submission. Redirect the user
             to the Success page to continue to the next step. Redirecting will prevent the form
             from being re-submitted due to browswer refresh.*/

            return RedirectToAction("CreateSubmissionSuccess", new { submissionId = submission.SubmissionId });


        }

        /// <summary>
        /// After a user submits the initial create submission form, if the form 
        /// is successfully created, he/she is directed to this method.
        /// </summary>
        /// <param name="submissionId">Unique identifier of the submission that was just created</param>
        /// <returns></returns>
        public IActionResult CreateSubmissionSuccess(int submissionId)
        {
            var submission = _submissionService.GetSubmission(submissionId);
            var model = new SubmissionProposalInputViewModel
            {
                ConferenceName = submission.Conference.ConferenceLongName,
                SubmissionCategoryId = submission.SubmissionCategoryId,
                SubmissionId = submissionId,
                SubmissionTitle = submission.SubmissionTitle,
                SubmissionAbstract = submission.SubmissionAbstract,
                SubmissionCategoryName = submission.SubmissionCategory.SubmissionCategoryName
            };
            return View("Success", model);
        }

        /// <summary>
        /// Responds to the user's initial request to create submission proposal.
        /// </summary>
        /// <returns>The proposal input.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="categoryId">Category identifier.</param>
        public IActionResult SubmissionProposalInput(string submissionId, int categoryId)
        {

            Log.Information($"User wants to create submission proposal for submissionId {submissionId} and categoryId {categoryId}");
            var submission = _submissionService.GetSubmission(Int32.Parse(submissionId));
            CategoryProposalBindingModel categoryProposalBindingModel = new CategoryProposalBindingModel
            {
                SubmissionId = submissionId,
                ConferenceName = submission.Conference.ConferenceLongName,
                CateogryName = submission.SubmissionCategory.SubmissionCategoryName,
                SubmissionTitle = submission.SubmissionTitle,
                FormFields = _formFieldService.GetFormFields(categoryId, "proposal")
            };

            return View(categoryProposalBindingModel);

        }

        /// <summary>
        /// Responds to user's http post request to create submission proposal.
        /// </summary>
        /// <returns>Redirects to either payment process, disclosure process, or proposal success</returns>
        public RedirectToActionResult CreateSubmissionProposal(string submissionId)
        {

            var user = _userMapper.Map(User.Claims);

            /* Loop over the Form Map which contains the form field names and values
             * We do this since the form fields were dynamically created at runtime
             */
            foreach (var formField in HttpContext.Request.Form)
            {
            

                /* For each formField that has a name which starts with FIELD 
                 * and the value is not null or empty string and the value does not
                 * start with "Select" (that is the first answer in the select options and 
                 * means the user did not select an option then
                 * we want to save
                 * that formField's value and fieldId into the database - this is the user's
                 * answer to a proposal form question
                 */
                if (formField.Key.StartsWith("FIELD", StringComparison.CurrentCulture) &&
                    !String.IsNullOrEmpty(formField.Value) )
                {

                    var fieldId = formField.Key.Split("_")[1];

                    bool allowedAnswer = checkAllowedAnswer(Int32.Parse(fieldId), formField.Value);

                    if (allowedAnswer)
                    {

                        SubmissionFormField submissionFormField = new SubmissionFormField
                        {

                            SubmissionId = Int32.Parse(submissionId),

                            FormFieldId = Int32.Parse(fieldId),

                            SubmissionFormFieldValue = formField.Value


                        };

                        _formFieldService.AddSubmissionFormField(submissionFormField);

                    }

                }

            }

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(Int32.Parse(submissionId), user.STFMUserId);


            /*If the user answered all the submission category specific form questions
             *then update the Submission to be considered complete
             */
            if (submissionWithProposalAnswersViewModel.SubmissionCompleted)
            {

                _submissionService.UpdateSubmissionCompleted(Int32.Parse(submissionId), true);

            }

            submissionWithProposalAnswersViewModel.AddParticipantUrl = _participantService.GetAddParticipantUrl(submissionId, Request.Host.Value);


            //Send Submission Proposal Completed Email

            Submission submission = _submissionService.GetSubmission(submissionWithProposalAnswersViewModel.SubmissionId);

            submissionWithProposalAnswersViewModel.ConferenceName = submission.Conference.ConferenceLongName;

            submissionWithProposalAnswersViewModel.ConferenceCfpEndDate = submission.Conference.ConferenceCFPEndDate;

            if (submissionWithProposalAnswersViewModel.ConferenceName.Contains("CERA", StringComparison.InvariantCultureIgnoreCase))
            {

                _emailNotificationService.SendCeraSubmissionProposalCompletedEmail(user.Email, submissionWithProposalAnswersViewModel);

            } else
            {
                _emailNotificationService.SendSubmissionProposalCompletedEmail(user.Email, submissionWithProposalAnswersViewModel);

            }

            Log.Information($"Submission proposal completed email sent to {user.Email}");


            Log.Information("About to check if submission payment is required.");

            if (_submissionService.IsSubmissionPaymentRequired(submissionWithProposalAnswersViewModel.SubmissionPaymentRequirement, Int32.Parse(submissionId)))
            {

                Log.Information("Submission payment is required.");

                submissionWithProposalAnswersViewModel.SubmissionFee = Int32.Parse(_configuration["SubmissionPayment:SubmissionFee"]);

                return RedirectToAction(actionName: "Index", controllerName: "Payment", routeValues: new { submissionId = Int32.Parse(submissionId) });


            }

            if (! submissionWithProposalAnswersViewModel.ConferenceName.Contains("CERA", StringComparison.InvariantCultureIgnoreCase) )
            {

                Log.Information("About to check if disclosure is required - conference start date is " + submissionWithProposalAnswersViewModel.ConferenceStartDate + " STFM User id is " + user.STFMUserId);

                if (_disclosureService.GetDisclosure(user.STFMUserId) == null)
                {

                    Log.Information("STFM User id " + user.STFMUserId + " does not have a disclosure.  So must complete a new disclosure.");

                    return RedirectToAction("Index", "Disclosure", routeValues: new { submissionId = Int32.Parse(submissionId) });


                }
                else if (!_disclosureService.isDisclosureCurrent(submissionWithProposalAnswersViewModel.ConferenceStartDate, user.STFMUserId))
                {

                    Log.Information("STFM User Id " + user.STFMUserId + " needs to update the disclosure.");

                    return RedirectToAction("EditDisclosure", "Disclosure", routeValues: new { submissionId = Int32.Parse(submissionId) });

                } 
            }


            return RedirectToAction(actionName: "ProposalSuccess", controllerName: "Submission", routeValues: new { submissionId = Int32.Parse(submissionId) });


        }


        private bool checkAllowedAnswer(int formFieldId, string answer) {

            FormField formField = _formFieldService.GetFormField(formFieldId);

            //select form fields cannot have an answer of "select"
            if (formField.FormFieldType == "select" && answer.ToLower(CultureInfo.InvariantCulture).StartsWith("select", StringComparison.InvariantCulture))
            {
                return false;
            }

            return true;


        }


        /// <summary>
        /// Sets up to show the proposal success view - shown after user
        /// successfully creates or updates the complete submission.
        /// </summary>
        /// <returns>The success.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public IActionResult ProposalSuccess(int submissionId)
        {

            var user = _userMapper.Map(User.Claims);

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId, user.STFMUserId);

            if (_submissionService.IsSubmissionPaymentRequired(submissionWithProposalAnswersViewModel.SubmissionPaymentRequirement, submissionId))
            {

                Log.Information("Submission payment was required.");

                submissionWithProposalAnswersViewModel.SubmissionFee = Int32.Parse(_configuration["SubmissionPayment:SubmissionFee"]);

            } else
            {

                Log.Information("Submission payment was not required.");

                submissionWithProposalAnswersViewModel.SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE;


            }

            submissionWithProposalAnswersViewModel.AddParticipantUrl = _participantService.GetAddParticipantUrl(submissionId.ToString(), Request.Host.Value);

            if (submissionWithProposalAnswersViewModel.ConferenceName.Contains("CERA", StringComparison.InvariantCultureIgnoreCase))
            {

                return View("CeraProposalSuccess", submissionWithProposalAnswersViewModel);
            }
            else
            {

                return View(submissionWithProposalAnswersViewModel);
            }

        }

        /// <summary>
        /// Responds to user's request to view a specific submission
        /// </summary>
        /// <returns>View for a specific submision</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public IActionResult ViewSubmission(int submissionId)
        {
            var user = _userMapper.Map(User.Claims);

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId, user.STFMUserId);


            if (_submissionService.IsSubmissionPaymentRequired(submissionWithProposalAnswersViewModel.SubmissionPaymentRequirement, submissionId))
            {

                Log.Information("Submission payment was required.");

                submissionWithProposalAnswersViewModel.SubmissionFee = Int32.Parse(_configuration["SubmissionPayment:SubmissionFee"]);

            }
            else
            {

                Log.Information("Submission payment was not required.");

                submissionWithProposalAnswersViewModel.SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE;


            }

            return View(submissionWithProposalAnswersViewModel);

        }


        /// <summary>
        /// Responds to user's request to view a specific CERA submission
        /// </summary>
        /// <returns>View for a specific CERA submision</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public IActionResult ViewCeraSubmission(int submissionId)
        {
            var user = _userMapper.Map(User.Claims);

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId, user.STFMUserId);

            return View(submissionWithProposalAnswersViewModel);

        }



        /// <summary>
        /// A form where the user can edit the sumission's title and abstract
        /// </summary>
        /// <param name="submissionId">Submission identifier</param>
        /// <returns></returns>
        public IActionResult EditSubmissionBaseDetails(int submissionId)
        {

            var submission = _submissionService.GetSubmission(submissionId);

            var user = _userMapper.Map(User.Claims);

            if (CheckEditAllowed(submission, user.STFMUserId) == false)
            {
                return View("EditNotAllowed");
            }
           
            return View(MapToEditBaseSubmissionDetailsBindingModel(user, submission));
        }

        /// <summary>
        /// Checks STFM business rules to determine if logged in
        /// user may edit a submission.
        /// </summary>
        /// <param name="submission">Submission being edited</param>
        /// <param name="stfmUserId">Logged in user's STFM User Id</param>
        /// <returns>true if edit is allowed otherwise false</returns>
        private bool CheckEditAllowed(Submission submission, String stfmUserId)
        {


            string accessPermissions = User.Claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions") == null ?
                           "" : User.Claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions").Value;
            
            if (accessPermissions.Contains("Admin") == true)
            {
                //Admin users may always edit a submission
                return true;
            }

            //Log.Information("DateTime.Now is " + DateTime.Now + " CFP End Date + 1 day is " + submission.Conference.ConferenceCFPEndDate.AddDays(1));
            //Log.Information("Comparison between DateTime.Now and CFP End Date + 1 Day is " + DateTime.Now.CompareTo(submission.Conference.ConferenceCFPEndDate.AddDays(1)));

            //Allow user 1 day after the CFP deadline to still edit submission

            if (DateTime.Now.CompareTo(submission.Conference.ConferenceCFPEndDate.AddDays(1)) > 0)
            {
                Log.Information("User not allowed to edit submission - CFP End Date + 1 day is " + submission.Conference.ConferenceCFPEndDate.AddDays(1) + "  DateTime.Now is " + DateTime.Now);
                Log.Information("Comparison between CFP end date and DateTime.Now.AddDays(1) is " + submission.Conference.ConferenceCFPEndDate.CompareTo(DateTime.Now.AddDays(1)));
                    
                    return false;

            }
           
            string submitterStfmUserId = _submissionParticipantService.GetPerson(submission.SubmissionId, ParticipantRoleType.SUBMITTER);

            string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(submission.SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

            if (stfmUserId.Equals(submitterStfmUserId) || stfmUserId.Equals(mainPresenterStfmUserId))
            {
                Log.Information("User is allowed to edit submission - User is either submitter or lead presenter");
                //Logged in user who is a submitter or main presenter on this submission may edit
                return true;

            }

            Log.Information("User not allowed to edit submission - Unknown reason");

            return false;
        }

        /// <summary>
        /// Takes the edits to the submission's title and abstract and saves them to the database
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns one of two views: 1) If the form is valid the user is redirected to the edit category specific page
        /// 2) If the form data is invalid, the user is redirected back to the form to fix the data</returns>
        [HttpPost]
        public IActionResult EditSubmissionBaseDetails(EditBaseSubmissionDetailsBindingModel model)
        {
            var user = _userMapper.Map(User.Claims);

            var submission = _submissionService.GetSubmission(model.SubmissionId);

            if (ModelState.IsValid)
            {
                _submissionService.UpdateSubmissionBaseDetails(model.SubmissionId, model.SubmissionTitle, model.SubmissionAbstract, false);

                Log.Information($"Submission base details updated and saved to the database. SubmissionId: {model.SubmissionId}. Updated by STFM user with ID: {user.STFMUserId}");

                SubmissionBaseEdited submissionBaseEdited = new SubmissionBaseEdited
                {

                    Conference = submission.Conference,
                    SubmissionId = submission.SubmissionId,
                    SubmissionTitle = submission.SubmissionTitle,
                    SubmissionAbstract = submission.SubmissionAbstract,
                    SubmissionCategory = submission.SubmissionCategory

                };

                string submitterStfmUserId = _submissionParticipantService.GetPerson(model.SubmissionId, ParticipantRoleType.SUBMITTER);

                User submitter = _salesforceAPIService.GetUserInfo(submitterStfmUserId).Result;

                submissionBaseEdited.SubmitterEmailAddress = submitter.Email;

                string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(model.SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

                if (mainPresenterStfmUserId != null)
                {

                    User mainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

                    submissionBaseEdited.MainPresenterEmailAddress = mainPresenter.Email;

                }

                string accessPermissions = User.Claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions") == null ?
                           "" : User.Claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions").Value;

                if (accessPermissions.Contains("Admin") == false)
                {
                    //Only send email if non-admin user is editing the submission
                    _emailNotificationService.SendSubmissionBaseEditedEmail(submissionBaseEdited);

                } else
                {
                    Log.Information("No email sent as admin user " + user.LastName + " edited submission base details");
                }

                return RedirectToAction("EditProposalAnswers", new { submissionId = model.SubmissionId });
            }

            return View(MapToEditBaseSubmissionDetailsBindingModel(user, submission, model.SubmissionTitle, model.SubmissionAbstract));
        }

        [HttpGet]
        [Route("Submission/EditProposalAnswers/{submissionId}")]
        public IActionResult EditProposalAnswers(int submissionId)
        {
            var submission = _submissionService.GetSubmission(submissionId);
            return View(new EditSubmissionWithProposalAnswersBindingModel
            {
                SubmissionId = submissionId,
                ConferenceName = submission.Conference.ConferenceLongName,
                SubmissionCategoryName = submission.SubmissionCategory.SubmissionCategoryName,
                SubmissionTitle = submission.SubmissionTitle,
                SubmissionCompleted = submission.SubmissionCompleted,
                SubmissionFormFields = _formFieldService.GetFormFieldsForSubmission(submissionId).ToList(),
                FormFields = _formFieldService.GetFormFields(submission.SubmissionCategoryId, "proposal").ToList()
            });
        }

        /// <summary>
        /// Responds to user's http post request to edit submission proposal.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult EditProposalAnswers(EditSubmissionWithProposalAnswersBindingModel model)
        {
            var user = _userMapper.Map(User.Claims);

            if (ModelState.IsValid)
            {
                //----------------------------------------------------------------------------------------------------------------
                //1. Find the Submission and delete all existing SubmissionFormFields for that Submissiom
                //2. Iterate of the Request's Form's KeyValuePairs. If the value is NOT null or empty and the value is not "Select",
                //   create a new SubmissionFormField for the Submission.
                //3. After all SubmissionFormFields are created, create a SubmissionWithProposalAnswersViewModel for the Submission.  
                //   In doing so, we also calculate the Submission's SubmissionComplete status.
                //4. Update the database with the Submission's newly calculated SubmissionComplete status.
                //-----------------------------------------------------------------------------------------------------------------

                //1.
                var submission = _submissionService.GetSubmission(model.SubmissionId);
                _formFieldService.RemoveAllFormFieldsForSubmission(model.SubmissionId);
                var formFields = _formFieldService.GetFormFields(submission.SubmissionCategoryId, "proposal")
                                                            .Where(f => f.GetPropertyValue("label") != "screentext")
                                                            .ToList();
                //2.
                foreach (var formField in Request.Form)
                {
                    if (!string.IsNullOrWhiteSpace(formField.Value.ToString()) )
                    {
                        if (formField.Key.StartsWith("FIELD_", StringComparison.InvariantCulture))
                        {
                            var formFieldId = Int32.Parse(formField.Key.Substring(6));

                            bool allowedAnswer = checkAllowedAnswer(formFieldId, formField.Value);

                            if (allowedAnswer)
                            {

                                _formFieldService.AddSubmissionFormField(new SubmissionFormField
                                {
                                    FormFieldId = formFieldId,
                                    SubmissionFormFieldValue = formField.Value,
                                    SubmissionId = model.SubmissionId
                                });
                            }
                        }
                    }
                }

                //3.
                SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(model.SubmissionId, user.STFMUserId);

                Log.Information("Submission details: " + submissionWithProposalAnswersViewModel);

                //4. = 
                _submissionService.UpdateSubmissionCompleted(model.SubmissionId, submissionWithProposalAnswersViewModel.SubmissionCompleted);

                string submitterStfmUserId = _submissionParticipantService.GetPerson(model.SubmissionId, ParticipantRoleType.SUBMITTER);

                User submitter = _salesforceAPIService.GetUserInfo(submitterStfmUserId).Result;

                string submitterEmail = submitter.Email;

                string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(model.SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

                string mainPresenterEmail = "";

                if (mainPresenterStfmUserId != null)
                {

                    User mainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

                    mainPresenterEmail = mainPresenter.Email;

                }

                string accessPermissions = User.Claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions") == null ?
                           "" : User.Claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions").Value;

                if (accessPermissions.Contains("Admin") == false)
                {
                    if ( submissionWithProposalAnswersViewModel.ConferenceName.Contains("CERA", StringComparison.InvariantCultureIgnoreCase))
                    {
                        _emailNotificationService.SendCeraSubmissionProposalEditedEmail(submissionWithProposalAnswersViewModel, submitterEmail, mainPresenterEmail);
                    } else
                    {
                        _emailNotificationService.SendSubmissionProposalEditedEmail(submissionWithProposalAnswersViewModel, submitterEmail, mainPresenterEmail);
                    }

                } else
                {
                    Log.Information("No email sent as admin user " + user.LastName + " edited submission proposal details");
                }

                return RedirectToAction(actionName: "ProposalSuccess", controllerName: "Submission", routeValues: new { submissionId = model.SubmissionId });
            }
            return View();
        }

        /// <summary>
        /// Withdraws the submission.
        /// </summary>
        /// <returns>Home View or ViewSubmissionsNotCanceledOrWithdrawn View</returns>
        /// <param name="SubmissionId">Submission identifier.</param>
        /// <param name="destination">Which page to redirect the user to.</param>
		public RedirectToActionResult WithdrawSubmission(int SubmissionId, string destination)
        {

            _submissionService.UpdateSubmissionStatus(SubmissionId, _submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.WITHDRAWN).SubmissionStatusId);

            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} has withdrawn submission id of  {SubmissionId}");

            Submission submission = _submissionService.GetSubmission(SubmissionId);

            string submitterStfmUserId = _submissionParticipantService.GetPerson(SubmissionId, ParticipantRoleType.SUBMITTER);

            User submitter = _salesforceAPIService.GetUserInfo(submitterStfmUserId).Result;

            string submitterEmail = submitter.Email;

            string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

            string mainPresenterEmail = "";

            if (mainPresenterStfmUserId != null)
            {

                User mainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

                mainPresenterEmail = mainPresenter.Email;

            }

            _emailNotificationService.SendSubmissionWithdrawnEmail(submission, submitterEmail, mainPresenterEmail);

            if (destination.ToLower() == "viewsubmissions")
            {
                return RedirectToAction(controllerName: "Home", actionName: "ViewSubmissionsNotCanceledOrWithdrawn");
            } else if (destination.ToLower() == "cerahome")
            {
                return RedirectToAction(controllerName: "Cera", actionName: "Index");
            }

            return RedirectToAction(controllerName: "Home", actionName: "Index");
        }

        /// <summary>
        /// Process user request to withdraw submission from presentation at the virtual
        /// conference supplement.
        /// </summary>
        /// <param name="SubmissionId"></param>
        /// <returns>Redirects back to home page</returns>
        public RedirectToActionResult WithdrawVirtual(int SubmissionId)
        {
            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} has withdrawn submission id of  {SubmissionId} from presentaton at the virtual conference supplement.");

            _submissionService.UpdateSubmissionPresentationMethodStatus(SubmissionId, PresentationMethodType.RECORDED_ONLINE, PresentationStatusType.DECLINED);

            return RedirectToAction(controllerName: "Home", actionName: "Index");
        }

        /// <summary>
        /// Changes submission status to Will Present At Virtual Conference. This is needed
        /// for accepted submissions of virtual conferences so submitter/main presenter can 
        /// let us know if he/she will present the submission in the rescheduled
        /// Virtual conference.
        /// </summary>
        /// <returns>Home View or ViewSubmissionsNotCanceledOrWithdrawn View</returns>
        /// <param name="SubmissionId">Submission identifier.</param>
        public RedirectToActionResult PresentVirtual(int SubmissionId)
        {

            _submissionService.UpdateSubmissionStatus(SubmissionId, _submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.WILL_PRESENT_AT_VIRTUAL_CONFERENCE).SubmissionStatusId);

            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} has stated they will present at virtual conference submission id of  {SubmissionId}");

            Submission submission = _submissionService.GetSubmission(SubmissionId);

            string submitterStfmUserId = _submissionParticipantService.GetPerson(SubmissionId, ParticipantRoleType.SUBMITTER);

            User submitter = _salesforceAPIService.GetUserInfo(submitterStfmUserId).Result;

            string submitterEmail = submitter.Email;

            string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

            string mainPresenterEmail = "";

            if (mainPresenterStfmUserId != null)
            {

                User mainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

                mainPresenterEmail = mainPresenter.Email;

            }

            _emailNotificationService.SendSubmissionWillPresentVirtualEmail(submission, submitterEmail, mainPresenterEmail);

            return RedirectToAction(controllerName: "Home", actionName: "Index");

        }

        /// <summary>
        /// Downloads a pdf version of the submission to the user's browser. Does not redirect the user.
        /// </summary>
        /// <param name="submissionId">Unique identifier of a submission</param>
        /// <returns>Pdf File</returns>
        public IActionResult PrintPdf(int submissionId)
        {
            try
            {
                var user = _userMapper.Map(User.Claims);

                var submissionPrintViewModel = _submissionService.GetSubmissionPrintViewModel(submissionId);

                submissionPrintViewModel.ImagePath = Path.Combine(_hostingEnvironment.WebRootPath, "images/STFM_logo.png");

                return File(
                    _submissionPrintService.GetPdfAsByteArray(submissionPrintViewModel),
                    "application/pdf",
                    submissionPrintViewModel.GetFileName());
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Server errored while attempting to print submission with Id of {submissionId}.");
            }
            return RedirectToAction("Error", "Home");
        }



        /// <summary>
        /// Gets the submission category items for the provided conference id.
        /// </summary>
        /// <returns>The submission category items.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        private List<SelectListItem> GetSubmissionCategoryItems(int conferenceId)
        {

            List<SubmissionCategoryViewModel> submissionCategoryViewModelList = _SubmissionCategoryService.GetActiveSubmissionCategories(conferenceId);

            List<SelectListItem> submissionCategoryItems = submissionCategoryViewModelList.Select(sc => new SelectListItem
            {
                Value = sc.SubmissionCategoryId.ToString(),
                Text = sc.SubmissionCategoryName

            }).ToList();

            return submissionCategoryItems;

        }

        private EditBaseSubmissionDetailsBindingModel MapToEditBaseSubmissionDetailsBindingModel(User user, Submission submission)
        {

            SubmissionCategory submissionCategory = _SubmissionCategoryService.GetSubmissionCategory(submission.SubmissionCategoryId);

            return new EditBaseSubmissionDetailsBindingModel
            {
                SubmissionId = submission.SubmissionId,
                ConferenceLongName = submission.Conference.ConferenceLongName,
                SubmissionTitle = submission.SubmissionTitle,
                SubmissionAbstract = submission.SubmissionAbstract,
                Category = submission.SubmissionCategory.SubmissionCategoryName,
                User = user,
                CategoryAbstractMaxLength = submissionCategory.CategoryAbstractMaxLength,
                CategoryAbstractInstructions = submissionCategory.CategoryAbstractInstructions
                
            };

        }

        private EditBaseSubmissionDetailsBindingModel MapToEditBaseSubmissionDetailsBindingModel(User user, Submission submission, string title, string _abstract)
        {
            var editBaseSubmissionDetailsBindingModel = MapToEditBaseSubmissionDetailsBindingModel(user, submission);

            editBaseSubmissionDetailsBindingModel.SubmissionTitle = title;
            editBaseSubmissionDetailsBindingModel.SubmissionAbstract = _abstract;

            return editBaseSubmissionDetailsBindingModel;
        }


        /// <summary>
        /// Responds to User's initial request to upload material for virtual
        /// presentation.  
        /// </summary>
        /// <returns>Returns user to create upload material view page based
        /// on submission accepted category</returns>
        /// <param name="SubmissionId">Submission identifier.</param>
        public IActionResult UploadMaterial(int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);

            //Use below to sent user to information page when upload process is not ready
            //if (submission.Conference.ConferenceId == 50 && submissionId != 9183 && submissionId != 9191)
            //{
            //    return View("UploadMaterial");
            //}

            //Check if user has already provided virtual material for this submission

            if (checkIfVirtualMaterialExists(submissionId) &&
                  submission.AcceptedCategory.SubmissionCategoryName.Contains("Poster", StringComparison.InvariantCultureIgnoreCase))
            {

                SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId);

                return View("AlreadyUploaded", submissionWithProposalAnswersViewModel);
            }


            VirtualConferenceData virtualConferenceData = _virtualConferenenceDataService.GetVirtualConferenceData(submission.Conference.ConferenceId);

            VirtualConferenceDataViewModel virtualConferenceDataViewModel = new VirtualConferenceDataViewModel()
            {
                Submission = submission,
                VirtualConferenceData = virtualConferenceData
            };

            if (submission.AcceptedCategory.SubmissionCategoryName.Contains("Poster", StringComparison.InvariantCultureIgnoreCase))
            {
                return View("UploadMaterialPosterPresentation", virtualConferenceDataViewModel);

            }
            else
            {
                return View("UploadMaterialOralPresentation", virtualConferenceDataViewModel);

            }



        }


        /// <summary>
        /// For the provided submission ID find records of virtual
        /// material uploaded and remove those records
        /// Remove any uploaded files from Azure blob
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns></returns>
        public IActionResult RemoveUploadedMaterial(int submissionId)
        {
            var user = _userMapper.Map(User.Claims);

            Log.Information(user.GetFullNamePretty() + " is removing virtual material previously uploaded for " + submissionId );
            
            //Get submission

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId);

            Submission submission = _submissionService.GetSubmission(submissionId);

            //Get virtual conference data

            VirtualConferenceData virtualConferenceData = _virtualConferenenceDataService.GetVirtualConferenceData(submission.Conference.ConferenceId);

            //Get collection of virtual material records for the submission

            List<VirtualMaterial> virtualMaterials = submissionWithProposalAnswersViewModel.VirtualMaterials;

            string blobContainer = virtualConferenceData.BlobContainerName;

            string blobstorageconnection = _configuration["BlobStorage:key"];

            BlobContainerClient blobContainerClient = new BlobContainerClient(blobstorageconnection, blobContainer);


            foreach (var virtualMaterial in virtualMaterials.ToList())
            {
                if (virtualMaterial.MaterialType.Equals("poster", StringComparison.InvariantCultureIgnoreCase) ||
                    virtualMaterial.MaterialType.Equals("handout", StringComparison.InvariantCultureIgnoreCase) ||
                    virtualMaterial.MaterialType.Equals("link", StringComparison.InvariantCultureIgnoreCase)
                    )
                {
                    //delete blob entry
                    //var blob = cloudBlobContainer.GetBlockBlobReference(virtualMaterial.MaterialValue);
                    //blob.DeleteIfExists();

                    var blob = blobContainerClient.GetBlobClient(virtualMaterial.MaterialValue);
                    blob.DeleteIfExists();

                    //delete virtual material record

                    VirtualMaterial virtualMaterialToRemove = _virtualMaterialService.GetVirtualMaterial(submissionId);

                    _virtualMaterialService.DeleteVirtualMaterial(virtualMaterialToRemove);

                }


            }

            //Change submission status to Will Present 

            _submissionService.UpdateSubmissionStatus(submissionId, 
                _submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.WILL_PRESENT_AT_VIRTUAL_CONFERENCE).SubmissionStatusId);

            submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId);

            return View(submissionWithProposalAnswersViewModel);

        }

        [HttpGet]
        public IActionResult ProvideRecordedOnDemandPresentation(int submissionId)
        {
            Submission submission = _submissionService.GetSubmission(submissionId);
            var model = new ProvideRecordedOnDemandPresentationBindingModel() { SubmissionId = submissionId,
            ConferenceName = submission.Conference.ConferenceLongName
            };

            if (submission.Conference.ConferenceShortName.StartsWith("MSE"))
            {
                model.Deadline = "December 16, 2022";
                model.VirtualConferenceFAQ = "https://www.stfm.org/conferences/mse/virtualsupplementfaqs/";
            } else
            {
                model.Deadline = "March 27, 2023";
                model.VirtualConferenceFAQ = " https://www.stfm.org/conferences/annual/virtualsupplementfaqs/";
            }

            return View("ProvideRecordedOnDemandPresentation", model);
        }

        /// <summary>
        /// Receives the Presenter's choice to provide/not provide a recorded version of their
        /// presentation for the On-Demand Conference. Records their choice in the database
        /// and sends an email to the Submitter and Lead Presenter thanking them for notifying STFM of their choice.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ProvideRecordedOnDemandPresentation(ProvideRecordedOnDemandPresentationBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userMapper.Map(User.Claims);

                Log.Information($"{user.GetFullNamePretty()} is notifying STFM that they will {(model.WillProvideRecordedOnDemandPresentation ? "" : "not")} provide a recorded on-demand presentation for submission {model.SubmissionId}.");

                _submissionService.UpdateSubmissionPresentationMethodStatus(model.SubmissionId, PresentationMethodType.RECORDED_ONLINE, model.WillProvideRecordedOnDemandPresentation ? PresentationStatusType.NEED_PRESENTATION_MATERIAL : PresentationStatusType.DECLINED);

                string submitterStfmUserId = _submissionParticipantService.GetPerson(model.SubmissionId, ParticipantRoleType.SUBMITTER);

                User submitter = _salesforceAPIService.GetUserInfo(submitterStfmUserId).Result;

                string submitterEmail = submitter.Email;

                string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(model.SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

                string mainPresenterEmail = "";

                if (mainPresenterStfmUserId != null)
                {

                    User mainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

                    mainPresenterEmail = mainPresenter.Email;

                }

                var submission = _submissionService.GetSubmission(model.SubmissionId);

                var emailModel = new ProvideRecordedOnDemandPresentationNotificationReceivedViewModel()
                {
                    SubmissionId = model.SubmissionId,
                    SubmissionTitle = submission.SubmissionTitle,
                    ConferenceName = submission.Conference.ConferenceLongName,
                    SubmissionCategory = submission.AcceptedCategory.SubmissionCategoryName,
                    WillProvideRecordedOnDemandPresentation = model.WillProvideRecordedOnDemandPresentation
                };

                _emailNotificationService.SendProvideRecordedOnDemandPresentationNotificationReceivedEmail(submitterEmail, mainPresenterEmail, emailModel);

                return RedirectToAction("Index", "Home");
            }
            
            return View(model);
        }

        /// <summary>
        /// Check if VirtualMaterial already exists for the provided submissionId
        /// </summary>
        /// <param name="submissionId">submissionId to check</param>
        /// <returns>true if VirtualMaterial already exists otherwise false</returns>
        private bool checkIfVirtualMaterialExists(int submissionId)
        {
            VirtualMaterial virtualMaterial = _virtualMaterialService.GetVirtualMaterial(submissionId);

            if (virtualMaterial != null)
            {
                var user = _userMapper.Map(User.Claims);

                Log.Information(user.GetFullNamePretty() + " has already provided virtual material for submission " + submissionId +
                    " and is providing new virtual material for the submission.");

                return true;

            }

            return false;
        }

    }
}
