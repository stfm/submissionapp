﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public class FAQRepository : IFAQRepository
    {
        private readonly AppDbContext _appDbContext;

        public FAQRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;
        }

        public List<FAQ> GetFAQs()
        {
            return _appDbContext.FAQs.ToList();
        }
    }
}
