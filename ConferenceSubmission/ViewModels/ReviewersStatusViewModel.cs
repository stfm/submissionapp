﻿using System;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.ViewModels
{
    public class ReviewersStatusViewModel
    {
        public ReviewersStatusViewModel()
        {
        }

        public string ConferenceLongName { get; set; }

        public int ConferenceId { get; set; }

        public List<ReviewerStatus> reviewerStatuses { get; set; }


    }
}
