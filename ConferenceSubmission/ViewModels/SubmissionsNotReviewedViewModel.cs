﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsNotReviewedViewModel
    {
        public Conference Conference { get; set; }

        public List<Submission> SubmissionsNotAssignedToReviewer { get; set; }

        public List<Submission> SubmissionsNoReviewedCompleted { get; set; }

        public SubmissionsNotReviewedViewModel()
        {

            SubmissionsNotAssignedToReviewer = new List<Submission>();

            SubmissionsNoReviewedCompleted = new List<Submission>();

        }


    
    }
}
