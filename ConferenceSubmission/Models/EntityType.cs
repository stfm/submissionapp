﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public enum EntityType
    {
        //Each enum value is the name of one of our model classes.
        //It must be spelled exactly like the model class.
        //As we want to track more entity types, we can add more enum values,
        //but [IMPORTANT] we cannot remove enum values or change the order of these values.

        Submission,                 // 0 in the database
        Conference,                 // 1 in the database
        ConferenceSession,          // 2 in the database
        Review,                     // 3 in the database
        SubmissionParticipant,      // 4 in the database
        Participant                 // 5 in the database
    }
}
