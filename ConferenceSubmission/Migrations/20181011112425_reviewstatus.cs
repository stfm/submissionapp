﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class reviewstatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReviewStatusId",
                table: "Reviews",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ReviewStatuses",
                columns: table => new
                {
                    ReviewStatusId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ReviewStatusType = table.Column<int>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewStatuses", x => x.ReviewStatusId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_ReviewStatusId",
                table: "Reviews",
                column: "ReviewStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reviews_ReviewStatuses_ReviewStatusId",
                table: "Reviews",
                column: "ReviewStatusId",
                principalTable: "ReviewStatuses",
                principalColumn: "ReviewStatusId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reviews_ReviewStatuses_ReviewStatusId",
                table: "Reviews");

            migrationBuilder.DropTable(
                name: "ReviewStatuses");

            migrationBuilder.DropIndex(
                name: "IX_Reviews_ReviewStatusId",
                table: "Reviews");

            migrationBuilder.DropColumn(
                name: "ReviewStatusId",
                table: "Reviews");
        }
    }
}
