﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class disclosure_complete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "Disclosures",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Part1Answer",
                table: "Disclosures",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Part2Answer",
                table: "Disclosures",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Part3Answer",
                table: "Disclosures",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Part4Answer",
                table: "Disclosures",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Part5Answer",
                table: "Disclosures",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Part6Answer",
                table: "Disclosures",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "Disclosures");

            migrationBuilder.DropColumn(
                name: "Part1Answer",
                table: "Disclosures");

            migrationBuilder.DropColumn(
                name: "Part2Answer",
                table: "Disclosures");

            migrationBuilder.DropColumn(
                name: "Part3Answer",
                table: "Disclosures");

            migrationBuilder.DropColumn(
                name: "Part4Answer",
                table: "Disclosures");

            migrationBuilder.DropColumn(
                name: "Part5Answer",
                table: "Disclosures");

            migrationBuilder.DropColumn(
                name: "Part6Answer",
                table: "Disclosures");
        }
    }
}
