﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.MSSqlServer;

namespace ConferenceSubmission
{
    public class Program
    {

        public static void Main(string[] args)
        {
            //BuildWebHost(args).Run();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                var configuration = setupConfiguration();
                setupLogging(configuration);

                    if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Microsoft.Extensions.Hosting.Environments.Development)
                    {
                        string certificateFileName = "localhost.pfx";
                        string certificatePassword = "stfm4all";

                        var certificate = new X509Certificate2(certificateFileName, certificatePassword);

                        webBuilder
                        .UseKestrel(
                            options =>
                            {
                                options.AddServerHeader = false;
                                options.Listen(IPAddress.Loopback, 44352, listenOptions =>
                                {
                                    listenOptions.UseHttps(certificate);
                                });
                            }
                        )
                        .UseConfiguration(configuration)
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .UseStartup<Startup>()
                        .UseUrls("https://localhost:44352");
                    }
                    else
                    {
                        webBuilder
                            .UseKestrel()
                            .UseConfiguration(configuration)
                            .UseContentRoot(Directory.GetCurrentDirectory())
                            .UseStartup<Startup>();
                    }

            });

        private static void setupLogging(IConfiguration configuration)
        {
            /*
             * Setup Serilog to also write the LogEvent data
             * to the database
             */

            ColumnOptions columnOptions = new ColumnOptions();

            columnOptions.Store.Add(StandardColumn.LogEvent);

            /*
             * Create Serilog Log object that writes the log messages
             * to an SQL Server database table named Logs
             * and also to the Console
             * this Log object can be used in any class in this project
             * NOTE I cannot figure out how to use the connection string
             * value set in user secrets.
             */
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.MSSqlServer(configuration["DefaultConnection"], "Logs", columnOptions: columnOptions)
                .WriteTo.Console()
               .CreateLogger();


            Log.Information("Getting the motors running...");
        }

        private static IConfiguration setupConfiguration()
        {
            //This configuration enabled the user secrets to be accessible
            //Code taken from this post https://github.com/serilog/serilog-sinks-applicationinsights/issues/37 by commenter Triwaters
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
                .AddEnvironmentVariables();
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Microsoft.Extensions.Hosting.Environments.Development)
            {
                configurationBuilder.AddUserSecrets<Program>(false);
            }
            IConfiguration configuration = configurationBuilder.Build();
            return configuration;
        }

        //public static IWebHost BuildWebHost(string[] args)
        //{
        //    This configuration enabled the user secrets to be accessible
        //    Code taken from this post https://github.com/serilog/serilog-sinks-applicationinsights/issues/37 by commenter Triwaters
        //    var configurationBuilder = new ConfigurationBuilder()
        //        .SetBasePath(Directory.GetCurrentDirectory())
        //        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        //        .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
        //        .AddEnvironmentVariables();
        //    if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == EnvironmentName.Development)
        //    {
        //        configurationBuilder.AddUserSecrets<Program>(false);
        //    }
        //    IConfiguration _configuration = configurationBuilder.Build();


        //    /*
        //     * Setup Serilog to also write the LogEvent data
        //     * to the database
        //     */

        //    ColumnOptions columnOptions = new ColumnOptions();

        //    columnOptions.Store.Add(StandardColumn.LogEvent);

        //    /*
        //     * Create Serilog Log object that writes the log messages
        //     * to an SQL Server database table named Logs
        //     * and also to the Console
        //     * this Log object can be used in any class in this project
        //     * NOTE I cannot figure out how to use the connection string
        //     * value set in user secrets.
        //     */
        //    Log.Logger = new LoggerConfiguration()
        //        .MinimumLevel.Debug()
        //        .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
        //        .Enrich.FromLogContext()
        //        .WriteTo.MSSqlServer(_configuration["DefaultConnection"], "Logs", columnOptions: columnOptions)
        //        .WriteTo.Console()
        //       .CreateLogger();


        //    Log.Information("Getting the motors running...");

        //    if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == EnvironmentName.Development)
        //    {
        //        string certificateFileName = "localhost.pfx";
        //        string certificatePassword = "stfm4all";

        //        var certificate = new X509Certificate2(certificateFileName, certificatePassword);

        //        return new WebHostBuilder()
        //        .UseKestrel(
        //            options =>
        //            {
        //                options.AddServerHeader = false;
        //                options.Listen(IPAddress.Loopback, 44352, listenOptions =>
        //                {
        //                    listenOptions.UseHttps(certificate);
        //                });
        //            }
        //        )
        //        .UseConfiguration(_configuration)
        //        .UseContentRoot(Directory.GetCurrentDirectory())
        //        .UseStartup<Startup>()
        //        .UseUrls("https://localhost:44352")
        //        .Build();
        //    }


        //    return new WebHostBuilder()
        //        .UseKestrel()
        //        .UseConfiguration(_configuration)
        //        .UseContentRoot(Directory.GetCurrentDirectory())
        //        .UseStartup<Startup>()
        //        .Build();

        //}
    }
}
