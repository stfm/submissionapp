﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using ConferenceSubmission.Data;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using Serilog;

namespace ConferenceSubmission.Services
{
    public class ConferenceService : IConferenceService
    {

        private readonly IMapper _mapper;

        private readonly IConferenceRepository _conferenceRepository;

        private readonly ISubmissionCategoryRepository _submissionCategoryRepository;

        private readonly ISubmissionRepository _submissionRepository;

        private readonly IFormFieldRepository _formFieldRepository;

        public ConferenceService(IMapper mapper, IConferenceRepository conferenceRepository,
            ISubmissionCategoryRepository submissionCategoryRepository,
            ISubmissionRepository submissionRepository, IFormFieldRepository formFieldRepository)
        {
            _mapper = mapper;

            _conferenceRepository = conferenceRepository;

            _submissionCategoryRepository = submissionCategoryRepository;

            _submissionRepository = submissionRepository;

            _formFieldRepository = formFieldRepository;

        }

        public List<ConferenceViewModel> GetActiveConferences()
        {
            // Due to breaking changes in EF Core 3.1, we must explicitely call ToList() on queries that return an interface like IEnumerable, otherwise
            // we get the following error: "There is already an open datareader associated with this connection which must be closed"
            // For more information, checkout the comments in this post: https://stackoverflow.com/q/59677609
            List<ConferenceViewModel> activeConferences = _mapper.Map<IEnumerable<Conference>, List<ConferenceViewModel>>(_conferenceRepository.GetActiveConferences().ToList() );

            return activeConferences;
        }

        public List<Conference> GetConferencesWithOpenReviewStatus()
        {
            return _conferenceRepository.GetConferencesWithOpenReviewStatus().ToList();
        }

        public Conference GetConference(int conferenceId)
		{
			return _conferenceRepository.GetConference(conferenceId);
		}

        public List<ConferenceViewModel> GetConferences()
        {
            List<ConferenceViewModel> allConferences = _mapper.Map<IEnumerable<Conference>, List<ConferenceViewModel>>(_conferenceRepository.GetConferences);

            return allConferences;
        }

        public List<Conference> GetConferencesWithUnstartedStatus()
        {
            return _conferenceRepository.GetConferencesWithUnstartedStatus().ToList();
        }

        public SubmissionsByCategoryForConferenceViewModel GetSubmissionsByCategoryForConference(int conferenceId)
        {
            Log.Information("About to get number of submissions by category for conferenceId " + conferenceId);

            Conference conference = _conferenceRepository.GetConference(conferenceId);

           Dictionary<string, int> completedSubmissionsByCategory = _submissionRepository.GetSubmissionsByCategory(conferenceId, true);
           
           Dictionary<string, int> inCompleteSubmissionsByCategory = _submissionRepository.GetSubmissionsByCategory(conferenceId, false);

            Dictionary<string, int> acceptedSubmissionsByCategory = _submissionRepository.GetSubmissionsByCategoryUsingStatus(conferenceId, SubmissionStatusType.ACCEPTED);

            Dictionary<string, int> rejectedSubmissionsByCategory = _submissionRepository.GetSubmissionsByCategoryUsingStatus(conferenceId, SubmissionStatusType.DECLINED);

            Dictionary<string, int> cancelledSubmissionsByCategory = _submissionRepository.GetSubmissionsByCategoryUsingStatus(conferenceId, SubmissionStatusType.CANCELED);

            Dictionary<string, int> withdrawnSubmissionsByCategory = _submissionRepository.GetSubmissionsByCategoryUsingStatus(conferenceId, SubmissionStatusType.WITHDRAWN);

            Dictionary<string, int> paidForSubmissionsByCategory = _submissionRepository.GetSubmissionsPaidForByCategory(conferenceId);


            List<SubmissionsForCategory> submissionsForCategories = new List<SubmissionsForCategory>();

            int totalSubmissionsNotFinished = 0;

            int totalSubmissionsCompleted = 0;

            int totalSubmissionsAccepted = 0;

            int totalSubmissionsRejected = 0;

            int totalSubmissionsCancelled = 0;

            int totalSubmissionsWithdrawn = 0;

            int totalSubmissionsPaidFor = 0;

            foreach(KeyValuePair<string, int> entry in completedSubmissionsByCategory)
            {

                SubmissionsForCategory submissionsForCategory = new SubmissionsForCategory
                {
                    category = entry.Key,
                    submissionsCompleted = entry.Value,

                };

                if (inCompleteSubmissionsByCategory.TryGetValue(entry.Key, out int submissionsNotFinished) )
                {
                    submissionsForCategory.submissionsNotFinished = submissionsNotFinished;
                }

                if ( acceptedSubmissionsByCategory.TryGetValue(entry.Key, out int submissionsAccepted))
                {
                    submissionsForCategory.submissionsAccepted = submissionsAccepted;
                }

                if (rejectedSubmissionsByCategory.TryGetValue(entry.Key, out int submissionsRejected))
                {
                    submissionsForCategory.submissionsRejected = submissionsRejected;
                }

                if (cancelledSubmissionsByCategory.TryGetValue(entry.Key, out int submissionsCancelled))
                {
                    submissionsForCategory.submissionsCancelled = submissionsCancelled;
                }

                if (withdrawnSubmissionsByCategory.TryGetValue(entry.Key, out int submissionsWithdrawn))
                {
                    submissionsForCategory.submissionsWithdrawn = submissionsWithdrawn;
                }

                if (paidForSubmissionsByCategory.TryGetValue(entry.Key, out int submissionsPaidFor))
                {
                    submissionsForCategory.submissionsPaidFor = submissionsPaidFor;
                }


                submissionsForCategory.totalSubmissions = submissionsForCategory.submissionsCompleted + submissionsForCategory.submissionsNotFinished +
                    submissionsForCategory.submissionsWithdrawn + submissionsForCategory.submissionsCancelled;

                totalSubmissionsNotFinished += submissionsForCategory.submissionsNotFinished;

                totalSubmissionsCompleted += submissionsForCategory.submissionsCompleted;

                totalSubmissionsAccepted += submissionsForCategory.submissionsAccepted;

                totalSubmissionsRejected += submissionsForCategory.submissionsRejected;

                totalSubmissionsCancelled += submissionsForCategory.submissionsCancelled;

                totalSubmissionsWithdrawn += submissionsForCategory.submissionsWithdrawn;

                totalSubmissionsPaidFor += submissionsForCategory.submissionsPaidFor;

                submissionsForCategories.Add(submissionsForCategory);
            }

            SubmissionsForCategory totalSubmissionsForCategory = new SubmissionsForCategory
            {
                category = "Totals",
                submissionsCompleted = totalSubmissionsCompleted,
                submissionsNotFinished = totalSubmissionsNotFinished,
                submissionsAccepted = totalSubmissionsAccepted,
                submissionsRejected = totalSubmissionsRejected,
                submissionsCancelled = totalSubmissionsCancelled,
                submissionsWithdrawn = totalSubmissionsWithdrawn,
                totalSubmissions = totalSubmissionsCompleted + totalSubmissionsNotFinished + totalSubmissionsWithdrawn + totalSubmissionsCancelled,
                submissionsPaidFor = totalSubmissionsPaidFor

            };

            Log.Information("Number of categories found " + submissionsForCategories.Count);

            submissionsForCategories.Add(totalSubmissionsForCategory);

            return new SubmissionsByCategoryForConferenceViewModel
            {
                ConferenceId = conferenceId,
                 ConferenceCFPEndDate = conference.ConferenceCFPEndDate.ToString(),
                SubmissionsForCategories = submissionsForCategories

            };


        }

        public void RetireConference(int conferenceId)
        {
            _conferenceRepository.RetireConference(conferenceId);
        }

        public int CreateNextConference(int conferenceId)
        {

            Conference currentConference = _conferenceRepository.GetConference(conferenceId);

            string currentConferenceLongName = currentConference.ConferenceLongName;

            string nextConferenceLongName = currentConferenceLongName;

            string conferenceYearStr = Regex.Match(currentConferenceLongName, @"\d+").Value;

            if (conferenceYearStr != null)
            {

                Int32 conferenceYearInt = Int32.Parse(conferenceYearStr) + 1;

                nextConferenceLongName = currentConferenceLongName.Replace(conferenceYearStr, conferenceYearInt.ToString());

            } 

            string currentConferenceShortName = currentConference.ConferenceShortName;

            string nextConferenceShortName = currentConferenceShortName;

            conferenceYearStr = Regex.Match(currentConferenceShortName, @"\d+").Value;

            if (conferenceYearStr != null)
            {

                Int32 conferenceYearInt = Int32.Parse(conferenceYearStr) + 1;

                nextConferenceShortName = currentConferenceShortName.Replace(conferenceYearStr, conferenceYearInt.ToString());
            }

            Conference nextConference = new Conference
            {
                CategoryDetailUrl = currentConference.CategoryDetailUrl,
                ConferenceCFPEndDate = currentConference.ConferenceCFPEndDate.AddYears(1),
                ConferenceDeleted = currentConference.ConferenceDeleted,
                ConferenceEndDate = currentConference.ConferenceEndDate.AddYears(1),
                ConferenceStartDate = currentConference.ConferenceStartDate.AddYears(1),
                ConferenceHybrid = currentConference.ConferenceHybrid,
                ConferenceInactive = currentConference.ConferenceInactive,
                ConferenceLongName = nextConferenceLongName,
                ConferenceShortName = nextConferenceShortName,
                ConferenceSubmissionReviewEndDate = currentConference.ConferenceSubmissionReviewEndDate.Value.AddYears(1),
                ConferenceType = currentConference.ConferenceType,
                ConferenceTypeId = currentConference.ConferenceTypeId,
                ConferenceVirtual = currentConference.ConferenceVirtual,
                SubmissionPaymentRequirementText = currentConference.SubmissionPaymentRequirementText,

            };

            int nextConferenceId = _conferenceRepository.CreateConference(nextConference);

            List<SubmissionCategory> submissionCategories = _submissionCategoryRepository.GetSubmissionCategories(currentConference.ConferenceId).ToList();

            foreach (SubmissionCategory submissionCategory in submissionCategories)
            {

                var submissionCategoryToAdd = new SubmissionCategory
                {
                    CategoryAbstractInstructions = submissionCategory.CategoryAbstractInstructions,
                    CategoryAbstractMaxLength = submissionCategory.CategoryAbstractMaxLength,
                    ConferenceId = nextConferenceId,
                    SubmissionCategoryInactive = submissionCategory.SubmissionCategoryInactive,
                    SubmissionCategoryName = submissionCategory.SubmissionCategoryName,
                    SubmissionPaymentRequirement = submissionCategory.SubmissionPaymentRequirement,
                    CategoryInstructions = submissionCategory.CategoryInstructions,

                };

                _submissionCategoryRepository.CreateSubmissionCategory(submissionCategoryToAdd);


            }


            return nextConferenceId;

        }

        public void CreateReviewForms(int conferenceId)
        {

            Conference currentConference = _conferenceRepository.GetConference(conferenceId);

            string currentConferenceShortName = currentConference.ConferenceShortName;

            string previousConferenceShortName = currentConferenceShortName;

            string conferenceYearStr = Regex.Match(currentConferenceShortName, @"\d+").Value;

            if (conferenceYearStr != null)
            {

                Int32 conferenceYearInt = Int32.Parse(conferenceYearStr) - 1;

                previousConferenceShortName = previousConferenceShortName.Replace(conferenceYearStr, conferenceYearInt.ToString());

            }

           Conference previousConference = _conferenceRepository.GetConference(previousConferenceShortName);

           CreateSubmissionCategoryReviewForms(previousConference.ConferenceId, conferenceId);

        }

        public bool ToggleConferenceInactive(int conferenceId)
        {
            return _conferenceRepository.ToggleConferenceInactive(conferenceId);
        }

        public DateTime EditCFPEndDate(int conferenceId, DateTime endDate)
        {
            return _conferenceRepository.EditCFPEndDate(conferenceId, endDate);
        }

        public DateTime EditSubmissionReviewEndDate(int conferenceId, DateTime endDate)
        {
            return _conferenceRepository.EditSubmissionReviewEndDate(conferenceId, endDate);
        }

        public List<ConferenceViewModel> GetActiveCeraConferences()
        {
            // Due to breaking changes in EF Core 3.1, we must explicitely call ToList() on queries that return an interface like IEnumerable, otherwise
            // we get the following error: "There is already an open datareader associated with this connection which must be closed"
            // For more information, checkout the comments in this post: https://stackoverflow.com/q/59677609
            List<ConferenceViewModel> activeConferences = _mapper.Map<IEnumerable<Conference>, List<ConferenceViewModel>>(_conferenceRepository.GetActiveCeraConferences().ToList());

            return activeConferences;
        }

        public string GetConferenceName(int conferenceId)
        {
            var conferenceName = _conferenceRepository.GetConferenceName(conferenceId);
            return conferenceName;
        }

        public IEnumerable<EntityByNameAndId> GetConferencesByDateRange(DateTime startDate, DateTime endDate)
        {
            var conferences = _conferenceRepository.GetConferencesByDateRange(startDate, endDate);

            return conferences;
        }

        public void CreateProposalForms(int oldConferenceId, int newConferenceId)
        {
            // Get submission categories for old conference
            // Get submission categories for new conference
            // Create dictionary key is old submission categoryid and value is new submission category id
            List<SubmissionCategory> oldSubmissionCategories = _submissionCategoryRepository.GetSubmissionCategories(oldConferenceId).ToList();

            List<SubmissionCategory> newSubmissionCategories = _submissionCategoryRepository.GetSubmissionCategories(newConferenceId).ToList();

            Dictionary<int, int> categoryDict = new Dictionary<int, int>();

            foreach (SubmissionCategory submissionCategory in oldSubmissionCategories)
            {

                string oldSubmissionCategoryName = submissionCategory.SubmissionCategoryName;

                SubmissionCategory newSubmissionCategory = newSubmissionCategories.Where(sc => sc.SubmissionCategoryName.Equals(oldSubmissionCategoryName)).FirstOrDefault();

                if (newSubmissionCategory != null)
                {

                    categoryDict[submissionCategory.SubmissionCategoryId] = newSubmissionCategory.SubmissionCategoryId;

                }

            }

            //for each dictionary key-value do the following

            foreach (KeyValuePair<int, int> entry in categoryDict)
            {


                // Get list of formfields for the old submission category

                List<FormField> oldCategoryFormFields = _formFieldRepository.GetFormFields(entry.Key, "proposal").ToList();

                foreach (FormField oldFormField in oldCategoryFormFields)
                {
                    // For each formfield in old category do the following

                    // Create new FormField with same details but new category id

                    FormField newFormField = new FormField
                    {
                        AnswerRequired = oldFormField.AnswerRequired,
                        FormFieldDeleted = oldFormField.FormFieldDeleted,
                        FormFieldRole = oldFormField.FormFieldRole,
                        FormFieldSortOrder = oldFormField.FormFieldSortOrder,
                        FormFieldType = oldFormField.FormFieldType,
                        SubmissionCategoryId = entry.Value

                    };

                    List<FormFieldProperty> oldFormFieldProperties = oldFormField.FormFieldProperties.ToList();

                    foreach (FormFieldProperty oldFormFieldProperty in oldFormFieldProperties)
                    {
                        //for each old form field's form field properties create a new form field property 

                        FormFieldProperty newFormFieldProperty = new FormFieldProperty
                        {
                            FormField = newFormField,
                            PropertyName = oldFormFieldProperty.PropertyName,
                            PropertyValue = oldFormFieldProperty.PropertyValue
                        };

                        newFormField.FormFieldProperties.Add(newFormFieldProperty);

                    }

                    //Add new form field with its form field properties to the database

                    int newFormFieldId = _formFieldRepository.CreateFormField(newFormField);

                }


            }
        }

            public void CreateSubmissionCategoryReviewForms(int oldConferenceId, int newConferenceId)
            {
                // Get submission categories for old conference
                // Get submission categories for new conference
                // Create dictionary key is old submission categoryid and value is new submission category id
                List<SubmissionCategory> oldSubmissionCategories = _submissionCategoryRepository.GetSubmissionCategories(oldConferenceId).ToList();

                List<SubmissionCategory> newSubmissionCategories = _submissionCategoryRepository.GetSubmissionCategories(newConferenceId).ToList();

                Dictionary<int, int> categoryDict = new Dictionary<int, int>();

                foreach (SubmissionCategory submissionCategory in oldSubmissionCategories)
                {

                    string oldSubmissionCategoryName = submissionCategory.SubmissionCategoryName;

                    SubmissionCategory newSubmissionCategory = newSubmissionCategories.Where(sc => sc.SubmissionCategoryName.Equals(oldSubmissionCategoryName)).FirstOrDefault();

                    if (newSubmissionCategory != null)
                    {

                        categoryDict[submissionCategory.SubmissionCategoryId] = newSubmissionCategory.SubmissionCategoryId;

                    }

                }

                //for each dictionary key-value do the following

                foreach (KeyValuePair<int, int> entry in categoryDict)
                {


                    // Get list of revivew formfields for the old submission category

                    List<FormField> oldCategoryFormFields = _formFieldRepository.GetFormFields(entry.Key, "review").ToList();

                    foreach (FormField oldFormField in oldCategoryFormFields)
                    {
                        // For each formfield in old category do the following

                        // Create new FormField with same details but new category id

                        FormField newFormField = new FormField
                        {
                            AnswerRequired = oldFormField.AnswerRequired,
                            FormFieldDeleted = oldFormField.FormFieldDeleted,
                            FormFieldRole = oldFormField.FormFieldRole,
                            FormFieldSortOrder = oldFormField.FormFieldSortOrder,
                            FormFieldType = oldFormField.FormFieldType,
                            SubmissionCategoryId = entry.Value

                        };

                        List<FormFieldProperty> oldFormFieldProperties = oldFormField.FormFieldProperties.ToList();

                        foreach (FormFieldProperty oldFormFieldProperty in oldFormFieldProperties)
                        {
                            //for each old form field's form field properties create a new form field property 

                            FormFieldProperty newFormFieldProperty = new FormFieldProperty
                            {
                                FormField = newFormField,
                                PropertyName = oldFormFieldProperty.PropertyName,
                                PropertyValue = oldFormFieldProperty.PropertyValue
                            };

                            newFormField.FormFieldProperties.Add(newFormFieldProperty);

                        }

                        //Add new form field with its form field properties to the database

                        int newFormFieldId = _formFieldRepository.CreateFormField(newFormField);

                    }


                }


            }
        
    }
}
