﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents a submission for presentation at a conference.
    /// </summary>
    [Auditable]
    public class Submission
    {
        public Submission()
        {

            ParticipantLink = new List<SubmissionParticipant>();

        }

        public int SubmissionId { get; set; }

        [StringLength(250)]
        [Required]
        public string SubmissionTitle { get; set; }

        [Required]
        public string SubmissionAbstract { get; set; }

        /// <summary>
        /// Represents the category the submitter entered 
        /// originally for the proposed presentation.
        /// </summary>
        /// <value>The submission category identifier.</value>
        [ForeignKey("SubmissionCategory")]
        public int SubmissionCategoryId { get; set; }

        public SubmissionCategory SubmissionCategory { get; set; }

        /// <summary>
        /// Initially the AcceptedCategoryId will match the
        /// SubmissionCategoryId.  After the submission is
        /// accepted this value may be changed if the submission
        /// will be presented in a category that is different
        /// then the original SubmissionCategoryId.
        /// </summary>
        /// <value>The accepted category identifier.</value>

        [ForeignKey("AcceptedCategory")]
        public int AcceptedCategoryId { get; set; }

        public SubmissionCategory AcceptedCategory { get; set; }

        public DateTime SubmissionCreatedDateTime { get; set; }

        public DateTime SubmissionLastUpdatedDateTime { get; set; }

        [Auditable]
        public int SubmissionStatusId { get; set; }

        public SubmissionStatus SubmissionStatus { get; set; }

        public SubmissionPayment SubmissionPayment { get; set; }

        public ConferenceSession ConferenceSession { get; set; }

        public List<VirtualConferenceSession> VirtualConferenceSessions {get; set;}

        public List<SubmissionParticipant> ParticipantLink { get; set; }

        public List<VirtualMaterial> VirtualMaterials { get; set; }

        [ForeignKey("ConferenceId")]
        public Conference Conference { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="T:ConferenceSubmission.ViewModels.SubmissionWithProposalAnswersViewModel"/> submission 
        /// is completed.
        /// A submission is considered completed if user has answered all the category
        /// specific proposal form questions where an answer is required.
        /// </summary>
        /// <value><c>true</c> if submission completed; otherwise, <c>false</c>.</value>
        public bool SubmissionCompleted { get; set; }

        /// <summary>
        /// If true an admin person has edited the accepted submission
        /// for display online and in the brochure.  
        /// </summary>
        public bool SubmissionAdminEdited { get; set; }

        /// <summary>
        /// If true, the participant order has been modified by a submission admin or by a 
        /// submission participant
        /// </summary>
        public bool ParticipantOrderModified { get; set; }

        public List<SubmissionPresentationMethod> SubmissionPresentationMethods {get; set;}

    }
}
