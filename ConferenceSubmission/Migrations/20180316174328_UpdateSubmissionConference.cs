﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class UpdateSubmissionConference : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.AlterColumn<int>(
                name: "ConferenceId",
                table: "Submissions",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Conferences_ConferenceId",
                table: "Submissions",
                column: "ConferenceId",
                principalTable: "Conferences",
                principalColumn: "ConferenceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Conferences_ConferenceId",
                table: "Submissions");

            migrationBuilder.AlterColumn<int>(
                name: "ConferenceId",
                table: "Submissions",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Conferences_ConferenceId",
                table: "Submissions",
                column: "ConferenceId",
                principalTable: "Conferences",
                principalColumn: "ConferenceId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
