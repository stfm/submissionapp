﻿using System;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.Data
{
    public interface ISubmissionReviewerRepository
    {
        /// <summary>
        /// Adds the submission reviewer.
        /// </summary>
        /// <returns>The submission reviewer id.</returns>
        /// <param name="submissionReviewer">Submission reviewer.</param>
        int AddSubmissionReviewer(SubmissionReviewer submissionReviewer);

        /// <summary>
        /// Gets the submission reviewer.
        /// </summary>
        /// <returns>The submission reviewer.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="reviewerId">Reviewer identifier.</param>
        SubmissionReviewer GetSubmissionReviewer(int submissionId, int reviewerId);

        /// <summary>
        /// Gets the submission reviewers for the provided submission id.
        /// </summary>
        /// <returns>The submission reviewers.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        IEnumerable<SubmissionReviewer> GetSubmissionReviewers(int submissionId);

        /// <summary>
        /// Gets the submission reviewers for the provided Reviewer.
        /// </summary>
        /// <returns>The submission reviewers.</returns>
        /// <param name="reviewer">Reviewer.</param>
        IEnumerable<SubmissionReviewer> GetSubmissionReviewers(Reviewer reviewer);
        
        /// <summary>
        /// Delete the provided SubmissionReviewer from the data repository.
        /// </summary>
        /// <param name="submissionReviewer">A SubmissionReviewer object populated from the data repository.</param>
        void DeleteSubmissionReviewer(SubmissionReviewer submissionReviewer);
    }
}
