﻿using System;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Status of the review.
    /// </summary>
    public enum ReviewStatusType
    {

        NOT_STARTED,

        INCOMPLETE,

        COMPLETE

    }
}
