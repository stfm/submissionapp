﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ReviewerSummaryForConference
    {

        public Conference conference { get; set; }

        public int totalAssignedSubmissionsToReview { get; set; }

        public int totalReviewsCompleted { get; set; }



    }
}
