﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class DataTableParameters
    {
        public DataTableParameters()
        {
            LengthMenuValues = new List<int>();
            LengthMenuDisplayValues = new List<string>();
        }

        public List<int>    LengthMenuValues        { get; set; }
        public List<string> LengthMenuDisplayValues { get; set; }
        public int          DefaultPageLength       { get; set; }
        public bool         DownloadAsCSV           { get; set; }
        public bool         SessionStateStorage     { get; set; }
        public string       ElementId               { get; set; }

        public string GetLengthMenuValues()
        {
            var result = "[";

            foreach(var value in LengthMenuValues)
            {
                result += $"{value}, ";
            }

            result = $"{result.TrimEnd(new char[] {',',' ' })}]";

            return result;
        }

        public string GetLengthMenuDisplayValues()
        {
            var result = "[";

            foreach (var value in LengthMenuDisplayValues)
            {
                result += int.TryParse(value, out int n) ? $"{int.Parse(value)}, " : $"'{value}', ";
            }

            result = $"{result.TrimEnd(new char[] { ',', ' ' })}]";

            return result;
        }
    }
}
