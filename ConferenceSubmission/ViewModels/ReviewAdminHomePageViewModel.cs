﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class ReviewAdminHomePageViewModel
    {
        public List<Conference> conferences { get; set; }


        public ReviewAdminHomePageViewModel()
        {
            conferences = new List<Conference>();
        }
    }
}
