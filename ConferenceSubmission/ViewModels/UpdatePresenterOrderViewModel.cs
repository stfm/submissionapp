﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class UpdatePresenterOrderViewModel
    {
        public UpdatePresenterOrderViewModel()
        {
            Presenters = new List<Presenter>();
        }
        public string           ConferenceName           { get; set; }
        public string           SubmittedCategory        { get; set; }
        public string           AcceptedCategory         { get; set; }
        [Required]
        public int              SubmissionId             { get; set; }
        [Required]
        public string           SubmissionParticipantIds { get; set; }
        public string           SubmissionTitle          { get; set; }
        public List<Presenter>  Presenters               { get; set; }

        public string GetOrderedPresenterIdsAsCommaSeparatedList()
        {
            return string.Join(',', Presenters.OrderBy(p => p.SortOrder).Select(p => p.SubmissionParticipantId.ToString()).ToList());
        }
    }
}
