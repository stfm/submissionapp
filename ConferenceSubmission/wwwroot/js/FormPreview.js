﻿let loadConferenceCategories = function () {
    let selectedConferenceId = $("#selectConference").val();
    if (selectedConferenceId === "")
    {
        return;
    }

    $("#categoriesContainer").empty();
    $("#formContainer").empty();
    $("#categoriesContainer").append(getSpinner("Loading Conference Categories"));

    $.ajax({
        url: "/submissionadmin/GetConferenceCategories?conferenceId=" + selectedConferenceId,
        dataType: "html",
        method: "GET"
    }).done(function (data) {
        $("#formContainer").empty();     
        $("#categoriesContainer").html(data);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#categoriesContainer").html("<p class='text-red'>The conference categories could not be loaded. There was an error on the server.</p>");
    });
};

let loadForm = function () {
    let selectedConferenceId = $("#selectConference").val();
    let selectedCategoryId = $("#confernceCategorySelect").val();
    let previewMode = $("#formPreviewModeSelect").val();

    if (selectedConferenceId === "" || selectedCategoryId === "") {
        return;
    }

    $("#formContainer").empty();
    $("#formContainer").append(getSpinner("Loading Form"));

    $.ajax({
        url: "/submissionadmin/GetPreviewForm?conferenceId=" + selectedConferenceId + "&categoryId=" + selectedCategoryId + "&previewMode=" + previewMode,
        dataType: "html",
        method: "GET"
    }).done(function (data) {
        $("#formContainer").html(data);
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#formContainer").offset().top-150
        }, 500);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#formContainer").html("<p class='text-red'>The conference categories could not be loaded. There was an error on the server.</p>");
    });
};

let previewFormSelectOnChange = function (formFieldId) {
    $(`#previewFormSelect-${formFieldId}`).val($(`#previewFormSelect-${formFieldId} option:first`).val());
};

let getSpinner = function (message)
{
    return `<div class='fa-2x text-primary'><span><i class='fas fa-spinner fa-spin'></i> ${message}</span></div>`;
}