﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class FAQRepositoryTest
    {
        [Fact]
        public void GetFAQs_Test()
        {
            //arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            var result = new List<FAQ>();

            //act
            using (var context = new AppDbContext(options))
            {
                var faqRepository = new FAQRepository(context);
                result = faqRepository.GetFAQs();
            }

            //assert
            Assert.NotEmpty(result);
            Assert.Equal(3, result.Count);
        }





        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                context.FAQs.Add(new FAQ {
                    FAQId = 1,
                    Question = "How many stars are in the universe?",
                    Answer = "Very, very, very many!",
                    SortOrder = 0,
                    Display = true
                });
                context.FAQs.Add(new FAQ
                {
                    FAQId = 2,
                    Question = "How rich is Bill Gates?",
                    Answer = "A lot richer than me!",
                    SortOrder = 1,
                    Display = true
                });
                context.FAQs.Add(new FAQ
                {
                    FAQId = 3,
                    Question = "Why is the sky blue?",
                    Answer = "A clear cloudless day-time sky is blue because molecules in the air scatter blue light from the sun more than they scatter red light.",
                    SortOrder = 2,
                    Display = true
                });

                context.SaveChanges();
            }

            return options;
        }
    }
}
