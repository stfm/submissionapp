﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class VirtualConferenceDataViewModel
    {

        public VirtualConferenceData VirtualConferenceData { get; set; }

        public Submission Submission { get; set; }

    }
}
