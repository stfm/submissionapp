﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Services
{
    public class MockConferenceService : IConferenceService
    {
        public List<ConferenceViewModel> GetActiveConferences()
        {
            return new List<ConferenceViewModel>()
            {
                new ConferenceViewModel()
                {
                    ConferenceId = 1,
                    ConferenceCFPEndDate = new DateTime(2018, 5, 21),
                    ConferenceLongName = "2018 Annual Spring Conference",
                    ConferenceShortName = "AN18"
                },
                new ConferenceViewModel()
                {
                    ConferenceId = 2,
                    ConferenceCFPEndDate = new DateTime(2019, 2, 7),
                    ConferenceLongName = "2018 Conference on Practice Improvement",
                    ConferenceShortName = "CPI18"
                },
                 new ConferenceViewModel()
                 {
                     ConferenceId = 3,
                     ConferenceCFPEndDate = new DateTime(2018, 12, 4),
                     ConferenceLongName = "2019 Conference on Medical Student Education",
                     ConferenceShortName = "MSE18"
                 }
            };
        }

        public List<Conference> GetConferencesWithOpenReviewStatus()
        {
            throw new NotImplementedException();
        }

        public Conference GetConference(int conferenceId)
		{
			throw new NotImplementedException();
		}

        public List<ConferenceViewModel> GetConferences()
        {
            throw new NotImplementedException();
        }

        public List<Conference> GetConferencesWithUnstartedStatus()
        {
            throw new NotImplementedException();
        }

        public SubmissionsByCategoryForConferenceViewModel GetSubmissionsByCategoryForConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public void RetireConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public bool ToggleConferenceInactive(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public DateTime EditCFPEndDate(int conferenceId, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public DateTime EditSubmissionReviewEndDate(int conferenceId, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public List<ConferenceViewModel> GetActiveCeraConferences()
        {
            throw new NotImplementedException();
        }

        public string GetConferenceName(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EntityByNameAndId> GetConferencesByDateRange(DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public int CreateNextConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public void CreateProposalForms(int oldConferenceId, int newConferenceId)
        {
            throw new NotImplementedException();
        }

        public void CreateReviewForms(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public void CreateSubmissionCategoryReviewForms(int oldConferenceId, int newConferenceId)
        {
            throw new NotImplementedException();
        }
    }
}
