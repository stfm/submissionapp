﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class virtualconferencedata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ConferenceVirtual",
                table: "Conferences",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "VirtualConferenceData",
                columns: table => new
                {
                    VirtualConferenceDataId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConferenceId = table.Column<int>(nullable: false),
                    BlobContainerName = table.Column<string>(maxLength: 100, nullable: true),
                    DropboxFolderName = table.Column<string>(maxLength: 100, nullable: true),
                    DropboxUploadUrl = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VirtualConferenceData", x => x.VirtualConferenceDataId);
                    table.ForeignKey(
                        name: "FK_VirtualConferenceData_Conferences_ConferenceId",
                        column: x => x.ConferenceId,
                        principalTable: "Conferences",
                        principalColumn: "ConferenceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VirtualConferenceData_ConferenceId",
                table: "VirtualConferenceData",
                column: "ConferenceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VirtualConferenceData");

            migrationBuilder.DropColumn(
                name: "ConferenceVirtual",
                table: "Conferences");
        }
    }
}
