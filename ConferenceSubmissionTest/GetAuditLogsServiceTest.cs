﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class GetAuditLogsServiceTest
    {
        #region setup

        private int entityId;

        private List<User> stfmUsers;

        private List<AuditLog> auditLogs;

        private GetAuditLogsService getAuditLogsService;

        public GetAuditLogsServiceTest()
        {
            this.stfmUsers = new List<User>();
            this.auditLogs = new List<AuditLog>();

            var mockSalesForceAPIService = new Mock<ISalesforceAPIService>();

            var mockAuditLogsRepository = new Mock<IAuditLogRepository>();

            mockSalesForceAPIService.Setup(s => s.GetMultipleUserInfo(It.IsAny<List<string>>())).ReturnsAsync(() => this.stfmUsers);

            mockAuditLogsRepository.Setup(a => a.GetByEntityId(It.IsAny<int>(), It.IsAny<EntityType>())).Returns(() => this.auditLogs);

            this.getAuditLogsService = new GetAuditLogsService(mockAuditLogsRepository.Object, mockSalesForceAPIService.Object);
        }

        #endregion

        [Fact]
        public void GetByEntityId_StubAnAuditLog_VerifyResults()
        {
            //arrange
            this.entityId = 123;

            this.auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    EntityId = this.entityId,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmeAAG",
                    EntityType = EntityType.Submission,
                    NewValue = "New Value",
                    OldValue = "Old Value",
                    PropertyName = "My Property",
                    TimeStamp = DateTime.Now
                }
            };

            this.stfmUsers = new List<User>
            {
                new User
                {
                    STFMUserId = "0051h000001TMmeAAG",
                    FirstName = "John",
                    LastName = "Doe",
                }
            };

            //act
            var results = this.getAuditLogsService.GetByEntityId(this.entityId, EntityType.Submission);

            //assert
            Assert.NotEmpty(results);
            Assert.Single(results);
            Assert.Equal("John Doe", results.FirstOrDefault().ChangedBy);
            Assert.Equal("New Value", results.FirstOrDefault().NewValue);
            Assert.Equal("Old Value", results.FirstOrDefault().OldValue);
        }

        [Fact]
        public void GetByEntityId_StubMultipleAuditLogsWhereSomeAreFromChildEntities_VerifyResults()
        {
            //arrange
            this.entityId = 123;

            this.auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    EntityId = this.entityId,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmeAAG",
                    EntityType = EntityType.Submission,
                    NewValue = "New Value",
                    OldValue = "Old Value",
                    PropertyName = "My Property",
                    TimeStamp = DateTime.Now
                },
                new AuditLog
                {
                    AuditLogId = 2,
                    EntityId = 99,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmeAAG",
                    EntityType = EntityType.ConferenceSession,
                    NewValue = "ENTITY CREATED",
                    OldValue = "",
                    PropertyName = "",
                    TimeStamp = DateTime.Now
                },
                new AuditLog
                {
                    AuditLogId = 3,
                    EntityId = 100,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmeAAG",
                    EntityType = EntityType.SubmissionParticipant,
                    NewValue = "ENTITY DELETED",
                    OldValue = "",
                    PropertyName = "",
                    TimeStamp = DateTime.Now
                }
            };

            this.stfmUsers = new List<User>
            {
                new User
                {
                    STFMUserId = "0051h000001TMmeAAG",
                    FirstName = "John",
                    LastName = "Doe",
                }
            };

            //act
            var results = this.getAuditLogsService.GetByEntityId(this.entityId, EntityType.Submission);

            //assert
            Assert.Equal("ENTITY CREATED (ConferenceSession)", results[1].NewValue);
            Assert.Equal("ENTITY DELETED (SubmissionParticipant)", results[2].NewValue);
        }

        [Fact]
        public void GetByEntityId_StubMultipleAuditLogs_VerifyResults()
        {
            //arrange
            this.entityId = 123;

            this.auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    EntityId = this.entityId,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmeAAG",
                    EntityType = EntityType.Submission,
                    NewValue = "New Value",
                    OldValue = "Old Value",
                    PropertyName = "Property 1",
                    TimeStamp = DateTime.Now
                },
                new AuditLog
                {
                    AuditLogId = 2,
                    EntityId = this.entityId,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmePSX",
                    EntityType = EntityType.Submission,
                    NewValue = "New Value",
                    OldValue = "Old Value",
                    PropertyName = "Property 2",
                    TimeStamp = DateTime.Now
                },
                new AuditLog
                {
                    AuditLogId = 3,
                    EntityId = this.entityId,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmePSX",
                    EntityType = EntityType.Submission,
                    NewValue = "New Value",
                    OldValue = "Old Value",
                    PropertyName = "Property 2",
                    TimeStamp = DateTime.Now
                }
            };

            this.stfmUsers = new List<User>
            {
                new User
                {
                    STFMUserId = "0051h000001TMmeAAG",
                    FirstName = "John",
                    LastName = "Doe",
                },
                new User
                {
                    STFMUserId = "0051h000001TMmePSX",
                    FirstName = "Jane",
                    LastName = "Doe",
                }
            };

            //act
            var results = this.getAuditLogsService.GetByEntityId(this.entityId, EntityType.Submission);

            //assert
            Assert.NotEmpty(results);
            Assert.Equal(3, results.Count);
            Assert.Equal("John Doe", results.FirstOrDefault(r => r.AuditLogId == 1).ChangedBy);
            Assert.Equal("New Value", results.FirstOrDefault(r => r.AuditLogId == 1).NewValue);
            Assert.Equal("Old Value", results.FirstOrDefault(r => r.AuditLogId == 1).OldValue);
            Assert.Equal("Jane Doe", results.FirstOrDefault(r => r.AuditLogId == 2).ChangedBy);
            Assert.Equal("New Value", results.FirstOrDefault(r => r.AuditLogId == 2).NewValue);
            Assert.Equal("Old Value", results.FirstOrDefault(r => r.AuditLogId == 2).OldValue);
            Assert.Equal("Jane Doe", results.FirstOrDefault(r => r.AuditLogId == 3).ChangedBy);
            Assert.Equal("New Value", results.FirstOrDefault(r => r.AuditLogId == 3).NewValue);
            Assert.Equal("Old Value", results.FirstOrDefault(r => r.AuditLogId == 3).OldValue);
        }

        [Fact]
        public void GetByEntityId_StubAnAuditLogSalesforceAPIDoesNotReturnMatchingUser_VerifyUserIdReturnedInsteadOfUserName()
        {
            //arrange
            this.entityId = 123;

            this.auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    EntityId = this.entityId,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmeAAG",
                    EntityType = EntityType.Submission,
                    NewValue = "New Value",
                    OldValue = "Old Value",
                    PropertyName = "My Property",
                    TimeStamp = DateTime.Now
                }
            };

            this.stfmUsers = new List<User>
            {
                new User
                {
                    STFMUserId = "0051h000001QWmePQX",
                    FirstName = "John",
                    LastName = "Doe",
                }
            };

            //act
            var results = this.getAuditLogsService.GetByEntityId(this.entityId, EntityType.Submission);

            //assert
            Assert.NotEmpty(results);
            Assert.Single(results);
            Assert.Equal("0051h000001TMmeAAG", results.FirstOrDefault().ChangedBy);
            Assert.Equal("New Value", results.FirstOrDefault().NewValue);
            Assert.Equal("Old Value", results.FirstOrDefault().OldValue);
        }

        [Fact]
        public void GetByEntityId_StubAnAuditLogSalesforceAPIReturnsNoUsers_VerifyUserIdReturnedInsteadOfUserName()
        {
            //arrange
            this.entityId = 123;

            this.auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    EntityId = this.entityId,
                    ParentId = null,
                    ChangedBy = "0051h000001TMmeAAG",
                    EntityType = EntityType.Submission,
                    NewValue = "New Value",
                    OldValue = "Old Value",
                    PropertyName = "My Property",
                    TimeStamp = DateTime.Now
                }
            };

            //act
            var results = this.getAuditLogsService.GetByEntityId(this.entityId, EntityType.Submission);

            //assert
            Assert.NotEmpty(results);
            Assert.Single(results);
            Assert.Equal("0051h000001TMmeAAG", results.FirstOrDefault().ChangedBy);
            Assert.Equal("New Value", results.FirstOrDefault().NewValue);
            Assert.Equal("Old Value", results.FirstOrDefault().OldValue);
        }
    }
}

