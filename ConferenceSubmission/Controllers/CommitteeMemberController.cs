﻿using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Controllers
{
    [Authorize("CommitteeMember")]
    public class CommitteeMemberController : Controller
    {
        private readonly IConferenceService _conferenceService;
        private readonly ISubmissionService _submissionService;
        private readonly IReviewAdminService _reviewAdminService;
        private readonly ISubmissionCategoryService _submissionCategoryService;

        public CommitteeMemberController(IConferenceService conferenceService, ISubmissionService submissionService, 
            IReviewAdminService reviewAdminService, ISubmissionCategoryService submissionCategoryService)
        {
            _conferenceService = conferenceService;
            _submissionService = submissionService;
            _reviewAdminService = reviewAdminService;
            _submissionCategoryService = submissionCategoryService;
        }

        public IActionResult Index()
        {

            var model = new SubmissionAdminHomeViewModel()
            {


                ActiveConferenceViewModels = _conferenceService.GetConferences()

            };

            return View(model);
        }

        public IActionResult ViewSubmissionsForConference(int conferenceId)
        {


            var model = new SubmissionsForConferenceViewModel
            {
                Conference = _conferenceService.GetConference(conferenceId),
                ConferenceSubmissions = _submissionService.GetSubmissionsForConference(conferenceId)

            };


            return View(model);

        }

        /// <summary>
        /// Responds to committe member's request to view a specific submission
        /// </summary>
        /// <returns>View for a specific submision</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public IActionResult ViewSubmission(int submissionId)
        {
            if (!_submissionService.SubmissionExists(submissionId))
            {
                return RedirectToAction("ContentNotFound", "Home", new { EntityType = "Submission", EntityId = submissionId, ReturnController = "CommitteeMember", ReturnAction = "Index" });
            }

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId);


            return View(submissionWithProposalAnswersViewModel);

        }

        /// <summary>
        /// Gets each review of the provided submission.
        /// </summary>
        /// <param name="submissionId">Submission ID number</param>
        /// <returns></returns>
        public ActionResult SubmissionDetailedReviews(int submissionId)
        {

            List<SubmissionDetailedReview> submissionDetailedReviews = _reviewAdminService.GetSubmissionDetailedReviewsForSubmission(submissionId);

            Submission submission = _submissionService.GetSubmission(submissionId);

            SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(submission.AcceptedCategoryId);


            var model = new SubmissionsDetailedReviewsViewModel()
            {

                ConferenceLongName = submissionCategory.Conference.ConferenceLongName,

                SubmissionCategoryName = submissionCategory.SubmissionCategoryName,

                SubmissionDetailedReviews = submissionDetailedReviews,

                SubmissionCategoryId = submissionCategory.SubmissionCategoryId

            };

            return View(model);

        }
    }
}
