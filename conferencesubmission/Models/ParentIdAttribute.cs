﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    [AttributeUsage(AttributeTargets.All, Inherited = false)]
    public class ParentIdAttribute : Attribute
    {
    }
}
