﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using FluentEmail.Core;
using FluentEmail.Mailgun;
using FluentEmail.Razor;
using Microsoft.Extensions.Configuration;
using Serilog;


namespace ConferenceSubmission.Services
{
    /// <summary>
    /// Defines what email notification services must be implemented.
    /// </summary>
    public interface IEmailNotificationService
    {
        /// <summary>
        /// Sends the submission created email.
        /// </summary>
        /// <param name="emailAddress">Email address.</param>
        /// <param name="submission">Submission model object used to replace tokens in email body.</param>
        void SendSubmissionCreatedEmail(string emailAddress, Submission submission);

        /// <summary>
        /// Sends the submission proposal completed email.
        /// </summary>
        /// <param name="emailAddress">Email address.</param>
        /// <param name="submissionWithProposalAnswersViewModel">Submission with proposal answers view model used to replace tokens in email body.</param>
        void SendSubmissionProposalCompletedEmail(string emailAddress, SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel);


        /// <summary>
        /// Sends the CERA submission proposal completed email.
        /// </summary>
        /// <param name="emailAddress">Email address.</param>
        /// <param name="submissionWithProposalAnswersViewModel">Submission with proposal answers view model used to replace tokens in email body.</param>
        void SendCeraSubmissionProposalCompletedEmail(string emailAddress, SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel);

        /// <summary>
        /// Sends the submission payment email.
        /// </summary>
        /// <param name="emailAddress">Email address</param>
        /// <param name="submissionPayment">Submission payment model object used to replace tokens in the email body.</param>
        void SendSubmissionPaymentEmail(string emailAddress, SubmissionPayment submissionPayment);

        /// <summary>
        /// Sends an email to submitter and to main presenter (if there is a 
        /// main presenter and it's not the same as submitter) after a person
        /// adds himself as a submission participant.
        /// </summary>
        /// <param name="submissionParticipantAdded">Submission participant added.</param>
        void SendParticipantAddedEmail(SubmissionParticipantAdded submissionParticipantAdded);

        /// <summary>
        /// Sends the submission base details edited email.
        /// </summary>
        /// <param name="submissionBaseEdited">Submission model object used to replace tokens in email body.</param>
        void SendSubmissionBaseEditedEmail(SubmissionBaseEdited submissionBaseEdited);

        /// <summary>
        /// Sends the submission proposal edited email.
        /// </summary>
        /// <param name="submissionWithProposalAnswersViewModel">Submission with proposal answers view model.</param>
        /// <param name="submitterEmail">Submitter email.</param>
        /// <param name="mainPresenterEmail">Main presenter email.</param>
        void SendSubmissionProposalEditedEmail(SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel, string submitterEmail, string mainPresenterEmail);

        /// <summary>
        /// Sends the CERA submission proposal edited email.
        /// </summary>
        /// <param name="submissionWithProposalAnswersViewModel">Submission with proposal answers view model.</param>
        /// <param name="submitterEmail">Submitter email.</param>
        /// <param name="mainPresenterEmail">Main presenter email.</param>
        void SendCeraSubmissionProposalEditedEmail(SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel, string submitterEmail, string mainPresenterEmail);

        /// <summary>
        /// Sends the disclosure email.
        /// </summary>
        /// <param name="disclosure">Disclosure.</param>
        void SendDisclosureEmail(string emailAddress, Disclosure disclosure);

        /// <summary>
        /// Sends the submission withdrawn email.
        /// </summary>
        /// <param name="submission">Submission.</param>
        /// <param name="submitterEmail">Submitter email.</param>
        /// <param name="mainPresenterEmail">Main presenter email.</param>
        void SendSubmissionWithdrawnEmail(Submission submission, string submitterEmail, string mainPresenterEmail);
        
        /// <summary>
        /// Sends email with upload material instructions.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="submission"></param>
        void SendEmailPresenterUploadMaterial(string emailAddress, Submission submission);

        /// <summary>
        /// Sends the submission accepted or rejected email to the submitter and main presenter.
        /// </summary>
        /// <param name="submissionWithProposalAnswersViewModel">Submission with proposal answers view model.</param>
        /// <param name="submissionStatusId">Submission status identifier.</param>
        ///<param name="emailAddresses">collection of email addresses</param>
        void SendSubmissionAcceptedOrRejectedEmail(SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel, int submissionStatusId, 
                                                    List<string> emailAddresses);
      /// <summary>
      /// Sends the submission canceled email.
      /// </summary>
      /// <param name="submission">Submission</param>
      /// <param name="submitterEmail"></param>
      /// <param name="mainPresenterEmail"></param>
        void SendSubmissionCancelledEmail(Submission submission, string submitterEmail, string mainPresenterEmail);

        /// <summary>
        /// Sends email to provided emailAddresses about online presentation.  The presentationStatusId
        /// determines the email text.
        /// </summary>
        /// <param name="submissionWithProposalAnswersViewModel"></param>
        /// <param name="presentationStatusId"></param>
        /// <param name="emailAddresses"></param>
        void SendEmailOnlinePresentation(SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel, int presentationStatusId, List<string> emailAddresses);

        /// <summary>
        /// Send reminder email to person for letting STFM know if they will present 
        /// in August.
        /// </summary>
        /// <param name="emailAddress"></param>
        void SendAN2020ReminderEmail(string emailAddress);

        /// <summary>
        /// Send email to provided email address
        /// for provided submission to remind
        /// receipient that he/she must 
        /// complete the submission.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="submission"></param>
        void SendIncompleteReminderEmail(string emailAddress, Submission submission);

        /// <summary>
        /// Send email to provided email addresses
        /// for provided submission to remind
        /// receipient that he/she must 
        /// complete the submission.
        /// </summary>
        /// <param name="emailAddresses">List of email addresses</param>
        /// <param name="submission"></param>
        void SendIncompleteReminderEmail(List<string> emailAddress, Submission submission);

        /// <summary>
        /// Send submitter and main presenter the email
        /// about will present in virtual conference.
        /// </summary>
        /// <param name="submission"></param>
        /// <param name="submitterEmail"></param>
        /// <param name="mainPresenterEmail"></param>
        void SendSubmissionWillPresentVirtualEmail(Submission submission, string submitterEmail, string mainPresenterEmail);
       
        /// <summary>
        /// Send submitter or main presenter initial email with instructions
        /// on how to tell STFM they will either present at virtual conference or withdraw
        /// submission.
        /// </summary>
        /// <param name="emailAddress">email address of person</param>
        /// <param name="submission">Submission</param>
        void SendInitialVirtualConferenceEmail(string emailAddress, SubmissionWithProposalAnswersViewModel submission);

        /// <summary>
        /// Sends the email that material for presentation at the
        /// virtual conference was successfully uploaded.
        /// </summary>
        /// <param name="submission">Submission.</param>
        /// <param name="submitterEmail">Submitter email.</param>
        /// <param name="mainPresenterEmail">Main presenter email.</param>
        void SendUploadedMaterialEmail(Submission submission, string submitterEmail, string mainPresenterEmail);

        /// <summary>
        /// Send email to provided email address
        /// for provided submission to remind
        /// receipient that he/she must 
        /// select will present or withdraw.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="submission"></param>
        void SendWillPresentReminderEmail(string emailAddress, Submission submission);

        /// <summary>
        /// Send email to provided email address for provided submission
        /// thanking the Presenter for notifying STFM that they will or 
        /// will not provide a recorded version of their submission for the
        /// On-Demand Conference.
        /// </summary>
        /// <param name="submitterEmail"></param>
        /// <param name="Submission"></param>
        void SendProvideRecordedOnDemandPresentationNotificationReceivedEmail(string submitterEmail, string mainPresenterEmail, ProvideRecordedOnDemandPresentationNotificationReceivedViewModel provideRecordedOnDemandPresentationNotificationReceivedViewModel);
        /// <summary>
        /// Send email to provided email address for provided submission
        /// telling the presenter that he/she must update their disclosure 
        /// and include a CV PDF
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="submissionId"></param>
        void SendCPQIDisclosureReminder(string emailAddress, int submissionId);
    }

    /// <summary>
    /// Provides services for sending emails after user completes various actions
    /// </summary>
    public class EmailNotificationService : IEmailNotificationService
    {

        private readonly IConfiguration _configuration;

        private readonly ISubmissionPrintService _submissionPrintService;

        public EmailNotificationService(IConfiguration configuration, ISubmissionPrintService submissionPrintService)
        {
            _configuration = configuration;

            _submissionPrintService = submissionPrintService;

        }

        public void SendSubmissionCreatedEmail(string emailAddress, Submission submission)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the Submission Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/submitted.cshtml");

            Email.DefaultSender = sender;

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From( _configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject("STFM Submission Created")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Created Email sent to MailGun. SubmissionId: {submission.SubmissionId}, Submitter Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Created Email may not have been sent to MailGun. SubmissionId: {submission.SubmissionId}, Submitter Email: {emailAddress}.");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"An email was not sent to MailGun because of an invalid email address. SubmissionId: {submission.SubmissionId}, Submitter Email: {emailAddress}.");

            }


        }

        public void SendSubmissionProposalCompletedEmail(string emailAddress, SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel)
        {


            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            string emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/proposalcreated.cshtml");

            string emailSubject = "STFM Submission Started But Not Completed";

            if (submissionWithProposalAnswersViewModel.SubmissionCompleted)
            {
                emailSubject = "STFM Submission Completed";
            } 

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From( _configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject(emailSubject)
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Proposal Created Email sent to MailGun. SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Submitter Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Proposal Created Email may not have been sent to MailGun. SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Submitter Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"An email was not sent to MailGun because of an invalid email address. SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Submitter Email: {emailAddress}.");

            }


        }


        public void SendSubmissionPaymentEmail(string emailAddress, SubmissionPayment submissionPayment)
        {

            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the SubmissionPayment Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submissionPayment, "/Views/Email/submissionpayment.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From( _configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject("STFM Submission Payment Successful")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Payment Email sent to MailGun. SubmissionId: {submissionPayment.SubmissionId}, Submitter Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Payment Email may not have been sent to MailGun. SubmissionId: {submissionPayment.SubmissionId}, Submitter Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"An email was not sent to MailGun because of an invalid email address. SubmissionId: {submissionPayment.SubmissionId}, Submitter Email: {emailAddress}.");

            }




        }

        public void SendParticipantAddedEmail(SubmissionParticipantAdded submissionParticipantAdded)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the SubmissionPayment Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submissionParticipantAdded, "/Views/Email/participantadded.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddresses = GetEmailAddresses(submissionParticipantAdded.SubmitterEmailAddress, submissionParticipantAdded.MainPresenterEmailAddress);

            foreach (string emailAddress in emailAddresses)
            {


                var email = Email.From( _configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                      .Subject("STFM Submission Participant Added")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Participant Added Email sent to MailGun. SubmissionId: {submissionParticipantAdded.SubmissionId}, Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Participant Added Email may not have been sent to MailGun. SubmissionId: {submissionParticipantAdded.SubmissionId}, Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }

        }

        public void SendSubmissionBaseEditedEmail(SubmissionBaseEdited submissionBaseEdited)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the SubmissionBaseEdited Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submissionBaseEdited, "/Views/Email/submissionbaseedited.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddresses = GetEmailAddresses(submissionBaseEdited.SubmitterEmailAddress, submissionBaseEdited.MainPresenterEmailAddress);

            foreach (string emailAddress in emailAddresses)

            {


                var email = Email.From( _configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                                 .Subject("STFM Submission Title and/or Abstract Edited")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Base Details Edited Email sent to MailGun. SubmissionId: {submissionBaseEdited.SubmissionId}, Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Base Details Edited Email may not have been sent to MailGun. SubmissionId: {submissionBaseEdited.SubmissionId}, Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }


        }




        public void SendSubmissionProposalEditedEmail(SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel, string submitterEmail, string mainPresenterEmail)
        {


            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the SubmissionWithProposalAnswersViewModel Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/proposaledited.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddresses = GetEmailAddresses(submitterEmail, mainPresenterEmail);

            foreach (string emailAddress in emailAddresses)
            {


                var email = Email.From( _configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                    .Subject("STFM Submission Proposal Edited")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Proposal Edited Email sent to MailGun. SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Proposal Edited Email may not have been sent to MailGun. SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }

        }

        public void SendSubmissionWithdrawnEmail(Submission submission, string submitterEmail, string mainPresenterEmail)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the SubmissionWithProposalAnswersViewModel Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/submissionwithdrawn.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddresses = GetEmailAddresses(submitterEmail, mainPresenterEmail);

            foreach (string emailAddress in emailAddresses)
            {


                var email = Email.From( _configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                    .Subject("STFM Submission Withdrawn")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Withdrawn Email sent to MailGun. SubmissionId: {submission.SubmissionId}, Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Withdrawn Email may not have been sent to MailGun. SubmissionId: {submission.SubmissionId}, Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
        }

        public void SendSubmissionWillPresentVirtualEmail(Submission submission, string submitterEmail, string mainPresenterEmail)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the SubmissionWithProposalAnswersViewModel Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/submissionwillpresentvirtual_mse2022.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddresses = GetEmailAddresses(submitterEmail, mainPresenterEmail);

            foreach (string emailAddress in emailAddresses)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                    .Subject("Will Present At STFM Virtual Conference")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Will Present  Email sent to MailGun. SubmissionId: {submission.SubmissionId}, Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Will Present Email may not have been sent to MailGun. SubmissionId: {submission.SubmissionId}, Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
        }

        public void SendDisclosureEmail(string emailAddress, Disclosure disclosure)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the Submission Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(disclosure, "/Views/Email/disclosure.cshtml");

            Email.DefaultSender = sender;

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From( _configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject("STFM Disclosure Completed")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Disclosure completed Email sent to MailGun. Disclosure ID: {disclosure.DisclosureId}, User Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Disclosure completed Email may not have been sent to MailGun. Disclosure ID: {disclosure.DisclosureId}, User Email: {emailAddress}.");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"An email was not sent to MailGun because of an invalid email address. Disclosure ID: {disclosure.DisclosureId}, User Email: {emailAddress}.");

            }
        }



        /// <summary>
        /// Get collection of email addresses to send to.
        /// If main presenter email address does not exist or matches
        /// submitter email address DO NOT add main presenter email 
        /// address to colleciton
        /// </summary>
        /// <returns>Set of email addresses</returns>
        /// <param name="submitterEmailAddress">Submitter's email address</param>
        /// <param name="mainPresenterEmailAddress">Main presenter email address</param>
        private HashSet<string> GetEmailAddresses(string submitterEmailAddress, string mainPresenterEmailAddress)
        {
            HashSet<string> emailAddresses = new HashSet<string>();


            if (validateEmail(submitterEmailAddress) == true)
            {


                emailAddresses.Add(submitterEmailAddress);

            }
            else
            {

                Log.Information($"An email was not sent to MailGun because of an invalid email address.  Submitter Email: {submitterEmailAddress}.");

            }


            if (validateEmail(mainPresenterEmailAddress) == true)
            {


                emailAddresses.Add(mainPresenterEmailAddress);

            }

            return emailAddresses;
        }



        /// <summary>
        /// Get collection of email addresses to send to.
        /// validate each email address and then add to set.
        /// Ensures we are not sending an email to a duplicate
        /// or incorrect email address.
        /// </summary>
        /// <returns>Set of email addresses</returns>
        ///<param name="emailAddresses">collection of email addresses </param>
        private HashSet<string> GetEmailAddresses(List<string> emailAddresses)
        {
            HashSet<string> emailAddressesToSend = new HashSet<string>();


            foreach (string emailAddress in emailAddresses)
            {

                if (validateEmail(emailAddress))
                {

                    emailAddressesToSend.Add(emailAddress);
                }
                else
                {
                    Log.Information($"An email was not sent to MailGun because of an invalid email address.  Email: " + emailAddress );

                }

            }

           

            return emailAddressesToSend;
        }


        private bool validateEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }
            if (!Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                return false;
            }
            return true;
        }

        public void SendSubmissionAcceptedOrRejectedEmail(SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel, int submissionStatusId, List<string> emailAddresses)
        {


            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            List<string> categoriesVirtualConf = new List<string>
            { "None" };

            string emailBody = "";
            string emailType = "";
            string subject = "";
            
            if (submissionStatusId == 3)
            {
                emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/submissionaccepted_AN2025.cshtml");
                emailType = "Accepted";
                subject = "STFM Conference Submission Decision";

            } else if (submissionStatusId == 4)
            {
                emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/submissiondeclined_frs_AN2025.cshtml");
                emailType = "Declined";
                subject = "Decision on Your STFM Conference Submission";

            } 

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddressesToSend = GetEmailAddresses(emailAddresses);

            foreach (string emailAddress in emailAddressesToSend)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress).BCC("bphillip@stfm.org")
                    .Subject(subject)
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission " + emailType + " email sent to MailGun. SubmissionId: " + submissionWithProposalAnswersViewModel.SubmissionId + " Email: " + emailAddress);
                }
                else
                {
                    Log.Information($"Submission " + emailType + " email MAY NOT HAVE BEEN sent to MailGun. SubmissionId: " + submissionWithProposalAnswersViewModel.SubmissionId + " Email: " + emailAddress);

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");

                    }
                }

            }

        }

        public void SendSubmissionCancelledEmail(Submission submission, string submitterEmail, string mainPresenterEmail)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the SubmissionWithProposalAnswersViewModel Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/submissioncancelled.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddresses = GetEmailAddresses(submitterEmail, mainPresenterEmail);

            foreach (string emailAddress in emailAddresses)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                    .Subject("STFM Submission Cancelled")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Submission Cancelled Email sent to MailGun. SubmissionId: {submission.SubmissionId}, Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Submission Cancelled Email may not have been sent to MailGun. SubmissionId: {submission.SubmissionId}, Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
        }

        public void SendInitialVirtualConferenceEmail(string emailAddress, SubmissionWithProposalAnswersViewModel submission)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );


            string emailBody = "";
            if (submission.SubmissionAcceptedCategoryName.Contains("Scholarly Topic", System.StringComparison.InvariantCultureIgnoreCase))
            {
                emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/mse2022presenter.cshtml");

            } else
            {
                emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/mse2022presenter.cshtml");
            }

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .BCC("bphillip@stfm.org")
                    .Subject("Update on Your STFM Conference Presentation")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Presenter Virtual Conference initial email sent to MailGun. Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Presenter Virtual Conference initial email not sent to MailGun. Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"Presenter Virtual Conference initial email was not sent to MailGun because of an invalid email address. Email: {emailAddress}.");

            }


        }

        public void SendCeraSubmissionProposalCompletedEmail(string emailAddress, SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );
            Submission submission = new Submission();

              //Create email body using the SubmissionWithProposalAnswersViewModel Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/ceraproposalcreated.cshtml");

            string emailSubject = "CERA Survey Submission Started But Not Completed";

            if (submissionWithProposalAnswersViewModel.SubmissionCompleted)
            {
                emailSubject = "CERA Survey Submission Completed";
            }

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)

                    .Subject(emailSubject)

                    .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"CERA Submission Proposal Created Email sent to MailGun. Email: {emailAddress}.");
                }

                else
                {
                    Log.Information($"CERA Submission Proposal Created Email may not have been sent to MailGun. SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Submitter Email: {emailAddress}");


                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"An email was not sent to MailGun because of an invalid email address. CERA SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Submitter Email: {emailAddress}.");

            }
        }

        public void SendCeraSubmissionProposalEditedEmail(SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel, string submitterEmail, string mainPresenterEmail)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the SubmissionWithProposalAnswersViewModel Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/ceraproposaledited.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddresses = GetEmailAddresses(submitterEmail, mainPresenterEmail);

            foreach (string emailAddress in emailAddresses)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                    .Subject("CERA Submission Proposal Edited")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"CERA Submission Proposal Edited Email sent to MailGun. SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"CERA Submission Proposal Edited Email may not have been sent to MailGun. SubmissionId: {submissionWithProposalAnswersViewModel.SubmissionId}, Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }


            }
        }

        public void SendAN2020ReminderEmail(string emailAddress)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );
            Submission submission = new Submission();

            //Create email body using a the Submission Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/reminderan2020presenter.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject("Reminder - Confirm Your Virtual Presentation at STFM Annual Conference")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"AN2020 Reminder Presenter Confirm Virtual Presentation email sent to MailGun. Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"AN2020 Reminder Presenter Confirm Virtual Presentation email not have been sent to MailGun. Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"AN2020 Reminder Confirm Virtual Presentation email was not sent to MailGun because of an invalid email address. Email: {emailAddress}.");

            }
        }

        public void SendUploadedMaterialEmail(Submission submission, string submitterEmailAddress, string mainPresenterEmailAddress)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

         

            //Create email body using a the SubmissionBaseEdited Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/virtualmaterialuploaded.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddresses = GetEmailAddresses(submitterEmailAddress, mainPresenterEmailAddress);

            foreach (string emailAddress in emailAddresses)

            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                                 .Subject("Material Successfully Uploaded For Presentation at STFM Conference")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Uploaded material successfully email sent to MailGun. SubmissionId: {submission.SubmissionId}, Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Uploaded material successfully email may not have been sent to MailGun. SubmissionId: {submission.SubmissionId}, Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
        }

        public void SendEmailPresenterUploadMaterial(string emailAddress, Submission submission)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );


           string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/uploadmaterialinstructions_mse2023.cshtml");
           

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject("FINAL REMINDER: STFM Conference - Upload Presentation Materials Action Required")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Presenter upload material email sent to MailGun. Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Presenter upload material email not sent to MailGun. Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"Presenter upload material email was not sent to MailGun because of an invalid email address. Email: {emailAddress}.");

            }

        }

        public void SendWillPresentReminderEmail(string emailAddress, Submission submission)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );


            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/willpresentreminder_mse.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject("STFM Conference Will Present Action Required")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    // Log.Information($"Will present reminder email sent to: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Will present reminder email not sent to MailGun. Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"Will present reminder email was not sent to MailGun because of an invalid email address. Email: {emailAddress}.");

            }
        }

        public void SendIncompleteReminderEmail(string emailAddress, Submission submission)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );


            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/incompletereminder.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject("STFM Conference Complete Your Submission Reminder")
                                 .Body(emailBody, true);

                var result = email.Send();

                

            }
            else
            {

                Log.Information($"Incomplete reminder email was not sent to MailGun because of an invalid email address. Email: {emailAddress}.");

            }
        }

        public void SendIncompleteReminderEmail(List<string> emailAddresses, Submission submission)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );


            string emailBody = _submissionPrintService.RenderViewToString(submission, "/Views/Email/incompletereminder.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            foreach (string emailAddress in emailAddresses)
            {

                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .CC("bphillip@stfm.org")
                    .Subject("STFM Conference Complete Your Submission Reminder")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    //Do nothing - no need to log successful email
                }
                else
                {
                    Log.Information($"Submission " + submission.SubmissionId + " incomplete reminder email may not have been sent to email address: " + emailAddress );

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");

                    }
                }

            }
            
            
        }

        public void SendEmailOnlinePresentation(SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel, int presentationStatusId, List<string> emailAddresses)
        {

            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            string emailBody = "";
            string emailType = "";
            string subject = "";
            if (presentationStatusId == 2)
            {
                emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/willpresentreminder.cshtml");
                emailType = "Agree To Present Online";
                subject = "STFM - Action Required - Agree To Present Online";


            }
            else if (presentationStatusId == 3)
            {
                emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/uploadmaterialinstructions_an2023.cshtml");
                emailType = "Need Online Material";
                subject = "STFM - Action Required - Provide Online Presentation Material";

            } else if (presentationStatusId == 0)
            {
                emailBody = _submissionPrintService.RenderViewToString(submissionWithProposalAnswersViewModel, "/Views/Email/notice_an2022.cshtml");
                emailType = "Present In Person";
                subject = "IMPORTANT: Additional Information Regarding the STFM Annual Conference Virtual Supplement";


            }

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            HashSet<string> emailAddressesToSend = GetEmailAddresses(emailAddresses);

            foreach (string emailAddress in emailAddressesToSend)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                                 .To(emailAddress)
                    .Subject(subject)
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    //Do nothing - no need to log successful email
                }
                else
                {
                    Log.Information($"Submission " + emailType + " email MAY NOT HAVE BEEN sent to MailGun. SubmissionId: " + submissionWithProposalAnswersViewModel.SubmissionId + " Email: " + emailAddress);

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");

                    }
                }

            }
        }

        public void SendProvideRecordedOnDemandPresentationNotificationReceivedEmail(string submitterEmail, string mainPresenterEmail, ProvideRecordedOnDemandPresentationNotificationReceivedViewModel provideRecordedOnDemandPresentationNotificationReceivedViewModel)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );

            //Create email body using a the Submission Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(provideRecordedOnDemandPresentationNotificationReceivedViewModel, "/Views/Email/ProvideRecordedOnDemandPresentationNotificationReceived.cshtml");

            Email.DefaultSender = sender;

            HashSet<string> emailAddresses = GetEmailAddresses(submitterEmail, mainPresenterEmail);

            foreach (var emailAddress in emailAddresses)
            {

                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .Subject("Recorded On-Demand Presentation Notification Received")
                    .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"Provide Recorded On-Demand Presentation Notification Received Email sent to MailGun. SubmissionId: {provideRecordedOnDemandPresentationNotificationReceivedViewModel.SubmissionId}, Submitter Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"Provide Recorded On-Demand Presentation Notification Received Email may not have been sent to MailGun. SubmissionId: {provideRecordedOnDemandPresentationNotificationReceivedViewModel.SubmissionId}, Submitter Email: {emailAddress}.");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }

        }

        public void SendCPQIDisclosureReminder(string emailAddress, int submissionId)
        {
            //use MailGun to send the emails
            var sender = new MailgunSender(
                _configuration["MailGun:MailGunDomain"], // Mailgun Domain
                _configuration["MailGun:MailGunAPIKey"] // Mailgun API Key
            );
            

            //Create email body using a the Submission Model class and a Razor view
            string emailBody = _submissionPrintService.RenderViewToString(submissionId, "/Views/Email/cpqidisclosure.cshtml");

            Email.DefaultSender = sender;

            Email.DefaultRenderer = new RazorRenderer();

            if (validateEmail(emailAddress) == true)
            {


                var email = Email.From(_configuration["MailGun:FromEmailAddress"])
                    .To(emailAddress)
                    .BCC("bphillip@stfm.org")
                    .Subject("STFM - Action Needed: Update Your Disclosure")
                                 .Body(emailBody, true);

                var result = email.Send();

                if (result.Successful)
                {
                    Log.Information($"CPQI Update Disclosure Reminder email sent to MailGun. Email: {emailAddress}.");
                }
                else
                {
                    Log.Information($"CPQI Update Disclosure email may not have been sent to MailGun. Email: {emailAddress}");

                    foreach (string errorMessage in result.ErrorMessages)
                    {

                        Log.Warning($"MailGun Error: {errorMessage}");
                    }
                }

            }
            else
            {

                Log.Information($"CPQI Update Disclosure email was not sent to MailGun because of an invalid email address. Email: {emailAddress}.");

            }
        }
    }
}
