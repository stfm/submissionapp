﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.DTOs
{
    /// <summary>
    /// Stores data related to a submission participant
    /// being added to a submission.  This data is used
    /// in the email sent to the submission's submitter
    /// and main presenter.
    /// </summary>
    public class SubmissionParticipantAdded
    {

        public int SubmissionId { get; set; }

        public string ConferenceTitle { get; set; }

        public string SubmissionCategory { get; set; }

        public String SubmissionAcceptedCategory { get; set; }

        public string SubmissionTitle { get; set; }

        public User ParticipantAdded { get; set; }

        public String SubmitterEmailAddress { get; set; }

        public String MainPresenterEmailAddress { get; set; }

        public ParticipantRoleType ParticipantRoleType { get; set; }


    }
}
