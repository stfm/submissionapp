﻿using System;
using Microsoft.AspNetCore.DataProtection;

namespace ConferenceSubmission.Services
{
	public class EncryptionService : IEncryptionService

    {
        IDataProtector dataProtector;

        public EncryptionService(IDataProtectionProvider provider)
        {

            dataProtector = provider.CreateProtector(GetType().FullName);

        }

        public string DecryptString(string stringToDecrypt)
        {
            return dataProtector.Unprotect(stringToDecrypt);
        }

        public string EncryptString(string stringToEncrypt)
        {
            return dataProtector.Protect(stringToEncrypt);
        }

    }
}
