﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;

namespace ConferenceSubmissionTest
{
    public class FormFieldRepositoryForTesting : IFormFieldRepository
    {
        public FormFieldRepositoryForTesting()
        {
        }

        public void AddSubmissionFormField(SubmissionFormField submissionFormField)
        {
            throw new NotImplementedException();
        }

        public int CreateFormField(FormField formField)
        {
            throw new NotImplementedException();
        }

        public FormField GetFormField(int formFieldId)
        {
            throw new NotImplementedException();
        }

        public string GetFormFieldAnswerForSubmission(int formFieldId, int submissionId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FormField> GetFormFields(int submissionCategoryId, string formFieldRole)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FormField> GetFormFieldsAnswerRequired(int submissionCategoryId, string formFieldRole)
        {
            FormFieldProperty formFieldProperty1 = new FormFieldProperty
            {


                FormFieldPropertyId = 1,
                FormFieldId = 1,
                PropertyName = "label",
                PropertyValue = "Question 1"

            };


            List<FormFieldProperty> formFieldProperties1 = new List<FormFieldProperty>();

            formFieldProperties1.Add(formFieldProperty1);

            FormField formField1 = new FormField
            {

                FormFieldId = 1,
                SubmissionCategoryId = 1000,
                FormFieldDeleted = false,
                FormFieldType = "textarea",
                FormFieldSortOrder = 2,
                FormFieldRole = "proposal",
                FormFieldProperties = formFieldProperties1,
                AnswerRequired = true
            };

            FormFieldProperty formFieldProperty2 = new FormFieldProperty
            {


                FormFieldPropertyId = 2,
                FormFieldId = 2,
                PropertyName = "label",
                PropertyValue = "Question 2"

            };

            List<FormFieldProperty> formFieldProperties2 = new List<FormFieldProperty>();

            formFieldProperties2.Add(formFieldProperty2);

            FormField formField2 = new FormField
            {

                FormFieldId = 2,
                SubmissionCategoryId = 1000,
                FormFieldDeleted = false,
                FormFieldType = "text",
                FormFieldSortOrder = 2,
                FormFieldRole = "proposal",
                FormFieldProperties = formFieldProperties2,
                AnswerRequired = true
            };


            List<FormField> formFields = new List<FormField>();

            formFields.Add(formField1);
            formFields.Add(formField2);

            return formFields;

        }


        public IEnumerable<SubmissionFormField> GetFormFieldsForSubmission(int submissionId)
        {
            FormFieldProperty formFieldProperty1 = new FormFieldProperty
            {


                FormFieldPropertyId = 1,
                FormFieldId = 1,
                PropertyName = "label",
                PropertyValue = "Question 1"
                    
            };


            List<FormFieldProperty> formFieldProperties1 = new List<FormFieldProperty>();

            formFieldProperties1.Add(formFieldProperty1);

            FormField formField1 = new FormField
            {

                FormFieldId = 1,
                SubmissionCategoryId = 1000,
                FormFieldDeleted = false,
                FormFieldType = "textarea",
                FormFieldSortOrder = 2,
                FormFieldRole = "proposal",
                FormFieldProperties = formFieldProperties1
            };


            SubmissionFormField submissionFormField1 = new SubmissionFormField
            {

                FormField = formField1,
                SubmissionFormFieldId = 1,
                SubmissionFormFieldValue = "Answer 1",
                SubmissionId = submissionId,
                FormFieldId = 1

            };

            FormFieldProperty formFieldProperty2 = new FormFieldProperty
            {


                FormFieldPropertyId = 2,
                FormFieldId = 2,
                PropertyName = "label",
                PropertyValue = "Question 2"

            };

            List<FormFieldProperty> formFieldProperties2 = new List<FormFieldProperty>();

            formFieldProperties2.Add(formFieldProperty2);

            FormField formField2 = new FormField
            {
                
                FormFieldId = 2,
                SubmissionCategoryId = 1000,
                FormFieldDeleted = false,
                FormFieldType = "text",
                FormFieldSortOrder = 2,
                FormFieldRole = "proposal",
                FormFieldProperties = formFieldProperties2
            };


            SubmissionFormField submissionFormField2 = new SubmissionFormField
            {
                
                FormField = formField2,
                SubmissionFormFieldId = 2,
                SubmissionFormFieldValue = "Answer 2",
                SubmissionId = submissionId,
                FormFieldId = 2

            };

            List<SubmissionFormField> submissionFormFields = new List<SubmissionFormField>();

            submissionFormFields.Add(submissionFormField1);
            submissionFormFields.Add(submissionFormField2);

            if (submissionId == 2001) {

                submissionFormFields.Remove(submissionFormField2);

            }

            return submissionFormFields;

        }

        public int RemoveAllFormFieldsForSubmission(int submissionId)
        {
            throw new NotImplementedException();
        }

        public void UpdateSubmissionFormField(int submissionFormFieldId, string value)
        {
            throw new NotImplementedException();
        }
    }
}
