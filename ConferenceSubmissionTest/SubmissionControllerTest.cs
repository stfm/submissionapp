﻿using System;
using Xunit;
using Moq;
using ConferenceSubmission.Services;
using ConferenceSubmission.Mappers;
using ConferenceSubmission.Controllers;
using System.Collections.Generic;
using ConferenceSubmission.ViewModels;
using Microsoft.AspNetCore.Mvc;
using ConferenceSubmission.BindingModels;
using System.Security.Claims;
using ConferenceSubmission.Models;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections;
using Microsoft.AspNetCore.Hosting;
using System.Linq;

namespace ConferenceSubmissionTest
{
	public class SubmissionControllerTest
	{

		protected Xunit.Abstractions.ITestOutputHelper Output { get; }

		public SubmissionControllerTest(Xunit.Abstractions.ITestOutputHelper output)
		{

			Output = output;
		}

        [Fact]
        public void StartSubmissionControllerTest()
        {
            Mock<ISubmissionCategoryService> mockSubmissionCategoryService =
                new Mock<ISubmissionCategoryService>();

            SubmissionCategory submissionCategory = new SubmissionCategory
            {

                ConferenceId = 100,
                SubmissionCategoryName = "TestCategory"

            };

            mockSubmissionCategoryService.Setup(x => x.GetSubmissionCategory(It.IsAny<int>())).Returns(submissionCategory);

            Mock<ISubmissionService> mockSubmissionService =
                new Mock<ISubmissionService>() { CallBase = true };

            mockSubmissionService.Setup(s => s.AddSubmission(It.IsAny<Submission>())).Returns(1);

            Submission submission = new Submission
            {

                SubmissionId = 100,
                SubmissionTitle = "Test Submission"

            };

            mockSubmissionService.Setup(s => s.CreateSubmission(It.IsAny<BaseSubmissionBindingModel>(), It.IsAny<List<SubmissionParticipant>>(), It.IsAny<Conference>())).Returns(submission);

            Mock<IParticipantService> mockParticipantService =
                new Mock<IParticipantService>();

            Mock<IConferenceService> mockConferenceService =
                new Mock<IConferenceService>();

            Mock<IDisclosureService> mockDisclosureService =
                new Mock<IDisclosureService>();

            Mock<ISubmissionStatusService> mockSubmissionStatusService =
                new Mock<ISubmissionStatusService>();

            Mock<IFormFieldService> mockFormFieldService =
                new Mock<IFormFieldService>();

            Mock<IEncryptionService> mockEncryptionService =
               new Mock<IEncryptionService>();


            Mock<IUserMapper> mockUserMapper =
                new Mock<IUserMapper>();

            Mock<ISubmissionParticipantService> mockSubmissionParticipantService =
                new Mock<ISubmissionParticipantService>();

            Mock<IHostingEnvironment> mockHostingEnvironment = new Mock<IHostingEnvironment>();

            Mock<ISubmissionPrintService> mockSubmissionPrintService = new Mock<ISubmissionPrintService>();

            Mock<IEmailNotificationService> mockEmailNotificationService = new Mock<IEmailNotificationService>();

            Mock<ISalesforceAPIService> mockSalesforceAPIService = new Mock<ISalesforceAPIService>();

            Mock<IConfiguration> mockConfiguration = new Mock<IConfiguration>();

            Mock<IVirtualMaterialService> mockVirtualService = new Mock<IVirtualMaterialService>();

            Mock<IVirtualConferenenceDataService> mockVirtualConferenceDataService = new Mock<IVirtualConferenenceDataService>();

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, "Bruce Phillips"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim("name", "Bruce Phillips"),
            };
            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            var mockPrincipal = new Mock<IPrincipal>();
            mockPrincipal.Setup(x => x.Identity).Returns(identity);
            mockPrincipal.Setup(x => x.IsInRole(It.IsAny<string>())).Returns(true);

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(m => m.User).Returns(claimsPrincipal);

            User user = new User();
            user.Email = "bphillip@stfm.org";
            user.FirstName = "Bruce";
            user.LastName = "Phillips";
            user.STFMUserId = "X10001";

            mockUserMapper.Setup(x => x.Map(It.IsAny<IEnumerable<Claim>>())).Returns(user);

            Participant participant = new Participant
            {

                ParticipantId = 1,
                StfmUserId = "X10001"

            };

            mockParticipantService.Setup(x => x.GetParticipantByStfmUserId("X10001")).Returns(participant);


            ParticipantRole participantRole = new ParticipantRole
            {

                ParticipantRoleId = 1,
                ParticipantRoleName = ParticipantRoleType.SUBMITTER
            };

            List<ParticipantRole> participantRoles = new List<ParticipantRole>();
            participantRoles.Add(participantRole);

           

            Conference conference = new Conference
            {

                ConferenceId = 100,
                ConferenceShortName = "AN20"

            };

            mockConferenceService.Setup(x => x.GetConference(100)).Returns(conference);

            var submissionController =
                new SubmissionController(
                    mockSubmissionCategoryService.Object,
                    mockSubmissionService.Object, mockParticipantService.Object,
                    mockConferenceService.Object, mockSubmissionStatusService.Object,
                    mockFormFieldService.Object, mockEncryptionService.Object,
                    mockUserMapper.Object,
                    mockSubmissionParticipantService.Object,
                    mockConfiguration.Object,
                    mockDisclosureService.Object,
                    mockHostingEnvironment.Object,
                    mockSubmissionPrintService.Object,
                    mockEmailNotificationService.Object,
                    mockSalesforceAPIService.Object,
                    mockVirtualService.Object,
                    mockVirtualConferenceDataService.Object
                );


            var context = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = new ClaimsPrincipal()
                }
            };

            submissionController.ControllerContext = context;

            IActionResult result = submissionController.StartSubmission(100, 1);

            Assert.IsType<ViewResult>(result);


        }


        [Fact]
		public void CreateSubmissionControllerTest()
		{
			Mock<ISubmissionCategoryService> mockSubmissionCategoryService =
				new Mock<ISubmissionCategoryService>();

			Mock<ISubmissionService> mockSubmissionService =
				new Mock<ISubmissionService>() { CallBase=true};
            
            mockSubmissionService.Setup(s => s.AddSubmission(It.IsAny<Submission>())).Returns(1);

            Submission submission = new Submission
            {

                SubmissionId = 100,
                SubmissionTitle = "Test Submission"

            };

            mockSubmissionService.Setup(s => s.CreateSubmission(It.IsAny<BaseSubmissionBindingModel>(), It.IsAny<List<SubmissionParticipant>>(), It.IsAny<Conference>())).Returns(submission);

			Mock<IParticipantService> mockParticipantService =
				new Mock<IParticipantService>();

			Mock<IConferenceService> mockConferenceService =
				new Mock<IConferenceService>();

            Mock<IDisclosureService> mockDisclosureService =
                new Mock<IDisclosureService>();

			Mock<ISubmissionStatusService> mockSubmissionStatusService =
				new Mock<ISubmissionStatusService>();

			Mock<IFormFieldService> mockFormFieldService =
                new Mock<IFormFieldService>();

            Mock<IEncryptionService> mockEncryptionService =
               new Mock<IEncryptionService>();


			Mock<IUserMapper> mockUserMapper =
				new Mock<IUserMapper>();

            Mock<ISubmissionParticipantService> mockSubmissionParticipantService =
                new Mock<ISubmissionParticipantService>();

            Mock<IHostingEnvironment> mockHostingEnvironment = new Mock<IHostingEnvironment>();

            Mock<ISubmissionPrintService> mockSubmissionPrintService = new Mock<ISubmissionPrintService>();

            Mock<IEmailNotificationService> mockEmailNotificationService = new Mock<IEmailNotificationService>();

            Mock<ISalesforceAPIService> mockSalesforceAPIService = new Mock<ISalesforceAPIService>();

            Mock<IConfiguration> mockConfiguration = new Mock<IConfiguration>();

            Mock<IVirtualMaterialService> mockVirtualService = new Mock<IVirtualMaterialService>();

            Mock<IVirtualConferenenceDataService> mockVirtualConferenceDataService = new Mock<IVirtualConferenenceDataService>();


            BaseSubmissionBindingModel baseSubmissionBindingModel = new BaseSubmissionBindingModel
			{

				ConferenceId = 100,
				SelectedSubmissionCategory = 1,
				SelectedUserRole = "submitter-only",
				SubmissionAbstract = "Test abstract",
				SubmissionTitle = "Test Title",

			};


			var claims = new List<Claim>()
			{
				new Claim(ClaimTypes.Name, "Bruce Phillips"),
				new Claim(ClaimTypes.NameIdentifier, "1"),
				new Claim("name", "Bruce Phillips"),
			};
			var identity = new ClaimsIdentity(claims, "TestAuthType");
			var claimsPrincipal = new ClaimsPrincipal(identity);

			var mockPrincipal = new Mock<IPrincipal>();
			mockPrincipal.Setup(x => x.Identity).Returns(identity);
			mockPrincipal.Setup(x => x.IsInRole(It.IsAny<string>())).Returns(true);

			var mockHttpContext = new Mock<HttpContext>();
			mockHttpContext.Setup(m => m.User).Returns(claimsPrincipal);

			User user = new User();
			user.Email = "bphillip@stfm.org";
			user.FirstName = "Bruce";
			user.LastName = "Phillips";
			user.STFMUserId = "X10001";

			mockUserMapper.Setup(x => x.Map(It.IsAny<IEnumerable<Claim>>())).Returns(user);

			Participant participant = new Participant
			{

				ParticipantId = 1,
				StfmUserId = "X10001"

			};

			mockParticipantService.Setup(x => x.GetParticipantByStfmUserId("X10001")).Returns(participant);


			ParticipantRole participantRole = new ParticipantRole
			{

				ParticipantRoleId = 1,
				ParticipantRoleName = ParticipantRoleType.SUBMITTER
			};

			List<ParticipantRole> participantRoles = new List<ParticipantRole>();
			participantRoles.Add(participantRole);

			mockParticipantService.Setup(x => x.GetParticipantRoles(baseSubmissionBindingModel.SelectedUserRole)).Returns(participantRoles);

			Conference conference = new Conference
			{

				ConferenceId = 100,
				ConferenceShortName = "AN20"

			};

			mockConferenceService.Setup(x => x.GetConference(baseSubmissionBindingModel.ConferenceId)).Returns(conference);

			var submissionController =
				new SubmissionController(
					mockSubmissionCategoryService.Object,
					mockSubmissionService.Object, mockParticipantService.Object,
					mockConferenceService.Object, mockSubmissionStatusService.Object,
                    mockFormFieldService.Object, mockEncryptionService.Object,
					mockUserMapper.Object,
                    mockSubmissionParticipantService.Object,
                    mockConfiguration.Object,
                    mockDisclosureService.Object,
                    mockHostingEnvironment.Object,
                    mockSubmissionPrintService.Object,
                    mockEmailNotificationService.Object,
                    mockSalesforceAPIService.Object,
                    mockVirtualService.Object,
                    mockVirtualConferenceDataService.Object
                );


			var context = new ControllerContext
			{
				HttpContext = new DefaultHttpContext
				{
					User = new ClaimsPrincipal()
				}
			};

            submissionController.ControllerContext = context;

			IActionResult result = submissionController.CreateSubmission(baseSubmissionBindingModel);

			Assert.IsType<RedirectToActionResult>(result);

            var routeValues = (result as RedirectToActionResult).RouteValues;

            Assert.Contains(routeValues.Keys, (k => k == "submissionId"));
            Assert.Contains(routeValues.Values, (v => v.ToString() == "1"));

            Output.WriteLine($"A the controller returned a RedirectToActionResult with route value'submissionId' whose value is {routeValues["submissionId"].ToString()}");


		}


	}
}
