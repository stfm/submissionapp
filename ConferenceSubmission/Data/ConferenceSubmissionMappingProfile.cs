﻿using System;
using AutoMapper;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Data
{
    public class ConferenceSubmissionMappingProfile : Profile
    {
        public ConferenceSubmissionMappingProfile()
        {

            CreateMap<SubmissionCategory, SubmissionCategoryViewModel>();

            CreateMap<Conference, ConferenceViewModel>();
        }
    }
}
