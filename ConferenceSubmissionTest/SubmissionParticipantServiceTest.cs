﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class SubmissionParticipantServiceTest
    {
        [Fact]
        public void UpdatePresenterOrderTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                //arrange
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);
                SubmissionRepository submissionRepository = new SubmissionRepository(context);
                IFormFieldRepository formFieldRepository = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                SubmissionService submissionService = new SubmissionService(submissionRepository, null, formFieldRepository, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                //act
                submissionParticipantService.UpdatePresenterOrder(new UpdatePresenterOrderViewModel
                {
                    SubmissionId = 10001,
                    SubmissionParticipantIds = "2,1",
                });
            }

            using (var context = new AppDbContext(options))
            {
                //assert
                SubmissionRepository submissionRepository = new SubmissionRepository(context);
                IFormFieldRepository formFieldRepository = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                SubmissionService submissionService = new SubmissionService(submissionRepository, null, formFieldRepository, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                var submissionParticipantIdsAfterReorder = submissionService.GetSubmission(10001).ParticipantLink
                    .OrderBy(pl => pl.SortOrder)
                    .Select(pl => pl.SubmissionParticipantId)
                    .ToList();

                Assert.True(submissionParticipantIdsAfterReorder[0] == 2);
                Assert.True(submissionParticipantIdsAfterReorder[1] == 1);
            }
        }

        [Fact]
        public void UpdatePresenterOrder_NewPresenterAddedDuringUpdate_Test()
        {
            //TO DO: Implement this test method after we've created repository/service
            //methods for adding SubmissionParticipants to Submissions.

            //TO DO: This method tests that the UpdatePresenterOrder correctly handles the
            //unlikely scenario when a user is updating presenter order and before he saves
            //the new order, a new SubmissionParticipant is added to the Submission.
        }

        [Fact]
        public void GetInitialSortOrder_ReturnsNegavitveOne_SubmitterOnly_Test()
        {
            //arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            int sortOrder;

            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);
                SubmissionRepository submissionRepository = new SubmissionRepository(context);
                IFormFieldRepository formFieldRepository = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                SubmissionService submissionService = new SubmissionService(submissionRepository, null, formFieldRepository, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);
                var submission = submissionService.GetSubmission(10002);
                var submissionParticipant = submission.ParticipantLink.FirstOrDefault(sp => sp.SubmissionParticipantId == 5);

                //act
                sortOrder = submissionParticipantService.GetInitialSortOrder(submissionParticipant);
            }

            //assert
            Assert.Equal(sortOrder, -1);
        }

        [Fact]
        public void GetInitialSortOrder_ReturnsZero_SubmitterAndLeadPresenter_Test()
        {
            //arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            int sortOrder;

            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);
                SubmissionRepository submissionRepository = new SubmissionRepository(context);
                IFormFieldRepository formFieldRepository = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                SubmissionService submissionService = new SubmissionService(submissionRepository, null, formFieldRepository, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);
                var submission = submissionService.GetSubmission(10003);
                var submissionParticipant = submission.ParticipantLink.FirstOrDefault(sp => sp.SubmissionParticipantId == 6);

                //act
                sortOrder = submissionParticipantService.GetInitialSortOrder(submissionParticipant);
            }

            //assert
            Assert.Equal(0, sortOrder);
        }

        [Fact]
        public void GetInitialSortOrder_ReturnsZero_SubmitterAndCoPresenter_Test()
        {
            //arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            int sortOrder;

            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);
                SubmissionRepository submissionRepository = new SubmissionRepository(context);
                IFormFieldRepository formFieldRepository = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                SubmissionService submissionService = new SubmissionService(submissionRepository, null, formFieldRepository, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);
                var submission = submissionService.GetSubmission(10004);
                var submissionParticipant = submission.ParticipantLink.FirstOrDefault(sp => sp.SubmissionParticipantId == 9);

                //act
                sortOrder = submissionParticipantService.GetInitialSortOrder(submissionParticipant);
            }

            //assert
            Assert.Equal(0, sortOrder);
        }

        [Fact]
        public void FilterSubmissionParticipantsExcludeSubmitterOnly_Test()
        {
            //arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            var filteredSubmissions = new List<SubmissionParticipant>();
            var unFilteredSubmissions = new List<SubmissionParticipant>();
            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);
                SubmissionRepository submissionRepository = new SubmissionRepository(context);
                IFormFieldRepository formFieldRepository = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                SubmissionService submissionService = new SubmissionService(submissionRepository, null, formFieldRepository, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);
                var submission = submissionService.GetSubmission(10002);

                //act
                unFilteredSubmissions = submission.ParticipantLink;
                filteredSubmissions = submissionParticipantService.FilterSubmissionParticipantsExcludeSubmitterOnly(unFilteredSubmissions);
            }

            //assert
            Assert.True(unFilteredSubmissions.Count(s => s.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.SUBMITTER)) == 1);
            Assert.True(filteredSubmissions.Count(s => s.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.SUBMITTER)) == 0);
        }

        [Fact]
        public void AddPresenter()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            int newlyCreatedSubmissionParticipantId;
            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);

                //Act
                newlyCreatedSubmissionParticipantId = submissionParticipantService.AddSubissionParticipant(4003, ParticipantRoleType.PRESENTER, 10001);
            }

            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);

                //Assert
                var submissionParticipantsAfterAdd = submissionParticipantService.GetSubmissionParticipants(10001);
                Assert.True(submissionParticipantsAfterAdd.Any(s => s.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.PRESENTER) && s.SubmissionParticipantId == newlyCreatedSubmissionParticipantId));
            }
        }

        [Fact]
        public void AddLeadPresenter_PreviousLeadPresenterDemotedToPresenter()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            int previousLeadPresenterId;
            int newlyCreatedSubmissionParticipantId;
            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);

                previousLeadPresenterId = submissionParticipantService
                    .GetSubmissionParticipants(10001)
                    .FirstOrDefault(s => s.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER))
                    .SubmissionParticipantId;

                //Act
                newlyCreatedSubmissionParticipantId = submissionParticipantService.AddSubissionParticipant(4003, ParticipantRoleType.LEAD_PRESENTER, 10001);

            }
            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);

                //Assert
                var submissionParticipantsAfterAdd = submissionParticipantService.GetSubmissionParticipants(10001);

                var newLeadPresenterAfterAdd = submissionParticipantsAfterAdd
                    .FirstOrDefault(s => s.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER));

                //The lead presenter matches the newly created presenter
                Assert.True(newLeadPresenterAfterAdd.SubmissionParticipantId == newlyCreatedSubmissionParticipantId);

                //The previous lead presenter now has a presenter role
                Assert.Contains
                    (
                        submissionParticipantsAfterAdd
                            .FirstOrDefault(s => s.SubmissionParticipantId == previousLeadPresenterId)
                            .SubmissionParticipantToParticipantRolesLink, 
                            (l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.PRESENTER)
                    );
                
                //The previous lead presenter no longer has a lead presenter role
                Assert.DoesNotContain
                    (
                        submissionParticipantsAfterAdd
                            .FirstOrDefault(s => s.SubmissionParticipantId == previousLeadPresenterId)
                            .SubmissionParticipantToParticipantRolesLink, 
                            (l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER)
                    );
            }

        }

        [Fact]
        public void GetSubmissionSubmitter()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            string expectedSubmitterStfmUserId = "3003";
            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);

                //Act
                string actualSubmitterStfmUserId = submissionParticipantService.GetPerson(10002, ParticipantRoleType.SUBMITTER);

                Assert.Equal(expectedSubmitterStfmUserId, actualSubmitterStfmUserId);
            }


        }

        [Fact]
        public void GetSubmissionMainPresenter()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            string expectedMainPresenterStfmUserId = "3001";
            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);

                //Act
                string actualMainPresenterStfmUserId = submissionParticipantService.GetPerson(10002, ParticipantRoleType.LEAD_PRESENTER);

                Assert.Equal(expectedMainPresenterStfmUserId, actualMainPresenterStfmUserId);
            }


        }

        [Fact]
        public void GetSubmissionMainPresenterNull()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);
                ParticipantRepository participantRepository = new ParticipantRepository(context);
                SubmissionParticipantService submissionParticipantService = new SubmissionParticipantService(submissionParticipantRepository, participantRepository, null, null);

                //Act
                string actualSubmitterStfmUserId = submissionParticipantService.GetPerson(10005, ParticipantRoleType.LEAD_PRESENTER);

                Assert.Null(actualSubmitterStfmUserId);
            }


        }

        [Fact]
        public void AssignLeadPresenterRole_ThereIsAlreadyALeadPresenterAssigned_VerifyCallIsMadeToRemoveTheCurrentLeadPresenter()
        {
            //Arrange
            var leadPresenterParticipantRole = new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER };
            var submitterParticipantRole = new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.SUBMITTER };
            var presenterParticipantRole = new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.PRESENTER };

            var leadPresenterSubmissionParticipantToParticipantRole = new SubmissionParticipantToParticipantRole
            { 
                ParticipantRole = leadPresenterParticipantRole,
                ParticipantRoleId = 1,
                SubmissionParticipantToParticipantRoleId = 1
            };
            var submitterSubmissionParticipantToParticipantRole = new SubmissionParticipantToParticipantRole
            {
                ParticipantRole = submitterParticipantRole,
                ParticipantRoleId = 2,
                SubmissionParticipantToParticipantRoleId = 2
            };
            var leadPresenterSubmissionParticipant = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                SubmissionId = 1,
                ParticipantId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>()
                    { leadPresenterSubmissionParticipantToParticipantRole }
            };
            var submitterSubmissionParticipant = new SubmissionParticipant
            {
                SubmissionParticipantId = 2,
                SubmissionId = 1,
                ParticipantId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>()
                    { submitterSubmissionParticipantToParticipantRole }
            };
            List<SubmissionParticipant> submissionParticipants = new List<SubmissionParticipant> { leadPresenterSubmissionParticipant, submitterSubmissionParticipant };

            var mockSubmissionParticipantRepository = new Mock<ISubmissionParticipantRepository>();
            mockSubmissionParticipantRepository.Setup(x => x.GetSubmissionParticipants(It.IsAny<int>())).Returns(submissionParticipants);
            var removeSubmissionParticipantToParticipantRoleCallCount = 0;
            var addSubmissionParticipantToParticipantRoleCallCount = 0;
            mockSubmissionParticipantRepository.Setup(x => x.RemoveSubmissionParticipantToParticipantRole(It.IsAny<int>())).Callback(() => { removeSubmissionParticipantToParticipantRoleCallCount++; });
            mockSubmissionParticipantRepository.Setup(x => x.AddSubmissionParticipantToParticipantRole(It.IsAny<int>(), It.IsAny<int>())).Callback(() => { addSubmissionParticipantToParticipantRoleCallCount++; });

            var mockParticipantRepository = new Mock<IParticipantRepository>();
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.LEAD_PRESENTER)).Returns(leadPresenterParticipantRole);
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.SUBMITTER)).Returns(submitterParticipantRole);
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.PRESENTER)).Returns(presenterParticipantRole);

            var submissionParticipantService = new SubmissionParticipantService(mockSubmissionParticipantRepository.Object, mockParticipantRepository.Object, null, null);

            //Act
            submissionParticipantService.AssignLeadPresenterRole(1, 2);

            //Assert
            Assert.Equal(1, removeSubmissionParticipantToParticipantRoleCallCount);
            Assert.Equal(2, addSubmissionParticipantToParticipantRoleCallCount);
        }

        [Fact]
        public void AssignLeadPresenterRole_ParticipantIsAlreadyPresenterRole_VerifyCallIsMadeToRemoveThePresenterRole()
        {
            //Arrange
            var leadPresenterParticipantRole = new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER };
            var submitterParticipantRole = new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.SUBMITTER };
            var presenterParticipantRole = new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.PRESENTER };

            var presenterSubmissionParticipantToParticipantRole = new SubmissionParticipantToParticipantRole
            {
                ParticipantRole = presenterParticipantRole,
                ParticipantRoleId = 3,
                SubmissionParticipantToParticipantRoleId = 1
            };
            var submitterSubmissionParticipantToParticipantRole = new SubmissionParticipantToParticipantRole
            {
                ParticipantRole = submitterParticipantRole,
                ParticipantRoleId = 2,
                SubmissionParticipantToParticipantRoleId = 2
            };
            var presenterSubmissionParticipant = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                SubmissionId = 1,
                ParticipantId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>()
                {
                    presenterSubmissionParticipantToParticipantRole,
                    submitterSubmissionParticipantToParticipantRole
                }
            };
            List<SubmissionParticipant> submissionParticipants = new List<SubmissionParticipant>
            {
                presenterSubmissionParticipant
            };

            var mockSubmissionParticipantRepository = new Mock<ISubmissionParticipantRepository>();
            mockSubmissionParticipantRepository.Setup(x => x.GetSubmissionParticipants(It.IsAny<int>())).Returns(submissionParticipants);
            var removeSubmissionParticipantToParticipantRoleCallCount = 0;
            var addSubmissionParticipantToParticipantRoleCallCount = 0;
            mockSubmissionParticipantRepository.Setup(x => x.RemoveSubmissionParticipantToParticipantRole(It.IsAny<int>())).Callback(() => { removeSubmissionParticipantToParticipantRoleCallCount++; });
            mockSubmissionParticipantRepository.Setup(x => x.AddSubmissionParticipantToParticipantRole(It.IsAny<int>(), It.IsAny<int>())).Callback(() => { addSubmissionParticipantToParticipantRoleCallCount++; });

            var mockParticipantRepository = new Mock<IParticipantRepository>();
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.LEAD_PRESENTER)).Returns(leadPresenterParticipantRole);
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.SUBMITTER)).Returns(submitterParticipantRole);
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.PRESENTER)).Returns(presenterParticipantRole);

            var submissionParticipantService = new SubmissionParticipantService(mockSubmissionParticipantRepository.Object, mockParticipantRepository.Object, null, null);

            //Act
            submissionParticipantService.AssignLeadPresenterRole(1, 1);

            //Assert
            Assert.Equal(1, removeSubmissionParticipantToParticipantRoleCallCount);
            Assert.Equal(1, addSubmissionParticipantToParticipantRoleCallCount);
        }

        [Fact]
        public void AssignLeadPresenterRole_NoParticipantIsCurrentlyAssignedLeadPresenter_VerifyOnlyOneCallIsMadeToAddTheLeadPresenterRole()
        {
            //Arrange
            var leadPresenterParticipantRole = new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER };
            var submitterParticipantRole = new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.SUBMITTER };
            var presenterParticipantRole = new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.PRESENTER };

            var submitterSubmissionParticipantToParticipantRole = new SubmissionParticipantToParticipantRole
            {
                ParticipantRole = submitterParticipantRole,
                ParticipantRoleId = 2,
                SubmissionParticipantToParticipantRoleId = 2
            };
            var presenterSubmissionParticipant = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                SubmissionId = 1,
                ParticipantId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>()
                {
                    submitterSubmissionParticipantToParticipantRole
                }
            };
            List<SubmissionParticipant> submissionParticipants = new List<SubmissionParticipant>
            {
                presenterSubmissionParticipant
            };

            var mockSubmissionParticipantRepository = new Mock<ISubmissionParticipantRepository>();
            mockSubmissionParticipantRepository.Setup(x => x.GetSubmissionParticipants(It.IsAny<int>())).Returns(submissionParticipants);
            var removeSubmissionParticipantToParticipantRoleCallCount = 0;
            var addSubmissionParticipantToParticipantRoleCallCount = 0;
            mockSubmissionParticipantRepository.Setup(x => x.RemoveSubmissionParticipantToParticipantRole(It.IsAny<int>())).Callback(() => { removeSubmissionParticipantToParticipantRoleCallCount++; });
            mockSubmissionParticipantRepository.Setup(x => x.AddSubmissionParticipantToParticipantRole(It.IsAny<int>(), It.IsAny<int>())).Callback(() => { addSubmissionParticipantToParticipantRoleCallCount++; });

            var mockParticipantRepository = new Mock<IParticipantRepository>();
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.LEAD_PRESENTER)).Returns(leadPresenterParticipantRole);
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.SUBMITTER)).Returns(submitterParticipantRole);
            mockParticipantRepository.Setup(x => x.GetParticipantRole(ParticipantRoleType.PRESENTER)).Returns(presenterParticipantRole);

            var submissionParticipantService = new SubmissionParticipantService(mockSubmissionParticipantRepository.Object, mockParticipantRepository.Object, null, null);

            //Act
            submissionParticipantService.AssignLeadPresenterRole(1, 1);

            //Assert
            Assert.Equal(0, removeSubmissionParticipantToParticipantRoleCallCount);
            Assert.Equal(1, addSubmissionParticipantToParticipantRoleCallCount);
        }

        [Fact]
        public void GetUpdatePresenterOrderViewModel_SubmissionWithPresenterAndLeadPresenterAndAuthor_VerifyResults()
        {
            //Arrange
            var users = new List<User>
            {
                new User
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "JohnDoe@email.com",
                    STFMUserId = "0009AHLD3fdsjj",
                    UserName = "JohnDoe"
                },
                new User
                {
                    FirstName = "Frank",
                    LastName = "Grimes",
                    Credentials = "MD",
                    Email = "FrankGrimes@email.com",
                    STFMUserId = "0009OD3fdsjj",
                    UserName = "FrankGrimes"
                },
                new User
                {
                    FirstName = "Lillian",
                    LastName = "Rearden",
                    Credentials = "MD",
                    Email = "LillianRearden@email.com",
                    STFMUserId = "0F09fk9fdsjj",
                    UserName = "LillianRearden"
                },
            };
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesforceAPIService.Setup(s => s.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).
                ReturnsAsync(() => users);

            var submission = new Submission
            {
                SubmissionId = 1,
                Conference = new Conference { ConferenceLongName = "2034 Annual Spring Conference" },
                SubmissionCategory = new SubmissionCategory { SubmissionCategoryName = "Lecture 30 Minutes" },
                AcceptedCategory = new SubmissionCategory { SubmissionCategoryName = "Lecture 60 Minutes" },
                SubmissionTitle = "The Cure for Heart Disease",
                ParticipantLink = new List<SubmissionParticipant>
                {
                    new SubmissionParticipant
                    {
                        Participant = new Participant{StfmUserId = "0009AHLD3fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER}
                            }
                        }
                    },
                    new SubmissionParticipant
                    {
                        Participant = new Participant{StfmUserId = "0009OD3fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER}
                            }
                        }
                    },
                    new SubmissionParticipant
                    {
                    Participant = new Participant{StfmUserId = "0F09fk9fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 4, ParticipantRoleName = ParticipantRoleType.AUTHOR}
                            }
                        }
                    }
                }
            };
            var submissionParticipantService = new SubmissionParticipantService(null, null, mockSalesforceAPIService.Object, null);

            //Act
            var result = submissionParticipantService.GetUpdatePresenterOrderViewModel(submission);

            //Assert
            Assert.True(result.ConferenceName == "2034 Annual Spring Conference");
            Assert.True(result.SubmissionTitle == "The Cure for Heart Disease");
            Assert.True(result.SubmittedCategory == "Lecture 30 Minutes");
            Assert.True(result.AcceptedCategory == "Lecture 60 Minutes");
            Assert.Contains(result.Presenters, p => p.FirstName == "John" && p.LastName == "Doe");
            Assert.Contains(result.Presenters, p => p.FirstName == "Lillian" && p.LastName == "Rearden");
            Assert.Contains(result.Presenters, p => p.FirstName == "Frank" && p.LastName == "Grimes");
        }

        [Fact]
        public void GetUpdatePresenterOrderViewModel_SubmissionWithPresenterWhoIsSubmitterAndLeadPresenter_VerifyResults()
        {
            //Arrange
            var users = new List<User>
            {
                new User
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "JohnDoe@email.com",
                    STFMUserId = "0009AHLD3fdsjj",
                    UserName = "JohnDoe"
                },
                new User
                {
                    FirstName = "Frank",
                    LastName = "Grimes",
                    Credentials = "MD",
                    Email = "FrankGrimes@email.com",
                    STFMUserId = "0009OD3fdsjj",
                    UserName = "FrankGrimes"
                },
                new User
                {
                    FirstName = "Lillian",
                    LastName = "Rearden",
                    Credentials = "MD",
                    Email = "LillianRearden@email.com",
                    STFMUserId = "0F09fk9fdsjj",
                    UserName = "LillianRearden"
                },
            };
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesforceAPIService.Setup(s => s.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).
                ReturnsAsync(() => users);

            var submission = new Submission
            {
                SubmissionId = 1,
                Conference = new Conference { ConferenceLongName = "2034 Annual Spring Conference" },
                SubmissionCategory = new SubmissionCategory { SubmissionCategoryName = "Lecture 30 Minutes" },
                AcceptedCategory = new SubmissionCategory { SubmissionCategoryName = "Lecture 60 Minutes" },
                SubmissionTitle = "The Cure for Heart Disease",
                ParticipantLink = new List<SubmissionParticipant>
                {
                    new SubmissionParticipant
                    {
                        Participant = new Participant{StfmUserId = "0009AHLD3fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER}
                            }
                        }
                    },
                    new SubmissionParticipant
                    {
                        Participant = new Participant{StfmUserId = "0009OD3fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER}
                            }
                        }
                    },
                    new SubmissionParticipant
                    {
                    Participant = new Participant{StfmUserId = "0F09fk9fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER}
                            },
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER}
                            }
                        }
                    }
                }
            };
            var submissionParticipantService = new SubmissionParticipantService(null, null, mockSalesforceAPIService.Object, null);

            //Act
            var result = submissionParticipantService.GetUpdatePresenterOrderViewModel(submission);

            //Assert
            Assert.True(result.ConferenceName == "2034 Annual Spring Conference");
            Assert.True(result.SubmissionTitle == "The Cure for Heart Disease");
            Assert.True(result.SubmittedCategory == "Lecture 30 Minutes");
            Assert.True(result.AcceptedCategory == "Lecture 60 Minutes");
            Assert.Contains(result.Presenters, p => p.FirstName == "John" && p.LastName == "Doe");
            Assert.Contains(result.Presenters, p => p.FirstName == "Lillian" && p.LastName == "Rearden");
            Assert.Contains(result.Presenters, p => p.FirstName == "Frank" && p.LastName == "Grimes");
        }

        [Fact]
        public void GetUpdatePresenterOrderViewModel_SubmissionWithSubmitterOnly_VerifyResults()
        {
            //Arrange
            var users = new List<User>
            {
                new User
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "JohnDoe@email.com",
                    STFMUserId = "0009AHLD3fdsjj",
                    UserName = "JohnDoe"
                },
                new User
                {
                    FirstName = "Frank",
                    LastName = "Grimes",
                    Credentials = "MD",
                    Email = "FrankGrimes@email.com",
                    STFMUserId = "0009OD3fdsjj",
                    UserName = "FrankGrimes"
                },
                new User
                {
                    FirstName = "Lillian",
                    LastName = "Rearden",
                    Credentials = "MD",
                    Email = "LillianRearden@email.com",
                    STFMUserId = "0F09fk9fdsjj",
                    UserName = "LillianRearden"
                },
            };
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesforceAPIService.Setup(s => s.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).
                ReturnsAsync(() => users);

            var submission = new Submission
            {
                SubmissionId = 1,
                Conference = new Conference { ConferenceLongName = "2034 Annual Spring Conference" },
                SubmissionCategory = new SubmissionCategory { SubmissionCategoryName = "Lecture 30 Minutes" },
                AcceptedCategory = new SubmissionCategory { SubmissionCategoryName = "Lecture 60 Minutes" },
                SubmissionTitle = "The Cure for Heart Disease",
                ParticipantLink = new List<SubmissionParticipant>
                {
                    new SubmissionParticipant
                    {
                        Participant = new Participant{StfmUserId = "0009AHLD3fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER}
                            }
                        }
                    },
                    new SubmissionParticipant
                    {
                        Participant = new Participant{StfmUserId = "0009OD3fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER}
                            }
                        }
                    },
                    new SubmissionParticipant
                    {
                    Participant = new Participant{StfmUserId = "0F09fk9fdsjj"},
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                        {
                            new SubmissionParticipantToParticipantRole
                            {
                                ParticipantRole = new ParticipantRole{ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER}
                            }
                        }
                    }
                }
            };
            var submissionParticipantService = new SubmissionParticipantService(null, null, mockSalesforceAPIService.Object, null);

            //Act
            var result = submissionParticipantService.GetUpdatePresenterOrderViewModel(submission);

            //Assert
            Assert.True(result.ConferenceName == "2034 Annual Spring Conference");
            Assert.True(result.SubmissionTitle == "The Cure for Heart Disease");
            Assert.True(result.SubmittedCategory == "Lecture 30 Minutes");
            Assert.True(result.AcceptedCategory == "Lecture 60 Minutes");
            Assert.DoesNotContain(result.Presenters, p => p.FirstName == "John" && p.LastName == "Doe");
            Assert.Contains(result.Presenters, p => p.FirstName == "Lillian" && p.LastName == "Rearden");
            Assert.Contains(result.Presenters, p => p.FirstName == "Frank" && p.LastName == "Grimes");
        }

        [Fact] 
        void GetSubmissionsWithParticipantsAddedOrRemoved_VerifyResults()
        {
            //arrange
            var users = new List<User>
            {
                new User
                {
                    STFMUserId = "ABC123",
                    Credentials = "MD",
                    FirstName = "John",
                    LastName = "Doe"
                },
                new User
                {
                    STFMUserId = "XYZ123",
                    Credentials = "MD",
                    FirstName = "Jane",
                    LastName = "Doe"
                },
            };

            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesforceAPIService.Setup(s => s.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).
                ReturnsAsync(() => users);

            var submissionsWithParticipantsAddedOrRemoved = new List<SubmissionWithParticipantsAddedOrRemoved>
            {
                new SubmissionWithParticipantsAddedOrRemoved
                {
                    SubmissionId = 1,
                    SubmissionTitle = "Advancing Medicine",
                    SessionCode = "L01",
                    ParticipantsAddedOrRemoved = new List<ParticipantAddedOrRemoved>
                    {
                        new ParticipantAddedOrRemoved
                        {
                            STFMUserId = "ABC123",
                            ParticipantName = "",
                            DateAddedOrRemoved = DateTime.Now,
                            AddedOrRemoved = AddedOrRemoved.ADDED
                        }
                    }
                },
                new SubmissionWithParticipantsAddedOrRemoved
                {
                    SubmissionId = 2,
                    SubmissionTitle = "A Cure for the Common Cold",
                    SessionCode = "PB89",
                    ParticipantsAddedOrRemoved = new List<ParticipantAddedOrRemoved>
                    {
                        new ParticipantAddedOrRemoved
                        {
                            STFMUserId = "XYZ123",
                            ParticipantName = "",
                            DateAddedOrRemoved = DateTime.Now,
                            AddedOrRemoved = AddedOrRemoved.REMOVED
                        }
                    }
                }
            };

            var mockSubmissionParticipantRepository = new Mock<ISubmissionParticipantRepository>();
            mockSubmissionParticipantRepository.Setup(s => s.GetSubmissionsWithParticipantsAddedOrRemoved(It.IsAny<int>(), It.IsAny<DateTime>())).
                 Returns(() => submissionsWithParticipantsAddedOrRemoved);

            var submissionParticipantService = new SubmissionParticipantService(mockSubmissionParticipantRepository.Object, null, mockSalesforceAPIService.Object, null);

            //act
            var results = submissionParticipantService.GetSubmissionsWithParticipantsAddedOrRemoved(1, DateTime.Now);

            //assert
            Assert.Equal(2, results.Count);
            Assert.Contains(results, r => r.SubmissionTitle == "Advancing Medicine" && r.ParticipantsAddedOrRemoved.Count == 1 && r.ParticipantsAddedOrRemoved.First().ParticipantName.Contains("John") && r.ParticipantsAddedOrRemoved.First().AddedOrRemoved == AddedOrRemoved.ADDED);
            Assert.Contains(results, r => r.SubmissionTitle == "A Cure for the Common Cold" && r.ParticipantsAddedOrRemoved.Count == 1 && r.ParticipantsAddedOrRemoved.First().ParticipantName.Contains("Jane") && r.ParticipantsAddedOrRemoved.First().AddedOrRemoved == AddedOrRemoved.REMOVED);
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }

                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {
                        AcceptedCategoryId = 1000,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract 2",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 11, 2018"),
                        SubmissionId = 10002,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 11, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title 2",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract 3",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Jan 11, 2018"),
                        SubmissionId = 10003,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Jan 11, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title 3",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract 4",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Jan 21, 2018"),
                        SubmissionId = 10004,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Jan 21, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title 4",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract 5",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Jan 21, 2018"),
                        SubmissionId = 10005,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Jan 21, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title 5",

                    }

                );

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }

                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(

                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10001, SortOrder = 1 },

                    new SubmissionParticipant { SubmissionParticipantId = 3, ParticipantId = 4001, SubmissionId = 10002, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 4, ParticipantId = 4002, SubmissionId = 10002, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 5, ParticipantId = 4003, SubmissionId = 10002, SortOrder = 0 },

                    new SubmissionParticipant { SubmissionParticipantId = 6, ParticipantId = 4001, SubmissionId = 10003, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 7, ParticipantId = 4002, SubmissionId = 10003, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 8, ParticipantId = 4003, SubmissionId = 10003, SortOrder = 0 },

                    new SubmissionParticipant { SubmissionParticipantId = 9, ParticipantId = 4001, SubmissionId = 10004, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 10, ParticipantId = 4002, SubmissionId = 10004, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 11, ParticipantId = 4003, SubmissionId = 10004, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 12, ParticipantId = 4001, SubmissionId = 10005, SortOrder = 0 }
                );

                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 2 },

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 3 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 4 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 5 },

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 6 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 6 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 7 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 8 },

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 9 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 9 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 10 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 11 },

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 12 }

                );

                context.SaveChanges();

            }

            return options;
        }
    }
}
