﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents a conference where presentations will be presented.
    /// </summary>
    public class Conference
    {
        public Conference()
        {

            Submissions = new List<Submission>();

            SubmissionCategories = new List<SubmissionCategory>();
        }

        public int ConferenceId { get; set;  }

        public int ConferenceTypeId { get; set; }

        [StringLength(20)]
        [Required]
        public string ConferenceShortName { get; set; }

        [StringLength(250)]
        [Required]
        public String ConferenceLongName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime ConferenceStartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime ConferenceEndDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ConferenceCFPEndDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ConferenceSubmissionReviewEndDate { get; set; }

        public string SubmissionPaymentRequirementText { get; set; }

        public bool ConferenceInactive { get; set; }

        public bool ConferenceDeleted { get; set; }

        public ConferenceType ConferenceType { get; set; }

        [StringLength(250)]
        public string CategoryDetailUrl { get; set; }

        public bool ConferenceVirtual { get; set; }

        public List<Submission> Submissions { get; set; }

        public List<SubmissionCategory> SubmissionCategories { get; set; }

        public bool ConferenceHybrid { get; set; }



    }
}
