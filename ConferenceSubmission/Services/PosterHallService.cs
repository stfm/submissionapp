﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.Data;
using ConferenceSubmission.DTOs.PosterHall;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public class PosterHallService : IPosterHallService
    {
        private readonly ISubmissionRepository _submissionRepository;
        private readonly ISalesforceAPIService _salesforceAPIService;

        public PosterHallService(ISubmissionRepository submissionRepository, ISalesforceAPIService salesforceAPIService)
        {
            _submissionRepository = submissionRepository;
            _salesforceAPIService = salesforceAPIService;
        }

        public List<PosterWithPresenters> GetSubmissionsForPosterHallCategory(int submissionCategoryId)
        {
            var posterDTOs = _submissionRepository.GetSubmissionsForPosterHallCategory(submissionCategoryId);

            var stfmUserIds = posterDTOs.Select(x => x.STFMUserId).Distinct();

            var users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result.ToList();

            var results = posterDTOs
                .GroupBy(x => x.SubmissionId)
                .Where(x => x.Any(y => y.MaterialType == "poster"))
                .Select(x => new PosterWithPresenters
                {
                    SubmissionId = x.FirstOrDefault()?.SubmissionId ?? 0,
                    Title = x.FirstOrDefault()?.SubmissionTitle ?? "",
                    Abstract = x.FirstOrDefault()?.SubmissionAbstract ?? "",
                    SessionCode = x.FirstOrDefault()?.SessionCode ?? "",
                    SessionTrack = x.FirstOrDefault()?.SessionTrack ?? "",
                    Presenters = getPresentersForPoster(users, x.Select(y => y.STFMUserId).Distinct()),
                    PosterImageFileName = x.Where(y => y.MaterialType == "poster").FirstOrDefault()?.MaterialValue ?? "",
                    YouTubeEmbedLink = x.Where(y => y.MaterialType == "link").FirstOrDefault()?.MaterialValue ?? "",
                    HandoutFileName = x.Where(y => y.MaterialType == "handout").FirstOrDefault()?.MaterialValue ?? "",
                    PresenterToNotify = getPresenterToNotify(users, x.Select(y => (y.STFMUserId, y.ParticipantRoleType)))
                }).ToList();

            return results;
        }

        #region Helper Methods

        private string getPresentersForPoster(List<User> users, IEnumerable<string> stfmUserIds)
        {
            var presenters = "";

            foreach (var stfmUserId in stfmUserIds)
            {
                presenters += $"{users.FirstOrDefault(x => x.STFMUserId == stfmUserId)?.GetFullNamePretty() ?? ""}; ";
            }

            return presenters.TrimEnd(new char[] { ' ', ';' });
        }

        private PresenterToNotify getPresenterToNotify(List<User> users, IEnumerable<(string stfmUserId, ParticipantRoleType role)> presenters)
        {
            var presenterToNotifySTFMUserId = presenters.FirstOrDefault(x => x.role == ParticipantRoleType.LEAD_PRESENTER).stfmUserId;
            if (string.IsNullOrWhiteSpace(presenterToNotifySTFMUserId))
            {
                presenterToNotifySTFMUserId = presenters.FirstOrDefault(x => x.role == ParticipantRoleType.PRESENTER).stfmUserId;
            }
            if (string.IsNullOrWhiteSpace(presenterToNotifySTFMUserId))
            {
                presenterToNotifySTFMUserId = presenters.FirstOrDefault(x => x.role == ParticipantRoleType.SUBMITTER).stfmUserId;
            }

            var user = users.FirstOrDefault(x => x.STFMUserId == presenterToNotifySTFMUserId);

            return new PresenterToNotify
            {
                FullName = user?.GetFullNamePretty() ?? string.Empty,
                Email = user?.Email ?? string.Empty
            };
        }

        #endregion
    }
}
