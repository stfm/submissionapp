﻿using System;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using ConferenceSubmission.Services;
using Microsoft.AspNetCore.Cors;
using System.Linq;
using System.Collections.Generic;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Controllers
{
    [Route("api/conferencesessions")]
    public class SessionApiController : Controller
    {

        private readonly IConferenceSessionService _conferenceSessionService;


        public SessionApiController(IConferenceSessionService conferenceSessionService)
        {

            _conferenceSessionService = conferenceSessionService;

        }

        /// <summary>
        /// Get the accepted submissions that have been scheduled as sessions
        /// for the provided conference id.
        /// </summary>
        /// <param name="id">conference id</param>
        /// <returns>Conference object</returns>
        [EnableCors]
        [HttpGet("conferences/{id:int}")]
        public IActionResult GetConference(int id)
        {

            /*
             * conference ID values provided to this method are either 1000 or 5000 more then the actual id value
             * so that the STFM website can use both conference ID values for old JLS conference sessions
             * and new STFM conference sessions.  
             * If the conference id values is > 1000 but less then 2000 that means the method needs to get
             * the in-person conference sessions.
             * If the conference id value is 5000 or higher that means method
             * needs to get the virtual conference supplement sessions.
             */
            Log.Information($"A request was made for conference sessions using id value of {id}");

            int stfmConferenceId = 0;

            ConferenceDTO conference = null;

            if (id > 1000 && id < 2000)
            {

                stfmConferenceId = id - 1000;

                conference = _conferenceSessionService.GetConferenceSessions(stfmConferenceId);

            }

            if (id > 5000 )
            {

                stfmConferenceId = id - 5000;

                conference = _conferenceSessionService.GetVirtualConferenceSessions(stfmConferenceId);

                conference.Name = "Virtual Conference Supplement";

            }

            if (conference == null)
            {
                Log.Warning($"A request was made for a conference that could not be found. The Id was {stfmConferenceId}");
                return NotFound();
            }

            if (conference.Sessions.Count == 0)
            {
                Log.Warning($"A request was made for a conference that has no accepted sessions {stfmConferenceId}");
                return NotFound();

            }
            Log.Information($"A request was received for the conference with an Id of {stfmConferenceId}. The conference was found and returned.");
            return Ok(conference);
        }

        /// <summary>
        /// Get the collection of sessions identified by the
        /// provided comma delimited list of submission IDs.
        /// </summary>
        /// <param name="submissionIds"></param>
        /// <returns></returns>
        [EnableCors]
        [HttpGet("conferences/sessions/{submissionIds}")]
        public IActionResult GetSessions(string submissionIds)
        {
            if (!string.IsNullOrWhiteSpace(submissionIds))
            {
                var ids = new List<int>();
                try
                {
                    ids = submissionIds.Split(',').Select(i => Int32.Parse(i)).ToList();

                    Log.Information("Submission IDs provided: " + string.Join(",", ids));

                    var sessions = _conferenceSessionService.GetConferenceSessions(ids);

                    return Ok(sessions);

                }
                catch (Exception ex)
                {
                    Log.Error($"A request was made to get Sessions by submission IDs, the application was unable to parse the parameter into a collection of integers. {ex.Message}");
                    return BadRequest();
                }



            } else
            {

                return NotFound();

            }

            

        }

        /// <summary>
        /// Get the accepted submissions that have been scheduled as sessions
        /// for the provided conference id with data needed for mobile app.
        /// </summary>
        /// <param name="id">conference id</param>
        /// <returns>Conference object</returns>
        [EnableCors]
        [HttpGet("conferencesmobileapp/{id:int}")]
        public IActionResult GetConferenceForMobileApp(int id)
        {

            /*
             * conference ID values provided to this method are either 1000 or 5000 more then the actual id value
             * so that the STFM website can use both conference ID values for old JLS conference sessions
             * and new STFM conference sessions.  
             * If the conference id values is > 1000 but less then 2000 that means the method needs to get
             * the in-person conference sessions.
             * If the conference id value is 5000 or higher that means method
             * needs to get the virtual conference supplement sessions.
             */
            Log.Information($"A request was made for conference sessions using id value of {id}");

            int stfmConferenceId = 0;

            ConferenceDTO conference = null;

            if (id > 1000 && id < 2000)
            {

                stfmConferenceId = id - 1000;

                conference = _conferenceSessionService.GetConferenceSessionsMobileApp(stfmConferenceId);

            }


            if (conference == null)
            {
                Log.Warning($"A request was made for a conference that could not be found. The Id was {stfmConferenceId}");
                return NotFound();
            }

            if (conference.Sessions.Count == 0)
            {
                Log.Warning($"A request was made for a conference that has no accepted sessions {stfmConferenceId}");
                return NotFound();

            }
            Log.Information($"A request was received for the conference with an Id of {stfmConferenceId}. The conference was found and returned.");
            return Ok(conference);
        }
    }
}
