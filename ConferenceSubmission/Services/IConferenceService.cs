﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.Models;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Services
{
    public interface IConferenceService
    {
        /// <summary>
        /// Gets the active conferences.
        /// </summary>
        /// <returns>The active conferences.</returns>
        List<ConferenceViewModel> GetActiveConferences();

		/// <summary>
        /// Get a specific conference using the provided ConferenceId.
        /// </summary>
        /// <returns>Conference object</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        Conference GetConference(int conferenceId);


        /// <summary>
        /// Get all the conferences.
        /// </summary>
        /// <returns>The conferences.</returns>
        List<ConferenceViewModel> GetConferences();

        /// <summary>
        /// Gets the conferences where the current datetime is less than
        /// or equal to the conference's ConferenceSubmissionReviewEndDate
        /// </summary>
        /// <returns>Collection of conference objects.</returns>
        List<Conference> GetConferencesWithOpenReviewStatus();

        /// <summary>
        /// Gets the submissions by category for conference.
        /// </summary>
        /// <returns>The submissions by category for conference.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        SubmissionsByCategoryForConferenceViewModel GetSubmissionsByCategoryForConference(int conferenceId);
        
        /// <summary>
        /// Get only active Cera Conferences
        /// </summary>
        /// <returns></returns>
        List<ConferenceViewModel> GetActiveCeraConferences();

        /// <summary>
        /// Gets the active conferences where the current datetime is less than
        /// or equal to the conference's ConferenceStartDate
        /// </summary>
        /// <returns>Collection of conference objects.</returns>
        List<Conference> GetConferencesWithUnstartedStatus();

        /// <summary>
        /// Retire the conference identified by the provided conference ID.
        /// </summary>
        /// <param name="conferenceId">conference ID</param>
        void RetireConference(int conferenceId);

        /// <summary>
        /// Create the next conference and its submission categories using data from the conference 
        /// identified by the provided conference Id
        /// </summary>
        /// <param name="conferenceId">conference ID</param>
        /// <returns>conference ID of new conference</returns>
        int CreateNextConference(int conferenceId);

        /// <summary>
        /// Create the review forms for the provided conference.
        /// </summary>
        /// <param name="conferenceId">conference ID</param>
        /// 
        void CreateReviewForms(int conferenceId);

        /// <summary>
        /// Toggle the value of the boolean ConferenceInactive property.
        /// </summary>
        /// <param name="conferenceId">Conference identifier</param>
        bool ToggleConferenceInactive(int conferenceId);

        /// <summary>
        /// Updates the value of the conference's CFP EndDate
        /// </summary>
        /// <param name="conferenceId"></param>
        DateTime EditCFPEndDate(int conferenceId, DateTime endDate);

        /// <summary>
        /// Updates the value of the conference's review deadline
        /// </summary>
        /// <param name="conferenceId"></param>
        DateTime EditSubmissionReviewEndDate(int conferenceId, DateTime endDate);

        /// <summary>
        /// Returns the name of a confernece, given its Id    
        /// </summary>
        /// <param name="conferenceId">Id of Conference</param>
        /// <returns></returns>
        string GetConferenceName(int conferenceId);

        /// <summary>
        /// Returns the conference whose start date falls withing the specified date range
        /// </summary>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">Start Date</param>
        /// <returns>Collection of Conferences</returns>
        IEnumerable<EntityByNameAndId> GetConferencesByDateRange(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Create proposal forms for new conference using data from old conference
        /// </summary>
        /// <param name="oldConferenceId">Conference Id of pervious conference</param>
        /// <param name="newConferenceId">Conference id of new conference</param>
        void CreateProposalForms(int oldConferenceId, int newConferenceId);

        /// <summary>
        /// Create review forms for the conference submission categories using the 
        /// review forms previously created for the old conference submission categories.
        /// </summary>
        /// <param name="oldConferenceId">Conference Id of pervious conference</param>
        /// <param name="newConferenceId">Conference id of new conference</param>
        void CreateSubmissionCategoryReviewForms(int oldConferenceId, int newConferenceId);
    }
}
