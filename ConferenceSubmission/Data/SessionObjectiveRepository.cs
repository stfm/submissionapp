﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Data
{
    public class SessionObjectiveRepository : ISessionObjectiveRepository
    {

        private readonly AppDbContext _appDbContext;

        public SessionObjectiveRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public IEnumerable<SessionObjectives> getSessionObjectives()
        {
            return _appDbContext.SessionObjectives;

        }

        public IEnumerable<SessionObjectives> getSessionObjectives(int conferenceId)
        {
            return _appDbContext.SessionObjectives.Where(so => so.conferenceId == conferenceId);
        }

        public Dictionary<int, SessionObjectives> getSessionObjectivesDictionary(int conferenceId)
        {
            

            IEnumerable<SessionObjectives> sessionObjectives = getSessionObjectives(conferenceId);

            Dictionary<int, SessionObjectives> sessionObjectivesDict = sessionObjectives.ToDictionary(so => so.submissionId, so => so);

            return sessionObjectivesDict;

        }

        public Dictionary<int, SessionObjectives> getSessionObjectivesDictionary(IEnumerable<int> submissionIds)
        {
            IEnumerable<SessionObjectives> sessionObjectives = _appDbContext.SessionObjectives.Where(so => submissionIds.Contains(so.submissionId));

            Dictionary<int, SessionObjectives> sessionObjectivesDict = sessionObjectives.ToDictionary(so => so.submissionId, so => so);

            return sessionObjectivesDict;

        }
    }
}
