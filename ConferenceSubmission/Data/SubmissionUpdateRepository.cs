﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public class SubmissionUpdateRepository : ISubmissionUpdateRepository
    {
        private readonly AppDbContext _appDbContext;

        public SubmissionUpdateRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public IEnumerable<SubmissionUpdate> getSubmissionUpdates(int acceptedCategoryId)
        {
            return _appDbContext.SubmissionUpdate.Where(su => su.AcceptedCategoryId == acceptedCategoryId);
        }
    }
}
