﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ParticipantsAddedOrRemovedReportDataViewModel
    {
        public ParticipantsAddedOrRemovedReportDataViewModel()
        {
            SubmissionsWithParticipantsAddedOrRemoved = new List<SubmissionWithParticipantsAddedOrRemoved>();
        }

        public string   ConferenceName  { get; set; }
        public DateTime AsOfDate        { get; set; }
        public List<SubmissionWithParticipantsAddedOrRemoved> SubmissionsWithParticipantsAddedOrRemoved { get; set; }
    }
}
