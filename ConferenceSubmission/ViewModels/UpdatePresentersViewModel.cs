﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class UpdatePresentersViewModel
    {
        public UpdatePresentersViewModel()
        {
            Presenters = new List<Presenter>();
            CurrentUserParticipantRoles = new List<ParticipantRoleType>();
        }

        public string ConferenceName { get; set; }

        public string SubmittedCategory { get; set; }

        public string AcceptedCategory { get; set; }

        public int SubmissionId { get; set; }

        public string SubmissionTitle { get; set; }

        public List<Presenter> Presenters { get; set; }

        public string AddParticipantUrl { get; set; }

        public List<ParticipantRoleType> CurrentUserParticipantRoles { get; set; }

        #region Helper Methods

        /// <summary>
        /// Returns the name of the current lead presenter.
        /// If there is currently not a lead presenter, returns an empty string
        /// </summary>
        /// <returns>string</returns>
        public string GetCurrentLeadPresenterName()
        {
            var leadPresenter = Presenters.FirstOrDefault(p => p.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER));
            return leadPresenter == null ? "" : $"{leadPresenter.FirstName} {leadPresenter.LastName}";
        }

        #endregion
    }
}
