﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents the status of a submission (e.g. accepted, declined, 
    /// canceled, withdrawn).
    /// </summary>
    public class SubmissionStatus
    {
        public SubmissionStatus()
        {
        }

        public int SubmissionStatusId { get; set; }

        [StringLength(250)]
        [Required]
        public SubmissionStatusType SubmissionStatusName { get; set; }



        public List<Submission> Submissions { get; set; }
    }
}
