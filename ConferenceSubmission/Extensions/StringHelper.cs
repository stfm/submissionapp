﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConferenceSubmission.Extensions
{
    public static class StringHelper
    {
        public static string ReplaceLineBreaksWithBreakTags(this string str)
        {
            var regex = new Regex("(\r\n|\r|\n)");
            return regex.Replace(str, "<br /><br />");
        }

        public static string CapitalizeFirstLetter(this string str)
        {
            if (str != null && str.Length > 0)
            {
                str = str.ToUpper()[0] + str.ToLower().Substring(1);
            }
            return str;
        }
    }
}
