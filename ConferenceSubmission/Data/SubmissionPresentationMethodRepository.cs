﻿using ConferenceSubmission.Models;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceSubmission.Data
{
    public class SubmissionPresentationMethodRepository : ISubmissionPresentationMethodRepository
    {

        private readonly AppDbContext _appDbContext;

        public SubmissionPresentationMethodRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public int AddSubmissionPresentationMethod(SubmissionPresentationMethod submissionPresentationMethod)
        {
            _appDbContext.Add(submissionPresentationMethod);

            _appDbContext.SaveChanges();

            return submissionPresentationMethod.SubmissionPresentationMethodId;
        }

        public void DeleteSubmissionPresentationMethod(int submissionPresentationMethodId)
        {
            SubmissionPresentationMethod submissionPresentationMethod =
                _appDbContext.SubmissionPresentationMethod.FirstOrDefault(spm => spm.SubmissionPresentationMethodId == submissionPresentationMethodId);

            _appDbContext.SubmissionPresentationMethod.Remove(submissionPresentationMethod);

            _appDbContext.SaveChanges();
        }

        public IEnumerable<SubmissionPresentationMethod> GetSubmissionPresentationMethods(int submissionId)
        {
            return _appDbContext.SubmissionPresentationMethod
                               .Where(spm => spm.SubmissionId == submissionId);
        }
    }
}
