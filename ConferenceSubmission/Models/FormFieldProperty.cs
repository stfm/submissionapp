﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Stores property name and values for a specific
    /// FormField - for example for a FormField of 
    /// FormFieldType = label - an object of this type
    /// would store the label's text.  For a FormField
    /// of FormFieldType an object of this type would
    /// store a propertyname of Options and a 
    /// property value of comma-delimited strings
    /// like YES,NO.
    /// </summary>
    public class FormFieldProperty
    {
        public FormFieldProperty()
        {
        }

        public int FormFieldPropertyId { get; set; }

        public int FormFieldId { get; set; }

        public FormField FormField { get; set; }

        [StringLength(250)]
        [Required]
        public String PropertyName { get; set; }

        [StringLength(500)]
        [Required]
        public String PropertyValue { get; set; }

    }
}
