﻿using System;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Services
{
    public interface IConferenceSessionService
    {
        /// <summary>
        /// Adds the conference session to the data repository
        /// </summary>
        /// <returns>The conference session database record id.</returns>
        /// <param name="sessionDataBindingModel">Session data binding model.</param>
        int AddConferenceSession(SessionDataBindingModel sessionDataBindingModel);

        /// <summary>
        /// Gets the conference session.
        /// </summary>
        /// <returns>The conference session.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        ConferenceSession GetConferenceSession(int submissionId);

        /// <summary>
        /// Updates the conference sesson.
        /// </summary>
        /// <returns>The conference sesson database record id.</returns>
        /// <param name="sessionDataBindingModel">Session data binding model.</param>
        int UpdateConferenceSession(SessionDataBindingModel sessionDataBindingModel);

        /// <summary>
        /// Get all the accepted submissions that have been scheduled for presentation
        /// at a conference.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns></returns>
        DTOs.ConferenceDTO GetConferenceSessions(int conferenceId);

        /// <summary>
        /// Get all the accepted submissions that have been scheduled for presentation
        /// at a conference with data needed for mobile app.
        /// </summary>
        /// <param name="conferenceId">conference ID</param>
        /// <returns>ConferenceDTO</returns>
        DTOs.ConferenceDTO GetConferenceSessionsMobileApp(int conferenceId);

        /// <summary>
        /// Get all the sessions identified by the collection of submission IDs
        /// provided.
        /// </summary>
        /// <param name="submissionIds"></param>
        /// <returns>Collection of DTOs.Session objects</returns>
        List<SessionWithConferenceDetails> GetConferenceSessions(IEnumerable<int> submissionIds);
        
        /// <summary>
        /// Get all the accepted submissions that have been scheduled for presentation
        /// at the virtual conference supplement.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns>ConferenceDTO object</returns>
        ConferenceDTO GetVirtualConferenceSessions(int stfmConferenceId);

        /// <summary>
        /// Update conference session with its room assignment and track.
        /// </summary>
        /// <param name="submissionId">submission ID</param>
        /// <param name="room">room assignment</param>
        /// <param name="track">track - use None if there is no track</param>
        void UpdateConferenceSessionWithRoom(int submissionId, string room, string track);

        /// <summary>
        /// Gets the conference sessions for in-person presentation
        /// </summary>
        /// <param name="conferenceId">Conference ID</param>
        /// <returns>List of ConferenceSession objects</returns>
        List<ConferenceSession> GetConferenceSessionsForInPerson(int conferenceId);

        /// <summary>
        /// Gets rooms used for all in person sessions (except Poster and Scholarly Topic)
        /// </summary>
        /// <param name="conferenceSessions">Collection of ConferenceSession objects for a conference that are presented in person</param>
        /// <returns>Sorted Dictionary</returns>
        SortedDictionary<DateOnly, List<RoomUse>> GetRoomUse(List<ConferenceSession> conferenceSessions);
    }
}
