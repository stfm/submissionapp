﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class PotentialReviewer
    {
        public PotentialReviewer() { }

        public User User { get; set; }

        public bool IsReviewer { get; set; }

    }
}
