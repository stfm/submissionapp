﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmissionTest
{
    public class ConferenceTypeTest
    {

        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public ConferenceTypeTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }


        [Fact]
        public void GetConferenceTypesTest()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new AppDbContext(options))
            {
                IEnumerable<ConferenceType> conferenceTypes = context.ConferenceTypes;

                foreach (var conferenceType in conferenceTypes)
                {

                    Output.WriteLine("Conference type is " + conferenceType.ConferenceTypeName);

                }

                Assert.NotNull(conferenceTypes);


            }


        }
    }
}
