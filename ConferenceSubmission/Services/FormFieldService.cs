﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
	public class FormFieldService : IFormFieldService
    {

		private readonly IFormFieldRepository _formFieldRepository;

		public FormFieldService(IFormFieldRepository formFieldRepository)
        {
			_formFieldRepository = formFieldRepository;
        }

		public void AddSubmissionFormField(SubmissionFormField submissionFormField)
		{

			_formFieldRepository.AddSubmissionFormField(submissionFormField);

		}

        public void UpdateSubmissionFormFieldValue(int submissionFormFieldId, string submissionFormFieldValue)
        {
            _formFieldRepository.UpdateSubmissionFormField(submissionFormFieldId, submissionFormFieldValue);
        }

		public IEnumerable<FormField> GetFormFields(int submissionCategoryId, string formFieldRole)
		{

			return _formFieldRepository.GetFormFields(submissionCategoryId, formFieldRole);

		}

        public IEnumerable<SubmissionFormField> GetFormFieldsForSubmission(int submissionId)
        {
            return _formFieldRepository.GetFormFieldsForSubmission(submissionId);
        }

        public int RemoveAllFormFieldsForSubmission(int submissionId)
        {
            return _formFieldRepository.RemoveAllFormFieldsForSubmission(submissionId);
        }

        public FormField GetFormField(int formFieldId)
        {
            return _formFieldRepository.GetFormField(formFieldId);
        }
    }
}
