﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmissionTest
{
    public class SubmissionParticipantTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public SubmissionParticipantTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }


        [Fact]
        public void GetSubmissionWithParticipants()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2018"),
                        ConferenceInactive = false,
                        ConferenceLongName = "Test Conference 1",
                        ConferenceShortName = "TC1",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2018"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture TC1" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar TC1" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }

                );

                context.SaveChanges();

                context.Submissions.AddRange(

                    new Submission
                    {
                        AcceptedCategoryId = 1000,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title",
                        
                    }

                );

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }

                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(

                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10001 }
                );


                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 2 }


                );

                context.SaveChanges();

            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new AppDbContext(options))
            {
                IEnumerable<Submission> submissions = context.Submissions.Include(s => s.ParticipantLink).ThenInclude(ptr => ptr.SubmissionParticipantToParticipantRolesLink);
                                                             

                foreach (var submission in submissions)
                {

                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                    foreach (SubmissionParticipant  submissionParticipant in submission.ParticipantLink ) {

                        Output.WriteLine("Submission participant is " + submissionParticipant.ParticipantId);

                        foreach(SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipant.SubmissionParticipantToParticipantRolesLink) {

                            Output.WriteLine("Submission participant role id is " + submissionParticipantToParticipantRole.ParticipantRoleId);

                        }


                    }

                }

                Assert.NotNull(submissions);

                IEnumerable<Submission> submissionsWithPresenters = context.Submissions.Include(s => s.ParticipantLink).ThenInclude(p => p.Participant);

                foreach (var submission in submissionsWithPresenters)
                {

                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                    foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                    {

                        Output.WriteLine("Submission participant is " + submissionParticipant.ParticipantId);

                        Output.WriteLine("Submission participant StfmUserId is " + submissionParticipant.Participant.StfmUserId);

                    }

                }


                IEnumerable<Submission> submissionsWithPresentersAndRoles = context.Submissions
                                                                                   .Include(s => s.ParticipantLink)
                                                                                       .ThenInclude(ptr => ptr.SubmissionParticipantToParticipantRolesLink)
                                                                                       .ThenInclude(pr => pr.ParticipantRole)
                                                                                   .Include(s => s.ParticipantLink)
                                                                                      .ThenInclude(p => p.Participant);


                foreach (var submission in submissionsWithPresentersAndRoles)
                {

                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                    foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                    {

                        Output.WriteLine("Submission participant is " + submissionParticipant.ParticipantId);

                        Output.WriteLine("Submission participant StfmUserId is " + submissionParticipant.Participant.StfmUserId);

                        foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipant.SubmissionParticipantToParticipantRolesLink)
                        {

                            Output.WriteLine("Submission participant role id is " + submissionParticipantToParticipantRole.ParticipantRoleId);

                            Output.WriteLine("Submission participant role name is " + submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName);

                        }

                    }

                }

            }


        }
    }
}
