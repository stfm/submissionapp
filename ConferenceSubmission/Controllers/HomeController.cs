﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Authorization;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.Mappers;
using Serilog;
using ConferenceSubmission.Services;

namespace ConferenceSubmission.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IUserMapper            _userMapper;
        private readonly ISubmissionService     _submissionService;
        private readonly IConferenceService     _conferenceService;
        private readonly IDisclosureService     _disclosureService;
        private readonly IFAQService            _fAQService;
        private readonly IAnnouncementService   _announcementService;

        public HomeController(IUserMapper userMapper, ISubmissionService submissionService, 
                              IConferenceService conferenceService, IDisclosureService disclosureService, IFAQService fAQService, IAnnouncementService announcementService)
        {
            _userMapper             = userMapper;
            _submissionService      = submissionService;
            _conferenceService      = conferenceService;
            _disclosureService      = disclosureService;
            _fAQService             = fAQService;
            _announcementService    = announcementService;
        }

        public IActionResult Index()
        {
           

            //
            var user = _userMapper.Map(User.Claims);
            var model = new HomePageViewModel()
            {
                User = user,
                SubmissionSummaryViewModels = _submissionService.GetCurrentSubmissions(user.STFMUserId),
                ActiveConferenceViewModels  = _conferenceService.GetActiveConferences(),
                Announcements               = _announcementService.GetAll()
            };

            //initiall set the conferenceDate for determining if user's disclosure is current
            //to today + 180 days
            var conferenceDate = DateTime.Now.AddDays(180);

            //Determine the latest conference date that this user has a submission to and then use that to determine
            //if user has a current disclosure - note normally we would use conference start date but since
            //we only have conference end date in this data we can use that

            if (model.SubmissionSummaryViewModels.Count > 0)
            {
                SubmissionSummaryViewModel submissionSummaryViewModel = model.SubmissionSummaryViewModels.OrderBy(x => x.ConferenceEndDate).ToList().Last();

                //use the the latest conferenceEndDate user has submitted to for the conferenceDate 
                //for determining if disclosure is current
                conferenceDate = submissionSummaryViewModel.ConferenceEndDate;

            } 

            model.isDisclosureCurrent = _disclosureService.isDisclosureCurrent(conferenceDate, user.STFMUserId);

            return View(model);
        }

        [Route("Home/ViewMySubmissions")]
        public IActionResult ViewSubmissionsNotCanceledOrWithdrawn()
        {
           

            var user = _userMapper.Map(User.Claims);

            var model = new HomePageViewModel()
            {
                User = user,
                SubmissionSummaryViewModels = _submissionService.GetAllSubmissionsNotCanceled(user.STFMUserId),
                ActiveConferenceViewModels = _conferenceService.GetConferences()
            };

            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Help()
        {
            var model = new HelpPageViewModel {
                FAQs = _fAQService.GetFAQs()
            };
            return View(model);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Route("Notauthorized")]
        public IActionResult Notauthorized()
        {
            return View();
        }

        public IActionResult ContentNotFound(ContentNotFoundPageViewModel model)
        {          
            return View(model);
        }
    }
}
