﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
namespace ConferenceSubmission.ViewModels

{
    public class SubmissionCategoryReviewIndexViewModel
    {
        public List<SubmissionCategoryViewModel> submissionCategoryViewModels { get; set; }

        public Conference conference { get; set; }

        public SubmissionCategoryReviewIndexViewModel()
        {
            submissionCategoryViewModels = new List<SubmissionCategoryViewModel>();
        }
    }
}
