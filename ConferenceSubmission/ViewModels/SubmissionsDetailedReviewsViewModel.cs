﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsDetailedReviewsViewModel
    {
        public string ConferenceLongName { get; set; }

        public int SubmissionCategoryId { get; set; }

        public string SubmissionCategoryName { get; set; }

        public List<SubmissionDetailedReview> SubmissionDetailedReviews { get; set; }

        public SubmissionsDetailedReviewsViewModel()
        {

            SubmissionDetailedReviews = new List<SubmissionDetailedReview>();

        }


    }
}
