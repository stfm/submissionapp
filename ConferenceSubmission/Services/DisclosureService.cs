﻿using System;
using System.Collections.Generic;
using Castle.Core.Internal;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;


namespace ConferenceSubmission.Services
{
    public class DisclosureService : IDisclosureService
    {

        private readonly IDisclosureRepository _disclosureRepository;

        public DisclosureService(IDisclosureRepository disclosureRepository)
        {

            _disclosureRepository = disclosureRepository;

        }

        public int AddDisclosure(Disclosure disclosure)
        {

            return _disclosureRepository.AddDisclosure(disclosure);

        }

        public Disclosure GetDisclosure(int disclosureId)
        {
            
            return _disclosureRepository.GetDisclosure(disclosureId);

        }

        public Disclosure GetDisclosure(string stfmUserId)
        {

            return _disclosureRepository.GetDisclosure(stfmUserId);

        }

        public List<Disclosure> GetDisclosures(Submission submission)
        {

            List<Disclosure> disclosures = new List<Disclosure>();

            foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
            {
                string stfmUserId = submissionParticipant.Participant.StfmUserId;

                Disclosure disclosure = GetDisclosure(stfmUserId);

                if (disclosure != null)
                {

                    disclosures.Add(disclosure);
                }


            }

            return disclosures;

        }

        public bool isDisclosureCurrent(DateTime conferenceStartDate, string stfmUserId)
        {
            bool dislosureCurrent = false;

            Disclosure disclosure = GetDisclosure(stfmUserId);

            /*
             * STFM's business rule for determing if the user has a current disclosure
             * is that the disclosure's date must be greater then the conference start
             * date minus 1 year
             */
            if ( disclosure != null) {

                if ( DateTime.Compare(disclosure.DisclosureDate, conferenceStartDate.AddYears(-1)) >= 0 ) {

                    dislosureCurrent = true;

                }

            }

            return dislosureCurrent;

        }

        public bool renameCvFile(Disclosure disclosure)
        {
            
            bool cvFileRenamed = false;

            if (disclosure != null)
            {

                string cvFilename = disclosure.CvFilename;

                // Get the first substring delimited by the first underscore and remove it from the cvFilename
                cvFilename = cvFilename.Substring(cvFilename.IndexOf('_') + 1);


                if (!cvFilename.IsNullOrEmpty())
                {

                    string fullName = disclosure.FullName;

                    fullName = fullName.Replace(" ", "_");

                    string newCvFilename = fullName + "_" + cvFilename;

                    disclosure.CvFilename = newCvFilename;

                    UpdateDisclosure(disclosure);

                    //Rename the file on the Azure blob storage


                    cvFileRenamed = true;

                }

            }

            return cvFileRenamed;   
        }

        public void UpdateDisclosure(Disclosure disclosure)
        {

            _disclosureRepository.UpdateDisclosure(disclosure);


        }
    }
}
