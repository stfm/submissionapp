﻿using ConferenceSubmission.DTOs.PosterHall;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface IPosterHallService
    {
        /// <summary>
        /// Returns accepted submissions (posters) for a particular category. 
        /// </summary>
        /// <param name="submissionCategoryId">Unique Identifier of the submission</param>
        /// <returns></returns>
        List<PosterWithPresenters> GetSubmissionsForPosterHallCategory(int submissionCategoryId);
    }
}
