﻿using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;
using AutoMapper;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using System;
using Moq;
using System.Threading;
using ConferenceSubmission.DTOs.SalesforceEvent;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmissionTest
{
    public class SubmissionServiceTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public SubmissionServiceTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;

        }

        [Fact]
        public void GetSubmissionsTest()
        {

            var repo = new SubmissionRepositoryForTesting();

            var formFieldRepo = new FormFieldRepositoryForTesting();

            var submissionCategoryService = new MockSubmissionCategoryService();

            var disclosureService = new MockDisclosoureService();

            var sut = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, new GetSubmissionActionsForSubmissionService());

            var result = sut.GetSubmissions("3001");


            foreach (SubmissionSummaryViewModel submissionSummaryViewModel in result)
            {

                Output.WriteLine(submissionSummaryViewModel.ToString());

            }

            Assert.Equal(3, result.Count());

        }

        [Fact]
        public void GetCurrentSubmissionsTest()
        {

            var repo = new SubmissionRepositoryForTesting();

            var formFieldRepo = new FormFieldRepositoryForTesting();

            var submissionCategoryService = new MockSubmissionCategoryService();

            var disclosureService = new MockDisclosoureService();

            var sut = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, new GetSubmissionActionsForSubmissionService());

            var result = sut.GetCurrentSubmissions("3001");


            foreach (SubmissionSummaryViewModel submissionSummaryViewModel in result)
            {

                Output.WriteLine(submissionSummaryViewModel.ToString());

            }

            Assert.True(result.Count() > 0);

        }


        [Fact]
        public void CfpEndDateNotPassedTest()
        {


            var repo = new SubmissionRepositoryForTesting();

            var formFieldRepo = new FormFieldRepositoryForTesting();

            var submissionCategoryService = new MockSubmissionCategoryService();

            var disclosureService = new MockDisclosoureService();

            var sut = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

            Submission aSubmission = sut.GetSubmission(10004);

            aSubmission.SubmissionStatus.SubmissionStatusName = SubmissionStatusType.OPEN;

            aSubmission.Conference.ConferenceCFPEndDate = DateTime.Now.AddHours(1);

            Thread.Sleep(2000);

            DateTime cfpEndDateUtc = TimeZoneInfo.ConvertTimeToUtc(aSubmission.Conference.ConferenceCFPEndDate);

            Output.WriteLine("CFP end datetime in UTC is {0} and server UTC DateTime is {1}", cfpEndDateUtc, TimeZoneInfo.ConvertTimeToUtc(DateTime.Now));

            SubmissionStatusType submissionStatusType = sut.GetSubmissionStatus(aSubmission);

            Assert.Equal(SubmissionStatusType.OPEN, submissionStatusType);


        }


        [Fact]
        public void CfpEndDateHasPassedTest()
        {


            var repo = new SubmissionRepositoryForTesting();

            var formFieldRepo = new FormFieldRepositoryForTesting();

            var submissionCategoryService = new MockSubmissionCategoryService();

            var disclosureService = new MockDisclosoureService();

            var sut = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

            Submission aSubmission = sut.GetSubmission(10004);

            aSubmission.SubmissionStatus.SubmissionStatusName = SubmissionStatusType.OPEN;

            aSubmission.Conference.ConferenceCFPEndDate = DateTime.Now.AddHours(-1);

            Thread.Sleep(2000);

            DateTime cfpEndDateUtc = TimeZoneInfo.ConvertTimeToUtc(aSubmission.Conference.ConferenceCFPEndDate);

            Output.WriteLine("CFP end datetime in UTC is {0} and server UTC DateTime is {1}", cfpEndDateUtc, TimeZoneInfo.ConvertTimeToUtc(DateTime.Now));

            SubmissionStatusType submissionStatusType = sut.GetSubmissionStatus(aSubmission);

            Assert.Equal(SubmissionStatusType.PENDING_REVIEW, submissionStatusType);


        }

        
        [Fact]
        public void GetSubmissionWithProposalTest_VerifyCorrectParticipants()
        {
            ///Arrange
            var mockDisclosureService = new Mock<IDisclosureService>();
            mockDisclosureService.Setup(x => x.isDisclosureCurrent(It.IsAny<DateTime>(), It.IsAny<string>())).Returns(true);

            var mockFormFieldRepository = new Mock<IFormFieldRepository>();
            mockFormFieldRepository.Setup(x => x.GetFormFieldsForSubmission(It.IsAny<int>())).Returns(new List<SubmissionFormField>());
            mockFormFieldRepository.Setup(x => x.GetFormFieldsAnswerRequired(It.IsAny<int>(), It.IsAny<string>())).Returns(new List<FormField>());

            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            mockSubmissionRepository.Setup(x => x.GetSubmissionAllDetailsBySubmissionId(It.IsAny<int>())).Returns(new Submission {
                SubmissionId = 1,
                SubmissionTitle = "Test Submission",
                AcceptedCategory = new SubmissionCategory { SubmissionCategoryName = "Accepted Category name"},
                SubmissionCategory = new SubmissionCategory { SubmissionCategoryName = "Submission Category name", SubmissionPaymentRequirement = SubmissionPaymentRequirement.EVERYONE },
                SubmissionStatus = new SubmissionStatus { SubmissionStatusName = SubmissionStatusType.ACCEPTED },
                SubmissionAbstract = "Abstract",
                Conference = new Conference { ConferenceLongName = "2020 Annual Spring Conference",ConferenceStartDate = DateTime.Now.AddDays(60), ConferenceCFPEndDate = DateTime.Now },
                ParticipantLink = new List<SubmissionParticipant>
                {
                   new SubmissionParticipant{ SubmissionParticipantId = 1, SortOrder = 3, Participant = new Participant { StfmUserId = "ABC" } },
                   new SubmissionParticipant{ SubmissionParticipantId = 2, SortOrder = 2, Participant = new Participant { StfmUserId = "DEF" } },
                   new SubmissionParticipant{ SubmissionParticipantId = 3, SortOrder = 1, Participant = new Participant { StfmUserId = "GHI" } }
                }
            });

            var mockSalesfoceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesfoceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).
                ReturnsAsync(new List<User>() {
                    new User
                    {
                        STFMUserId = "ABC",
                        FirstName = "John",
                        LastName = "Doe",
                        Credentials = "MD"
                    },
                    new User
                    {
                        STFMUserId = "DEF",
                        FirstName = "Jane",
                        LastName = "Doe",
                        Credentials = "MD"
                    },
                    new User
                    {
                        STFMUserId = "GHI",
                        FirstName = "Frank",
                        LastName = "Porridge",
                        Credentials = "MD"
                    },
                    new User
                    {
                        STFMUserId = "JKL",
                        FirstName = "Suzy",
                        LastName = "Sunshine",
                        Credentials = "MD"
                    }
                });
            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesfoceAPIService.Object, mockFormFieldRepository.Object, null, mockDisclosureService.Object, null, null, null, null, null, null, null);

            //Act
            var result = submissionService.GetSubmissionWithProposal(1, "ABC");

            //Assert
            Assert.Equal(3, result.Presenters.Count);
            Assert.Equal("Frank", result.Presenters.FirstOrDefault(p => p.SortOrder == 1).FirstName);
            Assert.Equal("Jane", result.Presenters.FirstOrDefault(p => p.SortOrder == 2).FirstName);
            Assert.Equal("John", result.Presenters.FirstOrDefault(p => p.SortOrder == 3).FirstName);
            Assert.DoesNotContain(result.Presenters, p => p.LastName == "Sunshine");
        }

        [Fact]
        public void GetSubmissionWithProposalTest()
        {

            var repo = new SubmissionRepositoryForTesting();

            var formFieldRepo = new FormFieldRepositoryForTesting();

            var salesforceAPIService = new MockSalesforceAPIService();

            var submissionCategoryService = new MockSubmissionCategoryService();

            var disclosureService = new MockDisclosoureService();

            var sut = new SubmissionService(repo, salesforceAPIService, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

            var result = sut.GetSubmissionWithProposal(1001, "user1");

            Output.WriteLine("Submission data: " + result.ToString());

            Assert.Equal("Submission Title", result.SubmissionTitle);




        }


        [Fact]
        public void IsSubmissionCategoryProposalCompletedTrue()
        {

            var repo = new SubmissionRepositoryForTesting();

            var formFieldRepo = new FormFieldRepositoryForTesting();

            var submissionCategoryService = new MockSubmissionCategoryService();

            var disclosureService = new MockDisclosoureService();

            var sut = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

            var result = sut.IsSubmissionCategoryProposalCompleted(1001);

            Output.WriteLine("Is submission completed value: " + result.ToString());

            Assert.True(result);


        }

        [Fact]
        public void IsSubmissionCategoryProposalCompletedFalse()
        {

            var repo = new SubmissionRepositoryForTesting();

            var formFieldRepo = new FormFieldRepositoryForTesting();

            var submissionCategoryService = new MockSubmissionCategoryService();

            var disclosureService = new MockDisclosoureService();

            var sut = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

            var result = sut.IsSubmissionCategoryProposalCompleted(2001);

            Output.WriteLine("Is submission completed value: " + result.ToString());

            Assert.False(result);


        }



        [Fact]
        public void UpdateSubmissionBaseDetailsTest()
        {
            var options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                //arrange
                var repo = new SubmissionRepository(context);
                var formFieldRepo = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                var submissionService = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                //act
                submissionService.UpdateSubmissionBaseDetails(10001, "A Great New Title", "A Great New Abstract", false);
            }

            using (var context = new AppDbContext(options))
            {
                var repo = new SubmissionRepository(context);
                var formFieldRepo = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                var submissionService = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                //Assert
                var updatedSubmission = submissionService.GetSubmission(10001);
                Assert.True(updatedSubmission.SubmissionTitle == "A Great New Title");
                Assert.True(updatedSubmission.SubmissionAbstract == "A Great New Abstract");
            }
        }


        [Fact]
        public void UpdateSubmissionAcceptedCategoryTest()
        {
            var options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                //arrange
                var repo = new SubmissionRepository(context);
                var formFieldRepo = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                var submissionService = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                //act
                submissionService.UpdateSubmissionAcceptedCategory(10001, 1002);
            }

            using (var context = new AppDbContext(options))
            {
                var repo = new SubmissionRepository(context);
                var formFieldRepo = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                var submissionService = new SubmissionService(repo, null, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                //Assert
                var updatedSubmission = submissionService.GetSubmission(10001);
                Assert.True(updatedSubmission.AcceptedCategoryId == 1002);
            }
        }

        [Fact]
        public void GetSubmissionPrintViewModelTest()
        {
            var options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                //arrange
                var repo = new SubmissionRepository(context);
                var formFieldRepo = new FormFieldRepository(context);
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                var salesforceAPIService = new MockSalesforceAPIService();
                var submissionService = new SubmissionService(repo, salesforceAPIService, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                //act
                var submissionPrintViewModel = submissionService.GetSubmissionPrintViewModel(10001);

                //Assert
                Assert.Equal("2019 Medical Student Education Conference", submissionPrintViewModel.ConferenceName);
                Assert.Equal("Test Abstract", submissionPrintViewModel.SubmissionAbstract);
                Assert.Equal("Test Title", submissionPrintViewModel.SubmissionTitle);
                Assert.Equal("Lecture MSE19", submissionPrintViewModel.SubmissionCategory);
                Assert.Equal("Seminar MSE19", submissionPrintViewModel.AcceptedCategory);
                Assert.Contains(submissionPrintViewModel.Presenters, (p => $"{p.FirstName} {p.LastName}" == "Bruce Phillips"));
                Assert.Contains(submissionPrintViewModel.Presenters, (p => $"{p.FirstName} {p.LastName}" == "John Doe"));
            }
        }

        [Fact]
        public void GetSubmissionFormFields()
        {
            //arrange
            var options = GetDbContextOptions();
            var submissionFormFields = new List<SubmissionFormField>();

            using (var context = new AppDbContext(options))
            {
                var repo = new SubmissionRepository(context);
                var formFieldRepo = new FormFieldRepositoryForTesting();
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                var salesforceAPIService = new MockSalesforceAPIService();
                var submissionService = new SubmissionService(repo, salesforceAPIService, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                //act
                submissionFormFields = submissionService.GetSubmissionFormFields(1234);
            }

            //assert
            Assert.True(submissionFormFields.Count == 2);
        }

        [Fact]
        public void BuildPresentersListFromSubmissionParticipants()
        {
            //arrange
            var options = GetDbContextOptions();
            var presenters = new List<Presenter>();

            using (var context = new AppDbContext(options))
            {
                var repo = new SubmissionRepository(context);
                var formFieldRepo = new FormFieldRepositoryForTesting();
                var submissionCategoryService = new MockSubmissionCategoryService();
                var disclosureService = new MockDisclosoureService();
                var salesforceAPIService = new MockSalesforceAPIService();
                var submissionService = new SubmissionService(repo, salesforceAPIService, formFieldRepo, submissionCategoryService, disclosureService, null, null, null, null, null, null, null);

                //act
                //I know that the MockSalesforceAPIService GetMultipleUserInfo() method 
                //will return 4 STFM users with ids of 3001, 3002, 3003
                presenters = submissionService.BuildPresentersListFromSubmissionParticipants(new List<SubmissionParticipant> {
                    new SubmissionParticipant{
                        SortOrder = 1,
                        Participant = new Participant{StfmUserId = "3001", ParticipantId = 1 },
                        SubmissionParticipantId = 1,
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>()
                    },
                    new SubmissionParticipant{
                        SortOrder = 2,
                        Participant = new Participant{StfmUserId = "3002", ParticipantId = 2 },
                        SubmissionParticipantId = 2,
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>()
                    },
                    new SubmissionParticipant{
                        SortOrder = 3,
                        Participant = new Participant{StfmUserId = "3003", ParticipantId = 3 },
                        SubmissionParticipantId = 3,
                        SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>()
                    },
                });
            }

            //assert
            Assert.Equal(3, presenters.Count);

        }

        [Fact]
        public void GetSubmissionsWhoseParticipantOrderHasBeenModified_MultipleSubmissionsNoneModified_VerifyResults()
        {
            //arrange
            var submissions = new List<Submission>
            {
                new Submission {ParticipantOrderModified = false},
                new Submission {ParticipantOrderModified = false},
                new Submission {ParticipantOrderModified = false},
                new Submission {ParticipantOrderModified = false}
            };
            var options = GetDbContextOptions(submissions);

            using (var context = new AppDbContext(options))
            {
                var submissionRepository = new SubmissionRepository(context);

                //act
                var results = submissionRepository.GetSubmissionsWhoseParticipantOrderHasBeenModified(100);

                //assert
                Assert.Empty(results);
            }
        }

        [Fact]
        public void GetSubmissionsWhoseParticipantOrderHasBeenModified_MultipleSubmissionsSomeModified_VerifyResults()
        {
            //arrange
            var submissions = new List<Submission>
            {
                new Submission {SubmissionId = 1, ParticipantOrderModified = false},
                new Submission {SubmissionId = 2, ParticipantOrderModified = true},
                new Submission {SubmissionId = 3, ParticipantOrderModified = false},
                new Submission {SubmissionId = 4, ParticipantOrderModified = true}
            };
            var options = GetDbContextOptions(submissions);

            using (var context = new AppDbContext(options))
            {
                var submissionRepository = new SubmissionRepository(context);

                //act
                var results = submissionRepository.GetSubmissionsWhoseParticipantOrderHasBeenModified(100);

                //assert
                Assert.Equal(2, results.Count);
                Assert.Contains(2, results);
                Assert.Contains(4, results);
            }
        }

        [Fact]
        public void GetSubmissionsWhoseParticipantOrderHasBeenModified_NoSubmissions_VerifyResults()
        {
            //arrange
            var submissions = new List<Submission>();
            var options = GetDbContextOptions(submissions);

            using (var context = new AppDbContext(options))
            {
                var submissionRepository = new SubmissionRepository(context);

                //act
                var results = submissionRepository.GetSubmissionsWhoseParticipantOrderHasBeenModified(100);

                //assert
                Assert.Empty(results);
            }
        }

        [Fact]
        public void GetSubmissionsWhoseParticipantOrderHasBeenModified_MultipleSubmissionsAllModified_VerifyResults()
        {
            var submissions = new List<Submission>
            {
                new Submission {SubmissionId = 1, ParticipantOrderModified = true},
                new Submission {SubmissionId = 2, ParticipantOrderModified = true},
                new Submission {SubmissionId = 3, ParticipantOrderModified = true},
                new Submission {SubmissionId = 4, ParticipantOrderModified = true}
            };
            var options = GetDbContextOptions(submissions);

            using (var context = new AppDbContext(options))
            {
                var submissionRepository = new SubmissionRepository(context);

                //act
                var results = submissionRepository.GetSubmissionsWhoseParticipantOrderHasBeenModified(100);

                //assert
                Assert.Equal(4, results.Count);
                Assert.Contains(1, results);
                Assert.Contains(2, results);
                Assert.Contains(3, results);
                Assert.Contains(4, results);
            }
        }

        [Fact]
        public void GetSubmissionsWhoseParticipantOrderHasBeenModified_ConferenceDoesNotExist_VerifyResults()
        {
            var submissions = new List<Submission>
            {
                new Submission {SubmissionId = 1, ParticipantOrderModified = true},
                new Submission {SubmissionId = 2, ParticipantOrderModified = true},
                new Submission {SubmissionId = 3, ParticipantOrderModified = true},
                new Submission {SubmissionId = 4, ParticipantOrderModified = true}
            };
            var options = GetDbContextOptions(submissions);

            using (var context = new AppDbContext(options))
            {
                var submissionRepository = new SubmissionRepository(context);
                var idOfConferenceThatDoesNotExist = 123;

                //act
                var results = submissionRepository.GetSubmissionsWhoseParticipantOrderHasBeenModified(idOfConferenceThatDoesNotExist);

                //assert
                Assert.Empty(results);
            }
        }

        [Fact]
        private void GetSessionsWithNoRegistrationsReportViewModel_StubTwoSubmissionsAndAllHaveAllPresentersRegistered_VerifyResults()
        {
            //Arrange
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            var mockConferenceService = new Mock<IConferenceService>();

            var conferenceName = "2020 Annual Conference";
            var eventName = "2020 Annual Conference Event";

            mockConferenceService.Setup(x => x.GetConferenceName(It.IsAny<int>())).Returns(() => conferenceName);

            var submissionsForRegistrationReport = new List<SubmissionForRegistrationReport>
            {
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 1 Title",
                    SubmissionId = 1,
                    SessionCode = "S01",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00GHS78FXFASD98",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00TRS78RTLAS998",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.PRESENTER                                
                            }
                        }
                    }
                },
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 2 Title",
                    SubmissionId = 2,
                    SessionCode = "S02",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00TRS78RTLAS998",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER                                
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00QWS7898YF7D99",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.SUBMITTER
                            }
                        }
                    }
                }
            };

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForConferenceRegistrationReport(It.IsAny<int>())).Returns(() => submissionsForRegistrationReport);

            var registeredEventAttendees = new List<EventAttendee>
            {
                new EventAttendee
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "johndoe@email.com",
                    STFMUserId = "00GHS78FXFASD98"
                },
                new EventAttendee
                {
                    FirstName = "Babs",
                    LastName = "Smith",
                    Credentials = "JD",
                    Email = "babssmith@email.com",
                    STFMUserId = "00TRS78RTLAS998"
                },
                new EventAttendee
                {
                    FirstName = "Brunhilda",
                    LastName = "Schoenhoffer",
                    Credentials = "PhD",
                    Email = "brunhildaschoenhoffer@email.com",
                    STFMUserId = "00QWS7898YF7D99"
                }
            };

            mockSalesforceAPIService.Setup(x => x.GetEventAttendees(It.IsAny<string>())).ReturnsAsync(() => registeredEventAttendees.AsEnumerable());

            var getMultipleUserInfoCallCount = 0;
            mockSalesforceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).Callback(() => getMultipleUserInfoCallCount++);

            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesforceAPIService.Object, null, null, null, null, null, mockConferenceService.Object, null, null, null, null);

            //Act
            var results = submissionService.GetSessionsWithNoRegistrationsReportViewModel(1, "00QE787FD88", eventName);

            //Assert
            Assert.Equal(conferenceName, results.ConferenceName);
            Assert.Equal(eventName, results.EventName);
            Assert.Empty(results.SessionsWithNoRegistrations);
            Assert.Equal(0, getMultipleUserInfoCallCount);
        }

        [Fact]
        private void GetSessionsWithNoRegistrationsReportViewModel_StubTwoSubmissionsAndAllHaveAtLeastOnePresenterRegistered_VerifyResults()
        {
            //Arrange
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            var mockConferenceService = new Mock<IConferenceService>();

            var conferenceName = "2020 Annual Conference";
            var eventName = "2020 Annual Conference Event";

            mockConferenceService.Setup(x => x.GetConferenceName(It.IsAny<int>())).Returns(() => conferenceName);

            var submissionsForRegistrationReport = new List<SubmissionForRegistrationReport>
            {
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 1 Title",
                    SubmissionId = 1,
                    SessionCode = "S01",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00GHS78FXFASD98",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00TRS78RTLAS998",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.PRESENTER
                            }
                        }
                    }
                },
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 2 Title",
                    SubmissionId = 2,
                    SessionCode = "S02",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00FR458RTLASPL3",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00QWS7898YF7D99",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.SUBMITTER
                            }
                        }
                    }
                }
            };

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForConferenceRegistrationReport(It.IsAny<int>())).Returns(() => submissionsForRegistrationReport);

            var registeredEventAttendees = new List<EventAttendee>
            {
                new EventAttendee
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "johndoe@email.com",
                    STFMUserId = "00GHS78FXFASD98"
                },
                new EventAttendee
                {
                    FirstName = "Brunhilda",
                    LastName = "Schoenhoffer",
                    Credentials = "PhD",
                    Email = "brunhildaschoenhoffer@email.com",
                    STFMUserId = "00QWS7898YF7D99"
                }
            };

            mockSalesforceAPIService.Setup(x => x.GetEventAttendees(It.IsAny<string>())).ReturnsAsync(() => registeredEventAttendees.AsEnumerable());

            var getMultipleUserInfoCallCount = 0;
            mockSalesforceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).Callback(() => getMultipleUserInfoCallCount++);

            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesforceAPIService.Object, null, null, null, null, null, mockConferenceService.Object, null, null, null, null);

            //Act
            var results = submissionService.GetSessionsWithNoRegistrationsReportViewModel(1, "00QE787FD88", eventName);

            //Assert
            Assert.Equal(conferenceName, results.ConferenceName);
            Assert.Equal(eventName, results.EventName);

            Assert.Empty(results.SessionsWithNoRegistrations);
            Assert.Equal(0, getMultipleUserInfoCallCount);
        }

        [Fact]
        private void GetSessionsWithNoRegistrationsReportViewModel_StubTwoSubmissionsAndOneHasNoRegisteredPresentersAndLeadPresenterAndSubmitterAreDifferentPeople_VerifyResults()
        {
            //Arrange
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            var mockConferenceService = new Mock<IConferenceService>();

            var conferenceName = "2020 Annual Conference";
            var eventName = "2020 Annual Conference Event";

            mockConferenceService.Setup(x => x.GetConferenceName(It.IsAny<int>())).Returns(() => conferenceName);

            var submissionsForRegistrationReport = new List<SubmissionForRegistrationReport>
            {
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 1 Title",
                    SubmissionId = 1,
                    SessionCode = "S01",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00GHS78FXFASD98",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00TRS78RTLAS998",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.PRESENTER
                            }
                        }
                    }
                },
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 2 Title",
                    SubmissionId = 2,
                    SessionCode = "S02",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00FR458RTLASPL3",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00QWS7898YF7D99",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.SUBMITTER
                            }
                        }
                    }
                }
            };

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForConferenceRegistrationReport(It.IsAny<int>())).Returns(() => submissionsForRegistrationReport);

            var registeredEventAttendees = new List<EventAttendee>
            {
                new EventAttendee
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "johndoe@email.com",
                    STFMUserId = "00GHS78FXFASD98"
                }
            };

            mockSalesforceAPIService.Setup(x => x.GetEventAttendees(It.IsAny<string>())).ReturnsAsync(() => registeredEventAttendees.AsEnumerable());

            var users = new List<User>
            {
                new User
                {
                    STFMUserId = "00FR458RTLASPL3",
                    FirstName = "Quenton",
                    LastName = "Smith",
                    Credentials = "MD",
                    Email = "quentonsmith@email.com"
                },
                new User
                {
                    STFMUserId = "00QWS7898YF7D99",
                    FirstName = "James",
                    LastName = "Wentworth",
                    Credentials = "DO",
                    Email = "jameswentworth@email.com"
                }
            };

            mockSalesforceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>()))
                .ReturnsAsync(() => users);          

            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesforceAPIService.Object, null, null, null, null, null, mockConferenceService.Object, null, null, null, null);

            //Act
            var results = submissionService.GetSessionsWithNoRegistrationsReportViewModel(1, "00QE787FD88",eventName);

            //Assert
            Assert.Equal(conferenceName, results.ConferenceName);
            Assert.Equal(eventName, results.EventName);

            Assert.Equal("Submission 2 Title", results.SessionsWithNoRegistrations.First().SubmissionTitle);
            Assert.Equal("S02", results.SessionsWithNoRegistrations.First().SessionCode);

            Assert.Single(results.SessionsWithNoRegistrations);

            Assert.Equal("Quenton Smith, MD", results.SessionsWithNoRegistrations.First().LeadPresenterFullName);
            Assert.Equal("quentonsmith@email.com", results.SessionsWithNoRegistrations.First().LeadPresenterEmail);

            Assert.Equal("James Wentworth, DO", results.SessionsWithNoRegistrations.First().SubmitterFullName);
            Assert.Equal("jameswentworth@email.com", results.SessionsWithNoRegistrations.First().SubmitterEmail);
        }

        [Fact]
        private void GetSessionsWithNoRegistrationsReportViewModel_StubTwoSubmissionsAndOneHasNoRegisteredPresentersAndLeadPresenterAndSubmitterAreTheSamePerson_VerifyResults()
        {
            //Arrange
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            var mockConferenceService = new Mock<IConferenceService>();

            var conferenceName = "2020 Annual Conference";
            var eventName = "2020 Annual Conference Event";

            mockConferenceService.Setup(x => x.GetConferenceName(It.IsAny<int>())).Returns(() => conferenceName);

            var submissionsForRegistrationReport = new List<SubmissionForRegistrationReport>
            {
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 1 Title",
                    SubmissionId = 1,
                    SessionCode = "S01",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00GHS78FXFASD98",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00TRS78RTLAS998",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.PRESENTER
                            }
                        }
                    }
                },
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 2 Title",
                    SubmissionId = 2,
                    SessionCode = "S02",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00FR458RTLASPL3",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        }
                    }
                }
            };

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForConferenceRegistrationReport(It.IsAny<int>())).Returns(() => submissionsForRegistrationReport);

            var registeredEventAttendees = new List<EventAttendee>
            {
                new EventAttendee
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "johndoe@email.com",
                    STFMUserId = "00GHS78FXFASD98"
                }
            };

            mockSalesforceAPIService.Setup(x => x.GetEventAttendees(It.IsAny<string>())).ReturnsAsync(() => registeredEventAttendees.AsEnumerable());

            var users = new List<User>
            {
                new User
                {
                    STFMUserId = "00FR458RTLASPL3",
                    FirstName = "Quenton",
                    LastName = "Smith",
                    Credentials = "MD",
                    Email = "quentonsmith@email.com"
                },
                new User
                {
                    STFMUserId = "00QWS7898YF7D99",
                    FirstName = "James",
                    LastName = "Wentworth",
                    Credentials = "DO",
                    Email = "jameswentworth@email.com"
                }
            };

            mockSalesforceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>()))
                .ReturnsAsync(() => users);

            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesforceAPIService.Object, null, null, null, null, null, mockConferenceService.Object, null, null, null, null);

            //Act
            var results = submissionService.GetSessionsWithNoRegistrationsReportViewModel(1, "00QE787FD88", eventName);

            //Assert
            Assert.Equal(conferenceName, results.ConferenceName);
            Assert.Equal(eventName, results.EventName);

            Assert.Equal("Submission 2 Title", results.SessionsWithNoRegistrations.First().SubmissionTitle);
            Assert.Equal("S02", results.SessionsWithNoRegistrations.First().SessionCode);

            Assert.Single(results.SessionsWithNoRegistrations);

            Assert.Equal("Quenton Smith, MD", results.SessionsWithNoRegistrations.First().LeadPresenterFullName);
            Assert.Equal("quentonsmith@email.com", results.SessionsWithNoRegistrations.First().LeadPresenterEmail);

            Assert.Equal("Quenton Smith, MD", results.SessionsWithNoRegistrations.First().SubmitterFullName);
            Assert.Equal("quentonsmith@email.com", results.SessionsWithNoRegistrations.First().SubmitterEmail);
        }

        [Fact]
        private void GetSessionsWithNoRegistrationsReportViewModel_StubTwoSubmissionsAndNoneHaveRegisteredPresenters_VerifyResults()
        {
            //Arrange
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            var mockConferenceService = new Mock<IConferenceService>();

            var conferenceName = "2020 Annual Conference";
            var eventName = "2020 Annual Conference Event";

            mockConferenceService.Setup(x => x.GetConferenceName(It.IsAny<int>())).Returns(() => conferenceName);

            var submissionsForRegistrationReport = new List<SubmissionForRegistrationReport>
            {
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 1 Title",
                    SubmissionId = 1,
                    SessionCode = "S01",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00GHS78FXFASD98",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00TRS78RTLAS993",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        }
                    }
                },
                new SubmissionForRegistrationReport
                {
                    SubmissionTitle = "Submission 2 Title",
                    SubmissionId = 2,
                    SessionCode = "S02",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = "00FR458RTLASPL3",
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        }
                    }
                }
            };

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForConferenceRegistrationReport(It.IsAny<int>())).Returns(() => submissionsForRegistrationReport);

            var registeredEventAttendees = new List<EventAttendee>
            {
                new EventAttendee
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "johndoe@email.com",
                    STFMUserId = "00YNN7ZFXFASD66"
                }
            };

            mockSalesforceAPIService.Setup(x => x.GetEventAttendees(It.IsAny<string>())).ReturnsAsync(() => registeredEventAttendees.AsEnumerable());

            var users = new List<User>
            {
                new User
                {
                    STFMUserId = "00GHS78FXFASD98",
                    FirstName = "Quenton",
                    LastName = "Smith",
                    Credentials = "MD",
                    Email = "quentonsmith@email.com"
                },
                new User
                {
                    STFMUserId = "00TRS78RTLAS993",
                    FirstName = "James",
                    LastName = "Wentworth",
                    Credentials = "DO",
                    Email = "jameswentworth@email.com"
                },
                new User
                {
                    STFMUserId = "00FR458RTLASPL3",
                    FirstName = "Brunhilda",
                    LastName = "Schneider",
                    Credentials = "MD",
                    Email = "brunhildaschneider@email.com"
                }
            };

            mockSalesforceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>()))
                .ReturnsAsync(() => users);

            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesforceAPIService.Object, null, null, null, null, null, mockConferenceService.Object, null, null, null, null);

            //Act
            var results = submissionService.GetSessionsWithNoRegistrationsReportViewModel(1, "00QE787FD88", eventName);

            //Assert
            Assert.Equal(conferenceName, results.ConferenceName);
            Assert.Equal(eventName, results.EventName);
            Assert.Equal(2, results.SessionsWithNoRegistrations.Count);

            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionTitle == "Submission 1 Title");
            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionTitle == "Submission 2 Title");

            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionId == 1 && x.LeadPresenterFullName == "Quenton Smith, MD");
            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionId == 1 && x.LeadPresenterEmail == "quentonsmith@email.com");
            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionId == 1 && x.SubmitterFullName == "James Wentworth, DO");
            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionId == 1 && x.SubmitterEmail == "jameswentworth@email.com");

            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionId == 2 && x.LeadPresenterFullName == "Brunhilda Schneider, MD");
            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionId == 2 && x.LeadPresenterEmail == "brunhildaschneider@email.com");
            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionId == 2 && x.SubmitterFullName == "Brunhilda Schneider, MD");
            Assert.Contains(results.SessionsWithNoRegistrations, x => x.SubmissionId == 2 && x.SubmitterEmail == "brunhildaschneider@email.com");
        }

        [Fact]
        private void AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel_StubNoSubmissions_VerifyResults()
        {
            //Arrange
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            var mockConferenceService = new Mock<IConferenceService>();

            var conferenceName = "2020 Annual Conference";
            var eventName = "2020 Annual Conference Event";
            var getMultipleUserInfoCallCount = 0;

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForConferenceRegistrationReport(It.IsAny<int>())).Returns(() => new List<SubmissionForRegistrationReport>());
            mockSalesforceAPIService.Setup(x => x.GetEventAttendees(It.IsAny<string>())).ReturnsAsync(() => new List<EventAttendee>());
            mockSalesforceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<List<string>>())).ReturnsAsync(() => new List<User>()).Callback(() => getMultipleUserInfoCallCount++);            
            mockConferenceService.Setup(x => x.GetConferenceName(It.IsAny<int>())).Returns(() => conferenceName);

            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesforceAPIService.Object, null, null, null, null, null, mockConferenceService.Object, null, null, null, null);

            //Act
            var results = submissionService.GetAllSessionsWithPresentersThatHaveNotRegisteredReportViewModel(1, "00034JERS34", eventName);

            //Assert
            Assert.Empty(results.SessionWithUnregisteredPresenters);
            Assert.Equal(conferenceName, results.ConferenceName);
            Assert.Equal(eventName, results.EventName);
            Assert.Equal(0, getMultipleUserInfoCallCount);
        }

        [Fact]
        private void AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel_StubASubmissionsAndNoUnregisteredPresenters_VerifyResults()
        {
            //Arrange
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            var mockConferenceService = new Mock<IConferenceService>();

            var conferenceName = "2020 Annual Conference";
            var eventName = "2020 Annual Conference Event";
            var getMultipleUserInfoCallCount = 0;

            var johnDoeSTFMUserId = "0092FJD9FK8";

            mockConferenceService.Setup(x => x.GetConferenceName(It.IsAny<int>())).Returns(() => conferenceName);

            var submissions = new List<SubmissionForRegistrationReport>
            {
                new SubmissionForRegistrationReport
                {
                    SubmissionId = 1001,
                    SessionCode = "CRP01",
                    SubmissionTitle = "A Nice Poster",
                    SubmissionCategory = "Completed Research Poster",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = johnDoeSTFMUserId,
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        }
                    }
                }
            };
            mockSubmissionRepository.Setup(x => x.GetSubmissionsForConferenceRegistrationReport(It.IsAny<int>())).Returns(() => submissions);

            var registeredEventAttendees = new List<EventAttendee>
            {
                new EventAttendee
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "johndoe@email.com",
                    STFMUserId = johnDoeSTFMUserId
                }
            };
            mockSalesforceAPIService.Setup(x => x.GetEventAttendees(It.IsAny<string>())).ReturnsAsync(() => registeredEventAttendees);
            mockSalesforceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).ReturnsAsync(() => new List<User>()).Callback(() => getMultipleUserInfoCallCount++);

            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesforceAPIService.Object, null, null, null, null, null, mockConferenceService.Object, null, null, null, null);

            //Act
            var result = submissionService.GetAllSessionsWithPresentersThatHaveNotRegisteredReportViewModel(1, "009JDF87DJF", eventName);

            Assert.Empty(result.SessionWithUnregisteredPresenters);
            Assert.Equal(conferenceName, result.ConferenceName);
            Assert.Equal(eventName, result.EventName);
            Assert.Equal(0, getMultipleUserInfoCallCount);
        }

        [Fact]
        private void AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel_StubASubmissionsWithRegisteredLeadPresenterAndUnregisteredSubmitterPresenter_VerifyResults()
        {
            //Arrange
            var mockSalesforceAPIService = new Mock<ISalesforceAPIService>();
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();
            var mockConferenceService = new Mock<IConferenceService>();

            var conferenceName = "2020 Annual Conference";
            var eventName = "2020 Annual Conference Event";
            var getMultipleUserInfoCallCount = 0;

            var registeredSTFMUserId = "00930PPlFK7";
            var unregisteredSTFMUserId = "0092FJD9FK8";

            mockConferenceService.Setup(x => x.GetConferenceName(It.IsAny<int>())).Returns(() => conferenceName);

            var submissions = new List<SubmissionForRegistrationReport>
            {
                new SubmissionForRegistrationReport
                {
                    SubmissionId = 1001,
                    SessionCode = "CRP01",
                    SubmissionTitle = "A Nice Poster",
                    SubmissionCategory = "Completed Research Poster",
                    PresentersForRegistrationReport = new List<PresenterForRegistrationReport>
                    {
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = registeredSTFMUserId,
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.LEAD_PRESENTER,
                                ParticipantRoleType.SUBMITTER
                            }
                        },
                        new PresenterForRegistrationReport
                        {
                            STFMUserId = unregisteredSTFMUserId,
                            Roles = new List<ParticipantRoleType>
                            {
                                ParticipantRoleType.SUBMITTER,
                                ParticipantRoleType.PRESENTER
                            }
                        }
                    }
                }
            };
            mockSubmissionRepository.Setup(x => x.GetSubmissionsForConferenceRegistrationReport(It.IsAny<int>())).Returns(() => submissions);

            var registeredEventAttendees = new List<EventAttendee>
            {
                new EventAttendee
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    Email = "johndoe@email.com",
                    STFMUserId = registeredSTFMUserId
                }
            };
            mockSalesforceAPIService.Setup(x => x.GetEventAttendees(It.IsAny<string>())).ReturnsAsync(() => registeredEventAttendees);

            var users = new List<User>
            {
                new User
                {
                    FirstName = "William",
                    LastName = "Dolittle",
                    Credentials = "MD",
                    Email = "WilliamDolittle@email.com",
                    STFMUserId = unregisteredSTFMUserId
                }
            };
            mockSalesforceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).ReturnsAsync(() => users).Callback(() => getMultipleUserInfoCallCount++);

            var submissionService = new SubmissionService(mockSubmissionRepository.Object, mockSalesforceAPIService.Object, null, null, null, null, null, mockConferenceService.Object, null, null, null, null);

            //Act
            var result = submissionService.GetAllSessionsWithPresentersThatHaveNotRegisteredReportViewModel(1, "009JDF87DJF", eventName);

            Assert.Single(result.SessionWithUnregisteredPresenters);
            Assert.Equal(conferenceName, result.ConferenceName);
            Assert.Equal(eventName, result.EventName);
            Assert.Equal(1, getMultipleUserInfoCallCount);

            Assert.Equal("William Dolittle, MD", result.SessionWithUnregisteredPresenters.First().UnregisteredPresenters.First().FullName);
            Assert.Equal("WilliamDolittle@email.com", result.SessionWithUnregisteredPresenters.First().UnregisteredPresenters.First().Email);
            Assert.Contains(ParticipantRoleType.SUBMITTER, result.SessionWithUnregisteredPresenters.First().UnregisteredPresenters.First().Roles);
            Assert.Contains(ParticipantRoleType.PRESENTER, result.SessionWithUnregisteredPresenters.First().UnregisteredPresenters.First().Roles);
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions(List<Submission> submissions)
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                new Conference
                {
                    ConferenceId = 100,
                    ConferenceDeleted = false,
                    ConferenceEndDate = DateTime.Parse($"Jan 15, {DateTime.Now.Year}"),
                    ConferenceInactive = false,
                    ConferenceLongName = $"{DateTime.Now.Year} Medical Student Education Conference",
                    ConferenceShortName = "MSE19",
                    ConferenceStartDate = DateTime.Parse($"Jan 11, {DateTime.Now.Year}"),
                    ConferenceTypeId = 1
                });

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" }
                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(
                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW },
                    new SubmissionStatus { SubmissionStatusId = 2002, SubmissionStatusName = SubmissionStatusType.CANCELED }
                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                for (int i = 0; i < submissions.Count; i++)
                {

                    submissions[i].AcceptedCategoryId = 1000;
                    submissions[i].Conference = aConference;
                    submissions[i].SubmissionAbstract = $"Test Abstract {i}";
                    submissions[i].SubmissionCategoryId = 1000;
                    submissions[i].SubmissionCreatedDateTime = DateTime.Parse($"Feb 14, {DateTime.Now.Year}");
                    submissions[i].SubmissionId = submissions[i].SubmissionId == 0 ? 10000 + i : submissions[i].SubmissionId;
                    submissions[i].SubmissionLastUpdatedDateTime = DateTime.Parse($"Feb 14, {DateTime.Now.Year}");
                    submissions[i].SubmissionStatusId = 2000;
                    submissions[i].SubmissionTitle = $"Test Title {i}";
                    submissions[i].Conference = aConference;

                    context.Submissions.Add(submissions[i]);
                }
                context.SaveChanges();
            }
            return options;
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW },
                    new SubmissionStatus { SubmissionStatusId = 2002, SubmissionStatusName = SubmissionStatusType.CANCELED }
               );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {

                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title",

                    },

                new Submission
                {

                    AcceptedCategoryId = 1000,
                    Conference = aConference,
                    SubmissionAbstract = "Test Abstract 2",
                    SubmissionCategoryId = 1000,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 12, 2018"),
                    SubmissionId = 10002,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 12, 2018"),
                    SubmissionStatusId = 2000,
                    SubmissionTitle = "Test Title 2",

                },

                new Submission
                {

                    AcceptedCategoryId = 1000,
                    Conference = aConference,
                    SubmissionAbstract = "Test Abstract 3",
                    SubmissionCategoryId = 1000,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10003,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 2000,
                    SubmissionTitle = "Test Title 3",

                },

                new Submission
                {

                    AcceptedCategoryId = 1000,
                    Conference = aConference,
                    SubmissionAbstract = "Test Abstract 4",
                    SubmissionCategoryId = 1000,
                    SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionId = 10004,
                    SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 14, 2018"),
                    SubmissionStatusId = 2000,
                    SubmissionTitle = "Test Title 3",

                }

                );

                context.SaveChanges();

                ConferenceSession aConferenceSession = new ConferenceSession { SessionCode = "S01", SessionLocation = "Green Room", SessionStartDateTime = DateTime.Parse(" Apr 10, 2018") };

                SubmissionPayment submissionPayment = new SubmissionPayment { PaymentAmount = 25, PaymentDateTime = DateTime.Parse(" Apr 10, 2018"), SubmissionId = 10001, PaymentTransactionId = "P999" };


                Submission aSubmission = context.Submissions.Find(10001);

                aSubmission.ConferenceSession = aConferenceSession;

                aSubmission.SubmissionPayment = submissionPayment;

                context.Update(aSubmission);

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }


                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(

                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 3, ParticipantId = 4002, SubmissionId = 10002 },
                    new SubmissionParticipant { SubmissionParticipantId = 4, ParticipantId = 4003, SubmissionId = 10003 }
                );


                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 2 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 3 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 4 }


                );

                context.SaveChanges();

            }

            return options;
        }

    }
}
