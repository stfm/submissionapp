﻿using System;
namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Models a person who has been assigned to review submissions.
    /// </summary>
    public class Reviewer
    {
        public Reviewer()
        {
        }

        public int ReviewerId { get; set; }

        public string StfmUserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the reviewer.  This value
        /// is retrieved from SalesForces at runtime and is not
        /// mapped to the database.
        /// </summary>
        /// <value>The name of the reviewer.</value>
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string ReviewerName { get; set; }

        /// <summary>
        /// Gets or sets the email of the reviewer.  This value
        /// is retrieved from SalesForces at runtime and is not
        /// mapped to the database.
        /// </summary>
        /// <value>The email of the reviewer.</value>
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string ReviewerEmail { get; set; }

        public override string ToString()
        {

            return "ReviewerId: " + ReviewerId + " | StfmUserId: " + StfmUserId;

        }
    }
}
