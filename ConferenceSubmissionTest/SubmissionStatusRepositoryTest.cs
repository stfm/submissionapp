﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class SubmissionStatusRepositoryTest
    {
        [Fact]
        public void GetSubmissionStatus_OPEN()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            SubmissionStatus submissionStatus;
            using (var context = new AppDbContext(options))
            {
                var submissionStatusRepository = new SubmissionStatusRepository(context);
                submissionStatus = submissionStatusRepository.GetBySubmissionStatusType(SubmissionStatusType.OPEN);
            }

            Assert.Equal(1, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetSubmissionStatus_PENDING_REVIEW()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            SubmissionStatus submissionStatus;
            using (var context = new AppDbContext(options))
            {
                var submissionStatusRepository = new SubmissionStatusRepository(context);
                submissionStatus = submissionStatusRepository.GetBySubmissionStatusType(SubmissionStatusType.PENDING_REVIEW);
            }

            Assert.Equal(2, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetSubmissionStatus_ACCEPTED()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            SubmissionStatus submissionStatus;
            using (var context = new AppDbContext(options))
            {
                var submissionStatusRepository = new SubmissionStatusRepository(context);
                submissionStatus = submissionStatusRepository.GetBySubmissionStatusType(SubmissionStatusType.ACCEPTED);
            }

            Assert.Equal(3, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetSubmissionStatus_DECLINED()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            SubmissionStatus submissionStatus;
            using (var context = new AppDbContext(options))
            {
                var submissionStatusRepository = new SubmissionStatusRepository(context);
                submissionStatus = submissionStatusRepository.GetBySubmissionStatusType(SubmissionStatusType.DECLINED);
            }

            Assert.Equal(4, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetSubmissionStatus_WITHDRAWN()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            SubmissionStatus submissionStatus;
            using (var context = new AppDbContext(options))
            {
                var submissionStatusRepository = new SubmissionStatusRepository(context);
                submissionStatus = submissionStatusRepository.GetBySubmissionStatusType(SubmissionStatusType.WITHDRAWN);
            }

            Assert.Equal(5, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetSubmissionStatus_CANCELED()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            SubmissionStatus submissionStatus;
            using (var context = new AppDbContext(options))
            {
                var submissionStatusRepository = new SubmissionStatusRepository(context);
                submissionStatus = submissionStatusRepository.GetBySubmissionStatusType(SubmissionStatusType.CANCELED);
            }

            Assert.Equal(6, submissionStatus.SubmissionStatusId);
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 1, SubmissionStatusName = SubmissionStatusType.OPEN });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 2, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 3, SubmissionStatusName = SubmissionStatusType.ACCEPTED });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 4, SubmissionStatusName = SubmissionStatusType.DECLINED });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 5, SubmissionStatusName = SubmissionStatusType.WITHDRAWN });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 6, SubmissionStatusName = SubmissionStatusType.CANCELED });

                context.SaveChanges();
            }

            return options;
        }
    }
}
