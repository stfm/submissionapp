﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class User
    {
        public User()
        {
            Memberships = new List<string>();
        }

        //public properties
        public string STFMUserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Credentials { get; set; }
        public string Email { get; set; }
        public List<string> Memberships { get; set; }
        #region Helper Methods

        /// <summary>
        /// returns the users full name nicely formatted 
        /// </summary>
        /// <returns>string</returns>
        public string GetFullNamePretty()
        {
            return $"{FirstName} {LastName}, {Credentials}".TrimEnd(new char[] { ',',' '}).Replace(", ,", ",");
        }

        #endregion
    }
}
