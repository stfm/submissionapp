﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface IParticipantRepository
    {

        IEnumerable<Participant> GetParticipants { get;  }
        /// <summary>
        /// Get all participant roles
        /// </summary>
        /// <value>Collection of ParticipantRole objects</value>
        IEnumerable<ParticipantRole> GetParticipantRoles { get; }

        /// <summary>
        /// Get a specific participant role using the provided
        /// ParticipantRoleId
        /// /// </summary>
        /// <returns>ParticipantRole object</returns>
        /// <param name="participantRoleId">Participant role identifier.</param>
        ParticipantRole GetParticipantRole(int participantRoleId);

        /// <summary>
        /// Get a specific participant role using the provided
        /// ParticipantRoleName
        /// </summary>
        /// <returns>ParticipantRole object</returns>
        /// <param name="participantRoleName">Participant role name.</param>
        ParticipantRole GetParticipantRole(ParticipantRoleType participantRoleName);

        /// <summary>
        /// Get a specific Participant using the provided StfmUserId.
        /// </summary>
        /// <returns>The Participant by stfm user identifier.</returns>
        /// <param name="StfmUserId">Stfm user identifier.</param>
        Participant GetParticipantByStfmUserId(string StfmUserId);

        /// <summary>
        /// Gets the participant by participant identifier.
        /// </summary>
        /// <returns>The participant by participant identifier.</returns>
        /// <param name="participantId">Participant identifier.</param>
        Participant GetParticipantByParticipantId(int participantId);

        /// <summary>
        /// Creates the Participant.
        /// </summary>
        /// <returns>The participant id</returns>
        /// <param name="participant">Participant.</param>
        int CreateParticipant(Participant participant);


    }
}
