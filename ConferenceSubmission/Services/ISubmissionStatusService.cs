﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface ISubmissionStatusService
    {
		/// <summary>
        /// Get the SubmissionStatus using the provided SubmissionStatusType
        /// </summary>
        /// <returns>SubmissionStatus</returns>
        /// <param name="submissionStatusType">Submission status type.</param>
        SubmissionStatus GetBySubmissionStatusType(SubmissionStatusType submissionStatusType);
    }
}
