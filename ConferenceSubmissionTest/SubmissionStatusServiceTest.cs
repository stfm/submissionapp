﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class SubmissionStatusServiceTest
    {
        [Fact]
        public void GetBySubmissionStatusType_OPEN()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            SubmissionStatus submissionStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var submissionStatusService = new SubmissionStatusService(new SubmissionStatusRepository(context));
                submissionStatus = submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.OPEN);
            }

            //Assert
            Assert.Equal(SubmissionStatusType.OPEN, submissionStatus.SubmissionStatusName);
            Assert.Equal(1, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetBySubmissionStatusType_PENDING_REVIEW()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            SubmissionStatus submissionStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var submissionStatusService = new SubmissionStatusService(new SubmissionStatusRepository(context));
                submissionStatus = submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.PENDING_REVIEW);
            }

            //Assert
            Assert.Equal(SubmissionStatusType.PENDING_REVIEW, submissionStatus.SubmissionStatusName);
            Assert.Equal(2, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetBySubmissionStatusType_ACCEPTED()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            SubmissionStatus submissionStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var submissionStatusService = new SubmissionStatusService(new SubmissionStatusRepository(context));
                submissionStatus = submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.ACCEPTED);
            }

            //Assert
            Assert.Equal(SubmissionStatusType.ACCEPTED, submissionStatus.SubmissionStatusName);
            Assert.Equal(3, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetBySubmissionStatusType_DECLINED()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            SubmissionStatus submissionStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var submissionStatusService = new SubmissionStatusService(new SubmissionStatusRepository(context));
                submissionStatus = submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.DECLINED);
            }

            //Assert
            Assert.Equal(SubmissionStatusType.DECLINED, submissionStatus.SubmissionStatusName);
            Assert.Equal(4, submissionStatus.SubmissionStatusId);
            Assert.Equal("Not Accepted", submissionStatus.SubmissionStatusName.GetDescription());
            Assert.Equal("DECLINED", Description.GetSubmissionStatusTypeString(4));
        }
       
        [Fact]
        public void GetBySubmissionStatusType_WITHDRAWN()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            SubmissionStatus submissionStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var submissionStatusService = new SubmissionStatusService(new SubmissionStatusRepository(context));
                submissionStatus = submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.WITHDRAWN);
            }

            //Assert
            Assert.Equal(SubmissionStatusType.WITHDRAWN, submissionStatus.SubmissionStatusName);
            Assert.Equal(5, submissionStatus.SubmissionStatusId);
        }

        [Fact]
        public void GetBySubmissionStatusType_CANCELED()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            SubmissionStatus submissionStatus;

            //Act
            using (var context = new AppDbContext(options))
            {
                var submissionStatusService = new SubmissionStatusService(new SubmissionStatusRepository(context));
                submissionStatus = submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.CANCELED);
            }

            //Assert
            Assert.Equal(SubmissionStatusType.CANCELED, submissionStatus.SubmissionStatusName);
            Assert.Equal(6, submissionStatus.SubmissionStatusId);
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 1, SubmissionStatusName = SubmissionStatusType.OPEN });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 2, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 3, SubmissionStatusName = SubmissionStatusType.ACCEPTED });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 4, SubmissionStatusName = SubmissionStatusType.DECLINED });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 5, SubmissionStatusName = SubmissionStatusType.WITHDRAWN });
                context.SubmissionStatuses.Add(new SubmissionStatus() { SubmissionStatusId = 6, SubmissionStatusName = SubmissionStatusType.CANCELED });

                context.SaveChanges();
            }

            return options;
        }
    }
}
