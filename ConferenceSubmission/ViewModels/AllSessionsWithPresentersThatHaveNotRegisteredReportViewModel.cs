﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel
    {
        public AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel()
        {
            SessionWithUnregisteredPresenters = new List<SessionWithUnregisteredPresenters>();
        }

        public string ConferenceName { get; set; }
        public string EventName { get; set; }

        public List<SessionWithUnregisteredPresenters> SessionWithUnregisteredPresenters { get; set; }
    }

    public class SessionWithUnregisteredPresenters
    {
        public SessionWithUnregisteredPresenters()
        {
            UnregisteredPresenters = new List<UnregisteredPresenter>();
        }

        public int SubmissionId { get; set; }
        public string SessionCode { get; set; }
        public string SubmissionTitle { get; set; }

        public List<UnregisteredPresenter> UnregisteredPresenters { get; set; }
    }

    public class UnregisteredPresenter
    {
        public UnregisteredPresenter()
        {
            Roles = new List<ParticipantRoleType>();
        }

        public string FullName { get; set; }
        public string Email { get; set; }
        public List<ParticipantRoleType> Roles { get; set; }

        public string GetRolesPretty()
        {
            return string.Join(", ", Roles.Select(x => x.ToString().Replace('_', ' '))).Trim();
        }
    }
}
