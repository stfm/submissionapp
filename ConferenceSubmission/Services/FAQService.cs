﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public class FAQService : IFAQService
    {
        private readonly IFAQRepository _fAQRepository;

        public FAQService(IFAQRepository fAQRepository)
        {
            _fAQRepository = fAQRepository;
        }

        public List<FAQ> GetCeraFAQs()
        {
            //ConferenceType 4 is CERA FAQs
            return _fAQRepository.GetFAQs().Where(x => x.ConferenceType.Contains("4", StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        public List<FAQ> GetFAQs()
        {
            return _fAQRepository.GetFAQs().Where(x => x.ConferenceType.Contains("1", StringComparison.InvariantCultureIgnoreCase)).ToList();
        }
    }
}
