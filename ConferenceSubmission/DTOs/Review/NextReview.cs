﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs.Review
{
    public class NextReview
    {
        public string submissionId { get; set; }
        public int categoryId { get; set; }
    }
}
