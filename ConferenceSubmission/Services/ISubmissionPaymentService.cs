﻿using System;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Services
{
    public interface ISubmissionPaymentService
    {
        /// <summary>
        /// Processes the payment for a submission.
        /// </summary>
        /// <returns>View model with transaction information.</returns>
        /// <param name="ApiLoginID">API login identifier.</param>
        /// <param name="ApiTransactionKey">API transaction key.</param>
        /// <param name="Production">If set to <c>true</c> production Authorize.net setting.</param>
        /// <param name="creditCardPaymentBindingModel">Credit card payment binding model.</param>
        SubmissionPaymentViewModel ProcessPayment(String ApiLoginID, String ApiTransactionKey, Boolean Production,
                                                  CreditCardPaymentBindingModel creditCardPaymentBindingModel);

        public SubmissionPaymentViewModel ProcessPaymentNew(string ApiLoginID, string ApiTransactionKey,
                                                         Boolean Production,
                                                         CreditCardPaymentBindingModel creditCardPaymentBindingModel);
    }
}
