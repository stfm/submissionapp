﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents a payment for a submission.  Some conferences require
    /// non-members to pay to submit.
    /// </summary>
    public class SubmissionPayment
    {
        public SubmissionPayment()
        {
        }

        public int SubmissionPaymentId { get; set; }

        public int SubmissionId { get; set; }

        public Submission Submission { get; set; }

        public int PaymentAmount { get; set; }

        public DateTime PaymentDateTime { get; set; }

        [StringLength(250)]
        public string PaymentTransactionId { get; set; }

    }
}
