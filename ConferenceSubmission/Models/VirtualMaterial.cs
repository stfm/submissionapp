﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class VirtualMaterial
    {

        public VirtualMaterial()
        {

        }

        public int VirtualMaterialId { get; set; }

        [ParentId]
        public int SubmissionId { get; set; }

        public Submission Submission { get; set; }

        [StringLength(250)]
        [Required]
        public String MaterialType { get; set; }


        [StringLength(500)]
        [Required]
        public String MaterialValue { get; set; }

        [StringLength(50)]
        [Required]
        public String PersonUploaded { get; set; }

        [Required]
        public DateTime UploadedDateTime { get; set; }

        [StringLength(500)]
        public String VideoUrl { get; set; }
    }
}
