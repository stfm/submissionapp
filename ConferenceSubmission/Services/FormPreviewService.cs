﻿using ConferenceSubmission.Data;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public class FormPreviewService : IFormPreviewService
    {
        private readonly IConferenceService _conferenceService;
        private readonly ISubmissionCategoryService _SubmissionCategoryService;
        private readonly IFormFieldService _formFieldService;
        private readonly ISubmissionCategoryRepository _submissionCategoryRepository;

        public FormPreviewService(IConferenceService conferenceService, ISubmissionCategoryService submissionCategoryService, IFormFieldService formFieldService, ISubmissionCategoryRepository submissionCategoryRepository)
        {
            _conferenceService = conferenceService;
            _SubmissionCategoryService = submissionCategoryService;
            _formFieldService = formFieldService;
            _submissionCategoryRepository = submissionCategoryRepository;
        }

        public FormPreviewGeneratorViewModel GetFormPreviewGeneratorViewModel(int conferenceId, int categoryId, FormPreviewMode previewMode)
        {
            var conference = _conferenceService.GetConference(conferenceId);
            var category = _SubmissionCategoryService.GetSubmissionCategory(categoryId);
            var model = new FormPreviewGeneratorViewModel
            {
                ConferenceName = conference.ConferenceLongName,
                ConferenceId = conferenceId,
                CategoryName = category.SubmissionCategoryName,
                CategoryId = category.SubmissionCategoryId,
                CategoryAbstractInstructions = category.CategoryAbstractInstructions,
                CategoryAbstractMaxLength = category.CategoryAbstractMaxLength,
                CategoryInstructions = category.CategoryInstructions,
                FormPreviewMode = previewMode,
                FormFields = _formFieldService.GetFormFields(categoryId, previewMode.ToString().ToLowerInvariant()).ToList()
            };

            return model;
        }

        public ProposalFormPreviewCategoriesDTO GetProposalFormPreviewCategories(int conferneceId)
        {
            var categories = _submissionCategoryRepository.GetSubmissionCategoriesWithFormFieldsByConference(conferneceId);
            var conferenceName = _conferenceService.GetConferenceName(conferneceId);

            var model = new ProposalFormPreviewCategoriesDTO
            {
                ConferenceId = conferneceId,
                ConferenceName = conferenceName,
                Categories = categories.Select(x => new ProposalFormPreviewCategory
                {
                    CategoryId = x.SubmissionCategoryId,
                    CategoryName = x.SubmissionCategoryName
                }).ToList()
            };

            return model;
        }
    }

    public interface IFormPreviewService
    {
        FormPreviewGeneratorViewModel GetFormPreviewGeneratorViewModel(int conferenceId, int categoryId, FormPreviewMode previewMode);
        ProposalFormPreviewCategoriesDTO GetProposalFormPreviewCategories(int conferneceId);
    }
}
