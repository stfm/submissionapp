﻿using System;
namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// Submission category view model.
    /// </summary>
    public class SubmissionCategoryViewModel
    {
        public SubmissionCategoryViewModel()
        {
        }

        public int SubmissionCategoryId { get; set; }

        public string SubmissionCategoryName { get; set; }

        public bool SubmissionCategoryInactive { get; set; }


    }
}
