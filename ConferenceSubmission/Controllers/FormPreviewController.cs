﻿using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Controllers
{
    public class FormPreviewController : Controller
    {
        private readonly IFormPreviewService _formPreviewService;

        public FormPreviewController(IFormPreviewService formPreviewService)
        {
            _formPreviewService = formPreviewService;
        }

        [EnableCors]
        [HttpGet]
        public IActionResult GetForm(int conferenceId, int submissionCategoryId)
        {
            var model = _formPreviewService.GetFormPreviewGeneratorViewModel(conferenceId, submissionCategoryId, FormPreviewMode.PROPOSAL);

            return PartialView("FormPreviewGenerator", model);
        }

        [EnableCors]
        [HttpGet]
        public IActionResult GetProposalFormCategories(int conferenceId)
        {
            var model = _formPreviewService.GetProposalFormPreviewCategories(conferenceId);

            return new JsonResult(model);
        }
    }
}
