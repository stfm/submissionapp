﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class SubmissionReviewerRepositoryTest
    {

        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public SubmissionReviewerRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;

        }


        [Fact]
        public void GetSubmissionReviewersBySubmissionIdTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                SubmissionReviewerRepository submissionReviewerRepository = new SubmissionReviewerRepository(context);

                IEnumerable<SubmissionReviewer> submissionReviewers = submissionReviewerRepository.GetSubmissionReviewers(10001);

                foreach (SubmissionReviewer submissionReviewer in submissionReviewers) {

                    Output.WriteLine("SubmissionReviewerId is " + submissionReviewer.SubmissionReviewerId);

                    Output.WriteLine("ReviewerId is " + submissionReviewer.ReviewerId);

                    Output.WriteLine("StfmUserId is " + submissionReviewer.Reviewer.StfmUserId);

                    Output.WriteLine("Submission Title is " + submissionReviewer.Submission.SubmissionTitle);



                }

                Assert.Equal(2, submissionReviewers.Count());

            }
        }


        [Fact]
        public void GetSubmissionReviewersByReviewerTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                SubmissionReviewerRepository submissionReviewerRepository = new SubmissionReviewerRepository(context);

                Reviewer reviewer = new Reviewer
                {

                    ReviewerId = 4002,
                    StfmUserId = "3002"
                };

                IEnumerable<SubmissionReviewer> submissionReviewers = submissionReviewerRepository.GetSubmissionReviewers(reviewer);

                foreach (SubmissionReviewer submissionReviewer in submissionReviewers)
                {

                    Output.WriteLine("SubmissionReviewerId is " + submissionReviewer.SubmissionReviewerId);

                    Output.WriteLine("ReviewerId is " + submissionReviewer.ReviewerId);

                    Output.WriteLine("StfmUserId is " + submissionReviewer.Reviewer.StfmUserId);

                    Output.WriteLine("Submission Title is " + submissionReviewer.Submission.SubmissionTitle);



                }

                Assert.Equal(3, submissionReviewers.Count());

            }
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceCFPEndDate = DateTime.Parse("Sep 11, 2018"),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(5),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceCFPEndDate = DateTime.Parse("Sep 11, 2018"),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(5),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }

                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {
                        AcceptedCategoryId = 1000,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title 1",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract 2",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 11, 2018"),
                        SubmissionId = 10002,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 11, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title 2",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Submission With A Submitter Only Participant",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                        SubmissionId = 10003,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 19, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Submission With A Submitter Only Participant - Abstract",

                    }

                );

                context.SaveChanges();

                //Create some Reviewers

                context.Reviewers.AddRange(

                    new Reviewer { ReviewerId = 4001, StfmUserId = "3001" },

                    new Reviewer { ReviewerId = 4002, StfmUserId = "3002" },

                    new Reviewer { ReviewerId = 4003, StfmUserId = "3003" }
                );

                context.SaveChanges();

                //Create some SubmissionReviewer records to assign reviewers to submissions

                context.SubmissionReviewer.AddRange(

                    new SubmissionReviewer { SubmissionReviewerId = 1, SubmissionId = 10001, ReviewerId = 4001 },

                    new SubmissionReviewer { SubmissionReviewerId = 2, SubmissionId = 10002, ReviewerId = 4001 },

                    new SubmissionReviewer { SubmissionReviewerId = 3, SubmissionId = 10001, ReviewerId = 4002 },

                    new SubmissionReviewer { SubmissionReviewerId = 4, SubmissionId = 10002, ReviewerId = 4002 },

                    new SubmissionReviewer { SubmissionReviewerId = 5, SubmissionId = 10003, ReviewerId = 4003 },

                    new SubmissionReviewer { SubmissionReviewerId = 6, SubmissionId = 10003, ReviewerId = 4002 }


                );

                context.SaveChanges();

            }

            return options;
        }

    }
}
