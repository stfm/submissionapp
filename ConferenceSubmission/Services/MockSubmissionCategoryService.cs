﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Services
{
	public class MockSubmissionCategoryService : ISubmissionCategoryService
    {
        public MockSubmissionCategoryService()
        {
        }

        public List<SubmissionCategoryViewModel> GetActiveSubmissionCategories(int ConferenceId)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionCategoryViewModel> GetSubmissionCategories(int ConferenceId)
        {
            throw new NotImplementedException();
        }

        public SubmissionCategory GetSubmissionCategory(int submissionCategoryId)
        {


            SubmissionCategory lecture = new SubmissionCategory();
            lecture.ConferenceId = 100;
            lecture.SubmissionCategoryId = 2;
            lecture.SubmissionCategoryName = "Lecture";
            lecture.SubmissionCategoryInactive = false;
            lecture.SubmissionPaymentRequirement = SubmissionPaymentRequirement.NON_MEMBER_ONLY;

            return lecture;


        }
    }
}
