﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class multipleparticipantroles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionParticipant_ParticipantRoles_ParticipantRoleId",
                table: "SubmissionParticipant");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubmissionParticipant",
                table: "SubmissionParticipant");

            migrationBuilder.DropIndex(
                name: "IX_SubmissionParticipant_ParticipantRoleId",
                table: "SubmissionParticipant");

            migrationBuilder.DropColumn(
                name: "ParticipantRoleId",
                table: "SubmissionParticipant");

            migrationBuilder.AddColumn<int>(
                name: "SubmissionParticipantId",
                table: "SubmissionParticipant",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubmissionParticipant",
                table: "SubmissionParticipant",
                column: "SubmissionParticipantId");

            migrationBuilder.CreateTable(
                name: "SubmissionParticipantToParticipantRole",
                columns: table => new
                {
                    SubmissionParticipantToParticipantRoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ParticipantRoleId = table.Column<int>(nullable: false),
                    SubmissionParticipantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionParticipantToParticipantRole", x => x.SubmissionParticipantToParticipantRoleId);
                    table.ForeignKey(
                        name: "FK_SubmissionParticipantToParticipantRole_ParticipantRoles_ParticipantRoleId",
                        column: x => x.ParticipantRoleId,
                        principalTable: "ParticipantRoles",
                        principalColumn: "ParticipantRoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubmissionParticipantToParticipantRole_SubmissionParticipant_SubmissionParticipantId",
                        column: x => x.SubmissionParticipantId,
                        principalTable: "SubmissionParticipant",
                        principalColumn: "SubmissionParticipantId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionParticipant_SubmissionId",
                table: "SubmissionParticipant",
                column: "SubmissionId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionParticipantToParticipantRole_ParticipantRoleId",
                table: "SubmissionParticipantToParticipantRole",
                column: "ParticipantRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionParticipantToParticipantRole_SubmissionParticipantId",
                table: "SubmissionParticipantToParticipantRole",
                column: "SubmissionParticipantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubmissionParticipantToParticipantRole");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubmissionParticipant",
                table: "SubmissionParticipant");

            migrationBuilder.DropIndex(
                name: "IX_SubmissionParticipant_SubmissionId",
                table: "SubmissionParticipant");

            migrationBuilder.DropColumn(
                name: "SubmissionParticipantId",
                table: "SubmissionParticipant");

            migrationBuilder.AddColumn<int>(
                name: "ParticipantRoleId",
                table: "SubmissionParticipant",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubmissionParticipant",
                table: "SubmissionParticipant",
                columns: new[] { "SubmissionId", "ParticipantId" });

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionParticipant_ParticipantRoleId",
                table: "SubmissionParticipant",
                column: "ParticipantRoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionParticipant_ParticipantRoles_ParticipantRoleId",
                table: "SubmissionParticipant",
                column: "ParticipantRoleId",
                principalTable: "ParticipantRoles",
                principalColumn: "ParticipantRoleId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
