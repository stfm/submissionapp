﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class StfmUserIdMissingFromSalesforce
    {

        public string StfmUserId { get; set; }

        /// <summary>
        /// Collection of Submission IDs where this
        /// person is a participant.
        /// </summary>
        public List<int> ParticipantSubmissionIds { get; set; }

        /// <summary>
        /// Collection of Submission IDs where this 
        /// person is a reviewer.
        /// </summary>
        public List<int> ReviewerSubmissionIds { get; set; }

        public StfmUserIdMissingFromSalesforce()
        {
            ParticipantSubmissionIds = new List<int>();

            ReviewerSubmissionIds = new List<int>();
        }


    }
}
