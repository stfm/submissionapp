﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class ParticipantServiceTest
    {
        private readonly IConfigurationRoot _configurationRoot;

        public ParticipantServiceTest()
        {
            _configurationRoot = GetIConfigurationRoot();
        }

        [Fact]
        public void GetOrCreateParticipant_FindsAndReturnsExistingParticipant()
        {
            var options = GetDbContextOptions();

            using(var context = new AppDbContext(options))
            {
                //Arrange
                var participantRepository = new ParticipantRepository(context);
                var participantService = new ParticipantService(participantRepository, null, null);

                //Act
                var existingParticipant = participantService.GetOrCreateParticipant("3001");

                //Assert
                Assert.NotNull(existingParticipant);
                Assert.Equal("3001", existingParticipant.StfmUserId);
                Assert.Equal(4001, existingParticipant.ParticipantId);
            }

        }

        [Fact]
        public void GetOrCreateParticipant_DoesNotFindExistingParticipant_CreatesNewParticipant()
        {
            var options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                //Arrange
                var participantRepository = new ParticipantRepository(context);
                var participantService = new ParticipantService(participantRepository, null, null);

                //Act
                var newParticipant = participantService.GetOrCreateParticipant("1234");

                //Assert
                Assert.NotNull(newParticipant);
                Assert.Equal("1234", newParticipant.StfmUserId);
                Assert.NotEqual(4001, newParticipant.ParticipantId );
                Assert.NotEqual(4002, newParticipant.ParticipantId );
                Assert.NotEqual(4003, newParticipant.ParticipantId );
            }

        }


        [Fact]
        public void GetParticipantsRolesBySubmission_GetSubmissionReturnsNull_VerifyEmptyListReturned()
        {
            ///Arrange
            var submissionService = new Mock<ISubmissionService>();
            submissionService.Setup(x => x.GetSubmission(It.IsAny<int>())).Returns<Submission>(null);
            var participantService = new ParticipantService(null, submissionService.Object, null);

            ///Act
            var results = participantService.GetParticipantsRolesBySubmission(1, "ABC123");

            ///Assert
            Assert.NotNull(results);
            Assert.Empty(results);
        }

        [Fact]
        public void GetParticipantsRolesBySubmission_VerifyResults()
        {
            ///Arrange
            var participant1 = new Participant
            {
                ParticipantId = 1,
                StfmUserId = "ABC123",
            };
            var participant2 = new Participant
            {
                ParticipantId = 2,
                StfmUserId = "XYZ789"
            };
            var submitterRole = new ParticipantRole
            {
                ParticipantRoleId = 1,
                ParticipantRoleName = ParticipantRoleType.SUBMITTER
            };
            var leadPresenterRole = new ParticipantRole
            {
                ParticipantRoleId = 2,
                ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER
            };
            var presenterRole = new ParticipantRole
            {
                ParticipantRoleId = 3,
                ParticipantRoleName = ParticipantRoleType.PRESENTER
            };
            var submissionParticipant1 = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                ParticipantId = 1,
                Participant = participant1,
                SubmissionId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                {
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 1, ParticipantRole = submitterRole
                    },
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 2, ParticipantRole = leadPresenterRole
                    }
                }
            };
            var submissionParticipant2 = new SubmissionParticipant
            {
                SubmissionParticipantId = 2,
                ParticipantId = 2,
                Participant = participant2,
                SubmissionId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                {
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 2, ParticipantRoleId = 3, ParticipantRole = presenterRole
                    }
                }
            };
            var submission = new Submission
            {
                SubmissionId = 1,
                ParticipantLink = new List<SubmissionParticipant> { submissionParticipant1, submissionParticipant2 }
            };
            var submissionService = new Mock<ISubmissionService>();
            submissionService.Setup(x => x.GetSubmission(It.IsAny<int>())).Returns(submission);
            var participantService = new ParticipantService(null, submissionService.Object, null);

            ///Act
            var results = participantService.GetParticipantsRolesBySubmission(1, "ABC123");

            ///Assert
            Assert.NotNull(results);
            Assert.NotEmpty(results);
            Assert.Equal(2, results.Count);
            Assert.Contains(results, x => x == ParticipantRoleType.SUBMITTER);
            Assert.Contains(results, x => x == ParticipantRoleType.LEAD_PRESENTER);
            Assert.DoesNotContain(results, x => x == ParticipantRoleType.PRESENTER);
        }

        [Fact]
        public void GetAssignedParticipantRolesType_SubmitterOnly_VerifyResults()
        {
            ///Arrange
            var participant1 = new Participant
            {
                ParticipantId = 1,
                StfmUserId = "ABC123",
            };
            var submitterRole = new ParticipantRole
            {
                ParticipantRoleId = 1,
                ParticipantRoleName = ParticipantRoleType.SUBMITTER
            };
            var submissionParticipant1 = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                ParticipantId = 1,
                Participant = participant1,
                SubmissionId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                {
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 1, ParticipantRole = submitterRole
                    }
                }
            };
            var submission = new Submission
            {
                SubmissionId = 1,
                ParticipantLink = new List<SubmissionParticipant> { submissionParticipant1 }
            };
            var submissionService = new Mock<ISubmissionService>();
            submissionService.Setup(x => x.GetSubmission(It.IsAny<int>())).Returns(submission);
            var participantService = new ParticipantService(null, submissionService.Object, null);

            ///Act
            var result = participantService.GetAssignedParticipantRolesType(1, 1);

            ///Assert
            Assert.Equal(AssignedParticipantRolesType.SUBMITTER_ONLY, result);

        }

        [Fact]
        public void GetAssignedParticipantRolesType_SubmitterAndLeadPresenter_VerifyResults()
        {
            ///Arrange
            var participant1 = new Participant
            {
                ParticipantId = 1,
                StfmUserId = "ABC123",
            };
            var submitterRole = new ParticipantRole
            {
                ParticipantRoleId = 1,
                ParticipantRoleName = ParticipantRoleType.SUBMITTER
            };
            var leadPresenterRole = new ParticipantRole
            {
                ParticipantRoleId = 2,
                ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER
            };
            var submissionParticipant1 = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                ParticipantId = 1,
                Participant = participant1,
                SubmissionId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                {
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 1, ParticipantRole = submitterRole
                    },
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 2, ParticipantRole = leadPresenterRole
                    }
                }
            };
            var submission = new Submission
            {
                SubmissionId = 1,
                ParticipantLink = new List<SubmissionParticipant> { submissionParticipant1 }
            };
            var submissionService = new Mock<ISubmissionService>();
            submissionService.Setup(x => x.GetSubmission(It.IsAny<int>())).Returns(submission);
            var participantService = new ParticipantService(null, submissionService.Object, null);

            ///Act
            var result = participantService.GetAssignedParticipantRolesType(1, 1);

            ///Assert
            Assert.Equal(AssignedParticipantRolesType.SUBMITTER_AND_LEADPRESENTER, result);

        }

        [Fact]
        public void GetAssignedParticipantRolesType_SubmitterAndPresenter_VerifyResults()
        {
            ///Arrange
            var participant1 = new Participant
            {
                ParticipantId = 1,
                StfmUserId = "ABC123",
            };
            var submitterRole = new ParticipantRole
            {
                ParticipantRoleId = 1,
                ParticipantRoleName = ParticipantRoleType.SUBMITTER
            };
            var leadPresenterRole = new ParticipantRole
            {
                ParticipantRoleId = 2,
                ParticipantRoleName = ParticipantRoleType.PRESENTER
            };
            var submissionParticipant1 = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                ParticipantId = 1,
                Participant = participant1,
                SubmissionId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                {
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 1, ParticipantRole = submitterRole
                    },
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 2, ParticipantRole = leadPresenterRole
                    }
                }
            };
            var submission = new Submission
            {
                SubmissionId = 1,
                ParticipantLink = new List<SubmissionParticipant> { submissionParticipant1 }
            };
            var submissionService = new Mock<ISubmissionService>();
            submissionService.Setup(x => x.GetSubmission(It.IsAny<int>())).Returns(submission);
            var participantService = new ParticipantService(null, submissionService.Object, null);

            ///Act
            var result = participantService.GetAssignedParticipantRolesType(1, 1);

            ///Assert
            Assert.Equal(AssignedParticipantRolesType.SUBMITTER_AND_PRESENTER, result);

        }

        [Fact]
        public void GetAssignedParticipantRolesType_LeadPresenterOnly_VerifyResults()
        {
            ///Arrange
            var participant1 = new Participant
            {
                ParticipantId = 1,
                StfmUserId = "ABC123",
            };
            var submitterRole = new ParticipantRole
            {
                ParticipantRoleId = 1,
                ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER
            };
            var submissionParticipant1 = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                ParticipantId = 1,
                Participant = participant1,
                SubmissionId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                {
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 1, ParticipantRole = submitterRole
                    }
                }
            };
            var submission = new Submission
            {
                SubmissionId = 1,
                ParticipantLink = new List<SubmissionParticipant> { submissionParticipant1 }
            };
            var submissionService = new Mock<ISubmissionService>();
            submissionService.Setup(x => x.GetSubmission(It.IsAny<int>())).Returns(submission);
            var participantService = new ParticipantService(null, submissionService.Object, null);

            ///Act
            var result = participantService.GetAssignedParticipantRolesType(1, 1);

            ///Assert
            Assert.Equal(AssignedParticipantRolesType.LEADPRESENTER_ONLY, result);

        }

        [Fact]
        public void GetAssignedParticipantRolesType_PresenterOnly_VerifyResults()
        {
            ///Arrange
            var participant1 = new Participant
            {
                ParticipantId = 1,
                StfmUserId = "ABC123",
            };
            var submitterRole = new ParticipantRole
            {
                ParticipantRoleId = 1,
                ParticipantRoleName = ParticipantRoleType.PRESENTER
            };
            var submissionParticipant1 = new SubmissionParticipant
            {
                SubmissionParticipantId = 1,
                ParticipantId = 1,
                Participant = participant1,
                SubmissionId = 1,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                {
                    new SubmissionParticipantToParticipantRole
                    {
                        SubmissionParticipantId = 1, ParticipantRoleId = 1, ParticipantRole = submitterRole
                    }
                }
            };
            var submission = new Submission
            {
                SubmissionId = 1,
                ParticipantLink = new List<SubmissionParticipant> { submissionParticipant1 }
            };
            var submissionService = new Mock<ISubmissionService>();
            submissionService.Setup(x => x.GetSubmission(It.IsAny<int>())).Returns(submission);
            var participantService = new ParticipantService(null, submissionService.Object, null);

            ///Act
            var result = participantService.GetAssignedParticipantRolesType(1, 1);

            ///Assert
            Assert.Equal(AssignedParticipantRolesType.PRESENTER_ONLY, result);

        }

        [Fact]
        public void GetParticipantRoleDefinitions_VerifyResults()
        {
            //Arrange
            var participantService = new ParticipantService(null, null, _configurationRoot);
            List<string> results;

            //Act
            results = participantService.GetParticipantRoleDefinitions();

            //Assert
            Assert.Contains(results, x => x.Contains("Submitter-Only"));
            Assert.Contains(results, x => x.Contains("Submitter and Lead Presenter"));
            Assert.Contains(results, x => x.Contains("Submitter and Co-Presenter"));
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                //Create some participants 
                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();

            }

            return options;
        }

        public static IConfigurationRoot GetIConfigurationRoot()
        {
            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
