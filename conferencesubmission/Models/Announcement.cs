﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class Announcement
    {
        public int AnnouncementId { get; set; }
        public int SortOrder { get; set; }

        [Required]
        public string Content { get; set; }
    }
}
