﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Services
{
    public interface ISubmissionCategoryService
    {
        /// <summary>
        /// Gets a collection of submission category view model
        /// objects for a specific conference.
        /// </summary>
        /// <returns>The submission categories.</returns>
        /// <param name="ConferenceId">Conference identifier.</param>
        List<SubmissionCategoryViewModel> GetSubmissionCategories(int ConferenceId);

        /// <summary>
        /// Gets the current submission categories.
        /// </summary>
        /// <returns>The current submission categories.</returns>
        /// <param name="ConferenceId">Conference identifier.</param>
        List<SubmissionCategoryViewModel> GetActiveSubmissionCategories(int ConferenceId);

        /// <summary>
        /// Gets the submission category.
        /// </summary>
        /// <returns>The submission category.</returns>
        /// <param name="submissionCategoryId">Submission category identifier.</param>
        SubmissionCategory GetSubmissionCategory(int submissionCategoryId);

    }

   
}
