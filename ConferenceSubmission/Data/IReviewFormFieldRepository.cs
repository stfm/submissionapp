﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface IReviewFormFieldRepository
    {
        /// <summary>
        /// Adds the review form field - this is the reviewer's
        /// answer to a review form question.
        /// </summary>
        /// <param name="reviewFormField">Review form field.</param>
        void AddReviewFormField(ReviewFormField reviewFormField);

        /// <summary>
        /// Gets the review form fields answers for a specific review.
        /// </summary>
        /// <returns>The form fields for review.</returns>
        /// <param name="reviewId">Review identifier.</param>
        IEnumerable<ReviewFormField> GetFormFieldsForReview(int reviewId);

        /// <summary>
        /// Deletes all ReviewFormFieds for a review.
        /// </summary>
        /// <param name="reviewId">review's unique identifier</param>
        void RemoveAllFormFieldsForReview(int reviewId);

        /// <summary>
        /// Gets all the form fields for all the provided review ids.
        /// </summary>
        /// <returns>The form fields for reviews.</returns>
        /// <param name="reviewIds">Collection of review ids</param>
        IEnumerable<ReviewFormField> GetAllFormFieldsForMultipleReviews(List<int> reviewIds);

        /// <summary>
        /// Gets just the rating scale form fields for all the provided review ids.
        /// </summary>
        /// <returns>The form fields for reviews.</returns>
        /// <param name="reviewIds">Collection of review ids</param>
        IEnumerable<ReviewFormField> GetRatingScaleFormFieldsForMultipleReviews(List<int> reviewIds);

        /// <summary>
        /// Gets the rating scale form fields for single review.
        /// </summary>
        /// <returns>The rating scale form fields for single review.</returns>
        /// <param name="reviewId">Review identifiers.</param>
        IEnumerable<ReviewFormField> GetRatingScaleFormFieldsForSingleReview(int reviewId);

    }
}
