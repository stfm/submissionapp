﻿using System;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.ViewModels

{
    /// <summary>
    /// Stores data for each incomplete submission
    /// that we need to show in the view.
    /// </summary>
    public class ReviewIncompleteViewModel
    {

        public List<ReviewIncomplete> reviewIncomplete { get; set; }

        public string ConferenceLongName { get; set; }

        public ReviewIncompleteViewModel()
        {
        }

    }
}