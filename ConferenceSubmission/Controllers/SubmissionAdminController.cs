﻿using System;
using Microsoft.AspNetCore.Mvc;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using ConferenceSubmission.Mappers;
using Serilog;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using ConferenceSubmission.BindingModels;
using System.Threading.Tasks;
using ConferenceSubmission.DTOs;
using Dropbox.Api.Files;
using Dropbox.Api;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace ConferenceSubmission.Controllers
{
    [Authorize("ConferenceAdmin")]
    public class SubmissionAdminController : Controller
    {

        private readonly IConferenceService _conferenceService;

        private readonly ISubmissionService _submissionService;

        private readonly ISubmissionParticipantService _submissionParticipantService;

        private readonly ISalesforceAPIService _salesforceAPIService;

        private readonly IEmailNotificationService _emailNotificationService;

        private readonly ISubmissionStatusService _submissionStatusService;

        private readonly IUserMapper _userMapper;

        private readonly ISubmissionCategoryService _SubmissionCategoryService;

        private readonly IFormFieldService _formFieldService;

        private readonly IParticipantService _participantService;

        private readonly IReviewerService _reviewerService;

        private readonly IFormPreviewService _formPreviewService;

        private readonly IVirtualMaterialService _virtualMaterialService;

        private readonly IVirtualConferenenceDataService _virtualConferenenceDataService;

        private readonly IDisclosureService _disclosureService;

        private readonly IConferenceSessionService _conferenceSessionService;

        private IConfiguration _configuration;

        private readonly IConferenceSessionsCacheService _cacheService;


        public SubmissionAdminController(IConferenceService conferenceService,
            ISubmissionService submissionService, 
            ISubmissionParticipantService submissionParticipantService,
            ISalesforceAPIService salesforceAPIService,
            IEmailNotificationService emailNotificationService,
            ISubmissionStatusService submissionStatusService,
            IUserMapper userMapper, ISubmissionCategoryService submissionCategoryService,
            IFormFieldService formFieldService, IParticipantService participantService,
            IReviewerService reviewerService, IFormPreviewService formPreviewService,
            IVirtualMaterialService virtualMaterialsService,
            IVirtualConferenenceDataService virtualConferenenceDataService,
            IConfiguration configuration, IDisclosureService disclosureService,
            IConferenceSessionService conferenceSessionService,
            IConferenceSessionsCacheService cacheService)
            
        {

            _conferenceService = conferenceService;

            _submissionService = submissionService;

            _submissionParticipantService = submissionParticipantService;

            _salesforceAPIService = salesforceAPIService;

            _emailNotificationService = emailNotificationService;

            _submissionStatusService = submissionStatusService;

            _userMapper = userMapper;

            _SubmissionCategoryService = submissionCategoryService;

            _formFieldService = formFieldService;

            _participantService = participantService;

            _reviewerService = reviewerService;

            _formPreviewService = formPreviewService;

            _virtualMaterialService = virtualMaterialsService;

            _virtualConferenenceDataService = virtualConferenenceDataService;

            _configuration = configuration;

            _disclosureService = disclosureService;

            _conferenceSessionService = conferenceSessionService;

            _cacheService = cacheService;

        }

        /// <summary>
        /// Responds to user request to view home page of submission admin.
        /// </summary>
        /// <returns>The index view.</returns>
        public IActionResult Index()
        {
            var user = _userMapper.Map(User.Claims);

            var accessPermissions = User.Claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions") == null ?
                            "" : User.Claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions").Value;

            Log.Information($"{user.STFMUserId} with access permissions of {accessPermissions} visited SubmissionAdmin home page.");

            var model = new SubmissionAdminHomeViewModel()
            {


                ActiveConferenceViewModels = _conferenceService.GetConferences()

            };

            return View(model);

        }

        /// <summary>
        /// Responds to user request to view all the incomplete submissions
        /// for a specific <paramref name="conferenceId"/>.
        /// </summary>
        /// <returns>The incomplete submissions view.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        public IActionResult FindIncompleteSubmissions(int conferenceId) {


            var model = _submissionService.GetIncompleteSubmissionsViewModel(conferenceId);
           

            return View(model);

        }

        public IActionResult GetConferenceSessions(int conferenceId)
        {

            var conferenceSessions = _conferenceSessionService.GetConferenceSessionsForInPerson(conferenceId);

            var model = new SessionsForConferenceViewModel
            {
                Conference = _conferenceService.GetConference(conferenceId),
                ConferenceSessions = conferenceSessions,
                RoomUseDict = _conferenceSessionService.GetRoomUse(conferenceSessions)

            };


            return View(model);

        }

        /// <summary>
        /// Responds to user request to send incomplete reminder emails
        /// to all presenters on submissions not yet completed
        /// for a specific <paramref name="conferenceId"/>.
        /// </summary>
        /// <returns>The SendIncompleteReminderEmails view page </returns>
        /// <param name="conferenceId">Conference identifier.</param>
        public async Task<IActionResult> SendIncompleteReminderEmailsAsync(int conferenceId)
        {

            Conference conference = _conferenceService.GetConference(conferenceId);

            ViewBag.conference = conference.ConferenceLongName;

            HttpClient Client = new HttpClient();

            var result = await Client.GetAsync("https://emailsubmissionincomplete20220716085708.azurewebsites.net/api/FunctionApp?conferenceId=" + conferenceId);
            
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                ViewBag.success = true;

            } else
            {
                ViewBag.success = false;
            }

            return View();

        }


        public IActionResult UpdateSubmissionStatusAndEmailPresenters(int submissionId, int submissionStatusId)
        {

            //Update the submission status
            _submissionService.UpdateSubmissionStatus(submissionId, submissionStatusId);

            //Get the complete details - use a fake STFM User ID value as it is not needed for our purposes
            var submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId, "1234567");

            List<SubmissionParticipant> participants = _submissionParticipantService.GetSubmissionParticipants(submissionId);

            List<string> emailAddresses = new List<string>();

            foreach (SubmissionParticipant participant in participants)
            {
                User user = _salesforceAPIService.GetUserInfo(participant.Participant.StfmUserId).Result;

                string emailAddress = user.Email;

                emailAddresses.Add(emailAddress);


            }


            _emailNotificationService.SendSubmissionAcceptedOrRejectedEmail(submissionWithProposalAnswersViewModel, submissionStatusId, emailAddresses);

            return View(submissionWithProposalAnswersViewModel);


        }
        
       

        /// <summary>
        /// Responds to admin user's request to view a specific submission
        /// </summary>
        /// <returns>View for a specific submision</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public IActionResult ViewSubmission(int submissionId)
        {
            if (!_submissionService.SubmissionExists(submissionId))
            {
                return RedirectToAction("ContentNotFound", "Home", new { EntityType = "Submission", EntityId = submissionId, ReturnController = "SubmissionAdmin", ReturnAction = "Index" });
            }

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId);

            if (_submissionService.IsSubmissionPaymentRequired(submissionWithProposalAnswersViewModel.SubmissionPaymentRequirement, submissionId))
            {

                Log.Information("Submission payment was required.");

                submissionWithProposalAnswersViewModel.SubmissionFee = Int32.Parse(_configuration["SubmissionPayment:SubmissionFee"]);

            }
            else
            {

                Log.Information("Submission payment was not required.");

                submissionWithProposalAnswersViewModel.SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE;


            }


            return View(submissionWithProposalAnswersViewModel);

        }


        public IActionResult ViewNumberOfSubmissions()
        {

            var model = new SubmissionAdminSubmissionsByConferenceViewModel
            {


                ConferenceViewModels = _conferenceService.GetConferences()

            };

            return View(model);

        }

        [HttpPost]
        public IActionResult GetCategories(int conferenceId)
        {
            SubmissionsByCategoryForConferenceViewModel submissionsByCategoryForConferenceViewModel =  _conferenceService.GetSubmissionsByCategoryForConference(conferenceId);

            return PartialView("NumberOfSubmissionsByCategory", submissionsByCategoryForConferenceViewModel);
        }


        public IActionResult ViewSubmissionsForConference(int conferenceId)
        {


            var model = new SubmissionsForConferenceViewModel
            {
                Conference = _conferenceService.GetConference(conferenceId),
                ConferenceSubmissions = _submissionService.GetSubmissionsForConference(conferenceId)

            };


            return View(model);

        }

        public IActionResult ViewOnlineSubmissionsForConference(int conferenceId)
        {


            var model = new SubmissionsForConferenceViewModel
            {
                Conference = _conferenceService.GetConference(conferenceId),
                ConferenceSubmissions = _submissionService.GetAcceptedOnlineSubmissionsForConference(conferenceId)

            };


            return View(model);

        }

        public IActionResult ViewSubmissionsForConferenceAllPresenters(int conferenceId)
        {


            var model = new SubmissionsForConferenceViewModel
            {
                Conference = _conferenceService.GetConference(conferenceId),
                ConferenceSubmissions = _submissionService.GetSubmissionsForConferenceWithAllParticipants(conferenceId)

            };


            return View(model);

        }

        public IActionResult ViewAllPresentersForConference(int conferenceId)
        {


            var model = new SubmissionsForConferenceViewModel
            {
                Conference = _conferenceService.GetConference(conferenceId),
                ConferenceSubmissions = _submissionService.GetSubmissionsForConferenceWithAllParticipants(conferenceId)

            };


            return View(model);

        }


        /// <summary>
        /// Change the Submission Status of the submission based on value provided by the user
        /// </summary>
        /// <param name="submissionId">Submssion identifier</param>
        /// <param name="submissionStatusType">Submission Status type</param>
        /// <returns>Submission Admin View Submission Page</returns>
        public RedirectToActionResult ChangeSubmissionStatus(int submissionId, SubmissionStatusType submissionStatusType)
        {
            _submissionService.UpdateSubmissionStatus(submissionId, _submissionStatusService.GetBySubmissionStatusType(submissionStatusType).SubmissionStatusId);

            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} has changed the status of submission id of {submissionId} to {submissionStatusType}.");

            
            if (submissionStatusType == SubmissionStatusType.ACCEPTED)
            {
                //When changing submission status to accepted need to add any associated presentation methods
                //to the submission
                _submissionService.AddSubmissionPresentationsToSubmission(submissionId);

            }

            //If submissionStatusType is != ACCEPTED need to remove any 
            //SubmissionPresentationMethods associated with the submission
            if (submissionStatusType != SubmissionStatusType.ACCEPTED)
            {

                _submissionService.RemoveSubmissionPresentationMethods(submissionId);

            }

            return RedirectToAction("ViewSubmission", new { submissionId = submissionId });
        }

        /// <summary>
        /// Process admin user request to withdraw submission from presentation at the virtual
        /// conference supplement.
        /// </summary>
        /// <param name="SubmissionId"></param>
        /// <returns>Redirects back to home page</returns>
        public RedirectToActionResult ChangeSubmissionVirtualStatus(int submissionId, PresentationStatusType presentationStatusType)
        {
            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} has changed submission id of {submissionId} virtual conference supplement presentation status to {presentationStatusType}");

            _submissionService.UpdateSubmissionPresentationMethodStatus(submissionId, PresentationMethodType.RECORDED_ONLINE, presentationStatusType);

            return RedirectToAction("ViewSubmission", new { submissionId = submissionId });
        }

        ///<summary>
        ///First step in admin user creating new submission.  Provides a collection
        ///of Conferences so admin user can then select a specific Conference
        ///on the view page.
        ///</summary>
        ///<returns>AdminCreateSubmission Home Page</returns>
        public IActionResult AdminCreateSubmissionSelectConference() { 



            List<ConferenceViewModel> activeConferenceViewModels = _conferenceService.GetConferences();

            List<SelectListItem> conferenceItems = activeConferenceViewModels.Select(c => new SelectListItem
            {
                Value = c.ConferenceId.ToString(),
                Text = c.ConferenceShortName

            }).ToList();

            ConferenceBindingModel conferenceBindingModel = new ConferenceBindingModel()
            {
                ConferenceItems = conferenceItems

            };


            return View(conferenceBindingModel);


        }

        ///<summary>
        ///First step in admin user editing multiple submissions.  Provides a collection
        ///of Conferences so admin user can then select a specific Conference
        ///on the view page.
        ///</summary>
        ///<returns>AdminCreateSubmission Home Page</returns>
        public IActionResult AdminEditSubmissionsSelectConference()
        {

            List<ConferenceViewModel> activeConferenceViewModels = _conferenceService.GetConferences();

            List<SelectListItem> conferenceItems = activeConferenceViewModels.Select(c => new SelectListItem
            {
                Value = c.ConferenceId.ToString(),
                Text = c.ConferenceShortName

            }).ToList();

            ConferenceBindingModel conferenceBindingModel = new ConferenceBindingModel()
            {
                ConferenceItems = conferenceItems

            };

            return View(conferenceBindingModel);


        }
        
        public IActionResult AdminEditSubmissionsSelectCategory(int conferenceId)
        {
            Conference conference = _conferenceService.GetConference(conferenceId);

            SubmissionCategoryBindingModel submissionCategoryBindingModel = new SubmissionCategoryBindingModel
            {
                ConferenceId = conferenceId,

                ConferenceName = conference.ConferenceLongName,

                SubmissionCategoryItems = GetSubmissionCategoryItems(conferenceId),

                SubmissionPaymentRequirementText = conference.SubmissionPaymentRequirementText,

                CategoryDetailUrl = conference.CategoryDetailUrl

            };

            var user = _userMapper.Map(User.Claims);

            /*
             * If the there are submission categories that require payment and the
             * user membership status means this user must pay to submit to those
             * submission categories we need to notify user on the view page
             */
            submissionCategoryBindingModel.DisplaySubmissionRequirementText =
                _submissionService.DisplayPaymentRequirementText(submissionCategoryBindingModel.SubmissionCategoryItems, user.STFMUserId);

            return View(submissionCategoryBindingModel);



        }

        /// <summary>
        /// Gets the submission category items for the provided conference id.
        /// </summary>
        /// <returns>The submission category items.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        private List<SelectListItem> GetSubmissionCategoryItems(int conferenceId)
        {

            List<SubmissionCategoryViewModel> submissionCategoryViewModelList = _SubmissionCategoryService.GetActiveSubmissionCategories(conferenceId);

            List<SelectListItem> submissionCategoryItems = submissionCategoryViewModelList.Select(sc => new SelectListItem
            {
                Value = sc.SubmissionCategoryId.ToString(),
                Text = sc.SubmissionCategoryName

            }).ToList();

            return submissionCategoryItems;

        }

        /// <summary>
        /// Return a view with all the accepted submissions in the provided acceptedCategory Id.
        /// </summary>
        /// <param name="SelectedSubmissionCategory"></param>
        /// <returns></returns>
        public async Task<IActionResult> AdminEditAcceptedSubmissions(int SelectedSubmissionCategory, int? pageNumber)
        {

            int pageSize = 5;
            return View(await PaginatedList<SubmissionUpdate>.CreateAsync(_submissionService.GetAcceptedSubmissionsForCategory(SelectedSubmissionCategory).AsQueryable(), pageNumber ?? 1, pageSize));

        }

        /// <summary>
        /// Return a view with all the accepted submissions in the provided acceptedCategory Id starting with the provided submission id.
        /// </summary>
        /// <param name="SelectedSubmissionCategory"></param>
        /// <returns></returns>
        public async Task<IActionResult> AdminEditAcceptedSubmissionsGoToSubmission(int SelectedSubmissionCategory, int submissionId)
        {

            // Get the accepted submissions in the selectedSubmissionCategory
            // Determine in which set of 5 submissions the provided submissionId is in to get the pageNumber value to return with the view

            List<SubmissionUpdate> acceptedSubmissions = _submissionService.GetAcceptedSubmissionsForCategory(SelectedSubmissionCategory).ToList<SubmissionUpdate>();

            int pageNumber = 1;

            int countSubmissions = 1;

            foreach (SubmissionUpdate submissionUpdate in acceptedSubmissions )
            {

                if (submissionId == submissionUpdate.SubmissionId)
                {
                    break;
                }

                if (countSubmissions % 5 == 0)
                {
                    pageNumber++;
                }

                countSubmissions++;

            }

            int pageSize = 5;
            return View("AdminEditAcceptedSubmissions", await PaginatedList<SubmissionUpdate>.CreateAsync(_submissionService.GetAcceptedSubmissionsForCategory(SelectedSubmissionCategory).AsQueryable(), pageNumber, pageSize));

        }

        /// <summary>
        /// Return a view with all the accepted submissions in the provided acceptedCategory Id starting with the provided submission id.
        /// </summary>
        /// <param name="SelectedSubmissionCategory"></param>
        /// <returns></returns>
        public RedirectToActionResult AdminEditAcceptedSubmissionsGoToLastEdited(int SelectedSubmissionCategory)
        {

            // Get the accepted submissions in the selectedSubmissionCategory
            // Determine in which set of 5 submissions the provided is the last edited submission to get the pageNumber value to return with the view

            List<SubmissionUpdate> acceptedSubmissions = _submissionService.GetAcceptedSubmissionsForCategory(SelectedSubmissionCategory).ToList<SubmissionUpdate>();

            SubmissionUpdate submissionUdpate = acceptedSubmissions.FindLast(su => su.SubmissionAdminEdited == true);

            return RedirectToAction("AdminEditAcceptedSubmissionsGoToSubmission", new { SelectedSubmissionCategory, submissionUdpate.SubmissionId });
        }




        /// <summary>
        /// Change the Accepted Category of the submission based on value provided by the user
        /// </summary>
        /// <param name="submissionId">Submssion identifier</param>
        /// <param name="acceptedCategoryId">Accepted category ID</param>
        /// <returns>Submission Admin View Submission Page</returns>
        public RedirectToActionResult ChangeSubmissionAcceptedCategory(int submissionId, int acceptedCategoryId)
        {
            _submissionService.UpdateSubmissionAcceptedCategory(submissionId, acceptedCategoryId);


            return RedirectToAction("ViewSubmission", new { submissionId });

        }

        public IActionResult RemoveParticipantRole(int submissionParticipantToParticipantRoleId, int submissionId)
        {
            _submissionParticipantService.RemoveParticipantRole(submissionParticipantToParticipantRoleId);
            return RedirectToAction("ViewSubmission", "SubmissionAdmin", new { submissionId = submissionId });
        }

        public IActionResult AddParticipantRole(ParticipantRoleType participantRoleType, int submissionParticipantId, int submissionId)
        {
            //ParticipantRoleType is an enum where each value is an integer that is one less than the corresponding participant role Id
            //in the database. So, we add one to the ParticipantRoleType to get the database Id
            var participantRoleId = (int)participantRoleType + 1;

            _submissionParticipantService.AddSubmissionParticipantToParticipantRole(submissionParticipantId, participantRoleId);
            return RedirectToAction("ViewSubmission", "SubmissionAdmin", new { submissionId = submissionId });
        }

        public RedirectToActionResult RemoveSubmissionParticipant(int submissionParticipantId, int submissionId)
        {

            Log.Information($"Request made to remove submission participant with id of {submissionParticipantId} from submission id of {submissionId}");

            _submissionParticipantService.DeleteSubmissionParticipant(submissionParticipantId);

            return RedirectToAction("ViewSubmission", "SubmissionAdmin", new { submissionId = submissionId });

        }

        /// <summary>
        /// Responds to initial request to reorder the presenters
        /// for a specific submission.
        /// </summary>
        /// <returns>A instance of the UpdatePresenterOrderViewModel</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public ActionResult UpdatePresenterOrder(int submissionId)
        {
            Log.Information($"Request made to update presenter order for submission id {submissionId}");

            var submission = _submissionService.GetSubmission(submissionId);
            var model = _submissionParticipantService.GetUpdatePresenterOrderViewModel(submission);

            return View(model);
        }

        /// <summary>
        /// Responds to the request to reorder the presenters (in the database) for a specific submission
        /// </summary>
        /// <returns>A instance of the UpdatePresenterOrderViewModel</returns>
        ///<param name="model">Update presenter order view model</param>
        [HttpPost]
        public IActionResult UpdatePresenterOrder(UpdatePresenterOrderViewModel model)
        {
            Log.Information($"User has reordered presenters and is requesting this new presenter order to be updated in the database for submission id {model.SubmissionId}.");
            if (ModelState.IsValid)
            {
                _submissionParticipantService.UpdatePresenterOrder(model);
                _submissionService.SetParticipantOrderModifiedToTrue(model.SubmissionId);
                return RedirectToAction("ViewSubmission", new { submissionId = model.SubmissionId });
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult SubmissionsWithParticipantOrderModified(int ConferenceId)
        {
            var model = _submissionService.GetSubmissionsWhoseParticipantOrderHasBeenModified(ConferenceId);

            return View(model);
        }

        /// <summary>
        /// Retire the conference identified by the conference ID provided.
        /// </summary>
        /// <param name="conferenceId">conference ID</param>
        /// <returns>Redirects to SubmissionAdmin home page</returns>
        public RedirectToActionResult RetireConference(int conferenceId)
        {
            _conferenceService.RetireConference(conferenceId);

            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} has retired conference with id of {conferenceId}");

            return RedirectToAction("Index", "SubmissionAdmin");
        }

        /// <summary>
        /// Create the next conference and its submission categories using data from the conference 
        /// identified by the provided conference Id
        /// </summary>
        /// <param name="conferenceId">Conference Id</param>
        /// <returns>Redirects to SubmissionAdmin home page after creating new conference</returns>
        public RedirectToActionResult CreateNextConference(int conferenceId)
        {
            int nextConferenceId = _conferenceService.CreateNextConference(conferenceId);

            _conferenceService.CreateProposalForms(conferenceId, nextConferenceId);

            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} has created next conference with id of {nextConferenceId}");

            return RedirectToAction("Index", "SubmissionAdmin");
        }

        /// <summary>
        /// Create the review forms for the conference identified by the provided conference ID.  
        /// Finds the previous conference and uses that conference's submissionc category review
        /// forms as the basis for the new review forms.
        /// </summary>
        /// <param name="conferenceId">Conference Id</param>
        /// <returns>Redirects to SubmissionAdmin home page after creating new conference</returns>
        public RedirectToActionResult CreateReviewForms(int conferenceId)
        {
           _conferenceService.CreateReviewForms(conferenceId);

            var user = _userMapper.Map(User.Claims);

            Log.Information($"{user.STFMUserId} has review forms form conference id of {conferenceId}");

            return RedirectToAction("Index", "SubmissionAdmin");
        }


        /// <summary>
        /// Find people by last name in order to find submissions they are participants on.
        /// </summary>
        /// <returns>view page showing all people from SalesForce with
        /// matching last name.</returns>
        public ActionResult FindPeopleByLastName(string lastName)
        {

            List<User> people = _submissionParticipantService.FindPeople(lastName);

            return View(people);
        }

        public ActionResult ViewSubmissionsForPerson(string stfmUserId)
        {
            List<SubmissionSummaryViewModel> submissions = _submissionService.GetSubmissions(stfmUserId);

            User user = _salesforceAPIService.GetUserInfo(stfmUserId).Result;

            SubmissionsForPersonViewModel submissionsForPersonViewModel = new SubmissionsForPersonViewModel
            {
                SubmissionSummaryViewModels = submissions,
                User = user
            };


            return View(submissionsForPersonViewModel);

        }

        /// <summary>
        /// For all submission participants and submission reviewers
        /// find any where the StfmUserId is no longer associated
        /// with a SalesForce record.
        /// </summary>
        /// <returns></returns>
        public ActionResult FindMissingStfmUserIds()
        {
            //Get all the participants

            List<Participant> participants = _participantService.GetParticipants();

            //Get all the reviewers

            List<Reviewer> reviewers = _reviewerService.GetReviewers();

            //Create a HashSet<string> of participant StfmUserIds

            HashSet<string> participantStfmUserIds = new HashSet<string>(participants.Select(s => s.StfmUserId));

            //Create a HashSet<string> of reviewer StfmUserIds

            HashSet<string> reviewerStfmUserIds = new HashSet<string>(reviewers.Select(s => s.StfmUserId));

            //Create a union of the two sets to create a single HashSet<string> of StfmUserIds

            HashSet<string> stfmUserIds = new HashSet<string>(participantStfmUserIds);
            stfmUserIds.UnionWith(reviewerStfmUserIds);

            //Find all SalesForce records associated with the above set of StfmUserIds - collection of User objects
   
            IEnumerable<User> users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result;

            List<User> userList = users.ToList();

            //Find those StfmUserIds that are not in the Collection of User objects
            List<StfmUserIdMissingFromSalesforce> stfmUserIdsMissingFromSalesforce = new List<StfmUserIdMissingFromSalesforce>();

            foreach(string stfmUserId in stfmUserIds)
            {

                if ( ! userList.Exists( u => u.STFMUserId.Equals(stfmUserId, StringComparison.InvariantCultureIgnoreCase) ) ) {

                    StfmUserIdMissingFromSalesforce stfmUserIdMissingFromSalesforce = new StfmUserIdMissingFromSalesforce
                    {
                        StfmUserId = stfmUserId,

                         ParticipantSubmissionIds = _submissionService.GetSubmissions(stfmUserId).Select( s => s.SubmissionId).ToList(),

                         ReviewerSubmissionIds = _reviewerService.GetAssignedSubmissions(stfmUserId).Select(s => s.SubmissionId).ToList()

                    };

                    stfmUserIdsMissingFromSalesforce.Add(stfmUserIdMissingFromSalesforce);
                }

            }


            StfmUserIdMissingViewModel stfmUserIdMissingViewModel = new StfmUserIdMissingViewModel
            {
                 StfmUserIds = stfmUserIdsMissingFromSalesforce

            };


            //Return view with collection of StfmUserIds that are not found in SalesForce

            return View(stfmUserIdMissingViewModel);
        }

        /// <summary>
        /// Go through all the video file names 
        /// for a conference and rename those that
        /// DO NOT meet file name requirements.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns>View Page</returns>
        public IActionResult ListVideoFiles(int conferenceId, int startFile, int endFile)
        {

            List<VirtualMaterial> virtualMaterials = _virtualMaterialService.GetVideoFileRecords(conferenceId).ToList();

            List<VirtualMaterial> virtualMaterialsSorted = virtualMaterials.OrderBy(vm => vm.SubmissionId).ToList();

            List<VirtualMaterialViewModel> virtualMaterialViewModels = new List<VirtualMaterialViewModel>();

            string folderName = "Annual2020Presentations";

            if (conferenceId == 18)
            {
                folderName = "CPQI2020Presentations";
            }


            foreach (VirtualMaterial virtualMaterial in virtualMaterialsSorted.GetRange(startFile, endFile) )
            {

                Log.Information("Getting dropbox File for " + virtualMaterial.MaterialValue);
                Metadata dropboxFile = GetDropboxFile(virtualMaterial.MaterialValue, folderName).Result;

                Log.Information("Got dropbox file " + dropboxFile.Name);

                VirtualMaterialViewModel virtualMaterialViewModel = new VirtualMaterialViewModel
                {
                    VirtualMaterial = virtualMaterial,
                    DropboxFile = dropboxFile
                };

                virtualMaterialViewModels.Add(virtualMaterialViewModel);

            }

            var model = new VirtualMaterialsForConference()
            {
                Conference = _conferenceService.GetConference(conferenceId),
                VirtualMaterialViewModels = virtualMaterialViewModels,
                startFile = startFile,
                endFile = endFile

            };

            return View(model);

        }


        private async Task<Metadata> GetDropboxFile(string videoFilename, string folderName)
        {

            try
            {

                using (var dbx = new DropboxClient("U5mw0KN7ybAAAAAAAAAF9nwXzKM6roPi8j96aNa7Zs3-VvE-K7HTOi4AcW6irg1s"))
                {

                    var list = await dbx.Files.ListFolderAsync("/File Requests/" + folderName + "/");

                    bool fileFound = false;

                    foreach (var item in list.Entries.Where(i => i.IsFile))
                    {
                        if (item.Name.Contains(videoFilename, StringComparison.InvariantCultureIgnoreCase))
                        {
                            fileFound = true;
                            return item;
                        }
                    }

                    foreach (var item in list.Entries.Where(i => i.IsFolder))
                    {
                        var folderFiles = await dbx.Files.ListFolderAsync("/File Requests/" + folderName + "/" + item.Name + "/");

                        foreach (var itemFile in folderFiles.Entries.Where(i => i.IsFile))
                        {
                            if (itemFile.Name.Contains(videoFilename, StringComparison.InvariantCultureIgnoreCase))
                            {
                                fileFound = true;
                                return itemFile;
                            }
                        }

                    }

                    return null;
                }

            }
            catch (ApiException<GetMetadataError> ex)
            {
                Log.Information($"Video with filename of  {videoFilename} was NOT found in our Dropbox folder - {ex.Message}");
                return null;
            }
        }


        [HttpPost]
        public ActionResult ToggleConferenceInactive(int conferenceId)
        {
            var conferenceInactive = _conferenceService.ToggleConferenceInactive(conferenceId);
            return PartialView("ConferenceInactivePartial", new ConferenceViewModel { ConferenceId = conferenceId, ConferenceInactive = conferenceInactive });
        }

        [HttpPost]
        public ActionResult EditCFPEndDate(int conferenceId, DateTime endDate)
        {
            var savedEndDate = _conferenceService.EditCFPEndDate(conferenceId, endDate);
            return PartialView("ConferenceCFPEndDateEditForm", new ConferenceDeadlineViewModel { ConferenceId = conferenceId, DeadlineDate = savedEndDate });
        }

        [HttpPost]
        public ActionResult EditSubmissionReviewEndDate(int conferenceId, DateTime endDate)
        {
            var savedEndDate = _conferenceService.EditSubmissionReviewEndDate(conferenceId, endDate);
            return PartialView("ConferenceSubmissionReviewEndDateEditForm", new ConferenceDeadlineViewModel { ConferenceId = conferenceId, DeadlineDate = savedEndDate });
        }

        public ActionResult ParticipantsAddedOrRemovedReport()
        {
            var model = new ParticipantsAddedOrRemovedReportViewModel
            {
                ConferencesByNameAndId = 
                    _conferenceService.GetConferences()
                        .Select(c => new EntityByNameAndId
                        {
                            EntityId = c.ConferenceId,
                            EntityName = c.ConferenceLongName
                        }).ToList()
            };

            return View(model);
        }

        [HttpGet]
        public JsonResult ParticipantsAddedOrRemovedReportData(int conferenceId, DateTime asOfDate)
        {
            var conference = _conferenceService.GetConference(conferenceId);

            if (conference == null)
            {
                Response.StatusCode = 400;
                return Json(new { });
            }

            var submissionsWithParticipantsAddedOrRemoved = _submissionParticipantService.GetSubmissionsWithParticipantsAddedOrRemoved(conferenceId, asOfDate);

            var model = new ParticipantsAddedOrRemovedReportDataViewModel
            {
                ConferenceName = conference.ConferenceLongName,
                AsOfDate = asOfDate,
                SubmissionsWithParticipantsAddedOrRemoved = submissionsWithParticipantsAddedOrRemoved
            };

            return Json(model);
        }

        [HttpGet]
        public IActionResult FormPreview()
        {
            var model = _conferenceService.GetConferences()
                .OrderByDescending(c => c.ConferenceCFPEndDate)
                .Select(c => new EntityByNameAndId
                {
                    EntityName  = c.ConferenceShortName,
                    EntityId    = c.ConferenceId
                });

            return View(model);
        }

        [HttpGet]
        public IActionResult GetConferenceCategories(int conferenceId)
        {
            var model = _SubmissionCategoryService.GetActiveSubmissionCategories(conferenceId)
                .Select(c => new EntityByNameAndId {
                    EntityName = c.SubmissionCategoryName,
                    EntityId = c.SubmissionCategoryId
                });

            return PartialView("ConferenceCategorySelect", model);
        }

        [HttpGet]
        public IActionResult GetPreviewForm(int conferenceId, int categoryId, FormPreviewMode previewMode)
        {
            var model = _formPreviewService.GetFormPreviewGeneratorViewModel(conferenceId, categoryId, previewMode);

            return PartialView("FormPreviewGenerator", model);
        }


        public IActionResult ViewVirtualConferenceData(int conferenceId)
        {

            Conference conference = _conferenceService.GetConference(conferenceId);

            VirtualConferenceData virtualConferenceData = _virtualConferenenceDataService.GetVirtualConferenceData(conferenceId);

            VirtualConferenceViewModel virtualConferenceViewModel = new VirtualConferenceViewModel()
            {

                Conference = conference,

                VirtualConferenceData = virtualConferenceData

            };

            return View(virtualConferenceViewModel);

        }

        [HttpGet]
        public IActionResult RegistrationReports()
        {
            return View();
        }

        [HttpGet]
        public IActionResult LoadConferencesForRegistrationReports(int year)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var model = new RegistrationReportsConferenceSelectorViewModel
            {
                SalesforceEvents = _salesforceAPIService.GetEventsByDateRange(startDate, endDate).Result.ToList(),
                Conferences = _conferenceService.GetConferencesByDateRange(startDate, endDate).ToList()
            };

            return PartialView("RegistrationReportsConferenceSelector", model);
        }

        [HttpGet]
        public IActionResult SessionsWithNoRegistrationsReport(int conferenceId, string eventId, string eventName)
        {
            var model = _submissionService.GetSessionsWithNoRegistrationsReportViewModel(conferenceId, eventId, eventName);
            return View(model);
        }

        [HttpGet]
        public IActionResult AllSessionsWithPresentersThatHaveNotRegisteredReport(int conferenceId, string eventId, string eventName)
        {
            var model = _submissionService.GetAllSessionsWithPresentersThatHaveNotRegisteredReportViewModel(conferenceId, eventId, eventName);
            return View(model);
        }

        [HttpGet]
        public IActionResult AllSessionsPresentersRegisteredUnregisteredReport(int conferenceId, string eventId, string eventName)
        {
            var model = _submissionService.GetAllSessionsWithPresentersRegisteredUnregistered(conferenceId, eventId, eventName);
            return View(model);
        }


        /// <summary>
        /// Responds to admin user's request to view all disclosures for a specific
        /// submission.
        /// </summary>
        /// <returns>View for a specific submission's disclosures</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public IActionResult ViewSubmissionDisclosures(int submissionId)
        {
            if (!_submissionService.SubmissionExists(submissionId))
            {
                return RedirectToAction("ContentNotFound", "Home", new { EntityType = "Submission", EntityId = submissionId, ReturnController = "SubmissionAdmin", ReturnAction = "Index" });
            }

            Submission submission = _submissionService.GetSubmission(submissionId);

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel =  _submissionService.GetSubmissionWithProposal(submissionId);

            List<Disclosure> disclosures = _disclosureService.GetDisclosures(submission);

            SubmissionWithProposalAndDisclosuresViewModel submissionWithProposalAndDisclosuresViewModel = new SubmissionWithProposalAndDisclosuresViewModel
            {
                disclosures = disclosures,
                submissionWithProposalAnswersViewModel = submissionWithProposalAnswersViewModel
            };

            return View(submissionWithProposalAndDisclosuresViewModel);

        }

        [HttpPost]
        public IActionResult ClearConferenceSessionsSearchCache(int conferenceId)
        {
            try
            {
                _cacheService.ClearConferenceSessionsSearchCache(conferenceId);
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Error($"The system errored when attempting to clear the Conference Session Search Cache for conference Id = {conferenceId}.", ex);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }

}
