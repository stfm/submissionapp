﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface IDisclosureRepository
    {
        /// <summary>
        /// Gets the disclosure using the provided disclosure ID.
        /// </summary>
        /// <returns>The disclosure.</returns>
        /// <param name="disclosureId">Disclosure identifier.</param>
        Disclosure GetDisclosure(int disclosureId);

        /// <summary>
        /// Gets the disclosure using the provides STFM User ID.
        /// </summary>
        /// <returns>The disclosure.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        Disclosure GetDisclosure(string stfmUserId);

        /// <summary>
        /// Adds the disclosure.
        /// </summary>
        /// <returns>The disclosure id value</returns>
        /// <param name="disclosure">Disclosure.</param>
        int AddDisclosure(Disclosure disclosure);

        /// <summary>
        /// Update an existing disclosure.
        /// </summary>
        /// <param name="disclosure">Disclosure.</param>
        void UpdateDisclosure(Disclosure disclosure);
    }
}
