﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.Models;
using ConferenceSubmission.BindingModels;

namespace ConferenceSubmission.ViewModels
{
    public class EditMultipleSubmissionsViewModel
    {
        public Conference Conference { get; set; }

        List<EditAcceptedSubmissionBindingModel> AcceptedSubmissions { get; set; }
    }
}
