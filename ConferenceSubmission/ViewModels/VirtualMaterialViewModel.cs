﻿using System;
using ConferenceSubmission.Models;
using Dropbox.Api.Files;

namespace ConferenceSubmission.ViewModels
{
    public class VirtualMaterialViewModel
    {
        public VirtualMaterial VirtualMaterial { get; set; }

        public Metadata DropboxFile { get; set; }

        public VirtualMaterialViewModel()
        {
        }
    }
}
