﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class ReviewFormFieldRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public ReviewFormFieldRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;

        }

        [Fact]
        public void GetRatingScaleReviewFormFieldForMultipleReviews()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ReviewFormFieldRepository reviewFormFieldRepository = new ReviewFormFieldRepository(context);

                List<int> reviewIds = new List<int> { 100, 101 };

                IEnumerable<ReviewFormField> reviewFormFields = reviewFormFieldRepository.GetRatingScaleFormFieldsForMultipleReviews(reviewIds);


                foreach (ReviewFormField reviewFormField in reviewFormFields)
                {

                    Output.WriteLine("Form field type is " + reviewFormField.FormField.FormFieldType);

                    foreach (var formFieldProperty in reviewFormField.FormField.FormFieldProperties)
                    {

                        Output.WriteLine("For field property name is " + formFieldProperty.PropertyName + " and field property value is " + formFieldProperty.PropertyValue);


                    }


                    Output.WriteLine("form field id is " + reviewFormField.FormFieldId);

                    Output.WriteLine("Form field sort order is " + reviewFormField.FormField.FormFieldSortOrder);

                    Output.WriteLine("Review form field value is " + reviewFormField.ReviewFormFieldValue);


                }

                Assert.True(reviewFormFields.Count() > 0);

            };
        }

        [Fact]
        public void GetAllReviewFormFieldForMultipleReviews()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ReviewFormFieldRepository reviewFormFieldRepository = new ReviewFormFieldRepository(context);

                List<int> reviewIds = new List<int> { 100, 101 };

                IEnumerable<ReviewFormField> reviewFormFields = reviewFormFieldRepository.GetAllFormFieldsForMultipleReviews(reviewIds);


                foreach (ReviewFormField reviewFormField in reviewFormFields)
                {

                    Output.WriteLine("Form field type is " + reviewFormField.FormField.FormFieldType);

                    foreach (var formFieldProperty in reviewFormField.FormField.FormFieldProperties)
                    {

                        Output.WriteLine("For field property name is " + formFieldProperty.PropertyName + " and field property value is " + formFieldProperty.PropertyValue);


                    }


                    Output.WriteLine("form field id is " + reviewFormField.FormFieldId);

                    Output.WriteLine("Form field sort order is " + reviewFormField.FormField.FormFieldSortOrder);

                    Output.WriteLine("Review form field value is " + reviewFormField.ReviewFormFieldValue);


                }

                Assert.True(reviewFormFields.Count() > 0);

            };
        }

        [Fact]
        public void AddReviewFormFieldTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ReviewFormFieldRepository reviewFormFieldRepository = new ReviewFormFieldRepository(context);

                ReviewFormField reviewFormField = new ReviewFormField
                {

                    FormFieldId = 3,
                    ReviewFormFieldValue = "My reviewer comments for reviewId 101",
                    ReviewId = 101


                };

                reviewFormFieldRepository.AddReviewFormField(reviewFormField);

            }

            using (var context = new AppDbContext(options))
            {
                ReviewFormFieldRepository reviewFormFieldRepository = new ReviewFormFieldRepository(context);


                IEnumerable<ReviewFormField> reviewFormFields = reviewFormFieldRepository.GetFormFieldsForReview(101);

                foreach( ReviewFormField reviewFormField in reviewFormFields) {

                    Output.WriteLine("Form field id is " + reviewFormField.FormFieldId);

                    Output.WriteLine("Form field answer is " + reviewFormField.ReviewFormFieldValue);


                }

                IEnumerable<ReviewFormField> reviewFormFields1 = reviewFormFieldRepository.GetFormFieldsForReview(100);

                foreach (ReviewFormField reviewFormField1 in reviewFormFields1)
                {

                    Output.WriteLine("Form field type is " + reviewFormField1.FormField.FormFieldType);

                    foreach (var formFieldProperty in reviewFormField1.FormField.FormFieldProperties)
                    {

                        Output.WriteLine("For field property name is " + formFieldProperty.PropertyName + " and field property value is " + formFieldProperty.PropertyValue);


                    }


                    Output.WriteLine("form field id is " + reviewFormField1.FormFieldId);

                    Output.WriteLine("Form field sort order is " + reviewFormField1.FormField.FormFieldSortOrder);

                    Output.WriteLine("Review form field value is " + reviewFormField1.ReviewFormFieldValue);


                }

                Assert.Equal(4, reviewFormFields1.Count());
            }
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceCFPEndDate = DateTime.Parse("Nov 11, 2018"),
                        ConferenceSubmissionReviewEndDate = DateTime.Parse("Dec 11, 2018"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceCFPEndDate = DateTime.Parse("Dec 11, 2018"),
                        ConferenceSubmissionReviewEndDate = DateTime.Parse("Dec 30, 2018"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }

               );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {

                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title",

                    }

                );

                context.SaveChanges();

                ConferenceSession aConferenceSession = new ConferenceSession { SessionCode = "S01", SessionLocation = "Green Room", SessionStartDateTime = DateTime.Parse(" Apr 10, 2018") };

                SubmissionPayment submissionPayment = new SubmissionPayment { PaymentAmount = 25, PaymentDateTime = DateTime.Parse(" Apr 10, 2018"), SubmissionId = 10001, PaymentTransactionId = "P999" };


                Submission aSubmission = context.Submissions.Find(10001);

                aSubmission.ConferenceSession = aConferenceSession;

                aSubmission.SubmissionPayment = submissionPayment;

                context.Update(aSubmission);

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }


                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(

                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10001 }
                );


                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 2 }


                );

                context.SaveChanges();

                context.FormFields.AddRange(

                    new FormField
                    {

                        FormFieldId = 2,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "screentext",
                        FormFieldSortOrder = 1,
                        FormFieldRole = "review",
                        AnswerRequired = false


                    },


                    new FormField
                    {

                        FormFieldId = 3,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "textarea",
                        FormFieldSortOrder = 2,
                        FormFieldRole = "review",
                        AnswerRequired = true

                    },

                    new FormField
                    {

                        FormFieldId = 1,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "select",
                        FormFieldSortOrder = 3,
                        FormFieldRole = "review",
                        AnswerRequired = true


                    },

                    new FormField
                    {

                        FormFieldId = 4,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "text",
                        FormFieldSortOrder = 4,
                        FormFieldRole = "review",
                        AnswerRequired = true

                    },

                    new FormField
                    {

                        FormFieldId = 5,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "ratingscale",
                        FormFieldSortOrder = 5,
                        FormFieldRole = "review",
                        AnswerRequired = true

                    }



                );

                context.SaveChanges();

                context.FormFieldProperties.AddRange(

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 1,
                        FormFieldId = 2,
                        PropertyName = "textvalue",
                        PropertyValue = "Reviewer Comments"
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 2,
                        FormFieldId = 3,
                        PropertyName = "rows",
                        PropertyValue = "5"
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 3,
                        FormFieldId = 3,
                        PropertyName = "cols",
                        PropertyValue = "50"
                    },

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 4,
                        FormFieldId = 1,
                        PropertyName = "options",
                        PropertyValue = "YES,NO"
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 5,
                        FormFieldId = 1,
                        PropertyName = "label",
                        PropertyValue = "Should this submission be accepted?"
                    },

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 6,
                        FormFieldId = 4,
                        PropertyName = "label",
                        PropertyValue = "Should this submission be accepted in a different category?"
                    },

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 7,
                        FormFieldId = 5,
                        PropertyName = "label",
                        PropertyValue = "Is the abstract well written?"
                    },

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 8,
                        FormFieldId = 5,
                        PropertyName = "options",
                        PropertyValue = "1,2,3,4,5"
                    }




                );

                context.SaveChanges();

                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 1, ReviewStatusType = ReviewStatusType.NOT_STARTED });
                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 2, ReviewStatusType = ReviewStatusType.INCOMPLETE });
                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 3, ReviewStatusType = ReviewStatusType.COMPLETE });


                context.SaveChanges();

                context.Reviewers.AddRange(

                    new Reviewer { ReviewerId = 4001, StfmUserId = "3001" },

                    new Reviewer { ReviewerId = 4002, StfmUserId = "3002" },

                    new Reviewer { ReviewerId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();

                //Create some SubmissionReviewer records to assign reviewers to submissions

                context.SubmissionReviewer.AddRange(

                    new SubmissionReviewer { SubmissionReviewerId = 1, SubmissionId = 10001, ReviewerId = 4001 },

                    new SubmissionReviewer { SubmissionReviewerId = 2, SubmissionId = 10001, ReviewerId = 4002 }

                );

                context.SaveChanges();

                context.Reviews.AddRange(


                    new Review { ReviewId = 100, ReviewDateTime = DateTime.Now, ReviewerId = 4001, SubmissionId = 10001, ReviewStatusId = 2},

                    new Review { ReviewId = 101, ReviewDateTime = DateTime.Now, ReviewerId = 4002, SubmissionId = 10001, ReviewStatusId = 2}

                );

                context.SaveChanges();

                context.ReviewFormFields.AddRange(


                    new ReviewFormField
                    {

                        ReviewFormFieldId = 1,
                        FormFieldId = 3,
                        ReviewFormFieldValue = "My reviewer comment for reviewId 100",
                        ReviewId = 100


                    },

                    new ReviewFormField
                    {

                        ReviewFormFieldId = 2,
                        FormFieldId = 1,
                        ReviewFormFieldValue = "YES",
                        ReviewId = 100


                    },

                    new ReviewFormField
                    {

                        ReviewFormFieldId = 3,
                        FormFieldId = 4,
                        ReviewFormFieldValue = "Do not move",
                        ReviewId = 100


                    },

                    new ReviewFormField
                    {

                        ReviewFormFieldId = 4,
                        FormFieldId = 5,
                        ReviewFormFieldValue = "3",
                        ReviewId = 100


                    }



                );

                context.SaveChanges();


            }



            return options;
        }
    }
}
