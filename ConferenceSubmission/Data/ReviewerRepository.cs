﻿using System;
using ConferenceSubmission.Models;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using TimeZoneConverter;
using ConferenceSubmission.DTOs.Review;
using System.Globalization;

namespace ConferenceSubmission.Data
{
    public class ReviewerRepository : IReviewerRepository
    {

        private readonly AppDbContext _appDbContext;

        public IEnumerable<Reviewer> GetReviewers => _appDbContext.Reviewers;

        public ReviewerRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }



        public int CreateReviewer(Reviewer reviewer)
        {
            _appDbContext.Reviewers.Add(reviewer);

            _appDbContext.SaveChanges();

            return reviewer.ReviewerId;
        }

        public List<SubmissionReviewer> GetAssignedReviews(int conferenceId)
        {
            return _appDbContext.SubmissionReviewer
                .Include(s => s.Reviewer)
                .Include(s => s.Submission)
                .ThenInclude(s => s.Conference)
                .Where(s => s.Submission.Conference.ConferenceId == conferenceId)
                .ToList();
        }

        public List<SubmissionReviewer> GetAssignedReviews(string stfmUserId, int conferenceId)
        {
            return _appDbContext.SubmissionReviewer
                .Include(s => s.Reviewer)
                .Include(s => s.Submission)
                .ThenInclude(s => s.Conference)
                .Include(s => s.Submission)
                .ThenInclude(s => s.SubmissionCategory)
                .Where(s => s.Submission.Conference.ConferenceId == conferenceId 
                && s.Reviewer.StfmUserId == stfmUserId)
                .ToList();
        }

        public List<SubmissionReviewer> GetAssignedSubmissions(string stfmUserId)
        {
            //Server DateTime Now is in UTC so we need to convert DateTime Now to CST
            //NOTE we need to use the TZConvert class so this code will work on
            //Windows, Linux or Mac OS
            //See - https://devblogs.microsoft.com/dotnet/cross-platform-time-zones-with-net-core/

            TimeZoneInfo targetZone = TZConvert.GetTimeZoneInfo("Central Standard Time");

            TimeSpan offset = targetZone.GetUtcOffset(DateTime.Now);

            DateTime dateTimeNowCST = DateTime.Now.AddHours(offset.TotalHours);

            //Log.Information("DateTime Now from server is {0}, DateTime Now CST is {1}", DateTime.Now, dateTimeNowCST);

            return _appDbContext.SubmissionReviewer
            .Include(s => s.Reviewer)
            .Include(s => s.Submission)
            .ThenInclude(s => s.Conference)
            .Include(s => s.Submission)
            .ThenInclude(s => s.AcceptedCategory)
            .Where(s => s.Reviewer.StfmUserId == stfmUserId && dateTimeNowCST <= s.Submission.Conference.ConferenceSubmissionReviewEndDate)
            .ToList();
        }

        public Reviewer GetReviewerByStfmUserId(string StfmUserId)
        {

            return _appDbContext.Reviewers.FirstOrDefault(p => p.StfmUserId == StfmUserId);

        }

        public Reviewer GetReviewerByReviewerId(int reviewerId)
        {
            return _appDbContext.Reviewers.FirstOrDefault(p => p.ReviewerId == reviewerId);
        }

        public bool ReviewerExists(string stfmUserId)
        {
            return _appDbContext.Reviewers.Any(r => r.StfmUserId == stfmUserId);
        }

        public List<SubmissionReviewer> GetAllAssignedSubmissions(string stfmUserId)
        {
            return _appDbContext.SubmissionReviewer
           .Include(s => s.Reviewer)
           .Include(s => s.Submission)
           .ThenInclude(s => s.Conference)
           .Include(s => s.Submission)
           .ThenInclude(s => s.AcceptedCategory)
           .Where(s => s.Reviewer.StfmUserId == stfmUserId)
           .ToList();
        }

        public List<SubmissionReviewer> GetAssignedSubmissions(string stfmUserId, int conferenceId)
        {
            return _appDbContext.SubmissionReviewer
           .Include(s => s.Reviewer)
           .Include(s => s.Submission)
           .ThenInclude(s => s.Conference)
           .Include(s => s.Submission)
           .ThenInclude(s => s.AcceptedCategory)
           .Where(s => s.Reviewer.StfmUserId == stfmUserId && s.Submission.Conference.ConferenceId == conferenceId)
           .ToList();
        }

        public List<SubmissionReviewer> GetAssignedReviewers(int submissionId)
        {
            return _appDbContext.SubmissionReviewer
           .Include(s => s.Reviewer)
           .Include(s => s.Submission)
           .ThenInclude(s => s.Conference)
           .Include(s => s.Submission)
           .ThenInclude(s => s.AcceptedCategory)
           .Where(s => s.SubmissionId == submissionId)
           .ToList();
        }

        public NextReview GetNextReview(string stfmUserId)
        {
            //Server DateTime Now is in UTC so we need to convert DateTime Now to CST
            //NOTE we need to use the TZConvert class so this code will work on
            //Windows, Linux or Mac OS
            //See - https://devblogs.microsoft.com/dotnet/cross-platform-time-zones-with-net-core/

            TimeZoneInfo targetZone = TZConvert.GetTimeZoneInfo("Central Standard Time");

            TimeSpan offset = targetZone.GetUtcOffset(DateTime.Now);

            DateTime dateTimeNowCST = DateTime.Now.AddHours(offset.TotalHours);

            var nextReview =
                 (from submissionReviewer in _appDbContext.SubmissionReviewer
                  join reviewer in _appDbContext.Reviewers on submissionReviewer.ReviewerId equals reviewer.ReviewerId
                  join submission in _appDbContext.Submissions on submissionReviewer.SubmissionId equals submission.SubmissionId
                  join conference in _appDbContext.Conferences on submission.Conference.ConferenceId equals conference.ConferenceId
                  join review in _appDbContext.Reviews on new { submissionReviewer.ReviewerId, submissionReviewer.SubmissionId } equals new { review.ReviewerId, review.SubmissionId } into reviewLeft
                  from review in reviewLeft.DefaultIfEmpty()
                  where reviewer.StfmUserId == stfmUserId && dateTimeNowCST <= conference.ConferenceSubmissionReviewEndDate && review == null
                  select new NextReview()
                  {
                      submissionId = submissionReviewer.SubmissionId.ToString(CultureInfo.InvariantCulture),
                      categoryId = submission.AcceptedCategoryId
                  }).FirstOrDefault();
                  
            return nextReview;
        }
    }
}
