﻿using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Services
{
    public interface IConferenceSessionsCacheService
    {
        void ClearConferenceSessionsSearchCache(int conferenceId);

        ConferenceDTO GetConferenceDTO(int conferenceId);

        void SetConferenceDTO(ConferenceDTO conferenceDTO);
    }
}
