﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Authorization;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.Mappers;
using Serilog;
using ConferenceSubmission.Services;

namespace ConferenceSubmission.Controllers
{
    [Authorize]
    public class CeraController : Controller
    {
        private readonly IUserMapper _userMapper;
        private readonly ISubmissionService _submissionService;
        private readonly IConferenceService _conferenceService;
        private readonly IDisclosureService _disclosureService;
        private readonly IFAQService _fAQService;
        private readonly IAnnouncementService _announcementService;

        public CeraController(IUserMapper userMapper, ISubmissionService submissionService,
                              IConferenceService conferenceService, IDisclosureService disclosureService, IFAQService fAQService, IAnnouncementService announcementService)
        {
            _userMapper = userMapper;
            _submissionService = submissionService;
            _conferenceService = conferenceService;
            _disclosureService = disclosureService;
            _fAQService = fAQService;
            _announcementService = announcementService;
        }

        public IActionResult Index()
        {

            var user = _userMapper.Map(User.Claims);
            var model = new CeraHomePageViewModel()
            {
                User = user,
                SubmissionSummaryViewModels = _submissionService.GetCurrentCeraSubmissions(user.STFMUserId),
                ActiveConferenceViewModels = _conferenceService.GetActiveCeraConferences(),
                Announcements = _announcementService.GetCeraAnnouncements()
            };


            return View(model);
        }

        public IActionResult Help()
        {
            var model = new HelpPageViewModel
            {
                FAQs = _fAQService.GetCeraFAQs()
            };
            return View(model);
        }
    }
}