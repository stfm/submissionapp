﻿using System;
using ConferenceSubmission.Models;
using System.Linq;

namespace ConferenceSubmission.Data
{
    public class DisclosureRepository : IDisclosureRepository
    {

        private readonly AppDbContext _appDbContext;

        public DisclosureRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext; 

        }

        public int AddDisclosure(Disclosure disclosure)
        {
            _appDbContext.Add(disclosure);

            _appDbContext.SaveChanges();

            return disclosure.DisclosureId;

        }

        public Disclosure GetDisclosure(int disclosureId)
        {
            return _appDbContext.Disclosures.FirstOrDefault(d => d.DisclosureId == disclosureId);
        }

        public Disclosure GetDisclosure(string stfmUserId)
        {
            return _appDbContext.Disclosures.FirstOrDefault(d => d.StfmUserId == stfmUserId);
        }

        public void UpdateDisclosure(Disclosure disclosure)
        {

            disclosure.DisclosureDate = DateTime.Now;

            _appDbContext.Update<Disclosure>(disclosure);

            _appDbContext.SaveChanges();

        }
    }
}
