﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class UserMultipleSubmissionsViewModel
    {

        public int ParticipantId { get; set; }

        public User User { get; set; }

        public List<Submission> Submissions { get; set; }

    }
}
