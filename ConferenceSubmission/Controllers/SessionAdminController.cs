﻿using System;
using Microsoft.AspNetCore.Mvc;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.Models;
using Serilog;
using System.Linq;

namespace ConferenceSubmission.Controllers
{

    public class SessionAdminController : Controller
    {

        private readonly ISubmissionService _submissionService;

        private readonly ISubmissionParticipantService _submissionParticipantService;

        private readonly ISalesforceAPIService _salesforceAPIService;

        private readonly IEmailNotificationService _emailNotificationService;

        private readonly IConferenceSessionService _conferenceSessionService;

        private readonly IVirtualConferenceSessionService _virtualConferenceSessionService;

        public SessionAdminController(ISubmissionService submissionService,
            ISubmissionParticipantService submissionParticipantService,
                               ISalesforceAPIService salesforceAPIService,
            IEmailNotificationService emailNotificationService,
            IConferenceSessionService conferenceSessionService,
            IVirtualConferenceSessionService virtualConferenceSessionService)
        {

            _submissionService = submissionService;

            _submissionParticipantService = submissionParticipantService;

            _salesforceAPIService = salesforceAPIService;

            _emailNotificationService = emailNotificationService;

            _conferenceSessionService = conferenceSessionService;

            _virtualConferenceSessionService = virtualConferenceSessionService;
        }

        /// <summary>
        /// Responds to user's initial request to add or edit
        /// a single accepted submission's session data.
        /// </summary>
        /// <returns>View where user can enter submission id</returns>
        public IActionResult SessionDataStart()
        {

            return View();

        }

        /// <summary>
        /// Get the submission identified by the provided id. Verify
        /// it has been accepted.  If no session data exists 
        /// return add session data view.  If session data exists
        /// return edit session data view.
        /// </summary>
        /// <returns>model for adding or editing session data</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public IActionResult SessionDataInput(int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);


            if (submission is null)
            {
                ViewData["error"] = "A submission with id number of " + submissionId + " could not be found.";

                return View("SessionDataStart");

            }

            if (submission.SubmissionStatus.SubmissionStatusName != SubmissionStatusType.ACCEPTED && submission.SubmissionStatus.SubmissionStatusName != SubmissionStatusType.WILL_PRESENT_IN_AUGUST)
            {

                ViewData["error"] = "The submission number " + submissionId + " for conference " +
                    submission.Conference.ConferenceShortName + " has not been accepted.";

                return View("SessionDataStart");


            }

            SubmissionSessionViewModel submissionSessionViewModel = new SubmissionSessionViewModel()
            {
                SubmissionId = submission.SubmissionId,
                SubmissionTitle = submission.SubmissionTitle,
                ConferenceLongName = submission.Conference.ConferenceLongName,
                SubmissionCategoryName = submission.AcceptedCategory.SubmissionCategoryName,
                SubmissionStatusName = submission.SubmissionStatus.SubmissionStatusName


            };

            SubmissionPresentationMethod submissionPresentationMethodInPerson = null;

            foreach(SubmissionPresentationMethod submissionPresentationMethod in submission.SubmissionPresentationMethods) {

                if (submissionPresentationMethod.PresentationMethod.PresentationMethodName == PresentationMethodType.LIVE_IN_PERSON)
                {
                    submissionPresentationMethodInPerson = submissionPresentationMethod;

                }


            }


            if (submissionPresentationMethodInPerson == null)
            {

                ViewData["error"] = "The submission number " + submissionId + " for conference " +
                    submission.Conference.ConferenceShortName + " does not have an associated in-person submission " +
                    "presentation method so you cannot create a in-person conference session.";
                
                return View("SessionDataStart");


            }

            //Find out if the submission already has session data

            ConferenceSession conferenceSession = _conferenceSessionService.GetConferenceSession(submissionId);


            if (conferenceSession.SubmissionId == 0)
            {

                SessionDataBindingModel sessionDataBindingModel = new SessionDataBindingModel()
                {
                    SubmissionData = submissionSessionViewModel,

                    SubmissionId = submission.SubmissionId,

                    SessionStartDateTime = submission.Conference.ConferenceStartDate,

                    SessionEndDateTime = submission.Conference.ConferenceStartDate,

                    SubmissionPresentationMethod = submissionPresentationMethodInPerson


                };


                return View(sessionDataBindingModel);

            }
            else
            {
                SessionDataBindingModel sessionDataBindingModel = new SessionDataBindingModel()
                {
                    SubmissionData = submissionSessionViewModel,

                    SubmissionId = submission.SubmissionId,

                    SessionStartDateTime = conferenceSession.SessionStartDateTime,

                    SessionEndDateTime = conferenceSession.SessionEndDateTime,

                    SessionCode = conferenceSession.SessionCode,

                    SessionTrack = conferenceSession.SessionTrack,

                    SessionLocation = conferenceSession.SessionLocation,

                    SubmissionPresentationMethod = submissionPresentationMethodInPerson


                };

                return View("SessionDataEdit", sessionDataBindingModel);

            }


        }

        /// <summary>
        /// Adds session data to submission.
        /// </summary>
        /// <returns>Success View Page</returns>
        /// <param name="sessionDataBindingModel">Session data binding model.</param>
        public IActionResult SessionDataAdd(SessionDataBindingModel sessionDataBindingModel)
        {

            Log.Information("Data provided " + sessionDataBindingModel);

            Submission submission = _submissionService.GetSubmission(sessionDataBindingModel.SubmissionId);

            if (sessionDataBindingModel.SessionStartDateTime.CompareTo(DateTime.Now) < 0 || sessionDataBindingModel.SessionEndDateTime.CompareTo(DateTime.Now) < 0)
            {
                ViewData["error"] = "The session start or end date and time provided is not correctly formatted or is not in the future.";

                SubmissionSessionViewModel submissionSessionViewModel = new SubmissionSessionViewModel()
                {
                    SubmissionId = submission.SubmissionId,
                    SubmissionTitle = submission.SubmissionTitle,
                    ConferenceLongName = submission.Conference.ConferenceLongName,
                    SubmissionCategoryName = submission.AcceptedCategory.SubmissionCategoryName,
                    SubmissionStatusName = submission.SubmissionStatus.SubmissionStatusName


                };

                SessionDataBindingModel sessionDataBindingModelError = new SessionDataBindingModel()
                {
                    SubmissionData = submissionSessionViewModel,

                    SubmissionId = submission.SubmissionId,

                    SessionStartDateTime = DateTime.Now,

                    SessionEndDateTime = DateTime.Now


                };

                return View("SessionDataInput", sessionDataBindingModelError);

            }

            SubmissionPresentationMethod submissionPresentationMethodInPerson = GetInPersonSubmissionPresentationMethod(submission);

            sessionDataBindingModel.SubmissionPresentationMethod = submissionPresentationMethodInPerson;

            int conferenceSessionId = _conferenceSessionService.AddConferenceSession(sessionDataBindingModel);

            Log.Information("Added Session data table record id " + conferenceSessionId);

            if (conferenceSessionId <= 0)
            {

                Log.Error("Session data: " + sessionDataBindingModel.ToString() + " not added to database.");

                ViewData["error"] = "The session data " + sessionDataBindingModel.ToString() + " was not added to the database.";

                return View("SessionDataStart");

            }


            return View(sessionDataBindingModel);

        }

        private static SubmissionPresentationMethod GetInPersonSubmissionPresentationMethod(Submission submission)
        {
            SubmissionPresentationMethod submissionPresentationMethodInPerson = null;

            foreach (SubmissionPresentationMethod submissionPresentationMethod in submission.SubmissionPresentationMethods)
            {

                if (submissionPresentationMethod.PresentationMethod.PresentationMethodName == PresentationMethodType.LIVE_IN_PERSON)
                {
                    submissionPresentationMethodInPerson = submissionPresentationMethod;

                }


            }

            return submissionPresentationMethodInPerson;
        }

        /// <summary>
        /// Update session data with user's provided input.
        /// </summary>
        /// <returns>Success view page.</returns>
        /// <param name="sessionDataBindingModel">Session data binding model.</param>
        public IActionResult SessionDataUpdate(SessionDataBindingModel sessionDataBindingModel)
        {

            Log.Information("Data provided " + sessionDataBindingModel);


            Submission submission = _submissionService.GetSubmission(sessionDataBindingModel.SubmissionId);

            if (sessionDataBindingModel.SessionStartDateTime.CompareTo(DateTime.Now) < 0 || sessionDataBindingModel.SessionEndDateTime.CompareTo(DateTime.Now) < 0)
            {
                ViewData["error"] = "The session start or end date and time provided is not correctly formatted or is not in the future.";


                SubmissionSessionViewModel submissionSessionViewModel = new SubmissionSessionViewModel()
                {
                    SubmissionId = submission.SubmissionId,
                    SubmissionTitle = submission.SubmissionTitle,
                    ConferenceLongName = submission.Conference.ConferenceLongName,
                    SubmissionCategoryName = submission.AcceptedCategory.SubmissionCategoryName,
                    SubmissionStatusName = submission.SubmissionStatus.SubmissionStatusName


                };

                ConferenceSession conferenceSession = _conferenceSessionService.GetConferenceSession(submission.SubmissionId);

                SessionDataBindingModel sessionDataBindingModelError = new SessionDataBindingModel()
                {
                    SubmissionData = submissionSessionViewModel,

                    SubmissionId = submission.SubmissionId,

                    SessionStartDateTime = conferenceSession.SessionStartDateTime,

                    SessionEndDateTime = conferenceSession.SessionEndDateTime,

                    SessionCode = conferenceSession.SessionCode,

                    SessionTrack = conferenceSession.SessionTrack,

                    SessionLocation = conferenceSession.SessionLocation


                };

                return View("SessionDataEdit", sessionDataBindingModelError);

            }

            SubmissionPresentationMethod submissionPresentationMethodInPerson = GetInPersonSubmissionPresentationMethod(submission);

            sessionDataBindingModel.SubmissionPresentationMethod = submissionPresentationMethodInPerson;

            int conferenceSessionId = _conferenceSessionService.UpdateConferenceSession(sessionDataBindingModel);

            Log.Information("Edited Session data table record id " + conferenceSessionId);

            if (conferenceSessionId <= 0)
            {

                Log.Error("Session data: " + sessionDataBindingModel.ToString() + " not updated in database.");

                ViewData["error"] = "The session data " + sessionDataBindingModel.ToString() + " was not updated in the database.";

                return View("SessionDataStart");

            }


            return View(sessionDataBindingModel);

        }

        /// <summary>
        /// Responds to a user's request to edit an existing Virtual Conference Session
        /// </summary>
        /// <param name="virtualConferenceSessionId">Id of the Virtual Conference Session</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult EditVirtualSession(int virtualConferenceSessionId)
        {
            var virualConferenceSession = _virtualConferenceSessionService.GetWithSubmissionData(virtualConferenceSessionId);

            var virtualSessionDataBindingModel = new VirtualSessionDataBindingModel()
            {
                SubmissionId = virualConferenceSession.SubmissionId,
                SubmissionTitle = virualConferenceSession.Submission?.SubmissionTitle ?? "",
                SubmissionCategoryName = virualConferenceSession.Submission?.AcceptedCategory?.SubmissionCategoryName ?? "",
                ConferenceName = virualConferenceSession.Submission?.Conference?.ConferenceLongName ?? "",
                VirtualConferenceSessionId = virtualConferenceSessionId,
                SessionCode = virualConferenceSession.SessionCode,
                SessionStartDateTime = virualConferenceSession.SessionStartDateTime,
                SessionEndDateTime = virualConferenceSession.SessionEndDateTime,
                SessionTrack = virualConferenceSession.SessionTrack,
                SubmssionPresentationMethod = virualConferenceSession.SubmissionPresentationMethod
            };

            return View("VirtualSessionDataEdit", virtualSessionDataBindingModel);
        }

        [HttpPost]
        public IActionResult EditVirtualSession(VirtualSessionDataBindingModel virtualSessionDataBindingModel)
        {
            if (ModelState.IsValid && (virtualSessionDataBindingModel?.IsSessionStartDateAndEndDateValid() ?? false))
            {
                _virtualConferenceSessionService.Update(virtualSessionDataBindingModel);

                return RedirectToAction("ViewSubmission", "SubmissionAdmin", new { submissionId = virtualSessionDataBindingModel.SubmissionId });
            }

            if (!virtualSessionDataBindingModel?.IsSessionStartDateAndEndDateValid() ?? false)
            {
                ModelState.AddModelError("", "The Session Start Time and Session End Time must be in the future and the Session End Date must be later than the Session Start Date.");
            }

            var virualConferenceSession = _virtualConferenceSessionService.GetWithSubmissionData(virtualSessionDataBindingModel.VirtualConferenceSessionId);

            virtualSessionDataBindingModel = new VirtualSessionDataBindingModel()
            {
                SubmissionId = virualConferenceSession.SubmissionId,
                SubmissionTitle = virualConferenceSession.Submission?.SubmissionTitle ?? "",
                SubmissionCategoryName = virualConferenceSession.Submission?.AcceptedCategory?.SubmissionCategoryName ?? "",
                ConferenceName = virualConferenceSession.Submission?.Conference?.ConferenceLongName ?? "",
                VirtualConferenceSessionId = virtualSessionDataBindingModel.VirtualConferenceSessionId,
                SessionCode = virtualSessionDataBindingModel.SessionCode,
                SessionStartDateTime = virtualSessionDataBindingModel.SessionStartDateTime,
                SessionEndDateTime = virtualSessionDataBindingModel.SessionEndDateTime,
                SessionTrack = virtualSessionDataBindingModel.SessionTrack,
                SubmssionPresentationMethod = virualConferenceSession.SubmissionPresentationMethod
            };

            return View("VirtualSessionDataEdit", virtualSessionDataBindingModel);
        }

        [HttpGet]
        public IActionResult VirtualSessionDataStart(int submissionId, PresentationMethodType presentationMethodType)
        {
            var submission = _submissionService.GetSubmission(submissionId);

            var virtualSessionDataBindingModel = new VirtualSessionDataBindingModel()
            {
                SubmissionId = submissionId,
                SubmissionTitle = submission.SubmissionTitle,
                SubmissionCategoryName = submission?.AcceptedCategory?.SubmissionCategoryName ?? "",
                ConferenceName = submission?.Conference?.ConferenceLongName ?? "",
                SubmssionPresentationMethod = submission.SubmissionPresentationMethods.FirstOrDefault(x => x.PresentationMethod.PresentationMethodName == presentationMethodType),
                SubmissionPresentationMethodId = submission.SubmissionPresentationMethods.FirstOrDefault(x => x.PresentationMethod.PresentationMethodName == presentationMethodType).SubmissionPresentationMethodId
            };

            return View("VirtualSessionDataStart", virtualSessionDataBindingModel);
        }

        [HttpPost]
        public IActionResult VirtualSessionDataStart(VirtualSessionDataBindingModel virtualSessionDataBindingModel)
        {
            if (ModelState.IsValid && (virtualSessionDataBindingModel?.IsSessionStartDateAndEndDateValid() ?? false))
            {
                var result = _virtualConferenceSessionService.Create(virtualSessionDataBindingModel);

                Log.Information("Added Virtual Session data table record id " + result);

                return RedirectToAction("ViewSubmission", "SubmissionAdmin", new { submissionId = virtualSessionDataBindingModel.SubmissionId });
            }

            if (!virtualSessionDataBindingModel?.IsSessionStartDateAndEndDateValid() ?? false)
            {
                ModelState.AddModelError("", "The Session Start Time and Session End Time must be in the future and the Session End Date must be later than the Session Start Date.");
            }

            var submission = _submissionService.GetSubmission(virtualSessionDataBindingModel.SubmissionId);

            virtualSessionDataBindingModel = new VirtualSessionDataBindingModel()
            {
                SubmissionId = virtualSessionDataBindingModel.SubmissionId,
                SubmissionTitle = submission.SubmissionTitle,
                SubmissionCategoryName = submission?.AcceptedCategory?.SubmissionCategoryName ?? "",
                ConferenceName = submission?.Conference?.ConferenceLongName ?? "",
                SubmssionPresentationMethod = submission.SubmissionPresentationMethods.FirstOrDefault(x => x.PresentationMethod.PresentationMethodName == virtualSessionDataBindingModel.PresentationMethodType),
                SessionCode = virtualSessionDataBindingModel.SessionCode,
                SessionStartDateTime = virtualSessionDataBindingModel.SessionStartDateTime,
                SessionEndDateTime = virtualSessionDataBindingModel.SessionEndDateTime,
                SessionTrack = virtualSessionDataBindingModel.SessionTrack,

            };

            return View("VirtualSessionDataStart", virtualSessionDataBindingModel);
        }
    }
}
