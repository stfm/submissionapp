﻿using System;
using System.Collections.Generic;

namespace ConferenceSubmission.DTOs
{
    /// <summary>
    /// Stores data about all accepted submissions
    /// that have been scheduled for presentation
    /// at a specific conference.
    /// </summary>
    public class ConferenceDTO
    {
        public ConferenceDTO()
        {
            Sessions = new List<Session>();
        }
        public int ConferenceId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ConferenceCode { get; set; }
        public List<Session> Sessions { get; set; }
    }
}
