﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class ParticipantRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public ParticipantRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;

        }

        [Fact]
        public void GetParticipantRolesTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                ParticipantRepository participantRepository = new ParticipantRepository(context);

                IEnumerable<ParticipantRole> participantRoles = participantRepository.GetParticipantRoles;

                foreach (var participantRole in participantRoles)
                {

                    Output.WriteLine("Participant role is " + participantRole.ParticipantRoleName);

                }

                Assert.NotNull(participantRoles);

                Assert.Equal(3, participantRoles.Count());

            }
        }


        [Fact]
        public void GetParticipantRoleByName()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ParticipantRepository participantRepository = new ParticipantRepository(context);

                ParticipantRole participantRole = participantRepository.GetParticipantRole(ParticipantRoleType.PRESENTER);

                Output.WriteLine("Participant role is " + participantRole.ParticipantRoleName);

                Assert.Equal(ParticipantRoleType.PRESENTER, participantRole.ParticipantRoleName);


            }

        }

        [Fact]
        public void GetParticipantRoleByIdTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ParticipantRepository participantRepository = new ParticipantRepository(context);

                ParticipantRole participantRole = participantRepository.GetParticipantRole(1);

                Output.WriteLine("Participant role is " + participantRole.ParticipantRoleName);

                Assert.Equal(ParticipantRoleType.LEAD_PRESENTER, participantRole.ParticipantRoleName);


            }

        }


        [Fact]
        public void GetParticipantByStfmUserIdTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ParticipantRepository participantRepository = new ParticipantRepository(context);

                Participant participant = participantRepository.GetParticipantByStfmUserId("3001");

                Output.WriteLine("Participant id is " + participant.ParticipantId);

                Assert.Equal(4001, participant.ParticipantId);


            }

        }


        [Fact]
        public void GetParticipantByStfmUserIdParticipantDoesNotExistTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ParticipantRepository participantRepository = new ParticipantRepository(context);

                Participant participant = participantRepository.GetParticipantByStfmUserId("3100");

                Assert.Null(participant);


            }

        }


        [Fact]
        public void CreateParticipantTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ParticipantRepository participantRepository = new ParticipantRepository(context);

                Participant participant = new Participant { StfmUserId = "3004" };

                int participantId = participantRepository.CreateParticipant(participant);

                Participant participantFromDb = participantRepository.GetParticipantByStfmUserId("3004");

                Output.WriteLine("Participant id is " + participantFromDb.ParticipantId);

                Assert.Equal(participantId, participantFromDb.ParticipantId);


            }

        }




        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }

                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();

            }

            return options;
        }
    }
}
