﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public enum SubmissionAction
    {
        VIEW,
        EDIT,
        WITHDRAW,
        UPDATE_PARTICIPANTS,
        UPLOAD_MATERIAL,
        ACCEPT_OR_DECLINE_PRESENTATION_METHOD,
        WITHDRAW_VIRTUAL_PRESENTATION
    }
}
