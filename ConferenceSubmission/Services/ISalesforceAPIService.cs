﻿using ConferenceSubmission.DTOs.SalesforceEvent;
using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface ISalesforceAPIService
    {
        /// <summary>
        /// Given a Salesforce UserId, this method fetches basic details about a user from Salesforce.
        /// </summary>
        /// <param name="stfmUserId">a string object that is the user's SalesforceId</param>
        /// <returns>A User object containing basic information about the user</returns>
        Task<User> GetUserInfo(string stfmUserId);

        /// <summary>
        /// Given a collection of Salesforce UserIds, this method fetches basic details about each user.
        /// </summary>
        /// <param name="stfmUserIds">A collection of Salesforce Ids</param>
        /// <returns>A collection of User objects</returns>
        Task<IEnumerable<User>> GetMultipleUserInfo(IEnumerable<string> stfmUserIds);

        /// <summary>
        /// Given an STFMUserId, returns true if the user is an STFM Member, false otherwise.
        /// This method hits the SalesForce API and requests the user's badges.
        /// Note: In SalesForce, badges are found in the Contact store - NOT the User store. In the application, we store the user's UserId, not his ContactId,
        ///       so we first query for the User, then we can query the related Contact record and get the badges.
        /// NOte: In SalesForce, badges are stored in the Contact record under the name "OrderApi__Badges__c"
        /// 
        /// The user must have one of the two following badges to be considered an STFM Member
        /// 1. STFM Member Badge
        /// 2. STFM Student Member Badge
        /// </summary>
        /// <returns>true or false</returns>
        Task<Boolean> IsSTFMMember(string stfmUserId);

        /// <summary>
        /// Find SalesForces users by last name.
        /// </summary>
        /// <param name="lastName">last name</param>
        /// <returns>Collection of Users</returns>
        Task<IEnumerable<User>> FindPeopleByLastName(string lastName);

        /// <summary>
        /// Queries Salesforce for the Events between the specified dates
        /// </summary>
        /// <param name="startDate">startDate</param>
        /// <param name="endDate">startDate</param>
        /// <returns>Collection of Salesforce Events</returns>                 
        Task<IEnumerable<SFEvent>> GetEventsByDateRange(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets the registered attendees for an event in Salesforce
        /// </summary>
        /// <param name="eventId">Unique id of the Event in Salesforce</param>
        /// <returns>Collectin of EventAttendee objects</returns>
        Task<IEnumerable<EventAttendee>> GetEventAttendees(string eventId);

        /// <summary>
        /// Return true is user has one of our Administrator permissions
        /// </summary>
        /// <param name="claims">Collection of user permissions</param>
        /// <returns>true if user has one of our Administrator permissions</returns>
        bool isAdminUser(IEnumerable<System.Security.Claims.Claim> claims);
    }
}
