﻿using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.DTOs;
using System.Linq;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ConferenceSubmission.Services;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace ConferenceSubmission.Models
{
    public class AppDbContext : DbContext

    {
        private readonly ICreateAuditLogsService _auditLogService;

        public AppDbContext(DbContextOptions<AppDbContext> options, ICreateAuditLogsService auditLogService = null) : base(options)
        {
            _auditLogService = auditLogService;
        }

        public DbSet<Conference> Conferences { get; set; }

        public DbSet<ConferenceSession> ConferenceSessions { get; set; }

        public DbSet<VirtualMaterial> VirtualMaterials { get; set; }

        public DbSet<ConferenceType> ConferenceTypes { get; set; }

        public DbSet<ParticipantRole> ParticipantRoles { get; set; }

        public DbSet<Submission> Submissions { get; set; }

        public DbSet<SubmissionCategory> SubmissionCategories { get; set; }

        public DbSet<SubmissionPayment> SubmissionPayments { get; set; }

        public DbSet<SubmissionStatus> SubmissionStatuses { get; set; }

        public DbSet<Participant> Participants { get; set; }

        public DbSet<SubmissionParticipant> SubmissionParticipant { get; set; }

        public DbSet<SubmissionParticipantToParticipantRole> SubmissionParticipantToParticipantRole { get; set; }

        public DbSet<FormField> FormFields { get; set; }

        public DbSet<FormFieldProperty> FormFieldProperties { get; set; }

        public DbSet<SubmissionFormField> SubmissionFormFields { get; set; }

        public DbSet<Disclosure> Disclosures { get; set; }

        public DbSet<Reviewer> Reviewers { get; set; }

        public DbSet<SubmissionReviewer> SubmissionReviewer { get; set; }

        public DbSet<ReviewFormField> ReviewFormFields { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public DbSet<ReviewStatus> ReviewStatuses { get; set; }

        public DbSet<FAQ> FAQs { get; set; }

        public DbSet<Announcement> Announcements { get; set; }

        public DbSet<SessionObjectives> SessionObjectives { get; set; }

        public DbSet<AuditLog> AuditLogs { get; set; }

        public DbSet<SubmissionUpdate> SubmissionUpdate { get; set; }

        public DbSet<AuditLogParentEntity> AuditLogParentEntities { get;set; }

        public DbSet<VirtualConferenceData> VirtualConferenceData { get; set; }

        public DbSet<PresentationMethod> PresentationMethod { get; set; }

        public DbSet<SubmissionCategoryToPresentationMethod> SubmissionCategoryToPresentationMethod { get; set; }

        public DbSet<SubmissionPresentationMethod> SubmissionPresentationMethod { get; set; }

        public DbSet<PresentationStatus> PresentationStatus { get; set; }

        public DbSet<VirtualConferenceSession> VirtualConferenceSession { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<SessionObjectives>().HasNoKey();
            modelBuilder.Entity<SubmissionUpdate>().HasNoKey();
        }

        public override int SaveChanges()
        {
            int result = 0;

            try
            {
                _auditLogService.Initialize(ChangeTracker.Entries());

                var auditLogsForModifiedEntities = _auditLogService.GetAuditLogsForModifiedEntities();
                var auditLogsForDeletedEntiies = _auditLogService.GetAuditLogsForDeletedEntities();

                if(auditLogsForModifiedEntities.Count > 0)
                {
                    this.AuditLogs.AddRange(auditLogsForModifiedEntities);
                }

                if(auditLogsForDeletedEntiies.Count > 0)
                {
                    this.AuditLogs.AddRange(auditLogsForDeletedEntiies);
                }
            }
            catch (Exception) { }

            result = base.SaveChanges();

            //In order to create audit logs for newly created entities, we have to wait until after the initial save
            //operation is complete so we have access to the database generated primary key. 
            try
            {
                var auditLogsForCreatedEntities = _auditLogService.GetAuditLogsForCreatedEntities();
                if (auditLogsForCreatedEntities.Count > 0)
                {
                    this.AuditLogs.AddRange(auditLogsForCreatedEntities);
                    base.SaveChanges();
                }

            }
            catch (Exception) { }

            return result;
        }
    }
}
