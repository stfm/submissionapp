﻿using System;
using System.Collections.Generic;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsByCategoryForConferenceViewModel
    {

        public List<SubmissionsForCategory> SubmissionsForCategories { get; set; }
       
        public string ConferenceName { get; set; }

        public string ConferenceCFPEndDate { get; set; }

        public int ConferenceId { get; set;  }

    }
}
