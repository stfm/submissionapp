﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// Models data about a submission
    /// that we need to show on the home page view
    /// </summary>
    public class SubmissionSessionViewModel
    {

        public int SubmissionId { get; set; }

        public string SubmissionTitle { get; set; }

        public SubmissionStatusType SubmissionStatusName { get; set; }

        public String ConferenceLongName { get; set; }

        public String SubmissionCategoryName { get; set; }

        public override string ToString()
        {

            return "SubmissionId: " + SubmissionId + " | SubmissionTitle: " +
                SubmissionTitle + " | SubmissionStatusName: " +
                SubmissionStatusName +
                " | ConferenceLongName: " + ConferenceLongName +
                " | SubmissionCategoryName " + SubmissionCategoryName;

        }

    }
}
