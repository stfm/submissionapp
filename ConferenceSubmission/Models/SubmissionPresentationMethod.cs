﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    public class SubmissionPresentationMethod
    {
        public SubmissionPresentationMethod()
        {
        }

        public int SubmissionPresentationMethodId { get; set; }

        [ParentId]
        public int SubmissionId { get; set; }

        public Submission Submission { get; set; }

        public int PresentationMethodId { get; set; }

        public int PresentationStatusId { get; set; }

        [StringLength(250)]
        [Required]
        public PresentationMethod  PresentationMethod {get; set;}

        [StringLength(250)]
        [Required]
        public PresentationStatus PresentationStatus { get; set; }


    }
}
