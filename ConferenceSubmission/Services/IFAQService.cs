﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface IFAQService
    {
        /// <summary>
        /// Get all non-CERA FAQs.
        /// </summary>
        /// <returns></returns>
        List<FAQ> GetFAQs();

        /// <summary>
        /// Get just those FAQs that are for CERA
        /// help.
        /// </summary>
        /// <returns></returns>
        List<FAQ> GetCeraFAQs();
    }
}
