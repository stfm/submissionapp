﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class submissioncategorytopresentermethods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PresentationMethod",
                columns: table => new
                {
                    PresentationMethodId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PresentationMethodName = table.Column<int>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PresentationMethod", x => x.PresentationMethodId);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionCategoryToPresentationMethod",
                columns: table => new
                {
                    SubmissionCategoryToPresentationMethodId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubmissionCategoryId = table.Column<int>(nullable: false),
                    PresentationMethodId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionCategoryToPresentationMethod", x => x.SubmissionCategoryToPresentationMethodId);
                    table.ForeignKey(
                        name: "FK_SubmissionCategoryToPresentationMethod_PresentationMethod_PresentationMethodId",
                        column: x => x.PresentationMethodId,
                        principalTable: "PresentationMethod",
                        principalColumn: "PresentationMethodId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubmissionCategoryToPresentationMethod_SubmissionCategories_SubmissionCategoryId",
                        column: x => x.SubmissionCategoryId,
                        principalTable: "SubmissionCategories",
                        principalColumn: "SubmissionCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionCategoryToPresentationMethod_PresentationMethodId",
                table: "SubmissionCategoryToPresentationMethod",
                column: "PresentationMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionCategoryToPresentationMethod_SubmissionCategoryId",
                table: "SubmissionCategoryToPresentationMethod",
                column: "SubmissionCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubmissionCategoryToPresentationMethod");

            migrationBuilder.DropTable(
                name: "PresentationMethod");
        }
    }
}
