﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{

    public class CreateAuditLogsServiceTest
    {
        #region Setup

        private Claim stfmUserId;

        private CreateAuditLogsService auditLogService;

        public CreateAuditLogsServiceTest()
        {
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();

            mockHttpContextAccessor.Setup(h => h.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)).Returns(() => stfmUserId);

            this.auditLogService = new CreateAuditLogsService(mockHttpContextAccessor.Object);
        }

        #endregion

        [Fact]
        public void GetAuditLogsForModifiedEntities_ModifiedStatusOfTwoExistingSubmissions_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var submission1 = context.Submissions.FirstOrDefault(s => s.SubmissionId == 10001);
                var submission2 = context.Submissions.FirstOrDefault(s => s.SubmissionId == 10002);

                submission1.SubmissionStatusId = 2002;
                submission2.SubmissionStatusId = 2003;

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var results = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Equal(2, results.Count);
                Assert.True(results.FirstOrDefault(r => r.OldValue == "2000" && r.NewValue == "2002" && r.EntityId == 10001) != null);
                Assert.True(results.FirstOrDefault(r => r.OldValue == "2001" && r.NewValue == "2003" && r.EntityId == 10002) != null);
                Assert.Null(results.FirstOrDefault(r => r.EntityId == 10001).ParentId);
                Assert.Null(results.FirstOrDefault(r => r.EntityId == 10002).ParentId);
                Assert.Equal(2, results.Where(r => r.ChangedBy == "AA1233JF880").Count());
                Assert.Equal(2, results.Where(r => r.EntityType == EntityType.Submission).Count());
                Assert.Equal(2, results.Where(r => r.PropertyName == "SubmissionStatusId").Count());
                Assert.Equal(AuditLogActionType.MODIFIED, results[0].AuditLogActionType);
                Assert.Equal(AuditLogActionType.MODIFIED, results[1].AuditLogActionType);
                Assert.Empty(results[0].AuditLogParentEntities);
                Assert.Empty(results[1].AuditLogParentEntities);
            }
        }

        [Fact]
        public void GetAuditLogsForModifiedEntities_StubZeroModifiedSubmissions_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            //var entityEntries = new List<EntityEntry>();
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var results = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Empty(results);
            }
        }

        [Fact]
        public void GetAuditLogsForCreatedEntities_StubTwoNewSubmissions_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            //var entityEntries = new List<EntityEntry>();
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(new List<Submission>
                {
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Three Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10003,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Three Title",
                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Four Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10004,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2001,
                        SubmissionTitle = "Test Four Title",
                    }
                });
                   
                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var auditLogsForAddedSubmissions = this.auditLogService.GetAuditLogsForCreatedEntities();
                var auditLogsForModifiedSubmissions = this.auditLogService.GetAuditLogsForModifiedEntities();
                var auditLogsForDeletedSubmissions = this.auditLogService.GetAuditLogsForDeletedEntities();

                //Assert
                Assert.Empty(auditLogsForModifiedSubmissions);
                Assert.Empty(auditLogsForDeletedSubmissions);
                Assert.Equal(2, auditLogsForAddedSubmissions.Count);
                Assert.Null(auditLogsForAddedSubmissions.FirstOrDefault(a => a.EntityId == 10003).ParentId);
                Assert.Null(auditLogsForAddedSubmissions.FirstOrDefault(a => a.EntityId == 10004).ParentId);
                Assert.NotNull(auditLogsForAddedSubmissions.First(a => a.EntityId == 10003));
                Assert.NotNull(auditLogsForAddedSubmissions.First(a => a.EntityId == 10004));
                Assert.True(auditLogsForAddedSubmissions.Where(r => r.ChangedBy == "AA1233JF880").Count() == 2);
                Assert.True(auditLogsForAddedSubmissions.Where(r => r.OldValue == "").Count() == 2);
                Assert.True(auditLogsForAddedSubmissions.Where(r => r.NewValue == "ENTITY CREATED").Count() == 2);
                Assert.True(auditLogsForAddedSubmissions.Where(r => r.PropertyName == "").Count() == 2);
                Assert.True(auditLogsForAddedSubmissions.Where(r => r.EntityType == EntityType.Submission).Count() == 2);
                Assert.Equal(AuditLogActionType.CREATED, auditLogsForAddedSubmissions[0].AuditLogActionType);
                Assert.Equal(AuditLogActionType.CREATED, auditLogsForAddedSubmissions[1].AuditLogActionType);
                Assert.Empty(auditLogsForAddedSubmissions[0].AuditLogParentEntities);
                Assert.Empty(auditLogsForAddedSubmissions[1].AuditLogParentEntities);
            }
        }

        [Fact]
        public void GetAuditLogsForModifiedEntities_StubTwoNewConferenceSessions_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                Conference aConference = context.Conferences.Find(100);

                context.ConferenceSessions.AddRange(new List<ConferenceSession>
                {
                    new ConferenceSession
                    {
                        ConferenceSessionId = 11,
                        SubmissionId = 10003,
                        SessionCode = "P59",
                        SessionStartDateTime = DateTime.Now,
                        SessionEndDateTime = DateTime.Now.AddHours(2),
                        SessionLocation = "The Grandview Ballroom",
                        SessionTrack = "Track 3",
                    },
                    new ConferenceSession
                    {
                        ConferenceSessionId = 12,
                        SubmissionId = 10004,
                        SessionCode = "P90",
                        SessionStartDateTime = DateTime.Now,
                        SessionEndDateTime = DateTime.Now.AddHours(2),
                        SessionLocation = "The Olathe Room",
                        SessionTrack = "Track 4",
                    }
                });

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var auditLogsForAddedEntities = this.auditLogService.GetAuditLogsForCreatedEntities();
                var auditLogsForModifiedEntities = this.auditLogService.GetAuditLogsForModifiedEntities();
                var auditLogsForDeletedEntities = this.auditLogService.GetAuditLogsForDeletedEntities();

                //Assert
                Assert.Empty(auditLogsForModifiedEntities);
                Assert.Empty(auditLogsForDeletedEntities);
                Assert.Equal(2, auditLogsForAddedEntities.Count);
                Assert.NotNull(auditLogsForAddedEntities.First(a => a.EntityId == 11));
                Assert.NotNull(auditLogsForAddedEntities.First(a => a.EntityId == 12));
                Assert.True(auditLogsForAddedEntities.Where(r => r.ChangedBy == "AA1233JF880").Count() == 2);
                Assert.True(auditLogsForAddedEntities.Where(r => r.OldValue == "").Count() == 2);
                Assert.True(auditLogsForAddedEntities.Where(r => r.NewValue == "ENTITY CREATED").Count() == 2);
                Assert.True(auditLogsForAddedEntities.Where(r => r.PropertyName == "").Count() == 2);
                Assert.True(auditLogsForAddedEntities.Where(r => r.EntityType == EntityType.ConferenceSession).Count() == 2);
                Assert.Equal(AuditLogActionType.CREATED, auditLogsForAddedEntities[0].AuditLogActionType);
                Assert.Equal(AuditLogActionType.CREATED, auditLogsForAddedEntities[1].AuditLogActionType);
                Assert.Single(auditLogsForAddedEntities[0].AuditLogParentEntities);
                Assert.Single(auditLogsForAddedEntities[1].AuditLogParentEntities);
                Assert.Equal(10003, auditLogsForAddedEntities[0].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, auditLogsForAddedEntities[0].AuditLogParentEntities[0].ParentEntityType);
                Assert.Equal(10004, auditLogsForAddedEntities[1].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, auditLogsForAddedEntities[1].AuditLogParentEntities[0].ParentEntityType);
            }
        }

        [Fact]
        public void GetAuditLogsForModifiedEntities_UpdateTheLocactionOfTwoExistingSessions_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            //var entityEntries = new List<EntityEntry>();
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var session1 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 1);
                var session2 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 2);

                session1.SessionLocation = "The Overland Park Room";
                session2.SessionLocation = "The Lee's Summit Room";

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var auditLogsForModifiedEntitiesresults = this.auditLogService.GetAuditLogsForModifiedEntities();
                var auditLogsForDeletedEntitiesresults = this.auditLogService.GetAuditLogsForDeletedEntities();
                var auditLogsForAddedEntitiesresults = this.auditLogService.GetAuditLogsForCreatedEntities();

                //Assert
                Assert.Empty(auditLogsForDeletedEntitiesresults);
                Assert.Empty(auditLogsForAddedEntitiesresults);
                Assert.Equal(2, auditLogsForModifiedEntitiesresults.Count);
                Assert.Single(auditLogsForModifiedEntitiesresults.Where(r => r.OldValue == "The Grand Ballroom" && r.NewValue == "The Overland Park Room" && r.EntityId == 1));
                Assert.Single(auditLogsForModifiedEntitiesresults.Where(r => r.OldValue == "The Kansas City Room" && r.NewValue == "The Lee's Summit Room" && r.EntityId == 2));
                Assert.Equal(2, auditLogsForModifiedEntitiesresults.Where(r => r.EntityType == EntityType.ConferenceSession).Count());
                Assert.Equal(2, auditLogsForModifiedEntitiesresults.Where(r => r.PropertyName == "SessionLocation").Count());
                Assert.Equal(2, auditLogsForModifiedEntitiesresults.Where(r => r.ChangedBy == "AA1233JF880").Count());
                Assert.Equal(AuditLogActionType.MODIFIED, auditLogsForModifiedEntitiesresults[0].AuditLogActionType);
                Assert.Equal(AuditLogActionType.MODIFIED, auditLogsForModifiedEntitiesresults[1].AuditLogActionType);
                Assert.Single(auditLogsForModifiedEntitiesresults[0].AuditLogParentEntities);
                Assert.Single(auditLogsForModifiedEntitiesresults[1].AuditLogParentEntities);
                Assert.Equal(10001, auditLogsForModifiedEntitiesresults[0].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, auditLogsForModifiedEntitiesresults[0].AuditLogParentEntities[0].ParentEntityType);
                Assert.Equal(10002, auditLogsForModifiedEntitiesresults[1].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, auditLogsForModifiedEntitiesresults[1].AuditLogParentEntities[0].ParentEntityType);
            }
        }

        [Fact]
        public void GetAuditLogsForModifiedEntities_UpdateTheStartTimeOfTwoExistingSessions_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            //var entityEntries = new List<EntityEntry>();
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var session1 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 1);
                var session2 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 2);

                var session1CurrentStartTime = session1.SessionStartDateTime;
                var session2CurrentStartTime = session2.SessionStartDateTime;

                var session1NewStartTime = DateTime.Now.AddDays(3);
                var session2NewStartTime = DateTime.Now.AddDays(5);

                session1.SessionStartDateTime = session1NewStartTime;
                session2.SessionStartDateTime = session2NewStartTime;

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var results = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Equal(2, results.Count);
                Assert.Single(results.Where(r => r.OldValue == session1CurrentStartTime.ToString() && r.NewValue == session1NewStartTime.ToString() && r.EntityId == 1));
                Assert.Single(results.Where(r => r.OldValue == session2CurrentStartTime.ToString() && r.NewValue == session2NewStartTime.ToString() && r.EntityId == 2));
                Assert.Equal(2, results.Where(r => r.EntityType == EntityType.ConferenceSession).Count());
                Assert.Equal(2, results.Where(r => r.PropertyName == "SessionStartDateTime").Count());
                Assert.Equal(2, results.Where(r => r.ChangedBy == "AA1233JF880").Count());
                Assert.Equal(AuditLogActionType.MODIFIED, results[0].AuditLogActionType);
                Assert.Equal(AuditLogActionType.MODIFIED, results[1].AuditLogActionType);
                Assert.Single(results[0].AuditLogParentEntities);
                Assert.Single(results[1].AuditLogParentEntities);
                Assert.Equal(10001, results[0].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, results[0].AuditLogParentEntities[0].ParentEntityType);
                Assert.Equal(10002, results[1].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, results[1].AuditLogParentEntities[0].ParentEntityType);
            }
        }

        [Fact]
        public void GetAuditLogsForModifiedEntities_UpdateTheEndTimeOfTwoExistingSessions_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            //var entityEntries = new List<EntityEntry>();
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var session1 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 1);
                var session2 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 2);

                var session1CurrentEndTime = session1.SessionEndDateTime;
                var session2CurrentEndTime = session2.SessionEndDateTime;

                var session1NewEndTime = DateTime.Now.AddDays(3);
                var session2NewEndTime = DateTime.Now.AddDays(5);

                session1.SessionEndDateTime = session1NewEndTime;
                session2.SessionEndDateTime = session2NewEndTime;

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var results = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Equal(2, results.Count);
                Assert.Single(results.Where(r => r.OldValue == session1CurrentEndTime.ToString() && r.NewValue == session1NewEndTime.ToString() && r.EntityId == 1));
                Assert.Single(results.Where(r => r.OldValue == session2CurrentEndTime.ToString() && r.NewValue == session2NewEndTime.ToString() && r.EntityId == 2));
                Assert.Equal(2, results.Where(r => r.EntityType == EntityType.ConferenceSession).Count());
                Assert.Equal(2, results.Where(r => r.PropertyName == "SessionEndDateTime").Count());
                Assert.Equal(2, results.Where(r => r.ChangedBy == "AA1233JF880").Count());
                Assert.Equal(AuditLogActionType.MODIFIED, results[0].AuditLogActionType);
                Assert.Equal(AuditLogActionType.MODIFIED, results[1].AuditLogActionType);
                Assert.Single(results[0].AuditLogParentEntities);
                Assert.Single(results[1].AuditLogParentEntities);
                Assert.Equal(10001, results[0].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, results[0].AuditLogParentEntities[0].ParentEntityType);
                Assert.Equal(10002, results[1].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, results[1].AuditLogParentEntities[0].ParentEntityType);
            }
        }

        [Fact]
        public void GetAuditLogsForModifiedEntities_UpdateTheSessionCodeOfTwoExistingSessions_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            //var entityEntries = new List<EntityEntry>();
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var session1 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 1);
                var session2 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 2);

                session1.SessionCode = "B52";
                session2.SessionCode = "S34";

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var results = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Equal(2, results.Count);
                Assert.Single(results.Where(r => r.OldValue == "L01" && r.NewValue == "B52" && r.EntityId == 1));
                Assert.Single(results.Where(r => r.OldValue == "L02" && r.NewValue == "S34" && r.EntityId == 2));
                Assert.Equal(2, results.Where(r => r.EntityType == EntityType.ConferenceSession).Count());
                Assert.Equal(2, results.Where(r => r.PropertyName == "SessionCode").Count());
                Assert.Equal(2, results.Where(r => r.ChangedBy == "AA1233JF880").Count());
                Assert.Equal(AuditLogActionType.MODIFIED, results[0].AuditLogActionType);
                Assert.Equal(AuditLogActionType.MODIFIED, results[1].AuditLogActionType);
                Assert.Single(results[0].AuditLogParentEntities);
                Assert.Single(results[1].AuditLogParentEntities);
                Assert.Equal(10001, results[0].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, results[0].AuditLogParentEntities[0].ParentEntityType);
                Assert.Equal(10002, results[1].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, results[1].AuditLogParentEntities[0].ParentEntityType);
            }
        }

        [Fact]
        public void GetAuditLogsForDeletedEntities_DeleteTwoEntities_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            //var entityEntries = new List<EntityEntry>();
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var session1 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 1);
                var session2 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 2);

                context.ConferenceSessions.Remove(session1);
                context.ConferenceSessions.Remove(session2);

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var deletedResults = this.auditLogService.GetAuditLogsForDeletedEntities();
                var createdResults = this.auditLogService.GetAuditLogsForCreatedEntities();
                var modifiedResults = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Empty(createdResults);
                Assert.Empty(modifiedResults);
                Assert.Equal(2, deletedResults.Count);
                Assert.Equal("ENTITY DELETED", deletedResults[0].NewValue);
                Assert.Equal("ENTITY DELETED", deletedResults[1].NewValue);
                Assert.Equal(2, deletedResults.Where(r => r.EntityType == EntityType.ConferenceSession).Count());
                Assert.Equal(2, deletedResults.Where(r => r.ChangedBy == "AA1233JF880").Count());
                Assert.Equal(AuditLogActionType.DELETED, deletedResults[0].AuditLogActionType);
                Assert.Equal(AuditLogActionType.DELETED, deletedResults[1].AuditLogActionType);
                Assert.Single(deletedResults[0].AuditLogParentEntities);
                Assert.Single(deletedResults[1].AuditLogParentEntities);
                Assert.Equal(10001, deletedResults[0].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, deletedResults[0].AuditLogParentEntities[0].ParentEntityType);
                Assert.Equal(10002, deletedResults[1].AuditLogParentEntities[0].ParentEntityId);
                Assert.Equal(EntityType.Submission, deletedResults[1].AuditLogParentEntities[0].ParentEntityType);
            }
        }

        [Fact]
        public void GetAuditLog_NoActionTaken_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");
            //var entityEntries = new List<EntityEntry>();
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var session1 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 1);
                var session2 = context.ConferenceSessions.FirstOrDefault(s => s.ConferenceSessionId == 2);

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var deletedResults = this.auditLogService.GetAuditLogsForDeletedEntities();
                var createdResults = this.auditLogService.GetAuditLogsForCreatedEntities();
                var modifiedResults = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Empty(deletedResults);
                Assert.Empty(createdResults);
                Assert.Empty(modifiedResults);
            }
        }

        [Fact]
        public void GetAuditLog_CreateEntityWithTwoParentEntityTypes_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");

            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var submission = context.Submissions.FirstOrDefault(s => s.SubmissionId == 10001);
                submission.ParticipantLink.Add(new SubmissionParticipant
                {
                    ParticipantId = 1,
                });

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var deletedResults = this.auditLogService.GetAuditLogsForDeletedEntities();
                var createdResults = this.auditLogService.GetAuditLogsForCreatedEntities();
                var modifiedResults = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Empty(deletedResults);
                Assert.Empty(modifiedResults);
                Assert.Single(createdResults);
                Assert.Equal(2, createdResults[0].AuditLogParentEntities.Count);
                Assert.Contains(createdResults[0].AuditLogParentEntities, p => p.ParentEntityType == EntityType.Participant);
                Assert.Contains(createdResults[0].AuditLogParentEntities, p => p.ParentEntityType == EntityType.Submission);
            }
        }

        [Fact]
        public void GetAuditLog_DeleteEntityWithTwoParentEntityTypes_VerifyResults()
        {
            //Arrange
            this.stfmUserId = new Claim(ClaimTypes.NameIdentifier, "AA1233JF880");

            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using (var context = new AppDbContext(options))
            {
                var submission = context.Submissions.FirstOrDefault(s => s.SubmissionId == 10001);
                submission.ParticipantLink.Add(new SubmissionParticipant
                {
                    ParticipantId = 1
                });

                context.SaveChanges();

                var submissionParticipant = context.Submissions.FirstOrDefault(s => s.SubmissionId == 10001).ParticipantLink.FirstOrDefault();

                context.SubmissionParticipant.Remove(submissionParticipant);

                var entityEntries = context.ChangeTracker.Entries();
                this.auditLogService.Initialize(entityEntries);

                //Act
                var deletedResults = this.auditLogService.GetAuditLogsForDeletedEntities();
                var createdResults = this.auditLogService.GetAuditLogsForCreatedEntities();
                var modifiedResults = this.auditLogService.GetAuditLogsForModifiedEntities();

                //Assert
                Assert.Empty(createdResults);
                Assert.Empty(modifiedResults);
                Assert.Single(deletedResults);
                Assert.Equal(2, deletedResults[0].AuditLogParentEntities.Count);
                Assert.Contains(deletedResults[0].AuditLogParentEntities, p => p.ParentEntityType == EntityType.Participant);
                Assert.Contains(deletedResults[0].AuditLogParentEntities, p => p.ParentEntityType == EntityType.Submission);
            }
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2018"),
                        ConferenceInactive = false,
                        ConferenceLongName = "Test Conference 1",
                        ConferenceShortName = "TC1",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2018"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture TC1" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar TC1" }

                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW },
                    new SubmissionStatus { SubmissionStatusId = 2002, SubmissionStatusName = SubmissionStatusType.ACCEPTED },
                    new SubmissionStatus { SubmissionStatusId = 2003, SubmissionStatusName = SubmissionStatusType.CANCELED }
                );

                context.SaveChanges();

                context.Submissions.AddRange(new List<Submission>
                {
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test One Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test One Title",
                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Two Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10002,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2001,
                        SubmissionTitle = "Test Two Title",
                    },
                                        new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Three Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10003,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Three Title",
                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Four Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10004,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2001,
                        SubmissionTitle = "Test Four Title",
                    }
                });
                context.SaveChanges();

                context.ConferenceSessions.AddRange(new List<ConferenceSession> {
                    new ConferenceSession
                    {
                        ConferenceSessionId = 1,
                        SubmissionId = 10001,
                        SessionCode = "L01",
                        SessionStartDateTime = DateTime.Now,
                        SessionEndDateTime = DateTime.Now.AddHours(2),
                        SessionLocation = "The Grand Ballroom",
                        SessionTrack = "Track 1",                        
                    },
                    new ConferenceSession
                    {
                        ConferenceSessionId = 2,
                        SubmissionId = 10002,
                        SessionCode = "L02",
                        SessionStartDateTime = DateTime.Now,
                        SessionEndDateTime = DateTime.Now.AddHours(2),
                        SessionLocation = "The Kansas City Room",
                        SessionTrack = "Track 2",
                    }
                });

                context.SaveChanges();

                context.Participants.AddRange(new List<Participant> {
                    new Participant
                    {
                        ParticipantId = 1,
                        StfmUserId = "273489HSJDF89273"
                    },
                    new Participant
                    {
                        ParticipantId = 2,
                        StfmUserId = "000000DFJDF89273"
                    }
                });

                context.SaveChanges();
            }

            return options;
        }
    }
}
