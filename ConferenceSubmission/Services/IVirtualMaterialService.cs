﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface IVirtualMaterialService
    {

        /// <summary>
        /// Add new VirtualMaterial record.
        /// </summary>
        /// <param name="virtualMaterial"></param>
        /// <returns>VirtualMaterialId value</returns>
        int AddVirtualMaterial(VirtualMaterial virtualMaterial);

        /// <summary>
        /// Get VirtualMaterial record for provided submissionId.
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns>VirtualMaterial object</returns>
        VirtualMaterial GetVirtualMaterial(int submissionId);

        /// <summary>
        /// Update the VirtualMaterial record with the state of the 
        /// provided VirtualMaterial object.
        /// </summary>
        /// <param name="virtualMaterial"></param>
        /// <returns>VirtualMaterialId</returns>
        int UpdateVirtualMaterial(VirtualMaterial virtualMaterial);

        /// <summary>
        /// Delete any VirtualMaterial records
        /// associated with the provided submissionId.
        /// </summary>
        /// <param name="submissionId"></param>
        void DeleteVirtualMaterial(int submissionId);


        /// <summary>
        /// Delete the provided VirtualMaterial record
        /// from the data repository.
        /// </summary>
        /// <param name="virtualMaterial"></param>
        void DeleteVirtualMaterial(VirtualMaterial virtualMaterial);

        /// <summary>
        /// Get all VirtualMaterial objects associated
        /// with the provided submissionId
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns>Collection of VirtualMaterial objects</returns>
        IEnumerable<VirtualMaterial> GetVirtualMaterials(int submissionId);
        
        /// <summary>
        /// Get collection of VirtualMaterial objects 
        /// that are just the video material types for the provided
        /// conference.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns>Colleciton of VirtualMaterial objects</returns>
        IEnumerable<VirtualMaterial> GetVideoFileRecords(int conferenceId);
    }
}
