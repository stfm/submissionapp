﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionPrintViewModel
    {
        public string                       ConferenceName          { get; set; }
        public int                          SubmissionId            { get; set; }
        public string                       SubmissionTitle         { get; set; }
        public string                       AcceptedCategory        { get; set; }
        public string                       SubmissionCategory      { get; set; }
        public string                       SubmissionAbstract      { get; set; }
        public List<SubmissionFormField>    SubmissionFormFields    { get; set; }
        public List<Presenter>              Presenters              { get; set; }
        public string                       ImagePath               { get; set; }

        public string sessionCode { get; set; }

        public DateTime sessionStartDatetime { get; set; }

        public DateTime sessionEndDatetime { get; set; }

        public string sessionLocation { get; set; }

        public string sessionTrack { get; set; }


        //Public helper methods
        public string GetFileName()
        {
            //Regex that matches characters that are invalid in windows file names
            var illegalInFileName = new Regex(@"[\\/:*?""<>|]");
            return $"{illegalInFileName.Replace(SubmissionTitle.Substring(0, Math.Min(50, SubmissionTitle.Length)), "").Replace(' ', '_')}.pdf";
        }
    }
}
