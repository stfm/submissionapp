﻿using System;
using ConferenceSubmission.Data;
using ConferenceSubmission.Services;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using ConferenceSubmission.ViewModels;
using Serilog;

namespace ConferenceSubmission.Services
{
    public class ReviewAdminService : IReviewAdminService
    {
        private readonly IReviewFormFieldRepository _reviewFormFieldRepository;

        private readonly IReviewRepository _reviewRepository;

        private readonly ISubmissionParticipantService _submissionParticipantService;

        private readonly ISalesforceAPIService _salesforceAPIService;

        private readonly IReviewerRepository _reviewerRepository;

        private readonly ISubmissionService _submissionService;

        private readonly IParticipantService _participantService;

        private readonly IFormFieldRepository _formFieldRepository;


        public ReviewAdminService(IReviewFormFieldRepository reviewFormFieldRepository,
                             IReviewRepository reviewRepository, ISalesforceAPIService salesforceAPIService,
                                ISubmissionParticipantService submissionParticipantService,
                             IReviewerRepository reviewerRepository,
                                ISubmissionService submissionService,
                                IParticipantService participantService, IFormFieldRepository formFieldRepository
                             )
        {

            _reviewFormFieldRepository = reviewFormFieldRepository;

            _reviewRepository = reviewRepository;

            _salesforceAPIService = salesforceAPIService;

            _submissionParticipantService = submissionParticipantService;

            _reviewerRepository = reviewerRepository;

            _submissionService = submissionService;

            _participantService = participantService;

            _formFieldRepository = formFieldRepository;

        }

        public double GetAverageReviewScoreForSingleReview(int reviewId)
        {

            IEnumerable<ReviewFormField> reviewFormFields = _reviewFormFieldRepository.GetRatingScaleFormFieldsForSingleReview(reviewId);

            List<int> ratingScaleValues = reviewFormFields.Select(rf => Int32.Parse(rf.ReviewFormFieldValue)).ToList();

            double averageReviewScore = ratingScaleValues.Average();

            return Math.Round(averageReviewScore, 1, MidpointRounding.AwayFromZero);

        }

        public double GetAverageReviewScoreForAllReviews(int submissionId)
        {

            IEnumerable<Review> reviews = _reviewRepository.GetCompletedReviews(submissionId);

            List<int> reviewIds = reviews.Select(r => r.ReviewId).ToList();

            IEnumerable<ReviewFormField> reviewFormFields = _reviewFormFieldRepository.GetRatingScaleFormFieldsForMultipleReviews(reviewIds);

            List<int> ratingScaleValues = reviewFormFields.Select(rf => Int32.Parse(rf.ReviewFormFieldValue)).ToList();

            double averageReviewScore = ratingScaleValues.Average();

            return Math.Round(averageReviewScore, 1, MidpointRounding.AwayFromZero);

        }

        public List<SubmissionsAverageReviewScore> GetCompletedReviewsForCategoryWithAverageReviewScore(int categoryId)
        {

            List<Review> completedReviewsForCategory = _reviewRepository.GetCompletedReviewsForCategory(categoryId).ToList();

            /*
             * If there is more then one review completed for a submission the above list
             * will contain duplicate submission records so we need to filter the above
             * list to have distinct submission id
             */
            var uniqueSubmissions = completedReviewsForCategory
                .GroupBy(x => x.SubmissionId)
                .Select(g => g.OrderByDescending(x => x.SubmissionId).First());


            var submissionSummaryViewModels = uniqueSubmissions
                .Select(cr => new SubmissionsAverageReviewScore
                {

                    SubmissionId = cr.SubmissionId,
                    SubmissionTitle = cr.Submission.SubmissionTitle,
                    SubmissionCategory = cr.Submission.SubmissionCategory.SubmissionCategoryName,
                    SubmitterStfmUserId = _submissionParticipantService.GetPerson(cr.SubmissionId, ParticipantRoleType.SUBMITTER),
                    LeadPresenterUserId = _submissionParticipantService.GetPerson(cr.SubmissionId, ParticipantRoleType.LEAD_PRESENTER),
                    AverageReviewScore = GetAverageReviewScoreForAllReviews(cr.SubmissionId),
                    SubmissionStatus = Description.GetSubmissionStatusTypeString(cr.Submission.SubmissionStatusId)
   
                }).ToList();

            GetSubmitters(submissionSummaryViewModels);

            GetLeadPresenters(submissionSummaryViewModels);

            foreach(SubmissionsAverageReviewScore submissionsAverageReviewScore in submissionSummaryViewModels)
            {

                List<string> answers = GetAdditionalSubmissionDetails(submissionsAverageReviewScore.SubmissionId);

                submissionsAverageReviewScore.KeyWordOne = answers[0];
                submissionsAverageReviewScore.KeyWordTwo = answers[1];
                submissionsAverageReviewScore.GroupSubmission = answers[2];
                submissionsAverageReviewScore.MultiInstitutional = answers[3];
                submissionsAverageReviewScore.StudentIncluded = answers[4];
                submissionsAverageReviewScore.AcademicCoordinator = answers[5];
                submissionsAverageReviewScore.BehavioralScience = answers[6];

            }

            return submissionSummaryViewModels;


        }

        private void GetSubmitters(List<SubmissionsAverageReviewScore> submissionSummaryViewModels)
        {

            //Initializing a List that will store all submitter StfmUserIds from all submissions
            List<string> stfmUserIds = submissionSummaryViewModels.Select(s => s.SubmitterStfmUserId).ToList();

            //Call the Salesforce API and ask for all the users in one trip (or possibly break it down into a few trips if it can't be done in one big trip)
            IEnumerable<User> users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result;

            //Now match the users returned from Salesforce with the Participants in the submissions
            //Note: this code assumes we add a User object property to the Participant object that is dynamically populated with the Salesforce user data
            submissionSummaryViewModels.ForEach(s =>
                s.SubmitterUser = users.FirstOrDefault(u => u.STFMUserId == s.SubmitterStfmUserId));


        }


        private void GetLeadPresenters(List<SubmissionsAverageReviewScore> submissionSummaryViewModels)
        {

            //Initializing a List that will store all lead presenter StfmUserIds from all submissions
            List<string> stfmUserIds = submissionSummaryViewModels.Select(s => s.LeadPresenterUserId).ToList();

            Log.Information("Number of lead presenter STFM User ids: " + stfmUserIds.Count);

            //Call the Salesforce API and ask for all the users in one trip (or possibly break it down into a few trips if it can't be done in one big trip)
            IEnumerable<User> users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result;

            Log.Information("Number of users for that are lead presenters: " + users.ToList().Count);

            //Now match the users returned from Salesforce with the Participants in the submissions
            //Note: this code assumes we add a User object property to the Participant object that is dynamically populated with the Salesforce user data
            submissionSummaryViewModels.ForEach(s =>
                s.LeadPresenterUser = users.FirstOrDefault(u => u.STFMUserId == s.LeadPresenterUserId));


        }

        private List<string> GetAdditionalSubmissionDetails(int submissionId)
        {
            
              List<SubmissionFormField> submissionFormFields = _formFieldRepository.GetFormFieldsForSubmission(submissionId).ToList();

                string keywordOne = "";

                string keywordTwo = "";

                string groupSubmission = "";

            string multiInstitutional = "";

            string studentIncluded = "";

            string academicCoordinator = "";

            string behavioralScience = "";

                foreach (SubmissionFormField submissionFormField in submissionFormFields)
                {
                    foreach (FormFieldProperty formFieldProperty in submissionFormField.FormField.FormFieldProperties)
                    {
                        if (formFieldProperty.PropertyValue.Equals("Keyword One", StringComparison.InvariantCultureIgnoreCase))
                        {
                            keywordOne = submissionFormField.SubmissionFormFieldValue;
                        }

                        if (formFieldProperty.PropertyValue.Equals("Keyword Two", StringComparison.InvariantCultureIgnoreCase))
                        {
                            keywordTwo = submissionFormField.SubmissionFormFieldValue;
                        }

                        if (formFieldProperty.PropertyValue.Contains("proposal an official submission", StringComparison.InvariantCultureIgnoreCase))
                        {
                            groupSubmission = submissionFormField.SubmissionFormFieldValue;
                        }

                        if (formFieldProperty.PropertyValue.Contains("include presenters from two", StringComparison.InvariantCultureIgnoreCase))
                        {
                            multiInstitutional = submissionFormField.SubmissionFormFieldValue;
                        }

                        if (formFieldProperty.PropertyValue.Contains("student, resident, or other learner", StringComparison.InvariantCultureIgnoreCase))
                        {
                            studentIncluded = submissionFormField.SubmissionFormFieldValue;
                        }

                        if (formFieldProperty.PropertyValue.Contains("session for coordinators", StringComparison.InvariantCultureIgnoreCase))
                        {
                            academicCoordinator = submissionFormField.SubmissionFormFieldValue;
                        }


                        if (formFieldProperty.PropertyValue.Contains("session should be considered for behavioral", StringComparison.InvariantCultureIgnoreCase))
                        {
                            behavioralScience = submissionFormField.SubmissionFormFieldValue;
                        }

                    }   
                }
             
           

            List<string> answers = new List<string>
            {
                keywordOne,
                keywordTwo,
                groupSubmission,
                multiInstitutional,
                studentIncluded,
                academicCoordinator,
                behavioralScience
            };

            return answers;

        }

        public List<SubmissionDetailedReview> GetSubmissionDetailedReviews(int categoryId)
        {

            List<Review> completedReviewsForCategory = _reviewRepository.GetCompletedReviewsForCategory(categoryId).ToList();

            /*
             * If there is more then one review completed for a submission the above list
             * will contain duplicate submission records so we need to filter the above
             * list to have distinct submission id
             */
            var uniqueSubmissions = completedReviewsForCategory
                .GroupBy(x => x.SubmissionId)
                .Select(g => g.OrderByDescending(x => x.SubmissionId).First());

            List<SubmissionDetailedReview> submissionDetailedReviews = new List<SubmissionDetailedReview>();

            foreach (Review review in uniqueSubmissions)
            {


                SubmissionDetailedReview submissionDetailedReview = new SubmissionDetailedReview();

                submissionDetailedReview.SubmissionId = review.SubmissionId;

                submissionDetailedReview.SubmissionTitle = review.Submission.SubmissionTitle;

                string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(review.SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

                if (mainPresenterStfmUserId != null)
                {

                    submissionDetailedReview.MainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

                }
                else
                {

                    submissionDetailedReview.MainPresenter = new User();

                }

                submissionDetailedReview.SubmissionAbstract = review.Submission.SubmissionAbstract;

                submissionDetailedReview.AverageReviewScore = GetAverageReviewScoreForAllReviews(submissionDetailedReview.SubmissionId);

                List<Review> completedReviewsForSubmission = _reviewRepository.GetCompletedReviews(submissionDetailedReview.SubmissionId).ToList();



                foreach (Review aReview in completedReviewsForSubmission)
                {

                    User reviewerUser = _salesforceAPIService.GetUserInfo(aReview.Reviewer.StfmUserId).Result;

                    aReview.Reviewer.ReviewerName = reviewerUser.GetFullNamePretty();

                    aReview.ReviewerAverageScore = GetAverageReviewScoreForSingleReview(aReview.ReviewId);

                }

                submissionDetailedReview.SubmissionCompletedReviews = completedReviewsForSubmission;

                submissionDetailedReviews.Add(submissionDetailedReview);

            }

            List<SubmissionDetailedReview> submissionDetailedReviewsSorted = submissionDetailedReviews.OrderByDescending(dr => dr.AverageReviewScore).ToList();



            return submissionDetailedReviewsSorted;

        }


        public List<SubmissionDetailedReview> GetSubmissionDetailedReviewsForSubmission(int submissionId)
        {



            List<SubmissionDetailedReview> submissionDetailedReviews = new List<SubmissionDetailedReview>();


            Submission submission = _submissionService.GetSubmission(submissionId);

            SubmissionDetailedReview submissionDetailedReview = new SubmissionDetailedReview();

            submissionDetailedReview.SubmissionId = submissionId;

            submissionDetailedReview.SubmissionTitle = submission.SubmissionTitle;

            string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(submissionId, ParticipantRoleType.LEAD_PRESENTER);

            if (mainPresenterStfmUserId != null)
            {

                submissionDetailedReview.MainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

            }
            else
            {

                submissionDetailedReview.MainPresenter = new User();

            }

            submissionDetailedReview.SubmissionAbstract = submission.SubmissionAbstract;

            submissionDetailedReview.AverageReviewScore = GetAverageReviewScoreForAllReviews(submissionDetailedReview.SubmissionId);

            List<Review> completedReviewsForSubmission = _reviewRepository.GetCompletedReviews(submissionDetailedReview.SubmissionId).ToList();



            foreach (Review aReview in completedReviewsForSubmission)
            {

                User reviewerUser = _salesforceAPIService.GetUserInfo(aReview.Reviewer.StfmUserId).Result;

                aReview.Reviewer.ReviewerName = reviewerUser.GetFullNamePretty();

                aReview.ReviewerAverageScore = GetAverageReviewScoreForSingleReview(aReview.ReviewId);

            }

            submissionDetailedReview.SubmissionCompletedReviews = completedReviewsForSubmission;

            submissionDetailedReviews.Add(submissionDetailedReview);



            List<SubmissionDetailedReview> submissionDetailedReviewsSorted = submissionDetailedReviews.OrderByDescending(dr => dr.AverageReviewScore).ToList();



            return submissionDetailedReviewsSorted;

        }

        public List<ReviewIncomplete> GetReviewersNotDone(int conferenceId)
        {
            List<ReviewIncomplete> reviewersNotDoneList = new List<ReviewIncomplete>();

            Dictionary<int, int> reviewersNotDone =
         new Dictionary<int, int>();

            //Get all reviews assigned for conference

            List<SubmissionReviewer> allReviews = _reviewerRepository.GetAssignedReviews(conferenceId);

            Log.Information($"Number of reviews assigned {allReviews.Count} for conference ID {conferenceId}");

            /*
             * Iterate over each review assigned and check if it is completed.
             * If it is not completed add 1 to the count of incomplete reviews
             * for the reviewerid.            
             */

            foreach (SubmissionReviewer aReview in allReviews)
            {

                bool isReviewComplete = _reviewRepository.GetReviewCompleted(aReview.ReviewerId, aReview.SubmissionId);

                if (isReviewComplete == false)
                {

                    try
                    {
                        reviewersNotDone.Add(aReview.ReviewerId, 1);
                    }
                    catch (ArgumentException)
                    {
                        int count = reviewersNotDone[aReview.ReviewerId] + 1;

                        reviewersNotDone[aReview.ReviewerId] = count;

                    }

                }


            }

            Log.Information($"Number of reviewers not done {reviewersNotDone.Count}");


            /*
             * Iterate over each reviewer not done
             * and create a ReviewIncompleteViewModel
             */

            foreach (KeyValuePair<int, int> kvp in reviewersNotDone)
            {

                Reviewer reviewer = _reviewerRepository.GetReviewerByReviewerId(kvp.Key);

                Log.Information("About to get User SalesForce data for " + reviewer.StfmUserId);

                try
                {
                    User user = _salesforceAPIService.GetUserInfo(reviewer.StfmUserId).Result;

                    Log.Information("User found in SalesForce is " + user.LastName);

                    ReviewIncomplete reviewIncompleteViewModel = new ReviewIncomplete
                    {
                        Reviewer = user,


                        NumberOfReviewsNotDone = kvp.Value

                    };

                    reviewersNotDoneList.Add(reviewIncompleteViewModel);

                }
                catch (Exception ex)
                {

                    Log.Error("Unable to get user " + reviewer.StfmUserId + " data from SalesForce: " + ex.Message);

                }

            }

            return reviewersNotDoneList;

        }

        public List<UserMultipleSubmissionsViewModel> GetUsersWithMoreThenTwoSubmissions(int conferenceId, int submissionStatusId)
        {

            List<UserMultipleSubmissionsViewModel> userMultipleSubmissionsViewModels = new List<UserMultipleSubmissionsViewModel>();

            List<int> participantIds = _submissionParticipantService.GetParticipantsMoreThenTwoSubmissions(conferenceId, submissionStatusId);

            HashSet<string> stfmUserIds = new HashSet<string>();

            foreach (int participantId in participantIds)
            {

                Participant participant = _participantService.GetParticipantByParticipantId(participantId);

                stfmUserIds.Add(participant.StfmUserId);

            }

            //Get first 500 STFMUserIds

            int processAmount = 500;

            List<string> stfmUserIdsPartial = stfmUserIds.Take(processAmount).ToList();

            //Call the Salesforce API to get first group of users
            IEnumerable<User> partialUsers1 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

            IEnumerable<User> users = partialUsers1;

            //how many stfmUserIds are left to process

            int stfmUserIdsToProcess = stfmUserIds.Count() - processAmount;

            //determine how many times we will need to loop over the remaining
            //STFM User IDs to process all of the remaining, doing processAmount
            //at at time

            double loopsToRunDouble = stfmUserIdsToProcess / processAmount;

            int loopsToRun = (int)Math.Ceiling(loopsToRunDouble);

            //loop over the remaining stfmUserIdsToProcess to get all the Users

            for (int i = 0; i <= loopsToRun; i++)
            {

                int skipAmount = processAmount + (i * processAmount);

                stfmUserIdsPartial = stfmUserIds.Skip(skipAmount).ToList().Take(processAmount).ToList();

                if (stfmUserIdsPartial.Count > 0)
                {
                    //Call the Salesforce API to get next group of users
                    IEnumerable<User> partialUsers2 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

                    //Join the two collections

                    users = users.Union(partialUsers2);
                }


            }

            foreach (int participantId in participantIds)
            {

                Participant participant = _participantService.GetParticipantByParticipantId(participantId);

                User user = users.FirstOrDefault(u => u.STFMUserId == participant.StfmUserId);

                List<Submission> submissions = _submissionService.GetCompleteSubmissions(participant.StfmUserId).ToList();

                List<Submission> submissionsActiveInConference = submissions.FindAll(s => s.SubmissionStatusId == submissionStatusId && s.Conference.ConferenceId == conferenceId);

                UserMultipleSubmissionsViewModel userMultipleSubmissionsViewModel = new UserMultipleSubmissionsViewModel
                {
                    ParticipantId = participantId,

                    User = user,

                    Submissions = submissionsActiveInConference


                };

                userMultipleSubmissionsViewModels.Add(userMultipleSubmissionsViewModel);

            }

            return userMultipleSubmissionsViewModels;

        }

        public List<ReviewerStatus> GetReviewersStatus(int conferenceId)
        {
            List<ReviewerStatus> reviewersStatus = new List<ReviewerStatus>();

            Dictionary<int, int> reviewsCompletedByReviewer =
         new Dictionary<int, int>();

            Dictionary<int, int> reviewsAssigedByReviewer = new Dictionary<int, int>();

            //Get all reviews assigned for conference

            List<SubmissionReviewer> allReviews = _reviewerRepository.GetAssignedReviews(conferenceId);

            Log.Information($"Number of reviews assigned {allReviews.Count} for conference ID {conferenceId}");

            /*
             * Iterate over each review assigned and check if it is completed.
             * If it is not completed add 1 to the count of incomplete reviews
             * for the reviewerid.            
             */

            foreach (SubmissionReviewer aReview in allReviews)
            {
                if (!reviewsAssigedByReviewer.ContainsKey(aReview.ReviewerId))
                {
                    reviewsAssigedByReviewer.Add(aReview.ReviewerId, 1);
                }

                else
                {
                    int count = reviewsAssigedByReviewer[aReview.ReviewerId] + 1;

                    reviewsAssigedByReviewer[aReview.ReviewerId] = count;

                }

                bool isReviewComplete = _reviewRepository.GetReviewCompleted(aReview.ReviewerId, aReview.SubmissionId);

                if (isReviewComplete == true)
                {

                    if (!reviewsCompletedByReviewer.ContainsKey(aReview.ReviewerId))
                    {
                        reviewsCompletedByReviewer.Add(aReview.ReviewerId, 1);
                    }
                    else
                    {
                        int count = reviewsCompletedByReviewer[aReview.ReviewerId] + 1;

                        reviewsCompletedByReviewer[aReview.ReviewerId] = count;

                    }

                }


            }

            Log.Information($"Number of reviewers with a status {reviewsAssigedByReviewer.Count}");

            var stfmUserIds = allReviews.Where(x => reviewsAssigedByReviewer.Keys.Contains(x.ReviewerId)).Select(x => x.Reviewer.StfmUserId).Distinct().ToList();

            try
            {
                var users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result.ToList();

                /*
                 * Iterate over each reviewer assigned to submissions
                 * and create a ReviewerStatus
                 */
                foreach (KeyValuePair<int, int> kvp in reviewsAssigedByReviewer)
                {
                    Reviewer reviewer = allReviews.Where(x => x.ReviewerId == kvp.Key).Select(x => x.Reviewer).FirstOrDefault();

                    var user = users.FirstOrDefault(x => x.STFMUserId == reviewer.StfmUserId);

                    ReviewerStatus reviewerStatus = new ReviewerStatus
                    {
                        Reviewer = user,

                        NumberOfReviewsAssigned = kvp.Value

                    };

                    if (reviewsCompletedByReviewer.TryGetValue(kvp.Key, out int reviewsCompleted))
                    {
                        reviewerStatus.NumberOfReviewsCompleted = reviewsCompleted;

                    }
                    else
                    {
                        reviewerStatus.NumberOfReviewsCompleted = 0;
                    }

                    reviewersStatus.Add(reviewerStatus);

                }
            }
            catch (Exception ex)
            {
                Log.Error($"The attempt to get {stfmUserIds.Count} users data from SalesForce failed. {ex.Message}");
            }

            return reviewersStatus;
        }

        public List<PotentialReviewer> AddNewReviewerFindPeople(string lastName)
        {
            List<User> people = _salesforceAPIService.FindPeopleByLastName(lastName).Result.ToList();

            List<PotentialReviewer> potentialReviewers = new List<PotentialReviewer>();

            foreach (User person in people)
            {
                var isReviewer = _reviewerRepository.ReviewerExists(person.STFMUserId);

                PotentialReviewer potentialReviewer = new PotentialReviewer
                {
                    User = person,
                    IsReviewer = isReviewer


                };

                potentialReviewers.Add(potentialReviewer);

            }

            return potentialReviewers;

        }

        public List<Submission> GetSubmissionsNotAssignedToReviewer(int conferenceId)
        {
            return _reviewRepository.GetSubmissionsNotAssignedToReviewer(conferenceId).ToList();
        }

        public List<Submission> GetSubmissionsNoReviewsCompleted(int conferenceId)
        {
            List<Submission> submissionsNoReviewsCompletedDuplicateSubmissions =
                _reviewRepository.GetSubmissionsNoReviewsCompleted(conferenceId).ToList();

            List<Submission> submissionsNoReviewsComnpleted =
                submissionsNoReviewsCompletedDuplicateSubmissions.Distinct(new SubmissionEqualityComparator()).ToList<Submission>();

            return submissionsNoReviewsComnpleted;


        }


        public List<SubmissionReviewer> GetAssignedReviewers(int submissionId)
        {

            List<SubmissionReviewer> submissionReviewers = _reviewerRepository.GetAssignedReviewers(submissionId);

            foreach (SubmissionReviewer submissionReviewer in submissionReviewers)
            {

                User reviewerUser = _salesforceAPIService.GetUserInfo(submissionReviewer.Reviewer.StfmUserId).Result;

                try
                {
                    submissionReviewer.Reviewer.ReviewerName = reviewerUser.GetFullNamePretty();

                    submissionReviewer.Reviewer.ReviewerEmail = reviewerUser.Email;

                }
                catch (Exception ex)
                {
                    submissionReviewer.Reviewer.ReviewerName = "Not Found";

                    submissionReviewer.Reviewer.ReviewerEmail = "Not Found";
                }

            }

            return submissionReviewers;

        }

        public List<SubmissionReviewStatus> GetSubmissionsReviewStatus(int conferenceId)
        {

            List<Submission> submissionsAssignedToReviewers = _reviewRepository.GetSubmissionsAssignedToReviewer(conferenceId).ToList<Submission>();

            Dictionary<int, int> submissionTotalReviewers =
         new Dictionary<int, int>();

            Dictionary<int, int> submissionTotalReviewsCompleted = new Dictionary<int, int>();

            //Get all reviews assigned for conference

            List<SubmissionReviewer> allReviews = _reviewerRepository.GetAssignedReviews(conferenceId);

            /*
             * Iterate over each review assigned and check if it is completed.
             * If it is not completed add 1 to the count of incomplete reviews
             * for the reviewerid.            
             */

            foreach (SubmissionReviewer aReview in allReviews)
            {
                if (!submissionTotalReviewers.ContainsKey(aReview.SubmissionId))
                {
                    submissionTotalReviewers.Add(aReview.SubmissionId, 1);
                }

                else
                {
                    int count = submissionTotalReviewers[aReview.SubmissionId] + 1;

                    submissionTotalReviewers[aReview.SubmissionId] = count;

                }

                bool isReviewComplete = _reviewRepository.GetReviewCompleted(aReview.ReviewerId, aReview.SubmissionId);

                if (isReviewComplete == true)
                {

                    if (!submissionTotalReviewsCompleted.ContainsKey(aReview.SubmissionId))
                    {
                        submissionTotalReviewsCompleted.Add(aReview.SubmissionId, 1);
                    }
                    else
                    {
                        int count = submissionTotalReviewsCompleted[aReview.SubmissionId] + 1;

                        submissionTotalReviewsCompleted[aReview.SubmissionId] = count;

                    }

                }


            }

            List<SubmissionReviewStatus> subissionsReviewStatus = new List<SubmissionReviewStatus>();

            foreach (Submission submission in submissionsAssignedToReviewers)
            {

                int totalSubmissionReviewsCompleted = 0;

                if (submissionTotalReviewsCompleted.ContainsKey(submission.SubmissionId))
                {
                    totalSubmissionReviewsCompleted = submissionTotalReviewsCompleted[submission.SubmissionId];

                }

                int reviewsNotCompleted = submissionTotalReviewers[submission.SubmissionId] - totalSubmissionReviewsCompleted;

                SubmissionReviewStatus submissionReviewStatus = new SubmissionReviewStatus()
                {
                    SubmissionId = submission.SubmissionId,
                    ReviewsAssigned = submissionTotalReviewers[submission.SubmissionId],
                    ReviewsCompleted = totalSubmissionReviewsCompleted,
                    ReviewsNotCompleted = reviewsNotCompleted

                };

                subissionsReviewStatus.Add(submissionReviewStatus);

            }

            return subissionsReviewStatus;
        }

        public bool GetReviewCompleted(int reviewerId, int submissionId)
        {
            return _reviewRepository.GetReviewCompleted(reviewerId, submissionId);
        }
    }
}
