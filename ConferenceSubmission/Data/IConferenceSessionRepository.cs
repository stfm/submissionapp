﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface IConferenceSessionRepository
    {
        /// <summary>
        /// Adds the conference session to the data repository.
        /// </summary>
        /// <returns>The conference session id value from the data repository</returns>
        /// <param name="conferenceSession">Conference session.</param>
        int AddConferenceSession(ConferenceSession conferenceSession);

        /// <summary>
        /// Gets the conference session.
        /// </summary>
        /// <returns>The conference session.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        ConferenceSession GetConferenceSession(int submissionId);

        /// <summary>
        /// Updates the conference session.
        /// </summary>
        /// <returns>The conference session id value from the data repository.</returns>
        /// <param name="conferenceSession">Conference session.</param>
        int UpdateConferenceSession(ConferenceSession conferenceSession);

        /// <summary>
        /// Gets the ConferenceSessions associated with the provided conferenceId
        /// </summary>
        /// <param name="conferenceId">ConferenceId</param>
        /// <returns>Collection of ConferenceSession objects</returns>
        IEnumerable<ConferenceSession> GetSessionsForConference(int conferenceId);

    }
}
