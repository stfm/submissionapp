﻿using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.Data
{
    public interface ISubmissionPresentationMethodRepository
    {
        /// <summary>
        /// Add new SubmissionPresentationMethod record.
        /// </summary>
        /// <param name="submissionPresentationMethod"></param>
        /// <returns>SubmissionPresentationMethodId value</returns>
        int AddSubmissionPresentationMethod(SubmissionPresentationMethod submissionPresentationMethod);

        /// <summary>
        /// Delete the SubmissionPresentationMethod referenced by the provided submissionPresentationMethodId
        /// </summary>
        /// <param name="submissionPresentationMethodId"></param>
        void DeleteSubmissionPresentationMethod(int submissionPresentationMethodId);

        /// <summary>
        /// Get all SubmissionPresentationMethod objects for the provided
        /// submission ID.
        /// </summary>
        /// <returns>Collection of SubmissionPresentationMethod objects</returns>
        /// <param name="submissionId">Submission identifier.</param>
        IEnumerable<SubmissionPresentationMethod> GetSubmissionPresentationMethods(int submissionId);


    }
}
