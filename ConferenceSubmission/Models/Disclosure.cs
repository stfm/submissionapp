﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    public class Disclosure
    {
        
        public int DisclosureId { get; set; }

        [StringLength(50)]
        [Required]
        public string StfmUserId { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime DisclosureDate { get; set; }

        [StringLength(10)]
        [Required]
        public string Part1Answer { get; set; }

        public string Part2Answer { get; set; }

        [StringLength(10)]
        [Required]
        public string Part3Answer { get; set; }

        public string Part4Answer { get; set; }

        [StringLength(10)]
        [Required]
        public string Part5Answer { get; set; }

        public string Part6Answer { get; set; }

        [StringLength(100)]
        public string CvFilename { get; set; }

        [StringLength(100)]
        [Required]
        public string FullName { get; set; }

    }
}
