﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmissionTest
{
    public class SubmissionPaymentTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public SubmissionPaymentTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }


        [Fact]
        public void GetSubmissionPayment()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2018"),
                        ConferenceInactive = false,
                        ConferenceLongName = "Test Conference 1",
                        ConferenceShortName = "TC1",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2018"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture TC1" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar TC1" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW },
                    new SubmissionStatus { SubmissionStatusId = 2002, SubmissionStatusName = SubmissionStatusType.ACCEPTED }

                );

                context.SaveChanges();

                context.Submissions.AddRange(

                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2002,
                        SubmissionTitle = "Test Title",
                       
                    }

                );

                context.SaveChanges();

                SubmissionPayment aSubmissionPayment = new SubmissionPayment { PaymentAmount = 20, PaymentDateTime = DateTime.Parse(" Feb 10, 2018"), PaymentTransactionId = "9999999999" };

          
                Submission aSubmission = context.Submissions.Find(10001);

                aSubmission.SubmissionPayment = aSubmissionPayment;

                context.Update(aSubmission);

                context.SaveChanges();


            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new AppDbContext(options))
            {
                IEnumerable<Submission> submissions = context.Submissions.Include(s => s.SubmissionPayment);

                foreach (var submission in submissions)
                {

                    Output.WriteLine("Submission title is " + submission.SubmissionTitle);

                    Output.WriteLine("Submission Payment Transaction Id is " + submission.SubmissionPayment.PaymentTransactionId);

                    Output.WriteLine("Submission Payment submission ID is " + submission.SubmissionPayment.SubmissionId);

                }

                Assert.NotNull(submissions);

            }


        }
    }
}
