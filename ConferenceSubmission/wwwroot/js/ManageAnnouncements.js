﻿var showCreateAnnouncementForm = function () {
    $("#showAnnouncementFormLink").hide();
    $("#createAnnouncementFormContainer").show();
};

var cancelCreateAnnouncement = function () {
    $("#announcemenTextArea").val("");
    $("#showAnnouncementFormLink").show();
    $("#createAnnouncementFormContainer").hide();
};

var deleteAnnouncement = function (announcementId) {
    $.ajax({
        url: "/ITAdmin/DeleteAnnouncement?announcementId=" + announcementId,
        dataType: "html",
        method: "POST"
    }).done(function (data) {
        $("#allAnnouncementsContainer").empty();
        $("#allAnnouncementsContainer").html(data);
    }).fail(function () {
        $("#showAnnouncementFormLink").hide();
        $("#allAnnouncementsContainer").empty();
        $("#allAnnouncementsContainer").html("<h4 class='text-red'>We're sorry but your request could not be processed at this time.</h4>");
    });
};

var createAnnouncement = function () {
    var announcement = $("#announcemenTextArea").val();
    if (announcement !== null && announcement.length > 0) {
        var createData = {
            "Content": announcement
        };
        $.ajax({
            url: "/ITAdmin/CreateAnnouncement",
            method: "POST",
            dataType: "html",
            data: createData
        }).done(function (data) {
            $("#showAnnouncementFormLink").show();
            $("#createAnnouncementFormContainer").hide();
            $("#allAnnouncementsContainer").empty();
            $("#allAnnouncementsContainer").html(data);
            $("#announcemenTextArea").val("");
        }).fail(function () {
            $("#createAnnouncementFormContainer").empty();
            $("#createAnnouncementFormContainer").hide();
            $("#allAnnouncementsContainer").empty();
            $("#allAnnouncementsContainer").html("<h4 class='text-red'>We're sorry but your request could not be processed at this time.</h4>");
        });
    }
};

var showEditAnnouncementForm = function (announcementId) {
    var val = $("#announcementContentContainer-" + announcementId).text();
    $("#editAnnouncemenTextArea-" + announcementId).val(val);
    $("#editAnnouncementForm-" + announcementId).show();
};

var cancelEditAnnouncement = function (announcementId) {
    $("#editAnnouncementForm-" + announcementId).hide();
};

var editAnnouncement = function (announcementId, sortOrder) {
    var val = $("#editAnnouncemenTextArea-" + announcementId).val();
    if (val !== null && val.length > 0) {
        var editData = {
            "announcementId": announcementId,
            "sortOrder": sortOrder,
            "Content": val
        };
        $.ajax({
            url: "/ITAdmin/UpdateAnnouncement",
            method: "POST",
            dataType: "html",
            data: editData
        }).done(function (data) {
            $("#allAnnouncementsContainer").empty();
            $("#allAnnouncementsContainer").html(data);
            $("#announcemenTextArea").val("");
        }).fail(function () {
            $("#showAnnouncementFormLink").hide();
            $("#allAnnouncementsContainer").empty();
            $("#allAnnouncementsContainer").html("<h4 class='text-red'>We're sorry but your request could not be processed at this time.</h4>");
        });
    }
};