﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using TimeZoneConverter;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.BindingModels;

namespace ConferenceSubmission.Data
{
    public class ConferenceRepository : IConferenceRepository
    {
        private readonly AppDbContext _appDbContext;


        public int CreateConference(Conference conference)
        {
            var conferenceToAdd = new Conference
            {
                CategoryDetailUrl = conference.CategoryDetailUrl,
                ConferenceCFPEndDate = conference.ConferenceCFPEndDate,
                ConferenceDeleted = conference.ConferenceDeleted,
                ConferenceEndDate = conference.ConferenceEndDate,
                ConferenceStartDate = conference.ConferenceStartDate,
                ConferenceHybrid = conference.ConferenceHybrid,
                ConferenceInactive = conference.ConferenceInactive,
                ConferenceLongName = conference.ConferenceLongName,
                ConferenceShortName = conference.ConferenceShortName,
                ConferenceSubmissionReviewEndDate = conference.ConferenceSubmissionReviewEndDate,
                ConferenceType = conference.ConferenceType,
                ConferenceTypeId = conference.ConferenceTypeId,
                ConferenceVirtual = conference.ConferenceVirtual,
                SubmissionPaymentRequirementText = conference.SubmissionPaymentRequirementText,

            };

            _appDbContext.Conferences.Add(conferenceToAdd);

            _appDbContext.SaveChanges();

            return conferenceToAdd.ConferenceId;
        }

        public ConferenceRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;
        }

        public IEnumerable<ConferenceType> GetConferenceTypes
        {

            get
            {

                return _appDbContext.ConferenceTypes;
            }

        }

        public ConferenceType GetConferenceType(int conferenceTypeId)
        {

            return _appDbContext.ConferenceTypes.FirstOrDefault(ct => ct.ConferenceTypeId == conferenceTypeId);


        }

        public ConferenceType GetConferenceType(string conferenceTypeName)
        {

            return _appDbContext.ConferenceTypes.FirstOrDefault(ct => ct.ConferenceTypeName == conferenceTypeName);

        }

      
        public IEnumerable<Conference> GetConferences {

            get
            {

                return _appDbContext.Conferences.Where(c => c.ConferenceDeleted == false);

            }

        }

        public Conference GetConference(int conferenceId)
        {
            return _appDbContext.Conferences.FirstOrDefault(c => c.ConferenceId == conferenceId);
        }

        public Conference GetConference(string conferenceShortName)
        {
            return _appDbContext.Conferences.FirstOrDefault(c => c.ConferenceShortName == conferenceShortName);
        }

        public IEnumerable<Conference> GetActiveConferences()
        {
            return _appDbContext.Conferences.Where(c => c.ConferenceInactive == false && c.ConferenceTypeId != 4);
        }

        public IEnumerable<Conference> GetConferencesWithOpenReviewStatus()
        {
            //Convert ConferenceSubmissionReviewEndDate to UTC as DateTime from the server will be UTC
            //NOTE we need to use the TZConvert class so this code will work on
            //Windows, Linux or Mac OS
            //See - https://devblogs.microsoft.com/dotnet/cross-platform-time-zones-with-net-core/

            TimeZoneInfo targetZone = TZConvert.GetTimeZoneInfo("Central Standard Time");

            TimeSpan offset = targetZone.GetUtcOffset(DateTime.Now);

            // EF Core cannot translate the DateTime comparison in the second Where clause into a database query and so must be evaluated in the code.
            // EF Core 3.0 requires us to explicitely call that in-code comparison, hence the call to ToList() before the second where clause.
            return _appDbContext.Conferences
                .Where(c => c.ConferenceSubmissionReviewEndDate.HasValue)
                .ToList()
                .Where(c => c.ConferenceSubmissionReviewEndDate.Value.AddHours(Math.Abs(offset.Hours)).CompareTo(TimeZoneInfo.ConvertTimeToUtc(DateTime.Now)) > 0);
        }

        public IEnumerable<Conference> GetConferencesWithUnstartedStatus()
        {
            //Convert ConferenceStartDate to UTC as DateTime from the server will be UTC
            //NOTE we need to use the TZConvert class so this code will work on
            //Windows, Linux or Mac OS
            //See - https://devblogs.microsoft.com/dotnet/cross-platform-time-zones-with-net-core/

            TimeZoneInfo targetZone = TZConvert.GetTimeZoneInfo("Central Standard Time");

            TimeSpan offset = targetZone.GetUtcOffset(DateTime.Now);


            // EF Core cannot translate the DateTime comparison in the second Where clause into a database query and so must be evaluated in the code.
            // EF Core 3.0 requires us to explicitely call that in-code comparison, hence the call to ToList() before the second where clause.
            var conferences = _appDbContext.Conferences
                .Where(c => c.ConferenceDeleted == false)
                .ToList()
                .Where(c => c.ConferenceStartDate.AddHours(Math.Abs(offset.Hours)).CompareTo(TimeZoneInfo.ConvertTimeToUtc(DateTime.Now)) > 0);

            return conferences;

        }

        public void RetireConference(int conferenceId)
        {
            Conference conference = GetConference(conferenceId);

            conference.ConferenceDeleted = true;

            _appDbContext.Update<Conference>(conference);

            _appDbContext.SaveChanges();
        }

        public bool ToggleConferenceInactive(int conferenceId)
        {
            var conference = _appDbContext.Conferences.FirstOrDefault(c => c.ConferenceId == conferenceId);
            conference.ConferenceInactive = !conference.ConferenceInactive;
            _appDbContext.SaveChanges();
            return conference.ConferenceInactive;
        }

        public DateTime EditCFPEndDate(int conferenceId, DateTime endDate)
        {
            var conference = _appDbContext.Conferences.FirstOrDefault(c => c.ConferenceId == conferenceId);
            if (conference != null)
            {
                conference.ConferenceCFPEndDate = endDate;
                _appDbContext.SaveChanges();
            }
            return conference?.ConferenceCFPEndDate ?? new DateTime();
        }

        public DateTime EditSubmissionReviewEndDate(int conferenceId, DateTime endDate)
        {
            var conference = _appDbContext.Conferences.FirstOrDefault(c => c.ConferenceId == conferenceId);
            if (conference != null)
            {
                conference.ConferenceSubmissionReviewEndDate = endDate;
                _appDbContext.SaveChanges();
            }
            return conference?.ConferenceSubmissionReviewEndDate ?? new DateTime();
        }

        public IEnumerable<Conference> GetActiveCeraConferences()
        {
            return _appDbContext.Conferences.Where(c => c.ConferenceInactive == false && c.ConferenceTypeId == 4);
        }

        public string GetConferenceName(int conferenceId)
        {
            var conferenceName = _appDbContext.Conferences.FirstOrDefault(x => x.ConferenceId == conferenceId)?.ConferenceLongName ?? "";

            return conferenceName;
        }

        public IEnumerable<EntityByNameAndId> GetConferencesByDateRange(DateTime startDate, DateTime endDate)
        {
            var conferences = _appDbContext.Conferences.Where(x => x.ConferenceStartDate >= startDate && x.ConferenceStartDate <= endDate)
                .Select(x => new EntityByNameAndId { EntityId = x.ConferenceId, EntityName = x.ConferenceShortName });

            return conferences;
        }

    }
}
