﻿using System;
namespace ConferenceSubmission.BindingModels
{
    public class DisclosureQuestion
    {

        public string DisclosureQuestionValue { get; set; }

        public string DisclosureQuestionText { get; set; }


    }
}
