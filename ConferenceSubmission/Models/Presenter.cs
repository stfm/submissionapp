﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Adds more detail to a SubmissionParticipant since all we store
    /// in our database is the StfmUserId for a SubmissionParticipant.
    /// </summary>
    public class Presenter : SubmissionParticipant
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Credentials { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public AssignedParticipantRolesType AssignedParticipantRolesType { get; set; }

        public List<ParticipantRoleType> GetUnassignedParticipantRoleTypes()
        {
            var assignedParticipantRoleTypes = SubmissionParticipantToParticipantRolesLink.Select(s => s.ParticipantRole.ParticipantRoleName);

            return Enum.GetValues(typeof(ParticipantRoleType)).Cast<ParticipantRoleType>()
                        .Where(p => !assignedParticipantRoleTypes.Contains(p))
                        .ToList();

        }

    }
}
