﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace ConferenceSubmission.Models
{
    public static class Description
    {

        public static string GetDescription<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(DescriptionAttribute), false)
                            .FirstOrDefault() as DescriptionAttribute;

                        if (descriptionAttribute != null)
                        {
                            return descriptionAttribute.Description;
                        }
                    }
                }
            }

            return string.Empty; 
        }

        public static string GetSubmissionStatusTypeString(int enumIndex)
        {
            string str = "";
            if (Enum.IsDefined(typeof(SubmissionStatusType), enumIndex - 1))
                str = ((SubmissionStatusType)enumIndex - 1).ToString();
            else
                str = "Invalid Value";

            return str;


        }

        public static string GetPresentatonStatusTypeString(int enumIndex)
        {
            string str = "";
            if (Enum.IsDefined(typeof(PresentationStatusType), enumIndex - 1))
                str = ((PresentationStatusType)enumIndex - 1).ToString();
            else
                str = "Invalid Value";

            return str;

        }

        public static string GetPresentationMethodTypeString(int enumIndex)
        {
            string str = "";
            if (Enum.IsDefined(typeof(PresentationMethodType), enumIndex - 1))
                str = ((PresentationMethodType)enumIndex - 1).ToString();
            else
                str = "Invalid Value";

            return str;

        }

    }


}



