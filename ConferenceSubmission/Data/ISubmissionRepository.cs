﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.DTOs.PosterHall;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface ISubmissionRepository
    {
        /// <summary>
        /// Adds the submission.
        /// </summary>
        /// <returns>The submission ID.</returns>
        /// <param name="submission">Submission.</param>
        int AddSubmission(Submission submission);

        /// <summary>
        /// Gets the submission by submission identifier.
        /// </summary>
        /// <returns>The submission by submission identifier includes 
        /// SubmissionCategory and SubmissionStatus</returns>
        /// <param name="submissionId">Submission identifier.</param>
        Submission GetSubmissionBySubmissionId(int submissionId);

        /// <summary>
        /// Gets the submission all details by submission identifier.
        /// </summary>
        /// <returns>The submission all details by submission identifier.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        Submission GetSubmissionAllDetailsBySubmissionId(int submissionId);

        /// <summary>
        /// Update the basic details (values that are not 
        /// part of a relationship) of a submission. 
        /// </summary>
        /// <param name="submission">Submission.</param>
        void UpdateSubmission(Submission submission);

        /// <summary>
        /// Adds the submission payment to submission.
        /// </summary>
        /// <returns>The submission payment ID.</returns>
        /// <param name="submissionPayment">Submission payment.</param>
        int AddSubmissionPaymentToSubmission(SubmissionPayment submissionPayment);


        /// <summary>
        /// Gets the submissions for the provided stfmUserId
        /// </summary>
        /// <returns>The submissions.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        IEnumerable<Submission> GetSubmissions(string stfmUserId);

        /// <summary>
        /// Updates the submission completed field for the submission with
        /// the provided submissionId.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="submissionCompleted">If set to <c>true</c> submission completed.</param>
        void UpdateSubmissionCompleted(int submissionId, bool submissionCompleted);

        /// <summary>
        /// Gets the submissions for a conference that either are completed or
        /// not completed based on value provided to parameter.
        /// </summary>
        /// <returns>The submissions.</returns>
        /// <param name="isSubmissionCompleted">If set to <c>true</c> is submission completed.</param>
        /// <param name="conferenceId">Conference ID</param>
        IEnumerable<Submission> GetSubmissions(bool isSubmissionCompleted, int conferenceId);

        /// <summary>
        /// Gets the submissions by category for a specific <paramref name="conferenceId"/>.
        /// NOTE does not include submissions cancelled or withdrawn.
        /// </summary>
        /// <returns>The submissions by category.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        /// <param name="submissionCompleted">true if to get only completed submissions; false to get only incomplete submissions </param>
        Dictionary<string, int> GetSubmissionsByCategory(int conferenceId, bool submissionCompleted);
        
        
        /// <summary>
        /// Gets the number of submissions by category using submission status for a specific 
        /// conferenceId.
        /// </summary>
        /// <param name="conferenceId">Conference Identifier</param>
        /// <param name="submissionStatusType">submission status type</param>
        /// <returns>The number of accepted submissions by category</returns>
        Dictionary<string, int> GetSubmissionsByCategoryUsingStatus(int conferenceId, SubmissionStatusType submissionStatusType);

        /// <summary>
        /// Gets the submissions where submitter paid to submit by category 
        /// for a specific <paramref name="conferenceId"/>.
        /// Includes all submissions no matter submission status
        /// </summary>
        /// <returns>The submissions where submitter paid to submit by category.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        Dictionary<string, int> GetSubmissionsPaidForByCategory(int conferenceId);

        /// <summary>
        /// Gets the submissions for conference.
        /// </summary>
        /// <returns>The submissions for conference.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        IEnumerable<Submission> GetSubmissionsForConference(int conferenceId);

        /// <summary>
        /// Gets the accepted submissions for conference.
        /// </summary>
        /// <returns>The submissions for conference.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        IEnumerable<Submission> GetAcceptedSubmissionsForConference(int conferenceId);

        /// <summary>
        /// Gets the accepted submissions for conference that are eligible to be 
        /// presented at virtual conference supplement.
        /// </summary>
        /// <returns>The submissions for conference that are eligible to be presented at 
        /// virtual conference supplement.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        IEnumerable<Submission> GetAcceptedOnlineSubmissionsForConference(int conferenceId);

        /// <summary>
        /// Gets the submissions for conference that are where virtual material has been
        /// provided for the virtual conference supplement.
        /// </summary>
        /// <returns>Submissions that will be presented at the virtual conference supplement</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        IEnumerable<Submission> GetVirtualConferenceSubmissions(int conferenceId);

        /// <summary>
        /// Gets the accepted submissions for the provided session IDs.
        /// </summary>
        /// <returns>The submissions</returns>
        ///<param name="submissionIds">Collecton of submission IDs</param>
        IEnumerable<Submission> GetSubmissions(IEnumerable<int> submissionIds);

        /// <summary>
        /// Gets the accepted submissions for category.
        /// </summary>
        /// <returns>The accepted submissions for accepted category ID provided.</returns>
        /// <param name="acceptedCategoryId">Accepted Category Id</param>
        IEnumerable<Submission> GetAcceptedSubmissionsForCategory(int acceptedCategoryId);

        /// <summary>
        /// For a given submission, sets the ParticipantOrderModified flag to true
        /// </summary>
        /// <param name="submissionId">Unique identifier of submission</param>
        void SetParticipantOrderModifiedToTrue(int submissionId);

        /// <summary>
        /// For a given conference, returns all submissions (by Id) whose participant order
        /// has been modified.
        /// </summary>
        /// <param name="conferenceId">Unique identifier of submission</param>
        /// <returns>List of ints where each int is a submission Id</returns>
        List<int> GetSubmissionsWhoseParticipantOrderHasBeenModified(int conferenceId);

        /// <summary>
        /// Gets a list of the submissions, by conference, that are not withdrawn or cancelled
        /// </summary>
        /// <param name="conference">Identifier of Conference</param>
        /// <returns>List of SubmissionWithNameAndSessionCode</returns>
        List<SubmissionWithNameAndSessionCode> GetConferenceSubmissionsByNameAndSessionCode(int conference);

        /// <summary>
        /// Attempts to find the submission in the database, returns true or false if found/not found
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns>boolean</returns>
        bool SubmissionExists(int submissionId);

        /// <summary>
        /// Returns all accepted submissions for a particular category. Note: returns a row for each participant for each submission
        /// So if a submission has 3 participants, that submission will be returned three times (once for each of the 3 participants)
        /// </summary>
        /// <param name="submissionCategoryId">Unique Identifier of the submission</param>
        /// <returns></returns>
        List<PosterDTO> GetSubmissionsForPosterHallCategory(int submissionCategoryId);

        /// <summary>
        /// Gets all submissions (that have a session) and the presenters in a format needed by the conference registration reports
        /// </summary>
        /// <param name="conferenceId">Unique identifier of conference</param>
        /// <returns>List of SubmissionForRegistrationReport objects</returns>
        List<SubmissionForRegistrationReport> GetSubmissionsForConferenceRegistrationReport(int conferenceId);

        /// <summary>
        /// For the given Submission, finds the SubmissionPresentationMethod whose PresentationMethodType matches the given PresentationMethodType and updates the PresentationMethodType to the given PresentationMethodType
        /// </summary>
        /// <param name="submissionId">Id of the submission</param>
        /// <param name="presentationMethodType">This tells the system which Submission Presentation Method needs to be updated.</param>
        /// <param name="presentationStatusType">Update the SubmissionPresntationMethod's status to this PresentationStatusType</param>
        void UpdateSubmissionPresentationMethodStatus(int submissionId, PresentationMethodType presentationMethodType, PresentationStatusType presentationStatusType);
    }
}
