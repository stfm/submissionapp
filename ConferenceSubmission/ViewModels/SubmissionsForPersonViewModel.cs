﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsForPersonViewModel
    {

        public SubmissionsForPersonViewModel()
        {
            SubmissionSummaryViewModels = new List<SubmissionSummaryViewModel>();
        }

        public List<SubmissionSummaryViewModel> SubmissionSummaryViewModels { get; set; }

        public Models.User User { get; set; }


    }
}
