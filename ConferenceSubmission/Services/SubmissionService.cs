using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Castle.Components.DictionaryAdapter;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.Data;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Serilog;
using TimeZoneConverter;

namespace ConferenceSubmission.Services
{
    public class SubmissionService : ISubmissionService
    {

        private readonly ISubmissionRepository _submissionRepository;

        private readonly IFormFieldRepository _formFieldRepository;

        private readonly ISalesforceAPIService _salesforceAPIService;

        private readonly ISubmissionCategoryService _submissionCategoryService;

        private readonly IDisclosureService _disclosureService;

        private readonly ISubmissionParticipantService _submissionParticipantService;

        private readonly ISubmissionStatusService _submissionStatusService;

        private readonly IConferenceService _conferenceService;

        private readonly ISubmissionCategoryService _SubmissionCategoryService;

        private readonly ISubmissionUpdateRepository _submissionUpdateRepository;

        private readonly ISubmissionPresentationMethodRepository _submissionPresentationMethodRepository;

        private readonly IGetSubmissionActionsForSubmissionService _getSubmissionActionsForSubmissionService;

        public SubmissionService(ISubmissionRepository submissionRepository,
                                 ISalesforceAPIService salesforceAPIService,
                                 IFormFieldRepository formFieldRepository,
                                 ISubmissionCategoryService submissionCategoryService,
                                 IDisclosureService disclosureService,
                                 ISubmissionParticipantService submissionParticipantService,
                                 ISubmissionStatusService submissionStatusService,
                                 IConferenceService conferenceService,
                                 ISubmissionCategoryService submmissionCategoryService,
                                 ISubmissionUpdateRepository submissionUpdateRepository,
                                 ISubmissionPresentationMethodRepository submissionPresentationMethodRepository,
                                 IGetSubmissionActionsForSubmissionService getSubmissionActionsForSubmissionService)
        {

            _submissionRepository = submissionRepository;

            _salesforceAPIService = salesforceAPIService;

            _formFieldRepository = formFieldRepository;

            _submissionCategoryService = submissionCategoryService;

            _disclosureService = disclosureService;

            _submissionParticipantService = submissionParticipantService;

            _submissionStatusService = submissionStatusService;

            _conferenceService = conferenceService;

            _SubmissionCategoryService = submmissionCategoryService;

            _submissionUpdateRepository = submissionUpdateRepository;

            _submissionPresentationMethodRepository = submissionPresentationMethodRepository;

            _getSubmissionActionsForSubmissionService = getSubmissionActionsForSubmissionService;

        }

        public List<SubmissionSummaryViewModel> GetCurrentSubmissions(string stfmUserId)
        {

            //Get all submissions for this STFM User
            List<SubmissionSummaryViewModel> allSubmissions = GetSubmissions(stfmUserId);

            //Filter all the submissions to create a new List of those submissons
            //where the conference end date has not passed and submission status is 
            //not canceled and submission status is not withdrawn and the submission
            //is not for a CERA survey
            List<SubmissionSummaryViewModel> currentSubmissions = allSubmissions.Where(
                           s => s.ConferenceEndDate.CompareTo(DateTime.Today) >= 0 &&
                           s.SubmissionStatusName != SubmissionStatusType.CANCELED &&
                           s.SubmissionStatusName != SubmissionStatusType.WITHDRAWN &&
                           ! s.ConferenceLongName.Contains("CERA", StringComparison.InvariantCultureIgnoreCase)).ToList();


            return currentSubmissions;

        }

        public List<SubmissionSummaryViewModel> GetAllSubmissionsNotWithdrawnOrCanceled(string stfmUserId)
        {

            //Get all submissions for this STFM User
            List<SubmissionSummaryViewModel> allSubmissions = GetSubmissions(stfmUserId);

            //Filter all the submissions to create a new List of those submissons
            //where the submission status is 
            //not canceled and submission status is not withdrawn
            List<SubmissionSummaryViewModel> allSubmissionsNotWithdrawnOrCanceled = allSubmissions.Where(
                s => s.SubmissionStatusName != SubmissionStatusType.CANCELED &&
                           s.SubmissionStatusName != SubmissionStatusType.WITHDRAWN).ToList();

          
            return allSubmissionsNotWithdrawnOrCanceled;

        }

        public List<SubmissionSummaryViewModel> GetAllSubmissionsNotCanceled(string stfmUserId)
        {

            //Get all submissions for this STFM User
            List<SubmissionSummaryViewModel> allSubmissions = GetSubmissions(stfmUserId);

            //Filter all the submissions to create a new List of those submissons
            //where the submission status is 
            //not canceled
            List<SubmissionSummaryViewModel> allSubmissionsNotCanceled = allSubmissions.Where(
                s => s.SubmissionStatusName != SubmissionStatusType.CANCELED).ToList();


            return allSubmissionsNotCanceled;

        }

        public List<SubmissionSummaryViewModel> GetSubmissions(string stfmUserId)
        {


            var submissionSummaryViewModels = _submissionRepository.GetSubmissions(stfmUserId)
                .Select(s => new SubmissionSummaryViewModel
                {
                    ConferenceLongName = s.Conference.ConferenceLongName,
                    ConferenceCFPEndDate = s.Conference.ConferenceCFPEndDate,
                    ConferenceEndDate = s.Conference.ConferenceEndDate,
                    SubmissionId = s.SubmissionId,
                    SubmissionTitle = s.SubmissionTitle,
                    SessionCode = s.ConferenceSession != null ? s.ConferenceSession.SessionCode : "",
                    SubmissionStatusName = GetSubmissionStatus(s),
                    SubmissionCompleted = s.SubmissionCompleted,
                    SubmissionCategory = s.AcceptedCategory.SubmissionCategoryName,
                    userRoles = s.ParticipantLink.FirstOrDefault(p => p.Participant.StfmUserId == stfmUserId).SubmissionParticipantToParticipantRolesLink.Select(sp => sp.ParticipantRole.ParticipantRoleName)
                                                 .ToList(),
                    SubmissionPresentationMethods = s.SubmissionPresentationMethods,
                }).ToList();

            submissionSummaryViewModels.ForEach(x => 
            {
                x.SubmissionActions = _getSubmissionActionsForSubmissionService.Execute(x.userRoles.Contains(ParticipantRoleType.LEAD_PRESENTER) || x.userRoles.Contains(ParticipantRoleType.SUBMITTER), x.SubmissionPresentationMethods, x.SubmissionStatusName, x.ConferenceCFPEndDate);
            });

            return submissionSummaryViewModels;

        }



        /// <summary>
        /// Gets the submission status - if the submission status
        /// is not Open then return the status; if the submission
        /// status is Open then check if the Conference's CFP End
        /// DateTime has past and if it has return Pending Review
        /// </summary>
        /// <returns>The submission status.</returns>
        /// <param name="submission">Submission.</param>
        public SubmissionStatusType GetSubmissionStatus(Submission submission)
        {

            SubmissionStatusType submissionStatus = submission.SubmissionStatus.SubmissionStatusName;



            if (submissionStatus.Equals(SubmissionStatusType.OPEN))
            {
                //Convert ConferenceCFPEndDate to UTC as DateTime from the server will be UTC
                //NOTE we need to use the TZConvert class so this code will work on
                //Windows, Linux or Mac OS
                //See - https://devblogs.microsoft.com/dotnet/cross-platform-time-zones-with-net-core/

                TimeZoneInfo targetZone = TZConvert.GetTimeZoneInfo("Central Standard Time");

                TimeSpan offset = targetZone.GetUtcOffset(DateTime.Now);

                DateTime cfpEndDateUtc = submission.Conference.ConferenceCFPEndDate.AddHours( Math.Abs(offset.Hours) );

                //Log.Information("CFP end datetime in UTC is {0} and server UTC DateTime is {1}", cfpEndDateUtc, TimeZoneInfo.ConvertTimeToUtc(DateTime.Now));

                if ( cfpEndDateUtc.CompareTo(TimeZoneInfo.ConvertTimeToUtc(DateTime.Now)) < 0 )
                {

                    //CFP End Date has past so change Open Submission Status to Pending Review
                    //to restrict what the logged in user may do.

                    submissionStatus = SubmissionStatusType.PENDING_REVIEW;

                }

            }

            return submissionStatus;

        }

        /// <summary>
        /// Gets the user submission right based on the user's role on the
        /// submission and the submission's status
        /// </summary>
        /// <returns>The user submission right.</returns>
        /// <param name="participantRoles">Participant roles.</param>
        /// <param name="submissionStatus">Status of the submission - submissions with a 
        /// certain status get different Participant rights</param>
        public ParticipantSubmissionRight GetUserSubmissionRight(List<ParticipantRoleType> participantRoles, SubmissionStatusType submissionStatus)
        {

            ParticipantSubmissionRight participantSubmissionRight = ParticipantSubmissionRight.VIEW_UPDATE_PRESENTERS;

            if (submissionStatus.Equals(SubmissionStatusType.OPEN) &&
                (participantRoles.Contains(ParticipantRoleType.LEAD_PRESENTER) ||
                 participantRoles.Contains(ParticipantRoleType.SUBMITTER)))
            {


                participantSubmissionRight = ParticipantSubmissionRight.ALL;

            }

            if (submissionStatus.Equals(SubmissionStatusType.ACCEPTED) &&
                (participantRoles.Contains(ParticipantRoleType.LEAD_PRESENTER) ||
                 participantRoles.Contains(ParticipantRoleType.SUBMITTER)))
            {


                participantSubmissionRight = ParticipantSubmissionRight.UPDATE_SUBMISSION_PRESENTATION_METHODS;

            }

            if (participantRoles.Contains(ParticipantRoleType.PRESENTER) &&
                !participantRoles.Contains(ParticipantRoleType.SUBMITTER))
            {

                participantSubmissionRight = ParticipantSubmissionRight.VIEW_ONLY;

            }

            if (submissionStatus == SubmissionStatusType.PENDING &&
                (participantRoles.Contains(ParticipantRoleType.LEAD_PRESENTER) ||
                 participantRoles.Contains(ParticipantRoleType.SUBMITTER)))
            {
                participantSubmissionRight = ParticipantSubmissionRight.PENDING_VIRTUAL_DECISION;
            }

            if (submissionStatus == SubmissionStatusType.WILL_PRESENT_AT_VIRTUAL_CONFERENCE &&
                (participantRoles.Contains(ParticipantRoleType.LEAD_PRESENTER) ||
                 participantRoles.Contains(ParticipantRoleType.SUBMITTER)))
            {
                participantSubmissionRight = ParticipantSubmissionRight.WILL_PRESENT_VIRTUALLY;
            }

            return participantSubmissionRight;

        }

        public bool UpdateSubmissionStatus(int submissionId, int submissionStatusId)
        {

            Submission submission = GetSubmission(submissionId);

            submission.SubmissionStatusId = submissionStatusId;

            _submissionRepository.UpdateSubmission(submission);

            return true;

        }

        public Submission GetSubmission(int submissionId)
        {

            return _submissionRepository.GetSubmissionAllDetailsBySubmissionId(submissionId);

        }

        public int AddSubmission(Submission submission)
        {
            return _submissionRepository.AddSubmission(submission);
        }

        public bool UpdateSubmissionBaseDetails(int submissionId, string title, string _abstract, bool adminEditedSubmission)
        {
            var submission = _submissionRepository.GetSubmissionBySubmissionId(submissionId);
            if (submission != null)
            {
                submission.SubmissionTitle = title;
                submission.SubmissionAbstract = _abstract;
                submission.SubmissionLastUpdatedDateTime = DateTime.Now;
                submission.SubmissionAdminEdited = adminEditedSubmission;
                _submissionRepository.UpdateSubmission(submission);

                return true;
            }
            return false;
        }

        public SubmissionWithProposalAnswersViewModel GetSubmissionWithProposal(int submissionId, string StfmUserId)
        {

            Submission submission = GetSubmission(submissionId);

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = new SubmissionWithProposalAnswersViewModel
            {
                SubmissionId = submissionId,

                SubmissionTitle = submission.SubmissionTitle,

                SubmissionCategoryName = submission.SubmissionCategory.SubmissionCategoryName,

                SubmissionAcceptedCategoryName = submission.AcceptedCategory.SubmissionCategoryName,

                SubmissionStatus = submission.SubmissionStatus.SubmissionStatusName,

                SubmissionAbstract = submission.SubmissionAbstract,

                SubmissionFormFields = _formFieldRepository.GetFormFieldsForSubmission(submissionId).ToList<SubmissionFormField>(),

                SubmissionCompleted = IsSubmissionCategoryProposalCompleted(submissionId),

                Presenters = new List<Presenter>(),

                SubmissionPaymentRequirement = submission.SubmissionCategory.SubmissionPaymentRequirement,

                ConferenceStartDate = submission.Conference.ConferenceStartDate,

                ConferenceName = submission.Conference.ConferenceLongName,

                ConferenceCfpEndDate = submission.Conference.ConferenceCFPEndDate,

                VirtualConferenceSessions = submission.VirtualConferenceSessions
            };

            if (submission.ConferenceSession != null)
            {
                submissionWithProposalAnswersViewModel.sessionCode = submission.ConferenceSession.SessionCode;

                submissionWithProposalAnswersViewModel.sessionStartDatetime = submission.ConferenceSession.SessionStartDateTime;

                submissionWithProposalAnswersViewModel.sessionEndDatetime = submission.ConferenceSession.SessionEndDateTime;

                submissionWithProposalAnswersViewModel.sessionLocation = submission.ConferenceSession.SessionLocation;

                submissionWithProposalAnswersViewModel.sessionTrack = submission.ConferenceSession.SessionTrack;

                submissionWithProposalAnswersViewModel.sessionPresentationMethod = submission.ConferenceSession.SubmissionPresentationMethod;
            }


            if (submission.SubmissionPayment != null)
            {


                submissionWithProposalAnswersViewModel.SubmissionPaymentAmountPaid = submission.SubmissionPayment.PaymentAmount;

            }


            submissionWithProposalAnswersViewModel.isDisclosureCurrent = _disclosureService.isDisclosureCurrent(submission.Conference.ConferenceStartDate, StfmUserId);


            //For each SubmissionParticipant get their data from SalesForce
            //and then create a Presenter object

            var users = _salesforceAPIService
                .GetMultipleUserInfo(submission.ParticipantLink.Select(p => p.Participant.StfmUserId))
                .Result;

            foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
            {

                var stfmUserId = submissionParticipant.Participant.StfmUserId;

                var user = users.FirstOrDefault(u => u.STFMUserId == submissionParticipant.Participant.StfmUserId);

                Presenter presenter = new Presenter
                {
                    SubmissionParticipantId = submissionParticipant.SubmissionParticipantId,

                    SubmissionParticipantToParticipantRolesLink = submissionParticipant.SubmissionParticipantToParticipantRolesLink,

                    FirstName = user.FirstName,

                    LastName = user.LastName,

                    Credentials = user.Credentials,

                    FullName = user.GetFullNamePretty(),

                    SortOrder = submissionParticipant.SortOrder

                };

                submissionWithProposalAnswersViewModel.Presenters.Add(presenter);

            }

            return submissionWithProposalAnswersViewModel;


        }

        public SubmissionWithProposalAnswersViewModel GetSubmissionWithProposal(int submissionId)
        {

            Submission submission = GetSubmission(submissionId);

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = new SubmissionWithProposalAnswersViewModel
            {
                SubmissionId = submissionId,

                SubmissionTitle = submission.SubmissionTitle,

                SubmissionCategoryName = submission.SubmissionCategory.SubmissionCategoryName,

                SubmissionAcceptedCategoryName = submission.AcceptedCategory.SubmissionCategoryName,

                SubmissionStatus = submission.SubmissionStatus.SubmissionStatusName,

                SubmissionAbstract = submission.SubmissionAbstract,

                SubmissionFormFields = _formFieldRepository.GetFormFieldsForSubmission(submissionId).ToList<SubmissionFormField>(),

                SubmissionCompleted = IsSubmissionCategoryProposalCompleted(submissionId),

                Presenters = new List<Presenter>(),

                SubmissionPaymentRequirement = submission.SubmissionCategory.SubmissionPaymentRequirement,

                ConferenceStartDate = submission.Conference.ConferenceStartDate,

                ConferenceName = submission.Conference.ConferenceLongName,

                VirtualMaterials = submission.VirtualMaterials,

                ConferenceVirtual = submission.Conference.ConferenceVirtual,

                SubmissionPresentationMethods = submission.SubmissionPresentationMethods,

                VirtualConferenceSessions = submission.VirtualConferenceSessions

                
            };

            if (submission.ConferenceSession != null)
            {
                submissionWithProposalAnswersViewModel.sessionCode = submission.ConferenceSession.SessionCode;

                submissionWithProposalAnswersViewModel.sessionStartDatetime = submission.ConferenceSession.SessionStartDateTime;

                submissionWithProposalAnswersViewModel.sessionEndDatetime = submission.ConferenceSession.SessionEndDateTime;

                submissionWithProposalAnswersViewModel.sessionLocation = submission.ConferenceSession.SessionLocation;

                submissionWithProposalAnswersViewModel.sessionTrack = submission.ConferenceSession.SessionTrack;

                submissionWithProposalAnswersViewModel.sessionPresentationMethod = submission.ConferenceSession.SubmissionPresentationMethod;


            }

            if (submission.SubmissionPayment != null)
            {

                submissionWithProposalAnswersViewModel.SubmissionPaymentAmountPaid = submission.SubmissionPayment.PaymentAmount;

            }


           


            //For each SubmissionParticipant get their data from SalesForce
            //and then create a Presenter object

            foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
            {

                var stfmUserId = submissionParticipant.Participant.StfmUserId;

                var user = _salesforceAPIService.GetUserInfo(stfmUserId).Result;

                Presenter presenter = new Presenter
                {
                    SubmissionParticipantId = submissionParticipant.SubmissionParticipantId,

                    SubmissionParticipantToParticipantRolesLink = submissionParticipant.SubmissionParticipantToParticipantRolesLink,

                    FirstName = user != null ? user.FirstName : "",

                    LastName = user != null ? user.LastName : "",

                    Credentials = user != null ? user.Credentials : "" ,

                    FullName = user != null ? user.GetFullNamePretty() : "",

                    SortOrder = submissionParticipant.SortOrder,

                    Participant = submissionParticipant.Participant,

                    Email = user?.Email ?? ""

                };

                submissionWithProposalAnswersViewModel.Presenters.Add(presenter);

            }

            submissionWithProposalAnswersViewModel.SubmissionCategories = GetSubmissionCategoryItems(submission.Conference.ConferenceId);

            return submissionWithProposalAnswersViewModel;


        }

        public bool IsSubmissionCategoryProposalCompleted(int submissionId)
        {
            //Get collection of answers user has provided for Submission Category Proposal Form Fields

            List<SubmissionFormField> submissionFormFields = _formFieldRepository.GetFormFieldsForSubmission(submissionId).ToList<SubmissionFormField>();

            //Get the submission
            Submission submission = GetSubmission(submissionId);

            //Get collection of FormField objects where AnswerRequired is true

            List<FormField> formFieldsAnswerRequired = _formFieldRepository.GetFormFieldsAnswerRequired(submission.SubmissionCategoryId, "proposal").ToList<FormField>();


            foreach (FormField formField in formFieldsAnswerRequired)
            {

                if (!submissionFormFields.Any(sff => sff.FormFieldId == formField.FormFieldId))
                {

                    //this FormField where an answer is required is not in the collection
                    //of SubmissionFormField objects so user did not answer all the 
                    //submission category proposal form questions that are required. Return false.
                    return false;
                }

            }
            //If we're here, we know that for each required FormField, we found a corresponding 
            //SubmissionFormField object, so the user answered all the submission category 
            //proposal form questions that are required. Return true.
            return true;

        }

        public void UpdateSubmissionCompleted(int submissionId, bool submissionCompleted)
        {
            _submissionRepository.UpdateSubmissionCompleted(submissionId, submissionCompleted);
        }


        public void AddSubmissionPayment(SubmissionPayment submissionPayment)
        {

            Submission aSubmission = _submissionRepository.GetSubmissionBySubmissionId(submissionPayment.SubmissionId);

            aSubmission.SubmissionPayment = submissionPayment;

            _submissionRepository.UpdateSubmission(aSubmission);

        }

        public bool IsSubmissionPaymentRequired(SubmissionPaymentRequirement submissionPaymentRequirement, int submissionId)
        {

            Log.Information("Checking if payment is required submission id "  + submissionId + " and submission payment requirement is " + submissionPaymentRequirement);

            if (submissionPaymentRequirement == SubmissionPaymentRequirement.EVERYONE)
            {

                //Everyone has to pay to submit no matter what their membership
                return true;

            }
            else if (submissionPaymentRequirement == SubmissionPaymentRequirement.NONE)
            {

                //No one has to pay to submit
                return false;

            }
            else
            {

                Log.Information("Payment is required for non-members so about to check membership status of submitter and main presenter");

                List<SubmissionParticipant> submissionParticipants = _submissionParticipantService.GetSubmissionParticipants(submissionId);

                bool isMemberOnSubmission = false;

                foreach (SubmissionParticipant submissionParticipant in submissionParticipants) {

                    List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles = submissionParticipant.SubmissionParticipantToParticipantRolesLink;

                    foreach( SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipantToParticipantRoles) {

                        if ( submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName == ParticipantRoleType.SUBMITTER || 
                            submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER) {

                            //if the participant role is submitter or lead presenter we need to check if that person is a member
                            //if that person is a member then no payment is needed.

                            bool isMember = _salesforceAPIService.IsSTFMMember( submissionParticipant.Participant.StfmUserId).Result;

                            if ( isMember == true) {

                                isMemberOnSubmission = true;

                            }

                        }
                    }


                }


                if (!isMemberOnSubmission)
                {

                    Log.Information("Submitter or lead presenter does not have an STFM membership.");

                    return true;

                }
                else
                {

                    Log.Information("Submitter or lead presenter does have an STFM membership.");

                    //Logged in user does have an STFM membership and no payment is required
                    return false;

                }

            }

        }

       


        public bool DisplayPaymentRequirementText(IEnumerable<SelectListItem> submissionCategoryItems, string STFMUserId)
        {

            bool displayPaymentRequirementText = false;

            List<SubmissionPaymentRequirement> submissionPaymentRequirements = new List<SubmissionPaymentRequirement>();

            foreach (SelectListItem categoryItem in submissionCategoryItems)
            {

                SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(Int32.Parse(categoryItem.Value));

                submissionPaymentRequirements.Add(submissionCategory.SubmissionPaymentRequirement);

            }

            if (submissionPaymentRequirements.Any(mn => mn == SubmissionPaymentRequirement.EVERYONE))
            {


                displayPaymentRequirementText = true;


            }
            else if (submissionPaymentRequirements.Any(mn => mn == SubmissionPaymentRequirement.NON_MEMBER_ONLY))
            {

                displayPaymentRequirementText = true;

                //need to check if user is an STFM member since payment requirement is for non-members

                bool isMember = _salesforceAPIService.IsSTFMMember(STFMUserId).Result;

                if (isMember)
                {

                    //user is an STFM member so change payment requirement to false

                    displayPaymentRequirementText = false;


                }



            }

            Log.Information("For the logged in user " + STFMUserId + " view page should display the payment requirement text: " + displayPaymentRequirementText);

            return displayPaymentRequirementText;


        }

        public SubmissionPrintViewModel GetSubmissionPrintViewModel(int submissionId)
        {
            var model = new SubmissionPrintViewModel();

            Submission submission = GetSubmission(submissionId);

            model.ConferenceName = submission.Conference.ConferenceLongName;
            model.SubmissionId = submissionId;
            model.SubmissionTitle = submission.SubmissionTitle;
            model.SubmissionCategory = submission.SubmissionCategory.SubmissionCategoryName;
            model.AcceptedCategory = submission.AcceptedCategory.SubmissionCategoryName;


            if (submission.ConferenceSession != null)
            {
                model.sessionCode = submission.ConferenceSession.SessionCode;

                model.sessionStartDatetime = submission.ConferenceSession.SessionStartDateTime;

                model.sessionEndDatetime = submission.ConferenceSession.SessionEndDateTime;

                model.sessionLocation = submission.ConferenceSession.SessionLocation;

                model.sessionTrack = submission.ConferenceSession.SessionTrack;


            }



            model.SubmissionAbstract = submission.SubmissionAbstract;
            model.SubmissionFormFields = _formFieldRepository.GetFormFieldsForSubmission(submissionId).ToList();
            var stfmUserIds = submission.ParticipantLink.Select(p => p.Participant.StfmUserId).ToList();
            var users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result;

            model.Presenters = submission.ParticipantLink
                .Where(p => p.SubmissionParticipantToParticipantRolesLink.Count > 1 || p.SubmissionParticipantToParticipantRolesLink.Any(sp => sp.ParticipantRole.ParticipantRoleName == ParticipantRoleType.SUBMITTER) == false)
                .Select(p => new Presenter
                {
                    SubmissionParticipantId = p.SubmissionParticipantId,
                    SubmissionParticipantToParticipantRolesLink = p.SubmissionParticipantToParticipantRolesLink,
                    FirstName = users.FirstOrDefault(u => u.STFMUserId == p.Participant.StfmUserId).FirstName,
                    LastName = users.FirstOrDefault(u => u.STFMUserId == p.Participant.StfmUserId).LastName,
                    FullName = users.FirstOrDefault(u => u.STFMUserId == p.Participant.StfmUserId).GetFullNamePretty(),
                    SortOrder = p.SortOrder
                })
                .OrderBy(p => p.SortOrder)
                .ToList();

            return model;
        }


        public Submission CreateSubmission(BaseSubmissionBindingModel baseSubmissionBindingModel,
                                                   List<SubmissionParticipant> submissionParticipants,
                                                   Models.Conference conference)
        {
            //Create the Submission object 
            return new Submission
            {

                AcceptedCategoryId = baseSubmissionBindingModel.SelectedSubmissionCategory,
                Conference = conference,
                SubmissionAbstract = baseSubmissionBindingModel.SubmissionAbstract,
                SubmissionCategoryId = baseSubmissionBindingModel.SelectedSubmissionCategory,
                SubmissionCreatedDateTime = DateTime.Now,
                SubmissionLastUpdatedDateTime = DateTime.Now,
                SubmissionStatus = _submissionStatusService.GetBySubmissionStatusType(SubmissionStatusType.OPEN),
                SubmissionTitle = baseSubmissionBindingModel.SubmissionTitle,
                ParticipantLink = submissionParticipants

            };

        }

        public List<Presenter> BuildPresentersListFromSubmissionParticipants(List<SubmissionParticipant> submissionParticipants)
        {
            //Foreach SubmissionParticipant, get his/her Salesforce user information.
            //Then build out a new Presenter object for each user returned from Salesforce
            return _salesforceAPIService
                .GetMultipleUserInfo(submissionParticipants.Select(s => s.Participant.StfmUserId)).Result
                .Select(u => new Presenter {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    FullName = u.GetFullNamePretty(),
                    SortOrder = submissionParticipants.FirstOrDefault(p => p.Participant.StfmUserId == u.STFMUserId).SortOrder,
                    SubmissionParticipantId = submissionParticipants.FirstOrDefault(p => p.Participant.StfmUserId == u.STFMUserId).SubmissionParticipantId,
                    SubmissionParticipantToParticipantRolesLink = submissionParticipants.FirstOrDefault(p => p.Participant.StfmUserId == u.STFMUserId).SubmissionParticipantToParticipantRolesLink
                }).ToList();
        }

        public List<SubmissionFormField> GetSubmissionFormFields(int submissionId)
        {
            return _formFieldRepository.GetFormFieldsForSubmission(submissionId).ToList();
        }

        /// <summary>
        /// Gets the incomplete submissions for a specific <paramref name="conferenceId"/>.
        /// </summary>
        /// <returns>The incomplete submissions.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        public IncompleteSubmissionsViewModel GetIncompleteSubmissionsViewModel(int conferenceId)
        {
            Models.Conference conference = _conferenceService.GetConference(conferenceId);

            List<Submission> submissions = _submissionRepository.GetSubmissions(false, conferenceId).ToList();

            List<SubmissionIncompleteViewModel> incompleteSubmissions = new List<SubmissionIncompleteViewModel>();


            foreach (Submission submission in submissions) {

                if (submission.SubmissionStatus.SubmissionStatusName == SubmissionStatusType.OPEN)
                {

                    //Screen out submission's with a status of cancelled or withdrawn

                    string submitterStfmUserId = _submissionParticipantService.GetPerson(submission.SubmissionId, ParticipantRoleType.SUBMITTER);

                    SubmissionIncompleteViewModel submissionIncompleteViewModel = new SubmissionIncompleteViewModel()
                    {

                        SubmissionId = submission.SubmissionId,
                        SubmissionTitle = submission.SubmissionTitle,
                        SubmissionCategory = submission.SubmissionCategory,
                        Submitter = _salesforceAPIService.GetUserInfo(submitterStfmUserId).Result

                    };


                    string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(submission.SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

                    if (mainPresenterStfmUserId != null)
                    {
                        Stopwatch stopWatch = new Stopwatch();
                        stopWatch.Start();



                        submissionIncompleteViewModel.MainPresenter = _salesforceAPIService.GetUserInfo(mainPresenterStfmUserId).Result;

                      
                        stopWatch.Stop();

                        // Get the elapsed time as a TimeSpan value.
                        TimeSpan ts = stopWatch.Elapsed;


                        Log.Information("Finished getting submitters sales force user infor.  It took " + ts.Seconds + "." + ts.Milliseconds + " seconds");

                    }
                    else
                    {

                        submissionIncompleteViewModel.MainPresenter = new User();

                    }

                    incompleteSubmissions.Add(submissionIncompleteViewModel);
                }

            }

            var incompleteSubmissionsViewModel = new IncompleteSubmissionsViewModel()
            {

                ConferenceViewModel = new ConferenceViewModel()
                {

                    ConferenceCFPEndDate = conference.ConferenceCFPEndDate,
                    ConferenceId = conferenceId,
                    ConferenceLongName = conference.ConferenceLongName,
                    ConferenceShortName = conference.ConferenceShortName

                },

                Submissions = incompleteSubmissions


            };

            return incompleteSubmissionsViewModel;
        }

       public List<Submission> GetSubmissionsForConference(int conferenceId)
        {

            List<Submission> submissions = _submissionRepository.GetSubmissionsForConference(conferenceId).ToList();


            //Initializing a List that will store all participant StfmUserIds from all submissions
            List<string> stfmUserIds = new List<string>();

            //Foreach submission, get the submitter's STFM User Id and add to the list
            foreach (Submission submission in submissions)
            {

                foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                {

                    List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles = submissionParticipant.SubmissionParticipantToParticipantRolesLink;

                    foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipantToParticipantRoles)
                    {

                        if (submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName == ParticipantRoleType.SUBMITTER || submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER)
                        {

                            stfmUserIds.Add(submissionParticipant.Participant.StfmUserId);

                        }


                    }


                }
            }

            //Call the Salesforce API and ask for all the users
            IEnumerable<User> users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result;

            //Now match the users returned from Salesforce with the Participants in the submissions
            //Note: this code assumes we add a User object property to the Participant object that is dynamically populated with the Salesforce user data
            submissions.ForEach (s => {
                s.ParticipantLink.ForEach (pl => pl.Participant.ParticipantUser = users.FirstOrDefault(u => u.STFMUserId == pl.Participant.StfmUserId));
            }) ;


            return submissions;
                

        }

        public List<Submission> GetSubmissionsForConferenceWithAllParticipants(int conferenceId)
        {

            List<Submission> submissions = _submissionRepository.GetSubmissionsForConference(conferenceId).ToList();


            //Initializing a List that will store all participant StfmUserIds from all submissions
            List<string> stfmUserIds = new List<string>();

            //Foreach submission, get the submitter's STFM User Id and add to the list
            foreach (Submission submission in submissions)
            {

                foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                {

                    stfmUserIds.Add(submissionParticipant.Participant.StfmUserId);

                }

            }

            Log.Information("number of users TO find is " + stfmUserIds.Count());

            var users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result;

            Log.Information("number of users found is " + users.Count());

            //Now match the users returned from Salesforce with the Participants in the submissions
            //Note: this code assumes we add a User object property to the Participant object that is dynamically populated with the Salesforce user data
            submissions.ForEach(s => {
                s.ParticipantLink.ForEach(pl => pl.Participant.ParticipantUser = users.FirstOrDefault(u => u.STFMUserId == pl.Participant.StfmUserId));
            });


            return submissions;


        }

        public List<Submission> GetAcceptedSubmissionsForConference(int conferenceId)
        {

            List<Submission> submissions = _submissionRepository.GetAcceptedSubmissionsForConference(conferenceId).ToList();


            //Initializing a List that will store all participant StfmUserIds from all submissions
            List<string> stfmUserIds = new List<string>();

            //Foreach submission, get the submitter's STFM User Id and add to the list
            foreach (Submission submission in submissions)
            {

                foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                {

                    stfmUserIds.Add(submissionParticipant.Participant.StfmUserId);

                }

            }

            //Get first 500 STFMUserIds

            int processAmount = 500;

            List<string> stfmUserIdsPartial = stfmUserIds.Take(processAmount).ToList();

            //Call the Salesforce API to get first group of users
            IEnumerable<User> partialUsers1 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

            IEnumerable<User> users = partialUsers1;

            //how many stfmUserIds are left to process

            int stfmUserIdsToProcess = stfmUserIds.Count() - processAmount;

            //determine how many times we will need to loop over the remaining
            //STFM User IDs to process all of the remaining, doing processAmount
            //at at time

            double loopsToRunDouble = stfmUserIdsToProcess / processAmount;

            int loopsToRun = (int)Math.Ceiling(loopsToRunDouble);

            //loop over the remaining stfmUserIdsToProcess to get all the Users

            for (int i = 0; i <= loopsToRun; i++)
            {

                int skipAmount = processAmount + (i * processAmount);

                stfmUserIdsPartial = stfmUserIds.Skip(skipAmount).ToList().Take(processAmount).ToList();

                
                //Call the Salesforce API to get second group of users
                IEnumerable<User> partialUsers2 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

                //Join the two collections

                users = users.Union(partialUsers2);


            }

            //Now match the users returned from Salesforce with the Participants in the submissions
            //Note: this code assumes we add a User object property to the Participant object that is dynamically populated with the Salesforce user data
            submissions.ForEach(s => {
                s.ParticipantLink.ForEach(pl => pl.Participant.ParticipantUser = users.FirstOrDefault(u => u.STFMUserId == pl.Participant.StfmUserId));
            });


            return submissions;


        }

        public List<Submission> GetAcceptedOnlineSubmissionsForConference(int conferenceId)
        {

            List<Submission> submissions = _submissionRepository.GetAcceptedOnlineSubmissionsForConference(conferenceId).ToList();


            //Initializing a List that will store all participant StfmUserIds from all submissions
            List<string> stfmUserIds = new List<string>();

            //Foreach submission, get the submitter's STFM User Id and add to the list
            foreach (Submission submission in submissions)
            {

                foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                {

                    stfmUserIds.Add(submissionParticipant.Participant.StfmUserId);

                }

            }

            //Get first 500 STFMUserIds

            int processAmount = 500;

            List<string> stfmUserIdsPartial = stfmUserIds.Take(processAmount).ToList();

            //Call the Salesforce API to get first group of users
            IEnumerable<User> partialUsers1 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

            IEnumerable<User> users = partialUsers1;

            //how many stfmUserIds are left to process

            int stfmUserIdsToProcess = stfmUserIds.Count() - processAmount;

            //determine how many times we will need to loop over the remaining
            //STFM User IDs to process all of the remaining, doing processAmount
            //at at time

            double loopsToRunDouble = stfmUserIdsToProcess / processAmount;

            int loopsToRun = (int)Math.Ceiling(loopsToRunDouble);

            //loop over the remaining stfmUserIdsToProcess to get all the Users

            for (int i = 0; i <= loopsToRun; i++)
            {

                int skipAmount = processAmount + (i * processAmount);

                stfmUserIdsPartial = stfmUserIds.Skip(skipAmount).ToList().Take(processAmount).ToList();


                //Call the Salesforce API to get second group of users
                IEnumerable<User> partialUsers2 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

                //Join the two collections

                users = users.Union(partialUsers2);


            }

            //Now match the users returned from Salesforce with the Participants in the submissions
            //Note: this code assumes we add a User object property to the Participant object that is dynamically populated with the Salesforce user data
            submissions.ForEach(s => {
                s.ParticipantLink.ForEach(pl => pl.Participant.ParticipantUser = users.FirstOrDefault(u => u.STFMUserId == pl.Participant.StfmUserId));
            });


            return submissions;


        }


        public List<Submission> GetVirtualConferenceSubmissions(int conferenceId)
        {

            List<Submission> submissions = _submissionRepository.GetVirtualConferenceSubmissions(conferenceId).ToList();


            //Initializing a List that will store all participant StfmUserIds from all submissions
            List<string> stfmUserIds = new List<string>();

            //Foreach submission, get the submitter's STFM User Id and add to the list
            foreach (Submission submission in submissions)
            {

                foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                {

                    stfmUserIds.Add(submissionParticipant.Participant.StfmUserId);

                }

            }

            //Get first 500 STFMUserIds

            int processAmount = 500;

            List<string> stfmUserIdsPartial = stfmUserIds.Take(processAmount).ToList();

            //Call the Salesforce API to get first group of users
            IEnumerable<User> partialUsers1 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

            IEnumerable<User> users = partialUsers1;

            //how many stfmUserIds are left to process

            int stfmUserIdsToProcess = stfmUserIds.Count() - processAmount;

            //determine how many times we will need to loop over the remaining
            //STFM User IDs to process all of the remaining, doing processAmount
            //at at time

            double loopsToRunDouble = stfmUserIdsToProcess / processAmount;

            int loopsToRun = (int)Math.Ceiling(loopsToRunDouble);

            //loop over the remaining stfmUserIdsToProcess to get all the Users

            for (int i = 0; i <= loopsToRun; i++)
            {

                int skipAmount = processAmount + (i * processAmount);

                stfmUserIdsPartial = stfmUserIds.Skip(skipAmount).ToList().Take(processAmount).ToList();


                //Call the Salesforce API to get second group of users
                IEnumerable<User> partialUsers2 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

                //Join the two collections

                users = users.Union(partialUsers2);


            }

            //Now match the users returned from Salesforce with the Participants in the submissions
            //Note: this code assumes we add a User object property to the Participant object that is dynamically populated with the Salesforce user data
            submissions.ForEach(s => {
                s.ParticipantLink.ForEach(pl => pl.Participant.ParticipantUser = users.FirstOrDefault(u => u.STFMUserId == pl.Participant.StfmUserId));
            });


            return submissions;


        }

        public IEnumerable<Submission> GetCompleteSubmissions(string stfmUserId)
        {
            return _submissionRepository.GetSubmissions(stfmUserId);
        }

        public List<Submission> GetSubmissions(IEnumerable<int> submissionIds)
        {
            Log.Information("In SubmissionService - Get Submissions...");

            Log.Information("Submission IDs provided: " + string.Join(",", submissionIds));

            List<Submission> submissions = _submissionRepository.GetSubmissions(submissionIds).ToList();

            Log.Information("Got " + submissions.Count + " from submission repository");

            //Initializing a List that will store all participant StfmUserIds from all submissions
            List<string> stfmUserIds = new List<string>();

            //Foreach submission, get the submitter's STFM User Id and add to the list
            foreach (Submission submission in submissions)
            {

                foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
                {

                    stfmUserIds.Add(submissionParticipant.Participant.StfmUserId);

                }

            }

            //Get first 400 STFMUserIds

            int processAmount = 500;

            List<string> stfmUserIdsPartial = stfmUserIds.Take(processAmount).ToList();

            //Call the Salesforce API to get first group of users
            IEnumerable<User> partialUsers1 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

            IEnumerable<User> users = partialUsers1;

            //how many stfmUserIds are left to process

            int stfmUserIdsToProcess = stfmUserIds.Count() - processAmount;

            //determine how many times we will need to loop over the remaining
            //STFM User IDs to process all of the remaining, doing processAmount
            //at at time

            double loopsToRunDouble = stfmUserIdsToProcess / processAmount;

            int loopsToRun = (int)Math.Ceiling(loopsToRunDouble);

            //loop over the remaining stfmUserIdsToProcess to get all the Users

            for (int i = 0; i < loopsToRun; i++)
            {
                stfmUserIdsPartial = stfmUserIds.Skip(processAmount).ToList().Take(processAmount).ToList();

                //Call the Salesforce API to get second group of users
                IEnumerable<User> partialUsers2 = _salesforceAPIService.GetMultipleUserInfo(stfmUserIdsPartial).Result;

                //Join the two collections

                users = users.Union(partialUsers2);


            }

            //Now match the users returned from Salesforce with the Participants in the submissions
            //Note: this code assumes we add a User object property to the Participant object that is dynamically populated with the Salesforce user data
            submissions.ForEach(s => {
                s.ParticipantLink.ForEach(pl => pl.Participant.ParticipantUser = users.FirstOrDefault(u => u.STFMUserId == pl.Participant.StfmUserId));
            });


            return submissions;
        }

        public bool UpdateSubmissionAcceptedCategory(int submissionId, int submissionCategoryId)
        {
            Submission submission = GetSubmission(submissionId);

            submission.AcceptedCategoryId = submissionCategoryId;

            _submissionRepository.UpdateSubmission(submission);

            return true;
        }

        /// <summary>
        /// Gets the submission category items for the provided conference id.
        /// </summary>
        /// <returns>The submission category items.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        private List<SelectListItem> GetSubmissionCategoryItems(int conferenceId)
        {

            List<SubmissionCategoryViewModel> submissionCategoryViewModelList = _SubmissionCategoryService.GetActiveSubmissionCategories(conferenceId);

            List<SelectListItem> submissionCategoryItems = submissionCategoryViewModelList.Select(sc => new SelectListItem
            {
                Value = sc.SubmissionCategoryId.ToString(),
                Text = sc.SubmissionCategoryName

            }).ToList();

            return submissionCategoryItems;

        }


        public IEnumerable<SubmissionUpdate> GetAcceptedSubmissionsForCategory(int acceptCategoryId)
        {

            IEnumerable<SubmissionUpdate> submissions = _submissionUpdateRepository.getSubmissionUpdates(acceptCategoryId);

            return submissions;

        }

        public void SetParticipantOrderModifiedToTrue(int submissionId)
        {
            _submissionRepository.SetParticipantOrderModifiedToTrue(submissionId);
        }

        public List<int> GetSubmissionsWhoseParticipantOrderHasBeenModified(int conferenceId)
        {
            return _submissionRepository.GetSubmissionsWhoseParticipantOrderHasBeenModified(conferenceId);
        }

        public bool SubmissionExists(int submissionId)
        {
            return _submissionRepository.SubmissionExists(submissionId);
        }

        public List<SubmissionSummaryViewModel> GetCurrentCeraSubmissions(string sTFMUserId)
        {
            //Get all submissions for this STFM User
            List<SubmissionSummaryViewModel> allSubmissions = GetSubmissions(sTFMUserId);

            //Filter all the submissions to create a new List of those submissons
            //where the conference end date has not passed and submission status is 
            //not canceled and submission status is not withdrawn and the submission is
            //for a cera conference
            List<SubmissionSummaryViewModel> currentSubmissions = allSubmissions.Where(
                           s => s.ConferenceEndDate.CompareTo(DateTime.Today) >= 0 &&
                           s.SubmissionStatusName != SubmissionStatusType.CANCELED &&
                           s.SubmissionStatusName != SubmissionStatusType.WITHDRAWN &&
                           s.ConferenceLongName.Contains("CERA")).ToList();


            return currentSubmissions;
        }

        public List<SubmissionForRegistrationReport> GetSubmissionsForConferenceRegistrationReport(int conferenceId)
        {
            return _submissionRepository.GetSubmissionsForConferenceRegistrationReport(conferenceId);
        }

        public SessionsWithNoRegistrationsReportViewModel GetSessionsWithNoRegistrationsReportViewModel(int conferenceId, string eventId, string eventName)
        {
            var result = new SessionsWithNoRegistrationsReportViewModel();

            result.ConferenceName = _conferenceService.GetConferenceName(conferenceId);
            result.EventName = eventName;

            //Get all submissions for the conference that are accepted and have a session
            var submissionsForRegistrationReport = _submissionRepository.GetSubmissionsForConferenceRegistrationReport(conferenceId);

            //Get the registered event attendees from Salesforce
            var eventAttendeesSTFMUserIds = _salesforceAPIService.GetEventAttendees(eventId).Result
                .Select(x => x.STFMUserId)
                .Distinct().ToList();

            //Get the submission Ids for any submissions that have zero registered attendees
            var submissionsWithNoRegisteredPrenterIds = submissionsForRegistrationReport
                .Where(x => !eventAttendeesSTFMUserIds.Intersect(x.PresentersForRegistrationReport.Select(y => y.STFMUserId).ToList()).Any())
                .Select(x => x.SubmissionId);

            if (!submissionsWithNoRegisteredPrenterIds.Any())
            {
                return result;
            }

            //Get the STFMUserIds of the lead presenter and submitter for each submission with zero registered attendees
            var leadPresenterSTFMUserIds = new List<string>();
            submissionsForRegistrationReport.Where(x => submissionsWithNoRegisteredPrenterIds.Contains(x.SubmissionId))
               .ToList()
               .ForEach(x => leadPresenterSTFMUserIds.AddRange(x.PresentersForRegistrationReport.Where(y => y.Roles.Any(z => z == ParticipantRoleType.LEAD_PRESENTER || z == ParticipantRoleType.SUBMITTER)).Select(y => y.STFMUserId).ToList()));

            //Get the user info for the lead presenters and submitters from Salesforce
            var users = _salesforceAPIService.GetMultipleUserInfo(leadPresenterSTFMUserIds.Distinct()).Result;

            submissionsWithNoRegisteredPrenterIds.ToList().ForEach(x => {
                var submission = submissionsForRegistrationReport.FirstOrDefault(y => y.SubmissionId == x);

                var leadPresenter = users.FirstOrDefault(y => y.STFMUserId == submission.PresentersForRegistrationReport.FirstOrDefault(z => z.Roles.Any(a => a == ParticipantRoleType.LEAD_PRESENTER))?.STFMUserId);
                var submitter = users.FirstOrDefault(y => y.STFMUserId == submission.PresentersForRegistrationReport.FirstOrDefault(z => z.Roles.Any(a => a == ParticipantRoleType.SUBMITTER))?.STFMUserId);

                result.SessionsWithNoRegistrations.Add(new SessionWithNoRegistrationViewModel {
                    SubmissionId = submission.SubmissionId,
                    SubmissionTitle = submission.SubmissionTitle,
                    SessionCode = submission.SessionCode,
                    LeadPresenterFullName = leadPresenter?.GetFullNamePretty() ?? "",
                    LeadPresenterEmail = leadPresenter?.Email ?? "",
                    SubmitterFullName = submitter?.GetFullNamePretty() ?? "",
                    SubmitterEmail = submitter?.Email ?? ""
                });
            });

            return result;
        }

        public AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel GetAllSessionsWithPresentersThatHaveNotRegisteredReportViewModel(int conferenceId, string eventId, string eventName)
        {
            var result = new AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel();

            result.ConferenceName = _conferenceService.GetConferenceName(conferenceId);
            result.EventName = eventName;

            //Get all submissions for the conference that are accepted and have a session
            var submissionsForRegistrationReport = _submissionRepository.GetSubmissionsForConferenceRegistrationReport(conferenceId);

            //Get the registered event attendees from Salesforce
            var eventAttendeesSTFMUserIds = _salesforceAPIService.GetEventAttendees(eventId).Result
                .Select(x => x.STFMUserId)
                .Distinct().ToList();

            //Build a list of all presenter STFM User Ids
            var allPresenterSTFMUserIds = new List<string>();
            submissionsForRegistrationReport.ForEach(x => allPresenterSTFMUserIds.AddRange(x.PresentersForRegistrationReport.Select(y => y.STFMUserId)));

            //Find the presenters that are NOT registered as attendees in Salesforce
            var unregisteredPresenterSTFMUserIds = allPresenterSTFMUserIds.Distinct().Except(eventAttendeesSTFMUserIds).ToList();

            //If there are no unregistered presenters, just return here
            if (!unregisteredPresenterSTFMUserIds.Any())
                return result;

            //Get the user information from Salesforce for the unregistered presenters
            var users = _salesforceAPIService.GetMultipleUserInfo(unregisteredPresenterSTFMUserIds).Result.ToList();

            submissionsForRegistrationReport.ForEach(x => {
                var unregisteredPresenters = x.PresentersForRegistrationReport.Select(y => y.STFMUserId).Intersect(unregisteredPresenterSTFMUserIds);
                if (unregisteredPresenterSTFMUserIds.Any())
                {
                    result.SessionWithUnregisteredPresenters.Add(new SessionWithUnregisteredPresenters {
                        SubmissionId = x.SubmissionId,
                        SessionCode = x.SessionCode,
                        SubmissionTitle = x.SubmissionTitle,
                        UnregisteredPresenters = GetUnregisteredPresenters(users, unregisteredPresenterSTFMUserIds, x.PresentersForRegistrationReport)
                    });
                }
            });

            return result;
        }

        public SessionRegisteredUnregisteredViewModel GetAllSessionsWithPresentersRegisteredUnregistered(int conferenceId, string eventId, string eventName)
        {
            var result = new SessionRegisteredUnregisteredViewModel();

            result.ConferenceName = _conferenceService.GetConferenceName(conferenceId);
            result.EventName = eventName;

            //Get all submissions for the conference that are accepted and have a session
            var submissionsForRegistrationReport = _submissionRepository.GetSubmissionsForConferenceRegistrationReport(conferenceId);

            //Get the registered event attendees from Salesforce
            var eventAttendeesSTFMUserIds = _salesforceAPIService.GetEventAttendees(eventId).Result
                .Select(x => x.STFMUserId)
                .Distinct().ToList();

            foreach(SubmissionForRegistrationReport submission in submissionsForRegistrationReport )
            {

                SessionRegisteredUnRegistered sessionRegisteredUnRegistered = new SessionRegisteredUnRegistered
                {
                    SessionCode = submission.SessionCode,
                    SubmissionId = submission.SubmissionId,
                };

                int countRegistered = 0;
                int countUnregistered = 0;

                foreach(PresenterForRegistrationReport presentersForRegistrationReport in submission.PresentersForRegistrationReport )
                {

                    if (eventAttendeesSTFMUserIds.Contains(presentersForRegistrationReport.STFMUserId) )
                    {
                        countRegistered++;

                    } else
                    {
                        countUnregistered++;
                    }

                }

                sessionRegisteredUnRegistered.Registered = countRegistered;
                sessionRegisteredUnRegistered.Unregistered = countUnregistered;

                result.SessionRegisteredUnregisteredList.Add(sessionRegisteredUnRegistered);

            }

            return result;
        }

        #region Helper Methods

        private List<UnregisteredPresenter> GetUnregisteredPresenters(List<User> users, List<string> presentersNotRegisteredForConferenceSTFMUserIds, List<PresenterForRegistrationReport> submissionPresenters)
        {
            var unregisteredSubmissionPresenterSTFMUserIds = submissionPresenters.Select(x => x.STFMUserId).Intersect(presentersNotRegisteredForConferenceSTFMUserIds).ToList();
            var unregisteredSubmissionPresentersAsUsers = users.Where(x => unregisteredSubmissionPresenterSTFMUserIds.Contains(x.STFMUserId));

            var results = unregisteredSubmissionPresentersAsUsers.Select(x => new UnregisteredPresenter {
                FullName = x?.GetFullNamePretty() ?? string.Empty,
                Email = x?.Email ?? string.Empty,
                Roles = submissionPresenters.FirstOrDefault(y => y.STFMUserId == (x?.STFMUserId ?? string.Empty))?.Roles ?? new List<ParticipantRoleType>()
            }).ToList();

            return results;

        }

        public void AddSubmissionPresentationMethods(int conferenceId)
        {
            List<Submission> submissions = GetAcceptedSubmissionsForConference(conferenceId);

            foreach(Submission submission in submissions)
            {
                AddSubmissionPresentationMethods(submission);

            }
        }

        public void AddSubmissionPresentationsToSubmission(int submissionId)
        {
            Submission submission = GetSubmission(submissionId);

            AddSubmissionPresentationMethods(submission);

        }

        private void AddSubmissionPresentationMethods(Submission submission)
        {
            int acceptedSubmissionCategoryId = submission.AcceptedCategoryId;

            SubmissionCategory submissionCategory = _submissionCategoryService.GetSubmissionCategory(acceptedSubmissionCategoryId);

            List<SubmissionPresentationMethod> submissionPresentationMethods = _submissionPresentationMethodRepository.GetSubmissionPresentationMethods(submission.SubmissionId).ToList();

            if (submissionPresentationMethods == null || submissionPresentationMethods.Count == 0)
            {

                int submissionId = submission.SubmissionId;

                foreach (SubmissionCategoryToPresentationMethod submissionCategoryToPresentationMethod in submissionCategory.SubmissionCategoryToPresentationMethodsLink)
                {

                    int presentationMethodId = submissionCategoryToPresentationMethod.PresentationMethodId;

                    int presentationStatusId = 1;

                    if (presentationMethodId > 1)
                    {
                        presentationStatusId = 2;
                    }

                    SubmissionPresentationMethod submissionPresentationMethod = new SubmissionPresentationMethod
                    {
                        SubmissionId = submissionId,
                        PresentationMethodId = presentationMethodId,
                        PresentationStatusId = presentationStatusId
                    };

                    int submissionPresentationMethodId = _submissionPresentationMethodRepository.AddSubmissionPresentationMethod(submissionPresentationMethod);


                }



            }
        }

        public void UpdateSubmissionPresentationMethodStatus(int submissionId, PresentationMethodType presentationMethodType, PresentationStatusType presentationStatusType)
        {
            _submissionRepository.UpdateSubmissionPresentationMethodStatus(submissionId, presentationMethodType, presentationStatusType);
        }

        public void RemoveSubmissionPresentationMethods(int submissionId)
        {
          

            List<SubmissionPresentationMethod> submissionPresentationMethods = 
                _submissionPresentationMethodRepository.GetSubmissionPresentationMethods(submissionId).ToList();

            foreach (SubmissionPresentationMethod submissionPresentationMethod in submissionPresentationMethods)
            {

                _submissionPresentationMethodRepository.DeleteSubmissionPresentationMethod(submissionPresentationMethod.SubmissionPresentationMethodId);

            }


        }

        #endregion
    }
}