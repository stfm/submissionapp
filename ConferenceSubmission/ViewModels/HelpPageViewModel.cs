﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class HelpPageViewModel
    {
        public HelpPageViewModel()
        {
            FAQs = new List<FAQ>();
        }
        public List<FAQ> FAQs { get; set; }
    }
}
