﻿using System.Collections.Generic;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsReviewStatusViewModel
    {

        public SubmissionsReviewStatusViewModel() { }

        public string ConferenceLongName { get; set; }

        public int ConferenceId { get; set; }

        public List<SubmissionReviewStatus> SubmissionsReviewStatus {get; set;}
    }
}
