﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs.SalesforceEvent
{
    public class EventAttendee
    {
        public string STFMUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Credentials { get; set; }
        public string Email { get; set; }
    }
}
