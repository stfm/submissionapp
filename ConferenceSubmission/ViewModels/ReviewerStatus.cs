﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class ReviewerStatus
    {
        public ReviewerStatus()
        {
        }

        public User Reviewer { get; set; }

        public int NumberOfReviewsCompleted { get; set; }

        public int NumberOfReviewsAssigned { get; set; }


    }
}
