﻿using System;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using System.Collections.Generic;

namespace ConferenceSubmission.ViewModels
{
    public class IncompleteSubmissionsViewModel
    {
        public ConferenceViewModel ConferenceViewModel { get; set; }

        public List<SubmissionIncompleteViewModel> Submissions { get; set; }



        public IncompleteSubmissionsViewModel()
        {
            Submissions = new List<SubmissionIncompleteViewModel>();

        }
    }
}
