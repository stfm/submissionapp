# Conference Submission Web App

This web application is used by people creating and editing a submission to a conference.

## Build Instructions

git clone https://bphillip@bitbucket.org/stfm/submissionapp.git

cd submissionapp/

dotnet restore

cd ConferenceSubmission/

dotnet user-secrets set DefaultConnection "Data Source=4.7.64.67,1433;Database=stfmdbv2;User Id=bruce;Password=ltkpapwe” (use the value for
the database connection for your local environment)

dotnet ef database update

In Program.cs edit the database connection string to match the connection string you setup for user-secrets above.  Then the Logs table will be written
in the same database.

Run the following SQL to create the Logs table if you are NOT using the 4.7.64.67 database server:

CREATE TABLE [Logs] (

   [Id] int IDENTITY(1,1) NOT NULL,
   [Message] nvarchar(max) NULL,
   [MessageTemplate] nvarchar(max) NULL,
   [Level] nvarchar(128) NULL,
   [TimeStamp] datetimeoffset(7) NOT NULL,  -- use datetime for SQL Server pre-2008
   [Exception] nvarchar(max) NULL,
   [Properties] xml NULL,
   [LogEvent] nvarchar(max) NULL

   CONSTRAINT [PK_Logs] 
     PRIMARY KEY CLUSTERED ([Id] ASC) 
	 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF,
	       ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
     ON [PRIMARY]

) ON [PRIMARY];