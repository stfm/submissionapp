﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs
{
    public class SubmissionWithNameAndSessionCode
    {
        public int SubmissionId { get; set; }
        public string SubmissionTitle { get; set; }
        public string SessionCode { get; set; }
    }
}
