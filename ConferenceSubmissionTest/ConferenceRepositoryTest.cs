﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmissionTest
{


    public class ConferenceRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public ConferenceRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }


        [Fact]
        public void GetConferenceTypesTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                IEnumerable<ConferenceType> conferenceTypes = conferenceRepository.GetConferenceTypes;

                foreach (var conferenceType in conferenceTypes)
                {

                    Output.WriteLine("Conference type is " + conferenceType.ConferenceTypeName);

                }

                Assert.NotNull(conferenceTypes);

                Assert.Equal(3, conferenceTypes.Count());


            }
        }


        [Fact]
        public void GetConferenceTypeByIdTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                ConferenceType conferenceType = conferenceRepository.GetConferenceType(1);

                Output.WriteLine("Conference type name is " + conferenceType.ConferenceTypeName);

                Assert.Equal("Annual", conferenceType.ConferenceTypeName);


            }

        }


        [Fact]
        public void GetConferenceTypeByNameTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                ConferenceType conferenceType = conferenceRepository.GetConferenceType("Medical Student Education");

                Output.WriteLine("Conference type name is " + conferenceType.ConferenceTypeName);

                Assert.Equal("Medical Student Education", conferenceType.ConferenceTypeName);


            }

        }


        [Fact]
        public void GetConferencesTest()
        {


            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                IEnumerable<Conference> conferences = conferenceRepository.GetConferences;

                foreach (var conference in conferences)
                {

                    Output.WriteLine("Conference name is " + conference.ConferenceLongName);

                    Output.WriteLine("Conference CFP end date is " + conference.ConferenceCFPEndDate);

                    Output.WriteLine("Conference submission review deadline is " + conference.ConferenceSubmissionReviewEndDate);

                }

                Assert.NotNull(conferences);

                Assert.Equal(3, conferences.Count());


            }

        }

        [Fact]
        public void GetConferenceByIdTest()
        {


            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                Conference conference = conferenceRepository.GetConference(100);

                Output.WriteLine("Conference name is " + conference.ConferenceLongName);


                Assert.NotNull(conference);

                Assert.Equal("2019 Medical Student Education Conference", conference.ConferenceLongName);


            }

        }


        [Fact]
        public void GetActiveConferencesTest()
        {


            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                IEnumerable<Conference> conferences = conferenceRepository.GetActiveConferences();

                foreach (var conference in conferences)
                {

                    Output.WriteLine("Conference name is " + conference.ConferenceLongName);

                }

                Assert.NotNull(conferences);

                Assert.Equal(2, conferences.Count());


            }

        }

        [Fact]
        public void GetConferenceByShortNameTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                Conference conference = conferenceRepository.GetConference("AN19");

                Output.WriteLine("Conference name is " + conference.ConferenceLongName);

                Assert.NotNull(conference);

                Assert.Equal("2019 Annual Conference", conference.ConferenceLongName);


            }

        }

        [Fact]
        public void GetActiveConferencesWithOpenReviewStatusTest()
        {
            //arrange
            var options = GetDbContextOptionsForTestingGettingConferenceWithOpenReviewStatus();
            var activeConferences = new List<Conference>();
            using (var context = new AppDbContext(options))
            {

              ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                //act
                activeConferences = conferenceRepository.GetConferencesWithOpenReviewStatus().ToList();
            }

            //assert
            Assert.Equal(3, activeConferences.Count);
        }

        [Fact]
        public void GetConferencesWithUnstartedStatus_StubAListWith3UnstartedConferences_VerifyResults()
        {
            //arrange
            var conferences = new List<Conference> {
                new Conference
                {
                    ConferenceId = 1,
                    ConferenceStartDate = DateTime.Now.AddDays(1),
                    ConferenceEndDate = DateTime.Now.AddDays(5),
                    ConferenceLongName = "Medical Student Education Conference",
                    ConferenceShortName = "MSE",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 1
                },
                new Conference
                {
                    ConferenceId = 2,
                    ConferenceStartDate = DateTime.Now.AddSeconds(60),
                    ConferenceEndDate = DateTime.Now.AddDays(5),
                    ConferenceLongName = "Annual Spring Conference",
                    ConferenceShortName = "AN",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 2
                },
                new Conference
                {
                    ConferenceId = 3,
                    ConferenceStartDate = DateTime.Now.AddDays(5),
                    ConferenceEndDate = DateTime.Now.AddDays(10),
                    ConferenceLongName = "Conference on Quality and Practice Improvement",
                    ConferenceShortName = "CPQI",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 3
                }
            };
            var options = GetDbContextOptionsForTestingGettingConferenceWithUnstartedStatus(conferences);
            var results = new List<Conference>();

            using (var context = new AppDbContext(options))
            {
                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                //act
                results = conferenceRepository.GetConferencesWithUnstartedStatus().ToList();

            }

            //assert
            Assert.Equal(3, results.Count);
            Assert.Contains(results, r => r.ConferenceId == 1);
            Assert.Contains(results, r => r.ConferenceId == 2);
            Assert.Contains(results, r => r.ConferenceId == 3);
        }

        [Fact]
        public void GetConferencesWithUnstartedStatus_StubAListWithNoUnstartedConferences_VerifyResults()
        {
            //arrange
            var conferences = new List<Conference> {
                new Conference
                {
                    ConferenceId = 1,
                    ConferenceStartDate = DateTime.Now.AddDays(-1),
                    ConferenceEndDate = DateTime.Now.AddDays(-5),
                    ConferenceLongName = "Medical Student Education Conference",
                    ConferenceShortName = "MSE",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 1
                },
                new Conference
                {
                    ConferenceId = 2,
                    ConferenceStartDate = DateTime.Now.AddSeconds(-60),
                    ConferenceEndDate = DateTime.Now.AddDays(5),
                    ConferenceLongName = "Annual Spring Conference",
                    ConferenceShortName = "AN",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 2
                },
                new Conference
                {
                    ConferenceId = 3,
                    ConferenceStartDate = DateTime.Now.AddDays(-10),
                    ConferenceEndDate = DateTime.Now.AddDays(-5),
                    ConferenceLongName = "Conference on Quality and Practice Improvement",
                    ConferenceShortName = "CPQI",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 3
                }
            };
            var options = GetDbContextOptionsForTestingGettingConferenceWithUnstartedStatus(conferences);
            var results = new List<Conference>();

            using (var context = new AppDbContext(options))
            {
                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                //act
                results = conferenceRepository.GetConferencesWithUnstartedStatus().ToList();

            }

            //assert
            Assert.Empty(results);
        }

        [Fact]
        public void GetConferencesWithUnstartedStatus_StubAListWith3UnstartedConferencesThatAreAllInactive_VerifyResults()
        {
            //arrange
            var conferences = new List<Conference> {
                new Conference
                {
                    ConferenceId = 1,
                    ConferenceStartDate = DateTime.Now.AddDays(1),
                    ConferenceEndDate = DateTime.Now.AddDays(5),
                    ConferenceLongName = "Medical Student Education Conference",
                    ConferenceShortName = "MSE",
                    ConferenceDeleted = false,
                    ConferenceInactive = true,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 1
                },
                new Conference
                {
                    ConferenceId = 2,
                    ConferenceStartDate = DateTime.Now.AddSeconds(60),
                    ConferenceEndDate = DateTime.Now.AddDays(5),
                    ConferenceLongName = "Annual Spring Conference",
                    ConferenceShortName = "AN",
                    ConferenceDeleted = false,
                    ConferenceInactive = true,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 2
                },
                new Conference
                {
                    ConferenceId = 3,
                    ConferenceStartDate = DateTime.Now.AddDays(5),
                    ConferenceEndDate = DateTime.Now.AddDays(10),
                    ConferenceLongName = "Conference on Quality and Practice Improvement",
                    ConferenceShortName = "CPQI",
                    ConferenceDeleted = false,
                    ConferenceInactive = true,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 3
                }
            };
            var options = GetDbContextOptionsForTestingGettingConferenceWithUnstartedStatus(conferences);
            var results = new List<Conference>();

            using (var context = new AppDbContext(options))
            {
                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                //act
                results = conferenceRepository.GetConferencesWithUnstartedStatus().ToList();

            }

            //assert
            Assert.Equal(3, results.Count);
        }

        [Fact]
        public void GetConferencesWithUnstartedStatus_StubAListWith2UnstartedConferencesAnd1StartedConference_VerifyResults()
        {
            //arrange
            var conferences = new List<Conference> {
                new Conference
                {
                    ConferenceId = 1,
                    ConferenceStartDate = DateTime.Now.AddDays(1),
                    ConferenceEndDate = DateTime.Now.AddDays(5),
                    ConferenceLongName = "Medical Student Education Conference",
                    ConferenceShortName = "MSE",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 1
                },
                new Conference
                {
                    ConferenceId = 2,
                    ConferenceStartDate = DateTime.Now.AddSeconds(60),
                    ConferenceEndDate = DateTime.Now.AddDays(5),
                    ConferenceLongName = "Annual Spring Conference",
                    ConferenceShortName = "AN",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 2
                },
                new Conference
                {
                    ConferenceId = 3,
                    ConferenceStartDate = DateTime.Now.AddDays(-10),
                    ConferenceEndDate = DateTime.Now.AddDays(-5),
                    ConferenceLongName = "Conference on Quality and Practice Improvement",
                    ConferenceShortName = "CPQI",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = DateTime.Now.AddDays(-60),
                    ConferenceSubmissionReviewEndDate = DateTime.Now.AddDays(-30),
                    ConferenceTypeId = 3
                }
            };
            var options = GetDbContextOptionsForTestingGettingConferenceWithUnstartedStatus(conferences);
            var results = new List<Conference>();

            using (var context = new AppDbContext(options))
            {
                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                //act
                results = conferenceRepository.GetConferencesWithUnstartedStatus().ToList();

            }

            //assert
            Assert.Contains(results, r => r.ConferenceId == 1);
            Assert.Contains(results, r => r.ConferenceId == 2);
            Assert.DoesNotContain(results, r => r.ConferenceId == 3);
        }

        [Fact]
        public void GetConferencesByDateRange_VerifyResults()
        {
            //arrange
            var conferences = new List<Conference> {
                new Conference
                {
                    ConferenceId = 1,
                    ConferenceStartDate = new DateTime(2019, 1, 31),
                    ConferenceEndDate = new DateTime(2019, 2, 3),
                    ConferenceLongName = "2019 Medical Student Education Conference",
                    ConferenceShortName = "MSE19",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = new DateTime(2019, 1, 31).AddDays(-60),
                    ConferenceSubmissionReviewEndDate = new DateTime(2019, 1, 31).AddDays(-30),
                    ConferenceTypeId = 1
                },
                new Conference
                {
                    ConferenceId = 2,
                    ConferenceStartDate = new DateTime(2019, 4, 30),
                    ConferenceEndDate = new DateTime(2019, 5, 5),
                    ConferenceLongName = "2019 Annual Spring Conference",
                    ConferenceShortName = "AN19",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = new DateTime(2019, 4, 30).AddDays(-60),
                    ConferenceSubmissionReviewEndDate = new DateTime(2019, 4, 30).AddDays(-30),
                    ConferenceTypeId = 1
                },
                new Conference
                {
                    ConferenceId = 3,
                    ConferenceStartDate = new DateTime(2018, 1, 31),
                    ConferenceEndDate = new DateTime(2018, 2, 3),
                    ConferenceLongName = "2018 Medical Student Education Conference",
                    ConferenceShortName = "MSE18",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = new DateTime(2018, 1, 31).AddDays(-60),
                    ConferenceSubmissionReviewEndDate = new DateTime(2018, 1, 31).AddDays(-30),
                    ConferenceTypeId = 1
                },
                new Conference
                {
                    ConferenceId = 4,
                    ConferenceStartDate = new DateTime(2017, 1, 31),
                    ConferenceEndDate = new DateTime(2017, 2, 3),
                    ConferenceLongName = "2017 Medical Student Education Conference",
                    ConferenceShortName = "MSE17",
                    ConferenceDeleted = false,
                    ConferenceInactive = false,
                    ConferenceCFPEndDate = new DateTime(2017, 1, 31).AddDays(-60),
                    ConferenceSubmissionReviewEndDate = new DateTime(2017, 1, 31).AddDays(-30),
                    ConferenceTypeId = 1
                },
            };
            var options = GetDbContextOptionsForTestingGettingConferenceWithUnstartedStatus(conferences);

            var startDate = new DateTime(2019, 1, 1);
            var endDate = new DateTime(2019, 12, 31);

            var results = new List<EntityByNameAndId>();

            using (var context = new AppDbContext(options))
            {
                ConferenceRepository conferenceRepository = new ConferenceRepository(context);

                //act
                results = conferenceRepository.GetConferencesByDateRange(startDate, endDate).ToList();

            }

            //assert
            Assert.Equal(2, results.Count);
            Assert.Single(results.Where(x => x.EntityName == "MSE19"));
            Assert.Single(results.Where(x => x.EntityName == "AN19"));
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptionsForTestingGettingConferenceWithUnstartedStatus(List<Conference> conferences)
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                var conferenceTypes = new List<ConferenceType> {
                    new ConferenceType
                    {
                        ConferenceTypeId = 1,
                        ConferenceTypeName = "MSE"
                    },
                    new ConferenceType
                    {
                        ConferenceTypeId = 2,
                        ConferenceTypeName = "AN"
                    },
                    new ConferenceType
                    {
                        ConferenceTypeId = 3,
                        ConferenceTypeName = "CPQI"
                    }
                };

                context.ConferenceTypes.AddRange(conferenceTypes);
                context.Conferences.AddRange(conferences);

                context.SaveChanges();
            }

            return options;
        }

        //This context is for testing time-dependent actions that depend on the ConferenceSubmissionReviewEndDate.
        //It creates 5 conferences with different ConferenceSubmissionReviewEndDate:
        //Conference 1:  ConferenceSubmissionReviewEndDate is the current date at 23:59:59
        //Conference 2:  ConferenceSubmissionReviewEndDate is 3 months ahead of the current date at 23:59:59
        //Conference 3:  ConferenceSubmissionReviewEndDate is 3 months behind the current date at 23:59:59
        //Conference 4:  ConferenceSubmissionReviewEndDate is NULL
        //Conference 5:  ConferenceSubmissionReviewEndDate is the current date at 23:59:59. ConferenceInactive is true
        private static DbContextOptions<AppDbContext> GetDbContextOptionsForTestingGettingConferenceWithOpenReviewStatus()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" },
                    new ConferenceType { ConferenceTypeId = 3, ConferenceTypeName = "Conference on Practice Improvement" },
                    new ConferenceType { ConferenceTypeId = 4, ConferenceTypeName = "STFM World Conference" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    //This conference's ConferenceSubmissionReviewEndDate
                    //is set to 23:59:59 of the current date
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Now.AddMonths(3),
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(-3),
                        ConferenceInactive = false,
                        ConferenceLongName = $"{DateTime.Now.AddMonths(3).Year} Medical Student Education Conference",
                        ConferenceShortName = $"MSE{DateTime.Now.AddMonths(3).Year}",
                        ConferenceStartDate = DateTime.Now.AddMonths(3).AddDays(-4),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddHours(23 - DateTime.Now.Hour).AddMinutes(59-DateTime.Now.Minute).AddSeconds(59- DateTime.Now.Second),
                        ConferenceTypeId = 2
                    },

                    //This conference's ConferenceSubmissionReviewEndDate
                    //is set to 23:59:59 of the current date + 3 months
                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Now.AddMonths(6),
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(-3),
                        ConferenceInactive = false,
                        ConferenceLongName = $"{DateTime.Now.AddMonths(6).Year} Medical Student Education Conference",
                        ConferenceShortName = $"MSE{DateTime.Now.AddMonths(6).Year}",
                        ConferenceStartDate = DateTime.Now.AddMonths(6).AddDays(-4),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddMonths(3).AddHours(23 - DateTime.Now.Hour).AddMinutes(59 - DateTime.Now.Minute).AddSeconds(59 - DateTime.Now.Second),
                        ConferenceTypeId = 2
                    },

                    //This conference's ConferenceSubmissionReviewEndDate
                    //is set to 23:59:59 of the current date - 3 months
                    new Conference
                    {
                        ConferenceId = 102,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Now.AddDays(10),
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(-6),
                        ConferenceInactive = false,
                        ConferenceLongName = $"{DateTime.Now.Year} Medical Student Education Conference",
                        ConferenceShortName = $"MSE{DateTime.Now.Year}",
                        ConferenceStartDate = DateTime.Now.AddDays(5),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddMonths(-3).AddHours(23 - DateTime.Now.Hour).AddMinutes(59 - DateTime.Now.Minute).AddSeconds(59 - DateTime.Now.Second),
                        ConferenceTypeId = 3
                    },

                    //This conference's ConferenceSubmissionReviewEndDate
                    //is set to NULL
                    new Conference
                    {
                        ConferenceId = 104,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Now.AddMonths(10),
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(5),
                        ConferenceInactive = false,
                        ConferenceLongName = $"{DateTime.Now.AddMonths(10).Year} Medical Student Education Conference",
                        ConferenceShortName = $"MSE{DateTime.Now.AddMonths(10).Year}",
                        ConferenceStartDate = DateTime.Now.AddMonths(10).AddDays(-5),
                        ConferenceSubmissionReviewEndDate = null,
                        ConferenceTypeId = 4
                    },

                    //This conference's ConferenceSubmissionReviewEndDate
                    //is set to 23:59:59 of the current date.
                    //Its ConferenceInactive date is set to truu
                    new Conference
                    {
                        ConferenceId = 105,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Now.AddMonths(3),
                        ConferenceCFPEndDate = DateTime.Now.AddMonths(-3),
                        ConferenceInactive = true,
                        ConferenceLongName = $"{DateTime.Now.AddMonths(3).Year} Medical Student Education Conference",
                        ConferenceShortName = $"MSE{DateTime.Now.AddMonths(3).Year}",
                        ConferenceStartDate = DateTime.Now.AddMonths(3).AddDays(-4),
                        ConferenceSubmissionReviewEndDate = DateTime.Now.AddHours(23 - DateTime.Now.Hour).AddMinutes(59 - DateTime.Now.Minute).AddSeconds(59 - DateTime.Now.Second),
                        ConferenceTypeId = 2
                    }

                );

                context.SaveChanges();
            }
             
            return options;
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" },
                    new ConferenceType { ConferenceTypeId = 3, ConferenceTypeName = "Conference on Practice Improvement" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceCFPEndDate = DateTime.Parse("Jun 1, 2018"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceSubmissionReviewEndDate = DateTime.Parse("Dec 1, 2018"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceCFPEndDate = DateTime.Parse("Sep 1, 2018"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    },

                    new Conference
                    {
                        ConferenceId = 102,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Nov 15, 2018"),
                        ConferenceCFPEndDate = DateTime.Parse("Apr 1, 2018"),
                        ConferenceInactive = true,
                        ConferenceLongName = "2018 CPI Conference",
                        ConferenceShortName = "CPI19",
                        ConferenceStartDate = DateTime.Parse("Nov 15, 2018"),
                        ConferenceTypeId = 3
                    }

                );

                context.SaveChanges();

            }

            return options;
        }


    }
}
