﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Services
{
    public interface IReviewAdminService
    {

        /// <summary>
        /// Gets the average review score for all reviews
        /// of the provided submission.
        /// </summary>
        /// <returns>The average review score to the nearest tenth (e.g. 2.7) - note that
        /// double value such as 3.45 will be rounded to 3.5.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        double GetAverageReviewScoreForAllReviews(int submissionId);

        /// <summary>
        /// Gets the average review score for single review.
        /// </summary>
        /// <returns>The average review score for single review. Note that
        /// double value such as 3.45 will be rounded to 3.5.</returns>
        /// <param name="reviewId">Review identifier.</param>
        double GetAverageReviewScoreForSingleReview(int reviewId);

        /// <summary>
        /// Gets all the completed reviews for all submissions
        /// in the provided category.
        /// </summary>
        /// <returns>The completed reviews for all submissions in the completed category.</returns>
        /// <param name="categoryId">Category identifier.</param>
        List<SubmissionsAverageReviewScore> GetCompletedReviewsForCategoryWithAverageReviewScore(int categoryId);

        /// <summary>
        /// Gets all the submissions that have a completed review with their
        /// detailed reviews.
        /// </summary>
        /// <returns>The submission detailed reviews.</returns>
        /// <param name="categoryId">Category identifier.</param>
        List<SubmissionDetailedReview> GetSubmissionDetailedReviews(int categoryId);

        /// <summary>
        /// Gets the submission detailed reviews for submission.
        /// </summary>
        /// <returns>The submission detailed reviews for submission.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        List<SubmissionDetailedReview> GetSubmissionDetailedReviewsForSubmission(int submissionId);

        /// <summary>
        /// Gets the reviewers not done reviewing for provided conference ID
        /// </summary>
        /// <returns>The reviewers not done.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        List<ReviewIncomplete> GetReviewersNotDone(int conferenceId);

        /// <summary>
        /// Gets each reviewers number of submissions assigned
        /// and number of submissions review completed.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns></returns>
        List<ReviewerStatus> GetReviewersStatus(int conferenceId);


        /// <summary>
        /// Gets the users with more then two submissions for specific conference
        /// and submission having a specific status.
        /// </summary>
        /// <returns>The users with more then two submissions.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        /// <param name="submissionStatusId">Submission status identifier.</param>
        List<UserMultipleSubmissionsViewModel> GetUsersWithMoreThenTwoSubmissions(int conferenceId, int submissionStatusId);

        List<PotentialReviewer> AddNewReviewerFindPeople(string lastName);

        /// <summary>
        /// Get accepted submissions not assigned to reviewer.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns></returns>
        List<Submission> GetSubmissionsNotAssignedToReviewer(int conferenceId);

        /// <summary>
        /// Get collection of Submission objects where the submission is
        /// completed, assigned to one or more reviewers but no
        /// review is completed.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns>Collection of Submission Objects</returns>
        List<Submission> GetSubmissionsNoReviewsCompleted(int conferenceId);


        /// <summary>
        /// Get all the reviewers assigned to a specific submission
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns>Collection of submission reviewer objects</returns>
        List<SubmissionReviewer> GetAssignedReviewers(int submissionId);

        /// <summary>
        /// Get collection of SubmissionReviewStatus objects that show for each 
        /// submission reviews assigned, reviews completed, reviews not completed
        /// </summary>
        /// <param name="conferenceId">Conference ID</param>
        /// <returns>collection of SubmissionReviewStatus objects</returns>
        List<SubmissionReviewStatus> GetSubmissionsReviewStatus(int conferenceId);
        
        /// <summary>
        /// Get if the reviewer completed the review for the submission.
        /// </summary>
        /// <param name="reviewerId">Reviewer Id</param>
        /// <param name="submissionId">Submission Id</param>
        /// <returns>True if reviewer completed the review of the submission</returns>
        bool GetReviewCompleted(int reviewerId, int submissionId);
    }
}
