﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;

namespace ConferenceSubmissionTest
{
    public class MockDisclosoureService : IDisclosureService
    {
        public MockDisclosoureService()
        {
        }

        public int AddDisclosure(Disclosure disclosure)
        {
            throw new NotImplementedException();
        }

        public Disclosure GetDisclosure(int disclosureId)
        {
            throw new NotImplementedException();
        }

        public Disclosure GetDisclosure(string stfmUserId)
        {
            throw new NotImplementedException();
        }

        public List<Disclosure> GetDisclosures(Submission submission)
        {
            throw new NotImplementedException();
        }

        public bool isDisclosureCurrent(DateTime conferenceStartDate, string stfmUserId)
        {
            return false;
        }

        public bool renameCvFile(Disclosure disclosure)
        {
            throw new NotImplementedException();
        }

        public void UpdateDisclosure(Disclosure disclosure)
        {
            throw new NotImplementedException();
        }
    }
}
