﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class updatesessiondatetime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SessionDateTime",
                table: "ConferenceSessions",
                newName: "SessionStartDateTime");

            migrationBuilder.AddColumn<DateTime>(
                name: "SessionEndDateTime",
                table: "ConferenceSessions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SessionEndDateTime",
                table: "ConferenceSessions");

            migrationBuilder.RenameColumn(
                name: "SessionStartDateTime",
                table: "ConferenceSessions",
                newName: "SessionDateTime");
        }
    }
}
