﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class SessionsForConferenceViewModel
    {
        public List<ConferenceSession> ConferenceSessions       { get; set; }
        public Conference  Conference { get; set; }

        public SortedDictionary<DateOnly, List<RoomUse>> RoomUseDict { get; set; }

    }
}
