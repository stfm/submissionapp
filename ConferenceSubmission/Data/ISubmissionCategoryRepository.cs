﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface ISubmissionCategoryRepository
    {

        /// <summary>
        /// Creates a new SubmissionCategory in the repository using
        /// details from the provided SubmissionCategory
        /// </summary>
        /// <param name="category">SubmissionCategory</param>
        void CreateSubmissionCategory(SubmissionCategory category);

        /// <summary>
        /// Get collection of SubmissionCategory objects
        /// for the provided Conference ID.
        /// </summary>
        /// <returns>The submission categories.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        IEnumerable<SubmissionCategory> GetSubmissionCategories(int conferenceId);

        /// <summary>
        /// Gets the submission category.
        /// </summary>
        /// <returns>The submission category.</returns>
        /// <param name="submissionCategoryId">Submission category identifier.</param>
        SubmissionCategory GetSubmissionCategory(int submissionCategoryId);

        /// <summary>
        /// Gets the submission categories (by conference) that have form fields associated with them    
        /// </summary>
        /// <param name="conferenceId">Conference unique identifier</param>
        /// <returns>List of SubmissionCategory objects</returns>
        List<SubmissionCategory> GetSubmissionCategoriesWithFormFieldsByConference(int conferenceId);


    }
}
