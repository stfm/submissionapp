﻿using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Models a review of a submission
    /// </summary>
    public class Review
    {
        public Review()
        {
        }

        public int ReviewId { get; set; }

        public int ReviewerId { get; set; }

        public Reviewer Reviewer { get; set; }

        public int SubmissionId { get; set; }

        public Submission Submission { get; set; }

        public DateTime ReviewDateTime { get; set; }

        public virtual List<ReviewFormField> ReviewFormFields { get; set; }

        public int ReviewStatusId { get; set; }

        public ReviewStatus ReviewStatus { get; set; }

        /// <summary>
        /// Gets or sets the reviewer average score for this submission.
        /// This value is calculated and not mapped into the database.
        /// </summary>
        /// <value>The reviewer average score.</value>
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public double ReviewerAverageScore { get; set; }


    }
}
