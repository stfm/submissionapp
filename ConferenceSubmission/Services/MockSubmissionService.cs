﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Services
{
    public class MockSubmissionService : ISubmissionService
    {
		public int AddSubmission(Submission submission)
		{
			throw new NotImplementedException();
		}

        public void AddSubmissionPayment(SubmissionPayment submissionPayment)
        {
            throw new NotImplementedException();
        }

        public List<Presenter> BuildPresentersListFromSubmissionParticipants(List<SubmissionParticipant> submissionParticipants)
        {
            throw new NotImplementedException();
        }

        public Submission CreateSubmission(BaseSubmissionBindingModel baseSubmissionBindingModel, List<SubmissionParticipant> submissionParticipants, Models.Conference conference)
        {
            throw new NotImplementedException();
        }

        public bool DisplayPaymentRequirementText(IEnumerable<SelectListItem> submissionCategoryItems, string STFMUserId)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionSummaryViewModel> GetAllSubmissionsNotWithdrawnOrCanceled(string stfmUserId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Submission> GetCompleteSubmissions(string stfmUserId)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionSummaryViewModel> GetCurrentSubmissions(string StfmUserId)
        {
            return new List<SubmissionSummaryViewModel>()
            {
                new SubmissionSummaryViewModel()
                {
                    SubmissionTitle = "Submission Number One",
                    ConferenceCFPEndDate = new DateTime(2018, 5, 19),
                    ConferenceLongName = "2018 Annual Spring Conference",
                    SubmissionId = 12345,
                    SubmissionStatusName = SubmissionStatusType.ACCEPTED
                },
                new SubmissionSummaryViewModel()
                {
                    SubmissionTitle = "Submission Number Two",
                    ConferenceCFPEndDate = new DateTime(2018, 5, 18),
                    ConferenceLongName = "2018 Annual Spring Conference",
                    SubmissionId = 56789,
                    SubmissionStatusName = SubmissionStatusType.WITHDRAWN
                },
                new SubmissionSummaryViewModel()
                {
                    SubmissionTitle = "Submission Number Three",
                    ConferenceCFPEndDate = new DateTime(2019, 2, 5),
                    ConferenceLongName = "2019 Conference on Medical Student Education",
                    SubmissionId = 89751,
                    SubmissionStatusName = SubmissionStatusType.OPEN
                },
                new SubmissionSummaryViewModel()
                {
                    SubmissionTitle = "Submission Number Four",
                    ConferenceCFPEndDate = new DateTime(2019, 2, 6),
                    ConferenceLongName = "2019 Conference on Medical Student Education",
                    SubmissionId = 45672,
                    SubmissionStatusName = SubmissionStatusType.OPEN
                },
                new SubmissionSummaryViewModel()
                {
                    SubmissionTitle = "Submission Number Five",
                    ConferenceCFPEndDate = new DateTime(2018, 12, 3),
                    ConferenceLongName = "2018 Conference on Practice Improvement",
                    SubmissionId = 44523,
                    SubmissionStatusName = SubmissionStatusType.ACCEPTED
                },
                new SubmissionSummaryViewModel()
                {
                    SubmissionTitle = "Submission Number Six",
                    ConferenceCFPEndDate = new DateTime(2018, 12, 2),
                    ConferenceLongName = "2018 Conference on Practice Improvement",
                    SubmissionId = 34487,
                    SubmissionStatusName = SubmissionStatusType.DECLINED
                },
            };
        }

        public IncompleteSubmissionsViewModel GetIncompleteSubmissionsViewModel(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public Submission GetSubmission(int submissionId)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionFormField> GetSubmissionFormFields(int submissionId)
        {
            throw new NotImplementedException();
        }

        public SubmissionPrintViewModel GetSubmissionPrintViewModel(int submissionId)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionSummaryViewModel> GetSubmissions(string StfmUserId)
        {
            throw new NotImplementedException();
        }

        public List<Submission> GetSubmissionsForConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public SubmissionWithProposalAnswersViewModel GetSubmissionWithProposal(int submissionId, string StfmUserId)
        {
            throw new NotImplementedException();
        }

        public SubmissionWithProposalAnswersViewModel GetSubmissionWithProposal(int submissionId)
        {
            throw new NotImplementedException();
        }

        public bool IsSubmissionCategoryProposalCompleted(int submissionId)
        {
            throw new NotImplementedException();
        }

        public bool IsSubmissionPaymentRequired(SubmissionPaymentRequirement submissionPaymentRequirement, int submissionId)
        {
            throw new NotImplementedException();
        }



        public bool UpdateSubmissionBaseDetails(int submissionId, string title, string _abstract, bool adminEditedSubmission)
        {
            throw new NotImplementedException();
        }

        public void UpdateSubmissionCompleted(int submissionId, bool submissionCompleted)
        {
            throw new NotImplementedException();
        }

        public bool UpdateSubmissionStatus(int submissionId, int submissionStatusId)
        {
            throw new NotImplementedException();
        }

        int ISubmissionService.AddSubmission(Submission submission)
        {
            throw new NotImplementedException();
        }

        void ISubmissionService.AddSubmissionPayment(SubmissionPayment submissionPayment)
        {
            throw new NotImplementedException();
        }

        List<Presenter> ISubmissionService.BuildPresentersListFromSubmissionParticipants(List<SubmissionParticipant> submissionParticipants)
        {
            throw new NotImplementedException();
        }

        Submission ISubmissionService.CreateSubmission(BaseSubmissionBindingModel baseSubmissionBindingModel, List<SubmissionParticipant> submissionParticipants, Models.Conference conference)
        {
            throw new NotImplementedException();
        }

        bool ISubmissionService.DisplayPaymentRequirementText(IEnumerable<SelectListItem> submissionCategoryItems, string STFMUserId)
        {
            throw new NotImplementedException();
        }

        List<Submission> ISubmissionService.GetAcceptedSubmissionsForConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        List<Submission> ISubmissionService.GetSubmissions(IEnumerable<int> sessionIds)
        {
            throw new NotImplementedException();
        }

        List<SubmissionSummaryViewModel> ISubmissionService.GetAllSubmissionsNotWithdrawnOrCanceled(string stfmUserId)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Submission> ISubmissionService.GetCompleteSubmissions(string stfmUserId)
        {
            throw new NotImplementedException();
        }

        List<SubmissionSummaryViewModel> ISubmissionService.GetCurrentSubmissions(string stfmUserId)
        {
            throw new NotImplementedException();
        }

        IncompleteSubmissionsViewModel ISubmissionService.GetIncompleteSubmissionsViewModel(int conferenceId)
        {
            throw new NotImplementedException();
        }

        Submission ISubmissionService.GetSubmission(int submissionId)
        {
            throw new NotImplementedException();
        }

        List<SubmissionFormField> ISubmissionService.GetSubmissionFormFields(int submissionId)
        {
            throw new NotImplementedException();
        }

        SubmissionPrintViewModel ISubmissionService.GetSubmissionPrintViewModel(int submissionId)
        {
            throw new NotImplementedException();
        }

        List<SubmissionSummaryViewModel> ISubmissionService.GetSubmissions(string stfmUserId)
        {
            throw new NotImplementedException();
        }

        List<Submission> ISubmissionService.GetSubmissionsForConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        SubmissionWithProposalAnswersViewModel ISubmissionService.GetSubmissionWithProposal(int submissionId, string StfmUserId)
        {
            throw new NotImplementedException();
        }

        SubmissionWithProposalAnswersViewModel ISubmissionService.GetSubmissionWithProposal(int submissionId)
        {
            throw new NotImplementedException();
        }

        bool ISubmissionService.IsSubmissionCategoryProposalCompleted(int submissionId)
        {
            throw new NotImplementedException();
        }

        bool ISubmissionService.IsSubmissionPaymentRequired(SubmissionPaymentRequirement submissionPaymentRequirement, int submissionId)
        {
            throw new NotImplementedException();
        }

        bool ISubmissionService.UpdateSubmissionBaseDetails(int submissionId, string title, string _abstract, bool adminEditedSubmission)
        {
            throw new NotImplementedException();
        }

        void ISubmissionService.UpdateSubmissionCompleted(int submissionId, bool submissionCompleted)
        {
            throw new NotImplementedException();
        }

        bool ISubmissionService.UpdateSubmissionStatus(int submissionId, int submissionStatusId)
        {
            throw new NotImplementedException();
        }

        public List<Submission> GetSubmissionsForConferenceWithAllParticipants(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateSubmissionAcceptedCategory(int submissionId, int submissionCategoryId)
        {
            throw new NotImplementedException();
        }

        public List<Submission> GetAcceptedSubmissionsForCategory(int acceptedCategoryId)
        {
            throw new NotImplementedException();
        }

        IEnumerable<SubmissionUpdate> ISubmissionService.GetAcceptedSubmissionsForCategory(int acceptedCategoryId)
        {
            throw new NotImplementedException();
        }

        public void SetParticipantOrderModifiedToTrue(int submissionId)
        {
            throw new NotImplementedException();
        }

        public List<int> GetSubmissionsWhoseParticipantOrderHasBeenModified(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public bool SubmissionExists(int submissionId)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionSummaryViewModel> GetCurrentCeraSubmissions(string sTFMUserId)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionForRegistrationReport> GetSubmissionsForConferenceRegistrationReport(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public SessionsWithNoRegistrationsReportViewModel GetSessionsWithNoRegistrationsReportViewModel(int conferenceId, string eventId, string eventName)
        {
            throw new NotImplementedException();
        }

        public AllSessionsWithPresentersThatHaveNotRegisteredReportViewModel GetAllSessionsWithPresentersThatHaveNotRegisteredReportViewModel(int conferenceId, string eventId, string eventName)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionSummaryViewModel> GetAllSubmissionsNotCanceled(string stfmUserId)
        {
            throw new NotImplementedException();
        }

        public void AddSubmissionPresentationMethods(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public void UpdateSubmissionPresentationMethodStatus(int submissionId, PresentationMethodType presentationMethodType, PresentationStatusType presentationStatusType)
        {
            throw new NotImplementedException();
        }

        public void AddSubmissionPresentationsToSubmission(int submissionId)
        {
            throw new NotImplementedException();
        }

        public void RemoveSubmissionPresentationMethods(int submissionId)
        {
            throw new NotImplementedException();
        }

        public List<Submission> GetAcceptedOnlineSubmissionsForConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public List<Submission> GetVirtualConferenceSubmissions(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public SessionRegisteredUnregisteredViewModel GetAllSessionsWithPresentersRegisteredUnregistered(int conferenceId, string eventId, string eventName)
        {
            throw new NotImplementedException();
        }
    }
}
