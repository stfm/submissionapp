﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public class CreateAuditLogsService : ICreateAuditLogsService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private List<EntityEntry> _modifiedEntities;
        private List<EntityEntry> _addedEntities;
        private List<EntityEntry> _deletedEntities;
        private string _userId;

        public CreateAuditLogsService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Initialize(IEnumerable<EntityEntry> entityEntries)
        {
            var auditableEntities = entityEntries
                .Where(e => TypeInfo.GetType(e.Metadata.Name).CustomAttributes.FirstOrDefault(a => a.AttributeType.Name == "AuditableAttribute") != null);

            _modifiedEntities = auditableEntities?.Where(e => e.State == EntityState.Modified)?.ToList() ?? new List<EntityEntry>();
            _addedEntities = auditableEntities?.Where(e => e.State == EntityState.Added)?.ToList() ?? new List<EntityEntry>();
            _deletedEntities = auditableEntities?.Where(e => e.State == EntityState.Deleted)?.ToList() ?? new List<EntityEntry>();
            _userId = _httpContextAccessor?.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier) ?? "";
        }

        public List<AuditLog> GetAuditLogsForCreatedEntities()
        {
            var auditLogs = new List<AuditLog>();

            foreach (var entity in _addedEntities)
            {
                auditLogs.Add(new AuditLog
                {
                    EntityId = (int) entity.Properties.FirstOrDefault(p => p.Metadata.Name.ToLower() == $"{entity.Metadata.Name.Substring(entity.Metadata.Name.LastIndexOf('.') + 1)}Id".ToLower())?.OriginalValue,
                    EntityType = (EntityType)Enum.Parse(typeof(EntityType), entity.Metadata.Name.Substring(entity.Metadata.Name.LastIndexOf('.') + 1)),
                    OldValue = "",
                    NewValue = "ENTITY CREATED",
                    TimeStamp = DateTime.Now,
                    PropertyName = "",
                    ChangedBy = _userId,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    AuditLogParentEntities = createAuditLogParentEntities(entity)
                });
            }
            return auditLogs;
        }

        public List<AuditLog> GetAuditLogsForModifiedEntities()
        {
            var auditLogs = new List<AuditLog>();

            foreach (var entity in _modifiedEntities)
            {
                foreach (var auditableProp in entity.Properties.Where(p => p.IsModified && p?.Metadata?.PropertyInfo?.CustomAttributes?.FirstOrDefault(a => a.AttributeType.Name == "AuditableAttribute") != null && p.CurrentValue.ToString() != p.OriginalValue.ToString()))
                {
                    auditLogs.Add(new AuditLog
                    {
                        EntityId = (int) entity.Properties.FirstOrDefault(p => p.Metadata.Name.ToLower() == $"{entity.Metadata.Name.Substring(entity.Metadata.Name.LastIndexOf('.') + 1)}Id".ToLower())?.OriginalValue,
                        EntityType = (EntityType)Enum.Parse(typeof(EntityType), entity.Metadata.Name.Substring(entity.Metadata.Name.LastIndexOf('.') + 1)),
                        OldValue = auditableProp?.OriginalValue?.ToString(),
                        NewValue = auditableProp?.CurrentValue?.ToString(),
                        TimeStamp = DateTime.Now,
                        PropertyName = auditableProp?.Metadata?.Name,
                        ChangedBy = _userId,
                        AuditLogActionType = AuditLogActionType.MODIFIED,
                        AuditLogParentEntities = createAuditLogParentEntities(entity)
                    });
                }
            }
            return auditLogs;
        }

        public List<AuditLog> GetAuditLogsForDeletedEntities()
        {
            var auditLogs = new List<AuditLog>();

            foreach (var entity in _deletedEntities)
            {
                auditLogs.Add(new AuditLog
                {
                    EntityId = (int)entity.Properties.FirstOrDefault(p => p.Metadata.Name.ToLower() == $"{entity.Metadata.Name.Substring(entity.Metadata.Name.LastIndexOf('.') + 1)}Id".ToLower())?.OriginalValue,
                    EntityType = (EntityType)Enum.Parse(typeof(EntityType), entity.Metadata.Name.Substring(entity.Metadata.Name.LastIndexOf('.') + 1)),
                    OldValue = "",
                    NewValue = "ENTITY DELETED",
                    TimeStamp = DateTime.Now,
                    PropertyName = "",
                    ChangedBy = _userId,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    AuditLogParentEntities = createAuditLogParentEntities(entity)
                });
            }

            return auditLogs;
        }

        private List<AuditLogParentEntity> createAuditLogParentEntities(EntityEntry entityEntry)
        {
            var auditLogParentEntities = new List<AuditLogParentEntity>();

            foreach (var prop in entityEntry.Properties.Where(p => p.Metadata?.PropertyInfo?.CustomAttributes?.FirstOrDefault(a => a.AttributeType.Name == "ParentIdAttribute") != null))
            {
                Enum.TryParse(prop.Metadata.Name.Substring(0, prop.Metadata.Name.Length - 2), true, out EntityType parentEntityType);
                auditLogParentEntities.Add(new AuditLogParentEntity {
                    ParentEntityId = (int)prop.CurrentValue,
                    ParentEntityType = parentEntityType
                });
            }

            return auditLogParentEntities;
        }
    }

    public interface ICreateAuditLogsService
    {
        /// <summary>
        /// This method is called by the DbContext. That is the only time
        /// it is necessary to call this method.
        /// </summary>
        /// <param name="entityEntries">The entities that the DbContext is tracking</param>
        void Initialize(IEnumerable<EntityEntry> entityEntries);
        /// <summary>
        /// This method is called by the DbContext. It returns a list of tracked entities
        /// that have been modified.
        /// </summary>
        /// <returns>The modified entities that the DbContext is tracking</returns>
        List<AuditLog> GetAuditLogsForModifiedEntities();
        /// <summary>
        /// This method is called by the DbContext. It returns a list of tracked entities
        /// that have been added (not yet in the database).
        /// </summary>
        /// <returns>The added entities that the DbContext is tracking</returns>
        List<AuditLog> GetAuditLogsForCreatedEntities();
        /// <summary>
        /// This method is called by the DbContext. It returns a list of tracked entities
        /// that have been marked for deletion.
        /// </summary>
        /// <returns>The deleted entities that the DbContext is tracking</returns>
        List<AuditLog> GetAuditLogsForDeletedEntities();
    }
}
