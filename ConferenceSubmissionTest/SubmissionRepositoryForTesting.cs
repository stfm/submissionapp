﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.DTOs.PosterHall;
using ConferenceSubmission.Models;

namespace ConferenceSubmissionTest
{
    public class SubmissionRepositoryForTesting : ISubmissionRepository
    {
        public SubmissionRepositoryForTesting()
        {
        }

        public int AddSubmission(Submission submission)
        {
            throw new NotImplementedException();
        }

        public int AddSubmissionPaymentToSubmission(SubmissionPayment submissionPayment)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<Submission> GetSubmissions(IEnumerable<int> submissionIds)
        {
            return GetSubmissions("stfm1000");
        }

        public IEnumerable<Submission> GetAcceptedSubmissionsForConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public Submission GetSubmissionAllDetailsBySubmissionId(int submissionId)
        {
            Conference conference = new Conference
            {
                ConferenceId = 100,
                ConferenceDeleted = false,
                ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                ConferenceInactive = false,
                ConferenceLongName = "2019 Medical Student Education Conference",
                ConferenceShortName = "MSE19",
                ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                ConferenceCFPEndDate = DateTime.Now.AddDays(5),
                ConferenceTypeId = 2
            };

            SubmissionCategory submissionCategory = new SubmissionCategory
            {

                SubmissionCategoryName = "Test Category",
                SubmissionCategoryId = 101

            };

            SubmissionStatus submissionStatus = new SubmissionStatus
            {

                SubmissionStatusId = 1,
                SubmissionStatusName = SubmissionStatusType.ACCEPTED

            };

            Submission submission = new Submission
            {

                SubmissionId = 1001,
                SubmissionTitle = "Submission Title",
                SubmissionAbstract = "This is the submission abstract.",
                SubmissionCategory = submissionCategory,
                SubmissionStatus = submissionStatus,
                AcceptedCategory = submissionCategory,
                Conference = conference,
                AcceptedCategoryId = 100
                



            };

            return submission;
        }

        public Submission GetSubmissionBySubmissionId(int submissionId)
        {
            SubmissionCategory submissionCategory = new SubmissionCategory
            {

                SubmissionCategoryName = "Test Category",
                SubmissionCategoryId = 101

            };

            SubmissionStatus submissionStatus = new SubmissionStatus
            {

                SubmissionStatusId = 1,
                SubmissionStatusName = SubmissionStatusType.ACCEPTED

            };

            Submission submission = new Submission
            {

                SubmissionId = 1001,
                SubmissionTitle = "Submission Title",
                SubmissionAbstract = "This is the submission abstract.",
                SubmissionCategory = submissionCategory,
                SubmissionStatus = submissionStatus,
                AcceptedCategory = submissionCategory



            };

            return submission;

        }

        public IEnumerable<Submission> GetSubmissions(string StfmUserId)
        {

            //Participants

            Participant participant = new Participant { ParticipantId = 4001, StfmUserId = "3001" };

            Participant participant2 = new Participant { ParticipantId = 4002, StfmUserId = "3002" };


            //Participant Roles
            ParticipantRole participantRole1 = new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName =ParticipantRoleType.SUBMITTER };

            ParticipantRole participantRole2 = new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER};

            ParticipantRole participantRole3 = new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.PRESENTER };

            //Conferences

            Conference conference = new Conference
            {
                ConferenceId = 100,
                ConferenceDeleted = false,
                ConferenceEndDate = DateTime.Now.AddMonths(2),
                ConferenceInactive = false,
                ConferenceLongName = "2019 Medical Student Education Conference",
                ConferenceShortName = "MSE19",
                ConferenceStartDate = DateTime.Now.AddMonths(2).AddDays(-4),
                ConferenceCFPEndDate = DateTime.Now.AddDays(5),
                ConferenceTypeId = 2
            };

            Conference conference2 = new Conference
            {
                ConferenceId = 101,
                ConferenceDeleted = false,
                ConferenceEndDate = DateTime.Parse("Apr 8, 2018"),
                ConferenceInactive = false,
                ConferenceLongName = "2018 Conference on Practice Improvement",
                ConferenceShortName = "CPI18",
                ConferenceStartDate = DateTime.Parse("Nov 5, 2018"),
                ConferenceCFPEndDate = DateTime.Parse("Apr 1, 2018"),
                ConferenceTypeId = 3
            };

            //Submission Statuses

            SubmissionStatus submissionStatus = new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN };

            SubmissionStatus submissionStatusCanceled = new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.CANCELED };

            SubmissionStatus submissionStatusWithdrawn = new SubmissionStatus { SubmissionStatusId = 2002, SubmissionStatusName = SubmissionStatusType.WITHDRAWN };

            SubmissionCategory submissionCategory = new SubmissionCategory
            {

                SubmissionCategoryName = "Test Category",
                SubmissionCategoryId = 1001

            };

            //Submissions
            Submission submission1 = new Submission
            {

                AcceptedCategoryId = 1001,
                Conference = conference,
                SubmissionAbstract = "Test Abstract 1",
                SubmissionCategoryId = 1000,
                SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                SubmissionId = 10001,
                SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                SubmissionStatus = submissionStatus,
                SubmissionTitle = "Test Title 1",
                AcceptedCategory = submissionCategory

            };


            Submission submission2 = new Submission
            {

                AcceptedCategoryId = 1000,
                Conference = conference2,
                SubmissionAbstract = "Test Abstract 2",
                SubmissionCategoryId = 1000,
                SubmissionCreatedDateTime = DateTime.Parse("Feb 12, 2018"),
                SubmissionId = 10002,
                SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 12, 2018"),
                SubmissionStatus = submissionStatus,
                SubmissionTitle = "Test Title 2",
                AcceptedCategory = submissionCategory

            };

            Submission submission3 = new Submission
            {

                AcceptedCategoryId = 1001,
                Conference = conference,
                SubmissionAbstract = "Test Abstract 3",
                SubmissionCategoryId = 1001,
                SubmissionCreatedDateTime = DateTime.Parse("Feb 12, 2018"),
                SubmissionId = 10003,
                SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 12, 2018"),
                SubmissionStatus = submissionStatusWithdrawn,
                SubmissionTitle = "Test Title 3",
                AcceptedCategory = submissionCategory

            };


            //SubmissionParticipants
            SubmissionParticipant submissionParticipant1 = new SubmissionParticipant { Participant = participant, SubmissionId = 10001, SubmissionParticipantId = 1 };

            SubmissionParticipant submissionParticipant2 = new SubmissionParticipant { Participant = participant, SubmissionId = 10002, SubmissionParticipantId = 2 };

            SubmissionParticipant submissionParticipant3 = new SubmissionParticipant { Participant = participant2, SubmissionId = 10001,SubmissionParticipantId = 3 };

            SubmissionParticipant submissionParticipant4 = new SubmissionParticipant { Participant = participant, SubmissionId = 10003, SubmissionParticipantId = 4 };


            //Roles for submissionParticipant1 - Submitter and Main Presenter
            List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles1 = new List<SubmissionParticipantToParticipantRole>();

            SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole1 = new SubmissionParticipantToParticipantRole { SubmissionParticipant = submissionParticipant1, ParticipantRole = participantRole1 };

            SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole2 = new SubmissionParticipantToParticipantRole { SubmissionParticipant = submissionParticipant1, ParticipantRole = participantRole2 };

            submissionParticipantToParticipantRoles1.Add(submissionParticipantToParticipantRole1);

            submissionParticipantToParticipantRoles1.Add(submissionParticipantToParticipantRole2);

            submissionParticipant1.SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRoles1;

            //Roles for submissionParticipant2 - Submitter

            List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles2 = new List<SubmissionParticipantToParticipantRole>();

            SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole3 = new SubmissionParticipantToParticipantRole { SubmissionParticipant = submissionParticipant2, ParticipantRole = participantRole1 };

            submissionParticipantToParticipantRoles2.Add(submissionParticipantToParticipantRole3);

            submissionParticipant2.SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRoles2;

           
            //Roles for submissionParticipant3 - Presenter

            List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles3 = new List<SubmissionParticipantToParticipantRole>();

            SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole4 = new SubmissionParticipantToParticipantRole { SubmissionParticipant = submissionParticipant3, ParticipantRole = participantRole3 };

            submissionParticipantToParticipantRoles3.Add(submissionParticipantToParticipantRole4);

            submissionParticipant3.SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRoles3;

            //Roles for submissionParticipant4 - Submitter

            List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles4 = new List<SubmissionParticipantToParticipantRole>();

            SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole5 = new SubmissionParticipantToParticipantRole { SubmissionParticipant = submissionParticipant4, ParticipantRole = participantRole1 };

            submissionParticipantToParticipantRoles4.Add(submissionParticipantToParticipantRole5);

            submissionParticipant4.SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRoles4;


            //Add submissionParticipant1 and submissionParticipant3 to the Submission1

            List<SubmissionParticipant> submissionParticipants1 = new List<SubmissionParticipant>();

            submissionParticipants1.Add(submissionParticipant1);

            submissionParticipants1.Add(submissionParticipant3);

            submission1.ParticipantLink = submissionParticipants1;


            //Add submissionParticipant2 to submission2

            List<SubmissionParticipant> submissionParticipants2 = new List<SubmissionParticipant>();

            submissionParticipants2.Add(submissionParticipant2);

            submission2.ParticipantLink = submissionParticipants2;

            //Add submissionParticipant4 to submission3

            List<SubmissionParticipant> submissionParticipants3 = new List<SubmissionParticipant>();

            submissionParticipants3.Add(submissionParticipant4);

            submission3.ParticipantLink = submissionParticipants3;


            //Create collection of Submission objects to return
            List<Submission> submissions = new List<Submission>();

            submissions.Add(submission1);

            submissions.Add(submission2);

            submissions.Add(submission3);

            return submissions;


        }

        public IEnumerable<Submission> GetSubmissions(bool isSubmissionCompleted, int conferenceId)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, int> GetSubmissionsByCategory(int conferenceId, bool submissionCompleted)
        {

            Dictionary<string, int> submissionsForCategory = new Dictionary<string, int>();

            submissionsForCategory.Add("Lecture", 5);
            submissionsForCategory.Add("Poster", 4);

            return submissionsForCategory;

        }

        public IEnumerable<Submission> GetSubmissionsForConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public void UpdateSubmission(Submission submission)
        {
            throw new NotImplementedException();
        }

        public void UpdateSubmissionCompleted(int submissionId, bool submissionCompleted)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Submission> GetAcceptedSubmissionsForCategory(int acceptedCategoryId)
        {
            throw new NotImplementedException();
        }

        public void SetParticipantOrderModifiedToTrue(int submissionId)
        {
            throw new NotImplementedException();
        }

        public List<int> GetSubmissionsWhoseParticipantOrderHasBeenModified(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, int> GetSubmissionsByCategoryUsingStatus(int conferenceId, SubmissionStatusType submissionStatusType)
        {
            Dictionary<string, int> submissionsForCategory = new Dictionary<string, int>();

            submissionsForCategory.Add("Lecture", 5);
            submissionsForCategory.Add("Poster", 4);

            return submissionsForCategory;
        }

        public List<int> GetSubmissionIdsByConference(int conference)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionWithNameAndSessionCode> GetConferenceSubmissionsByNameAndSessionCode(int conference)
        {
            throw new NotImplementedException();
        }

        public bool SubmissionExists(int submissionId)
        {
            throw new NotImplementedException();
        }

        public List<PosterDTO> GetSubmissionsForPosterHallCategory(int submissionCategoryId)
        {
            throw new NotImplementedException();
        }

        public List<SubmissionForRegistrationReport> GetSubmissionsForConferenceRegistrationReport(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public void UpdateSubmissionPresentationMethodStatus(int submissionId, PresentationMethodType presentationMethodType, PresentationStatusType presentationStatusType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Submission> GetAcceptedOnlineSubmissionsForConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Submission> GetVirtualConferenceSubmissions(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, int> GetSubmissionsPaidForByCategory(int conferenceId)
        {
            throw new NotImplementedException();
        }
    }
}
