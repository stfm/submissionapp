﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents a person who is presenting a presentation at a conference or
    /// who contributed to the presentation but is not presenting.
    /// </summary>
    [Auditable]
    public class Participant
    {
        public Participant()
        {
        }

        public int ParticipantId { get; set; }

        public string StfmUserId { get; set; }

        public ICollection<SubmissionParticipant> SubmissionsLink { get; set; }

        [NotMapped]
        public User ParticipantUser { get; set; }

		public override string ToString()
		{

			return "ParticipantId: " + ParticipantId + " | StfmUserId: " + StfmUserId;

		}
    }
}
