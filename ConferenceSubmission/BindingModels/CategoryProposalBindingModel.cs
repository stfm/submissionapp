﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Stores data needed to create form fields for category
    /// specific proposal and to store the user's answers.
    /// </summary>
    public class CategoryProposalBindingModel
    {
        public string ConferenceName { get; set; }

        public string CateogryName { get; set; }

        public string SubmissionTitle { get; set; }

        public string SubmissionId { get; set; }

        public IEnumerable<FormField> FormFields { get; set; }


    }
}
