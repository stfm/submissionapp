﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class ReviewersAssignedToSubmissionViewModel
    {

        public List<SubmissionReviewer> reviewers { get; set; }

        public Conference Conference { get; set; }

        public Submission Submission { get; set; }

        public ReviewersAssignedToSubmissionViewModel()
        {

            reviewers = new List<SubmissionReviewer>();

        }


    }
}
