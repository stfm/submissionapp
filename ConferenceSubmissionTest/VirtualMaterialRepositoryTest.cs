﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class VirtualMaterialRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public VirtualMaterialRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }


        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2018"),
                        ConferenceInactive = false,
                        ConferenceLongName = "Test Conference 1",
                        ConferenceShortName = "TC1",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2018"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture TC1" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar TC1" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW },
                    new SubmissionStatus { SubmissionStatusId = 2002, SubmissionStatusName = SubmissionStatusType.ACCEPTED }


                );

                context.SaveChanges();

                context.Submissions.AddRange(

                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2002,
                        SubmissionTitle = "Test Title",

                    }

                );

                context.SaveChanges();

                VirtualMaterial virtualMaterial = new VirtualMaterial { MaterialType = "video", MaterialValue = "my awesome video.mov", PersonUploaded = "xxxxxx", SubmissionId = 10001, UploadedDateTime = DateTime.Parse("Jun 11, 2020") };

                context.VirtualMaterials.Add(virtualMaterial);

                context.SaveChanges();

                return options;


            }

        }

        [Fact]
        public void VirtualMaterialExists_True()
        {
            //arrange
            var submissionId = 10001;

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                VirtualMaterialRepository virtualMaterialRepository = new VirtualMaterialRepository(context);

                //Act
                VirtualMaterial result = virtualMaterialRepository.GetVirtualMaterial(submissionId);

                Output.WriteLine("virtualMaterialId is " + result.VirtualMaterialId);

                //assert
                Assert.Equal("my awesome video.mov", result.MaterialValue);

            }
        }

        [Fact]
        public void VirtualMaterialExists_False()
        {
            //arrange
            var submissionId = 10002;

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                VirtualMaterialRepository virtualMaterialRepository = new VirtualMaterialRepository(context);

                //Act
                VirtualMaterial result = virtualMaterialRepository.GetVirtualMaterial(submissionId);


                //assert
                Assert.Null(result);
            }
        }

        [Fact]
        public void VirtualMaterialUpdate()
        {
            //arrange
            var submissionId = 10001;

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                VirtualMaterialRepository virtualMaterialRepository = new VirtualMaterialRepository(context);

                //Act
                VirtualMaterial result = virtualMaterialRepository.GetVirtualMaterial(submissionId);

                result.MaterialType = "poster";

                int virtualMaterialId = virtualMaterialRepository.UpdateVirtualMaterial(result);

                //assert
                Assert.Equal(result.VirtualMaterialId, virtualMaterialId);
            }
        }



    }
}
