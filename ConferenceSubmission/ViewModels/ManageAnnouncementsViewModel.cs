﻿using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.ViewModels
{
    public class ManageAnnouncementsViewModel
    {
        public ManageAnnouncementsViewModel()
        {
            Announcements = new List<Announcement>();
        }

        public List<Announcement> Announcements { get; set; }
    }
}
