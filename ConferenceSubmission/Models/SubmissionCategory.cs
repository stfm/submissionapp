﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents the category (e.g. Research Poster, Lecture) that the 
    /// submission and the category for the presentation.  Usually the 
    /// submission category and the presentation category will match but
    /// they can be different if the presentation will be in a category
    /// (e.g. Poster) that is different then the submission category
    /// (e.g. Lecture).
    /// </summary>
    public class SubmissionCategory
    {
        public SubmissionCategory()
        {

            Submissions = new List<Submission>();

            SubmissionCategoryToPresentationMethodsLink = new List<SubmissionCategoryToPresentationMethod>();

        }

        public int SubmissionCategoryId { get; set; }

        [StringLength(150)]
        [Required]
        public string SubmissionCategoryName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:ConferenceSubmission.Models.SubmissionCategory"/>
        /// submission category inactive. By default a this will be false.
        /// </summary>
        /// <value><c>true</c> if submission category inactive; otherwise, <c>false</c>.</value>
        public bool SubmissionCategoryInactive { get; set; }

        /// <summary>
        /// Stores the submission payment requirement for this submission category.
        /// </summary>
        /// <value>The submission payment requirement.</value>
        public SubmissionPaymentRequirement SubmissionPaymentRequirement { get; set; }

        public int ConferenceId { get; set; }

        public Conference Conference { get; set; }

        public string CategoryAbstractInstructions { get; set; }

        public string CategoryInstructions { get; set; }

        public int CategoryAbstractMaxLength { get; set; }

        [InverseProperty("SubmissionCategory")]
        public List<Submission> Submissions { get; set; }

        [InverseProperty("AcceptedCategory")]
        public List<Submission> AcceptedSubmissions { get; set; }

        public List<SubmissionCategoryToPresentationMethod> SubmissionCategoryToPresentationMethodsLink { get; set; }

    }
}