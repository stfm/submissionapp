﻿using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface ISubmissionPrintService
    {
        byte[] GetPdfAsByteArray(SubmissionPrintViewModel submissionPrintViewModel);

        string RenderViewToString<T>(T model, string pathToView);

    }
}
