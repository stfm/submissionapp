﻿$(document).ready(function () {

    $('#assignedSubmissionsTable').DataTable({
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: ''
            }
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "pageLength": -1
        });

     $('#submissionsReviewedTable').DataTable({

         dom: 'Bfrtip',
buttons: [
        {
            extend: 'csv',
            text: 'Save To CSV File',
            exportOptions: {
                modifier: {
                    search: 'none'
                }
            }
        }
    ],
         responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: ''
            }
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "pageLength": 25

        });

    $('#reviewersNotDoneTable').DataTable({
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: ''
            }
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "pageLength": 25
    });

    $('#reviewersStatusTable').DataTable({
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: ''
            }
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "pageLength": 25
    });

    $('#incompleteSubmissionsTable').DataTable( {
        dom: 'Bfrtip',
buttons: [
        {
            extend: 'csv',
            text: 'Save To CSV File',
            exportOptions: {
                modifier: {
                    search: 'none'
                }
            }
        }
    ]
    });

    $('#allReviewersTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csv',
                text: 'Save To CSV File',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: ''
            }
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "pageLength": 25
    });

  

    

});