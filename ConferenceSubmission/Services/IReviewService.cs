﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public interface IReviewService
    {
        /// <summary>
        /// Saves the review.
        /// </summary>
        /// <returns>The review id.</returns>
        /// <param name="review">Review.</param>
        int SaveReview(Review review);

        /// <summary>
        /// For the provided reviewId did the user answer all the questions that
        /// require an answer.
        /// </summary>
        /// <returns><c>true</c>, if the review has an answer for all 
        /// submission category review questions that require an answer 
        /// <c>false</c> otherwise.</returns>
        /// <param name="reviewId">review identifier.</param>
        bool IsSubmissionCategoryReviewCompleted(int reviewId);

        /// <summary>
        /// Updates the submission.
        /// </summary>
        /// <param name="review">Review.</param>
        void UpdateReview(Review review);

        /// <summary>
        /// Gets the review.
        /// </summary>
        /// <returns>The review.</returns>
        /// <param name="reviewId">Review identifier.</param>
        Review GetReview(int reviewId);

        /// <summary>
        /// Checks if review exists.
        /// </summary>
        /// <returns><c>true</c>, if if review exists was checked, <c>false</c> otherwise.</returns>
        /// <param name="reviewerId">Reviewer identifier.</param>
        /// <param name="submissionId">Submission identifier.</param>
        bool CheckIfReviewExists(int reviewerId, int submissionId);
    }
}
