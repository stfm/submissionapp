﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class removesubmitter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Submitters_SubmitterId",
                table: "Submissions");

            migrationBuilder.DropTable(
                name: "SubmissionPresenters");

            migrationBuilder.DropTable(
                name: "Submitters");

            migrationBuilder.DropTable(
                name: "Presenters");

            migrationBuilder.DropTable(
                name: "PresenterRoles");

            migrationBuilder.DropIndex(
                name: "IX_Submissions_SubmitterId",
                table: "Submissions");

            migrationBuilder.DropColumn(
                name: "SubmitterId",
                table: "Submissions");

            migrationBuilder.CreateTable(
                name: "ParticipantRoles",
                columns: table => new
                {
                    ParticipantRoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ParticipantRoleName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParticipantRoles", x => x.ParticipantRoleId);
                });

            migrationBuilder.CreateTable(
                name: "Participants",
                columns: table => new
                {
                    ParticipantId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StfmUserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participants", x => x.ParticipantId);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionParticipant",
                columns: table => new
                {
                    SubmissionId = table.Column<int>(nullable: false),
                    ParticipantId = table.Column<int>(nullable: false),
                    ParticipantRoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionParticipant", x => new { x.SubmissionId, x.ParticipantId });
                    table.ForeignKey(
                        name: "FK_SubmissionParticipant_Participants_ParticipantId",
                        column: x => x.ParticipantId,
                        principalTable: "Participants",
                        principalColumn: "ParticipantId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubmissionParticipant_ParticipantRoles_ParticipantRoleId",
                        column: x => x.ParticipantRoleId,
                        principalTable: "ParticipantRoles",
                        principalColumn: "ParticipantRoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubmissionParticipant_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionParticipant_ParticipantId",
                table: "SubmissionParticipant",
                column: "ParticipantId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionParticipant_ParticipantRoleId",
                table: "SubmissionParticipant",
                column: "ParticipantRoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubmissionParticipant");

            migrationBuilder.DropTable(
                name: "Participants");

            migrationBuilder.DropTable(
                name: "ParticipantRoles");

            migrationBuilder.AddColumn<int>(
                name: "SubmitterId",
                table: "Submissions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PresenterRoles",
                columns: table => new
                {
                    PresenterRoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PresenterRoleName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PresenterRoles", x => x.PresenterRoleId);
                });

            migrationBuilder.CreateTable(
                name: "Presenters",
                columns: table => new
                {
                    PresenterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StfmUserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Presenters", x => x.PresenterId);
                });

            migrationBuilder.CreateTable(
                name: "Submitters",
                columns: table => new
                {
                    SubmitterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StfmUserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Submitters", x => x.SubmitterId);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionPresenters",
                columns: table => new
                {
                    SubmissionId = table.Column<int>(nullable: false),
                    PresenterId = table.Column<int>(nullable: false),
                    PresenterRoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionPresenters", x => new { x.SubmissionId, x.PresenterId });
                    table.ForeignKey(
                        name: "FK_SubmissionPresenters_Presenters_PresenterId",
                        column: x => x.PresenterId,
                        principalTable: "Presenters",
                        principalColumn: "PresenterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubmissionPresenters_PresenterRoles_PresenterRoleId",
                        column: x => x.PresenterRoleId,
                        principalTable: "PresenterRoles",
                        principalColumn: "PresenterRoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubmissionPresenters_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Submissions_SubmitterId",
                table: "Submissions",
                column: "SubmitterId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionPresenters_PresenterId",
                table: "SubmissionPresenters",
                column: "PresenterId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionPresenters_PresenterRoleId",
                table: "SubmissionPresenters",
                column: "PresenterRoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Submitters_SubmitterId",
                table: "Submissions",
                column: "SubmitterId",
                principalTable: "Submitters",
                principalColumn: "SubmitterId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
