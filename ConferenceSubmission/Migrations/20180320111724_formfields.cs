﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class formfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FormFields",
                columns: table => new
                {
                    FormFieldId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FormFieldDeleted = table.Column<bool>(nullable: false),
                    FormFieldSortOrder = table.Column<int>(nullable: false),
                    FormFieldType = table.Column<string>(maxLength: 50, nullable: false),
                    SubmissionCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormFields", x => x.FormFieldId);
                    table.ForeignKey(
                        name: "FK_FormFields_SubmissionCategories_SubmissionCategoryId",
                        column: x => x.SubmissionCategoryId,
                        principalTable: "SubmissionCategories",
                        principalColumn: "SubmissionCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FormFieldProperties",
                columns: table => new
                {
                    FormFieldPropertyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FormFieldId = table.Column<int>(nullable: false),
                    PropertyName = table.Column<string>(maxLength: 250, nullable: false),
                    PropertyValue = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormFieldProperties", x => x.FormFieldPropertyId);
                    table.ForeignKey(
                        name: "FK_FormFieldProperties_FormFields_FormFieldId",
                        column: x => x.FormFieldId,
                        principalTable: "FormFields",
                        principalColumn: "FormFieldId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionFormFields",
                columns: table => new
                {
                    SubmissionFormFieldId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FormFieldId = table.Column<int>(nullable: false),
                    SubmissionFormFieldValue = table.Column<string>(nullable: false),
                    SubmissionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionFormFields", x => x.SubmissionFormFieldId);
                    table.ForeignKey(
                        name: "FK_SubmissionFormFields_FormFields_FormFieldId",
                        column: x => x.FormFieldId,
                        principalTable: "FormFields",
                        principalColumn: "FormFieldId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FormFieldProperties_FormFieldId",
                table: "FormFieldProperties",
                column: "FormFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_FormFields_SubmissionCategoryId",
                table: "FormFields",
                column: "SubmissionCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionFormFields_FormFieldId",
                table: "SubmissionFormFields",
                column: "FormFieldId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FormFieldProperties");

            migrationBuilder.DropTable(
                name: "SubmissionFormFields");

            migrationBuilder.DropTable(
                name: "FormFields");
        }
    }
}
