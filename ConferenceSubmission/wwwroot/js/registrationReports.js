﻿var selectedReport = "";

$(document).ready(function () {
    loadConferencesByYear(new Date().getFullYear());
    selectedReport = "SessionsWithNoRegistrations";
});

let loadConferences = function () {
    let year = $("#yearInput").val();
    loadConferencesByYear(year);
};

let loadConferencesByYear = function (year) {   
    $("#conferenceSelectorContainer").hide();
    $("#loadingConferences").show();

    $.ajax({
        url: `/SubmissionAdmin/LoadConferencesForRegistrationReports?year=${year}`,
        dataType: "html"
    }).done(function (data) {
        $("#loadingConferences").hide();
        $("#conferenceSelectorContainer").html(data);
        $("#conferenceSelectorContainer").show();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#loadingConferences").hide();
    });
};

let loadReport = function () {    
    let selectedEventId = $("#eventSelector").val();
    let selectedEventName = $("#eventSelector option:selected").text();
    let selectedConferenceId = $("#conferenceSelector").val();

    if (selectedConferenceId === undefined || selectedConferenceId === null || selectedConferenceId === "" ||
        selectedEventId      === undefined || selectedEventId      === null || selectedEventId      === "") {
        alert("Please select a conference and an event.");
        return;
    }

    window.open(`/SubmissionAdmin/${selectedReport}Report?conferenceId=${selectedConferenceId}&eventId=${selectedEventId}&eventName=${selectedEventName}`, "_blank");
};

let selectSessionsWithNoRegistrations = function () {
    $("#sessionsWithNoRegistrationsCheck").show();
    $("#allSessionsWithUnregisteredPresentersCheck").hide();
    $("#allSessionsPresentersRegisteredCheck").hide();
    selectedReport = "SessionsWithNoRegistrations";
};

let selectAllSessionsWithUnregisteredPresenters = function () {
    $("#sessionsWithNoRegistrationsCheck").hide();
    $("#allSessionsPresentersRegisteredCheck").hide();
    $("#allSessionsWithUnregisteredPresentersCheck").show();
    selectedReport = "AllSessionsWithPresentersThatHaveNotRegistered";
};

let selectAllSessionsPresentersRegistered = function () {
    $("#sessionsWithNoRegistrationsCheck").hide();
    $("#allSessionsPresentersRegisteredCheck").show();
    $("#allSessionsWithUnregisteredPresentersCheck").hide();
    selectedReport = "AllSessionsPresentersRegisteredUnregistered";
};