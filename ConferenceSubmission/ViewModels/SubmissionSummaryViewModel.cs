﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// Models data about a submission
    /// that we need to show on the home page view
    /// </summary>
    public class SubmissionSummaryViewModel
    {

        public int SubmissionId { get; set; }

        public string SubmissionTitle { get; set; }

        public SubmissionStatusType SubmissionStatusName { get; set; }

        public string SessionCode { get; set; }

        /// <summary>
        /// The logged in user's roles on this submission.
        /// </summary>
        public List<ParticipantRoleType> userRoles;

        /// <summary>
        /// The conference for this submission.
        /// </summary>
        /// <value>The name of the conference.</value>
        public String ConferenceLongName { get; set; }

        public DateTime ConferenceEndDate { get; set; }


        public DateTime ConferenceCFPEndDate { get; set; }

        public bool SubmissionCompleted { get; set; }

        public String SubmissionCategory { get; set; }

        public List<SubmissionPresentationMethod> SubmissionPresentationMethods { get; set; }

        /// <summary>
        /// Actions the user can take on this submission (e.g. view only, edit, etc)
        /// </summary>
        public List<SubmissionAction> SubmissionActions { get; set; }

        public string GetRolesAsStringPretty()
        {
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

            if (userRoles.Count > 0)
            {
                return myTI.ToTitleCase(string.Join(", ", userRoles.Select(l => l.ToString().Replace('_', ' '))));
            }
            else
            {
                return string.Empty;
            }
        }

        public override string ToString()
        {
            string userRolesString = "";

            foreach (ParticipantRoleType participantRoleType in userRoles)
            {

                userRolesString = userRolesString + " " + participantRoleType;
            }
            return "SubmissionId: " + SubmissionId + " | SubmissionTitle: " +
                SubmissionTitle + " | SubmissionStatusName: " +
                SubmissionStatusName + " | userRoles: " + userRolesString +
                " | ConferenceLongName: " + ConferenceLongName +
                " | ConferenceEndDate: " + ConferenceEndDate +
                " | ConferencCFPEndDate: " + ConferenceCFPEndDate +
                " | Submission Actions: " + String.Join(',', SubmissionActions.Select(x => x.ToString())) +
                " | SubmissionCategory: " + SubmissionCategory;

        }

        public bool HasPendingSubmissionPresentationMethod()
        {
            var result = SubmissionPresentationMethods.Any(x => x.PresentationStatus.PresentationStatusName == PresentationStatusType.PENDING && x.PresentationMethod.PresentationMethodName == PresentationMethodType.RECORDED_ONLINE);

            return result;
        }

    }
}
