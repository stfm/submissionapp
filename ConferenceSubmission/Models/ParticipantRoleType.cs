﻿using System;
namespace ConferenceSubmission.Models
{
    public enum ParticipantRoleType
    {

        LEAD_PRESENTER,

        PRESENTER,

        SUBMITTER,

        AUTHOR

    }
}
