﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents a type of conference (e.g. Annual or MSE)
    /// </summary>
    public class ConferenceType
    {
        public ConferenceType()
        {
        }

        public int ConferenceTypeId { get; set;  }

        [StringLength(50)]
        [Required]
        public string ConferenceTypeName { get; set;  }

        public List<Conference> conferences { get; set; }

    }
}
