﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Services
{
    public class SubmissionParticipantService : ISubmissionParticipantService
    {
        private readonly ISubmissionParticipantRepository _submissionParticipantRepository;
        private readonly IParticipantRepository _participantRepository;
        private readonly ISalesforceAPIService _salesforceAPIService;
        private readonly ISubmissionRepository _submissionRepository;

        public SubmissionParticipantService(ISubmissionParticipantRepository submissionParticipantRepository, IParticipantRepository participantRepository, ISalesforceAPIService salesforceAPIService, ISubmissionRepository submissionRepository)
        {
            _participantRepository = participantRepository;
            _submissionParticipantRepository = submissionParticipantRepository;
            _salesforceAPIService = salesforceAPIService;
            _submissionRepository = submissionRepository;
        }

        public int AddSubissionParticipant(int participantId, ParticipantRoleType participantRoleType, int submissionId)
        {
            //Get the participant role the user is requesting
            var requestedRole = _participantRepository.GetParticipantRole(participantRoleType);

            //Set the SortOrder: get the max SortOrder of the current SubmissionParticipants (excluding submitter only) and add 1
            //If there are no current participants, assign the sort order's value to be 0
            var currentParticipants = GetSubmissionParticipantsExcludeSubmitterOnly(submissionId).ToList();
            var sortOrder = currentParticipants == null || currentParticipants.Count == 0 ? 0 : currentParticipants.Max(c => c.SortOrder) + 1;

            var submissionParticipant = new SubmissionParticipant
            {
                SubmissionId = submissionId,
                ParticipantId = participantId,
                SortOrder = sortOrder,
                SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>
                {
                    new SubmissionParticipantToParticipantRole
                    {
                        ParticipantRole = requestedRole
                    }
                }
            };

            var newSubmissionParticipantId = _submissionParticipantRepository.AddSubmissionParticipant(submissionParticipant);

            //If the newly added participant is a lead presenter, check to see if the submission already has a lead presenter
            if (requestedRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER)
            {
                var currentLeadPresenter = currentParticipants.FirstOrDefault(c => c.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER));
                if (currentLeadPresenter != null)
                {
                    //If we're here, there was already a lead presenter assigned to the submission
                    //Remove his lead presenter role
                    var submissionParticipantToParticipantRoleId = currentLeadPresenter.SubmissionParticipantToParticipantRolesLink
                            .FirstOrDefault(s => s.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER)
                            .SubmissionParticipantToParticipantRoleId;
                    _submissionParticipantRepository.RemoveSubmissionParticipantToParticipantRole(submissionParticipantToParticipantRoleId);

                    //Assign him the presenter role
                    var participantRoleId = _participantRepository.GetParticipantRole(ParticipantRoleType.PRESENTER).ParticipantRoleId;
                    _submissionParticipantRepository.AddSubmissionParticipantToParticipantRole(currentLeadPresenter.SubmissionParticipantId, participantRoleId);
                }
            }
            return newSubmissionParticipantId;

        }

        public void DeleteSubmissionParticipant(int submissionParticipantId)
        {
            _submissionParticipantRepository.DeleteSubmissionParticipant(submissionParticipantId);
        }

        public List<SubmissionParticipant> FilterSubmissionParticipantsExcludeSubmitterOnly(List<SubmissionParticipant> submissionParticipants)
        {
            //Return those SubmissionParticipants who either: 
            //      1) have more than one ParticipantRole OR 
            //      2) have one role and that role is NOT submitter.
            return submissionParticipants.Where(s =>
                        s.SubmissionParticipantToParticipantRolesLink.Count > 1 ||
                        (
                            s.SubmissionParticipantToParticipantRolesLink.Count == 1 &&
                            s.SubmissionParticipantToParticipantRolesLink.FirstOrDefault().ParticipantRole.ParticipantRoleName != ParticipantRoleType.SUBMITTER)
                        )
                    .ToList();
        }

        public int GetInitialSortOrder(SubmissionParticipant submssionParticipant)
        {
            //If the participant has more than one role, then they're not a Submitter-only - return 0
            //Or, if participant has only one role and that role is not submitter, then they're not Submitter-only - return 0
            //Else - return -1
            return submssionParticipant.SubmissionParticipantToParticipantRolesLink.Count > 1 ||
                    (
                        submssionParticipant.SubmissionParticipantToParticipantRolesLink.Count == 1 &&
                        submssionParticipant.SubmissionParticipantToParticipantRolesLink.FirstOrDefault().ParticipantRole.ParticipantRoleName != ParticipantRoleType.SUBMITTER
                    )
                   ? 0 : -1;
        }

        public void UpdatePresenterOrder(UpdatePresenterOrderViewModel model)
        {
            //orderedIds = list of SubmissionParticipantIds in the order designated by the user
            var orderedIds = model.SubmissionParticipantIds.Split(',').Select(i => Int32.Parse(i)).ToList();

            //Get the submission participants for the submission, then filter out the participants who are Submitters-only
            var submissionParticipants = _submissionParticipantRepository.GetSubmissionParticipants(model.SubmissionId).ToList();
            var submissionParticipantsExcludeSubmittersOnly = FilterSubmissionParticipantsExcludeSubmitterOnly(submissionParticipants);

            //Foreach submission participant whose Id is in the list of orderedIds, update their sort order according 
            //to the list of orderedIds
            submissionParticipantsExcludeSubmittersOnly
                .Where(s => orderedIds.Contains(s.SubmissionParticipantId))
                .ToList()
                .ForEach(s =>
                {
                    s.SortOrder = orderedIds.IndexOf(s.SubmissionParticipantId);
                });

            //MaxSortOrder is the maximum sort order value from SubmissionParticipants whose SortOrder properties
            //were just updated.
            var maxSortOrder = submissionParticipantsExcludeSubmittersOnly
                .Where(s => orderedIds.Contains(s.SubmissionParticipantId))
                .Max(s => s.SortOrder);

            //Fetch any presenters that were added to the submission during the update presenter order process.
            //Assign such presenters maxSortOrder++
            submissionParticipantsExcludeSubmittersOnly
                .Where(s => !orderedIds.Contains(s.SubmissionParticipantId))
                .ToList()
                .ForEach(s =>
                {
                    s.SortOrder = ++maxSortOrder;
                });

            _submissionParticipantRepository.UpdateSubmissionParticipantSortOrder(submissionParticipantsExcludeSubmittersOnly, model.SubmissionId);
        }

        public List<SubmissionParticipant> GetSubmissionParticipants(int submissionId)
        {
            return _submissionParticipantRepository.GetSubmissionParticipants(submissionId).ToList();
        }

        public IEnumerable<SubmissionParticipant> GetSubmissionParticipantsExcludeSubmitterOnly(int submissionId)
        {
            return _submissionParticipantRepository.GetSubmissionParticipants(submissionId)
                .Where(s =>
                        s.SubmissionParticipantToParticipantRolesLink.Count > 1 ||
                        (
                            s.SubmissionParticipantToParticipantRolesLink.Count == 1 &&
                            s.SubmissionParticipantToParticipantRolesLink.First().ParticipantRole.ParticipantRoleName != ParticipantRoleType.SUBMITTER
                        )
                      )
                .ToList();
        }

        public bool CheckIfSubmissionParticipantExists(int submissionId, int participantId)
        {
            var submissionParticipants = _submissionParticipantRepository.GetSubmissionParticipants(submissionId);
            return submissionParticipants.Any(s => s.ParticipantId == participantId);
        }


        public string GetPerson(int submissionId, ParticipantRoleType participantRoleType)
        {

            string stfmUserId = null;

            List<SubmissionParticipant> submissionParticipants = GetSubmissionParticipants(submissionId);

            foreach (SubmissionParticipant submissionParticipant in submissionParticipants)
            {

                List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles = submissionParticipant.SubmissionParticipantToParticipantRolesLink;

                foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipantToParticipantRoles)
                {

                    if (submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName == participantRoleType)
                    {

                        stfmUserId = submissionParticipant.Participant.StfmUserId;

                    }


                }


            }

            return stfmUserId;

        }

        public List<int> GetParticipantsMoreThenTwoSubmissions(int conferenceId, int submissionStatusId)
        {
            return _submissionParticipantRepository.GetParticipantsMoreThenTwoSubmissions(conferenceId, submissionStatusId);

        }

        public void AssignLeadPresenterRole(int submissionId, int submissionParticipantId)
        {
            var submissionParticipants = _submissionParticipantRepository.GetSubmissionParticipants(submissionId);

            //Get the current lead presenter (if there is one)
            var currentLeadPresenter = submissionParticipants.FirstOrDefault(p => p.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER));
            if (currentLeadPresenter != null && currentLeadPresenter.SubmissionParticipantId == submissionParticipantId)
                return;
            if (currentLeadPresenter != null)
            {
                //There is currently a lead presenter assigned to this submission.
                //Unassign him/her the lead presenter role 
                _submissionParticipantRepository.RemoveSubmissionParticipantToParticipantRole(currentLeadPresenter.SubmissionParticipantToParticipantRolesLink.FirstOrDefault(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER).SubmissionParticipantToParticipantRoleId);
                
                //Assign him/her the presenter role
                var presenterParticipantRoleId = _participantRepository.GetParticipantRole(ParticipantRoleType.PRESENTER).ParticipantRoleId;
                _submissionParticipantRepository.AddSubmissionParticipantToParticipantRole(currentLeadPresenter.SubmissionParticipantId, presenterParticipantRoleId);
            }

            //Before assigning the lead presenter role, we need to remove the presenter role (if assigned)
            var newLeadPresenter = submissionParticipants.FirstOrDefault(p => p.SubmissionParticipantId == submissionParticipantId);
            if (newLeadPresenter.SubmissionParticipantToParticipantRolesLink.Any(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.PRESENTER))
                _submissionParticipantRepository.RemoveSubmissionParticipantToParticipantRole(newLeadPresenter.SubmissionParticipantToParticipantRolesLink.FirstOrDefault(l => l.ParticipantRole.ParticipantRoleName == ParticipantRoleType.PRESENTER).SubmissionParticipantToParticipantRoleId);

            //Now assign the lead presenter role
            var leadPresenterParticipantRoleId = _participantRepository.GetParticipantRole(ParticipantRoleType.LEAD_PRESENTER).ParticipantRoleId;
            _submissionParticipantRepository.AddSubmissionParticipantToParticipantRole(newLeadPresenter.SubmissionParticipantId, leadPresenterParticipantRoleId);
        }

        public void RemoveParticipantRole(int submissionParticipantToParticipantRoleId)
        {
            _submissionParticipantRepository.RemoveSubmissionParticipantToParticipantRole(submissionParticipantToParticipantRoleId);
        }

        public int AddSubmissionParticipantToParticipantRole(int submissionParticipantId, int participantRoleId)
        {
            return _submissionParticipantRepository.AddSubmissionParticipantToParticipantRole(submissionParticipantId, participantRoleId);
        }

        public UpdatePresenterOrderViewModel GetUpdatePresenterOrderViewModel(Submission submission)
        {
            var model = new UpdatePresenterOrderViewModel
            {
                ConferenceName = submission.Conference.ConferenceLongName,
                SubmittedCategory = submission.SubmissionCategory.SubmissionCategoryName,
                AcceptedCategory = submission.AcceptedCategory.SubmissionCategoryName,
                SubmissionId = submission.SubmissionId,
                SubmissionTitle = submission.SubmissionTitle,
            };
            var participantsExcludingSubmittersOnly = FilterSubmissionParticipantsExcludeSubmitterOnly(submission.ParticipantLink);
            var salesforceUsers = _salesforceAPIService.GetMultipleUserInfo(participantsExcludingSubmittersOnly.Select(p => p.Participant.StfmUserId)).Result;

            //For each SubmissionParticipant (exluding Submitter-only), get their data from SalesForce
            //and then create a Presenter object and add that to UpdatePresenterOrderViewModel.Presenters
            participantsExcludingSubmittersOnly.ForEach(p =>
            {
                var user = salesforceUsers.FirstOrDefault(su => su.STFMUserId == p.Participant.StfmUserId);
                model.Presenters.Add(new Presenter
                {
                    SubmissionParticipantId = p.SubmissionParticipantId,
                    SortOrder = p.SortOrder,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                });
            });

            return model;
        }

        public List<User> FindPeople(string lastName)
        {
            List<User> people = _salesforceAPIService.FindPeopleByLastName(lastName).Result.ToList();

            return people;

        }

        public List<SubmissionWithParticipantsAddedOrRemoved> GetSubmissionsWithParticipantsAddedOrRemoved(int conferenceId, DateTime asOfDate)
        {
            var submissionWithParticipantsAddedOrRemoved = _submissionParticipantRepository.GetSubmissionsWithParticipantsAddedOrRemoved(conferenceId, asOfDate);

            var stfmUserIds = new List<string>();

            submissionWithParticipantsAddedOrRemoved.ForEach(s => {
                stfmUserIds.AddRange(s.ParticipantsAddedOrRemoved.Select(p => p.STFMUserId).ToList());
            });

            var users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds.Distinct().ToList()).Result;

            submissionWithParticipantsAddedOrRemoved.ForEach(s => {
                s.ParticipantsAddedOrRemoved.ForEach(p => {
                    p.ParticipantName = users.FirstOrDefault(u => u.STFMUserId == p.STFMUserId)?.GetFullNamePretty() ?? "";
                });
            });

            return submissionWithParticipantsAddedOrRemoved;
        }
    }
}
