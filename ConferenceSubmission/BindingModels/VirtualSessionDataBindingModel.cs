﻿using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.BindingModels
{
    public class VirtualSessionDataBindingModel
    {
        public int SubmissionId { get; set; }
        public string SubmissionTitle { get; set; }
        public string ConferenceName { get; set; }
        public string SubmissionCategoryName { get; set; }

        public int VirtualConferenceSessionId { get; set; }

        [StringLength(10)]
        [Required]
        [Display(Name ="Session Code")]
        public string SessionCode { get; set; }

        [Required]
        [Display(Name = "Session Start Time")]
        public DateTime SessionStartDateTime { get; set; }

        [Required]
        [Display(Name = "Session End Time")]
        public DateTime SessionEndDateTime { get; set; }

        [StringLength(250)]
        [Display(Name = "Session Track")]
        public string SessionTrack { get; set; }

        public int SubmissionPresentationMethodId { get; set; }
        public SubmissionPresentationMethod SubmssionPresentationMethod { get; set; }

        public PresentationMethodType PresentationMethodType { get; set; }

        public bool IsSessionStartDateAndEndDateValid()
        {
            return SessionStartDateTime.CompareTo(DateTime.Now) >= 0 && SessionEndDateTime.CompareTo(DateTime.Now) >= 0 && SessionEndDateTime.CompareTo(SessionStartDateTime) > 0;
        }
    }
}
