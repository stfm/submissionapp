﻿using System;
namespace ConferenceSubmission.ViewModels
{
    public class SubmissionPaymentViewModel
    {
        public Boolean SuccessfulPayment { get; set; }

        public string PaymentResult { get; set; }

        public string PaymentTransactionId { get; set; }

        public int SubmissionId { get; set; }

        public bool ShowDisclosureLink { get; set; }

        public string DisclosureLinkMethod { get; set; }

    }
}
