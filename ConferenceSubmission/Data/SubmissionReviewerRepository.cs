﻿using System;
using ConferenceSubmission.Models;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace ConferenceSubmission.Data
{
    public class SubmissionReviewerRepository : ISubmissionReviewerRepository
    {

        private readonly AppDbContext _appDbContext;

        public SubmissionReviewerRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public int AddSubmissionReviewer(SubmissionReviewer submissionReviewer)
        {

            _appDbContext.SubmissionReviewer.Add(submissionReviewer);

            _appDbContext.SaveChanges();

            return submissionReviewer.SubmissionReviewerId;

        }

        public void DeleteSubmissionReviewer(SubmissionReviewer submissionReviewer)
        {
            _appDbContext.SubmissionReviewer.Remove(submissionReviewer);

            _appDbContext.SaveChanges();
 
        }

        public SubmissionReviewer GetSubmissionReviewer(int submissionId, int reviewerId)
        {


            return _appDbContext.SubmissionReviewer.Where(sr => sr.SubmissionId == submissionId && sr.ReviewerId == reviewerId).FirstOrDefault();


        }

        public IEnumerable<SubmissionReviewer> GetSubmissionReviewers(int submissionId)
        {

            return _appDbContext.SubmissionReviewer.Include(sr => sr.Reviewer ).Include(sr => sr.Submission).Where(sr => sr.SubmissionId == submissionId);

        }

        public IEnumerable<SubmissionReviewer> GetSubmissionReviewers(Reviewer reviewer)
        {

            return _appDbContext.SubmissionReviewer.Include(sr => sr.Reviewer).Include(sr => sr.Submission).Where( sr => sr.ReviewerId == reviewer.ReviewerId);

        }
    }
}
