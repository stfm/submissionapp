﻿using System;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionAdminHomeViewModel
    {
        public List<ConferenceViewModel> ActiveConferenceViewModels { get; set; }

        public SubmissionAdminHomeViewModel()
        {

            ActiveConferenceViewModels = new List<ConferenceViewModel>();

        }
    }
}
