﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public class SubmissionEqualityComparator : IEqualityComparer<Submission>
    {
        public SubmissionEqualityComparator()
        {
        }

        public bool Equals(Submission x, Submission y)
        {
            return x.SubmissionId == y.SubmissionId;
        }

        public int GetHashCode(Submission obj)
        {
            return obj.SubmissionId.GetHashCode();
        }
    }
}
