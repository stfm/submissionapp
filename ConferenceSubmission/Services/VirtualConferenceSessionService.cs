﻿using ConferenceSubmission.BindingModels;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface IVirtualConferenceSessionService
    {
        /// <summary>
        /// Gets the Virtual Conference Session that matches the passed in VirtualConferenceSessionId
        /// </summary>
        /// <param name="virtualConferenceSessionId">Id of the Virtual Conference Sessions</param>
        /// <returns>VirtualConferenceSession</returns>
        VirtualConferenceSession GetWithSubmissionData(int virtualConferenceSessionId);

        /// <summary>
        /// Updates a Virtual Conference Sessions
        /// </summary>
        /// <param name="virtualSessionDataBindingModel">Model class that contains the updated Virtual Conference Session data</param>
        void Update(VirtualSessionDataBindingModel virtualSessionDataBindingModel);

        /// <summary>
        /// Creates a new Virtual Conference Session Record
        /// </summary>
        /// <param name="virtualSessionDataBindingModel">Model class that contains the Virtual Conference Sessions data</param>
        /// <returns>Id of the created Virtual Conference Session</returns>
        int Create(VirtualSessionDataBindingModel virtualSessionDataBindingModel);
    }

    public class VirtualConferenceSessionService : IVirtualConferenceSessionService
    {
        private readonly IVirtualConferenceSessionRepository _virtualConferenceSessionRepository;
        public VirtualConferenceSessionService(IVirtualConferenceSessionRepository virtualConferenceSessionRepository)
        {
            _virtualConferenceSessionRepository = virtualConferenceSessionRepository;
        }

        public int Create(VirtualSessionDataBindingModel virtualSessionDataBindingModel)
        {
            var result = _virtualConferenceSessionRepository.Create(virtualSessionDataBindingModel);

            return result;
        }

        public VirtualConferenceSession GetWithSubmissionData(int virtualConferenceSessionId)
        {
            var virtualConferenceSession = _virtualConferenceSessionRepository.GetWithSubmissionData(virtualConferenceSessionId);

            return virtualConferenceSession;
        }

        public void Update(VirtualSessionDataBindingModel virtualSessionDataBindingModel)
        {
            _virtualConferenceSessionRepository.Update(virtualSessionDataBindingModel);
        }
    }
}
