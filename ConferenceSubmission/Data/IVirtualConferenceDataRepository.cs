﻿using ConferenceSubmission.Models;


namespace ConferenceSubmission.Data
{
    public interface IVirtualConferenceDataRepository
    {

        /// <summary>
        /// Get VirtualConferenceData record for provided conferenceId.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns>VirtualConferenceData</returns>
        VirtualConferenceData GetVirtualConferenceData(int conferenceId);
    }
}
