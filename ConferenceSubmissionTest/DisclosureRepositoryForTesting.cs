﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class DisclosureRepositoryForTesting : IDisclosureRepository
    {
        public int AddDisclosure(Disclosure disclosure)
        {
            throw new NotImplementedException();
        }

        public Disclosure GetDisclosure(int disclosureId)
        {

            Disclosure disclosure = new Disclosure
            {

                DisclosureId = 1,

                StfmUserId = "user1",

                DisclosureDate = DateTime.Parse("Jan 15, 2018")


            };

            return disclosure;
        }

        public Disclosure GetDisclosure(string stfmUserId)
        {

            Disclosure disclosure1 = new Disclosure
            {

                DisclosureId = 1,

                StfmUserId = "user1",

                DisclosureDate = DateTime.Parse("Jan 15, 2018")


            };


            Disclosure disclosure2 = new Disclosure
            {

                DisclosureId = 1,

                StfmUserId = "user2",

                DisclosureDate = DateTime.Parse("Jan 15, 2017")


            };


            if ( stfmUserId.Equals("user1")) {

                return disclosure1;
            }

            if (stfmUserId.Equals("user2"))
            {

                return disclosure2;
            }

            return null;

        }

        public void UpdateDisclosure(Disclosure disclosure)
        {
            throw new NotImplementedException();
        }
    }
}
