﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ContentNotFoundPageViewModel
    {
        public int EntityId { get; set; }
        public string EntityType { get; set; }
        public string ReturnController { get; set; }
        public string ReturnAction { get; set; }
    }
}
