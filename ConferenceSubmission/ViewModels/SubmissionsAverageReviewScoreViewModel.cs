﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsAverageReviewScoreViewModel
    {
       
        public List<SubmissionsAverageReviewScore> SubmissionsAverageReviews { get; set; }

        public string ConferenceLongName { get; set; }

        public string SubmissionCategoryName { get; set; }

        public int CategoryId { get; set; }


        public SubmissionsAverageReviewScoreViewModel() {


            SubmissionsAverageReviews = new List<SubmissionsAverageReviewScore>();

        }

    }
}
