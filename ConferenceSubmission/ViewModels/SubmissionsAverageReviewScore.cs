﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsAverageReviewScore
    {
        public int SubmissionId { get; set; }

        public string SubmissionTitle { get; set; }

        public string SubmissionCategory { get; set; }

        public string SubmitterStfmUserId { get; set; }

        public User SubmitterUser { get; set; }

        public string LeadPresenterUserId { get; set; }

        public User LeadPresenterUser { get; set; }

        public double AverageReviewScore { get; set; }

        public string SubmissionStatus { get; set; }

        public string KeyWordOne { get; set; }

        public string KeyWordTwo { get; set; }

        public string GroupSubmission { get; set; }

        public string MultiInstitutional { get; set; }

        public string StudentIncluded { get; set; }

        public string AcademicCoordinator { get; set; }

        public string BehavioralScience { get; set; }

        

    }


}
