﻿var toggleConferenceInactive = function (conferenceId) {
    $.ajax({
        url: "/submissionadmin/ToggleConferenceInactive?conferenceId=" + conferenceId,
        dataType: "html",
        method:  "POST"
    }).done(function (data) {
        $("#conferenceInactive-" + conferenceId).html(data);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#conferenceInactive-" + conferenceId).html("<p class='text-red'>There was an error on the server.</p>");
    });
};

var showEndDatePicker = function (conferenceId, isCFP) {
    let deadlineType = isCFP ? "CFP" : "SubmissionReview";
    $(`#toggle${deadlineType}EndDate-${conferenceId}`).hide();
    $(`#edit${deadlineType}EndDateContainer-${conferenceId}`).show();
    $(`#${deadlineType}DateTextOnly-${conferenceId}`).show();
};

var cancelEditEnddate = function (conferenceId, isCFP) {
    let deadlineType = isCFP ? "CFP" : "SubmissionReview";
    $(`#toggle${deadlineType}EndDate-${conferenceId}`).show();
    $(`#edit${deadlineType}EndDateContainer-${conferenceId}`).hide();
    $(`#${deadlineType}DateTextOnly-${conferenceId}`).hide();
};

var saveEnddate = function (conferenceId, isCFP) {
    let deadlineType = isCFP ? "CFP" : "SubmissionReview";
    var selectedDate = new Date($(`#${deadlineType}EndDatePicker-${conferenceId}`).val());
    var data = {
        conferenceId: conferenceId,
        endDate: formatJavascriptDateToDotNetDate(selectedDate)
    };

    $.ajax({
        url: `/SubmissionAdmin/Edit${deadlineType}EndDate`,
        method: "POST",
        dataType: "HTML",
        data: data
    })
    .done(function (data) {
        $(`#${deadlineType}EndDateContainer-${conferenceId}`).html(data);
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        $(`#${deadlineType}EndDateContainer-${conferenceId}`).html("<p class='text-red'>We're sorry, there was an error on the server while processing your request.</p>");
    });
};

var formatJavascriptDateToDotNetDate = function (javascriptDate) {
    return `${javascriptDate.getFullYear()}-${javascriptDate.getMonth() + 1}-${javascriptDate.getDate()}T${formatTimeParameter(javascriptDate.getHours())}:${formatTimeParameter(javascriptDate.getMinutes())}:${formatTimeParameter(javascriptDate.getSeconds())}`;
};

var formatTimeParameter = function (timeParameter) {
    return timeParameter < 10 ? `0${timeParameter}` : timeParameter;
};

var refreshSessionSearchCache = function (conferenceId, conferenceName) {
    $.ajax({
        url: "/submissionadmin/ClearConferenceSessionsSearchCache?conferenceId=" + conferenceId,
        dataType: "html",
        method: "POST"
    }).done(function (data) {
        alert(`The Conference Session Search Cache has been cleared for ${conferenceName}.`);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert(`The server errored when attempting to clear the Conference Session Search Cache for ${conferenceName}.`);
    });
}