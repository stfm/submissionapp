﻿using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class GetSubmissionActionsForSubmissionServiceTest
    {
        private GetSubmissionActionsForSubmissionService getSubmissionActionsForSubmissionService = new GetSubmissionActionsForSubmissionService();

        [Fact]
        public void Execute_IsNotLeadPresenterOrSubmitterAndIsOpenAndSubmissionDeadlineNotPassed_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(30);
            var submissionPresenationMethods = new List<SubmissionPresentationMethod>();
            var submissionStatusTypeOpen = SubmissionStatusType.OPEN;
            var isLeadPresenterOrSubmitter = false;

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Single(results);
            Assert.Contains(SubmissionAction.VIEW, results);
        }

        [Fact]
        public void Execute_IsNotLeadPresenterOrSubmitterAndIsAcceptedAndNoVirtualSubmissionPresentationMethods_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(30);
            var submissionPresenationMethods = new List<SubmissionPresentationMethod>();
            var submissionStatusTypeOpen = SubmissionStatusType.ACCEPTED;
            var isLeadPresenterOrSubmitter = false;

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Single(results);
            Assert.Contains(SubmissionAction.VIEW, results);
        }

        [Fact]
        public void Execute_IsNotLeadPresenterOrSubmitterAndIsAcceptedAndHasRecordedOnlineSubmissionPresentationMethodAndStatusIsPending_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(30);
            var submissionStatusTypeOpen = SubmissionStatusType.ACCEPTED;
            var isLeadPresenterOrSubmitter = false;

            var submissionPresenationMethods = new List<SubmissionPresentationMethod>()
            {
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.LIVE_IN_PERSON
                    }
                },
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.RECORDED_ONLINE
                    },
                    PresentationStatus = new PresentationStatus
                    {
                        PresentationStatusName = PresentationStatusType.PENDING
                    }
                }
            };

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Single(results);
            Assert.Contains(SubmissionAction.VIEW, results);
        }

        [Fact]
        public void Execute_IsLeadPresenterOrSubmitterAndIsOpenAndSubmissionDeadlineNotPassed_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(30);
            var submissionPresenationMethods = new List<SubmissionPresentationMethod>();
            var submissionStatusTypeOpen = SubmissionStatusType.OPEN;
            var isLeadPresenterOrSubmitter = true;

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Equal(4, results.Count);
            Assert.Contains(SubmissionAction.UPDATE_PARTICIPANTS, results);
            Assert.Contains(SubmissionAction.VIEW, results);
            Assert.Contains(SubmissionAction.EDIT, results);
            Assert.Contains(SubmissionAction.WITHDRAW, results);
        }

        [Fact]
        public void Execute_IsLeadPresenterOrSubmitterAndIsOpenAndSubmissionDeadlinePassed_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(-30);
            var submissionPresenationMethods = new List<SubmissionPresentationMethod>();
            var submissionStatusTypeOpen = SubmissionStatusType.OPEN;
            var isLeadPresenterOrSubmitter = true;

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Equal(2, results.Count);
            Assert.Contains(SubmissionAction.UPDATE_PARTICIPANTS, results);
            Assert.Contains(SubmissionAction.VIEW, results);
        }

        [Fact]
        public void Execute_IsLeadPresenterOrSubmitterAndIsAcceptedAndNoVirtualSubmissionPresentationMethods_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(-30);
            var submissionPresenationMethods = new List<SubmissionPresentationMethod>()
            {
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.LIVE_IN_PERSON
                    }
                }
            };
            var submissionStatusTypeOpen = SubmissionStatusType.ACCEPTED;
            var isLeadPresenterOrSubmitter = true;

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Equal(2, results.Count);
            Assert.Contains(SubmissionAction.UPDATE_PARTICIPANTS, results);
            Assert.Contains(SubmissionAction.VIEW, results);
        }

        [Fact]
        public void Execute_IsLeadPresenterOrSubmitterAndIsAcceptedAndHasRecordedOnlineSubmissionPresentationMethodAndStatusIsPending_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(-30);
            var submissionPresenationMethods = new List<SubmissionPresentationMethod>()
            {
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.LIVE_IN_PERSON
                    }
                },
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.RECORDED_ONLINE
                    },
                    PresentationStatus = new PresentationStatus
                    {
                        PresentationStatusName = PresentationStatusType.PENDING
                    }
                }
            };
            var submissionStatusTypeOpen = SubmissionStatusType.ACCEPTED;
            var isLeadPresenterOrSubmitter = true;

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Equal(3, results.Count);
            Assert.Contains(SubmissionAction.UPDATE_PARTICIPANTS, results);
            Assert.Contains(SubmissionAction.VIEW, results);
            Assert.Contains(SubmissionAction.ACCEPT_OR_DECLINE_PRESENTATION_METHOD, results);
        }

        [Fact]
        public void Execute_IsLeadPresenterOrSubmitterAndIsAcceptedAndHasRecordedOnlineSubmissionPresentationMethodAndStatusIsNeedPresentationMaterial_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(-30);
            var submissionPresenationMethods = new List<SubmissionPresentationMethod>()
            {
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.LIVE_IN_PERSON
                    }
                },
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.RECORDED_ONLINE
                    },
                    PresentationStatus = new PresentationStatus
                    {
                        PresentationStatusName = PresentationStatusType.NEED_PRESENTATION_MATERIAL
                    }
                }
            };
            var submissionStatusTypeOpen = SubmissionStatusType.ACCEPTED;
            var isLeadPresenterOrSubmitter = true;

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Equal(4, results.Count);
            Assert.Contains(SubmissionAction.UPDATE_PARTICIPANTS, results);
            Assert.Contains(SubmissionAction.VIEW, results);
            Assert.Contains(SubmissionAction.UPLOAD_MATERIAL, results);
        }

        [Fact]
        public void Execute_IsLeadPresenterOrSubmitterAndIsAcceptedAndHasRecordedOnlineSubmissionPresentationMethodAndStatusIsDeclined_VerifyResults()
        {
            //Arrange
            var submissionDeadline = DateTime.Now.AddDays(-30);
            var submissionPresenationMethods = new List<SubmissionPresentationMethod>()
            {
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.LIVE_IN_PERSON
                    }
                },
                new SubmissionPresentationMethod
                {
                    PresentationMethod = new PresentationMethod
                    {
                        PresentationMethodName = PresentationMethodType.RECORDED_ONLINE
                    },
                    PresentationStatus = new PresentationStatus
                    {
                        PresentationStatusName = PresentationStatusType.DECLINED
                    }
                }
            };
            var submissionStatusTypeOpen = SubmissionStatusType.ACCEPTED;
            var isLeadPresenterOrSubmitter = true;

            //Act
            var results = getSubmissionActionsForSubmissionService.Execute(isLeadPresenterOrSubmitter, submissionPresenationMethods, submissionStatusTypeOpen, submissionDeadline);

            //Assert
            Assert.Equal(2, results.Count);
            Assert.Contains(SubmissionAction.UPDATE_PARTICIPANTS, results);
            Assert.Contains(SubmissionAction.VIEW, results);
        }

    }

}

