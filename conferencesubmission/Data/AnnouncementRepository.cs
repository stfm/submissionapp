﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Data
{
    public class AnnouncementRepository : IAnnouncementRepository
    {
        private readonly AppDbContext _context;
        public AnnouncementRepository(AppDbContext context)
        {
            _context = context;
        }

        public int Create(Announcement announcement)
        {
            _context.Announcements.Add(announcement);
            _context.SaveChanges();
            return announcement.AnnouncementId;
        }

        public void Delete(int announcementId)
        {
            var announcementToRemove = _context.Announcements.FirstOrDefault(a => a.AnnouncementId == announcementId);
            _context.Announcements.Remove(announcementToRemove);
            _context.SaveChanges();
        }

        public Announcement Get(int id)
        {
            return _context.Announcements.FirstOrDefault(a => a.AnnouncementId == id);
        }

        public List<Announcement> GetAll()
        {
            // When using StringComparison.InvariantCultureIgnoreCase, the filtering is done on the server. In EF Core 3, we are required to explicitely
            // tell the compiler that we intend on doing so by adding a ToList() before we do the filtering.
            // https://docs.microsoft.com/en-us/ef/core/querying/client-eval
            return _context.Announcements.ToList().Where(a => a.Content.Contains("CERA", StringComparison.InvariantCultureIgnoreCase).Equals(false)).OrderBy(a => a.SortOrder).ToList();
        }

        public List<Announcement> GetAllCera()
        {
            // When using StringComparison.InvariantCultureIgnoreCase, the filtering is done on the server. In EF Core 3, we are required to explicitely
            // tell the compiler that we intend on doing so by adding a ToList() before we do the filtering.
            // https://docs.microsoft.com/en-us/ef/core/querying/client-eval
            return _context.Announcements.ToList().Where(a => a.Content.Contains("CERA", StringComparison.InvariantCultureIgnoreCase)).OrderBy(a => a.SortOrder).ToList();
        }

        public void Update(Announcement announcement)
        {
            var announcementFromDb = _context.Announcements.FirstOrDefault(a => a.AnnouncementId == announcement.AnnouncementId);
            if(announcementFromDb != null)
            {
                announcementFromDb.SortOrder = announcement.SortOrder;
                announcementFromDb.Content = announcement.Content;
                _context.SaveChanges();
            }
        }
    }

    public interface IAnnouncementRepository
    {
        Announcement Get(int id);
        List<Announcement> GetAll();
        int Create(Announcement announcement);
        void Update(Announcement announcement);
        void Delete(int announcementId);
       
        /// <summary>
        /// Get just the CERA announcements.
        /// </summary>
        /// <returns></returns>
        List<Announcement> GetAllCera();
    }
}
