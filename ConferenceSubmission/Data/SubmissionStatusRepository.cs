﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public class SubmissionStatusRepository : ISubmissionStatusRepository
    {
        private readonly AppDbContext _appDbContext;

        public SubmissionStatusRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public SubmissionStatus GetBySubmissionStatusType(SubmissionStatusType submissionStatusType)
        {
            return _appDbContext.SubmissionStatuses.FirstOrDefault(s => s.SubmissionStatusName == submissionStatusType);
        }
    }
}
