﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using Moq;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class ReviewerServiceTest
    {
        [Fact]
        public void GetAssignedSubmissionsWithReviewStatusTest()
        {
            //arrange
            Mock<IReviewStatusService> reviewStatusService = new Mock<IReviewStatusService>();
            reviewStatusService
                .Setup(r => r.GetReviewStatusForReview(1, 1))
                .Returns(new ReviewStatus { ReviewStatusType = ReviewStatusType.NOT_STARTED});

            reviewStatusService
                .Setup(r => r.GetReviewStatusForReview(2, 1))
                .Returns(new ReviewStatus { ReviewStatusType = ReviewStatusType.INCOMPLETE });

            reviewStatusService
                .Setup(r => r.GetReviewStatusForReview(3, 1))
                .Returns(new ReviewStatus { ReviewStatusType = ReviewStatusType.COMPLETE });

            Mock<IReviewerRepository> reviewerRepository = new Mock<IReviewerRepository>();
            reviewerRepository
                .Setup(r => r.GetAssignedSubmissions("12345"))
                    .Returns(new List<SubmissionReviewer> {
                        new SubmissionReviewer
                        {
                            ReviewerId = 1,
                            SubmissionId = 1,
                            SubmissionReviewerId = 1
                        },
                        new SubmissionReviewer
                        {
                            ReviewerId = 1,
                            SubmissionId = 2,
                            SubmissionReviewerId = 2
                        },
                                                new SubmissionReviewer
                        {
                            ReviewerId = 1,
                            SubmissionId = 3,
                            SubmissionReviewerId = 3
                        },
                    });

            reviewerRepository
                .Setup(r => r.ReviewerExists("12345")).Returns(true);


            ReviewerService reviewerService = new ReviewerService(reviewerRepository.Object, reviewStatusService.Object, null, null);

            //act
            var result = reviewerService.GetAssignedSubmissionWithReviewStatus("12345");

            //assert
            Assert.Equal(3, result.Count);
            Assert.True(result.Where(r => r.ReviewStatus.ReviewStatusType == ReviewStatusType.INCOMPLETE).ToList().Count() == 1);
            Assert.True(result.Where(r => r.ReviewStatus.ReviewStatusType == ReviewStatusType.NOT_STARTED).ToList().Count() == 1);
            Assert.True(result.Where(r => r.ReviewStatus.ReviewStatusType == ReviewStatusType.COMPLETE).ToList().Count() == 1);
        }
    }
}
