﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.DTOs.SalesforceEvent;
using ConferenceSubmission.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Serilog;

namespace ConferenceSubmission.Services
{
    public class SalesForceAPIService : ISalesforceAPIService
    {
        private readonly IConfiguration _configuration;
        private readonly ISalesforceGetUsersService _salesforceGetUsersService;

        public SalesForceAPIService(IConfiguration configuration, ISalesforceGetUsersService salesforceGetUsersService)
        {
            _configuration = configuration;
            _salesforceGetUsersService = salesforceGetUsersService;
        }

        public async Task<IEnumerable<User>> FindPeopleByLastName(string lastName)
        {
            try
            {
                var salesforceAPIVariables = await getAPIVariables();

                var usersReturnedFromSalesforce = await _salesforceGetUsersService.GetUsersByLastName(lastName, salesforceAPIVariables);

                return usersReturnedFromSalesforce;
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"The was a problem making the call to the Salesforce API to get users with last name of " + lastName);
                throw ex;
            }

        }

        public async Task<IEnumerable<SFEvent>> GetEventsByDateRange(DateTime startDate, DateTime endDate)
        {
            try
            {
                var query = $"SELECT+Id,Name,EventApi__Start_Date__c,EventApi__End_Date__c+FROM+EventApi__Event__c+WHERE+EventApi__Start_Date__c+>=+{getFormattedDateForSOQLQuery(startDate)}+AND+EventApi__Start_Date__c+<=+{getFormattedDateForSOQLQuery(endDate)}";

                JToken records = await GetResultsFromSOQLQuery(query);

                var results = records.Children().Select(c => new SFEvent
                {
                    EventId = c.Value<string>("Id") ?? string.Empty,
                    EventName = c.Value<string>("Name") ?? string.Empty,
                    EventStartDate = c.Value<string>("EventApi__Start_Date__c") ?? string.Empty,
                    EventEndDate = c.Value<string>("EventApi__End_Date__c") ?? string.Empty
                });

                return results;
            }
            catch(Exception ex)
            {
                Log.Error(ex, $"The system errored when calling Salesforce for Events between {startDate} and {endDate}. Exception: {ex.Message}");
                throw;
            }
        }

        public async Task<IEnumerable<User>> GetMultipleUserInfo(IEnumerable<string> stfmUserIds)
        {
            if (stfmUserIds == null || stfmUserIds.Count() == 0)
            {
                return new List<User>();
            }
            try
            {
                var salesforceAPIVariables = await getAPIVariables();

                var userQueryCount = (_configuration["SalesforceAPI:UserQueryCount"] == null || _configuration["SalesforceAPI:UserQueryCount"] == "0") ? 500 : Convert.ToInt32(_configuration["SalesforceAPI:UserQueryCount"]);

                var numberOfCallsToSalesforce = stfmUserIds.Count() % userQueryCount == 0 ?
                    stfmUserIds.Count() / userQueryCount :
                    (stfmUserIds.Count() / userQueryCount) + 1;

                var usersReturnedFromSalesforce = new List<User>();

                for (int i = 0; i < numberOfCallsToSalesforce; i++)
                {
                    var partitionedUserIds = stfmUserIds.Skip(i * userQueryCount).Take(userQueryCount).ToList();
                    var partitionedUsers = await _salesforceGetUsersService.GetPartitionedUsers(partitionedUserIds, salesforceAPIVariables);
                    usersReturnedFromSalesforce.AddRange(partitionedUsers);
                }

                return usersReturnedFromSalesforce;
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"The was a problem making the call to the Salesforce API to get {stfmUserIds.Count()} users.");
                throw ex;
            }
        }

        public async Task<User> GetUserInfo(string stfmUserId)
        {
            //Build a SOQL query to fetch a single User by Id
            var query = $"SELECT+Contact.FirstName,Contact.LastName,Contact.Credentials__c,Contact.Other_Credentials__c,Contact.Email,Id,UserName+FROM+User+WHERE+Id+=+'{stfmUserId}'";

            JToken records = await GetResultsFromSOQLQuery(query);

            return records.Children().Select(c => new User
            {
                FirstName = c.SelectToken("Contact")?.SelectToken("FirstName")?.Value<string>() ?? string.Empty,
                LastName = c.SelectToken("Contact")?.SelectToken("LastName")?.Value<string>() ?? string.Empty,
                STFMUserId = c.Value<string>("Id"),
                Email = c.SelectToken("Contact")?.SelectToken("Email")?.Value<string>() ?? string.Empty,
                UserName = c.Value<string>("Username"),
                Credentials = $"{(c.SelectToken("Contact")?.SelectToken("Credentials__c")?.Value<string>() ?? string.Empty).Replace(";", ", ")}, {(c.SelectToken("Contact")?.SelectToken("Other_Credentials__c")?.Value<string>() ?? string.Empty).Replace(";", ", ")}".TrimEnd(new char[] { ',', ' ' })
            }).FirstOrDefault();
        }

        /// <summary>
        /// Given an STFMUserId, returns true if the user is an STFM Member, false otherwise.
        /// This method hits the SalesForce API and requests the user's badges.
        /// Note: In SalesForce, badges are found in the Contact store - NOT the User store. In the application, we store the user's UserId, not his ContactId,
        ///       so we first query for the User, then we can query the related Contact record and get the badges.
        /// NOte: In SalesForce, badges are stored in the Contact record under the name "OrderApi__Badges__c"
        /// 
        /// The user must have one of the two following badges to be considered an STFM Member
        /// 1. STFM Member Badge
        /// 2. STFM Student Member Badge
        /// </summary>
        /// <returns>true or false</returns>
        public async Task<bool> IsSTFMMember(string stfmUserId)
        {
            var salesforceAPIVariables = await getAPIVariables();

            //Build an SOQL query to fetch the member's information from Salesforce
            var query = $"Select+OrderApi__Badges__c+from+Contact+where+Id+in+(SELECT+ContactId+FROM+User+where+Id='{stfmUserId}')";
            string restQuery = salesforceAPIVariables.ServiceUrl + $"/services/data/v56.0/query/?q={query}";

            //Create the request that will call the Salesforce API
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, restQuery);

            //Add OAuth token to header
            request.Headers.Add("Authorization", "Bearer " + salesforceAPIVariables.OAuthToken);

            //Tell Salesforce to return JSON
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Call endpoint async
            HttpClient queryClient = new HttpClient();
            HttpResponseMessage response = await queryClient.SendAsync(request);

            string result = await response.Content.ReadAsStringAsync();

            var records = JObject.Parse(result)["records"];

            var badges = records.Children().Select(child => ((string)child["OrderApi__Badges__c"])?.Split(",").ToList()
            ).FirstOrDefault();

            //badges will be null if SalesForce did not find any badges for the user.
            //For this method to return true, badges must NOT be null and one of the badges must be "STFM Member Badge" or "STFM Student Member Badge"
            return badges != null && (badges.Any(b => b.ToLower() == "stfm member badge") || badges.Any(b => b.ToLower() == "stfm student member badge"));
        }

        public async Task<IEnumerable<EventAttendee>> GetEventAttendees(string eventId)
        {
            //Build a SOQL query to fetch a single User by Id
            var query = $"SELECT+Id,Contact.FirstName,Contact.LastName,Contact.Credentials__c,Contact.Email+FROM+User+WHERE+ContactId+IN+(SELECT+EventApi__Contact__c+FROM+EventApi__Attendee__c+WHERE+EventApi__Attendee_Event__r.Id+=+'{eventId}'+AND+EventApi__Status__c+=+'Registered')";

            JToken records = await GetResultsFromSOQLQuery(query);

            var results = records.Children().Select(c => new EventAttendee
            {
                STFMUserId = c.Value<string>("Id") ?? string.Empty,
                FirstName = c.SelectToken("Contact")?.SelectToken("FirstName")?.Value<string>() ?? string.Empty,
                LastName = c.SelectToken("Contact")?.SelectToken("LastName")?.Value<string>() ?? string.Empty,
                Credentials = c.SelectToken("Contact")?.SelectToken("Credentials__c")?.Value<string>() ?? string.Empty,
                Email = c.SelectToken("Contact")?.SelectToken("Email")?.Value<string>() ?? string.Empty
            });

            return results;
        }

        #region Helper Methods

        private async Task<JToken> GetResultsFromSOQLQuery(string query)
        {
            var salesforceAPIVariables = await getAPIVariables();

            string restQuery = salesforceAPIVariables.ServiceUrl + $"/services/data/v56.0/query/?q={query}";

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, restQuery);

            //add token to header
            request.Headers.Add("Authorization", "Bearer " + salesforceAPIVariables.OAuthToken);

            //return JSON to the caller
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Create the request that will call the Salesforce API
            HttpClient queryClient = new HttpClient();

            //call endpoint async
            HttpResponseMessage response = await queryClient.SendAsync(request);
            string result = await response.Content.ReadAsStringAsync();
            //Log.Information("Result of calling SalesForce for " + stfmUserId + " is " + result);
            var obj = JObject.Parse(result);

            var records = JObject.Parse(result)["records"];
            return records;
        }

        /// <summary>
        /// This method authenticates the application with Salesforce using OAuth2.
        /// Once authenticated, Salesforce returns an Access Token and 
        /// an Instance URL, both of which are required to access the Salesforce API. 
        /// </summary>
        /// <returns>A SalesforceAPIVariables object which contains the Access Token and Instance URL</returns>
        private async Task<SalesforceAPIVariables> getAPIVariables()
        {

            HttpContent content = new FormUrlEncodedContent(new Dictionary<string, string> {
                    {"grant_type",      "password"},
                    {"client_id",       _configuration["SalesforceAPI:SalesforceAPIConsumerKey"]},
                    {"client_secret",   _configuration["SalesforceAPI:SalesforceAPIConsumerSecret"]},
                    {"username",        _configuration["SalesforceAPI:SalesforceAPIUsername"]},
                    {"password",        $"{_configuration["SalesforceAPI:SalesforceAPIPassword"]}{_configuration["SalesforceAPI:SalesforceAPISecurityToken"]}"}
            });

            var client = new HttpClient();
            HttpResponseMessage message;
            if (_configuration["Salesforce:Environment"] == "Test")
            {
                message = await client.PostAsync("https://test.salesforce.com/services/oauth2/token", content);
            }
            else
            {
                message = await client.PostAsync("https://login.salesforce.com/services/oauth2/token", content);
            }

            string responseString = await message.Content.ReadAsStringAsync();

            //Parse out the JSON
            JObject obj = JObject.Parse(responseString);

            //Retrieve the access token and instance URL that will be used to make API calls
            return new SalesforceAPIVariables
            {
                OAuthToken = (string)obj["access_token"],
                ServiceUrl = (string)obj["instance_url"]
            };
        }

        private string getFormattedDateForSOQLQuery(DateTime dateTime)
        {
            var result = $"{dateTime.Year}-{formatMonthOrDayWithLeadingZero(dateTime.Month)}-{formatMonthOrDayWithLeadingZero(dateTime.Day)}";
            return result;
        }

        private string formatMonthOrDayWithLeadingZero(int number)
        {
            return number > 9 ? $"{number}" : $"0{number}";
        }

        public bool isAdminUser(IEnumerable<System.Security.Claims.Claim> claims)
        {
            bool isAdmin = false;

            string accessPermissions = claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions") == null ?
                           "" : claims.FirstOrDefault(c => c.Type == "urn:salesforce:AccessPermissions").Value;

            if (accessPermissions.Contains("Admin", StringComparison.CurrentCultureIgnoreCase) == true)
            {

                isAdmin = true;

            }
            

            return isAdmin;
        }

        #endregion
    }
}
