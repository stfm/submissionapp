﻿using System;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.Data
{
    public interface IReviewRepository
    {
        /// <summary>
        /// Adds the review.
        /// </summary>
        /// <returns>The review id</returns>
        /// <param name="review">Review.</param>
        int AddReview(Review review);

        /// <summary>
        /// Gets the review.
        /// </summary>
        /// <returns>The review.</returns>
        /// <param name="reviewId">Review identifier.</param>
        Review GetReview(int reviewId);

        /// <summary>
        /// Gets the review using submissionId and reviewerId.
        /// May return null if no review exists.
        /// </summary>
        /// <returns>The review.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="reviewerId">Reviewer identifier.</param>
        Review GetReview(int submissionId, int reviewerId);

        /// <summary>
        /// Updates the review.
        /// </summary>
        /// <param name="review">Review.</param>
        void UpdateReview(Review review);

        /// <summary>
        /// Gets the COMPLETED reviews for the provided submission
        /// </summary>
        /// <returns>The reviews.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        IEnumerable<Review> GetCompletedReviews(int submissionId);

        /// <summary>
        /// Gets all the completed reviews for all submissions
        /// in the provided category.
        /// </summary>
        /// <returns>The completed reviews for all submissions in the completed category.</returns>
        /// <param name="categoryId">Category identifier.</param>
        IEnumerable<Review> GetCompletedReviewsForCategory(int categoryId);

        /// <summary>
        /// Gets if the reviewer has completed his review of the provided 
        /// <paramref name="submissionId"/>.
        /// </summary>
        /// <returns><c>true</c>, if review of submission by the provided 
        /// reviewerId is complete <c>false</c> otherwise.</returns>
        /// <param name="reviewerId">Reviewer identifier.</param>
        /// <param name="submissionId">Submission identifier.</param>
        Boolean GetReviewCompleted(int reviewerId, int submissionId);

        /// <summary>
        /// Get collection of Submission objects where the submission is 
        /// completed but not assigned to a reviewer.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns>Collection of Submission objects</returns>
        IEnumerable<Submission> GetSubmissionsNotAssignedToReviewer(int conferenceId);

        /// <summary>
        /// Get collection of Submission objects where the submission is
        /// completed, assigned to one or more reviewers but no
        /// review is completed.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns>Collection of Submission Objects</returns>
        IEnumerable<Submission> GetSubmissionsNoReviewsCompleted(int conferenceId);


        /// <summary>
        /// Get collection of Submission objects where the submission is 
        /// assigned to one or more reviewers.
        /// </summary>
        /// <param name="conferenceId">conference ID</param>
        /// <returns>Collection of Submissions</returns>
        IEnumerable<Submission> GetSubmissionsAssignedToReviewer(int conferenceId);

    }

}
