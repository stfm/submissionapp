﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ConferenceSubmission.Data
{
    public class ConferenceSessionRepository : IConferenceSessionRepository
    {

        private readonly AppDbContext _appDbContext;

        public ConferenceSessionRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public int AddConferenceSession(ConferenceSession conferenceSession)
        {
            _appDbContext.Add(conferenceSession);

            _appDbContext.SaveChanges();

            return conferenceSession.ConferenceSessionId;
        }

        public ConferenceSession GetConferenceSession(int submissionId)
        {

            return _appDbContext.ConferenceSessions.FirstOrDefault(cs => cs.SubmissionId == submissionId);

        }

        public IEnumerable<ConferenceSession> GetSessionsForConference(int conferenceId)
        {
            return _appDbContext.ConferenceSessions.Where(cs => cs.Submission.Conference.ConferenceId == conferenceId).
                                Include(cs => cs.Submission).ThenInclude(s => s.AcceptedCategory);
        }

        public int UpdateConferenceSession(ConferenceSession conferenceSession)
        {
            _appDbContext.Update<ConferenceSession>(conferenceSession);

            _appDbContext.SaveChanges();

            return conferenceSession.ConferenceSessionId;
        }
    }
}
