﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class FAQ
    {
        public int      FAQId       { get; set; }
        public string   Question    { get; set; }
        public string   Answer      { get; set; }
        public int      SortOrder   { get; set; }

        //Comma delimited list of ConferenceTypeIds from
        //table ConferenceTypes that this FAQ applies to
        //For example "1,2,3" applies to all 3 of those
        //ConferenceTypes and "4" applies just to that
        //ConferenceType and "1,2,3,4" applies to all those
        //ConferenceTypes
        public string ConferenceType { get; set; }

        //Set this flag to true if you want this Question and
        //Answer to be displayed on the FAQs page.
        public bool     Display     { get; set; }


    }
}
