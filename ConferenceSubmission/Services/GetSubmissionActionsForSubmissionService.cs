﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface IGetSubmissionActionsForSubmissionService
    {
        List<SubmissionAction> Execute(bool isLeadPresenterOrSubmittter, List<SubmissionPresentationMethod> submissionPresentationMethods, SubmissionStatusType submissionStatusType, DateTime submissionDeadline);
    }

    public class GetSubmissionActionsForSubmissionService : IGetSubmissionActionsForSubmissionService
    {
        public List<SubmissionAction> Execute(bool isLeadPresenterOrSubmittter, List<SubmissionPresentationMethod> submissionPresentationMethods, SubmissionStatusType submissionStatusType, DateTime submissionDeadline)
        {
            var submissionActions = new List<SubmissionAction>() { SubmissionAction.VIEW };

            if (!isLeadPresenterOrSubmittter)
            {
                return submissionActions;
            }

            if (submissionStatusType == SubmissionStatusType.OPEN || submissionStatusType == SubmissionStatusType.PENDING_REVIEW)
            {
                submissionActions.Add(SubmissionAction.UPDATE_PARTICIPANTS);

                // If the Submission Deadline has not yet passed...
                if (submissionDeadline.CompareTo(DateTime.UtcNow.AddHours(-6)) >= 0)
                {
                    submissionActions.Add(SubmissionAction.WITHDRAW);
                    submissionActions.Add(SubmissionAction.EDIT);
                }

                return submissionActions;
            }

            if (submissionStatusType == SubmissionStatusType.ACCEPTED)
            {
                submissionActions.Add(SubmissionAction.UPDATE_PARTICIPANTS);

                if (submissionPresentationMethods.Any(x => x.PresentationMethod.PresentationMethodName == PresentationMethodType.RECORDED_ONLINE && x.PresentationStatus.PresentationStatusName == PresentationStatusType.PENDING))
                {
                    submissionActions.Add(SubmissionAction.ACCEPT_OR_DECLINE_PRESENTATION_METHOD);
                }

                if (submissionPresentationMethods.Any(x => x.PresentationMethod.PresentationMethodName == PresentationMethodType.RECORDED_ONLINE && x.PresentationStatus.PresentationStatusName == PresentationStatusType.NEED_PRESENTATION_MATERIAL))
                {
                    submissionActions.Add(SubmissionAction.UPLOAD_MATERIAL);
                    submissionActions.Add(SubmissionAction.WITHDRAW_VIRTUAL_PRESENTATION);
                }
            }
            
            return submissionActions;
        }
    }
}
