﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class SubmissionCategoryRepositoryForTesting : ISubmissionCategoryRepository
    {
        public SubmissionCategoryRepositoryForTesting()
        {
        }

        public void CreateSubmissionCategory(SubmissionCategory category)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SubmissionCategory> GetSubmissionCategories(int conferenceId)
        {

            List<SubmissionCategory> submissionCategories = new List<SubmissionCategory>();

            SubmissionCategory poster = new SubmissionCategory();
            poster.ConferenceId = 100;
            poster.SubmissionCategoryId = 1;
            poster.SubmissionCategoryName = "Poster";
            poster.SubmissionCategoryInactive = true;
            poster.SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE;

            SubmissionCategory lecture = new SubmissionCategory();
            lecture.ConferenceId = 100;
            lecture.SubmissionCategoryId = 2;
            lecture.SubmissionCategoryName = "Lecture";
            lecture.SubmissionCategoryInactive = false;
            lecture.SubmissionPaymentRequirement = SubmissionPaymentRequirement.NON_MEMBER_ONLY;

            submissionCategories.Add(poster);
            submissionCategories.Add(lecture);

            return submissionCategories;


        }

        public List<SubmissionCategory> GetSubmissionCategoriesWithFormFieldsByConference(int conferenceId)
        {
            throw new NotImplementedException();
        }

        public SubmissionCategory GetSubmissionCategory(int submissionCategoryId)
        {
            SubmissionCategory lecture = new SubmissionCategory();
            lecture.ConferenceId = 100;
            lecture.SubmissionCategoryId = 2;
            lecture.SubmissionCategoryName = "Lecture";
            lecture.SubmissionCategoryInactive = false;
            lecture.SubmissionPaymentRequirement = SubmissionPaymentRequirement.NON_MEMBER_ONLY;

            return lecture;
        }
    }
}
