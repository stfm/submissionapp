﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs
{
    public class SubmissionForRegistrationReport
    {
        public SubmissionForRegistrationReport()
        {
            PresentersForRegistrationReport = new List<PresenterForRegistrationReport>();
        }

        public int SubmissionId { get; set; }
        public string SubmissionTitle { get; set; }
        public string SessionCode { get; set; }
        public string SubmissionCategory { get; set; }

        public List<PresenterForRegistrationReport> PresentersForRegistrationReport { get; set; }
    }

    public class PresenterForRegistrationReport
    {
        public PresenterForRegistrationReport()
        {
            Roles = new List<ParticipantRoleType>();
        }

        public string STFMUserId { get; set; }
        public List<ParticipantRoleType> Roles { get; set; }
    }
}
