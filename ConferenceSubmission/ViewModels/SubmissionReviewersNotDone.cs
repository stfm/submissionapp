﻿using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmission.ViewModels

{

    public class SubmissionReviewersNotDone
    {

        public List<SubmissionReviewer> submissionReviewersNotDone = new List<SubmissionReviewer>();

        public string ConferenceLongName { get; set; }

         public Submission Submission { get; set; }

        public SubmissionReviewersNotDone()
        {
        }

    }
}