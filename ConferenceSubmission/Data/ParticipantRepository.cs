﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using AuthorizeNet.Api.Contracts.V1;

namespace ConferenceSubmission.Data
{
    public class ParticipantRepository : IParticipantRepository
    {
        private readonly AppDbContext _appDbContext;

        public ParticipantRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public IEnumerable<ParticipantRole> GetParticipantRoles {

            get
            {

                return _appDbContext.ParticipantRoles;
            }

        }

        public IEnumerable<Participant> GetParticipants => _appDbContext.Participants;

        public int CreateParticipant(Participant participant)
        {
            _appDbContext.Participants.Add(participant);

            _appDbContext.SaveChanges();

            return participant.ParticipantId;

        }

        public Participant GetParticipantByParticipantId(int participantId)
        {
            return _appDbContext.Participants.FirstOrDefault(p => p.ParticipantId == participantId);
        }

        public Participant GetParticipantByStfmUserId(string StfmUserId)
        {


            return _appDbContext.Participants.FirstOrDefault(p => p.StfmUserId == StfmUserId);

        }

        public ParticipantRole GetParticipantRole(int participantRoleId)
        {
            
            return _appDbContext.ParticipantRoles.FirstOrDefault(pr => pr.ParticipantRoleId == participantRoleId );

        }

        public ParticipantRole GetParticipantRole(ParticipantRoleType participantRoleType)
        {
            
            return _appDbContext.ParticipantRoles.FirstOrDefault(pr => pr.ParticipantRoleName == participantRoleType);

        }
    }
}
