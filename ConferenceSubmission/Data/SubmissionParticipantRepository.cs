﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Data
{
    public class SubmissionParticipantRepository : ISubmissionParticipantRepository
    {
        private readonly AppDbContext _appDbContext;

        public SubmissionParticipantRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public IEnumerable<SubmissionParticipant> GetSubmissionParticipants(int submissionId)
        {

            return _appDbContext.SubmissionParticipant
                                .Include(sp => sp.SubmissionParticipantToParticipantRolesLink)
                                .ThenInclude(pr => pr.ParticipantRole)
                                .Include(sp => sp.Participant)
                                .Where(sp => sp.SubmissionId == submissionId);


        }


        public int CreateSubmissionParticipant(SubmissionParticipant submissionParticipant)
        {

            _appDbContext.SubmissionParticipant.Add(submissionParticipant);

            _appDbContext.SaveChanges();

            return submissionParticipant.SubmissionParticipantId;

        }

        public void DeleteSubmissionParticipant(int submissionParticipantId)
        {


            SubmissionParticipant submissionParticipant = _appDbContext.SubmissionParticipant.FirstOrDefault(sp => sp.SubmissionParticipantId == submissionParticipantId);

            _appDbContext.SubmissionParticipant.Remove(submissionParticipant);

            _appDbContext.SaveChanges();


        }

        public void UpdateSubmissionParticipantRoles(int submissionParticipantId, List<int> participantRoleIds)
        {

            SubmissionParticipant submissionParticipant = _appDbContext.SubmissionParticipant.Include(sp => sp.SubmissionParticipantToParticipantRolesLink).FirstOrDefault(sp => sp.SubmissionParticipantId == submissionParticipantId);


            List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRoles = new List<SubmissionParticipantToParticipantRole>();

            foreach (var participantRoleId in participantRoleIds)
            {


                ParticipantRole participantRole = _appDbContext.ParticipantRoles.Find(participantRoleId);

                SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole = new SubmissionParticipantToParticipantRole
                {

                    SubmissionParticipant = submissionParticipant,

                    ParticipantRole = participantRole


                };

                submissionParticipantToParticipantRoles.Add(submissionParticipantToParticipantRole);


            }

            submissionParticipant.SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRoles;

            _appDbContext.SaveChanges();


        }

        public SubmissionParticipant GetSubmissionParticipant(int participantId, int submissionId)
        {


            return _appDbContext.SubmissionParticipant
                                .Include(sp => sp.SubmissionParticipantToParticipantRolesLink)
                                .ThenInclude(pr => pr.ParticipantRole)
                                  .Include(sp => sp.Participant)
                                .Where(sp => sp.SubmissionId == submissionId && sp.ParticipantId == participantId)
                                .FirstOrDefault();



        }

        public void UpdateSubmissionParticipantSortOrder(List<SubmissionParticipant> submissionParticipants, int submissionId)
        {
            //The parameter submissionParticipants is a list of SubmissionParticipants (excluding Submitters-only) that have been reordered by the user.
            //Fetch the Submission (with the parameter submissionId) and update the sort order of its SubmissionParticipants to match that of the
            //submissionParticipants list provided by the user. Note, only those SubmissionParticipants in the parameter submissionParticipants
            //will have their SortOrder property updated.

            // Some LINQ expressions like: submissionParticipants.Any(sps => sps.SubmissionParticipantId == sp.SubmissionParticipantId) cannot be translated 
            // into a SQL query and have to be evaluated on the client. EF Core 2.2  automatically evaluated such queries on the client, but EF Core 3.1 does not.
            // Changing submissionParticipants.Any(sps => sps.SubmissionParticipantId == sp.SubmissionParticipantId) => submissionId && submissionParticipantIds.Contains(sp.SubmissionParticipantId)
            // fixed the issue as this new epxression can be turned into a query that can run on the database.
            // For more info: https://docs.microsoft.com/en-us/ef/core/what-is-new/ef-core-3.x/breaking-changes#linq-queries-are-no-longer-evaluated-on-the-client

            var submissionParticipantIds = submissionParticipants.Select(x => x.SubmissionParticipantId);
            _appDbContext.SubmissionParticipant
                .Where(sp => sp.SubmissionId == submissionId && submissionParticipantIds.Contains(sp.SubmissionParticipantId))
                .ToList()
                .ForEach(sp =>
                {
                    sp.SortOrder = submissionParticipants.FirstOrDefault(spr => spr.SubmissionParticipantId == sp.SubmissionParticipantId).SortOrder;
                });
            _appDbContext.SaveChanges();
        }

        public int AddSubmissionParticipant(SubmissionParticipant submissionParticipant)
        {
            _appDbContext.SubmissionParticipant.Add(submissionParticipant);
            _appDbContext.SaveChanges();
            return submissionParticipant.SubmissionParticipantId;
        }

        public void RemoveSubmissionParticipantToParticipantRole(int submissionParticipantToParticipantRoleId)
        {
            var submissionParticipantToParticipantRoleToRemove
                = _appDbContext.SubmissionParticipantToParticipantRole
                               .FirstOrDefault(s => s.SubmissionParticipantToParticipantRoleId == submissionParticipantToParticipantRoleId);

            if (submissionParticipantToParticipantRoleToRemove != null)
            {
                _appDbContext.SubmissionParticipantToParticipantRole.Remove(submissionParticipantToParticipantRoleToRemove);
                _appDbContext.SaveChanges();
            }
            return;
        }

        public int AddSubmissionParticipantToParticipantRole(int submissionParticipantId, int participantRoleId)
        {
            var newSubmissionParticipantToParticipantRole = new SubmissionParticipantToParticipantRole
            {
                SubmissionParticipantId = submissionParticipantId,
                ParticipantRoleId = participantRoleId
            };
            _appDbContext.SubmissionParticipantToParticipantRole.Add(newSubmissionParticipantToParticipantRole);
            _appDbContext.SaveChanges();
            return newSubmissionParticipantToParticipantRole.SubmissionParticipantToParticipantRoleId;
        }

        public List<int> GetParticipantsMoreThenTwoSubmissions(int conferenceId, int submissionStatusId)
        {
            var reports = _appDbContext.SubmissionParticipant.Where(sp => sp.Submission.Conference.ConferenceId == conferenceId && sp.Submission.SubmissionStatusId == submissionStatusId).GroupBy(sp => new { sp.ParticipantId }).Where(sp => sp.Count() > 2).Select(sp => new { sp.Key, Count = sp.Count() }).ToList();

            List<int> participantIds = new List<int>();

            foreach (var report in reports)
            {
                participantIds.Add(report.Key.ParticipantId);

            }

            return participantIds;
        }

        public List<SubmissionWithParticipantsAddedOrRemoved> GetSubmissionsWithParticipantsAddedOrRemoved(int conferenceId, DateTime asOfDate)
        {
            var results = 
            (from submission in _appDbContext.Submissions
             join auditLogSubmissionParentEntity in _appDbContext.AuditLogParentEntities on submission.SubmissionId equals auditLogSubmissionParentEntity.ParentEntityId
             join auditLog in _appDbContext.AuditLogs on auditLogSubmissionParentEntity.AuditLogId equals auditLog.AuditLogId
             join auditLogParticipantParentEntity in _appDbContext.AuditLogParentEntities on auditLog.AuditLogId equals auditLogParticipantParentEntity.AuditLogId
             join participant in _appDbContext.Participants on auditLogParticipantParentEntity.ParentEntityId equals participant.ParticipantId
             join conferenceSession in _appDbContext.ConferenceSessions on submission.ConferenceSession.ConferenceSessionId equals conferenceSession.ConferenceSessionId
             where (submission.Conference.ConferenceId == conferenceId &&
                      submission.SubmissionStatus.SubmissionStatusName != SubmissionStatusType.DECLINED &&
                      submission.SubmissionStatus.SubmissionStatusName != SubmissionStatusType.WITHDRAWN &&
                      submission.SubmissionStatus.SubmissionStatusName != SubmissionStatusType.CANCELED &&
                      auditLogSubmissionParentEntity.ParentEntityType == EntityType.Submission &&
                      auditLogParticipantParentEntity.ParentEntityType == EntityType.Participant &&
                      auditLog.EntityType == EntityType.SubmissionParticipant &&
                      (auditLog.AuditLogActionType == AuditLogActionType.CREATED || auditLog.AuditLogActionType == AuditLogActionType.DELETED) &&
                      auditLog.TimeStamp >= asOfDate)
             orderby auditLog.TimeStamp
             select new
             {
                 AuditLogId = auditLog.AuditLogId,
                 SubmissionId = submission.SubmissionId,
                 SubmissionTitle = submission.SubmissionTitle,
                 SessionCode = conferenceSession.SessionCode,
                 ParticipantId = participant.ParticipantId,
                 STFMUserId = participant.StfmUserId,
                 TimeStamp = auditLog.TimeStamp,
                 AddedOrRemoved = auditLog.AuditLogActionType == AuditLogActionType.CREATED ? AddedOrRemoved.ADDED : AddedOrRemoved.REMOVED
             })
             .ToList()
             .GroupBy(x => x.SubmissionId)
             .Select(x => new SubmissionWithParticipantsAddedOrRemoved
             {
                 SubmissionId = x.Key,
                 SubmissionTitle = x.FirstOrDefault().SubmissionTitle,
                 SessionCode = x.FirstOrDefault().SessionCode,
                 ParticipantsAddedOrRemoved = x.Select(y => new ParticipantAddedOrRemoved
                 {
                     AddedOrRemoved = y.AddedOrRemoved,
                     DateAddedOrRemoved = y.TimeStamp,
                     DateAddedOrRemovedAsShortDateString = y.TimeStamp.ToShortDateString(),
                     STFMUserId = y.STFMUserId
                 }).ToList()
             }).ToList();

            return results;
        }

    }
}
