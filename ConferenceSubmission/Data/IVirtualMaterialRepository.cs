﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Data
{
    public interface IVirtualMaterialRepository
    {

        /// <summary>
        /// Add new VirtualMaterial record.
        /// </summary>
        /// <param name="virtualMaterial"></param>
        /// <returns>VirtualMaterialId value</returns>
        int AddVirtualMaterial(VirtualMaterial virtualMaterial);

        /// <summary>
        /// Get VirtualMaterial record for provided submissionId.
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns>VirtualMaterial object</returns>
        VirtualMaterial GetVirtualMaterial(int submissionId);

        /// <summary>
        /// Get all the VirtualMaterial objects where the
        /// material type is video for a specific conference.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns></returns>
        IEnumerable<VirtualMaterial> GetVideoFileRecords(int conferenceId);

        /// <summary>
        /// Update the VirtualMaterial record with the state of the 
        /// provided VirtualMaterial object.
        /// </summary>
        /// <param name="virtualMaterial"></param>
        /// <returns>VirtualMaterialId</returns>
        int UpdateVirtualMaterial(VirtualMaterial virtualMaterial);

        /// <summary>
        /// Delete any VirtualMaterial records
        /// associated with the provided submissionId.
        /// </summary>
        /// <param name="submissionId"></param>
        void DeleteVirtualMaterial(int submissionId);

        /// <summary>
        /// Deletes the provided VirtualMaterial object from
        /// the data repository
        /// </summary>
        /// <param name="virtualMaterial"></param>
        void DeleteVirtualMaterial(VirtualMaterial virtualMaterial);

        /// <summary>
        /// Get all VirtualMaterial objects associated
        /// with the provided submissionId
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns>Collection of VirtualMaterial objects</returns>
        IEnumerable<VirtualMaterial> GetVirtualMaterials(int submissionId);

    }
}
