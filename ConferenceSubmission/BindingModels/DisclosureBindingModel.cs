﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.BindingModels
{
    public class DisclosureBindingModel
    {

        public int DislosureId { get; set; }

        /// <summary>
        /// If the submission is for CPQI and need a CV file for CME
        /// then this value needs to be set to true.
        /// When true the disclosure form will include upload CV File form field.
        /// </summary>
        public bool isSubmissionForCPQI { get; set; }

        /// <summary>
        /// This value is optional and will only
        /// be present when user is creating a new
        /// disclosure or updating an existing 
        /// disclosure when creating a new submission.
        /// </summary>
        /// <value>The submission identifier.</value>
        public int SubmissionId { get; set; }

        [Required]
        public string StfmUserId { get; set; }

        public IFormFile CvFile { get; set; }

        public List<DisclosureQuestion> Part1 { get; set; } = new List<DisclosureQuestion> {

            new DisclosureQuestion {

                DisclosureQuestionValue = "A",

                DisclosureQuestionText = "A. Neither I nor my immediate family has a financial relationship or interest in any commercial interest producing, marketing, re-selling, or distributing health care goods or services consumed by, or used on, patients"

            },

            new DisclosureQuestion {

                DisclosureQuestionValue = "B",

                DisclosureQuestionText = "B. I have or a member of my immediate family have a financial relationship or interest in any commercial interest producing, marketing, re-selling, or distributing health care goods or services consumed by, or used on, patients"


            }


        };

        [Required(ErrorMessage = "You must check either A or B")]
        public string Part1Answer { get; set; }



        public string Part2Answer { get; set; }


        public List<DisclosureQuestion> Part3 { get; set; } = new List<DisclosureQuestion> {

            new DisclosureQuestion {

                DisclosureQuestionValue = "A",

                DisclosureQuestionText = "A. The content of my material(s)/presentation(s) in this CME activity WILL NOT include discussion of unapproved or investigational uses of products or devices."

            },

            new DisclosureQuestion {

                DisclosureQuestionValue = "B",

                DisclosureQuestionText = "B. The content of my material(s)/presentation(s) in this CME activity WILL include discussion of unapproved or investigational uses of products or devices as indicated below."


            }


        };


        [Required(ErrorMessage = "You must check either A or B")]
        public string Part3Answer { get; set; }


        public string Part4Answer { get; set; }


        public List<DisclosureQuestion> Part5 { get; set; } = new List<DisclosureQuestion> {

            new DisclosureQuestion {

                DisclosureQuestionValue = "A",

                DisclosureQuestionText = "A. Neither I nor anyone in my immediate family is a member of a speakers' bureau for a proprietary entity producing health care goods or services."

            },

            new DisclosureQuestion {

                DisclosureQuestionValue = "B",

                DisclosureQuestionText = "B. I am or a member of my immediate family is a member of a speakers' bureau for a proprietary entity producing health care goods or services."


            }


        };


        [Required(ErrorMessage = "You must check either A or B")]
        public string Part5Answer { get; set; }


        public string Part6Answer { get; set; }

        [Required(ErrorMessage = "You must type your full name to acknowledge the disclosure policy.")]
        public string FullName { get; set; }


        public override string ToString()
        {

            return "Disclosure ID: " + DislosureId + " | Part 1 Answer: " + Part1Answer + " | Part2Answer: " + Part2Answer
                + " | Part3Answer: " + Part3Answer + " | Part4Answer: " + Part4Answer
                + " | Part5Answer: " + Part5Answer + " | Part6Answer: " + Part6Answer
                + " | Full Name: " + FullName;
        }

    }
}
