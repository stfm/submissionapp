﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public enum PresentationStatusType
    {

        [Description("No Action By Presenter")]
        NO_ACTION,

        [Description("Pending Action By Presenter")]
        PENDING,

        [Description("Presenter Needs To Provide Presentation Files")]
        NEED_PRESENTATION_MATERIAL,

        [Description("Presenter Declined To Present")]
        DECLINED


    }

    
}
