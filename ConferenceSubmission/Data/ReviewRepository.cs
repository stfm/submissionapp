﻿using System;
using ConferenceSubmission.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;


namespace ConferenceSubmission.Data
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly AppDbContext _appDbContext;

        public ReviewRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public int AddReview(Review review)
        {
            _appDbContext.Reviews.Add(review);

            _appDbContext.SaveChanges();

            return review.ReviewId;
        }

        public IEnumerable<Review> GetCompletedReviews(int submissionId)
        {

            return _appDbContext.Reviews.Where(r => r.SubmissionId.Equals(submissionId) && r.ReviewStatusId == 3).Include(r => r.ReviewStatus).Include(r => r.Reviewer).Include(r => r.ReviewFormFields).ThenInclude(s => s.FormField)
                                .ThenInclude(ff => ff.FormFieldProperties);


        }

        public IEnumerable<Review> GetCompletedReviewsForCategory(int categoryId)
        {
            return _appDbContext.Reviews.Where(r => r.Submission.SubmissionCategoryId.Equals(categoryId) && r.ReviewStatusId == 3).Include(r => r.ReviewStatus).Include(r => r.Submission).Include(s => s.Submission.SubmissionCategory);
        }

        public Review GetReview(int reviewId)
        {

            return _appDbContext.Reviews.Include(r => r.Submission).Include(r => r.ReviewStatus)
.Include(r => r.Reviewer)
.Include(r => r.ReviewFormFields).ThenInclude(s => s.FormField)
                                .ThenInclude(ff => ff.FormFieldProperties)
.FirstOrDefault(r => r.ReviewId == reviewId);

        }

        public Review GetReview(int submissionId, int reviewerId)
        {

            return _appDbContext.Reviews.Include(r => r.ReviewStatus)

                                .FirstOrDefault(r => r.SubmissionId == submissionId && r.ReviewerId == reviewerId);
        }

        public bool GetReviewCompleted(int reviewerId, int submissionId)
        {

            var count = _appDbContext.Reviews
            .Where(r => r.ReviewerId == reviewerId && r.SubmissionId == submissionId && r.ReviewStatus.ReviewStatusType == ReviewStatusType.COMPLETE).ToList().Count;

            if (count > 0)
            {
                return true;
            }

            return false;


        }

        public void UpdateReview(Review review)
        {

            review.ReviewDateTime = DateTime.Now;

            _appDbContext.Update<Review>(review);

            _appDbContext.SaveChanges();


        }

        public IEnumerable<Submission> GetSubmissionsNotAssignedToReviewer(int conferenceId)
        {

            string query = @"select s.SubmissionId, s.ConferenceId, s.AcceptedCategoryId, s.SubmissionTitle, s.SubmissionStatusId, s.SubmissionAbstract, s.SubmissionCategoryId, s.SubmissionCreatedDateTime, s.SubmissionLastUpdatedDateTime, s.SubmissionCompleted, s.SubmissionAdminEdited, s.ParticipantOrderModified from submissions s
where s.submissionstatusid = 1 and s.conferenceid = {0}
and s.submissionid NOT IN
(select sr.submissionid from submissionreviewer sr)";

            return _appDbContext.Submissions.FromSqlRaw(query, conferenceId).Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.SubmissionPayment).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole); ;


        }

        public IEnumerable<Submission> GetSubmissionsAssignedToReviewer(int conferenceId)
        {

            string query = @"select s.SubmissionId, s.ConferenceId, s.AcceptedCategoryId, s.SubmissionTitle, s.SubmissionStatusId, s.SubmissionAbstract, s.SubmissionCategoryId, s.SubmissionCreatedDateTime, s.SubmissionLastUpdatedDateTime, s.SubmissionCompleted, s.SubmissionAdminEdited, s.ParticipantOrderModified from submissions s
where s.submissionstatusid = 1 and s.conferenceid = {0}
and s.submissionid IN
(select sr.submissionid from submissionreviewer sr)";

            return _appDbContext.Submissions.FromSqlRaw(query, conferenceId).Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.SubmissionPayment).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole); ;


        }

        public IEnumerable<Submission> GetSubmissionsNoReviewsCompleted(int conferenceId)
        {
            string query = @"select s.SubmissionId, s.ConferenceId, s.AcceptedCategoryId, s.SubmissionTitle, s.SubmissionStatusId, s.SubmissionAbstract, s.SubmissionCategoryId, s.SubmissionCreatedDateTime, s.SubmissionLastUpdatedDateTime, s.SubmissionCompleted, s.SubmissionAdminEdited, s.ParticipantOrderModified from submissions s
inner join submissionreviewer sr on s.SubmissionId = sr.SubmissionId where s.conferenceid = {0}
and s.submissionid NOT IN
(select r2.submissionid from reviews r2 where r2.reviewstatusid = 3)";

            return _appDbContext.Submissions.FromSqlRaw(query, conferenceId).Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.SubmissionPayment).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole); ;
        }
    }
}
