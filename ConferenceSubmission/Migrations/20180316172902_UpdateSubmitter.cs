﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class UpdateSubmitter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionCategories_Conferences_ConferenceId",
                table: "SubmissionCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionPresenters_PresenterRoles_PresenterRoleId",
                table: "SubmissionPresenters");

            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Conferences_ConferenceId",
                table: "Submissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_SubmissionStatuses_SubmissionStatusId",
                table: "Submissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Submitters_Submissions_SubmissionId",
                table: "Submitters");

            migrationBuilder.DropIndex(
                name: "IX_Submitters_SubmissionId",
                table: "Submitters");

            migrationBuilder.DropColumn(
                name: "SubmissionId",
                table: "Submitters");

            migrationBuilder.AlterColumn<int>(
                name: "SubmissionStatusId",
                table: "Submissions",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ConferenceId",
                table: "Submissions",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubmitterId",
                table: "Submissions",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PresenterRoleId",
                table: "SubmissionPresenters",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ConferenceId",
                table: "SubmissionCategories",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SessionTrack",
                table: "ConferenceSessions",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SessionLocation",
                table: "ConferenceSessions",
                maxLength: 250,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_Submissions_SubmitterId",
                table: "Submissions",
                column: "SubmitterId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionCategories_Conferences_ConferenceId",
                table: "SubmissionCategories",
                column: "ConferenceId",
                principalTable: "Conferences",
                principalColumn: "ConferenceId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionPresenters_PresenterRoles_PresenterRoleId",
                table: "SubmissionPresenters",
                column: "PresenterRoleId",
                principalTable: "PresenterRoles",
                principalColumn: "PresenterRoleId",
                onDelete: ReferentialAction.Cascade);


            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_SubmissionStatuses_SubmissionStatusId",
                table: "Submissions",
                column: "SubmissionStatusId",
                principalTable: "SubmissionStatuses",
                principalColumn: "SubmissionStatusId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Submitters_SubmitterId",
                table: "Submissions",
                column: "SubmitterId",
                principalTable: "Submitters",
                principalColumn: "SubmitterId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionCategories_Conferences_ConferenceId",
                table: "SubmissionCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_SubmissionPresenters_PresenterRoles_PresenterRoleId",
                table: "SubmissionPresenters");

            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Conferences_ConferenceId",
                table: "Submissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_SubmissionStatuses_SubmissionStatusId",
                table: "Submissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Submitters_SubmitterId",
                table: "Submissions");

            migrationBuilder.DropIndex(
                name: "IX_Submissions_SubmitterId",
                table: "Submissions");

            migrationBuilder.DropColumn(
                name: "SubmitterId",
                table: "Submissions");

            migrationBuilder.AddColumn<int>(
                name: "SubmissionId",
                table: "Submitters",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "SubmissionStatusId",
                table: "Submissions",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ConferenceId",
                table: "Submissions",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PresenterRoleId",
                table: "SubmissionPresenters",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ConferenceId",
                table: "SubmissionCategories",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "SessionTrack",
                table: "ConferenceSessions",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SessionLocation",
                table: "ConferenceSessions",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 250);

            migrationBuilder.CreateIndex(
                name: "IX_Submitters_SubmissionId",
                table: "Submitters",
                column: "SubmissionId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionCategories_Conferences_ConferenceId",
                table: "SubmissionCategories",
                column: "ConferenceId",
                principalTable: "Conferences",
                principalColumn: "ConferenceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubmissionPresenters_PresenterRoles_PresenterRoleId",
                table: "SubmissionPresenters",
                column: "PresenterRoleId",
                principalTable: "PresenterRoles",
                principalColumn: "PresenterRoleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Conferences_ConferenceId",
                table: "Submissions",
                column: "ConferenceId",
                principalTable: "Conferences",
                principalColumn: "ConferenceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_SubmissionStatuses_SubmissionStatusId",
                table: "Submissions",
                column: "SubmissionStatusId",
                principalTable: "SubmissionStatuses",
                principalColumn: "SubmissionStatusId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Submitters_Submissions_SubmissionId",
                table: "Submitters",
                column: "SubmissionId",
                principalTable: "Submissions",
                principalColumn: "SubmissionId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
