﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;


namespace ConferenceSubmission.Models
{
    public enum PresentationMethodType
    {

        [Description("Live In-Person")]
        LIVE_IN_PERSON,

        [Description("Live Online")]
        LIVE_ONLINE,

        [Description("Recorded Online")]
        RECORDED_ONLINE

    }

   


}
