﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class SubmissionCategoryRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public SubmissionCategoryRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }

        [Fact]
        public void GetSubmissionCategoriesByConferenceIdTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                SubmissionCategoryRepository submissionCategoryRepository = new SubmissionCategoryRepository(context);

                IEnumerable<SubmissionCategory> submissionCategories = submissionCategoryRepository.GetSubmissionCategories(100);

                foreach(SubmissionCategory submissionCategory in submissionCategories) {

                    Output.WriteLine("Submission category name is " + submissionCategory.SubmissionCategoryName);

                }

                Assert.Equal(3, submissionCategories.Count());

            }
        }



        [Fact]
        public void GetSubmissionCategoryTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                SubmissionCategoryRepository submissionCategoryRepository = new SubmissionCategoryRepository(context);

                SubmissionCategory submissionCategory = submissionCategoryRepository.GetSubmissionCategory(1000);

                Output.WriteLine("Submission category name is " + submissionCategory.SubmissionCategoryName);

                Output.WriteLine("Submission category payment requirement is " + submissionCategory.SubmissionPaymentRequirement);

                Output.WriteLine("Number of Submission category presentation methods " + submissionCategory.SubmissionCategoryToPresentationMethodsLink.Count);

                foreach (SubmissionCategoryToPresentationMethod submissionCategoryToPresentationMethod in submissionCategory.SubmissionCategoryToPresentationMethodsLink)
                {

                    Output.WriteLine("Submission category presentation id is " + submissionCategoryToPresentationMethod.PresentationMethodId);

                    Output.WriteLine("Submission category presentation method is " + submissionCategoryToPresentationMethod.PresentationMethod.PresentationMethodName);

                }

                Assert.Equal("Lecture MSE19", submissionCategory.SubmissionCategoryName);

                submissionCategory = submissionCategoryRepository.GetSubmissionCategory(1001);

                Output.WriteLine("Submission category name is " + submissionCategory.SubmissionCategoryName);

                Output.WriteLine("Submission category payment requirement is " + submissionCategory.SubmissionPaymentRequirement);

                Output.WriteLine("Number of Submission category presentation methods " + submissionCategory.SubmissionCategoryToPresentationMethodsLink.Count);

            }
        }



        [Fact]
        public void GetSubmissionCategoriesWithFormFieldsByConferenceTest()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            var conferneceId = 100; 

            using (var context = new AppDbContext(options))
            {
                SubmissionCategoryRepository submissionCategoryRepository = new SubmissionCategoryRepository(context);

                //Act
                var result = submissionCategoryRepository.GetSubmissionCategoriesWithFormFieldsByConference(conferneceId);

                //Assert
                Assert.Equal(2, result.Count);
                Assert.Single(result.Where(x => x.SubmissionCategoryName == "Lecture MSE19"));
                Assert.Single(result.Where(x => x.SubmissionCategoryName == "Seminar MSE19"));
            }
        }


        [Fact]
        public void GetSubmissionCategoriesWithFormFieldsByConferenceTest_VerifyCategoriesWithOnlyReviewFormFieldsNotReturned()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            var conferneceId = 101;

            using (var context = new AppDbContext(options))
            {
                SubmissionCategoryRepository submissionCategoryRepository = new SubmissionCategoryRepository(context);

                //Act
                var result = submissionCategoryRepository.GetSubmissionCategoriesWithFormFieldsByConference(conferneceId);

                //Assert
                Assert.Empty(result);
            }
        }

        [Fact]
        public void GetSubmissionCategoriesWithFormFieldsByConferenceTest_VerifyCategoriesWithNoFormFieldsNotReturned()
        {
            //Arrange
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            var conferneceId = 102;

            using (var context = new AppDbContext(options))
            {
                SubmissionCategoryRepository submissionCategoryRepository = new SubmissionCategoryRepository(context);

                //Act
                var result = submissionCategoryRepository.GetSubmissionCategoriesWithFormFieldsByConference(conferneceId);

                //Assert
                Assert.Empty(result);
            }
        }


        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" },
                    new ConferenceType { ConferenceTypeId = 3, ConferenceTypeName = "Practice and Quality Improvement" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    },

                    new Conference
                    {
                        ConferenceId = 102,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("December 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Conference on Practice and Quality Improvement",
                        ConferenceShortName = "CPQI19",
                        ConferenceStartDate = DateTime.Parse("December 11, 2019"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19", SubmissionPaymentRequirement = SubmissionPaymentRequirement.NON_MEMBER_ONLY },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19", SubmissionPaymentRequirement = SubmissionPaymentRequirement.NON_MEMBER_ONLY },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19", SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19", SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19",  SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19", SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE },
                    new SubmissionCategory { ConferenceId = 102, SubmissionCategoryId = 1006, SubmissionCategoryName = "Poster CPQI19", SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE },
                    new SubmissionCategory { ConferenceId = 102, SubmissionCategoryId = 1007, SubmissionCategoryName = "Lecture CPQI19", SubmissionPaymentRequirement = SubmissionPaymentRequirement.NONE }


                );

                context.SaveChanges();

                context.PresentationMethod.AddRange(

                   new PresentationMethod { PresentationMethodId = 1, PresentationMethodName = PresentationMethodType.LIVE_IN_PERSON },
                    new PresentationMethod { PresentationMethodId = 2, PresentationMethodName = PresentationMethodType.LIVE_ONLINE },
                     new PresentationMethod { PresentationMethodId = 3, PresentationMethodName = PresentationMethodType.RECORDED_ONLINE }



                   );

                context.SaveChanges();

                context.SubmissionCategoryToPresentationMethod.AddRange(

                    new SubmissionCategoryToPresentationMethod { SubmissionCategoryId = 1000, PresentationMethodId = 1 },
                    new SubmissionCategoryToPresentationMethod { SubmissionCategoryId = 1000, PresentationMethodId = 3 }



                    );

                context.SaveChanges();

                context.FormFields.AddRange(
                    new FormField
                    {
                        FormFieldId = 1, 
                        SubmissionCategoryId = 1000,
                        FormFieldRole = "proposal",
                        FormFieldType = "textarea"
                    },
                    new FormField
                    {
                        FormFieldId = 2,
                        SubmissionCategoryId = 1001,
                        FormFieldRole = "proposal",
                        FormFieldType = "select"
                    },
                    new FormField
                    {
                        FormFieldId = 5,
                        SubmissionCategoryId = 1001,
                        FormFieldRole = "review",
                        FormFieldType = "textarea"
                    },
                    new FormField
                    {
                        FormFieldId = 3,
                        SubmissionCategoryId = 1005,
                        FormFieldRole = "review",
                        FormFieldType = "textarea"
                    },
                    new FormField
                    {
                        FormFieldId = 4,
                        SubmissionCategoryId = 1005,
                        FormFieldRole = "review",
                        FormFieldType = "textarea"
                    }
                );
                context.SaveChanges();
            }

            return options;
        }



    }
}
