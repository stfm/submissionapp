﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionsDetailedReviewsPrintViewModel
    {
        public string ConferenceLongName { get; set; }

        public string SubmissionCategoryName { get; set; }

        public List<SubmissionDetailedReview> SubmissionDetailedReviews { get; set; }

        public string ImagePath { get; set; }

        //Public helper methods
        public string GetFileName()
        {
            //Regex that matches characters that are invalid in windows file names
            var illegalInFileName = new Regex(@"[\\/:*?""<>|]");
            return $"{illegalInFileName.Replace(SubmissionCategoryName.Substring(0, Math.Min(50, SubmissionCategoryName.Length)), "").Replace(' ', '_')}_Detailed_Reviews.pdf";
        }

        //Public helper methods
        public string GetSingleSubmissionFileName()
        {
            //Regex that matches characters that are invalid in windows file names
            var illegalInFileName = new Regex(@"[\\/:*?""<>|]");
            return $"{illegalInFileName.Replace(SubmissionCategoryName.Substring(0, Math.Min(50, SubmissionCategoryName.Length)), "").Replace(' ', '_')}_" + SubmissionDetailedReviews[0].SubmissionId + "_Detailed_Reviews.pdf";
        }

        public SubmissionsDetailedReviewsPrintViewModel()
        {

            SubmissionDetailedReviews = new List<SubmissionDetailedReview>();

        }
    }
}
