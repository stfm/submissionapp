﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public class GetAuditLogsService : IGetAuditLogsService
    {
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly ISalesforceAPIService _salesforceAPIService;

        public GetAuditLogsService(IAuditLogRepository auditLogRepository, ISalesforceAPIService salesforceAPIService)
        {
            _auditLogRepository = auditLogRepository;
            _salesforceAPIService = salesforceAPIService;
        }

        public List<AuditLog> GetByEntityId(int entityId, EntityType entityType)
        {
            var result = _auditLogRepository.GetByEntityId(entityId, entityType);
            if (result.Count > 0)
            {

                var stfmUserIds = result.Select(r => r.ChangedBy).Distinct().ToList();
                var stfmUsers = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result;
                result.ForEach(r => r.ChangedBy = stfmUsers.FirstOrDefault(u => u.STFMUserId == (r.ChangedBy ?? ""))?.GetFullNamePretty() ?? r.ChangedBy);
            }

            result.Where(r => r.EntityType != entityType && r.NewValue == "ENTITY CREATED")
                .ToList()
                .ForEach(r => r.NewValue = $"ENTITY CREATED ({r.EntityType.ToString()})");

            result.Where(r => r.EntityType != entityType && r.NewValue == "ENTITY DELETED")
                .ToList()
                .ForEach(r => r.NewValue = $"ENTITY DELETED ({r.EntityType.ToString()})");

            return result;
        }
    }

    public interface IGetAuditLogsService
    {
        List<AuditLog> GetByEntityId(int entityId, EntityType entityType);
    }
}
