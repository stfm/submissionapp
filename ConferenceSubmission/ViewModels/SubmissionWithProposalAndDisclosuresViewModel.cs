﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionWithProposalAndDisclosuresViewModel 
    {

        public SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel { get; set; }

        public List<Disclosure> disclosures { get; set; }

        public SubmissionWithProposalAndDisclosuresViewModel()
        {

            disclosures = new List<Disclosure>();

        }


    }
}
