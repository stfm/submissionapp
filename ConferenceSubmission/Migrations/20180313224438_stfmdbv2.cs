﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ConferenceSubmission.Migrations
{
    public partial class stfmdbv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConferenceTypes",
                columns: table => new
                {
                    ConferenceTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConferenceTypeName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConferenceTypes", x => x.ConferenceTypeId);
                });

            migrationBuilder.CreateTable(
                name: "PresenterRoles",
                columns: table => new
                {
                    PresenterRoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PresenterRoleName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PresenterRoles", x => x.PresenterRoleId);
                });

            migrationBuilder.CreateTable(
                name: "Presenters",
                columns: table => new
                {
                    PresenterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StfmUserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Presenters", x => x.PresenterId);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionStatuses",
                columns: table => new
                {
                    SubmissionStatusId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SubmissionStatusName = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionStatuses", x => x.SubmissionStatusId);
                });

            migrationBuilder.CreateTable(
                name: "Conferences",
                columns: table => new
                {
                    ConferenceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConferenceDeleted = table.Column<bool>(nullable: false),
                    ConferenceEndDate = table.Column<DateTime>(nullable: false),
                    ConferenceInactive = table.Column<bool>(nullable: false),
                    ConferenceLongName = table.Column<string>(maxLength: 250, nullable: false),
                    ConferenceShortName = table.Column<string>(maxLength: 20, nullable: false),
                    ConferenceStartDate = table.Column<DateTime>(nullable: false),
                    ConferenceTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conferences", x => x.ConferenceId);
                    table.ForeignKey(
                        name: "FK_Conferences_ConferenceTypes_ConferenceTypeId",
                        column: x => x.ConferenceTypeId,
                        principalTable: "ConferenceTypes",
                        principalColumn: "ConferenceTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionCategories",
                columns: table => new
                {
                    SubmissionCategoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConferenceId = table.Column<int>(nullable: true),
                    SubmissionCategoryName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionCategories", x => x.SubmissionCategoryId);
                    table.ForeignKey(
                        name: "FK_SubmissionCategories_Conferences_ConferenceId",
                        column: x => x.ConferenceId,
                        principalTable: "Conferences",
                        principalColumn: "ConferenceId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Submissions",
                columns: table => new
                {
                    SubmissionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AcceptedCategoryId = table.Column<int>(nullable: false),
                    ConferenceId = table.Column<int>(nullable: true),
                    SubmissionAbstract = table.Column<string>(nullable: false),
                    SubmissionCategoryId = table.Column<int>(nullable: false),
                    SubmissionCreatedDateTime = table.Column<DateTime>(nullable: false),
                    SubmissionLastUpdatedDateTime = table.Column<DateTime>(nullable: false),
                    SubmissionStatusId = table.Column<int>(nullable: true),
                    SubmissionTitle = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Submissions", x => x.SubmissionId);
                    table.ForeignKey(
                        name: "FK_Submissions_Conferences_ConferenceId",
                        column: x => x.ConferenceId,
                        principalTable: "Conferences",
                        principalColumn: "ConferenceId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Submissions_SubmissionCategories_SubmissionCategoryId",
                        column: x => x.SubmissionCategoryId,
                        principalTable: "SubmissionCategories",
                        principalColumn: "SubmissionCategoryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Submissions_SubmissionStatuses_SubmissionStatusId",
                        column: x => x.SubmissionStatusId,
                        principalTable: "SubmissionStatuses",
                        principalColumn: "SubmissionStatusId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConferenceSessions",
                columns: table => new
                {
                    ConferenceSessionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SessionCode = table.Column<string>(maxLength: 10, nullable: false),
                    SessionDateTime = table.Column<DateTime>(nullable: false),
                    SessionLocation = table.Column<string>(nullable: false),
                    SessionTrack = table.Column<string>(nullable: true),
                    SubmissionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConferenceSessions", x => x.ConferenceSessionId);
                    table.ForeignKey(
                        name: "FK_ConferenceSessions_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionPayments",
                columns: table => new
                {
                    SubmissionPaymentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PaymentAmount = table.Column<int>(nullable: false),
                    PaymentDateTime = table.Column<DateTime>(nullable: false),
                    PaymentTransactionId = table.Column<string>(maxLength: 250, nullable: true),
                    SubmissionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionPayments", x => x.SubmissionPaymentId);
                    table.ForeignKey(
                        name: "FK_SubmissionPayments_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubmissionPresenters",
                columns: table => new
                {
                    SubmissionId = table.Column<int>(nullable: false),
                    PresenterId = table.Column<int>(nullable: false),
                    PresenterRoleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmissionPresenters", x => new { x.SubmissionId, x.PresenterId });
                    table.ForeignKey(
                        name: "FK_SubmissionPresenters_Presenters_PresenterId",
                        column: x => x.PresenterId,
                        principalTable: "Presenters",
                        principalColumn: "PresenterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubmissionPresenters_PresenterRoles_PresenterRoleId",
                        column: x => x.PresenterRoleId,
                        principalTable: "PresenterRoles",
                        principalColumn: "PresenterRoleId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubmissionPresenters_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Submitters",
                columns: table => new
                {
                    SubmitterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StfmUserId = table.Column<int>(nullable: false),
                    SubmissionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Submitters", x => x.SubmitterId);
                    table.ForeignKey(
                        name: "FK_Submitters_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Conferences_ConferenceTypeId",
                table: "Conferences",
                column: "ConferenceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ConferenceSessions_SubmissionId",
                table: "ConferenceSessions",
                column: "SubmissionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionCategories_ConferenceId",
                table: "SubmissionCategories",
                column: "ConferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionPayments_SubmissionId",
                table: "SubmissionPayments",
                column: "SubmissionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionPresenters_PresenterId",
                table: "SubmissionPresenters",
                column: "PresenterId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmissionPresenters_PresenterRoleId",
                table: "SubmissionPresenters",
                column: "PresenterRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Submissions_ConferenceId",
                table: "Submissions",
                column: "ConferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_Submissions_SubmissionCategoryId",
                table: "Submissions",
                column: "SubmissionCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Submissions_SubmissionStatusId",
                table: "Submissions",
                column: "SubmissionStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Submitters_SubmissionId",
                table: "Submitters",
                column: "SubmissionId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConferenceSessions");

            migrationBuilder.DropTable(
                name: "SubmissionPayments");

            migrationBuilder.DropTable(
                name: "SubmissionPresenters");

            migrationBuilder.DropTable(
                name: "Submitters");

            migrationBuilder.DropTable(
                name: "Presenters");

            migrationBuilder.DropTable(
                name: "PresenterRoles");

            migrationBuilder.DropTable(
                name: "Submissions");

            migrationBuilder.DropTable(
                name: "SubmissionCategories");

            migrationBuilder.DropTable(
                name: "SubmissionStatuses");

            migrationBuilder.DropTable(
                name: "Conferences");

            migrationBuilder.DropTable(
                name: "ConferenceTypes");
        }
    }
}
