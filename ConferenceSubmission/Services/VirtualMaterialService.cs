﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public class VirtualMaterialService : IVirtualMaterialService
    {

        private readonly IVirtualMaterialRepository _virtualMaterialRepository;


        public VirtualMaterialService(IVirtualMaterialRepository virtualMaterialRepository)
        {

            _virtualMaterialRepository = virtualMaterialRepository;



        }

        public int AddVirtualMaterial(VirtualMaterial virtualMaterial)
        {
            return _virtualMaterialRepository.AddVirtualMaterial(virtualMaterial);
        }

        public void DeleteVirtualMaterial(int submissionId)
        {
            _virtualMaterialRepository.DeleteVirtualMaterial(submissionId);
        }

        public void DeleteVirtualMaterial(VirtualMaterial virtualMaterial)
        {
            _virtualMaterialRepository.DeleteVirtualMaterial(virtualMaterial);
        }

        public IEnumerable<VirtualMaterial> GetVideoFileRecords(int conferenceId)
        {
            return _virtualMaterialRepository.GetVideoFileRecords(conferenceId);
        }

        public VirtualMaterial GetVirtualMaterial(int submissionId)
        {
            return _virtualMaterialRepository.GetVirtualMaterial(submissionId);
        }

        public IEnumerable<VirtualMaterial> GetVirtualMaterials(int submissionId)
        {
            return _virtualMaterialRepository.GetVirtualMaterials(submissionId);
        }

        public int UpdateVirtualMaterial(VirtualMaterial virtualMaterial)
        {
            return _virtualMaterialRepository.UpdateVirtualMaterial(virtualMaterial);
        }
    }
}
