﻿using System;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public class ReviewStatusService : IReviewStatusService
    {

        private readonly IReviewStatusRepository _reviewStatusRepository;

        private readonly IReviewRepository _reviewRepository;

        public ReviewStatusService(IReviewStatusRepository reviewStatusRepository, 
                                   IReviewRepository reviewRepository)
        {
            _reviewStatusRepository = reviewStatusRepository;

            _reviewRepository = reviewRepository;

        }

        public int GetReviewIdForReview(int submissionId, int reviewerId)
        {

            Review review = _reviewRepository.GetReview(submissionId, reviewerId);

            if (review == null)
            {

                //No review yet exists for this review assignment to this reviewer
                //so reviewId should be 0

                return 0;


            }
            else
            {

                //There is a review for this review assignment to this reviewer
                //so just return that review's ID number

                return review.ReviewId;

            }
        }

        public ReviewStatus GetReviewStatusByReviewStatusType(ReviewStatusType reviewStatusType)
        {
            return _reviewStatusRepository.GetReviewStatusByReviewStatusType(reviewStatusType);
        }

       public ReviewStatus GetReviewStatusForReview(int submissionId, int reviewerId) {


            Review review = _reviewRepository.GetReview(submissionId, reviewerId);

            if ( review == null) {

                //No review yet exists for this review assignment to this reviewer
                //so ReviewStatus is NOT_STARTED

                return GetReviewStatusByReviewStatusType(ReviewStatusType.NOT_STARTED);


            } else {

                //There is a review for this review assignment to this reviewer
                //so just return that review's ReviewStatus

                return review.ReviewStatus;

            }

        }
    }
}
