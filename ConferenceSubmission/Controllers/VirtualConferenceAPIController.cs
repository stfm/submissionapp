﻿using ConferenceSubmission.DTOs.PosterHall;
using ConferenceSubmission.DTOs.VirtualConference;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PresenterToNotify = ConferenceSubmission.DTOs.VirtualConference.PresenterToNotify;

namespace ConferenceSubmission.Controllers
{
    [Route("api/virtualconference")]
    public class VirtualConferenceAPIController : Controller
    {
        private readonly ISubmissionService _submissionService;

        private readonly ISalesforceAPIService _salesforceAPIService;

        public VirtualConferenceAPIController(ISubmissionService submissionService, ISalesforceAPIService salesforceAPIService)
        {
            _submissionService = submissionService;
            _salesforceAPIService = salesforceAPIService;   
        }

        [EnableCors]
        [HttpGet("conference/{conferenceId:int}")]
        public IActionResult GetVirtualConferenceSubmissions(int conferenceId)
        {
            var submissions = _submissionService.GetVirtualConferenceSubmissions(conferenceId);

            List<VirtualPresentation> virtualPresentations = ConvertSubmissionsToVirtualPresentations(submissions);

            return Ok(virtualPresentations);
        }

        [EnableCors]
        [HttpGet("stfmuserid/{stfmUserId}")]
        public IActionResult GetPerson(string stfmUserId)
        {
            User user = _salesforceAPIService.GetUserInfo(stfmUserId).Result;

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        private List<VirtualPresentation> ConvertSubmissionsToVirtualPresentations(List<Submission> submissions)
        {
            var virtualPresentations = submissions
                .Select(submission => new VirtualPresentation
                {
                    SubmissionId = submission.SubmissionId,
                    Title = submission.SubmissionTitle,
                    Abstract = submission.SubmissionAbstract,
                    Presenters = getPresenters(submission.ParticipantLink),
                    PresenterToNotify = getPresenterToNotify(submission.ParticipantLink),
                    VideoLink = getVideoLink(submission),
                    Handout = getHandout(submission),
                    PosterImageLink = getPosterImageLink(submission),
                    SubmissionCategory = submission.AcceptedCategory.SubmissionCategoryName,
                    VirtualConferenceSessionID = getVirtualConferenceSessionId(submission),
                    KeyWords = GetSubmissionKeyWords(submission),
                    PresentationType = GetPresentationType(submission),
                    SessionCode = getSessionCode(submission)


                }).ToList();

            return virtualPresentations;
        }


        private string getPresenters(List<SubmissionParticipant> submissionParticipants)
        {
            string presenters = "";

            foreach (var submissionParticipant in submissionParticipants)
            {

                string fullName = submissionParticipant.Participant.ParticipantUser.GetFullNamePretty();

                presenters += fullName + "; ";

            }

            presenters = presenters.TrimEnd(new char[] { ' ', ';' });
            
            return presenters;


        }

        private PresenterToNotify getPresenterToNotify(List<SubmissionParticipant> submissionParticipants)
        {
            PresenterToNotify presenterToNotify = new PresenterToNotify();

            //There should be a lead presenter - but if not use the submitter - there will always be a submitter
            SubmissionParticipant submissionParticipant = submissionParticipants.FirstOrDefault(sp => sp.SubmissionParticipantToParticipantRolesLink.FirstOrDefault(spr => spr.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER) != null);

            if (submissionParticipant == null)
            {
                submissionParticipant = submissionParticipants.FirstOrDefault(sp => sp.SubmissionParticipantToParticipantRolesLink.FirstOrDefault(spr => spr.ParticipantRole.ParticipantRoleName == ParticipantRoleType.SUBMITTER) != null);

            }

            if (submissionParticipant != null)
            {
                presenterToNotify.FullName = submissionParticipant.Participant.ParticipantUser.GetFullNamePretty();
                presenterToNotify.Email = submissionParticipant.Participant.ParticipantUser.Email;

            }

            return presenterToNotify;
        }


        /// <summary>
        /// Get the link to either the video hosted on vimeo (if not a poster)
        /// or the YouTube embedded link (if a poster).
        /// </summary>
        /// <param name="submission"></param>
        /// <returns>video link</returns>
        private string getVideoLink(Submission submission)
        {

            string videoLink = null;

            var virtualMaterial = submission.VirtualMaterials.FirstOrDefault(vm => vm.MaterialType == "video");

            if (virtualMaterial != null)
            {
                videoLink = virtualMaterial.VideoUrl;

            }

            //OK to do a second check since a submission will either have a virtual material record for video
            //OR a virtual material record for link BUT not both
            virtualMaterial = submission.VirtualMaterials.FirstOrDefault(vm => vm.MaterialType == "link");

            if (virtualMaterial != null)
            {
                videoLink = virtualMaterial.MaterialValue;

            }


            return videoLink;

        }

        private string getHandout(Submission submission)
        {

            string handout = null;

            var virtualMaterial = submission.VirtualMaterials.FirstOrDefault(vm => vm.MaterialType == "handout");

            if (virtualMaterial != null)
            {
                handout = virtualMaterial.MaterialValue;
            }

            return handout;

        }

        private string getPosterImageLink(Submission submission)
        {

            string posterImageLink = null;

            var virtualMaterial = submission.VirtualMaterials.FirstOrDefault(vm => vm.MaterialType == "poster");

            if (virtualMaterial != null)
            {
                posterImageLink = virtualMaterial.MaterialValue;
            }

            return posterImageLink;

        }

        private int getVirtualConferenceSessionId(Submission submission)
        {
            int virtualConferenceSessionId = 0;

            var virtualConferenceSession = submission.VirtualConferenceSessions.
                FirstOrDefault(vcs => vcs.SubmissionPresentationMethod.PresentationMethod.PresentationMethodName 
                == PresentationMethodType.RECORDED_ONLINE);

            if (virtualConferenceSession != null)
            {

                virtualConferenceSessionId = virtualConferenceSession.VirtualConferenceSessionId;

            }

            return virtualConferenceSessionId;
        }


        private string getSessionCode(Submission submission)
        {
            string sessionCode = null;

            var virtualConferenceSession = submission.VirtualConferenceSessions.
                FirstOrDefault(vcs => vcs.SubmissionPresentationMethod.PresentationMethod.PresentationMethodName
                == PresentationMethodType.RECORDED_ONLINE);

            if (virtualConferenceSession != null)
            {

                sessionCode = virtualConferenceSession.SessionCode;

            }

            return sessionCode;
        }

        private string GetSubmissionKeyWords(Submission submission)
        {

            List<SubmissionFormField> submissionFormFields = _submissionService.GetSubmissionFormFields(submission.SubmissionId).ToList();

            string keyWordOne = "";

            string keyWordTwo = "";


            foreach (SubmissionFormField submissionFormField in submissionFormFields)
            {
                foreach (FormFieldProperty formFieldProperty in submissionFormField.FormField.FormFieldProperties)
                {
                    if (formFieldProperty.PropertyValue.Contains("Keyword One", StringComparison.InvariantCultureIgnoreCase))
                    {
                        keyWordOne = submissionFormField.SubmissionFormFieldValue;
                    }

                    if (formFieldProperty.PropertyValue.Contains("Keyword Two", StringComparison.InvariantCultureIgnoreCase))
                    {
                        keyWordTwo = submissionFormField.SubmissionFormFieldValue;
                    }

                }
            }

            string keyWords =  keyWordOne + "|" + keyWordTwo ;

            return keyWords;

        }

        private string GetPresentationType(Submission submission)
        {
            string presentationType = "poster";

            if (! submission.AcceptedCategory.SubmissionCategoryName.Contains("poster", StringComparison.InvariantCultureIgnoreCase))
            {

                presentationType = "video";

            }

            return presentationType;
        }
    }
}
