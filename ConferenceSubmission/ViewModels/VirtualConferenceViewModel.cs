﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels
{
    public class VirtualConferenceViewModel
    {

        public Conference Conference { get; set; }

        public VirtualConferenceData VirtualConferenceData { get; set; }
    }
}
