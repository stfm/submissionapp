﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ConferenceSubmission.Data
{
    public class ReviewFormFieldRepository : IReviewFormFieldRepository
    {

        private readonly AppDbContext _appDbContext;

        public ReviewFormFieldRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public void AddReviewFormField(ReviewFormField reviewFormField)
        {

            _appDbContext.ReviewFormFields.Add(reviewFormField);

            _appDbContext.SaveChanges();

        }

        public IEnumerable<ReviewFormField> GetFormFieldsForReview(int reviewId)
        {

            return _appDbContext.ReviewFormFields.Where(r => r.ReviewId.Equals(reviewId))
                                .Include(s => s.FormField)
                                .ThenInclude(ff => ff.FormFieldProperties);


        }

        public IEnumerable<ReviewFormField> GetAllFormFieldsForMultipleReviews(List<int> reviewIds)
        {


            return _appDbContext.ReviewFormFields.Where(r => reviewIds.Contains(r.ReviewId))
                                .Include(s => s.FormField)
                                                        .ThenInclude(ff => ff.FormFieldProperties);            

        }

        public void RemoveAllFormFieldsForReview(int reviewId)
        {
            var reviewFormFieldsToRemove = _appDbContext.ReviewFormFields.Where(r => r.ReviewId == reviewId);

            _appDbContext.RemoveRange(reviewFormFieldsToRemove);

            _appDbContext.SaveChanges();

        }

        public IEnumerable<ReviewFormField> GetRatingScaleFormFieldsForMultipleReviews(List<int> reviewIds)
        {
            return _appDbContext.ReviewFormFields.Where(r => reviewIds.Contains(r.ReviewId) && r.FormField.FormFieldType.Equals("ratingscale") )
                                .Include(s => s.FormField)
                                                        .ThenInclude(ff => ff.FormFieldProperties);

        
        }

        public IEnumerable<ReviewFormField> GetRatingScaleFormFieldsForSingleReview(int reviewId)
        {
            return _appDbContext.ReviewFormFields.Where(r => r.ReviewId == reviewId && r.FormField.FormFieldType.Equals("ratingscale"))
                                .Include(s => s.FormField)
                                                        .ThenInclude(ff => ff.FormFieldProperties);

        }
    }
}
