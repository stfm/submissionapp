﻿using ConferenceSubmission.DTOs;
using ConferenceSubmission.DTOs.SalesforceEvent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class RegistrationReportsParametersViewModel
    {
        public RegistrationReportsParametersViewModel()
        {
            Confernces = new List<EntityByNameAndId>();
            SalesforceEvents = new List<SFEvent>();
        }

        public List<EntityByNameAndId> Confernces { get; set; }
        public List<SFEvent> SalesforceEvents { get; set; }
        public RegistrationReportType ReportType { get; set; }
    }

    public enum RegistrationReportType
    {
        SESSIONS_WITH_NO_REGISTRATIONS,
        ALL_SESSIONS
    }
}
