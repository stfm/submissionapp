﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs.PosterHall
{
    public class PosterWithPresenters
    {
        public int                  SubmissionId          { get; set; }
        public string               Title                 { get; set; }
        public string               SessionCode           { get; set; }
        public string               Abstract              { get; set; }
        public string               SessionTrack          { get; set; }
        public string               Presenters            { get; set; }
        public string               PosterImageFileName   { get; set; }
        public string               YouTubeEmbedLink      { get; set; }
        public string               HandoutFileName       { get; set; }
        public PresenterToNotify    PresenterToNotify     { get; set; }
    }

    public class PresenterToNotify
    {
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}
