﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class AuditLogRepositoryTest
    {
        [Fact]
        public void GetByEntityId_NoAuditLogs_VerifyEmptyResults()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var auditLogParentEntities = new List<AuditLogParentEntity>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var auditLogRepository = new AuditLogRepository(ctx);

                //Act
                var results = auditLogRepository.GetByEntityId(1, EntityType.Submission);

                //Assert
                Assert.Empty(results);
            }
        }

        [Fact]
        public void GetByEntityId_StubAnAuditLogAndSearchForAnAuditLogWithSameEntityIdButDifferentEntityType_VerifyEmptyResults()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            auditLogs.Add(new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                EntityType = EntityType.Submission
            });

            var auditLogParentEntities = new List<AuditLogParentEntity>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var auditLogRepository = new AuditLogRepository(ctx);

                //Act
                var results = auditLogRepository.GetByEntityId(1, EntityType.ConferenceSession);

                //Assert
                Assert.Empty(results);
            }
        }

        [Fact]
        public void GetByEntityId_StubAnAuditLog_VerifyResults()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            auditLogs.Add(new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                EntityType = EntityType.Submission,
                OldValue = "3",
                NewValue = "4"
            });

            var auditLogParentEntities = new List<AuditLogParentEntity>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var auditLogRepository = new AuditLogRepository(ctx);

                //Act
                var results = auditLogRepository.GetByEntityId(1, EntityType.Submission);

                //Assert
                Assert.Single(results);
                Assert.Equal(EntityType.Submission, results[0].EntityType);
                Assert.Equal(1, results[0].EntityId);
                Assert.Equal("3", results[0].OldValue);
                Assert.Equal("4", results[0].NewValue);
            }
        }

        [Fact]
        public void GetByEntityId_StubMultipleAuditLogs_VerifyResults()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            auditLogs.Add(new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                EntityType = EntityType.Submission,
                OldValue = "3",
                NewValue = "4",
                TimeStamp = DateTime.Now.AddDays(3)
            });
            auditLogs.Add(new AuditLog
            {
                AuditLogId = 2,
                EntityId = 1,
                EntityType = EntityType.Submission,
                OldValue = "4",
                NewValue = "5",
                TimeStamp = DateTime.Now.AddDays(1)
            });
            auditLogs.Add(new AuditLog
            {
                AuditLogId = 3,
                EntityId = 1,
                EntityType = EntityType.Submission,
                OldValue = "5",
                NewValue = "6",
                TimeStamp = DateTime.Now.AddDays(5)
            });

            var auditLogParentEntities = new List<AuditLogParentEntity>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var auditLogRepository = new AuditLogRepository(ctx);

                //Act
                var results = auditLogRepository.GetByEntityId(1, EntityType.Submission);

                //Assert
                Assert.Equal(3, results.Count);
                Assert.Equal(EntityType.Submission, results[0].EntityType);
                Assert.Equal(1, results[0].EntityId);
                Assert.Equal("4", results[0].OldValue);
                Assert.Equal("5", results[0].NewValue);
                Assert.Equal(EntityType.Submission, results[1].EntityType);
                Assert.Equal(1, results[1].EntityId);
                Assert.Equal("3", results[1].OldValue);
                Assert.Equal("4", results[1].NewValue);
                Assert.Equal(EntityType.Submission, results[2].EntityType);
                Assert.Equal(1, results[2].EntityId);
                Assert.Equal("5", results[2].OldValue);
                Assert.Equal("6", results[2].NewValue);

                //Verifying Order by timestamp
                Assert.Equal(2, results[0].AuditLogId);
                Assert.Equal(1, results[1].AuditLogId);
                Assert.Equal(3, results[2].AuditLogId);
            }
        }

        [Fact]
        public void GetByEntityId_StubAnAuditLogWithChildAuditLogs_VerifyResults()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var parentAuditLog = new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                EntityType = EntityType.Submission,
                OldValue = "1",
                NewValue = "2",
                AuditLogActionType = AuditLogActionType.MODIFIED,
                TimeStamp = DateTime.Now
            };
            var childAuditLog1 = new AuditLog
            {
                AuditLogId = 2,
                EntityId = 100,
                EntityType = EntityType.ConferenceSession,
                OldValue = "P35",
                NewValue = "P19",
                PropertyName = "SessionCode",
                AuditLogActionType = AuditLogActionType.MODIFIED,
                TimeStamp = DateTime.Now.AddDays(-1)
            };
            var childAuditLog2 = new AuditLog
            {
                AuditLogId = 3,
                EntityId = 1000,
                EntityType = EntityType.SubmissionParticipant,
                OldValue = "",
                NewValue = "ENTITY DELETED",
                PropertyName = "",
                AuditLogActionType = AuditLogActionType.DELETED,
                TimeStamp = DateTime.Now.AddDays(2)
            };
            auditLogs.Add(parentAuditLog);
            auditLogs.Add(childAuditLog1);
            auditLogs.Add(childAuditLog2);

            var auditLogParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity1 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 2,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity2 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 2,
                AuditLogId = 3,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            auditLogParentEntities.Add(auditLogParentEntity1);
            auditLogParentEntities.Add(auditLogParentEntity2);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var auditLogRepository = new AuditLogRepository(ctx);

                //Act
                var results = auditLogRepository.GetByEntityId(1, EntityType.Submission);

                //Assert
                Assert.Equal(3, results.Count);

                Assert.Contains(results, r => r.EntityType == EntityType.Submission);
                Assert.Contains(results, r => r.EntityType == EntityType.SubmissionParticipant);
                Assert.Contains(results, r => r.EntityType == EntityType.ConferenceSession);

                Assert.Contains(results, r => r.EntityId == 1);
                Assert.Contains(results, r => r.EntityId == 100);
                Assert.Contains(results, r => r.EntityId == 1000);

                Assert.Contains(results, r => r.AuditLogId == 1);
                Assert.Contains(results, r => r.AuditLogId == 2);
                Assert.Contains(results, r => r.AuditLogId == 3);

                //Verify order by timestamp
                Assert.Equal(2, results[0].AuditLogId);
                Assert.Equal(1, results[1].AuditLogId);
                Assert.Equal(3, results[2].AuditLogId);
            }
        }

        [Fact]
        public void GetByEntityId_StubMultipleAuditLogsWithMultipleChildAuditLogs_VerifyResults()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var parentAuditLog_Submission = new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                EntityType = EntityType.Submission,
                OldValue = "1",
                NewValue = "2",
                AuditLogActionType = AuditLogActionType.MODIFIED
            };
            var parentAuditLog_Participant = new AuditLog
            {
                AuditLogId = 2,
                EntityId = 50,
                EntityType = EntityType.Participant,
                NewValue = "ENTITY CREATED",
                AuditLogActionType = AuditLogActionType.CREATED
            };
            var childAuditLogSubmissionParticipant = new AuditLog
            {
                AuditLogId = 3,
                EntityId = 100,
                EntityType = EntityType.ConferenceSession,
                OldValue = "P35",
                NewValue = "P19",
                PropertyName = "SessionCode",
                AuditLogActionType = AuditLogActionType.MODIFIED
            };
            var childAuditLogConferenceSession = new AuditLog
            {
                AuditLogId = 4,
                EntityId = 1000,
                EntityType = EntityType.SubmissionParticipant,
                OldValue = "",
                NewValue = "ENTITY DELETED",
                PropertyName = "",
                AuditLogActionType = AuditLogActionType.DELETED
            };
            auditLogs.Add(parentAuditLog_Submission);
            auditLogs.Add(parentAuditLog_Participant);
            auditLogs.Add(childAuditLogSubmissionParticipant);
            auditLogs.Add(childAuditLogConferenceSession);

            var auditLogParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity1 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 3,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity2 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 2,
                AuditLogId = 4,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity3 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 3,
                AuditLogId = 4,
                ParentEntityId = 50,
                ParentEntityType = EntityType.Participant
            };
            auditLogParentEntities.Add(auditLogParentEntity1);
            auditLogParentEntities.Add(auditLogParentEntity2);
            auditLogParentEntities.Add(auditLogParentEntity3);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var auditLogRepository = new AuditLogRepository(ctx);

                //Act
                var auditLogsForSubmission = auditLogRepository.GetByEntityId(1, EntityType.Submission);
                var auditLogsForParticipant = auditLogRepository.GetByEntityId(50, EntityType.Participant);

                //Assert
                Assert.Equal(3, auditLogsForSubmission.Count);
                Assert.Equal(2, auditLogsForParticipant.Count);

                Assert.Contains(auditLogsForSubmission, r => r.EntityType == EntityType.Submission);
                Assert.Contains(auditLogsForSubmission, r => r.EntityType == EntityType.SubmissionParticipant);
                Assert.Contains(auditLogsForSubmission, r => r.EntityType == EntityType.ConferenceSession);

                Assert.Contains(auditLogsForParticipant, r => r.EntityType == EntityType.SubmissionParticipant);
                Assert.Contains(auditLogsForParticipant, r => r.EntityType == EntityType.Participant);

                Assert.Contains(auditLogsForSubmission, r => r.EntityId == 1);
                Assert.Contains(auditLogsForSubmission, r => r.EntityId == 100);
                Assert.Contains(auditLogsForSubmission, r => r.EntityId == 1000);

                Assert.Contains(auditLogsForParticipant, r => r.EntityId == 50);
                Assert.Contains(auditLogsForParticipant, r => r.EntityId == 1000);

                Assert.Contains(auditLogsForSubmission, r => r.AuditLogId == 1);
                Assert.Contains(auditLogsForSubmission, r => r.AuditLogId == 3);
                Assert.Contains(auditLogsForSubmission, r => r.AuditLogId == 4);

                Assert.Contains(auditLogsForParticipant, r => r.AuditLogId == 2);
                Assert.Contains(auditLogsForParticipant, r => r.AuditLogId == 4);
            }
        }

        [Fact]
        public void GetByEntityId_StubOneParentAuditLogAndMultipleChildAuditLogsWhereChildEntityParentTypeDoesNotMatchParentEntityType_VerifyResults()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var parentAuditLog_Participant = new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                EntityType = EntityType.Participant,
                OldValue = "ENTITY CREATED",
                AuditLogActionType = AuditLogActionType.CREATED
            };
            var childAuditLogSubmissionParticipant = new AuditLog
            {
                AuditLogId = 2,
                EntityId = 1000,
                EntityType = EntityType.SubmissionParticipant,
                OldValue = "",
                NewValue = "ENTITY DELETED",
                PropertyName = "",
                AuditLogActionType = AuditLogActionType.DELETED
            };
            auditLogs.Add(parentAuditLog_Participant);
            auditLogs.Add(childAuditLogSubmissionParticipant);

            var auditLogParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity1 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 2,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            auditLogParentEntities.Add(auditLogParentEntity1);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var auditLogRepository = new AuditLogRepository(ctx);

                //Act
                var results = auditLogRepository.GetByEntityId(1, EntityType.Participant);

                //Assert
                Assert.Single(results);
                Assert.Contains(results, r => r.EntityType == EntityType.Participant);
                Assert.Contains(results, r => r.EntityId == 1);
                Assert.Contains(results, r => r.AuditLogId == 1);
            }
        }

        [Fact]
        public void GetByEntityId_StubTwoAuditsLogsOfTheSameEntityTypeAndOneChildAuditLog_VerifyChildAuditLogReturnedForTheCorrectParent()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var parentAuditLog_Submission1 = new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                EntityType = EntityType.Submission,
                OldValue = "5",
                NewValue = "6",
                AuditLogActionType = AuditLogActionType.MODIFIED
            };
            var parentAuditLog_Submission2 = new AuditLog
            {
                AuditLogId = 2,
                EntityId = 2,
                EntityType = EntityType.Submission,
                OldValue = "15",
                NewValue = "16",
                AuditLogActionType = AuditLogActionType.MODIFIED
            };
            var childAuditLogConferenceSession = new AuditLog
            {
                AuditLogId = 3,
                EntityId = 1000,
                EntityType = EntityType.ConferenceSession,
                OldValue = "L05",
                NewValue = "L06",
                PropertyName = "SessionCode",
                AuditLogActionType = AuditLogActionType.MODIFIED
            };
            auditLogs.Add(parentAuditLog_Submission1);
            auditLogs.Add(parentAuditLog_Submission2);
            auditLogs.Add(childAuditLogConferenceSession);

            var auditLogParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity1 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 3,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            auditLogParentEntities.Add(auditLogParentEntity1);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var auditLogRepository = new AuditLogRepository(ctx);

                //Act
                var auditLogsForSubmission1 = auditLogRepository.GetByEntityId(1, EntityType.Submission);
                var auditLogsForSubmission2 = auditLogRepository.GetByEntityId(2, EntityType.Submission);

                //Assert
                Assert.Equal(2, auditLogsForSubmission1.Count);
                Assert.Single(auditLogsForSubmission2);

                Assert.Contains(auditLogsForSubmission1, a => a.AuditLogId == 1);
                Assert.Contains(auditLogsForSubmission1, a => a.AuditLogId == 3);

                Assert.Contains(auditLogsForSubmission2, a => a.AuditLogId == 2);

                Assert.Contains(auditLogsForSubmission1, a => a.EntityType == EntityType.Submission);
                Assert.Contains(auditLogsForSubmission1, a => a.EntityType == EntityType.ConferenceSession);

                Assert.Contains(auditLogsForSubmission2, a => a.EntityType == EntityType.Submission);
            }
        }

        [Fact]
        public void GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType_StubParentAuditLogAndChildAuditLogThatMatchesFilterConditions_VerifyResult()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var parentAuditLog = new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                AuditLogActionType = AuditLogActionType.CREATED,
                EntityType = EntityType.Submission,
                NewValue = "ENTITY CREATED",
                TimeStamp = DateTime.Now
            };
            var childAuditLog = new AuditLog
            {
                AuditLogId = 2,
                EntityId = 1000,
                AuditLogActionType = AuditLogActionType.DELETED,
                EntityType = EntityType.ConferenceSession,
                NewValue = "ENTITY DELETED",
                TimeStamp = DateTime.Now
            };
            auditLogs.Add(parentAuditLog);
            auditLogs.Add(childAuditLog);

            var auditLogParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 2,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            auditLogParentEntities.Add(auditLogParentEntity);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                //Act
                var auditLogRepository = new AuditLogRepository(ctx);

                var results = auditLogRepository.GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType(new List<int> { 1 }, EntityType.Submission, AuditLogActionType.DELETED, EntityType.ConferenceSession);

                //Assert
                Assert.Single(results);
                Assert.Contains(results, r => r.EntityId == 1000);
            }
        }

        [Fact]
        public void GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType_StubParentAuditLogAndChildAuditLogThatDoesNotMatchAuditLogActionTypeFilterCondition_VerifyResult()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var parentAuditLog = new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                AuditLogActionType = AuditLogActionType.CREATED,
                EntityType = EntityType.Submission,
                NewValue = "ENTITY CREATED",
                TimeStamp = DateTime.Now
            };
            var childAuditLog = new AuditLog
            {
                AuditLogId = 2,
                EntityId = 1000,
                AuditLogActionType = AuditLogActionType.MODIFIED,
                EntityType = EntityType.ConferenceSession,
                NewValue = "5",
                OldValue = "6",
                TimeStamp = DateTime.Now
            };
            auditLogs.Add(parentAuditLog);
            auditLogs.Add(childAuditLog);

            var auditLogParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 2,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            auditLogParentEntities.Add(auditLogParentEntity);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                //Act
                var auditLogRepository = new AuditLogRepository(ctx);

                var results = auditLogRepository.GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType(new List<int>() { 1 }, EntityType.Submission, AuditLogActionType.DELETED, EntityType.ConferenceSession);

                //Assert
                Assert.Empty(results);
            }
        }

        [Fact]
        public void GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType_StubMultipleParentAuditLogsAndChildAuditLogsThatMatchFilterCritera_VerifyResult()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var parentAuditLog1 = new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                AuditLogActionType = AuditLogActionType.CREATED,
                EntityType = EntityType.Submission,
                NewValue = "ENTITY CREATED",
                TimeStamp = DateTime.Now
            };
            var parentAuditLog2 = new AuditLog
            {
                AuditLogId = 2,
                EntityId = 2,
                AuditLogActionType = AuditLogActionType.CREATED,
                EntityType = EntityType.Submission,
                NewValue = "ENTITY CREATED",
                TimeStamp = DateTime.Now
            };
            var childAuditLog1 = new AuditLog
            {
                AuditLogId = 3,
                EntityId = 1000,
                AuditLogActionType = AuditLogActionType.MODIFIED,
                EntityType = EntityType.ConferenceSession,
                NewValue = "5",
                OldValue = "6",
                TimeStamp = DateTime.Now
            };
            var childAuditLog2 = new AuditLog
            {
                AuditLogId = 4,
                EntityId = 1001,
                AuditLogActionType = AuditLogActionType.MODIFIED,
                EntityType = EntityType.ConferenceSession,
                NewValue = "7",
                OldValue = "8",
                TimeStamp = DateTime.Now
            };
            auditLogs.Add(parentAuditLog1);
            auditLogs.Add(parentAuditLog2);
            auditLogs.Add(childAuditLog1);
            auditLogs.Add(childAuditLog2);

            var auditLogParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity1 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 3,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity2 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 2,
                AuditLogId = 4,
                ParentEntityId = 2,
                ParentEntityType = EntityType.Submission
            };
            auditLogParentEntities.Add(auditLogParentEntity1);
            auditLogParentEntities.Add(auditLogParentEntity2);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                //Act
                var auditLogRepository = new AuditLogRepository(ctx);

                var results = auditLogRepository.GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType(new List<int>() { 1, 2 }, EntityType.Submission, AuditLogActionType.MODIFIED, EntityType.ConferenceSession);

                //Assert
                Assert.Equal(2, results.Count);
                Assert.Equal(2, results.Where(r => r.EntityType == EntityType.ConferenceSession).ToList().Count);
                Assert.Single(results.Where(r => r.EntityId == 1000));
                Assert.Single(results.Where(r => r.EntityId == 1001));
            }
        }

        [Fact]
        public void GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType_StubMultipleParentAuditLogsAndChildAuditLogsAndSometMatchFilterCriteraAndSomeDoNot_VerifyResult()
        {
            //Arrange
            var auditLogs = new List<AuditLog>();
            var parentAuditLog1 = new AuditLog
            {
                AuditLogId = 1,
                EntityId = 1,
                AuditLogActionType = AuditLogActionType.CREATED,
                EntityType = EntityType.Submission,
                NewValue = "ENTITY CREATED",
                TimeStamp = DateTime.Now
            };
            var parentAuditLog2 = new AuditLog
            {
                AuditLogId = 2,
                EntityId = 2,
                AuditLogActionType = AuditLogActionType.CREATED,
                EntityType = EntityType.Submission,
                NewValue = "ENTITY CREATED",
                TimeStamp = DateTime.Now
            };
            var childAuditLog1 = new AuditLog
            {
                AuditLogId = 3,
                EntityId = 1000,
                AuditLogActionType = AuditLogActionType.MODIFIED,
                EntityType = EntityType.ConferenceSession,
                NewValue = "5",
                OldValue = "6",
                TimeStamp = DateTime.Now
            };
            var childAuditLog2 = new AuditLog
            {
                AuditLogId = 4,
                EntityId = 1001,
                AuditLogActionType = AuditLogActionType.MODIFIED,
                EntityType = EntityType.ConferenceSession,
                NewValue = "7",
                OldValue = "8",
                TimeStamp = DateTime.Now
            };
            var childAuditLog3 = new AuditLog
            {
                AuditLogId = 5,
                EntityId = 1002,
                AuditLogActionType = AuditLogActionType.MODIFIED,
                EntityType = EntityType.ConferenceSession,
                NewValue = "5",
                OldValue = "6",
                TimeStamp = DateTime.Now
            };
            var childAuditLog4 = new AuditLog
            {
                AuditLogId = 6,
                EntityId = 1003,
                AuditLogActionType = AuditLogActionType.DELETED,
                EntityType = EntityType.ConferenceSession,
                NewValue = "7",
                OldValue = "8",
                TimeStamp = DateTime.Now
            };
            auditLogs.Add(parentAuditLog1);
            auditLogs.Add(parentAuditLog2);
            auditLogs.Add(childAuditLog1);
            auditLogs.Add(childAuditLog2);
            auditLogs.Add(childAuditLog3);
            auditLogs.Add(childAuditLog4);

            var auditLogParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity1 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 3,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity2 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 2,
                AuditLogId = 4,
                ParentEntityId = 2,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity3 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 3,
                AuditLogId = 5,
                ParentEntityId = 1,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity4 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 4,
                AuditLogId = 6,
                ParentEntityId = 2,
                ParentEntityType = EntityType.Submission
            };
            auditLogParentEntities.Add(auditLogParentEntity1);
            auditLogParentEntities.Add(auditLogParentEntity2);
            auditLogParentEntities.Add(auditLogParentEntity3);
            auditLogParentEntities.Add(auditLogParentEntity4);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                //Act
                var auditLogRepository = new AuditLogRepository(ctx);

                var results = auditLogRepository.GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType(new List<int>() { 1, 2 }, EntityType.Submission, AuditLogActionType.MODIFIED, EntityType.ConferenceSession);

                //Assert
                Assert.Equal(3, results.Count);
                Assert.Equal(3, results.Where(r => r.EntityType == EntityType.ConferenceSession).ToList().Count);
                Assert.Single(results.Where(r => r.EntityId == 1000));
                Assert.Single(results.Where(r => r.EntityId == 1001));
                Assert.Single(results.Where(r => r.EntityId == 1002));
            }
        }

        [Fact]
        public void GetParentIdsForEntities_StubAnAuditLogForEntityWithParent_VerifyResults()
        {
            //Act
            var auditLogs = new List<AuditLog>();
            var childAuditLog = new AuditLog
            {
                AuditLogId = 1,
                AuditLogActionType = AuditLogActionType.DELETED,
                EntityId = 1,
                EntityType = EntityType.SubmissionParticipant,
                NewValue = "ENTITY DELETED"
            };
            auditLogs.Add(childAuditLog);

            var auditLogsParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 1,
                ParentEntityId = 100,
                ParentEntityType = EntityType.Participant
            };
            auditLogsParentEntities.Add(auditLogParentEntity);


            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogsParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                //Act
                var auditLogRepository = new AuditLogRepository(ctx);

                var results = auditLogRepository.GetParentIdsForEntities(new List<int> { 1 }, EntityType.Participant);

                //Assert
                Assert.Single(results);
                Assert.Contains(results, r => r.Key == 1);
                Assert.Contains(results, r => r.Value == 100);
            }
        }

        [Fact]
        public void GetParentIdsForEntities_StubAnAuditLogForEntityWithNoParent_VerifyResults()
        {
            //Act
            var auditLogs = new List<AuditLog>();
            var childAuditLog = new AuditLog
            {
                AuditLogId = 1,
                AuditLogActionType = AuditLogActionType.CREATED,
                EntityId = 1,
                EntityType = EntityType.Submission,
                NewValue = "ENTITY CREATED"
            };
            auditLogs.Add(childAuditLog);

            var auditLogsParentEntities = new List<AuditLogParentEntity>();

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogsParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                //Act
                var auditLogRepository = new AuditLogRepository(ctx);

                var results = auditLogRepository.GetParentIdsForEntities(new List<int> { 1 }, EntityType.Participant);

                //Assert
                Assert.Empty(results);
            }
        }

        [Fact]
        public void GetParentIdsForEntities_StubAnAuditLogForEntityWithTwoParents_VerifyCorrectParentReturned()
        {
            //Act
            var auditLogs = new List<AuditLog>();
            var childAuditLog = new AuditLog
            {
                AuditLogId = 1,
                AuditLogActionType = AuditLogActionType.DELETED,
                EntityId = 12,
                EntityType = EntityType.SubmissionParticipant,
                NewValue = "ENTITY DELETED"
            };
            auditLogs.Add(childAuditLog);

            var auditLogsParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity1 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 1,
                ParentEntityId = 1000,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity2 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 2,
                AuditLogId = 1,
                ParentEntityId = 99,
                ParentEntityType = EntityType.Participant
            };
            auditLogsParentEntities.Add(auditLogParentEntity1);
            auditLogsParentEntities.Add(auditLogParentEntity2);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogsParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                //Act
                var auditLogRepository = new AuditLogRepository(ctx);

                var results = auditLogRepository.GetParentIdsForEntities(new List<int> { 1 }, EntityType.Participant);

                //Assert
                Assert.Single(results);
                Assert.Contains(results, r => r.Key == 12);
                Assert.Contains(results, r => r.Value == 99);
            }
        }

        [Fact]
        public void GetParentIdsForEntities_StubMultipleAuditLogs_VerifyResults()
        {
            //Act
            var auditLogs = new List<AuditLog>();
            var childAuditLog1 = new AuditLog
            {
                AuditLogId = 1,
                AuditLogActionType = AuditLogActionType.DELETED,
                EntityId = 12,
                EntityType = EntityType.SubmissionParticipant,
                NewValue = "ENTITY DELETED"
            };
            var childAuditLog2 = new AuditLog
            {
                AuditLogId = 2,
                AuditLogActionType = AuditLogActionType.DELETED,
                EntityId = 13,
                EntityType = EntityType.SubmissionParticipant,
                NewValue = "ENTITY DELETED"
            };
            var childAuditLog3 = new AuditLog
            {
                AuditLogId = 3,
                AuditLogActionType = AuditLogActionType.DELETED,
                EntityId = 14,
                EntityType = EntityType.SubmissionParticipant,
                NewValue = "ENTITY DELETED"
            };
            auditLogs.Add(childAuditLog1);
            auditLogs.Add(childAuditLog2);
            auditLogs.Add(childAuditLog3);

            var auditLogsParentEntities = new List<AuditLogParentEntity>();
            var auditLogParentEntity1 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 1,
                AuditLogId = 1,
                ParentEntityId = 1000,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity2 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 2,
                AuditLogId = 1,
                ParentEntityId = 99,
                ParentEntityType = EntityType.Participant
            };
            var auditLogParentEntity3 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 3,
                AuditLogId = 2,
                ParentEntityId = 1001,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity4 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 4,
                AuditLogId = 2,
                ParentEntityId = 98,
                ParentEntityType = EntityType.Participant
            };
            var auditLogParentEntity5 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 5,
                AuditLogId = 3,
                ParentEntityId = 1002,
                ParentEntityType = EntityType.Submission
            };
            var auditLogParentEntity6 = new AuditLogParentEntity
            {
                AuditLogParentEntityId = 6,
                AuditLogId = 3,
                ParentEntityId = 97,
                ParentEntityType = EntityType.Participant
            };
            auditLogsParentEntities.Add(auditLogParentEntity1);
            auditLogsParentEntities.Add(auditLogParentEntity2);
            auditLogsParentEntities.Add(auditLogParentEntity3);
            auditLogsParentEntities.Add(auditLogParentEntity4);
            auditLogsParentEntities.Add(auditLogParentEntity5);
            auditLogsParentEntities.Add(auditLogParentEntity6);

            DbContextOptions<AppDbContext> options = GetDbContextOptions(auditLogs, auditLogsParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                //Act
                var auditLogRepository = new AuditLogRepository(ctx);

                var resultsForParentTypeParticipant = auditLogRepository.GetParentIdsForEntities(new List<int> { 1, 2, 3 }, EntityType.Participant);
                var resultsForParentTypeSubmission = auditLogRepository.GetParentIdsForEntities(new List<int> { 1, 2, 3 }, EntityType.Submission);

                //Assert
                Assert.Equal(3, resultsForParentTypeParticipant.Count);
                Assert.Equal(3, resultsForParentTypeSubmission.Count);

                Assert.Equal(99, resultsForParentTypeParticipant[12]);
                Assert.Equal(98, resultsForParentTypeParticipant[13]);
                Assert.Equal(97, resultsForParentTypeParticipant[14]);

                Assert.Equal(1000, resultsForParentTypeSubmission[12]);
                Assert.Equal(1001, resultsForParentTypeSubmission[13]);
                Assert.Equal(1002, resultsForParentTypeSubmission[14]);
            }
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions(List<AuditLog> auditLogs, List<AuditLogParentEntity> auditLogParentEntities)
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                context.AuditLogs.AddRange(auditLogs);
                context.AuditLogParentEntities.AddRange(auditLogParentEntities);
                context.SaveChanges();
            }

            return options;
        }
    }
}
