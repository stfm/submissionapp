﻿using System;
namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Models different submission payment requirements.
    /// </summary>
    public enum SubmissionPaymentRequirement
    {
        //Submission payment is not required
        NONE,

        //Submission paymnent is required by only non-member submitters
        NON_MEMBER_ONLY,

        //Submission payment is required by all submitters
        EVERYONE

    }
}
