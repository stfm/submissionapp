﻿using System;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using ConferenceSubmission.Mappers;
using ConferenceSubmission.DTOs;

namespace ConferenceSubmission.Controllers
{
    /// <summary>
    /// Responds to requests involving presenters on a submission
    /// </summary>
    [Authorize]
    public class PresenterController : Controller
    {
        private readonly ISubmissionService _submissionService;

        private readonly IParticipantService _participantService;

        private readonly ISalesforceAPIService _salesforceAPIService;

        private readonly ISubmissionParticipantService _submissionParticipantService;

        private readonly IUserMapper _userMapper;

        private readonly IDisclosureService _disclosureService;

        private readonly IEmailNotificationService _emailNotificationService;

        public PresenterController(ISubmissionService submissionService, IParticipantService participantService,
                                   ISalesforceAPIService salesforceAPIService,
                                   ISubmissionParticipantService submissionParticipantService,
                                   IUserMapper userMapper,
                                   IDisclosureService disclosureService,
                                  IEmailNotificationService emailNotificationService)
        {

            _submissionService = submissionService;

            _participantService = participantService;

            _salesforceAPIService = salesforceAPIService;

            _submissionParticipantService = submissionParticipantService;

            _userMapper = userMapper;

            _disclosureService = disclosureService;

            _emailNotificationService = emailNotificationService;

        }

        /// <summary>
        /// Responds to initail update presenters request
        /// for a specific submission.
        /// </summary>
        /// <returns>The index.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public IActionResult Index(int submissionId)
        {

            Log.Information($"Request made to update presenters for submission id {submissionId}");

            Submission submission = _submissionService.GetSubmission(submissionId);

            UpdatePresentersViewModel updatePresentersViewModel = new UpdatePresentersViewModel
            {
                ConferenceName = submission.Conference.ConferenceLongName,

                SubmittedCategory = submission.SubmissionCategory.SubmissionCategoryName,

                AcceptedCategory = submission.AcceptedCategory.SubmissionCategoryName,

                SubmissionId = submissionId,

                SubmissionTitle = submission.SubmissionTitle,

                AddParticipantUrl = _participantService.GetAddParticipantUrl(Convert.ToString(submissionId), Request.Host.Value),

                Presenters = new List<Presenter>(),

                CurrentUserParticipantRoles = _participantService.GetParticipantsRolesBySubmission(submissionId, _userMapper.Map(User.Claims).STFMUserId)

            };

            //Get the STFM User ID for each SubmissionParticipant for this submission and then send that collection of STFM User ID values
            //to Salesforce to get their user data - store the result in collection of User objects.
            var salesforceUsers = _salesforceAPIService.GetMultipleUserInfo(submission.ParticipantLink.Select(p => p.Participant.StfmUserId)).Result;

            //For each SubmissionParticipant get their data from SalesForce
            //and then create a Presenter object

            foreach (SubmissionParticipant submissionParticipant in submission.ParticipantLink)
            {

                var stfmUserId = submissionParticipant.Participant.StfmUserId;

                //Get the Salesforce user data for this submission participant
                var user = salesforceUsers.FirstOrDefault(su => su.STFMUserId == submissionParticipant.Participant.StfmUserId);

                Presenter presenter = new Presenter
                {
                    SubmissionParticipantId = submissionParticipant.SubmissionParticipantId,

                    SubmissionParticipantToParticipantRolesLink = submissionParticipant.SubmissionParticipantToParticipantRolesLink,

                    FirstName = user.FirstName,

                    LastName = user.LastName,

                    AssignedParticipantRolesType = _participantService.GetAssignedParticipantRolesType(submissionId, submissionParticipant.SubmissionParticipantId),

                    SortOrder = submissionParticipant.SortOrder

                };

                updatePresentersViewModel.Presenters.Add(presenter);

            }

            return View(updatePresentersViewModel);

        }


        public RedirectToActionResult RemoveSubmissionParticipant(int submissionParticipantId, int submissionId)
        {

            Log.Information($"Request made to remove submission participant with id of {submissionParticipantId} from submission id of {submissionId}");

            _submissionParticipantService.DeleteSubmissionParticipant(submissionParticipantId);

            return RedirectToAction(actionName: "Index", controllerName: "Presenter", routeValues: new { submissionId = submissionId });

        }

        /// <summary>
        /// Action that responds to a users request to add himself as a presenter to a submission
        /// </summary>
        /// <param name="SubmissionId"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult AddParticipant(int SubmissionId)
        {
            Log.Information($"User with STFMUserId {_userMapper.Map(User.Claims).STFMUserId} request to add himself as presenter to the submission with id {SubmissionId}");

            var submission = _submissionService.GetSubmission(SubmissionId);

            //User needs to see which presenter(s) - including submitter only -  are currently on the submission
            // 1) Get the current submission participants
            var currentParticipants = _submissionParticipantService.GetSubmissionParticipants(SubmissionId).OrderBy(p => p.SortOrder).ToList();
            var currentParticipantsPretty = new List<string>();

            if (currentParticipants != null && currentParticipants.Count > 0)
            {
                //2) For each current participant, get their profile information from Salesforce
                var currentParticipantsAsUsers = _salesforceAPIService
                    .GetMultipleUserInfo(currentParticipants
                    .Select(c => c.Participant.StfmUserId)).Result
                    .ToList();

                // 3) Combine each participant and his/her name information into a nicely formatted string
                currentParticipantsPretty = 
                    currentParticipants.Select(cp => $"{currentParticipantsAsUsers.FirstOrDefault(cpu => cpu.STFMUserId == cp.Participant.StfmUserId).GetFullNamePretty()} - {string.Join(", ", cp.GetRolesAsStringPretty())}").ToList();
            }

            //4) Check to see if the current user has already been added as a participant to the submission and set a boolean flag accordingly
            var user = _userMapper.Map(User.Claims);
            var submissionParticipantExists = currentParticipants.Any(c => c.Participant.StfmUserId == user.STFMUserId);

            var model = new AddParticipantViewModel
            {
                SubmissionId                = SubmissionId,
                ConferenceTitle             = submission.Conference.ConferenceLongName,
                SubmissionTitle             = submission.SubmissionTitle,
                SubmittedCategory           = submission.SubmissionCategory.SubmissionCategoryName,
                AcceptedCategory            = submission.AcceptedCategory.SubmissionCategoryName,
                CurrentParticipants         = currentParticipantsPretty,
                SubmissionParticipantExists = submissionParticipantExists,
                Role                        = -1,
                ParticipantRoleDefinitions = _participantService.GetAddParticipantRoleDefinitions(),

            };
            return View(model);
        }

        /// <summary>
        /// Responds to a user's form submit request adding himself as a presenter to a submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Redirects to view submission or to complete disclosure process</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddParticipant(AddParticipantViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userMapper.Map(User.Claims);
                var participant = _participantService.GetOrCreateParticipant(user.STFMUserId);

                //Do another check that the user is not already a participant on the submission to handle multiple post requests done by mistake 
                var currentParticipants = _submissionParticipantService.GetSubmissionParticipants(model.SubmissionId).OrderBy(p => p.SortOrder).ToList();

                var submissionParticipantExists = currentParticipants.Any(c => c.Participant.StfmUserId == user.STFMUserId);

                if (submissionParticipantExists)
                {

                    return RedirectToAction("ViewSubmission", "Submission", new { submissionId = model.SubmissionId });

                }

                _submissionParticipantService.AddSubissionParticipant(participant.ParticipantId, (ParticipantRoleType)model.Role, model.SubmissionId);


                Log.Information($"User with STFMUserId {_userMapper.Map(User.Claims).STFMUserId} added himself as {_participantService.GetParticipantRole((ParticipantRoleType)model.Role).ParticipantRoleName} to the submission with id {model.SubmissionId}");

                Submission submission = _submissionService.GetSubmission(model.SubmissionId);

                //Create DTO needed for sending email nofifying submitter and main presenter that 
                //a new participant was added to the submission
                SubmissionParticipantAdded submissionParticipantAdded = new SubmissionParticipantAdded
                {

                    ParticipantAdded = user,
                    ConferenceTitle = submission.Conference.ConferenceLongName,
                    SubmissionCategory = submission.SubmissionCategory.SubmissionCategoryName,
                    SubmissionAcceptedCategory = submission.AcceptedCategory.SubmissionCategoryName,
                    SubmissionId = model.SubmissionId,
                    ParticipantRoleType = (ParticipantRoleType)model.Role,
                    SubmissionTitle = submission.SubmissionTitle

                };

                string submitterStfmUserId = _submissionParticipantService.GetPerson(model.SubmissionId, ParticipantRoleType.SUBMITTER);

                submissionParticipantAdded.SubmitterEmailAddress = getEmailAddress(submitterStfmUserId);

                string mainPresenterStfmUserId = _submissionParticipantService.GetPerson(model.SubmissionId, ParticipantRoleType.LEAD_PRESENTER);

                if ( mainPresenterStfmUserId != null) {

                    submissionParticipantAdded.MainPresenterEmailAddress = getEmailAddress(mainPresenterStfmUserId);

                }

                _emailNotificationService.SendParticipantAddedEmail(submissionParticipantAdded);



                if (_disclosureService.GetDisclosure(user.STFMUserId) == null)
                {

                    Log.Information("STFM User id " + user.STFMUserId + " does not have a disclosure.  So must complete a new disclosure.");

                    return RedirectToAction("Index", "Disclosure", routeValues: new { model.SubmissionId });


                }
                else if (!_disclosureService.isDisclosureCurrent(submission.Conference.ConferenceStartDate, user.STFMUserId))
                {

                    Log.Information("STFM User Id " + user.STFMUserId + " needs to update the disclosure.");

                    return RedirectToAction("EditDisclosure", "Disclosure", routeValues: new { model.SubmissionId });

                }
                


                return RedirectToAction("ViewSubmission", "Submission", new { submissionId = model.SubmissionId });
            }

            //If we're here, something's wrong.
            Log.Error($"A user with an Id of {_userMapper.Map(User.Claims).STFMUserId} was unable to add himself as a presenter to the submission with an Id of: {model.SubmissionId}");
            return RedirectToAction("Error", "Home");
        }

        private string getEmailAddress(string submitterStfmUserId)
        {

            User user = _salesforceAPIService.GetUserInfo(submitterStfmUserId).Result;

            return user.Email;


        }

        /// <summary>
        /// Responds to initial request to reorder the presenters
        /// for a specific submission.
        /// </summary>
        /// <returns>A instance of the UpdatePresenterOrderViewModel</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public ActionResult UpdatePresenterOrder(int submissionId)
        {
            Log.Information($"Request made to update presenter order for submission id {submissionId}");

            var submission = _submissionService.GetSubmission(submissionId);
            var model = _submissionParticipantService.GetUpdatePresenterOrderViewModel(submission);

            return View(model);
        }


        /// <summary>
        /// Responds to the request to reorder the presenters (in the database) for a specific submission
        /// </summary>
        /// <returns>A instance of the UpdatePresenterOrderViewModel</returns>
        ///<param name="model">Update presenter order view model</param>
        [HttpPost]
        public IActionResult UpdatePresenterOrder(UpdatePresenterOrderViewModel model)
        {
            Log.Information($"User has reordered presenters and is requesting this new presenter order to be updated in the database for submission id {model.SubmissionId}.");
            if (ModelState.IsValid)
            {
                _submissionParticipantService.UpdatePresenterOrder(model);
                _submissionService.SetParticipantOrderModifiedToTrue(model.SubmissionId);
                return RedirectToAction("Index", new { submissionId = model.SubmissionId });
            }
            return View(model);
        }

        public IActionResult AssignLeadPresenterRole(int submissionParticipantId, int submissionId)
        {
            Log.Information($"A request has been made to assign a submission participant (submissionParticipantId: {submissionParticipantId}) the lead presenter role on a submission (submissionId: {submissionId}).");
            _submissionParticipantService.AssignLeadPresenterRole(submissionId, submissionParticipantId);
            return RedirectToAction("index", new { submissionId = submissionId});
        }

    }
}
