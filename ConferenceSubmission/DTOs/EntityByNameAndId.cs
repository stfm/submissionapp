﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs
{
    public class EntityByNameAndId
    {
        public int EntityId { get; set; }

        public string EntityName { get; set; }
    }
}
