﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.BindingModels;

namespace ConferenceSubmission.Data
{
    public interface IVirtualConferenceSessionRepository
    {
        /// <summary>
        /// Gets the Virtual Conference Session from the Database that matches the passed in VirtualConferenceSessionId
        /// </summary>
        /// <param name="virtualConferenceSessionId">Id of the Virtual Conference Sessions</param>
        /// <returns>VirtualConferenceSession</returns>
        VirtualConferenceSession GetWithSubmissionData(int virtualConferenceSessionId);

        /// <summary>
        /// Updates a Virtual Conference Sessions
        /// </summary>
        /// <param name="virtualSessionDataBindingModel">Model class that contains the updated Virtual Conference Session data</param>
        void Update(VirtualSessionDataBindingModel virtualSessionDataBindingModel);

        /// <summary>
        /// Creates a new Virtual Conference Session Record
        /// </summary>
        /// <param name="virtualSessionDataBindingModel">Model class that contains the Virtual Conference Sessions data</param>
        /// <returns>Id of the created Virtual Conference Session</returns>
        int Create(VirtualSessionDataBindingModel virtualSessionDataBindingModel);
    }

    public class VirtualConferenceSessionRepository : IVirtualConferenceSessionRepository
    {
        private readonly AppDbContext _context;
        public VirtualConferenceSessionRepository(AppDbContext context)
        {
            _context = context;
        }

        public int Create(VirtualSessionDataBindingModel virtualSessionDataBindingModel)
        {
            var virtualConferenceSessionToAdd = new VirtualConferenceSession
            {
                SubmissionId = virtualSessionDataBindingModel.SubmissionId,
                SessionCode = virtualSessionDataBindingModel.SessionCode,
                SessionStartDateTime = virtualSessionDataBindingModel.SessionStartDateTime,
                SessionEndDateTime = virtualSessionDataBindingModel.SessionEndDateTime,
                SessionTrack = virtualSessionDataBindingModel.SessionTrack,
                SubmissionPresentationMethodId = virtualSessionDataBindingModel.SubmissionPresentationMethodId
            };

            _context.VirtualConferenceSession.Add(virtualConferenceSessionToAdd);

            _context.SaveChanges();

            return virtualConferenceSessionToAdd.VirtualConferenceSessionId;
        }

        public VirtualConferenceSession GetWithSubmissionData(int virtualConferenceSessionId)
        {
            var virtualConferenceSession = _context.VirtualConferenceSession
                .Include(x => x.SubmissionPresentationMethod)
                    .ThenInclude(x => x.PresentationMethod)
                .Include(x => x.SubmissionPresentationMethod)
                    .ThenInclude(x => x.PresentationStatus)
                .Include(x => x.Submission)
                    .ThenInclude(x => x.Conference)
                .Include(x => x.Submission)
                    .ThenInclude(x => x.AcceptedCategory)
                .FirstOrDefault(x => x.VirtualConferenceSessionId == virtualConferenceSessionId);

            return virtualConferenceSession;
        }

        public void Update(VirtualSessionDataBindingModel virtualSessionDataBindingModel)
        {
            var virtualConferenceSessionToUpdate = _context.VirtualConferenceSession.FirstOrDefault(x => x.VirtualConferenceSessionId == virtualSessionDataBindingModel.VirtualConferenceSessionId);

            if (virtualConferenceSessionToUpdate != null)
            {
                virtualConferenceSessionToUpdate.SessionCode = virtualSessionDataBindingModel.SessionCode;
                virtualConferenceSessionToUpdate.SessionStartDateTime = virtualSessionDataBindingModel.SessionStartDateTime;
                virtualConferenceSessionToUpdate.SessionEndDateTime = virtualSessionDataBindingModel.SessionEndDateTime;
                virtualConferenceSessionToUpdate.SessionTrack = virtualSessionDataBindingModel.SessionTrack;

                _context.SaveChanges();
            }
        }
    }
}
