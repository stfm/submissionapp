﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs.VirtualConference
{
    public class VirtualPresentation
    {
        public int                  SubmissionId          { get; set; }
        public string               Title                 { get; set; }
        public string               Abstract              { get; set; }
        public string               Presenters            { get; set; }
        public PresenterToNotify    PresenterToNotify     { get; set; }

        public string VideoLink { get; set; }   

        public string Handout { get; set; }

        public string PosterImageLink { get; set; }

        public string SubmissionCategory { get; set; }

        public int VirtualConferenceSessionID { get; set; }

        public string KeyWords { get; set; }

        public string PresentationType { get; set; }

        public String SessionCode { get; set; }
    }

    public class PresenterToNotify
    {
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}
