﻿using System.ComponentModel;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Models a submission status type.
    /// </summary>
    public enum SubmissionStatusType
    {
        [Description("Open")]
        OPEN,

        [Description("Pending Review")]
        PENDING_REVIEW,

        [Description("Accepted")]
        ACCEPTED,

        [Description("Not Accepted")]
        DECLINED,

        [Description("Withdrawn")]
        WITHDRAWN,

        [Description("Canceled")]
        CANCELED,

        [Description("Will Present In August")]
        WILL_PRESENT_IN_AUGUST,

        [Description("Pending")]
        PENDING,

        [Description("Will Present At Virtual Conference")]
        WILL_PRESENT_AT_VIRTUAL_CONFERENCE

    }


}



