﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.BindingModels
{
    public class DeleteAnnouncementModel
    {
        public int AnnouncementId { get; set; }
    }
}
