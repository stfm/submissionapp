﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.BindingModels
{
    public class EditAcceptedSubmissionBindingModel
    {

        [Required]
        public int SubmissionId { get; set; }

        [Required(ErrorMessage = "The submission title is required")]
        [Display(Name = "Title")]
        public string SubmissionTitle { get; set; }

    }

   
}
