﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ConferenceSubmission.DTOs
{
    public class SessionWithConferenceDetails : Session
    {
        public SessionWithConferenceDetails()
        {

        }
        public SessionWithConferenceDetails(Session session)
        {
            Abstract     = session.Abstract;
            Title        = session.Title;
            Category     = session.Category;
            SessionCode  = session.SessionCode;
            SessionId    = session.SessionId;
            StartTime    = session.StartTime;
            EndTime      = session.EndTime;
            Participants = session.Participants;
            Room         = session.Room;
        }
        public int      ConferenceId { get; set; }
        public string   ConferenceName { get; set; }
        public string   ConferenceYear { get; set; }
    }
}
