﻿using ConferenceSubmission.Data;
using ConferenceSubmission.DTOs.PosterHall;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class PosterHallServiceTest
    {
        [Fact]
        public void GetSubmissionsForPosterHallCategory()
        {
            //arrange
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForPosterHallCategory(It.IsAny<int>()))
                .Returns(() => new List<PosterDTO>
                {
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234HHAF00",
                        MaterialType = "poster",
                        MaterialValue = "poster.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234HHAF00",
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc-submission1"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAHU84HHAF98",
                        MaterialType = "poster",
                        MaterialValue = "poster.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAHU84HHAF98",
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc-submission1"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234BHAF34",
                        MaterialType = "poster",
                        MaterialValue = "poster.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234BHAF34",
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc-submission1"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 2,
                        SubmissionTitle = "Test Submission 2 Title",
                        SubmissionAbstract = "Test Submission 2 Abstract",
                        SessionCode = "P02",
                        STFMUserId = "OOAH29P2HAF03",
                        MaterialType = "poster",
                        MaterialValue = "poster2.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 2,
                        SubmissionTitle = "Test Submission 2 Title",
                        SubmissionAbstract = "Test Submission 2 Abstract",
                        SessionCode = "P02",
                        STFMUserId = "OOAH29P2HAF03",
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc-submission2"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 2,
                        SubmissionTitle = "Test Submission 2 Title",
                        SubmissionAbstract = "Test Submission 2 Abstract",
                        SessionCode = "P02",
                        STFMUserId = "OOPK184HHAF38",
                        MaterialType = "poster",
                        MaterialValue = "poster2.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 2,
                        SubmissionTitle = "Test Submission 2 Title",
                        SubmissionAbstract = "Test Submission 2 Abstract",
                        SessionCode = "P02",
                        STFMUserId = "OOPK184HHAF38",
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc-submission2"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 3,
                        SubmissionTitle = "Test Submission 3 Title",
                        SubmissionAbstract = "Test Submission 3 Abstract",
                        SessionCode = "P03",
                        STFMUserId = "OOAPL81BHAF37",
                        MaterialType = "poster",
                        MaterialValue = "poster3.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 3,
                        SubmissionTitle = "Test Submission 3 Title",
                        SubmissionAbstract = "Test Submission 3 Abstract",
                        SessionCode = "P03",
                        STFMUserId = "OOAPL81BHAF37",
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc-submission3"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 3,
                        SubmissionTitle = "Test Submission 3 Title",
                        SubmissionAbstract = "Test Submission 3 Abstract",
                        SessionCode = "P03",
                        STFMUserId = "OOAPL81BHAF37",
                        MaterialType = "handout",
                        MaterialValue = "poster3Handout.pdf"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 4,
                        SubmissionTitle = "Test Submission 4 Title",
                        SubmissionAbstract = "Test Submission 4 Abstract",
                        SessionCode = "P04",
                        STFMUserId = "OOA2H81BH8UI8",
                        MaterialType = "poster",
                        MaterialValue = "poster4.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 4,
                        SubmissionTitle = "Test Submission 4 Title",
                        SubmissionAbstract = "Test Submission 4 Abstract",
                        SessionCode = "P04",
                        STFMUserId = "OOA2H81BH8UI8",
                        MaterialType = "handout",
                        MaterialValue = "poster4Handout.pdf"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 4,
                        SubmissionTitle = "Test Submission 4 Title",
                        SubmissionAbstract = "Test Submission 4 Abstract",
                        SessionCode = "P04",
                        STFMUserId = "OOQ0E81BH844F",
                        MaterialType = "poster",
                        MaterialValue = "poster4.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 4,
                        SubmissionTitle = "Test Submission 4 Title",
                        SubmissionAbstract = "Test Submission 4 Abstract",
                        SessionCode = "P04",
                        STFMUserId = "OOQ0E81BH844F",
                        MaterialType = "handout",
                        MaterialValue = "poster4Handout.pdf"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 4,
                        SubmissionTitle = "Test Submission 4 Title",
                        SubmissionAbstract = "Test Submission 4 Abstract",
                        SessionCode = "P04",
                        STFMUserId = "OOA2UY23HG1I8",
                        MaterialType = "poster",
                        MaterialValue = "poster4.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 4,
                        SubmissionTitle = "Test Submission 4 Title",
                        SubmissionAbstract = "Test Submission 4 Abstract",
                        SessionCode = "P04",
                        STFMUserId = "OOA2UY23HG1I8",
                        MaterialType = "handout",
                        MaterialValue = "poster4Handout.pdf"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 4,
                        SubmissionTitle = "Test Submission 4 Title",
                        SubmissionAbstract = "Test Submission 4 Abstract",
                        SessionCode = "P04",
                        STFMUserId = "OOA2HPO8UH5WH",
                        MaterialType = "poster",
                        MaterialValue = "poster4.png"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 4,
                        SubmissionTitle = "Test Submission 4 Title",
                        SubmissionAbstract = "Test Submission 4 Abstract",
                        SessionCode = "P04",
                        STFMUserId = "OOA2HPO8UH5WH",
                        MaterialType = "handout",
                        MaterialValue = "poster4Handout.pdf"
                    },
                    new PosterDTO
                    {
                        SubmissionId = 5,
                        SubmissionTitle = "Test Submission 5 Title",
                        SubmissionAbstract = "Test Submission 5 Abstract",
                        SessionCode = "P05",
                        STFMUserId = "OOA2HPO8UH5WH",
                        MaterialType = "link",
                        MaterialValue = "https://www.youtube.com/embed/20mBi8-QsSc-submission3"
                    },
                });

            var mockSalesForceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesForceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).ReturnsAsync(() => new List<User>
            {
                new User
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    STFMUserId = "OOAH234HHAF00"
                },
                new User
                {
                    FirstName = "Frank",
                    LastName = "Jones",
                    Credentials = "DDO, MD",
                    STFMUserId = "OOAHU84HHAF98"
                },
                new User
                {
                    FirstName = "William",
                    LastName = "Smith",
                    Credentials = "MD",
                    STFMUserId = "OOAH234BHAF34"
                },
                new User
                {
                    FirstName = "Jennifer",
                    LastName = "Dodge",
                    Credentials = "MD, PhD",
                    STFMUserId = "OOAH29P2HAF03"
                },
                new User
                {
                    FirstName = "Sarah",
                    LastName = "Evans",
                    Credentials = "MD",
                    STFMUserId = "OOPK184HHAF38"
                },
                new User
                {
                    FirstName = "Tristan",
                    LastName = "Reigh",
                    Credentials = "MD, PhD, JD",
                    STFMUserId = "OOAPL81BHAF37"
                },
                new User
                {
                    FirstName = "Sally",
                    LastName = "Peterson",
                    Credentials = "MD",
                    STFMUserId = "OOA2H81BH8UI8"
                },
                new User
                {
                    FirstName = "Jake",
                    LastName = "Keller",
                    Credentials = "PhD",
                    STFMUserId = "OOQ0E81BH844F"
                },
                new User
                {
                    FirstName = "Stewart",
                    LastName = "Johnson",
                    Credentials = "MD, PhD",
                    STFMUserId = "OOA2UY23HG1I8"
                },
                new User
                {
                    FirstName = "Allison",
                    LastName = "Quinn",
                    Credentials = "JD",
                    STFMUserId = "OOA2HPO8UH5WH"
                }
            });

            var posterHallService = new PosterHallService(mockSubmissionRepository.Object, mockSalesForceAPIService.Object);

            //act
            var results = posterHallService.GetSubmissionsForPosterHallCategory(1);

            //assert
            Assert.Equal(4, results.Count);

            Assert.Equal("John Doe, MD; Frank Jones, DDO, MD; William Smith, MD", results.FirstOrDefault(x => x.SubmissionId == 1).Presenters);
            Assert.Equal("Jennifer Dodge, MD, PhD; Sarah Evans, MD", results.FirstOrDefault(x => x.SubmissionId == 2).Presenters);
            Assert.Equal("Tristan Reigh, MD, PhD, JD", results.FirstOrDefault(x => x.SubmissionId == 3).Presenters);
            Assert.Equal("Sally Peterson, MD; Jake Keller, PhD; Stewart Johnson, MD, PhD; Allison Quinn, JD", results.FirstOrDefault(x => x.SubmissionId == 4).Presenters);

            Assert.Equal("poster.png", results.FirstOrDefault(x => x.SubmissionId == 1).PosterImageFileName);
            Assert.Equal("poster2.png", results.FirstOrDefault(x => x.SubmissionId == 2).PosterImageFileName);
            Assert.Equal("poster3.png", results.FirstOrDefault(x => x.SubmissionId == 3).PosterImageFileName);
            Assert.Equal("poster4.png", results.FirstOrDefault(x => x.SubmissionId == 4).PosterImageFileName);

            Assert.Equal("", results.FirstOrDefault(x => x.SubmissionId == 1).HandoutFileName);
            Assert.Equal("", results.FirstOrDefault(x => x.SubmissionId == 2).HandoutFileName);
            Assert.Equal("poster3Handout.pdf", results.FirstOrDefault(x => x.SubmissionId == 3).HandoutFileName);
            Assert.Equal("poster4Handout.pdf", results.FirstOrDefault(x => x.SubmissionId == 4).HandoutFileName);

            Assert.Equal("https://www.youtube.com/embed/20mBi8-QsSc-submission1", results.FirstOrDefault(x => x.SubmissionId == 1).YouTubeEmbedLink);
            Assert.Equal("https://www.youtube.com/embed/20mBi8-QsSc-submission2", results.FirstOrDefault(x => x.SubmissionId == 2).YouTubeEmbedLink);
            Assert.Equal("https://www.youtube.com/embed/20mBi8-QsSc-submission3", results.FirstOrDefault(x => x.SubmissionId == 3).YouTubeEmbedLink);
            Assert.Equal("", results.FirstOrDefault(x => x.SubmissionId == 4).YouTubeEmbedLink);

            //For submission id = 5, the there is a YouTube link but  NOT an image. We only create posters for submissions with an uploaded image.
            Assert.DoesNotContain(results, r => r.SubmissionId == 5);
        }

        [Fact]
        public void GetSubmissionsForPosterHallCategory_StubASubmissionWithALeadPresenterAndAPresenter_VerifyLeadPresenterEmailReturned()
        {
            //arrange
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForPosterHallCategory(It.IsAny<int>()))
                .Returns(() => new List<PosterDTO>
                {
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234HHAF00",
                        MaterialType = "poster",
                        MaterialValue = "poster.png",
                        ParticipantRoleType = ParticipantRoleType.PRESENTER
                    },
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234HHAH6F7",
                        MaterialType = "poster",
                        MaterialValue = "poster.png",
                        ParticipantRoleType = ParticipantRoleType.LEAD_PRESENTER
                    },
                });

            var mockSalesForceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesForceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).ReturnsAsync(() => new List<User>
            {
                new User
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    STFMUserId = "OOAH234HHAF00",
                    Email = "johndoe@email.com"
                },
                new User
                {
                    FirstName = "Frank",
                    LastName = "Jones",
                    Credentials = "DDO, MD",
                    STFMUserId = "OOAH234HHAH6F7",
                    Email = "frankjones@email.com"
                }                
            });

            var posterHallService = new PosterHallService(mockSubmissionRepository.Object, mockSalesForceAPIService.Object);

            //act
            var results = posterHallService.GetSubmissionsForPosterHallCategory(1);

            //assert
            Assert.Single(results);

            Assert.Equal("frankjones@email.com", results.First().PresenterToNotify.Email);
            Assert.Equal("Frank Jones, DDO, MD", results.First().PresenterToNotify.FullName);
        }

        [Fact]
        public void GetSubmissionsForPosterHallCategory_StubASubmissionWithNoLeadPresenterAndAPresenterAndASubmitter_VerifyPresenterEmailReturned()
        {
            //arrange
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForPosterHallCategory(It.IsAny<int>()))
                .Returns(() => new List<PosterDTO>
                {
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234HHAF00",
                        MaterialType = "poster",
                        MaterialValue = "poster.png",
                        ParticipantRoleType = ParticipantRoleType.SUBMITTER
                    },
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234HHAH6F7",
                        MaterialType = "poster",
                        MaterialValue = "poster.png",
                        ParticipantRoleType = ParticipantRoleType.PRESENTER
                    },
                });

            var mockSalesForceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesForceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).ReturnsAsync(() => new List<User>
            {
                new User
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    STFMUserId = "OOAH234HHAF00",
                    Email = "johndoe@email.com"
                },
                new User
                {
                    FirstName = "Frank",
                    LastName = "Jones",
                    Credentials = "DDO, MD",
                    STFMUserId = "OOAH234HHAH6F7",
                    Email = "frankjones@email.com"
                }
            });

            var posterHallService = new PosterHallService(mockSubmissionRepository.Object, mockSalesForceAPIService.Object);

            //act
            var results = posterHallService.GetSubmissionsForPosterHallCategory(1);

            //assert
            Assert.Single(results);

            Assert.Equal("frankjones@email.com", results.First().PresenterToNotify.Email);
            Assert.Equal("Frank Jones, DDO, MD", results.First().PresenterToNotify.FullName);
        }

        [Fact]
        public void GetSubmissionsForPosterHallCategory_StubASubmissionWithNoLeadPresenterAndNoPresenterAndASubmitter_VerifySubmitterEmailReturned()
        {
            //arrange
            var mockSubmissionRepository = new Mock<ISubmissionRepository>();

            mockSubmissionRepository.Setup(x => x.GetSubmissionsForPosterHallCategory(It.IsAny<int>()))
                .Returns(() => new List<PosterDTO>
                {
                    new PosterDTO
                    {
                        SubmissionId = 1,
                        SubmissionTitle = "Test Submission 1 Title",
                        SubmissionAbstract = "Test Submission 1 Abstract",
                        SessionCode = "P01",
                        STFMUserId = "OOAH234HHAF00",
                        MaterialType = "poster",
                        MaterialValue = "poster.png",
                        ParticipantRoleType = ParticipantRoleType.SUBMITTER
                    }
                });

            var mockSalesForceAPIService = new Mock<ISalesforceAPIService>();
            mockSalesForceAPIService.Setup(x => x.GetMultipleUserInfo(It.IsAny<IEnumerable<string>>())).ReturnsAsync(() => new List<User>
            {
                new User
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Credentials = "MD",
                    STFMUserId = "OOAH234HHAF00",
                    Email = "johndoe@email.com"
                }
            });

            var posterHallService = new PosterHallService(mockSubmissionRepository.Object, mockSalesForceAPIService.Object);

            //act
            var results = posterHallService.GetSubmissionsForPosterHallCategory(1);

            //assert
            Assert.Single(results);

            Assert.Equal("johndoe@email.com", results.First().PresenterToNotify.Email);
            Assert.Equal("John Doe, MD", results.First().PresenterToNotify.FullName);
        }
    }
}
