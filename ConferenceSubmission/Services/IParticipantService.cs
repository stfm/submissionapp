﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ConferenceSubmission.Services
{
    public interface IParticipantService
    {

        List<Participant> GetParticipants();

        /// <summary>
        /// This method fetches a participant by STFMUserId. If a participant is not found,
        /// it creates a new Participant and returns it
        /// </summary>
        /// <param name="stfmUserId"></param>
        /// <returns></returns>
        Participant GetOrCreateParticipant(string stfmUserId);

		/// <summary>
        /// Get a specific Participant using the provided StfmUserId.
        /// </summary>
        /// <returns>The Participant by stfm user identifier.</returns>
        /// <param name="StfmUserId">Stfm user identifier.</param>
        Participant GetParticipantByStfmUserId(string StfmUserId);

        /// <summary>
        /// Creates the Participant.
        /// </summary>
        /// <returns>The participant id</returns>
        /// <param name="participant">Participant.</param>
        int CreateParticipant(Participant participant);

		/// <summary>
        /// Get a specific participant role using the provided
        /// ParticipantRoleName
        /// </summary>
        /// <returns>ParticipantRole object</returns>
        /// <param name="participantRoleName">Participant role name.</param>
        ParticipantRole GetParticipantRole(ParticipantRoleType participantRoleName);

        /// <summary>
        /// Gets the participant roles based on the provided submission role.
        /// </summary>
        /// <returns>The participant roles.</returns>
        /// <param name="submissionRole">Submission role.</param>
		List<ParticipantRole> GetParticipantRoles(string submissionRole);

        /// <summary>
        /// Gets the add participant URL.
        /// </summary>
        /// <returns>The add participant URL.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="host">The web app host</param>
        string GetAddParticipantUrl(string submissionId, string host);

        /// <summary>
        /// Gets the participant by participant identifier.
        /// </summary>
        /// <returns>The participant by participant identifier.</returns>
        /// <param name="participantId">Participant identifier.</param>
        Participant GetParticipantByParticipantId(int participantId);

        /// <summary>
        /// Based on the current particpant roles assigned to a particular participant for a particular submission,
        /// returns an AssignedParticipantRolesType object
        /// </summary>
        /// <param name="submissionId">Unique Identifier of Submission</param>
        /// <param name="submissionParticipantId">Unique Identifier of Submission Participant</param>
        /// <returns></returns>
        AssignedParticipantRolesType GetAssignedParticipantRolesType(int submissionId, int submissionParticipantId);

        /// <summary>
        /// Gets all the roles assigned to a participant for a particular submission.
        /// </summary>
        /// <returns>The roles assigned to the participant for the submission.</returns>
        /// <param name="submissionId">Unique Identifier of submission.</param>
        ///  /// <param name="stfmUserId">Salesforce ID of participant.</param>
        List<ParticipantRoleType> GetParticipantsRolesBySubmission(int submissionId, string stfmUserId);

        /// <summary>
        /// Gets Participant Roles by name with defintion.
        /// </summary>
        /// <returns>List of strings</returns>
        List<string> GetParticipantRoleDefinitions();

        /// <summary>
        /// Gets Add Participant Roles by name with defintion.
        /// </summary>
        /// <returns>List of strings</returns>
        List<string> GetAddParticipantRoleDefinitions();
    }
}
