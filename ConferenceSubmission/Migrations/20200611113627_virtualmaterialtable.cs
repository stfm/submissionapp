﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class virtualmaterialtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VirtualMaterials",
                columns: table => new
                {
                    VirtualMaterialId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SubmissionId = table.Column<int>(nullable: false),
                    MaterialType = table.Column<string>(maxLength: 250, nullable: false),
                    MaterialValue = table.Column<string>(maxLength: 500, nullable: false),
                    PersonUploaded = table.Column<string>(maxLength: 50, nullable: false),
                    UploadedDateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VirtualMaterials", x => x.VirtualMaterialId);
                    table.ForeignKey(
                        name: "FK_VirtualMaterials_Submissions_SubmissionId",
                        column: x => x.SubmissionId,
                        principalTable: "Submissions",
                        principalColumn: "SubmissionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VirtualMaterials_SubmissionId",
                table: "VirtualMaterials",
                column: "SubmissionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VirtualMaterials");
        }
    }
}
