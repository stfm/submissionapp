﻿using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class HomePageViewModel
    {

        public HomePageViewModel()
        {
            SubmissionSummaryViewModels = new List<SubmissionSummaryViewModel>();
            ActiveConferenceViewModels  = new List<ConferenceViewModel>();
            Announcements               = new List<Announcement>();
        }

        //public properties
        public User                             User                        { get; set; }
        public List<SubmissionSummaryViewModel> SubmissionSummaryViewModels { get; set; }
        public List<ConferenceViewModel>        ActiveConferenceViewModels  { get; set; }
        public List<Announcement>               Announcements               { get; set; }
        public bool isDisclosureCurrent { get; set; }
    }
}
