﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Models;


namespace ConferenceSubmission.Data
{
    public interface IConferenceRepository
    {
        /// <summary>
        /// Create a new Confeence in the repository using the data provided by the 
        /// Conference object
        /// </summary>
        /// <param name="conference">A Conference</param>
        /// <returns>conference id of new conference</returns>
        int CreateConference(Conference conference);

        /// <summary>
        /// Get all ConferenceType objects from the data repository.
        /// </summary>
        /// <value>The get conference types.</value>
        IEnumerable<ConferenceType> GetConferenceTypes { get; }

        /// <summary>
        /// Get a specific ConferenceType using the provided ConferenceTypeId
        /// </summary>
        /// <returns>A single ConferenceType</returns>
        /// <param name="conferenceTypeId">ConferenceTypeId</param>
        ConferenceType GetConferenceType(int conferenceTypeId);

        /// <summary>
        /// Get a specific ConferenceType using the provided ConferenceTypeName
        /// </summary>
        /// <returns>A single ConferenceType</returns>
        /// <param name="conferenceTypeName">ConferenceTypeName</param>
        ConferenceType GetConferenceType(string conferenceTypeName);

        /// <summary>
        /// Get all Conferences.
        /// </summary>
        /// <value>Collection of Conference objects</value>
        IEnumerable<Conference> GetConferences { get; }

        /// <summary>
        /// Get a specific conference using the provided ConferenceId.
        /// </summary>
        /// <returns>Conference object</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        Conference GetConference(int conferenceId);

        /// <summary>
        /// Get specific conference using the provided ConferenceShortName.
        /// </summary>
        /// <returns>Conference object</returns>
        /// <param name="conferenceShortName">Conference short name.</param>
        Conference GetConference(string conferenceShortName);


        /// <summary>
        /// Gets the active conferences.
        /// </summary>
        /// <returns>The active conferences.</returns>
        IEnumerable<Conference> GetActiveConferences();

        /// <summary>
        /// Gets the conferences where the current datetime is less than
        /// or equal to the conference's ConferenceSubmissionReviewEndDate.
        /// Note: the expectation is that the ConferenceSubmissionReviewEndDate field
        /// will include the time component. For example: 2018-2-12 23:59:49
        /// </summary>
        /// <returns>Collection of conference objects.</returns>
        IEnumerable<Conference> GetConferencesWithOpenReviewStatus();

        /// <summary>
        /// Gets the conferences where the current datetime is less than
        /// or equal to the conference's ConfernceStartDate.
        /// </summary>
        /// <returns>Collection of conference objects.</returns>
        IEnumerable<Conference> GetConferencesWithUnstartedStatus();
      
        /// <summary>
        /// Retire the conference identified by the conference ID 
        /// provided.  This means to set the conferencedeleted
        /// column value to 1.  
        /// </summary>
        /// <param name="conferenceId">Conference ID</param>
        void RetireConference(int conferenceId);

        /// <summary>
        /// Toggle the value of the boolean ConferenceInactive property.
        /// </summary>
        /// <param name="conferenceId">Conference identifier</param>
        bool ToggleConferenceInactive(int conferenceId);

        /// <summary>
        /// Updates the value of the conference's CFP EndDate
        /// </summary>
        /// <param name="conferenceId"></param>
        DateTime EditCFPEndDate(int conferenceId, DateTime endDate);

        /// <summary>
        /// Updates the value of the conference's review deadline
        /// </summary>
        /// <param name="conferenceId"></param>
        DateTime EditSubmissionReviewEndDate(int conferenceId, DateTime endDate);
        
        /// <summary>
        /// Get active CERA conferences.
        /// </summary>
        /// <returns>Collection of conference objects</returns>
        IEnumerable<Conference> GetActiveCeraConferences();

        /// <summary>
        /// Returns the name of a confernece, given its Id    
        /// </summary>
        /// <param name="conferenceId">Id of Conference</param>
        /// <returns></returns>
        string GetConferenceName(int conferenceId);

        /// <summary>
        /// Returns the conference whose start date falls withing the specified date range
        /// </summary>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">Start Date</param>
        /// <returns>Collection of Conferences</returns>
        IEnumerable<EntityByNameAndId> GetConferencesByDateRange(DateTime startDate, DateTime endDate);
    }


}
