﻿using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class ReviewStatusRepositoryTest
    {
        [Fact]
        public void GetReviewStatus_NOT_STARTED()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            ReviewStatus reviewStatus;
            using (var context = new AppDbContext(options))
            {
                var reviewStatusRepository = new ReviewStatusRepository(context);
                reviewStatus = reviewStatusRepository.GetReviewStatusByReviewStatusType(ReviewStatusType.NOT_STARTED);
            }

            Assert.Equal(1, reviewStatus.ReviewStatusId);
        }

        [Fact]
        public void GetReviewStatus_INCOMPLETE()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            ReviewStatus reviewStatus;
            using (var context = new AppDbContext(options))
            {
                var reviewStatusRepository = new ReviewStatusRepository(context);
                reviewStatus = reviewStatusRepository.GetReviewStatusByReviewStatusType(ReviewStatusType.INCOMPLETE);
            }

            Assert.Equal(2, reviewStatus.ReviewStatusId);
        }

        [Fact]
        public void GetReviewStatus_COMPLETE()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            ReviewStatus reviewStatus;
            using (var context = new AppDbContext(options))
            {
                var reviewStatusRepository = new ReviewStatusRepository(context);
                reviewStatus = reviewStatusRepository.GetReviewStatusByReviewStatusType(ReviewStatusType.COMPLETE);
            }

            Assert.Equal(3, reviewStatus.ReviewStatusId);
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();

                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 1, ReviewStatusType = ReviewStatusType.NOT_STARTED });
                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 2, ReviewStatusType = ReviewStatusType.INCOMPLETE });
                context.ReviewStatuses.Add(new ReviewStatus() { ReviewStatusId = 3, ReviewStatusType = ReviewStatusType.COMPLETE });
  

                context.SaveChanges();
            }

            return options;
        }
    }
}
