﻿namespace ConferenceSubmission.ViewModels
{
    public class SubmissionReviewStatus
    {

        public SubmissionReviewStatus() { }

        public int SubmissionId { get; set; }

        public int ReviewsAssigned { get; set; }

        public int ReviewsCompleted{ get; set;}

        public int ReviewsNotCompleted { get; set;}


    }
}
