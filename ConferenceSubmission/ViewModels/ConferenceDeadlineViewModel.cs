﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ConferenceDeadlineViewModel
    {
        public int      ConferenceId { get; set; }
        public DateTime DeadlineDate { get; set; }
    }
}
