﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Models the Review form answers by 
    /// a specific reviewer.
    /// </summary>
    public class ReviewFormField
    {
        public ReviewFormField()
        {
        }

        public int ReviewFormFieldId { get; set; }

        public int ReviewId { get; set; }

        public int FormFieldId { get; set; }

        public FormField FormField { get; set; }

        [Required]
        public String ReviewFormFieldValue { get; set; }


    }
}
