﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ReviewerSummaryAllConferences
    {

        public List<ReviewerSummaryForConference> ReviewerSummaryForConferences { get; set; }

        public Reviewer Reviewer { get; set; }

        public string SuccessMessage { get; set; }


    }
}
