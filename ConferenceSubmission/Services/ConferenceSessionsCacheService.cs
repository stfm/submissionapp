﻿using ConferenceSubmission.Cache;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Models;
using Microsoft.Extensions.Caching.Memory;

namespace ConferenceSubmission.Services
{
    public class ConferenceSessionsCacheService : IConferenceSessionsCacheService
    {
        private readonly IMemoryCache _memoryCache;
        public ConferenceSessionsCacheService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public void ClearConferenceSessionsSearchCache(int conferenceId)
        {
            _memoryCache.Remove($"{CacheKeyType.CONFERENCE_SESSIONSSEARCH_CACHE_KEY}-{conferenceId}");
        }

        public ConferenceDTO GetConferenceDTO(int conferenceId)
        {
            if (_memoryCache.TryGetValue($"{CacheKeyType.CONFERENCE_SESSIONSSEARCH_CACHE_KEY}-{conferenceId}", out ConferenceDTO result))
                return result;

            return null;
        }

        public void SetConferenceDTO(ConferenceDTO conferenceDTO)
        {
            _memoryCache.Set($"{CacheKeyType.CONFERENCE_SESSIONSSEARCH_CACHE_KEY}-{conferenceDTO.ConferenceId}", conferenceDTO);
        }
    }
}
