﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents a specific presentation at a conference.
    /// </summary>
    [Auditable]
    public class ConferenceSession
    {
        public ConferenceSession()
        {
        }

        public int ConferenceSessionId { get; set; }

        [ParentId]
        public int SubmissionId { get; set; }

        public Submission Submission { get; set; }

        [StringLength(10)]
        [Required]
        [Auditable]
        public string SessionCode { get; set; }

        [Required]
        [Auditable]
        public DateTime SessionStartDateTime { get; set; }

        [Required]
        [Auditable]
        public DateTime SessionEndDateTime { get; set; }

        [StringLength(250)]
        [Required]
        [Auditable]
        public string SessionLocation { get; set; }

        [StringLength(250)]
        [Auditable]
        public string SessionTrack { get; set; }

        public SubmissionPresentationMethod SubmissionPresentationMethod { get; set; }

        override
        public string ToString()
        {

            return "Submission ID: " + SubmissionId + " |Session Code: " +
                SessionCode + " |Session Start DateTime: " + SessionStartDateTime +
                " |Session End DateTime: " + SessionEndDateTime +
                " |Session Location: " + SessionLocation + " |Session Track: " +
                SessionTrack;



        }

    }
}
