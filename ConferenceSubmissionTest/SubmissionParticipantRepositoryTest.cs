﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmissionTest
{
    public class SubmissionParticipantRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public SubmissionParticipantRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;

        }



        [Fact]
        public void GetSubmissionParticpantsBySubmissionIdTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);


                IEnumerable<SubmissionParticipant> submissionParticipants = submissionParticipantRepository.GetSubmissionParticipants(10001);

                foreach (SubmissionParticipant submissionParticipant in submissionParticipants)
                {


                    Output.WriteLine("SubmissionParticipantId is " + submissionParticipant.SubmissionParticipantId);

                    Output.WriteLine("Participant ID is " + submissionParticipant.ParticipantId);

                    Output.WriteLine("Submission participant's StfmUserId is " + submissionParticipant.Participant.StfmUserId);

                    foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipant.SubmissionParticipantToParticipantRolesLink)
                    {

                        Output.WriteLine("Submission participant role name is " + submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName);

                    }

                }

                Assert.Equal(2, submissionParticipants.Count());

            }
        }


        [Fact]
        public void UpdateSubmissionParticipantRolesTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);

                List<int> participantRoleIds = new List<int>();

                participantRoleIds.Add(3);

                participantRoleIds.Add(2);

                submissionParticipantRepository.UpdateSubmissionParticipantRoles(1, participantRoleIds);


                IEnumerable<SubmissionParticipant> submissionParticipants = submissionParticipantRepository.GetSubmissionParticipants(10001);


                foreach (SubmissionParticipant submissionParticipant in submissionParticipants)
                {


                    Output.WriteLine("SubmissionParticipantId is " + submissionParticipant.SubmissionParticipantId);

                    Output.WriteLine("Participant ID is " + submissionParticipant.ParticipantId);

                    Output.WriteLine("Submission participant's StfmUserId is " + submissionParticipant.Participant.StfmUserId);

                    foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipant.SubmissionParticipantToParticipantRolesLink)
                    {

                        Output.WriteLine("Submission participant role name is " + submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName);

                    }

                    if (submissionParticipant.SubmissionParticipantId == 1)
                    {

                        Assert.Equal(2, submissionParticipant.SubmissionParticipantToParticipantRolesLink.Count());

                    }

                }



            }
        }



        [Fact]
        public void CreateSubmissionParticipantTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);

                SubmissionParticipantToParticipantRole submissionParticipantToParticipantRoleToAdd = new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2 };

                List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRolesToAdd = new List<SubmissionParticipantToParticipantRole>();

                submissionParticipantToParticipantRolesToAdd.Add(submissionParticipantToParticipantRoleToAdd);

                SubmissionParticipant submissionParticipantToAdd = new SubmissionParticipant { ParticipantId = 4003, SubmissionId = 10002, SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRolesToAdd };

                submissionParticipantRepository.CreateSubmissionParticipant(submissionParticipantToAdd);

                IEnumerable<SubmissionParticipant> submissionParticipants = submissionParticipantRepository.GetSubmissionParticipants(10002);

                foreach (SubmissionParticipant submissionParticipant in submissionParticipants)
                {

                    Output.WriteLine("SubmissionParticipantId is " + submissionParticipant.SubmissionParticipantId);

                    Output.WriteLine("Participant ID is " + submissionParticipant.ParticipantId);

                    Output.WriteLine("Submission participant's StfmUserId is " + submissionParticipant.Participant.StfmUserId);

                    foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipant.SubmissionParticipantToParticipantRolesLink)
                    {

                        Output.WriteLine("Submission participant role name is " + submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName);

                    }

                }

                Assert.True(submissionParticipants.Count() == 1);


            }
        }

        [Fact]
        public void DeleteSubmissionParticipantTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);

                SubmissionParticipantToParticipantRole submissionParticipantToParticipantRoleToAdd = new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2 };

                List<SubmissionParticipantToParticipantRole> submissionParticipantToParticipantRolesToAdd = new List<SubmissionParticipantToParticipantRole>();

                submissionParticipantToParticipantRolesToAdd.Add(submissionParticipantToParticipantRoleToAdd);

                SubmissionParticipant submissionParticipantToAdd = new SubmissionParticipant { ParticipantId = 4001, SubmissionId = 10002, SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRolesToAdd };

                submissionParticipantRepository.CreateSubmissionParticipant(submissionParticipantToAdd);

                SubmissionParticipant submissionParticipantToAdd2 = new SubmissionParticipant { ParticipantId = 4002, SubmissionId = 10002, SubmissionParticipantToParticipantRolesLink = submissionParticipantToParticipantRolesToAdd };

                submissionParticipantRepository.CreateSubmissionParticipant(submissionParticipantToAdd2);

                IEnumerable<SubmissionParticipant> submissionParticipants = submissionParticipantRepository.GetSubmissionParticipants(10002);

                int submissionParticipantId = 0;

                foreach (SubmissionParticipant submissionParticipant in submissionParticipants)
                {

                    Output.WriteLine("SubmissionParticipantId is " + submissionParticipant.SubmissionParticipantId);

                    submissionParticipantId = submissionParticipant.SubmissionParticipantId;

                    Output.WriteLine("Participant ID is " + submissionParticipant.ParticipantId);

                    Output.WriteLine("Submission participant's StfmUserId is " + submissionParticipant.Participant.StfmUserId);

                    foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipant.SubmissionParticipantToParticipantRolesLink)
                    {

                        Output.WriteLine("Submission participant role name is " + submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName);

                    }

                }

                Assert.Equal(2, submissionParticipants.Count());


                Output.WriteLine("About to delete submissionparticipant with id of " + submissionParticipantId);


                submissionParticipantRepository.DeleteSubmissionParticipant(submissionParticipantId);

                IEnumerable<SubmissionParticipant> submissionParticipantsAfterDelete = submissionParticipantRepository.GetSubmissionParticipants(10002);


                foreach (SubmissionParticipant submissionParticipant in submissionParticipantsAfterDelete)
                {

                    Output.WriteLine("SubmissionParticipantId is " + submissionParticipant.SubmissionParticipantId);



                    Output.WriteLine("Participant ID is " + submissionParticipant.ParticipantId);

                    Output.WriteLine("Submission participant's StfmUserId is " + submissionParticipant.Participant.StfmUserId);

                    foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in submissionParticipant.SubmissionParticipantToParticipantRolesLink)
                    {

                        Output.WriteLine("Submission participant role name is " + submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName);

                    }

                }

                Assert.True(1 == submissionParticipantsAfterDelete.Count());

            }

        }

        [Fact]
        public void GetSubmissionParticipantTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);

                SubmissionParticipant submissionParticipant = submissionParticipantRepository.GetSubmissionParticipant(4001, 10001);

                Output.WriteLine("Submission Participant STFM User Id is " + submissionParticipant.Participant.StfmUserId);

                foreach (SubmissionParticipantToParticipantRole role in submissionParticipant.SubmissionParticipantToParticipantRolesLink)
                {


                    Output.WriteLine("Submission Participant role is " + role.ParticipantRole.ParticipantRoleName);

                }


                Assert.Equal(2, submissionParticipant.SubmissionParticipantToParticipantRolesLink.Count());

            }

        }

        [Fact]
        public void GetSubmissionParticipantMoreThenTwoSubmissionsTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);

                List<int> participantIds = submissionParticipantRepository.GetParticipantsMoreThenTwoSubmissions(100, 2000);

                foreach (int participantId in participantIds)
                {


                    Console.WriteLine("Participant ID: " + participantId);

                }

                Assert.Equal(1, participantIds.Count);
            }

        }

        [Fact]
        public void ReorderSubmissionParticipantsTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);

                //arrange
                var submissionParticipantIdsBeforeReorder = context.SubmissionParticipant.Where(sp => sp.SubmissionId == 10001);

                //act
                submissionParticipantRepository.UpdateSubmissionParticipantSortOrder(new List<SubmissionParticipant> {
                    new SubmissionParticipant{ SubmissionParticipantId = 1, SortOrder = 1},
                    new SubmissionParticipant{ SubmissionParticipantId = 2, SortOrder = 0},
                }, 10001);

            }

            using(var context = new AppDbContext(options))
            {
                //assert
                var submissionParticipantIdsAfterReorder = context.SubmissionParticipant.Where(sp => sp.SubmissionId == 10001);
                Assert.True(submissionParticipantIdsAfterReorder.FirstOrDefault(sp => sp.SubmissionParticipantId == 1).SortOrder == 1);
                Assert.True(submissionParticipantIdsAfterReorder.FirstOrDefault(sp => sp.SubmissionParticipantId == 2).SortOrder == 0);
            }
        }

        /// <summary>
        /// UpdateSubmissionParticipantOrder(List<SubmissionParticipant> submissionParticipantsReordered, int submissionId)
        /// should not attempt to set the sort order on any SubmissionParticipants who are submitter-only. 
        /// </summary>
        [Fact]
        public void UpdateSubmissionParticipantOrder_DoNotTryToUpdateSubmitterOnly()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {
                SubmissionParticipantRepository submissionParticipantRepository = new SubmissionParticipantRepository(context);

                //arrange
                //Get the SubmissionParticipants from the Submission that has a Submitter-only participant
                var submissionParticipantsBeforeReorder = context.SubmissionParticipant
                    .Include(s => s.SubmissionParticipantToParticipantRolesLink)
                    .ThenInclude(sp => sp.ParticipantRole)
                    .Where(s => s.SubmissionId == 10003).ToList();

                //Mock up a list of reordered SubmissionParticipants
                var reorderedSubmissionParticipantsMock 
                    = submissionParticipantsBeforeReorder
                        .Where(s => 
                            s.SubmissionParticipantToParticipantRolesLink.Count > 1 || 
                            (
                                s.SubmissionParticipantToParticipantRolesLink.Count == 1 && 
                                s.SubmissionParticipantToParticipantRolesLink.FirstOrDefault().ParticipantRole.ParticipantRoleName != ParticipantRoleType.SUBMITTER)
                            )
                        .ToList();
                reorderedSubmissionParticipantsMock.FirstOrDefault(s => s.SubmissionParticipantId == 3).SortOrder = 1;
                reorderedSubmissionParticipantsMock.FirstOrDefault(s => s.SubmissionParticipantId == 4).SortOrder = 0;

                //act
                submissionParticipantRepository.UpdateSubmissionParticipantSortOrder(reorderedSubmissionParticipantsMock, 10003);

                //assert
                Assert.Equal(1, submissionParticipantRepository.GetSubmissionParticipant(4001, 10003).SortOrder);
                Assert.Equal(0, submissionParticipantRepository.GetSubmissionParticipant(4002, 10003).SortOrder);
                Assert.Equal(-1, submissionParticipantRepository.GetSubmissionParticipant(4003, 10003).SortOrder);
            }
        }

        [Fact]
        public void GetSubmissionsWithParticipantsAddedOrRemoved_StubSomeAddedSubmissionParticipants_VerifyResults()
        {
            //Arrange
            var auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    EntityId = 1,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-10)
                },
                new AuditLog
                {
                    AuditLogId = 2,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    EntityId = 2,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-29)
                },
                new AuditLog
                {
                    AuditLogId = 3,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    EntityId = 3,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-10)
                },
                new AuditLog
                {
                    AuditLogId = 4,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    EntityId = 4,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-23)
                }
            };

            var auditLogParentEntities = new List<AuditLogParentEntity>
            {
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 1,
                    AuditLogId = 1,
                    ParentEntityId = 10001,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 2,
                    AuditLogId = 2,
                    ParentEntityId = 10002,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 3,
                    AuditLogId = 3,
                    ParentEntityId = 10003,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 4,
                    AuditLogId = 4,
                    ParentEntityId = 10004,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 5,
                    AuditLogId = 1,
                    ParentEntityId = 4001,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 6,
                    AuditLogId = 2,
                    ParentEntityId = 4002,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 7,
                    AuditLogId = 3,
                    ParentEntityId = 4003,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 8,
                    AuditLogId = 4,
                    ParentEntityId = 4004,
                    ParentEntityType = EntityType.Participant
                }
            };

            DbContextOptions<AppDbContext> options = GetDbContextOptions_IncludeAuditLogs(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var submissionParticipantRepository = new SubmissionParticipantRepository(ctx);

                var results = submissionParticipantRepository.GetSubmissionsWithParticipantsAddedOrRemoved(100, DateTime.Now.AddDays(-30));

                Assert.Equal(4, results.Count);

                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10001).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.REMOVED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10002).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.REMOVED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10003).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.REMOVED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10004).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.REMOVED);

                Assert.Equal("3001", results.First(r => r.SubmissionId == 10001).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3002", results.First(r => r.SubmissionId == 10002).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3003", results.First(r => r.SubmissionId == 10003).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3004", results.First(r => r.SubmissionId == 10004).ParticipantsAddedOrRemoved.First().STFMUserId);

                Assert.Equal("L09", results.First(r => r.SubmissionId == 10001).SessionCode);
                Assert.Equal("B52", results.First(r => r.SubmissionId == 10002).SessionCode);
                Assert.Equal("PR45", results.First(r => r.SubmissionId == 10003).SessionCode);
                Assert.Equal("PB3", results.First(r => r.SubmissionId == 10004).SessionCode);
            }
        }

        [Fact]
        public void GetSubmissionsWithParticipantsAddedOrRemoved_StubSomeDeletedSubmissionParticipants_VerifyResults()
        {
            //Arrange           
            var auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    EntityId = 1,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-10)
                },
                new AuditLog
                {
                    AuditLogId = 2,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    EntityId = 2,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-29)
                },
                new AuditLog
                {
                    AuditLogId = 3,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    EntityId = 3,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-10)
                },
                new AuditLog
                {
                    AuditLogId = 4,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    EntityId = 4,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-23)
                }
            };

            var auditLogParentEntities = new List<AuditLogParentEntity>
            {
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 1,
                    AuditLogId = 1,
                    ParentEntityId = 10001,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 2,
                    AuditLogId = 2,
                    ParentEntityId = 10002,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 3,
                    AuditLogId = 3,
                    ParentEntityId = 10003,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 4,
                    AuditLogId = 4,
                    ParentEntityId = 10004,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 5,
                    AuditLogId = 1,
                    ParentEntityId = 4001,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 6,
                    AuditLogId = 2,
                    ParentEntityId = 4002,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 7,
                    AuditLogId = 3,
                    ParentEntityId = 4003,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 8,
                    AuditLogId = 4,
                    ParentEntityId = 4004,
                    ParentEntityType = EntityType.Participant
                }
            };
            
            DbContextOptions<AppDbContext> options = GetDbContextOptions_IncludeAuditLogs(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var submissionParticipantRepository = new SubmissionParticipantRepository(ctx);

                var results = submissionParticipantRepository.GetSubmissionsWithParticipantsAddedOrRemoved(100, DateTime.Now.AddDays(-30));

                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10001).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.ADDED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10002).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.ADDED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10003).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.ADDED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10004).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.ADDED);

                Assert.Equal("3001", results.First(r => r.SubmissionId == 10001).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3002", results.First(r => r.SubmissionId == 10002).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3003", results.First(r => r.SubmissionId == 10003).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3004", results.First(r => r.SubmissionId == 10004).ParticipantsAddedOrRemoved.First().STFMUserId);

                Assert.Equal("L09", results.First(r => r.SubmissionId == 10001).SessionCode);
                Assert.Equal("B52", results.First(r => r.SubmissionId == 10002).SessionCode);
                Assert.Equal("PR45", results.First(r => r.SubmissionId == 10003).SessionCode);
                Assert.Equal("PB3", results.First(r => r.SubmissionId == 10004).SessionCode);
            }
        }

        [Fact]
        public void GetSubmissionsWithParticipantsAddedOrRemoved_StubSomeDeletedAndSomeCreatedSubmissionParticipants_VerifyResults()
        {
            //Arrange          
            var auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    EntityId = 1,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-10)
                },
                new AuditLog
                {
                    AuditLogId = 2,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    EntityId = 2,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-29)
                },
                new AuditLog
                {
                    AuditLogId = 3,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    EntityId = 3,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-10)
                },
                new AuditLog
                {
                    AuditLogId = 4,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    EntityId = 4,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = DateTime.Now.AddDays(-23)
                }
            };

            var auditLogParentEntities = new List<AuditLogParentEntity>
            {
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 1,
                    AuditLogId = 1,
                    ParentEntityId = 10001,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 2,
                    AuditLogId = 2,
                    ParentEntityId = 10002,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 3,
                    AuditLogId = 3,
                    ParentEntityId = 10003,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 4,
                    AuditLogId = 4,
                    ParentEntityId = 10004,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 5,
                    AuditLogId = 1,
                    ParentEntityId = 4001,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 6,
                    AuditLogId = 2,
                    ParentEntityId = 4002,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 7,
                    AuditLogId = 3,
                    ParentEntityId = 4003,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 8,
                    AuditLogId = 4,
                    ParentEntityId = 4004,
                    ParentEntityType = EntityType.Participant
                }
            };

            DbContextOptions<AppDbContext> options = GetDbContextOptions_IncludeAuditLogs(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var submissionParticipantRepository = new SubmissionParticipantRepository(ctx);

                var results = submissionParticipantRepository.GetSubmissionsWithParticipantsAddedOrRemoved(100, DateTime.Now.AddDays(-30));


                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10001).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.REMOVED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10002).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.REMOVED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10003).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.ADDED);
                Assert.DoesNotContain(results.First(r => r.SubmissionId == 10004).ParticipantsAddedOrRemoved, p => p.AddedOrRemoved == AddedOrRemoved.ADDED);

                Assert.Equal("3001", results.First(r => r.SubmissionId == 10001).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3002", results.First(r => r.SubmissionId == 10002).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3003", results.First(r => r.SubmissionId == 10003).ParticipantsAddedOrRemoved.First().STFMUserId);
                Assert.Equal("3004", results.First(r => r.SubmissionId == 10004).ParticipantsAddedOrRemoved.First().STFMUserId);

                Assert.Equal("L09", results.First(r => r.SubmissionId == 10001).SessionCode);
                Assert.Equal("B52", results.First(r => r.SubmissionId == 10002).SessionCode);
                Assert.Equal("PR45", results.First(r => r.SubmissionId == 10003).SessionCode);
                Assert.Equal("PB3", results.First(r => r.SubmissionId == 10004).SessionCode);
            }
        }

        [Fact]
        public void GetSubmissionsWithParticipantsAddedOrRemoved_StubSomeDeletedAndSomeCreatedSubmissionParticipantsAndSomeDeletedDateBeforeAsOfDate_VerifyResults()
        {
            //Arrange      
            var asOfDate = DateTime.Now.AddDays(-30);
            var auditLogs = new List<AuditLog>
            {
                new AuditLog
                {
                    AuditLogId = 1,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    EntityId = 1,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = asOfDate.AddDays(2)
                },
                new AuditLog
                {
                    AuditLogId = 2,
                    AuditLogActionType = AuditLogActionType.CREATED,
                    EntityId = 2,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = asOfDate.AddDays(-2)
                },
                new AuditLog
                {
                    AuditLogId = 3,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    EntityId = 3,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = asOfDate.AddDays(10)
                },
                new AuditLog
                {
                    AuditLogId = 4,
                    AuditLogActionType = AuditLogActionType.DELETED,
                    EntityId = 4,
                    EntityType = EntityType.SubmissionParticipant,
                    TimeStamp = asOfDate.AddDays(-10)
                }
            };

            var auditLogParentEntities = new List<AuditLogParentEntity>
            {
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 1,
                    AuditLogId = 1,
                    ParentEntityId = 10001,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 2,
                    AuditLogId = 2,
                    ParentEntityId = 10002,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 3,
                    AuditLogId = 3,
                    ParentEntityId = 10003,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 4,
                    AuditLogId = 4,
                    ParentEntityId = 10004,
                    ParentEntityType = EntityType.Submission
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 5,
                    AuditLogId = 1,
                    ParentEntityId = 4001,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 6,
                    AuditLogId = 2,
                    ParentEntityId = 4002,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 7,
                    AuditLogId = 3,
                    ParentEntityId = 4003,
                    ParentEntityType = EntityType.Participant
                },
                new AuditLogParentEntity
                {
                    AuditLogParentEntityId = 8,
                    AuditLogId = 4,
                    ParentEntityId = 4004,
                    ParentEntityType = EntityType.Participant
                }
            };

            DbContextOptions<AppDbContext> options = GetDbContextOptions_IncludeAuditLogs(auditLogs, auditLogParentEntities);

            using (var ctx = new AppDbContext(options))
            {
                var submissionParticipantRepository = new SubmissionParticipantRepository(ctx);

                var results = submissionParticipantRepository.GetSubmissionsWithParticipantsAddedOrRemoved(100, asOfDate);

                Assert.Equal(2, results.Count);

                Assert.Contains(results, r => r.SubmissionId == 10001);
                Assert.Contains(results, r => r.SubmissionId == 10003);

                Assert.DoesNotContain(results, r => r.SubmissionId == 10002);
                Assert.DoesNotContain(results, r => r.SubmissionId == 10004);

            }
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }

                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {
                        AcceptedCategoryId = 1000,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract 2",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 11, 2018"),
                        SubmissionId = 10002,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 11, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title 2",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Submission With A Submitter Only Participant",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                        SubmissionId = 10003,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 19, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Submission With A Submitter Only Participant - Abstract",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Submission With A Submitter Only Participant",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                        SubmissionId = 10004,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 19, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Submission With A Submitter Only Participant - Abstract",

                    }

                );

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }

                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(

                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10001, SortOrder = 1 },
                    new SubmissionParticipant { SubmissionParticipantId = 6, ParticipantId = 4002, SubmissionId = 10004, SortOrder = 1 },
                    new SubmissionParticipant { SubmissionParticipantId = 3, ParticipantId = 4001, SubmissionId = 10003, SortOrder = 0 },
                    new SubmissionParticipant { SubmissionParticipantId = 4, ParticipantId = 4002, SubmissionId = 10003, SortOrder = 1 },
                    new SubmissionParticipant { SubmissionParticipantId = 5, ParticipantId = 4003, SubmissionId = 10003, SortOrder = -1 }
                );


                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 2 },

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 3 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 4 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 5 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 6 }



                );

                context.SaveChanges();

            }

            return options;
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions_IncludeAuditLogs(List<AuditLog> auditLogs, List<AuditLogParentEntity> auditLogParentEntities)
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }

                );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {
                        AcceptedCategoryId = 1000,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract 2",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 11, 2018"),
                        SubmissionId = 10002,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 11, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title 2",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Submission With A Submitter Only Participant",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                        SubmissionId = 10003,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 19, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Submission With A Submitter Only Participant - Abstract",

                    },
                    new Submission
                    {
                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Submission With A Submitter Only Participant",
                        SubmissionCategoryId = 1001,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 14, 2018"),
                        SubmissionId = 10004,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 19, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Submission With A Submitter Only Participant - Abstract",

                    }

                );

                context.SaveChanges();

                //Create some conference sessions

                context.ConferenceSessions.AddRange(new List<ConferenceSession> {
                    new ConferenceSession
                    {
                        ConferenceSessionId = 1,
                        SubmissionId = 10001,
                        SessionCode = "L09",
                        SessionLocation = "The Blue Room"
                    },
                    new ConferenceSession
                    {
                        ConferenceSessionId = 2,
                        SubmissionId = 10002,
                        SessionCode = "B52",
                        SessionLocation = "The Red Room"
                    },
                    new ConferenceSession
                    {
                        ConferenceSessionId = 3,
                        SubmissionId = 10003,
                        SessionCode = "PR45",
                        SessionLocation = "The Green Room"
                    },
                    new ConferenceSession
                    {
                        ConferenceSessionId = 4,
                        SubmissionId = 10004,
                        SessionCode = "PB3",
                        SessionLocation = "The Yellow Room"
                    }
                });

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }

                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" },
                    new Participant { ParticipantId = 4004, StfmUserId = "3004" },
                    new Participant { ParticipantId = 4005, StfmUserId = "3005" },
                    new Participant { ParticipantId = 4006, StfmUserId = "3006" }

                );

                context.SaveChanges();


                context.AuditLogs.AddRange(auditLogs);
                context.AuditLogParentEntities.AddRange(auditLogParentEntities);

                context.SaveChanges();

            }

            return options;
        }

    }
}
