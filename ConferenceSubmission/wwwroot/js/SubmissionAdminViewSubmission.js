﻿var showChangeStatusDialog = function () {
    if ($("#changeSubmissionStatusContainer").css("display") === "none") {
        $("#changeSubmissionStatusContainer").show(150);
    }
    else {
        $("#submissionStatusSelect").val("");
        $("#changeStatusLinksContainer").hide(150);
        $("#changeSubmissionStatusContainer").hide(150);
        $("#changeSubmissionStatusContainer").hide(150);
    }
};

var submissionStatusChanged = function () {
    var selectedValue = $("#submissionStatusSelect").val();
    var uncancelLink = $("#updateStatusLink").attr("href");
    var indexOfTypeParam = uncancelLink.indexOf("Type=");
    var currentTypeParam = uncancelLink.slice(indexOfTypeParam);
    uncancelLink = uncancelLink.replace(currentTypeParam, "Type=" + selectedValue);
    $("#updateStatusLink").attr("href", uncancelLink);

    if (selectedValue !== "") {
        $("#changeStatusLinksContainer").show(150);
    }
    else {
        $("#changeStatusLinksContainer").hide(150);
    }
};

var cancelChangeStatus = function () {
    $("#submissionStatusSelect").val("");
    $("#changeStatusLinksContainer").hide(150);
    $("#changeSubmissionStatusContainer").hide(150);
};

var showChangePresentationMethodStatusDialog = function () {
    if ($("#changePresentationMethodStatusContainer").css("display") === "none") {
        $("#changePresentationMethodStatusContainer").show(150);
    }
    else {
        $("#presentationMethodStatusSelect").val("");
        $("#changePresentatonMethodStatusLinksContainer").hide(150);
        $("#changePresentationMethodStatusContainer").hide(150);
        $("#changePresentationMethodStatusContainer").hide(150);
    }
};

var presentationMethodStatusChanged = function () {
    var selectedValue = $("#presentationMethodStatusSelect").val();
    var uncancelLink = $("#updatePresentationMethodStatusLink").attr("href");
    var indexOfTypeParam = uncancelLink.indexOf("Type=");
    var currentTypeParam = uncancelLink.slice(indexOfTypeParam);
    uncancelLink = uncancelLink.replace(currentTypeParam, "Type=" + selectedValue);
    $("#updatePresentationMethodStatusLink").attr("href", uncancelLink);

    if (selectedValue !== "") {
        $("#changePresentationMethodStatusLinksContainer").show(150);
    }
    else {
        $("#changePresentationMethodStatusLinksContainer").hide(150);
    }
};

var cancelChangePresentationMethodStatus = function () {
    $("#presentationMethodStatusSelect").val("");
    $("#changePresentationMethodStatusLinksContainer").hide(150);
    $("#changePresentationStatusContainer").hide(150);
};

var showChangeCategoryDialog = function () {
    if ($("#changeSubmissionCategoryContainer").css("display") === "none") {
        $("#changeSubmissionCategoryContainer").show(150);
    }
    else {
        $("#submissionCategorySelect").val("");
        $("#changeCategoryLinksContainer").hide(150);
        $("#changeSubmissionCategoryContainer").hide(150);
    }
};

var submissionCategoryChanged = function () {
    var selectedValue = $("#submissionCategorySelect").val();
    var uncancelLink = $("#updateCategoryLink").attr("href");
    var indexOfChangeParam = uncancelLink.indexOf("acceptedCategoryId=");
    var currentChangeParam = uncancelLink.slice(indexOfChangeParam);
    uncancelLink = uncancelLink.replace(currentChangeParam, "acceptedCategoryId=" + selectedValue);
    $("#updateCategoryLink").attr("href", uncancelLink);

    if (selectedValue !== "") {
        $("#changeCategoryLinksContainer").show(150);
    }
    else {
        $("#changeCategoryLinksContainer").hide(150);
    }
};

var cancelChangeCategory = function () {
    $("#submissionCategorySelect").val("");
    $("#changeCategoryLinksContainer").hide(150);
    $("#changeSubmissionCategoryContainer").hide(150);
};

var toggleShowHideEditParticipantDialog = function (participantId) {
    var currentDisplayStats = $("#editParticipant-" + participantId + "-container").css("display");
    $(".editParticipantContainer").hide(150);
    if (currentDisplayStats === "none")
        $("#editParticipant-" + participantId + "-container").toggle(150);
    $(".verifyRemoveRoleContainer").hide(150);
    $(".verifyAddRoleContainer").hide(150);
    $(".removeParticipantContainer").hide(150);
};

var toggleVerifyRemoveRoleContainer = function (submissionParticipantId, participantRole) {
    $("#verifyRemoveRoleContainer-" + submissionParticipantId + "-" + participantRole).toggle(150);
};

var toggleVerifyAddRoleContainer = function (submissionParticipantId, participantRoleType) {
    $("#verifyAddRoleContainer-" + submissionParticipantId + "-" + participantRoleType).toggle(150);
};

var toggleRemoveParticipantContainer = function (submissionParticipantId) {
    $("#removeParticipantContainer-" + submissionParticipantId).toggle(150);
};

$(".actionLink").click(function () {
    $(".actionLink,.cancelLink").hide();
});