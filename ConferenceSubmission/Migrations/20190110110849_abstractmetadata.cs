﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConferenceSubmission.Migrations
{
    public partial class abstractmetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CategoryAbstractInstructions",
                table: "SubmissionCategories",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryAbstractMaxLength",
                table: "SubmissionCategories",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryAbstractInstructions",
                table: "SubmissionCategories");

            migrationBuilder.DropColumn(
                name: "CategoryAbstractMaxLength",
                table: "SubmissionCategories");
        }
    }
}
