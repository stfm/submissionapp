﻿using System;
using Microsoft.AspNetCore.Mvc;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using ConferenceSubmission.Mappers;
using Serilog;
using Dropbox.Api.Files;
using Dropbox.Api;
using System.Threading.Tasks;
using System.Linq;
using System.Globalization;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Configuration;

namespace ConferenceSubmission.Controllers
{

    public class SubmissionStatusController : Controller
    {

        private readonly IConferenceService _conferenceService;

        private readonly ISubmissionService _submissionService;

        private readonly ISubmissionParticipantService _submissionParticipantService;

        private readonly ISalesforceAPIService _salesforceAPIService;

        private readonly IEmailNotificationService _emailNotificationService;

        private readonly ISubmissionStatusService _submissionStatusService;

        private readonly IUserMapper _userMapper;

        private readonly IConferenceSessionService _conferenceSessionService;

        private readonly IVirtualMaterialService _virtualMaterialService;

        private readonly IVirtualConferenenceDataService _virtualConferenenceDataService;

        private readonly IDisclosureService _disclosureService;

        private readonly IConfiguration _configuration;

        public SubmissionStatusController(IConferenceService conferenceService,
                               ISubmissionService submissionService,
            ISubmissionParticipantService submissionParticipantService,
                               ISalesforceAPIService salesforceAPIService,
            IEmailNotificationService emailNotificationService,
            ISubmissionStatusService submissionStatusService,
            IUserMapper userMapper, IConferenceSessionService conferenceSessionService,
            IVirtualMaterialService virtualMaterialService,
            IVirtualConferenenceDataService virtualConferenenceDataService, IDisclosureService disclosureService,
            IConfiguration configuration)
        {

            _conferenceService = conferenceService;

            _submissionService = submissionService;

            _submissionParticipantService = submissionParticipantService;

            _salesforceAPIService = salesforceAPIService;

            _emailNotificationService = emailNotificationService;

            _submissionStatusService = submissionStatusService;

            _userMapper = userMapper;

            _conferenceSessionService = conferenceSessionService;

            _virtualMaterialService = virtualMaterialService;

            _virtualConferenenceDataService = virtualConferenenceDataService;

            _disclosureService = disclosureService;

            _configuration = configuration;
        }



        /// <summary>
        /// For the provided submission id update its submission status to the value provided.
        /// This is used to process submissions for accept and decline.
        /// It is normally called by class ProcessFromFile in project processsubmissions
        /// </summary>
        /// <param name="submissionId">Submission Id</param>
        /// <param name="submissionStatusId">Submission Status Id</param>
        /// <returns></returns>
        public IActionResult UpdateSubmissionStatusAndEmailPresenters(int submissionId, int submissionStatusId)
        {

            //Update the submission status
            _submissionService.UpdateSubmissionStatus(submissionId, submissionStatusId);

            //Get the complete details - use a fake STFM User ID value as it is not needed for our purposes
            var submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId, "1234567");

            _submissionService.AddSubmissionPresentationsToSubmission(submissionId);

            List<SubmissionParticipant> participants = _submissionParticipantService.GetSubmissionParticipants(submissionId);

            List<string> emailAddresses = new List<string>();

            foreach (SubmissionParticipant participant in participants)
            {
                User user = _salesforceAPIService.GetUserInfo(participant.Participant.StfmUserId).Result;

                string emailAddress = user.Email;

                emailAddresses.Add(emailAddress);


            }

            _emailNotificationService.SendSubmissionAcceptedOrRejectedEmail(submissionWithProposalAnswersViewModel, submissionStatusId, emailAddresses);

            return View();


        }

        /// <summary>
        /// Email lead presenter and submitter for the provided submission ID an email about online presentation.
        /// The value of presentationStatusId will determine which email text to send.  A value of 2 
        /// means we need to know if the presenter will do a recorded online presentation and a value of 3 means 
        /// we need the presenter to provide their material for the online presentation.
        /// </summary>
        /// <param name="submissionId"></param>
        /// <param name="presentationstatusid"></param>
        /// <returns></returns>
        public IActionResult EmailPresentersAboutOnlinePresentation(int submissionId, int presentationStatusId)
        {
            //Get the complete details - use a fake STFM User ID value as it is not needed for our purposes
            var submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId, "1234567");

            List<SubmissionParticipant> participants = _submissionParticipantService.GetSubmissionParticipants(submissionId);

            List<string> emailAddresses = new List<string>();

            foreach (SubmissionParticipant participant in participants)
            {
                foreach (SubmissionParticipantToParticipantRole submissionParticipantToParticipantRole in participant.SubmissionParticipantToParticipantRolesLink)
                {

                    if (submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName == ParticipantRoleType.SUBMITTER ||
                        submissionParticipantToParticipantRole.ParticipantRole.ParticipantRoleName == ParticipantRoleType.LEAD_PRESENTER)
                    {

                        User user = _salesforceAPIService.GetUserInfo(participant.Participant.StfmUserId).Result;

                        string emailAddress = "bruceaphillips@gmail.com";

                        if (user != null)
                        {
                            emailAddress = user.Email;
                        }

                        emailAddresses.Add(emailAddress);
                    }
                }


            }

            _emailNotificationService.SendEmailOnlinePresentation(submissionWithProposalAnswersViewModel, presentationStatusId, emailAddresses);


            return View();
        }


        public IActionResult UpdateSessionWithRoom(int submissionId, string room, string track)
        {
            _conferenceSessionService.UpdateConferenceSessionWithRoom(submissionId, room, track);

            return View();
        }


        /// <summary>
        /// Responds to request to email submitter and main presenter
        /// to provide him/her instructions on how to tell STFM if they 
        /// will present their submissions in at the virutal conference 
        /// or will withdraw.
        /// </summary>
        /// <param name="emailAddress">Email address of person</param>
        /// <param name="conferenceId">Conference Id </param>
        /// <returns></returns>
        public IActionResult EmailPresenterVirtualConference(string emailAddress, int submissionId)
        {
            //Get the complete details - use a fake STFM User ID value as it is not needed for our purposes
            var submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId, "1234567");
            _emailNotificationService.SendInitialVirtualConferenceEmail(emailAddress, submissionWithProposalAnswersViewModel);

            return View();


        }

        /// <summary>
        /// Responds to request to email submitter and main presenter
        /// to provide him/her instructions on how to upload material
        /// for virtual presentation.
        /// </summary>
        /// <param name="emailAddress">Email address of person</param>
        /// <param name="submissionId">Submission Id </param>
        /// <returns></returns>
        public IActionResult EmailPresenterUploadMaterial(string emailAddress, int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);
            _emailNotificationService.SendEmailPresenterUploadMaterial(emailAddress, submission);

            return View();


        }

        /// <summary>
        /// Responds to request to send reminder email to AN2020 
        /// submitter or main presenter
        /// to provide him/her instructions on how to tell STFM if they 
        /// will present their submissions in August or will withdraw.
        /// </summary>
        /// <param name="stfmUserId"></param>
        /// <returns></returns>
        public IActionResult AN2020RemindPresenter(string stfmUserId)
        {
            User user = _salesforceAPIService.GetUserInfo(stfmUserId).Result;

            String emailAddress = user.Email;

            _emailNotificationService.SendAN2020ReminderEmail(emailAddress);

            return View();


        }

        public IActionResult SendCPQIDisclosureReminder(string stfmUserId, int submissionId)
        {
            User user = _salesforceAPIService.GetUserInfo(stfmUserId).Result;

            String emailAddress = user.Email;

            List<SubmissionFormField> submissionFormFields = _submissionService.GetSubmissionFormFields(submissionId).ToList();

            string cmeanswer = "";

            foreach (SubmissionFormField submissionFormField in submissionFormFields)
            {
                foreach (FormFieldProperty formFieldProperty in submissionFormField.FormField.FormFieldProperties)
                {
                    if (formFieldProperty.PropertyValue.Contains("should be considered for behavioral science", StringComparison.InvariantCultureIgnoreCase))
                    {
                        cmeanswer = submissionFormField.SubmissionFormFieldValue;
                    }
 

                }
            }

            if (cmeanswer.Contains("yes", StringComparison.InvariantCultureIgnoreCase))
            {
                Log.Information("Behavorial Science CME for " + submissionId + " for person " + user.GetFullNamePretty() + " email " + emailAddress);
            }
            
            _emailNotificationService.SendCPQIDisclosureReminder(emailAddress, submissionId);

            return View();


        }

        /// <summary>
        /// Responds to request to email submitter and main presenter
        /// to remind them to complete their submission.
        /// </summary>
        /// <param name="emailAddress">Email address of person</param>
        /// <param name="submissionId">Submission Id </param>
        /// <returns></returns>
        public IActionResult EmailIncompleteReminder(string emailAddress, int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);
            _emailNotificationService.SendIncompleteReminderEmail(emailAddress, submission);

            return View();


        }

        public IActionResult GetAcceptedEmail(int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);


            return View(submission);


        }

        public IActionResult GetAcceptedEmailSent(string stfmEmail, int submissionId)
        {
            if (!stfmEmail.EndsWith("stfm.org"))
            {
                ViewBag.error = "You must specify only an STFM email address to have the accept email sent to.  You can then forward the email to the presenter after reviewing it.";

                Submission submission = _submissionService.GetSubmission(submissionId);

                return View("GetAcceptedEmail", submission);
            }

            //Get the complete details - use a fake STFM User ID value as it is not needed for our purposes
            var submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId, "1234567");

            List<string> emailAddresses = new List<string>();
            emailAddresses.Add(stfmEmail);

            _emailNotificationService.SendSubmissionAcceptedOrRejectedEmail(submissionWithProposalAnswersViewModel, 3, emailAddresses);

            ViewBag.emailAddress = stfmEmail;

            return View();


        }

        /// <summary>
        /// Responds to request to email submitter and main presenter
        /// to remind them to complete their submission.
        /// </summary>
        /// <param name="submissionId">Submission Id </param>
        /// <returns></returns>
        public IActionResult SendIncompleteReminder(int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);

            List<SubmissionParticipant> participants = _submissionParticipantService.GetSubmissionParticipants(submissionId);

            List<string> emailAddresses = new List<string>();

            foreach (SubmissionParticipant participant in participants)
            {
                User user = _salesforceAPIService.GetUserInfo(participant.Participant.StfmUserId).Result;

                string emailAddress = user.Email;

                emailAddresses.Add(emailAddress);


            }


            _emailNotificationService.SendIncompleteReminderEmail(emailAddresses, submission);

            return View();


        }

        public IActionResult RenameDropboxFile(int submissionId, int conferenceId)
        {

            Metadata renamedDropboxFile = RenameDropboxFileAction(submissionId, conferenceId).Result;

            Log.Information("Dropbox file renamed to " + renamedDropboxFile.Name + " for submissionID: " + submissionId);

            if (renamedDropboxFile != null)
            {

                List<VirtualMaterial> virtualMaterials = _virtualMaterialService.GetVirtualMaterials(submissionId).ToList();

                VirtualMaterial videoVirtualMaterial = virtualMaterials.FirstOrDefault(vm => vm.MaterialType.Equals("video", StringComparison.InvariantCultureIgnoreCase));

                videoVirtualMaterial.MaterialValue = renamedDropboxFile.Name;

                _virtualMaterialService.UpdateVirtualMaterial(videoVirtualMaterial);

                Log.Information("VirtualMaterial video file name changed to: " + renamedDropboxFile.Name + " for submissionId: " + submissionId);

            }

            return View();
        }

        private async Task<Metadata> GetDropboxFile(string videoFilename, string folderName)
        {

            try
            {

                using (var dbx = new DropboxClient("U5mw0KN7ybAAAAAAAAAF9nwXzKM6roPi8j96aNa7Zs3-VvE-K7HTOi4AcW6irg1s"))
                {

                    var list = await dbx.Files.ListFolderAsync("/File Requests/" + folderName + "/");

                    bool fileFound = false;

                    foreach (var item in list.Entries.Where(i => i.IsFile))
                    {
                        if (item.Name.Contains(videoFilename, StringComparison.InvariantCultureIgnoreCase))
                        {
                            fileFound = true;
                            return item;
                        }
                    }

                    foreach (var item in list.Entries.Where(i => i.IsFolder))
                    {
                        var folderFiles = await dbx.Files.ListFolderAsync("/File Requests/" + folderName + "/" + item.Name + "/");

                        foreach (var itemFile in folderFiles.Entries.Where(i => i.IsFile))
                        {
                            if (itemFile.Name.Contains(videoFilename, StringComparison.InvariantCultureIgnoreCase))
                            {
                                fileFound = true;
                                return itemFile;
                            }
                        }

                    }
                    Log.Information($"Video with filename of  {videoFilename} was NOT found in our Dropbox folder.");
                    return null;
                }

            }
            catch (ApiException<GetMetadataError> ex)
            {
                Log.Information($"Video with filename of  {videoFilename} was NOT found in our Dropbox folder - {ex.Message}");
                return null;
            }
        }

        private async Task<Metadata> RenameDropboxFileAction(int submissionId, int conferenceId)
        {


            List<VirtualMaterial> virtualMaterials = _virtualMaterialService.GetVirtualMaterials(submissionId).ToList();

            VirtualMaterial videoVirtualMaterial = virtualMaterials.FirstOrDefault(vm => vm.MaterialType.Equals("video", StringComparison.InvariantCultureIgnoreCase));

            SubmissionWithProposalAnswersViewModel submissionWithProposalAnswersViewModel = _submissionService.GetSubmissionWithProposal(submissionId);

            VirtualConferenceData virtualConferenceData = _virtualConferenenceDataService.GetVirtualConferenceData(conferenceId);

            string folderName = virtualConferenceData.DropboxFolderName;

            string filenamewithoutext = videoVirtualMaterial.MaterialValue.Split('.')[0].ToLowerInvariant();

            Metadata dropboxFile = GetDropboxFile(filenamewithoutext, folderName).Result;

            var ext = dropboxFile.Name.Split('.')[1].Split(' ')[0].ToLowerInvariant();

            String newFileName = submissionId + "_video." + ext;


            Metadata renamedDropboxFile = null;

            if (!newFileName.Contains("None", StringComparison.CurrentCultureIgnoreCase))
            {
                Log.Information("About to rename dropbox file " + dropboxFile.Name + " to " + newFileName);

                try
                {

                    using (var dbx = new DropboxClient("U5mw0KN7ybAAAAAAAAAF9nwXzKM6roPi8j96aNa7Zs3-VvE-K7HTOi4AcW6irg1s"))
                    {

                        string filePath = dropboxFile.PathLower;

                        string fileName = (dropboxFile.Name).ToLower(new CultureInfo("en-US", false));

                        string newFilePath = filePath.Replace(fileName, newFileName);

                        RelocationResult relocationResult = dbx.Files.MoveV2Async(dropboxFile.PathLower, newFilePath).Result;

                        renamedDropboxFile = relocationResult.Metadata;


                    }

                }
                catch (ApiException<GetMetadataError> ex)
                {
                    Log.Information($"Video with filename of  {dropboxFile.Name} could not be renamed - {ex.Message}");
                    return null;
                }
            }

            return renamedDropboxFile;

        }

        /// <summary>
        /// Responds to request to email submitter and main presenter
        /// to remind them to select either will present or withdraw
        /// for their submission.
        /// </summary>
        /// <param name="emailAddress">Email address of person</param>
        /// <param name="submissionId">Submission Id </param>
        /// <returns></returns>
        public IActionResult EmailWillPresentReminder(string emailAddress, int submissionId)
        {

            Submission submission = _submissionService.GetSubmission(submissionId);
            _emailNotificationService.SendWillPresentReminderEmail(emailAddress, submission);

            return View();


        }

        public IActionResult AddSubmissionPresentationMethods(int conferenceId)
        {

            Conference conference = _conferenceService.GetConference(conferenceId);

            _submissionService.AddSubmissionPresentationMethods(conferenceId);

            return View(conference);


        }

        public IActionResult RenameCvFile(int disclosureId)
        {

            Disclosure disclosure = _disclosureService.GetDisclosure(disclosureId);

            string sourceBlobName = disclosure.CvFilename;

            bool disclosureFileRenamed = _disclosureService.renameCvFile(disclosure);

            string destinationBlobName = disclosure.CvFilename;

            string blobstorageconnection = _configuration["BlobStorage:key"];

            string blobContainer = "cvfiles";

            // Get a reference to a blob
            BlobClient sourceBlob = new BlobClient(blobstorageconnection, blobContainer, sourceBlobName);
            BlobClient destinationBlob = new BlobClient(blobstorageconnection, blobContainer, destinationBlobName);

            // Ensure that the source blob exists
            if (sourceBlob.ExistsAsync().Result)
            {
                // Start the copy operation
                destinationBlob.StartCopyFromUriAsync(sourceBlob.Uri);

                // Optionally, you can also wait for the copy to complete
                while ((destinationBlob.GetPropertiesAsync().Result).Value.CopyStatus == Azure.Storage.Blobs.Models.CopyStatus.Pending)
                {
                    Task.Delay(1000);
                }

                // Delete the source blob
                //sourceBlob.DeleteIfExistsAsync();
            }


            if (disclosureFileRenamed)
            {
                ViewData["Success"] = "CV File has been renamed to include your full name.";
            }
            else
            {
                ViewData["Error"] = "CV File could not be renamed. Please contact STFM for assistance.";
            }

            return View(disclosure);

        }




    }
}
