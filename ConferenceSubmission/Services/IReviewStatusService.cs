﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public interface IReviewStatusService
    {

        /// <summary>
        /// Gets ReviewStatus by ReviewStatusType
        /// </summary>
        /// <returns>ReviewStatus</returns>
        /// <param name="reviewStatusType">Review status type.</param>
        ReviewStatus GetReviewStatusByReviewStatusType(ReviewStatusType reviewStatusType);

        /// <summary>
        /// Gets the review status for review.
        /// </summary>
        /// <returns>The review status for review.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="reviewerId">Reviewer identifier.</param>
        ReviewStatus GetReviewStatusForReview(int submissionId, int reviewerId);


        /// <summary>
        /// Gets the review identifier for review - returns 0 if
        /// no review exists.
        /// </summary>
        /// <returns>The review identifier for review or 0 if
        /// no review exists.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="reviewerId">Reviewer identifier.</param>
        int GetReviewIdForReview(int submissionId, int reviewerId);

    }
}
