﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class AuditLogParentEntity
    {
        public int          AuditLogParentEntityId  { get; set; }
        public int          AuditLogId              { get; set; }
        public AuditLog     AuditLog                { get; set; }
        public int          ParentEntityId          { get; set; }
        public EntityType   ParentEntityType        { get; set; }
    }
}
