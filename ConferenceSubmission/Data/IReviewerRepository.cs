﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.DTOs.Review;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Data
{
    public interface IReviewerRepository
    {

        /// <summary>
        /// Get all reviewers.
        /// </summary>
        /// <value>Collection of Reviewer objects</value>
        IEnumerable<Reviewer> GetReviewers { get; }


        /// <summary>
        /// Gets the reviewer by stfm user identifier.
        /// </summary>
        /// <returns>The reviewer by stfm user identifier.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        Reviewer GetReviewerByStfmUserId(string stfmUserId);

        /// <summary>
        /// Creates the reviewer in the database.
        /// </summary>
        /// <returns>The reviewer id.</returns>
        /// <param name="reviewer">Reviewer.</param>
        int CreateReviewer(Reviewer reviewer);

        /// <summary>
        /// Gets the Submission Assignments for a particular reviewer.
        /// Includes only those submissions where the
        /// current date is less than the Conference Submission Review End Date.
        /// Note: the expectation is that the ConferenceSubmissionReviewEndDate field
        /// will include the time component. For example: 2018-2-12 23:59:49
        /// </summary>
        /// <param name="stfmUserId">Stfm user identifier</param>
        /// <returns>A list of Review Assignments</returns>
        List<SubmissionReviewer> GetAssignedSubmissions(string stfmUserId);

        /// <summary>
        /// Gets the Submission Assignments for a particular reviewer
        /// and for a particular conference.

        /// </summary>
        /// <param name="stfmUserId">Stfm user identifier</param>
        /// <param name="conferenceId">Conference id</param>
        /// <returns>A list of Review Assignments</returns>
        List<SubmissionReviewer> GetAssignedSubmissions(string stfmUserId, int conferenceId);

        /// <summary>
        /// Gets ALL the submission reviewer assignments for the provided
        /// STFMUserId.
        /// </summary>
        /// <param name="stfmUserId">Stfm user identifier</param>
        /// <returns>A list of Review Assignments</returns>
        List<SubmissionReviewer> GetAllAssignedSubmissions(string stfmUserId);

        bool ReviewerExists(string stfmUserId);

        /// <summary>
        /// Gets all the reviews assigned for a conference.
        /// </summary>
        /// <returns>The assigned reviews.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        List<SubmissionReviewer> GetAssignedReviews(int conferenceId);

        /// <summary>
        /// Gets all the reviews assigned to a particular person
        /// for a particular conference.
        /// </summary>
        /// <param name="stfmUserId">STFM User Id</param>
        /// <param name="conferenceId">Conference ID</param>
        /// <returns>Collection of submission reviewer objects</returns>
        List<SubmissionReviewer> GetAssignedReviews(string stfmUserId, int conferenceId);

        /// <summary>
        /// Gets the stfm user identifier by reviewer identifier.
        /// </summary>
        /// <returns>The stfm user identifier by reviewer identifier.</returns>
        /// <param name="reviewerId">Reviewer identifier.</param>
        Reviewer GetReviewerByReviewerId(int reviewerId);


        /// <summary>
        /// Get all the reviewers assigned to a specific submission
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns>Collection of submission reviewer objects</returns>
        List<SubmissionReviewer> GetAssignedReviewers(int submissionId);

        /// <summary>
        /// Returns a NextReview object that represents the next Unstarted submission review that is assigned to a reviewer
        /// Returns null if the reviewer has no unstarted reviews.
        /// </summary>
        /// <param name="stfmUserId">The Unique Id of the user</param>
        NextReview GetNextReview(string stfmUserId);
    }


}
