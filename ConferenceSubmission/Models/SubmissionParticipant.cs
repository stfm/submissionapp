﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceSubmission.Models
{
    /// <summary>
    /// Represents a multi join table for the
    /// Submission to Participant many-to-many
    /// relationship.
    /// </summary>
    /// 

    [Auditable]
    public class SubmissionParticipant
    {
        public SubmissionParticipant()
        {

            SubmissionParticipantToParticipantRolesLink = new List<SubmissionParticipantToParticipantRole>();

        }

        public int SubmissionParticipantId { get; set; }

        [ParentId]
        public int SubmissionId { get; set; }

        [ParentId]
        public int ParticipantId { get; set; }

        public Submission Submission { get; set; }

        public Participant Participant { get; set; }

        public int SortOrder { get; set; }

        public List<SubmissionParticipantToParticipantRole> SubmissionParticipantToParticipantRolesLink { get; set; }

        #region Helper Methods

        /// <summary>
        /// Convenience method that returns the presenter's submission roles as a nicely formatted string.
        /// If no roles exist for the user, returns an empty string.
        /// </summary>
        /// <returns>string</returns>
        public string GetRolesAsStringPretty()
        {
            if(SubmissionParticipantToParticipantRolesLink.Count > 0)
            {
                return string.Join(", ", SubmissionParticipantToParticipantRolesLink.Select(l => l.ParticipantRole.ParticipantRoleName.ToString().Replace('_', ' ')));
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion
    }
}
