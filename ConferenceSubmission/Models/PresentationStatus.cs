﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class PresentationStatus
    {
        public PresentationStatus()
        {
        }


        public int PresentationStatusId { get; set; }

        [StringLength(250)]
        [Required]
        public PresentationStatusType PresentationStatusName { get; set; }
    }
}
