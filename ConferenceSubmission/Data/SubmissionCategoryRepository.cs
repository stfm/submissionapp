﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ConferenceSubmission.Data
{
    public class SubmissionCategoryRepository : ISubmissionCategoryRepository
    {
        private readonly AppDbContext _appDbContext;

        public SubmissionCategoryRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public void CreateSubmissionCategory(SubmissionCategory category)
        {

            var submissionCategoryToAdd = new SubmissionCategory
            {
                CategoryAbstractInstructions = category.CategoryAbstractInstructions,
                CategoryAbstractMaxLength = category.CategoryAbstractMaxLength,
                ConferenceId = category.ConferenceId,
                SubmissionCategoryInactive = category.SubmissionCategoryInactive,
                SubmissionCategoryName = category.SubmissionCategoryName,
                SubmissionPaymentRequirement = category.SubmissionPaymentRequirement,
                CategoryInstructions = category.CategoryInstructions,

            };

            _appDbContext.Add(submissionCategoryToAdd);

            _appDbContext.SaveChanges();

        }

        public IEnumerable<SubmissionCategory> GetSubmissionCategories(int conferenceId)
        {


            return _appDbContext.SubmissionCategories.Where(sc => sc.ConferenceId == conferenceId);


        }

        public List<SubmissionCategory> GetSubmissionCategoriesWithFormFieldsByConference(int conferenceId)
        {
            var categories = 
                (from conference in _appDbContext.Conferences
                 join submissionCategory in _appDbContext.SubmissionCategories on conference.ConferenceId equals submissionCategory.ConferenceId
                 join formField in _appDbContext.FormFields on submissionCategory.SubmissionCategoryId equals formField.SubmissionCategoryId
                 where conference.ConferenceId == conferenceId && formField.FormFieldRole == "proposal" && !submissionCategory.SubmissionCategoryInactive
                 select submissionCategory).Distinct().ToList();

            return categories;
        }

        public SubmissionCategory GetSubmissionCategory(int submissionCategoryId)
        {

            return _appDbContext.SubmissionCategories.Include(sc => sc.Conference).Include(sc => sc.SubmissionCategoryToPresentationMethodsLink).ThenInclude(scp => scp.PresentationMethod).FirstOrDefault(sc => sc.SubmissionCategoryId == submissionCategoryId);

        }
    }
}
