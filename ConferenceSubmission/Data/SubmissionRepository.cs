﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.DTOs.PosterHall;

namespace ConferenceSubmission.Data
{
    public class SubmissionRepository : ISubmissionRepository
    {
        private readonly AppDbContext _appDbContext;

        public SubmissionRepository(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public int AddSubmission(Submission submission)
        {

            _appDbContext.Add(submission);

            _appDbContext.SaveChanges();

            return submission.SubmissionId;

        }

        public int AddSubmissionPaymentToSubmission(SubmissionPayment submissionPayment)
        {


            _appDbContext.SubmissionPayments.Add(submissionPayment);

            _appDbContext.SaveChanges();

            return submissionPayment.SubmissionPaymentId;


        }

        public Submission GetSubmissionAllDetailsBySubmissionId(int submissionId)
        {


            return _appDbContext.Submissions.Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                ThenInclude(sc => sc.SubmissionPresentationMethod).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.SubmissionPayment).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                    ThenInclude(sp => sp.ParticipantRole).
                                Include(s => s.VirtualMaterials).
                                Include(s=> s.VirtualConferenceSessions).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(spm => spm.PresentationMethod).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(spm => spm.PresentationStatus).
                                FirstOrDefault(s => s.SubmissionId == submissionId);


        }

        public Submission GetSubmissionBySubmissionId(int submissionId)
        {

            return _appDbContext.Submissions
                .Include(s => s.Conference)
                .Include(s => s.SubmissionCategory)
                .Include(s => s.SubmissionStatus)
                .Include(s => s.ParticipantLink)
                .ThenInclude(pl => pl.SubmissionParticipantToParticipantRolesLink)
                .ThenInclude(sp => sp.ParticipantRole)
                .Include(s => s.ParticipantLink)
                .ThenInclude(pl => pl.Participant)
                .FirstOrDefault(s => s.SubmissionId == submissionId);
        }

        public IEnumerable<Submission> GetSubmissions(string stfmUserId)
        {


            var result = _appDbContext.Submissions.FromSqlRaw("select s.SubmissionId, s.ConferenceId, s.AcceptedCategoryId, s.SubmissionTitle, s.SubmissionStatusId, s.SubmissionAbstract, s.SubmissionCategoryId, s.SubmissionCreatedDateTime, s.SubmissionLastUpdatedDateTime, s.SubmissionCompleted, s.SubmissionAdminEdited, s.ParticipantOrderModified from submissions s inner join SubmissionParticipant sp on s.SubmissionId = sp.SubmissionId inner join participants p on sp.ParticipantId = p.ParticipantId where stfmuserid = {0}", stfmUserId).
                                Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.SubmissionPayment).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                    ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                        ThenInclude(sp => sp.ParticipantRole).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(pm => pm.PresentationStatus).
                                Include(x => x.SubmissionPresentationMethods).
                                    ThenInclude(mn => mn.PresentationMethod);

            return result;

        }


        public IEnumerable<Submission> GetSubmissions(bool isSubmissionCompleted, int conferenceId)
        {
            return _appDbContext.Submissions.Where(s => s.SubmissionCompleted == isSubmissionCompleted && s.Conference.ConferenceId == conferenceId).
                                Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.SubmissionPayment).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole);
        }

        public Dictionary<string, int> GetSubmissionsByCategory(int conferenceId, bool submissionCompleted)
        {


            var submissionsByCategory = _appDbContext.Submissions.Where(x => x.SubmissionCompleted == submissionCompleted
           && x.Conference.ConferenceId == conferenceId && x.SubmissionStatusId != 4 && x.SubmissionStatusId != 5
            ).GroupBy(x => new { x.SubmissionCategory.SubmissionCategoryName }).Select(x => new { x.Key, Count = x.Count() }).ToList();

            var submissionsByCategoryMap = new Dictionary<string, int>();

            foreach (var key in submissionsByCategory)
            {

                submissionsByCategoryMap.Add(key.Key.SubmissionCategoryName, key.Count);

            }

            return submissionsByCategoryMap;

        }

        public Dictionary<string, int> GetSubmissionsPaidForByCategory(int conferenceId)
        {
            var submissionsByCategory = _appDbContext.Submissions.Where(x => x.Conference.ConferenceId == conferenceId && x.SubmissionPayment != null)
            .GroupBy(x => new { x.SubmissionCategory.SubmissionCategoryName }).Select(x => new { x.Key, Count = x.Count() }).ToList();

            var submissionsByCategoryMap = new Dictionary<string, int>();

            foreach (var key in submissionsByCategory)
            {

                submissionsByCategoryMap.Add(key.Key.SubmissionCategoryName, key.Count);

            }

            return submissionsByCategoryMap;
        }

        public IEnumerable<Submission> GetSubmissionsForConference(int conferenceId)
        {
            return _appDbContext.Submissions.Where(s => s.Conference.ConferenceId == conferenceId).
                                Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.VirtualConferenceSessions).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.SubmissionPayment).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole);
        }

        public IEnumerable<Submission> GetAcceptedSubmissionsForConference(int conferenceId)
        {
            return _appDbContext.Submissions.Where(s => s.Conference.ConferenceId == conferenceId && s.SubmissionStatusId == 3).
                                Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(spm => spm.PresentationMethod).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(spm => spm.PresentationStatus).
                                Include(s => s.VirtualConferenceSessions).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole)
                                .OrderBy(s => s.ConferenceSession.SessionStartDateTime)
                                .ThenBy(s => s.ConferenceSession.SessionCode);
        }

        public IEnumerable<Submission> GetAcceptedOnlineSubmissionsForConference(int conferenceId)
        {
            return _appDbContext.Submissions.Where(s => s.Conference.ConferenceId == conferenceId && s.SubmissionStatusId == 3).Where(s => s.SubmissionPresentationMethods.Any(spm => spm.PresentationMethodId == 3)).
                                Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(spm => spm.PresentationMethod).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(spm => spm.PresentationStatus).
                                Include(s => s.VirtualConferenceSessions).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole)
                                .OrderBy(s => s.ConferenceSession.SessionStartDateTime)
                                .ThenBy(s => s.ConferenceSession.SessionCode);
        }

        public IEnumerable<Submission> GetVirtualConferenceSubmissions(int conferenceId)
        {
            return _appDbContext.Submissions.Where(s => s.Conference.ConferenceId == conferenceId && s.SubmissionStatusId == 3).
                Where(s => s.SubmissionPresentationMethods.Any(spm => spm.PresentationMethodId == 3  && spm.PresentationStatusId==1)).
                                Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(spm => spm.PresentationMethod).
                                Include(s => s.SubmissionPresentationMethods).
                                    ThenInclude(spm => spm.PresentationStatus).
                                Include(s => s.VirtualConferenceSessions).
                                Include(s => s.VirtualMaterials).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole)
                                .OrderBy(s => s.ConferenceSession.SessionStartDateTime)
                                .ThenBy(s => s.ConferenceSession.SessionCode);
        }

        public void UpdateSubmission(Submission submission)
        {

            submission.SubmissionLastUpdatedDateTime = DateTime.Now;

            _appDbContext.Update<Submission>(submission);

            _appDbContext.SaveChanges();

        }

        public void UpdateSubmissionCompleted(int submissionId, bool submissionCompleted)
        {

            Submission submission = _appDbContext.Submissions.
                                   FirstOrDefault(s => s.SubmissionId == submissionId);

            submission.SubmissionCompleted = submissionCompleted;

            UpdateSubmission(submission);

        }

        public IEnumerable<Submission> GetSubmissions(IEnumerable<int> submissionIds)
        {
            return _appDbContext.Submissions
               .Where(s => submissionIds.Contains(s.SubmissionId)).
                                Include(s => s.SubmissionCategory).
                                Include(s => s.Conference).
                                Include(s => s.ConferenceSession).
                                Include(s => s.AcceptedCategory).
                                Include(s => s.SubmissionStatus).
                                Include(s => s.ParticipantLink).
                                ThenInclude(pl => pl.Participant).
                                Include(s => s.ParticipantLink).
                                   ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                                ThenInclude(sp => sp.ParticipantRole);
        }

        public IEnumerable<Submission> GetAcceptedSubmissionsForCategory(int acceptedCategoryId)
        {
            return _appDbContext.Submissions.Where(s => s.AcceptedCategoryId == acceptedCategoryId && s.SubmissionStatusId == 3).
                               Include(s => s.SubmissionCategory).
                               Include(s => s.Conference).
                               Include(s => s.ConferenceSession).
                               Include(s => s.AcceptedCategory).
                               Include(s => s.SubmissionStatus).
                               Include(s => s.ParticipantLink).
                               ThenInclude(pl => pl.Participant).
                               Include(s => s.ParticipantLink).
                                  ThenInclude(p => p.SubmissionParticipantToParticipantRolesLink).
                               ThenInclude(sp => sp.ParticipantRole).OrderBy(s => s.SubmissionId);
        }

        public void SetParticipantOrderModifiedToTrue(int submissionId)
        {
            var submission = _appDbContext.Submissions.FirstOrDefault(s => s.SubmissionId == submissionId);
            if (submission != null)
            {
                submission.ParticipantOrderModified = true;
                _appDbContext.SaveChanges();
            }
        }

        public List<int> GetSubmissionsWhoseParticipantOrderHasBeenModified(int conferenceId)
        {
            var submissions = _appDbContext.Submissions
                .Where(s => s.Conference.ConferenceId == conferenceId && s.ParticipantOrderModified)
                .OrderBy(s => s.SubmissionId)
                .Select(s => s.SubmissionId)
                .ToList();

            return submissions;
        }

        public Dictionary<string, int> GetSubmissionsByCategoryUsingStatus(int conferenceId, SubmissionStatusType submissionStatusType)
        {
            var submissionsByCategory = _appDbContext.Submissions.Where(x => x.SubmissionStatus.SubmissionStatusName == submissionStatusType
          && x.Conference.ConferenceId == conferenceId
           ).GroupBy(x => new { x.SubmissionCategory.SubmissionCategoryName }).Select(x => new { x.Key, Count = x.Count() }).ToList();

            var submissionsByCategoryMap = new Dictionary<string, int>();

            foreach (var key in submissionsByCategory)
            {

                submissionsByCategoryMap.Add(key.Key.SubmissionCategoryName, key.Count);

            }

            return submissionsByCategoryMap;
        }

        public List<SubmissionWithNameAndSessionCode> GetConferenceSubmissionsByNameAndSessionCode(int conferenceId)
        {
            var submissions = _appDbContext.Submissions.Where
                (
                    s => s.Conference.ConferenceId == conferenceId &&
                         s.SubmissionStatus.SubmissionStatusName != SubmissionStatusType.CANCELED &&
                         s.SubmissionStatus.SubmissionStatusName != SubmissionStatusType.WITHDRAWN &&
                         s.SubmissionStatus.SubmissionStatusName != SubmissionStatusType.DECLINED
                )
                .Include(s => s.ConferenceSession)
                .Select(s => new SubmissionWithNameAndSessionCode
                {
                    SessionCode = s.ConferenceSession.SessionCode,
                    SubmissionId = s.SubmissionId,
                    SubmissionTitle = s.SubmissionTitle
                }).ToList();

            return submissions;
        }

        public bool SubmissionExists(int submissionId)
        {
            return _appDbContext.Submissions.Any(s => s.SubmissionId == submissionId);
        }

        public List<PosterDTO> GetSubmissionsForPosterHallCategory(int submissionCategoryId)
        {
            var results =
            (from submission in _appDbContext.Submissions
             join category in _appDbContext.SubmissionCategories on submission.AcceptedCategoryId equals category.SubmissionCategoryId
             join virtualSession in _appDbContext.VirtualConferenceSession on submission.SubmissionId equals virtualSession.SubmissionId
             join submissionPresentationMethod in _appDbContext.SubmissionPresentationMethod on virtualSession.SubmissionPresentationMethodId equals submissionPresentationMethod.SubmissionPresentationMethodId
             join submissionParticipant in _appDbContext.SubmissionParticipant on submission.SubmissionId equals submissionParticipant.SubmissionId
             join participant in _appDbContext.Participants on submissionParticipant.ParticipantId equals participant.ParticipantId
             join submissionParticipantToParticipantRole in _appDbContext.SubmissionParticipantToParticipantRole on submissionParticipant.SubmissionParticipantId equals submissionParticipantToParticipantRole.SubmissionParticipantId
             join participantRole in _appDbContext.ParticipantRoles on submissionParticipantToParticipantRole.ParticipantRoleId equals participantRole.ParticipantRoleId
             join virtualMaterials in _appDbContext.VirtualMaterials on submission.SubmissionId equals virtualMaterials.SubmissionId
             where submission.SubmissionStatusId == 3 
                && category.SubmissionCategoryId == submissionCategoryId 
                && (virtualMaterials.MaterialType == "poster" || virtualMaterials.MaterialType == "link" || virtualMaterials.MaterialType == "handout") 
                && submissionPresentationMethod.PresentationMethodId == 3 
                && submissionPresentationMethod.PresentationStatusId == 1
             orderby submission.SubmissionId, submissionParticipant.SortOrder
             select new PosterDTO
             {
                 SubmissionId = submission.SubmissionId,
                 SubmissionTitle = submission.SubmissionTitle,
                 SubmissionAbstract = submission.SubmissionAbstract,
                 SessionCode = virtualSession.SessionCode,
                 SessionTrack = virtualSession.SessionTrack,
                 STFMUserId = participant.StfmUserId,
                 MaterialType = virtualMaterials.MaterialType,
                 MaterialValue = virtualMaterials.MaterialValue,
                 ParticipantRoleType = participantRole.ParticipantRoleName
             })
             .ToList();

            return results;
        }

        public List<SubmissionForRegistrationReport> GetSubmissionsForConferenceRegistrationReport(int conferenceId)
        {
            var result = (from submission in _appDbContext.Submissions
                          join submissionParticipant in _appDbContext.SubmissionParticipant on submission.SubmissionId equals submissionParticipant.SubmissionId
                          join submissionParticipantToParticipantRole in _appDbContext.SubmissionParticipantToParticipantRole on submissionParticipant.SubmissionParticipantId equals submissionParticipantToParticipantRole.SubmissionParticipantId
                          join participantRole in _appDbContext.ParticipantRoles on submissionParticipantToParticipantRole.ParticipantRoleId equals participantRole.ParticipantRoleId
                          join participant in _appDbContext.Participants on submissionParticipant.ParticipantId equals participant.ParticipantId
                          join session in _appDbContext.ConferenceSessions on submission.SubmissionId equals session.SubmissionId
                          where submission.Conference.ConferenceId == conferenceId && submission.SubmissionStatusId == 3
                          select new
                          {
                              SubmissionId = submission.SubmissionId,
                              SessionCode = session.SessionCode,
                              SubmissionTitle = submission.SubmissionTitle,
                              ParticipantRole = participantRole.ParticipantRoleName,
                              STFMUserId = participant.StfmUserId
                          })
                          .ToList()
                          .GroupBy(x => x.SubmissionId).Select(x => new SubmissionForRegistrationReport
                          {
                              SubmissionId = x.Key,
                              SubmissionTitle = x.FirstOrDefault().SubmissionTitle,
                              SessionCode = x.FirstOrDefault().SessionCode,
                              PresentersForRegistrationReport =
                                x.Select(y => new { STFMUserId = y.STFMUserId, Role = y.ParticipantRole })
                                 .GroupBy(y => y.STFMUserId)
                                 .Select(y => new PresenterForRegistrationReport
                                 {
                                     STFMUserId = y.Key,
                                     Roles = y.Select(z => z.Role).ToList()
                                 }).ToList()
                          }).ToList();

            return result;
        }

        public void UpdateSubmissionPresentationMethodStatus(int submissionId, PresentationMethodType presentationMethodType, PresentationStatusType presentationStatusType)
        {
            var submission = _appDbContext.Submissions
                .Include(x => x.SubmissionPresentationMethods)
                    .ThenInclude(x => x.PresentationMethod)
                .Include(x => x.SubmissionPresentationMethods)
                    .ThenInclude(x => x.PresentationStatus)
                .FirstOrDefault(x => x.SubmissionId == submissionId);

            if (submission != null)
            {
                var submissionPresentationMethodToUpdate = submission.SubmissionPresentationMethods?.FirstOrDefault(x => x.PresentationMethod.PresentationMethodName == presentationMethodType) ?? null;
                var newPresentationStatus = _appDbContext.PresentationStatus.FirstOrDefault(x => x.PresentationStatusName == presentationStatusType);

                if (submissionPresentationMethodToUpdate != null && newPresentationStatus != null)
                {
                    submissionPresentationMethodToUpdate.PresentationStatus = newPresentationStatus;

                    _appDbContext.SaveChanges();
                }                
            }
        }
    }
}