﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.DTOs.SalesforceEvent
{
    public class SFEvent
    {
        public string EventId { get; set; }
        public string EventName { get; set; }
        public string EventStartDate  { get; set; }
        public string EventEndDate { get; set; }
    }
}
