﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ExampleAddParticipantEmailViewModel
    {
        public string AddParticipantUrl { get; set; }
        public string ConferenceName    { get; set; }
        public string SubmissionTitle   { get; set; }
    }
}
