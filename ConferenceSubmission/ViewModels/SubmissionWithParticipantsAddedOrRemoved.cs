﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class SubmissionWithParticipantsAddedOrRemoved
    {
        public SubmissionWithParticipantsAddedOrRemoved()
        {
            ParticipantsAddedOrRemoved = new List<ParticipantAddedOrRemoved>();
        }

        public int      SubmissionId    { get; set; }
        public string   SubmissionTitle { get; set; }
        public string   SessionCode     { get; set; }
        public List<ParticipantAddedOrRemoved> ParticipantsAddedOrRemoved { get; set; }
    }

    public class ParticipantAddedOrRemoved
    {
        public string           ParticipantName             { get; set; }
        public string           STFMUserId                  { get; set; }
        public AddedOrRemoved   AddedOrRemoved              { get; set; }
        public DateTime         DateAddedOrRemoved          { get; set; }
        public string DateAddedOrRemovedAsShortDateString   { get; set; }

    }

    public enum AddedOrRemoved
    {
        ADDED,
        REMOVED
    }
}
