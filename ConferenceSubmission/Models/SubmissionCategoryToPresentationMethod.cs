﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class SubmissionCategoryToPresentationMethod
    {
        public SubmissionCategoryToPresentationMethod()
        {
        }

        public int SubmissionCategoryToPresentationMethodId { get; set; }

        public int SubmissionCategoryId { get; set; }

        public SubmissionCategory SubmissionCategory { get; set; }

        public int PresentationMethodId { get; set; }

        public PresentationMethod PresentationMethod { get; set; }

    }
}
