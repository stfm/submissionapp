﻿using ConferenceSubmission.DTOs.Review;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface IReviewerService
    {
        /// <summary>
        /// Gets the Submission Assignments for a particular reviewer.
        /// If the STFM user does not yet exist as a reviewer in the database,
        /// a new reviewer is created using the user's STFM User Id.
        /// </summary>
        /// <param name="stfmUserId">Stfm user identifier</param>
        /// <returns>A list of Review Assignments</returns>
        List<SubmissionReviewer> GetAssignedSubmissions(string stfmUserId);

        /// <summary>
        /// Creates a list AssignedSubmissionViewModel objects for a particular STFM user. 
        /// If the STFM user does not yet exist as a reviewer in the database,
        /// a new reviewer is created using the user's STFM User Id.
        /// </summary>
        /// <param name="stfmUserId">Stfm user identifier</param>
        /// <returns>A list of Review Assignments</returns>
        List<AssignedSubmissionViewModel> GetAssignedSubmissionWithReviewStatus(string stfmUserId);

        /// <summary>
        /// Creates a list AssignedSubmissionViewModel objects for a particular STFM user
        /// for a particular conference. 
        /// </summary>
        /// <param name="stfmUserId">Stfm user identifier</param>
        /// <param name="conferenceId">conference Id</param>
        /// <returns>A list of Review Assignments</returns>
        List<AssignedSubmissionViewModel> GetAssignedSubmissionWithReviewStatus(string stfmUserId, int conferenceId);

        List<ReviewerSummaryForConference> GetReviewerSummaryForConferences(string stfmUserId);

        /// <summary>
        /// Gets the reviewer.
        /// </summary>
        /// <returns>The reviewer.</returns>
        /// <param name="stfmUserId">Stfm user identifier.</param>
        Reviewer GetReviewer(string stfmUserId);

        /// <summary>
        /// Creates the reviewer in the database.
        /// </summary>
        /// <returns>The reviewer id.</returns>
        /// <param name="reviewer">Reviewer.</param>
        int CreateReviewer(Reviewer reviewer);

        /// <summary>
        /// Get all reviewers.
        /// </summary>
        /// <returns>Collection of Reviewer objects</returns>
        List<Reviewer> GetReviewers();

        /// <summary>
        /// Assign reviewer to a list of submissions.
        /// </summary>
        /// <param name="reviewerId">Reviewer ID</param>
        /// <param name="submissionIdList">Collection of submission IDs</param>
        void AssignReviewerToSubmissions(string reviewerId, List<string> submissionIdList);
        
        /// <summary>
        /// Unassign reviewer from list of submissions.
        /// </summary>
        /// <param name="reviewerId">Reviewer Id</param>
        /// <param name="submissionIdList">Collection of submission IDs</param>
        void UnassignReviewerFromSubmissions(string reviewerId, List<string> submissionIdList);

        /// <summary>
        /// Returns a NextReview object that represents the next Unstarted submission review that is assigned to a reviewer
        /// Returns null if the reviewer has no unstarted reviews.
        /// </summary>
        /// <param name="stfmUserId">The Unique Id of the user</param>
        NextReview GetNextReview(string stfmUserId);
    }
}
