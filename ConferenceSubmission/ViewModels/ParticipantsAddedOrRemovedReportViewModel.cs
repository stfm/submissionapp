﻿using ConferenceSubmission.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ParticipantsAddedOrRemovedReportViewModel
    {
        public ParticipantsAddedOrRemovedReportViewModel()
        {
            ConferencesByNameAndId = new List<EntityByNameAndId>();
        }
        
        public List<EntityByNameAndId> ConferencesByNameAndId { get; set; }
    }
}
