﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class ReviewerSubmissionDetailsViewModel
    {
        public Submission Submission { get; set; }
        public List<Presenter> Presenters { get; set; }
        public List<SubmissionFormField> SubmissionFormFields { get; set; }
    }
}
