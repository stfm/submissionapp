﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.BindingModels
{
    public class ProvideRecordedOnDemandPresentationBindingModel
    {
        [Required]
        public int SubmissionId { get; set; }
        [Required]
        public bool WillProvideRecordedOnDemandPresentation { get; set; }

        public string ConferenceName { get; set; }

        public string VirtualConferenceFAQ { get; set; }

        public string Deadline { get; set; }
    }
}
