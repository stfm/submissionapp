﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class VirtualMaterialsForConference
    {
        public Conference Conference { get; set; }

        public List<VirtualMaterialViewModel> VirtualMaterialViewModels { get; set; }

        public int startFile { get; set; }

        public int endFile { get; set; }

        public VirtualMaterialsForConference()
        {

            VirtualMaterialViewModels = new List<VirtualMaterialViewModel>();

        }
    }
}
