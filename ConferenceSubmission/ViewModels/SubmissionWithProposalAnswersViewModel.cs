﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// Models data for a submission that we need to show
    /// after user completes creating a new submission or updating an existing submission.
    /// Includes category proposal questions and answers.
    /// </summary>
    public class SubmissionWithProposalAnswersViewModel
    {

        public int SubmissionId { get; set; }

        public string SubmissionTitle { get; set; }

        public string SubmissionCategoryName { get; set; }

        public string SubmissionAcceptedCategoryName { get; set; }

        public SubmissionStatusType SubmissionStatus { get; set; }

        public string SubmissionAbstract { get; set; }

        public string AddParticipantUrl { get; set; }

        public List<SubmissionFormField> SubmissionFormFields { get; set; }

        public List<Presenter> Presenters { get; set; }

        public string sessionCode { get; set; }

        public DateTime sessionStartDatetime { get; set; }

        public DateTime sessionEndDatetime { get; set; }

        public string sessionLocation { get; set; }

        public string sessionTrack { get; set; }

        public IEnumerable<SelectListItem> SubmissionStatusTypes() =>
            Enum.GetValues(typeof(SubmissionStatusType))
                .Cast<SubmissionStatusType>()
                .Select(s => new SelectListItem
                {
                    Value = ((int)s).ToString(),
                    Text = s.ToString()
                }).ToList();

        public IEnumerable<SelectListItem> PresentationMethodStatusTypes() =>
            Enum.GetValues(typeof(PresentationStatusType))
                .Cast<PresentationStatusType>()
                .Select(s => new SelectListItem
                {
                    Value = ((int)s).ToString(),
                    Text = s.ToString()
                }).ToList();


        public IEnumerable<SelectListItem> SubmissionCategories { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="T:ConferenceSubmission.ViewModels.SubmissionWithProposalAnswersViewModel"/> submission completed.
        /// A submission is considered completed if user has answered all the category
        /// specific proposal form questions where an answer is required.
        /// </summary>
        /// <value><c>true</c> if submission completed; otherwise, <c>false</c>.</value>
        public bool SubmissionCompleted { get; set; }

        /// <summary>
        /// Stores the submission payment requirement for this submission category.
        /// </summary>
        /// <value>The submission payment requirement.</value>
        public SubmissionPaymentRequirement SubmissionPaymentRequirement { get; set; }

        /// <summary>
        /// Gets or sets the submission payment amount paid.
        /// </summary>
        /// <value>The submission payment amount paid. If the amount paid is 0
        /// and SubmissionPaymentRequirement is not NONE then user did not 
        /// pay for the submission.</value>
        public int SubmissionPaymentAmountPaid { get; set; }

        /// <summary>
        /// Gets or sets the submission fee.
        /// </summary>
        /// <value>The submission fee - 0 if there is no submission fee.</value>
        public int SubmissionFee { get; set; }

        /// <summary>
        /// This value is needed for determining if user needs to complete
        /// or update disclosure.
        /// </summary>
        /// <value>The conference start date.</value>
        public DateTime ConferenceStartDate { get; set; }

        /// <summary>
        /// Is the disclosure current for the logged in user?
        /// </summary>
        /// <value><c>true</c> if is disclosure current; otherwise, <c>false</c>.</value>
        public bool isDisclosureCurrent { get; set; }

        public string ConferenceName { get; set; }

        public DateTime ConferenceCfpEndDate { get; set; }

        public List<VirtualMaterial> VirtualMaterials { get; set; }

        public bool ConferenceVirtual { get; set; }

        public List<SubmissionPresentationMethod> SubmissionPresentationMethods { get; set; }

        public SubmissionPresentationMethod sessionPresentationMethod { get; set; }

        public List<VirtualConferenceSession> VirtualConferenceSessions { get; set; }


        public override string ToString()
        {

            string submissionFormFieldString = "";



            foreach (SubmissionFormField submissionFormField in SubmissionFormFields)
            {

                string question = submissionFormField.FormField.GetPropertyValue("label");

                if (!question.Equals(""))
                {

                    submissionFormFieldString = submissionFormFieldString + " | Question: " + question +
                        " | Answer: " + submissionFormField.SubmissionFormFieldValue;

                }



            }

            string presentersString = "";

            foreach (Presenter presenter in Presenters) {

                presentersString = presentersString + " - " + presenter.LastName;


            }

            return "SubmissionId: " + SubmissionId + " | Encrypted Submission ID: "
                + AddParticipantUrl + " | SubmissionTitle: " +
                SubmissionTitle + " | SubmissionCategoryName: " +
                SubmissionCategoryName + " | SubmissionAcceptedCategoryName: " + SubmissionAcceptedCategoryName +
                " | SubmissionStatus: " + SubmissionStatus +
                " | abstract: " + SubmissionAbstract +
                " | Submission completed: " + SubmissionCompleted +
                " | Submission Payment Requirement: " + SubmissionPaymentRequirement +
                " | cateogry proposal questions/answers: " + submissionFormFieldString +
                " | Presenters: " + presentersString +
                " | Session Code: " + sessionCode +
                " | Session Start Datetime: " + sessionStartDatetime +
                " | Session End Datetime: " + sessionEndDatetime +
                " | Session Location: " + sessionLocation +
                " | Session Track: " + sessionTrack
                ;

        }

        public bool CanAddConferenceSession(PresentationMethodType presentationMethodType)
        {
            if (presentationMethodType == PresentationMethodType.LIVE_IN_PERSON)
            {
                return SubmissionPresentationMethods.Any(x => x.PresentationMethod.PresentationMethodName == PresentationMethodType.LIVE_ONLINE) && string.IsNullOrWhiteSpace(sessionCode);
            }
            else
            {
                return SubmissionPresentationMethods.Any(x => x.PresentationMethod.PresentationMethodName == presentationMethodType && x.PresentationStatus.PresentationStatusName != PresentationStatusType.PENDING && x.PresentationStatus.PresentationStatusName != PresentationStatusType.DECLINED) && !VirtualConferenceSessions.Any(x => x.SubmissionPresentationMethod.PresentationMethod.PresentationMethodName == presentationMethodType);
            }
        }
    }
}
