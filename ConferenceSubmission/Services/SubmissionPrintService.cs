﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using DinkToPdf;
using DinkToPdf.Contracts;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public class SubmissionPrintService : ISubmissionPrintService
    {
        private readonly IRazorViewEngine   _razorViewEngine;
        private readonly ITempDataProvider  _tempDataProvider;
        private readonly IServiceProvider   _serviceProvider;
        private readonly IConverter         _converter;

        public SubmissionPrintService(
            IRazorViewEngine razorViewEngine,
            ITempDataProvider tempDataProvider,
            IServiceProvider serviceProvider,
            IConverter converter)
        {
            _razorViewEngine    = razorViewEngine;
            _tempDataProvider   = tempDataProvider;
            _serviceProvider    = serviceProvider;
            _converter          = converter;
        }

        public byte[] GetPdfAsByteArray(SubmissionPrintViewModel submissionPrintViewModel)
        {
            return _converter.Convert(new HtmlToPdfDocument
            {
                Objects =
                {
                    new ObjectSettings()
                    {
                        HtmlContent = RenderViewToString(submissionPrintViewModel, "/Views/Submission/SubmissionPrint.cshtml")
                    }
                }
            });
        }

        /// <summary>
        /// Generates an html document in string form rendered from a Razor view and a view model.
        /// </summary>
        /// <typeparam name="T">Type of the generic model</typeparam>
        /// <param name="model">A generic model class</param>
        /// <param name="pathToView">Relative path to the view (relative from the root of the web project)</param>
        /// <returns>string</returns>
        public string RenderViewToString<T>(T model, string pathToView)
        {
            var httpContext = new DefaultHttpContext { RequestServices = _serviceProvider };
            var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());


            using (var sw = new StringWriter())
            {
                var viewResult = _razorViewEngine.GetView(null, pathToView, false);

                if (viewResult.View == null)
                {
                    throw new ArgumentNullException($"{pathToView} does not match any available view");
                }

                var viewDictionary =
                    new ViewDataDictionary(
                        new EmptyModelMetadataProvider(),
                        new ModelStateDictionary())
                    { Model = model };

                var viewContext = new ViewContext(
                    actionContext,
                    viewResult.View,
                    viewDictionary,
                    new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                    sw,
                    new HtmlHelperOptions());

                viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }

    }
}
