﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.BindingModels
{
    public class EditBaseSubmissionDetailsBindingModel
    {
        [Required]
        public int SubmissionId { get; set; }

        public string ConferenceLongName { get; set; }

        public string Category { get; set; }

        public User User { get; set; }

        [Required(ErrorMessage = " | The title is required"), MaxLength(125, ErrorMessage = " | A title cannot exceed 125 characters including spaces")]
        [Display(Name = "Title (125 characters including spaces - DO NOT use all capitals)")]
        public string SubmissionTitle { get; set; }

        [Required(ErrorMessage = " | The abstract is required")]
        [Display(Name = "Abstract")]
        public string SubmissionAbstract { get; set; }

        public string CategoryAbstractInstructions { get; set; }

        public int CategoryAbstractMaxLength { get; set; }
    }
}
