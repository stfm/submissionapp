﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
namespace ConferenceSubmission.ViewModels

{
    public class ConferenceUserMultipleSubmissionsViewModel
    {
        public ConferenceUserMultipleSubmissionsViewModel()
        {
        }

        public Conference Conference { get; set; }

        public List<UserMultipleSubmissionsViewModel> UserMultipleSubmissionsViewModels { get; set; }
    }
}
