﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Data
{
    public class AuditLogRepository : IAuditLogRepository
    {
        private readonly AppDbContext _context;

        public AuditLogRepository(AppDbContext context)
        {
            _context = context;
        }

        public List<AuditLog> GetByEntityId(int entityId, EntityType entityType)
        {
            var auditLogs = _context.AuditLogs
                .Where(a => a.EntityId == entityId && a.EntityType == entityType)
                .Union
                (
                    _context.AuditLogParentEntities
                        .Where(a => a.ParentEntityId == entityId && a.ParentEntityType == entityType)
                        .Select(a => a.AuditLog)
                )
                .OrderBy(x => x.TimeStamp)
                .ToList();

            return auditLogs;
        }

        public List<AuditLog> GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType(List<int> parentEntityIds, EntityType parentEntityType, AuditLogActionType childAuditLogActionType, EntityType childEntityType)
        {
            var childAuditLogs = 
                _context.AuditLogParentEntities
                        .Where(a => parentEntityIds.Contains(a.ParentEntityId) && a.ParentEntityType == parentEntityType && a.AuditLog.EntityType == childEntityType && a.AuditLog.AuditLogActionType == childAuditLogActionType)
                        .Select(a => a.AuditLog)
                        .ToList();

            return childAuditLogs;
        }

        public Dictionary<int, int> GetParentIdsForEntities(List<int> auditLogIds, EntityType parentEntityType)
        {
            var results =
                (
                    from auditLog in _context.AuditLogs
                    join auditLogParentEntity in _context.AuditLogParentEntities on auditLog.AuditLogId equals auditLogParentEntity.AuditLogId
                    where auditLogIds.Contains(auditLog.AuditLogId) && auditLogParentEntity.ParentEntityType == parentEntityType
                    select new { auditLog.EntityId, auditLogParentEntity.ParentEntityId }
                 )
                 .ToList()
                 .ToDictionary(x => x.EntityId, x => x.ParentEntityId);
           
            return results;
        }
    }

    public interface IAuditLogRepository
    {
        List<AuditLog> GetByEntityId(int entityId, EntityType entityType);
        List<AuditLog> GetChildAuditLogsFilterByChildAuditLogActionTypeAndChildEntityType(List<int> parentEntityIds, EntityType parentEntityType, AuditLogActionType childAuditLogActionType, EntityType childEntityType);
        Dictionary<int, int> GetParentIdsForEntities(List<int> auditLogIds, EntityType parentEntityType);
    }
}
