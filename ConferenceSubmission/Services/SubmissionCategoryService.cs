﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using System.Linq;

namespace ConferenceSubmission.Services
{
    public class SubmissionCategoryService : ISubmissionCategoryService
    {

        private readonly IMapper _mapper;

        private readonly ISubmissionCategoryRepository _submissionCategoryRepository;

        public SubmissionCategoryService(IMapper mapper, ISubmissionCategoryRepository submissionCategoryRepository)
        {

            _mapper = mapper;

            _submissionCategoryRepository = submissionCategoryRepository;

        }

        public List<SubmissionCategoryViewModel> GetActiveSubmissionCategories(int ConferenceId) 
        {
            return GetSubmissionCategories(ConferenceId)
                .Where(cs => cs.SubmissionCategoryInactive == false).ToList();
        }

        public List<SubmissionCategoryViewModel> GetSubmissionCategories(int ConferenceId)
        {
            // Due to breaking changes in EF Core 3.1, we must explicitely call ToList() on queries that return an interface like IEnumerable, otherwise
            // we get the following error: "There is already an open datareader associated with this connection which must be closed"
            // For more information, checkout the comments in this post: https://stackoverflow.com/q/59677609
            List<SubmissionCategoryViewModel> submissionCategoriesViewModelList = 
                _mapper.Map<IEnumerable<SubmissionCategory>, List<SubmissionCategoryViewModel>>(_submissionCategoryRepository.GetSubmissionCategories(ConferenceId).ToList() );

            return submissionCategoriesViewModelList;


        }

        public SubmissionCategory GetSubmissionCategory(int submissionCategoryId)
        {

            return _submissionCategoryRepository.GetSubmissionCategory(submissionCategoryId);

        }

    }
}
