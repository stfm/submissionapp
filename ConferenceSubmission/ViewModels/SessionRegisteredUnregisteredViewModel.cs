﻿using System.Collections.Generic;

namespace ConferenceSubmission.ViewModels
{
    public class SessionRegisteredUnregisteredViewModel
    {

            public SessionRegisteredUnregisteredViewModel()
            {
                SessionRegisteredUnregisteredList = new List<SessionRegisteredUnRegistered>();
            }

            public string ConferenceName { get; set; }
            public string EventName { get; set; }

            public List<SessionRegisteredUnRegistered> SessionRegisteredUnregisteredList { get; set; }


    }

    public class SessionRegisteredUnRegistered
    {

        public string SessionCode { get; set; }
        public int SubmissionId { get; set; }

        public int Registered { get; set; }

        public int Unregistered { get; set; }
    }
}
