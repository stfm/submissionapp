﻿using System;
namespace ConferenceSubmission.Services
{
    public interface IEncryptionService
    {

        /// <summary>
        /// Encrypts the string.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="stringToEncrypt">String to encrypt.</param>
        string EncryptString(string stringToEncrypt);

        /// <summary>
        /// Decrypts the string.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="stringToDecrypt">String to decrypt.</param>
        string DecryptString(string stringToDecrypt);


    }
}
