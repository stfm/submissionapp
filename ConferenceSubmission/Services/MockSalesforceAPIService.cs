﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ConferenceSubmission.DTOs.SalesforceEvent;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.Services
{
    public class MockSalesforceAPIService : ISalesforceAPIService
    {
        public MockSalesforceAPIService()
        {
        }

        public Task<IEnumerable<User>> FindPeopleByLastName(string lastName)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<EventAttendee>> GetEventAttendees(string eventId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<SFEvent>> GetEventsByDateRange(DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<User>> GetMultipleUserInfo(IEnumerable<string> stfmUserIds)
        {
            return Task.FromResult<IEnumerable<User>>(new List<User>() {
                new User
                {
                    FirstName = "Bruce",
                    LastName = "Phillips",
                    STFMUserId = "3001",
                    Email = "bphillip@stfm.org",
                    UserName = "bphillip"
                },
                new User
                {
                    FirstName = "John",
                    LastName = "Doe",
                    STFMUserId = "3002",
                    Email = "johndoe@stfm.org",
                    UserName = "johndoe"
                },
                new User
                {
                    FirstName = "Jane",
                    LastName = "Doe",
                    STFMUserId = "3003",
                    Email = "janedoe@stfm.org",
                    UserName = "janedoe"
                },
            });
        }

        public async Task<User> GetUserInfo(string stfmUserId)
        {
            return new User
            {
                FirstName = "Bruce",
                LastName = "Phillips",
                STFMUserId = "1234567",
                Email = "bphillip@stfm.org",
                UserName = "bphillip"
            };
        }

        public bool isAdminUser(IEnumerable<Claim> claims)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsSTFMMember(string stfmUserId)
        {
            throw new NotImplementedException();
        }
    }
}
