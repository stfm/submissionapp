﻿using ConferenceSubmission.Mappers;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using ConferenceSubmission.BindingModels;
using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

namespace ConferenceSubmission.Controllers
{
    [Authorize]
    public class ReviewController : Controller
    {
        private readonly IReviewerService _reviewerService;
        private readonly IUserMapper _userMapper;
        private readonly ISubmissionService _submissionService;
        private readonly IConferenceService _conferenceService;
        private readonly IReviewStatusService _reviewStatusService;
        private readonly IFormFieldService _formFieldService;
        private readonly IReviewService _reviewService;
        private readonly IReviewFormFieldService _reviewFormFieldService;

        public ReviewController(IReviewerService reviewerService, IUserMapper userMapper, ISubmissionService submissionService,
                                IConferenceService conferenceService, IReviewStatusService reviewStatusService,
                                IFormFieldService formFieldService, IReviewService reviewService,
                                IReviewFormFieldService reviewFormFieldService)
        {

            _reviewerService = reviewerService;
            _userMapper = userMapper;
            _submissionService = submissionService;
            _conferenceService = conferenceService;
            _reviewStatusService = reviewStatusService;
            _formFieldService = formFieldService;
            _reviewService = reviewService;
            _reviewFormFieldService = reviewFormFieldService;

        }
        /// <summary>
        /// "Home" Page for Submission Review
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model = new ReviewHomePageViewModel();
            var user = _userMapper.Map(User.Claims);
            model.AssignedSubmissions = _reviewerService.GetAssignedSubmissionWithReviewStatus(user.STFMUserId);
            model.ConferencesWithOpenReviews = _conferenceService.GetConferencesWithOpenReviewStatus();
            return View(model);
        }


        /// <summary>
        /// Responds to review's request to view a specific submission
        /// </summary>
        /// <returns>(reviewer) View for a specific submision</returns>
        /// <param name="submissionId">Submission identifier.</param>
        public ActionResult Submission(int submissionId)
        {
            var submission = _submissionService.GetSubmission(submissionId);
            var model = new ReviewerSubmissionDetailsViewModel
            {
                Submission = submission,
                Presenters = _submissionService.BuildPresentersListFromSubmissionParticipants(submission.ParticipantLink),
                SubmissionFormFields = _submissionService.GetSubmissionFormFields(submissionId)
            };
            return View(model);
        }

        /// <summary>
        /// Responds to the user's initial request to create submission review.
        /// </summary>
        /// <returns>The review input.</returns>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="categoryId">Category identifier.</param>
        public IActionResult SubmissionReviewInput(string submissionId, int categoryId)
        {

            Log.Information($"User wants to create submission review for submissionId {submissionId} and categoryId {categoryId}");

            var submission = _submissionService.GetSubmission(Int32.Parse(submissionId));

            SubmissionReviewBindingModel submissionReviewBindingModel = new SubmissionReviewBindingModel
            {
                Submission = submission,

                FormFields = _formFieldService.GetFormFields(categoryId, "review"),

                Presenters = _submissionService.BuildPresentersListFromSubmissionParticipants(submission.ParticipantLink),

                SubmissionFormFields = _submissionService.GetSubmissionFormFields(Int32.Parse(submissionId))
            };

            return View(submissionReviewBindingModel);

        }

        /// <summary>
        /// Responds to user's http post request to create submission review.
        /// </summary>
        /// <returns>Redirects to SaveReviewSuccess Action</returns>
        /// <param name="submissionReviewBindingModel">Base submission binding model - user's
        /// input for the review submission data.</param>
        public RedirectToActionResult CreateSubmissionReview(SubmissionReviewBindingModel submissionReviewBindingModel)
        {

            var user = _userMapper.Map(User.Claims);

            /*
             * Before saving the review check that a review by this reviewer for 
             * this submission does not already exist.  It is possible that the
             * reviewer is creating a review for this submission but he already
             * created it in another tab.            
             */


           bool reviewExists =  _reviewService.CheckIfReviewExists(_reviewerService.GetReviewer(user.STFMUserId).ReviewerId,
                submissionReviewBindingModel.SubmissionId);


            if (reviewExists)
            {

                return RedirectToAction(actionName: "Index", controllerName: "Review");

            }

            Review review = new Review
            {

                ReviewDateTime = DateTime.Now,

                ReviewerId = _reviewerService.GetReviewer(user.STFMUserId).ReviewerId,

                SubmissionId = submissionReviewBindingModel.SubmissionId,

                ReviewStatusId = _reviewStatusService.GetReviewStatusByReviewStatusType(ReviewStatusType.INCOMPLETE).ReviewStatusId


            };

            review.ReviewId = _reviewService.SaveReview(review);

            Log.Information($"Review for submission id of {review.SubmissionId} by reviewer {review.ReviewerId} was successfully saved to the database.  Review ID is {review.ReviewId}");

            /* Loop over the Form Map which contains the form field names and values
             * We do this since the form fields were dynamically created at runtime
             */
            foreach (var formField in HttpContext.Request.Form)
            {
            

                /* For each formField that has a name which starts with FIELD 
                 * and the value is not null or empty string and the value does not
                 * contain "Select" (that is the first answer in the select options and 
                 * means the user did not select an option then
                 * we want to save
                 * that formField's value and fieldId into the database - this is the user's
                 * answer to a review form question
                 */
                if (formField.Key.StartsWith("FIELD", StringComparison.CurrentCulture) &&
                    !String.IsNullOrEmpty(formField.Value) &&
                    !formField.Value.ToString().Contains("Select"))
                {

                    var fieldId = formField.Key.Split("_")[1];


                    ReviewFormField reviewFormField = new ReviewFormField
                    {

                        ReviewId = review.ReviewId,

                        FormFieldId = Int32.Parse(fieldId),

                        ReviewFormFieldValue = formField.Value


                    };

                    _reviewFormFieldService.AddReviewFormField(reviewFormField);

                }
            }

            bool isReviewComplete = _reviewService.IsSubmissionCategoryReviewCompleted(review.ReviewId);

            Log.Information($"Did reviewer answer all the required review questions: {isReviewComplete}");

            if (isReviewComplete)
            {

                review.ReviewStatusId = _reviewStatusService.GetReviewStatusByReviewStatusType(ReviewStatusType.COMPLETE).ReviewStatusId;

            }
            else
            {


                review.ReviewStatusId = _reviewStatusService.GetReviewStatusByReviewStatusType(ReviewStatusType.INCOMPLETE).ReviewStatusId;

            }

            _reviewService.UpdateReview(review);

            if (submissionReviewBindingModel.IsSaveAndNext)
            {
                var nextReview = _reviewerService.GetNextReview(user.STFMUserId);

                if (nextReview == null)
                {
                    //If the user has no more Unstarted reviews, just return them to the review home page
                    return RedirectToAction("Index");
                }

                return RedirectToAction("SubmissionReviewInput", nextReview);
            }

            return RedirectToAction(actionName: "SaveReviewSuccess", controllerName: "Review", routeValues: new { reviewId = review.ReviewId });

        }

        /// <summary>
        /// Redirects to the SaveReviewSuccess view.
        /// </summary>
        /// <returns>The SaveReviewSuccess view.</returns>
        /// <param name="reviewId">Review identifier.</param>
        public IActionResult SaveReviewSuccess(int reviewId)
        {

            Review review = _reviewService.GetReview(reviewId);

            Submission submission = _submissionService.GetSubmission(review.Submission.SubmissionId);

            Conference conference = _conferenceService.GetConference(submission.Conference.ConferenceId);

            ReviewWithReviewerAnswersViewModel reviewerAnswersViewModel = new ReviewWithReviewerAnswersViewModel
            {

                Review = review,

                Submission = submission

            };

            return View(reviewerAnswersViewModel);

        }


        /// <summary>
        /// Respond to user request to edit a specific review.
        /// </summary>
        /// <returns>The review.</returns>
        /// <param name="reviewId">Review identifier.</param>
        [HttpGet]
        public IActionResult EditReview(int reviewId)
        {

            Log.Information($"User wants to edit review id {reviewId}");

            var review = _reviewService.GetReview(reviewId);

            var submission = _submissionService.GetSubmission(review.SubmissionId);

            return View(new EditReviewBindingModel
            {
                ReviewId = reviewId,

                ReviewFormFields = _reviewFormFieldService.GetFormFieldsForReview(reviewId).ToList(),

                FormFields = _formFieldService.GetFormFields(review.Submission.SubmissionCategoryId, "review").ToList(),

                Submission = submission,

                Presenters = _submissionService.BuildPresentersListFromSubmissionParticipants(submission.ParticipantLink),

                SubmissionFormFields = _submissionService.GetSubmissionFormFields(submission.SubmissionId)

            });


        }

        /// <summary>
        /// Responds to user post request to update review answers.
        /// </summary>
        /// <returns>The review.</returns>
        /// <param name="model">EditReviewBindingModel</param>
        [HttpPost]
        public IActionResult EditReview(EditReviewBindingModel model)
        {
            Log.Information($"User submitted updated review answers for review id {model.ReviewId}");

            var user = _userMapper.Map(User.Claims);

            if (ModelState.IsValid)
            {
                //----------------------------------------------------------------------------------------------------------------
                //Find the Review object for this review

                Review review = _reviewService.GetReview(model.ReviewId);

                //Delete all existing ReviewFormFields for that review

                _reviewFormFieldService.RemoveAllReviewFormFieldsForReview(review.ReviewId);

                //Iterate over the Request's Form's KeyValuePairs. If the value is NOT null or empty and the value is not "Select",
                //create a new ReviewFormField for the Review.
                //Save the ReviewFormField to the database

                /* Loop over the Form Map which contains the form field names and values
                * We do this since the form fields were dynamically created at runtime
                */
                foreach (var formField in HttpContext.Request.Form)
                {
                

                    /* For each formField that has a name which starts with FIELD 
                     * and the value is not null or empty string and the value does not
                     * contain "Select" (that is the first answer in the select options and 
                     * means the user did not select an option then
                     * we want to save
                     * that formField's value and fieldId into the database - this is the user's
                     * answer to a review form question
                     */
                    if (formField.Key.StartsWith("FIELD", StringComparison.CurrentCulture) &&
                        !String.IsNullOrEmpty(formField.Value) &&
                        !formField.Value.ToString().Contains("Select"))
                    {

                        var fieldId = formField.Key.Split("_")[1];


                        ReviewFormField reviewFormField = new ReviewFormField
                        {

                            ReviewId = review.ReviewId,

                            FormFieldId = Int32.Parse(fieldId),

                            ReviewFormFieldValue = formField.Value


                        };

                        _reviewFormFieldService.AddReviewFormField(reviewFormField);

                    }
                }

                //Update the Review object's ReviewDateTime

                review.ReviewDateTime = DateTime.Now;

                //Update the Review object's ReviewStatus

                bool isReviewComplete = _reviewService.IsSubmissionCategoryReviewCompleted(review.ReviewId);

                Log.Information($"Did reviewer answer all the required review questions: {isReviewComplete}");

                if (isReviewComplete)
                {

                    review.ReviewStatusId = _reviewStatusService.GetReviewStatusByReviewStatusType(ReviewStatusType.COMPLETE).ReviewStatusId;

                }
                else
                {


                    review.ReviewStatusId = _reviewStatusService.GetReviewStatusByReviewStatusType(ReviewStatusType.INCOMPLETE).ReviewStatusId;

                }

                //Update the Review in the database
                _reviewService.UpdateReview(review);

                return RedirectToAction(actionName: "SaveReviewSuccess", controllerName: "Review", routeValues: new { reviewId = model.ReviewId });

            }

            return View();
        }
    }
}


