﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;
using ConferenceSubmission.Data;
using System.Linq;

namespace ConferenceSubmissionTest
{
    public class FormFieldRepositoryTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public FormFieldRepositoryTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;

        }

        [Fact]
        public void UpdateSubmissionFormFieldTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using(var context = new AppDbContext(options))
            {
                //Arrange
                FormFieldRepository formFieldRepository = new FormFieldRepository(context);

                //Act
                formFieldRepository.UpdateSubmissionFormField(2, "NEW VALUE");

                //Assert
                Assert.Equal("NEW VALUE", formFieldRepository.GetFormFieldAnswerForSubmission(2, 10001));
            }
        }

        [Fact]
        public void GetFormFieldsTest() {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                FormFieldRepository formFieldRepository = new FormFieldRepository(context);

                IEnumerable<FormField> formFields = formFieldRepository.GetFormFields(1000, "proposal");

                foreach (FormField formField in formFields) {


                    Output.WriteLine("Form field type is " + formField.FormFieldType);

                    Output.WriteLine("Form sort order is " + formField.FormFieldSortOrder);

                    foreach (var formFieldProperty in formField.FormFieldProperties)
                    {

                        Output.WriteLine("For field property name is " + formFieldProperty.PropertyName + " and field property value is " + formFieldProperty.PropertyValue);


                    }

                }

                Assert.Equal(4, formFields.Count());

            }

        }

        [Fact]
        public void GetFormFieldsAnswerRequiredTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                FormFieldRepository formFieldRepository = new FormFieldRepository(context);

                IEnumerable<FormField> formFields = formFieldRepository.GetFormFieldsAnswerRequired(1000, "proposal");

                foreach (FormField formField in formFields)
                {
                    


                    Output.WriteLine("Form field id is " + formField.FormFieldId);

                    Output.WriteLine("Form field answer required is " + formField.AnswerRequired);

                }

                Assert.Equal(3, formFields.Count());

            }

        }


        [Fact]
        public void GetSubmissionFormFieldsTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                FormFieldRepository formFieldRepository = new FormFieldRepository(context);

                IEnumerable<SubmissionFormField> submissionformFields = formFieldRepository.GetFormFieldsForSubmission(10001);

                foreach (SubmissionFormField submissionFormField in submissionformFields)
                {

                    Output.WriteLine("Form field type is " + submissionFormField.FormField.FormFieldType);

                    foreach (var formFieldProperty in submissionFormField.FormField.FormFieldProperties)
                    {

                        Output.WriteLine("For field property name is " + formFieldProperty.PropertyName + " and field property value is " + formFieldProperty.PropertyValue);


                    }


                    Output.WriteLine("Submission form field id is " + submissionFormField.FormFieldId);

                    Output.WriteLine("Form field sort order is " + submissionFormField.FormField.FormFieldSortOrder);

                    Output.WriteLine("Submission form field value is " + submissionFormField.SubmissionFormFieldValue);


                }

                Assert.Equal(3, submissionformFields.Count());

            }

        }

        [Fact]
        public void RemoveAllSubmissionFormFieldsTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();
            using(var context = new AppDbContext(options))
            {
                //arranage
                var formFieldRepository     = new FormFieldRepository(context);
                var submission              = context.Submissions.FirstOrDefault();
                var submissionFormFields    = context.SubmissionFormFields.Where(s => s.SubmissionId == submission.SubmissionId).ToList();

                //act
                formFieldRepository.RemoveAllFormFieldsForSubmission(submission.SubmissionId);
                var submissionFormFieldsAfterRemove = context.SubmissionFormFields.Where(s => s.SubmissionId == submission.SubmissionId).ToList();

                //assert
                Assert.True(submissionFormFields.Count != submissionFormFieldsAfterRemove.Count);
                Assert.True(submissionFormFieldsAfterRemove.Count == 0);
            }
        }


        [Fact]
        public void GetSubmissionFormFieldValueTest()
        {

            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {


                FormFieldRepository formFieldRepository = new FormFieldRepository(context);

                string submissionFormFieldValue = formFieldRepository.GetFormFieldAnswerForSubmission(4, 10001);

                Output.WriteLine("Submission form field value is " + submissionFormFieldValue);

                Assert.Equal("I need 5 chairs", submissionFormFieldValue);



            }

        }

        [Fact]
        public void GetFormFieldTest()
        {
            DbContextOptions<AppDbContext> options = GetDbContextOptions();

            using (var context = new AppDbContext(options))
            {

                FormFieldRepository formFieldRepository = new FormFieldRepository(context);

                FormField formField = formFieldRepository.GetFormField(1);

                Output.WriteLine("FormfieldType is " + formField.FormFieldType);

                Assert.Equal("select", formField.FormFieldType);

            }
        }


        private static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Medical Student Education Conference",
                        ConferenceShortName = "MSE19",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2019"),
                        ConferenceTypeId = 2
                    },

                    new Conference
                    {
                        ConferenceId = 101,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("May 15, 2019"),
                        ConferenceInactive = false,
                        ConferenceLongName = "2019 Annual Conference",
                        ConferenceShortName = "AN19",
                        ConferenceStartDate = DateTime.Parse("May 11, 2019"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar MSE19" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1002, SubmissionCategoryName = "Poster MSE19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1003, SubmissionCategoryName = "Lecture AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1004, SubmissionCategoryName = "Seminar AN19" },
                    new SubmissionCategory { ConferenceId = 101, SubmissionCategoryId = 1005, SubmissionCategoryName = "Poster AN19" }


                );

                context.SaveChanges();

                context.SubmissionStatuses.AddRange(

                    new SubmissionStatus { SubmissionStatusId = 2000, SubmissionStatusName = SubmissionStatusType.OPEN },
                    new SubmissionStatus { SubmissionStatusId = 2001, SubmissionStatusName = SubmissionStatusType.PENDING_REVIEW }

               );

                context.SaveChanges();

                Conference aConference = context.Conferences.Find(100);

                context.Submissions.AddRange(

                    new Submission
                    {

                        AcceptedCategoryId = 1001,
                        Conference = aConference,
                        SubmissionAbstract = "Test Abstract",
                        SubmissionCategoryId = 1000,
                        SubmissionCreatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionId = 10001,
                        SubmissionLastUpdatedDateTime = DateTime.Parse("Feb 10, 2018"),
                        SubmissionStatusId = 2000,
                        SubmissionTitle = "Test Title",

                    }

                );

                context.SaveChanges();

                ConferenceSession aConferenceSession = new ConferenceSession { SessionCode = "S01", SessionLocation = "Green Room", SessionStartDateTime = DateTime.Parse(" Apr 10, 2018") };

                SubmissionPayment submissionPayment = new SubmissionPayment { PaymentAmount = 25, PaymentDateTime = DateTime.Parse(" Apr 10, 2018"), SubmissionId = 10001, PaymentTransactionId = "P999" };


                Submission aSubmission = context.Submissions.Find(10001);

                aSubmission.ConferenceSession = aConferenceSession;

                aSubmission.SubmissionPayment = submissionPayment;

                context.Update(aSubmission);

                context.SaveChanges();

                //Create some participant roles

                context.ParticipantRoles.AddRange(

                    new ParticipantRole { ParticipantRoleId = 1, ParticipantRoleName = ParticipantRoleType.LEAD_PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 2, ParticipantRoleName = ParticipantRoleType.PRESENTER },
                    new ParticipantRole { ParticipantRoleId = 3, ParticipantRoleName = ParticipantRoleType.SUBMITTER }


                );

                context.SaveChanges();

                //Create some participants 

                context.Participants.AddRange(

                    new Participant { ParticipantId = 4001, StfmUserId = "3001" },
                    new Participant { ParticipantId = 4002, StfmUserId = "3002" },
                    new Participant { ParticipantId = 4003, StfmUserId = "3003" }

                );

                context.SaveChanges();


                //create SubmissionParticipant records to add participants to the submission

                context.SubmissionParticipant.AddRange(

                    new SubmissionParticipant { SubmissionParticipantId = 1, ParticipantId = 4001, SubmissionId = 10001 },
                    new SubmissionParticipant { SubmissionParticipantId = 2, ParticipantId = 4002, SubmissionId = 10001 }
                );


                context.SaveChanges();

                context.SubmissionParticipantToParticipantRole.AddRange(

                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 1, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 3, SubmissionParticipantId = 1 },
                    new SubmissionParticipantToParticipantRole { ParticipantRoleId = 2, SubmissionParticipantId = 2 }


                );

                context.SaveChanges();

                context.FormFields.AddRange(

                    new FormField
                    {

                        FormFieldId = 2,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "screentext",
                        FormFieldSortOrder = 1,
                        FormFieldRole = "proposal",
                    AnswerRequired = false
                        

                    },


                    new FormField
                    {

                        FormFieldId = 3,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "textarea",
                        FormFieldSortOrder = 2,
                        FormFieldRole = "proposal",
                    AnswerRequired = true

                    },

                    new FormField
                    {

                        FormFieldId = 1,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "select",
                        FormFieldSortOrder = 3,
                        FormFieldRole = "proposal",
                    AnswerRequired = true
                        

                    }, 

                    new FormField
                    {

                        FormFieldId = 4,
                        SubmissionCategoryId = 1000,
                        FormFieldDeleted = false,
                        FormFieldType = "text",
                        FormFieldSortOrder = 4,
                        FormFieldRole = "proposal",
                    AnswerRequired = true

                    }



                );

                context.SaveChanges();

                context.FormFieldProperties.AddRange(

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 1,
                        FormFieldId = 2,
                        PropertyName = "textvalue",
                        PropertyValue = "Outline your session below."
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 2,
                        FormFieldId = 3,
                        PropertyName = "rows",
                        PropertyValue = "5"
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 3,
                        FormFieldId = 3,
                        PropertyName = "cols",
                        PropertyValue = "50"
                    },

                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 4,
                        FormFieldId = 1,
                        PropertyName = "options",
                        PropertyValue = "Medical Students,Residents,Physicians"
                    },


                    new FormFieldProperty
                    {


                        FormFieldPropertyId = 5,
                        FormFieldId = 1,
                        PropertyName = "label",
                        PropertyValue = "Select the intended audience"
                    },
                  
                    new FormFieldProperty
                     {


                         FormFieldPropertyId = 6,
                         FormFieldId = 4,
                         PropertyName = "label",
                         PropertyValue = "Enter the number of chairs required"
                     }



                );

                context.SaveChanges();

                context.SubmissionFormFields.AddRange(




                    new SubmissionFormField
                    {

                        SubmissionFormFieldId = 1,
                        FormFieldId = 3,
                        SubmissionFormFieldValue = "Residents",
                        SubmissionId = 10001


                    },
                    new SubmissionFormField
                    {

                        SubmissionFormFieldId = 2,
                        FormFieldId = 2,
                        SubmissionFormFieldValue = "This is an outline of my submission.",
                        SubmissionId = 10001


                },
                    new SubmissionFormField
                    {

                        SubmissionFormFieldId = 3,
                        FormFieldId = 4,
                        SubmissionFormFieldValue = "I need 5 chairs",
                        SubmissionId = 10001


                    }

                );

                context.SaveChanges();


            }



            return options;
        }

    }
}
