﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    /// <summary>
    /// This class models a submission review assignment for a user
    /// and the status of the assigned review.
    /// </summary>
    public class AssignedSubmissionViewModel
    {
        public SubmissionReviewer   SubmissionReviewer  { get; set; }
        public ReviewStatus         ReviewStatus        { get; set; }

        /// <summary>
        /// Gets or sets the review identifier. NOTE: this value
        /// may be 0 if the reviewer has not yet started the 
        /// review.
        /// </summary>
        /// <value>The review identifier.</value>
        public int ReviewId { get; set; }

        #region Helper Methods

        public string GetReviewStatusPretty => ReviewStatus.ReviewStatusType.ToString().Replace('_', ' ');

        #endregion Helper Methods
    }
}
