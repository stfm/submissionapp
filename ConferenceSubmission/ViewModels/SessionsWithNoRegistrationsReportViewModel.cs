﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class SessionsWithNoRegistrationsReportViewModel
    {
        public SessionsWithNoRegistrationsReportViewModel()
        {
            SessionsWithNoRegistrations = new List<SessionWithNoRegistrationViewModel>();
        }

        public string ConferenceName { get; set; }
        public string EventName { get; set; }
        public List<SessionWithNoRegistrationViewModel> SessionsWithNoRegistrations { get; set; }
    }

    public class SessionWithNoRegistrationViewModel
    {
        public string SubmissionTitle { get; set; }
        public int SubmissionId { get; set; }
        public string SessionCode { get; set; }
        public string LeadPresenterFullName { get; set; }
        public string LeadPresenterEmail { get; set; }
        public string SubmitterFullName { get; set; }
        public string SubmitterEmail { get; set; }
    }
}
