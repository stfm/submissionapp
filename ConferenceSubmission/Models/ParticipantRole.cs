﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConferenceSubmission.Models
{
    public class ParticipantRole
    {
        public ParticipantRole()
        {
        }

        public int ParticipantRoleId { get; set; }

        [StringLength(50)]
        [Required]
        public ParticipantRoleType ParticipantRoleName { get; set; }

		public override string ToString()
		{
			return "ParticipantRoleId: " + ParticipantRoleId + " | ParticipantRoleName: " + ParticipantRoleName;
		}


    }
}
