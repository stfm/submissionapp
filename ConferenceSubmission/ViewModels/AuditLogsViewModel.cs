﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class AuditLogsViewModel
    {
        public int          EntityId    { get; set; }
        public EntityType   EntityType  { get; set; }
    }
}
