﻿using System;
using System.Collections.Generic;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;

namespace ConferenceSubmission.Data
{
    public interface ISubmissionParticipantRepository
    {

        /// <summary>
        /// Get all SubmissionParticipants for the provided
        /// submission ID.
        /// </summary>
        /// <returns>Collection of SubmissionParticipants</returns>
        /// <param name="submissionId">Submission identifier.</param>
        IEnumerable<SubmissionParticipant> GetSubmissionParticipants(int submissionId);

        /// <summary>
        /// Creates the submission participant.
        /// </summary>
        /// <returns>The submission participant.</returns>
        /// <param name="submissionParticipant">Submission participant.</param>
        int CreateSubmissionParticipant( SubmissionParticipant submissionParticipant);

        /// <summary>
        /// Deletes the submission participant.
        /// </summary>
        /// <param name="submissionParticipantId">Submission participant identifier.</param>
        void DeleteSubmissionParticipant(int submissionParticipantId);

        /// <summary>
        /// Updates the submission participant roles. NOTE the provided collection
        /// of participantRoleIds will be used to completely replace
        /// any existing SubmissionParticipantToParticipantRole objects associated
        /// with provided SubmissionParticipantId.
        /// </summary>
        /// <param name="submissionParticipantId">Submission participant identifier.</param>
        /// <param name="participantRoleIds">Collection of participantRoleIds that identify each new ParticipantRole this SubmissionParticipant should have.</param>
        void UpdateSubmissionParticipantRoles(int submissionParticipantId, List<int> participantRoleIds);

        /// <summary>
        /// Gets the submission participant using the provided participant ID
        /// and Submission ID.
        /// </summary>
        /// <returns>The submission participant.</returns>
        /// <param name="participantId">Participant identifier.</param>
        /// <param name="submissionId">Submission identifier.</param>
        SubmissionParticipant GetSubmissionParticipant(int participantId, int submissionId);

        /// <summary>
        /// For each SubmissionParticipant in the given a list of SubmissionParticipants, 
        /// update the SortOrder property in the database. This method updates ONLY the SortOrder property.
        /// </summary>
        /// <param name="submissionParticipantsReordered">A list of SubmissionParticipants whose SortOrder property need updating.</param>
        /// /// <param name="submissionId">Unique identifier of the submission</param>
        void UpdateSubmissionParticipantSortOrder(List<SubmissionParticipant> submissionParticipantsReordered, int submissionId);

        /// <summary>
        /// Adds a SubmissionParticipant object to the database.
        /// </summary>
        /// <param name="submissionParticipant"></param>
        /// <returns>Returns the database Id of the newly created SubmissionParticipant</returns>
        int AddSubmissionParticipant(SubmissionParticipant submissionParticipant);

        /// <summary>
        /// Removes a SubmissionParticipantToParticipantRole object
        /// </summary>
        /// <param name="submissionParticipantToParticipantRoleId">Unique identifier of the SubmissionParticipantToParticipantRole</param>
        void RemoveSubmissionParticipantToParticipantRole(int submissionParticipantToParticipantRoleId);

        /// <summary>
        /// Adds a SubmissionParticipantToParticipantRole object
        /// </summary>
        /// <param name="submissionParticipantId">Id of a SubmissionParticipant</param>
        /// <param name="participantRoleId">Id of a ParticipantRole</param>
        /// <returns>Database Id of the newly created SubmissionParticipantToParticipantRole</returns>
        int AddSubmissionParticipantToParticipantRole(int submissionParticipantId, int participantRoleId);

        /// <summary>
        /// Gets the participants on more then two submissions that have the provided 
        /// submissionStatusId and are in the provided conferenceId.
        /// </summary>
        /// <returns>The participants more then two submissions.</returns>
        /// <param name="conferenceId">Conference identifier.</param>
        /// <param name="submissionStatusId">Submission status identifier.</param>
        List<int> GetParticipantsMoreThenTwoSubmissions(int conferenceId, int submissionStatusId);

        /// <summary>
        /// Gets a list of conference submissions where each submission has at least one submission participant that has been added or removed
        /// as of a specified date.
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <param name="asOfDate">Only include participants added or removed after as of this date or later</param>
        /// <returns>List of submissions and the participants that have been added or removed</returns>
        List<SubmissionWithParticipantsAddedOrRemoved> GetSubmissionsWithParticipantsAddedOrRemoved(int conferenceId, DateTime asOfDate);

    }
}
