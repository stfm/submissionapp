﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.Data;
using ConferenceSubmission.DTOs.Review;
using ConferenceSubmission.Models;
using ConferenceSubmission.ViewModels;
using Serilog;

namespace ConferenceSubmission.Services
{
    public class ReviewerService : IReviewerService
    {
        private readonly IReviewerRepository  _reviewerRepository;
        private readonly IReviewStatusService _reviewStatusService;
        private readonly ISalesforceAPIService _salesforceAPIService;
        private readonly ISubmissionReviewerRepository _submissionReviewerRepository;

        public ReviewerService(IReviewerRepository reviewerRepository, IReviewStatusService reviewStatusService, 
            ISalesforceAPIService salesforceAPIService, ISubmissionReviewerRepository submissionReviewerRepository)
        {
            _reviewerRepository  = reviewerRepository;
            _reviewStatusService = reviewStatusService;
            _salesforceAPIService = salesforceAPIService;
            _submissionReviewerRepository = submissionReviewerRepository;
        }

        public void AssignReviewerToSubmissions(string reviewerId, List<string> submissionIdList)
        {
            int reviewerIdInt = int.Parse(reviewerId);
            foreach (var submissionId in submissionIdList)
            {
                SubmissionReviewer submissionReviewer = new SubmissionReviewer
                {
                    ReviewerId = reviewerIdInt,
                    SubmissionId = int.Parse(submissionId)
                };

                _submissionReviewerRepository.AddSubmissionReviewer(submissionReviewer);

            }
           
        }

        public int CreateReviewer(Reviewer reviewer)
        {
            return _reviewerRepository.CreateReviewer(reviewer);
        }

        public List<SubmissionReviewer> GetAssignedSubmissions(string stfmUserId)
        {
            //Check to see if the current user already exists as a Reviewer
            if (_reviewerRepository.ReviewerExists(stfmUserId))
            {
                //Get any Submission Reviewer assignments for this user
                return _reviewerRepository.GetAssignedSubmissions(stfmUserId);
            }
            else
            {
                //Create a new Reviewer object using the current user's STFM User Id
                _reviewerRepository.CreateReviewer(new Reviewer { StfmUserId = stfmUserId });

                /* If we're here, a new reviewer has been created (who will not yet have any submission review assignments).
                   Just return an empty list.
                */
                return new List<SubmissionReviewer>();
            }
        }

        public List<AssignedSubmissionViewModel> GetAssignedSubmissionWithReviewStatus(string stfmUserId)
        {
            return GetAssignedSubmissions(stfmUserId).Select(s => new AssignedSubmissionViewModel {
                SubmissionReviewer = s,
                ReviewStatus = _reviewStatusService.GetReviewStatusForReview(s.SubmissionId, s.ReviewerId),
                ReviewId = _reviewStatusService.GetReviewIdForReview(s.SubmissionId, s.ReviewerId)
            }).ToList();
        }


        public List<AssignedSubmissionViewModel> GetAssignedSubmissionWithReviewStatus(string stfmUserId, int conferenceId)
        {
            return _reviewerRepository.GetAssignedReviews(stfmUserId, conferenceId).
                Select(s => new AssignedSubmissionViewModel
                {
                SubmissionReviewer = s,
                ReviewStatus = _reviewStatusService.GetReviewStatusForReview(s.SubmissionId, s.ReviewerId),
                ReviewId = _reviewStatusService.GetReviewIdForReview(s.SubmissionId, s.ReviewerId)
                }).ToList();

        }

        public NextReview GetNextReview(string stfmUserId)
        {
            return _reviewerRepository.GetNextReview(stfmUserId);
        }

        public Reviewer GetReviewer(string stfmUserId)
        {

            Reviewer reviewer =  _reviewerRepository.GetReviewerByStfmUserId(stfmUserId);

            User user = _salesforceAPIService.GetUserInfo(stfmUserId).Result;

            if (user != null)
            {
                reviewer.ReviewerName = user.GetFullNamePretty();

                reviewer.ReviewerEmail = user.Email;
            }

            return reviewer;

        }

        public List<Reviewer> GetReviewers()
        {
            List<Reviewer> reviewers = _reviewerRepository.GetReviewers.ToList();

            //Initializing a List that will store all participant StfmUserIds from all submissions
            List<string> stfmUserIds = new List<string>();

            reviewers.ForEach(r => stfmUserIds.Add(r.StfmUserId));


            Log.Information("number of reviewers to find in SalesForce is " + stfmUserIds.Count());

            var users = _salesforceAPIService.GetMultipleUserInfo(stfmUserIds).Result;

            //Now match the users returned from Salesforce with the reviewers to populate the reviewers data that comes from SalesForce
            
           foreach (Reviewer reviewer in reviewers) {

                User user = users.FirstOrDefault(u => u.STFMUserId == reviewer.StfmUserId);

                if (user != null)
                {
                    reviewer.ReviewerName = user.GetFullNamePretty();

                    reviewer.ReviewerEmail = user.Email;
                }

            }

            return reviewers;
        }

        public List<ReviewerSummaryForConference> GetReviewerSummaryForConferences(string stfmUserId)
        {
            List<ReviewerSummaryForConference> reviewerSummaryForConferences = new List<ReviewerSummaryForConference>();

            List<SubmissionReviewer> submissions = _reviewerRepository.GetAllAssignedSubmissions(stfmUserId);

            foreach (SubmissionReviewer submissionReviewer in submissions)
            {
                //Is the conference in reviewerSummaryForConferences - if not add it

                int index = reviewerSummaryForConferences.FindIndex(rs => rs.conference.ConferenceId == submissionReviewer.Submission.Conference.ConferenceId);

                if ( index > -1)
                {
                    ReviewerSummaryForConference reviewerSummaryForConference = reviewerSummaryForConferences[index];

                    reviewerSummaryForConference.totalAssignedSubmissionsToReview += 1;

                    ReviewStatus reviewStatus = _reviewStatusService.GetReviewStatusForReview(submissionReviewer.SubmissionId, submissionReviewer.ReviewerId);

                    if (reviewStatus.ReviewStatusType == ReviewStatusType.COMPLETE)
                    {
                        reviewerSummaryForConference.totalReviewsCompleted += 1;
                    }
                } else
                {

                    ReviewerSummaryForConference reviewerSummaryForConference = new ReviewerSummaryForConference
                    {
                        conference = submissionReviewer.Submission.Conference,
                        totalAssignedSubmissionsToReview = 1

                    };

                     ReviewStatus reviewStatus = _reviewStatusService.GetReviewStatusForReview(submissionReviewer.SubmissionId, submissionReviewer.ReviewerId);

                    if (reviewStatus.ReviewStatusType == ReviewStatusType.COMPLETE)
                    {
                        reviewerSummaryForConference.totalReviewsCompleted = 1;
                    }

                    reviewerSummaryForConferences.Add(reviewerSummaryForConference);

                }

            }

            return reviewerSummaryForConferences;
        }

        public void UnassignReviewerFromSubmissions(string reviewerId, List<string> submissionIdList)
        {
            int reviewerIdInt = int.Parse(reviewerId);

            foreach (var submissionId in submissionIdList)
            {
                int submissionIdInt = int.Parse(submissionId);

                SubmissionReviewer submissionReviewer = _submissionReviewerRepository.GetSubmissionReviewer(submissionIdInt, reviewerIdInt);

                _submissionReviewerRepository.DeleteSubmissionReviewer(submissionReviewer);

            }
        }
    }
}
