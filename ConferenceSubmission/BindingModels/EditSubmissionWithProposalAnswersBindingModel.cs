﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.BindingModels
{
    /// <summary>
    /// Models the data needed for a user to be able
    /// to edit the category specific details for a submission.
    /// </summary>
    public class EditSubmissionWithProposalAnswersBindingModel
    {
        [Required]
        public int                       SubmissionId           { get; set; }
        public string                    ConferenceName         { get; set; }
        public string                    SubmissionTitle        { get; set; }
        public string                    SubmissionCategoryName { get; set; }
        public List<SubmissionFormField> SubmissionFormFields   { get; set; }
        public List<FormField>           FormFields { get; set; }
        public bool                      SubmissionCompleted    { get; set; }

        #region Public Methods

        public string GetSubmissionFormFieldValue(int formFieldId)
        {
            var submissionFormField = SubmissionFormFields.FirstOrDefault(s => s.FormFieldId == formFieldId);
            return submissionFormField == null ? string.Empty : submissionFormField.SubmissionFormFieldValue;
        }

        #endregion
    }
}
