﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceSubmission.DTOs;
using ConferenceSubmission.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ConferenceSubmission.Controllers
{
    [Route("api/submission")]
    [ApiController]
    public class SubmissionApiController : ControllerBase
    {

        private readonly ISubmissionService _submissionService;

        private readonly IFormFieldService _formFieldService;

        public SubmissionApiController(ISubmissionService submissionService, IFormFieldService formFieldService)
        {
            _submissionService = submissionService;

            _formFieldService = formFieldService;
        }

        [HttpPut("update/{id:int}")]
        public IActionResult UpdateSubmission(int id, SubmissionUpdate submissionUpdate)
        {

            _submissionService.UpdateSubmissionBaseDetails(submissionUpdate.SubmissionId, submissionUpdate.SubmissionTitle, submissionUpdate.SubmissionAbstract, true);

            if (submissionUpdate.LearningObjOneFormFieldId > 0)
            {
                _formFieldService.UpdateSubmissionFormFieldValue(submissionUpdate.LearningObjOneFormFieldId.Value, submissionUpdate.LearningObjOneValue);
            }

            if (submissionUpdate.LearningObjTwoFormFieldId > 0) { 
           
                _formFieldService.UpdateSubmissionFormFieldValue(submissionUpdate.LearningObjTwoFormFieldId.Value, submissionUpdate.LearningObjTwoValue);
            }

            if (submissionUpdate.LearningObjThreeFormFieldId > 0)
            {
                _formFieldService.UpdateSubmissionFormFieldValue(submissionUpdate.LearningObjThreeFormFieldId.Value, submissionUpdate.LearningObjThreeValue);
            }

            return Ok(submissionUpdate);
        }
    }
}