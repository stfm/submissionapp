﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Models
{
    public class AuditLog
    {
        public AuditLog()
        {
            AuditLogParentEntities = new List<AuditLogParentEntity>();
        }

        public int          AuditLogId      { get; set; }
        public string       OldValue        { get; set; }
        public string       NewValue        { get; set; }

        [MaxLength(256)]
        public string       PropertyName    { get; set; }

        //Primary key of the entity that was modiefied
        public int          EntityId        { get; set; }

        //Primary key of the parent entity of the entity that was modified 
        public int?         ParentId        { get; set; }

        public DateTime     TimeStamp       { get; set; }

        [MaxLength(128)]
        public string       ChangedBy       { get; set; }

        public EntityType   EntityType       { get; set; }

        public AuditLogActionType AuditLogActionType { get; set; }

        public List<AuditLogParentEntity> AuditLogParentEntities { get; set; }
    }

    public enum AuditLogActionType
    {
        CREATED,
        DELETED,
        MODIFIED
    }
}
