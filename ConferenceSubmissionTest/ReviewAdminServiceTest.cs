﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConferenceSubmission.Data;
using ConferenceSubmission.Models;
using ConferenceSubmission.Services;
using ConferenceSubmission.ViewModels;
using Moq;
using Xunit;

namespace ConferenceSubmissionTest
{
    public class ReviewerAdminServiceTest
    {
        [Fact]
        public void GetAverageReviewScoreForAllReviews()
        {

            Review review1 = new Review()
            {

                ReviewId = 1

            };

            Review review2 = new Review()
            {

                ReviewId = 2

            };

            List<Review> reviews = new List<Review>() {

                review1,
                review2

            };

            Mock<IReviewRepository> reviewRepository = new Mock<IReviewRepository>();

            reviewRepository.Setup(rr => rr.GetCompletedReviews(100)).Returns(reviews);

            ReviewFormField reviewFormField1 = new ReviewFormField()
            {

                ReviewFormFieldValue = "5"


            };

            ReviewFormField reviewFormField2 = new ReviewFormField()
            {

                ReviewFormFieldValue = "1"


            };

            ReviewFormField reviewFormField3 = new ReviewFormField()
            {

                ReviewFormFieldValue = "2"


            };

            List<ReviewFormField> reviewFormFields = new List<ReviewFormField>() {

                reviewFormField1,
                reviewFormField2,
                reviewFormField3

            };

            List<int> reviewIds = new List<int>() { 1, 2 };

            Mock<IReviewFormFieldRepository> reviewFormFieldRepository = new Mock<IReviewFormFieldRepository>();

            reviewFormFieldRepository.Setup(rffr => rffr.GetRatingScaleFormFieldsForMultipleReviews(reviewIds)).Returns(reviewFormFields);


            ReviewAdminService reviewAdminService = new ReviewAdminService( reviewFormFieldRepository.Object, reviewRepository.Object, null, null, null, null, null, null);

            double averageReviewScore = reviewAdminService.GetAverageReviewScoreForAllReviews(100);

            Assert.Equal(2.7, averageReviewScore);

        }

        [Fact]
        public void GetAverageReviewScoreForSingleReview()
        {

            Review review1 = new Review()
            {

                ReviewId = 1

            };

            Review review2 = new Review()
            {

                ReviewId = 2

            };

            List<Review> reviews = new List<Review>() {

                review1,
                review2

            };

            Mock<IReviewRepository> reviewRepository = new Mock<IReviewRepository>();

            reviewRepository.Setup(rr => rr.GetCompletedReviews(100)).Returns(reviews);

            ReviewFormField reviewFormField1 = new ReviewFormField()
            {

                ReviewFormFieldValue = "3"


            };

            ReviewFormField reviewFormField2 = new ReviewFormField()
            {

                ReviewFormFieldValue = "1"


            };

            ReviewFormField reviewFormField3 = new ReviewFormField()
            {

                ReviewFormFieldValue = "2"


            };

            List<ReviewFormField> reviewFormFields = new List<ReviewFormField>() {

                reviewFormField1,
                reviewFormField2,
                reviewFormField3

            };

            List<int> reviewIds = new List<int>() { 1, 2 };

            Mock<IReviewFormFieldRepository> reviewFormFieldRepository = new Mock<IReviewFormFieldRepository>();

            reviewFormFieldRepository.Setup(rffr => rffr.GetRatingScaleFormFieldsForSingleReview(1)).Returns(reviewFormFields);


            ReviewAdminService reviewAdminService = new ReviewAdminService(reviewFormFieldRepository.Object, reviewRepository.Object, null, null, null, null, null, null);

            double averageReviewScore = reviewAdminService.GetAverageReviewScoreForSingleReview(1);

            Assert.Equal(2.0, averageReviewScore);

        }
    }
}
