﻿using System;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ConferenceSubmission.Models;
using System.Collections.Generic;

namespace ConferenceSubmissionTest
{
    public class ConferenceTest
    {
        protected Xunit.Abstractions.ITestOutputHelper Output { get; }

        public ConferenceTest(Xunit.Abstractions.ITestOutputHelper output)
        {

            Output = output;
        }



        [Fact]
        public void GetConferencesTest()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(connection)
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                context.Database.EnsureCreated();
                context.ConferenceTypes.AddRange(
                    new ConferenceType { ConferenceTypeId = 1, ConferenceTypeName = "Annual" },
                    new ConferenceType { ConferenceTypeId = 2, ConferenceTypeName = "Medical Student Education" });
                context.SaveChanges();

                context.Conferences.AddRange(
                    new Conference
                    {
                        ConferenceId = 100,
                        ConferenceDeleted = false,
                        ConferenceEndDate = DateTime.Parse("Jan 15, 2018"),
                        ConferenceInactive = false,
                        ConferenceLongName = "Test Conference 1",
                        ConferenceShortName = "TC1",
                        ConferenceStartDate = DateTime.Parse("Jan 11, 2018"),
                        ConferenceTypeId = 1
                    }

                );

                context.SaveChanges();

                context.SubmissionCategories.AddRange(

                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1000, SubmissionCategoryName = "Lecture TC1" },
                    new SubmissionCategory { ConferenceId = 100, SubmissionCategoryId = 1001, SubmissionCategoryName = "Seminar TC1" }


                );

                context.SaveChanges();


            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new AppDbContext(options))
            {
                IEnumerable<Conference> conferences = context.Conferences;

                foreach (var conference in conferences)
                {

                    Output.WriteLine("Conference is " + conference.ConferenceLongName );


                }

                Assert.NotNull(conferences);


                IEnumerable<SubmissionCategory> submissionCategories = context.SubmissionCategories;


                foreach (var submissionCategory in submissionCategories ) {


                    Output.WriteLine("Submission Category is " + submissionCategory.SubmissionCategoryName +
                                     " and Conference is " + submissionCategory.Conference.ConferenceLongName);

                    Assert.Equal("TC1", submissionCategory.Conference.ConferenceShortName);


                }




            }


        }

    }
}
