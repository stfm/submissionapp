﻿using ConferenceSubmission.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.ViewModels
{
    public class AddParticipantViewModel
    {
        public AddParticipantViewModel()
        {
            CurrentParticipants = new List<string>();
            ParticipantRoleDefinitions = new List<string>();
        }
        #region Public Propeties
        [Required]
        public int                  SubmissionId        { get; set; }

        [Required]
        [Range(0, 3, ErrorMessage ="Please select a role.")]
        [Display(Name ="Select your role on the submission")]
        public int                  Role                { get; set; }

        public string               ConferenceTitle     { get; set; }
        public string               SubmittedCategory   { get; set; }
        public string               AcceptedCategory    { get; set; }
        public string               SubmissionTitle     { get; set; }

        public bool                 SubmissionParticipantExists { get; set; }

        /// <summary>
        /// The participants that have already been added to the submission
        /// </summary>
        public List<string> CurrentParticipants { get; set; }

        public List<string> ParticipantRoleDefinitions { get; set; }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Returns a list of select list items where each item represents a participant role the user can select
        /// </summary>
        /// <returns>List of SelectList items</returns>
        public List<SelectListItem> GetParticipantRoleSelectList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text="Select a Role",
                    Value="-1",
                    //Selected = true
                },
                new SelectListItem
                {
                    Text = "Lead Presenter",
                    Value = $"{(int)ParticipantRoleType.LEAD_PRESENTER}",
                    //Selected = false
                },
                new SelectListItem
                {
                    Text = "Presenter",
                    Value = $"{(int)ParticipantRoleType.PRESENTER}",
                    //Selected = false
                },
                new SelectListItem
                {
                    Text = "Author, Not Presenting",
                    Value = $"{(int)ParticipantRoleType.AUTHOR}",
                    //Selected = false
                }
            };
        }

        #endregion
    }
}
