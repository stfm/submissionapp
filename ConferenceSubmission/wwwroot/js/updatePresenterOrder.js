﻿$(document).ready(function () {
    $("#presentersList").sortable({
        cursor: "move",
        delay: 200,
        placeholder: "highlight",
        update: function (event, ui) {
            $("#SubmissionParticipantIds").val($("#presentersList").sortable("toArray"));
        }
    });
});


