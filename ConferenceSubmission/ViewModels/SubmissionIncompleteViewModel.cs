﻿using System;
using ConferenceSubmission.Models;

namespace ConferenceSubmission.ViewModels

{
    /// <summary>
    /// Stores data for each incomplete submission
    /// that we need to show in the view.
    /// </summary>
    public class SubmissionIncompleteViewModel
    {

        public int SubmissionId { get; set; }

        public string SubmissionTitle { get; set; }

        public SubmissionCategory SubmissionCategory { get; set; }

        public User Submitter { get; set; }

        public User MainPresenter { get; set; }

        public SubmissionIncompleteViewModel()
        {
        }
    }
}
