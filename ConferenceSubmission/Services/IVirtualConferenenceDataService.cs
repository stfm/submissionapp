﻿using ConferenceSubmission.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConferenceSubmission.Services
{
    public interface IVirtualConferenenceDataService
    {
        /// <summary>
        /// Get VirtualConferenceData item using the 
        /// provided conferenceId
        /// </summary>
        /// <param name="conferenceId"></param>
        /// <returns>VirtualConferenceData</returns>
        VirtualConferenceData GetVirtualConferenceData(int conferenceId);
    }
}
